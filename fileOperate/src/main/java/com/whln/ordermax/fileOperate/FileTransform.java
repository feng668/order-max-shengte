package com.whln.ordermax.fileOperate;

import cn.afterturn.easypoi.excel.ExcelXorHtmlUtil;
import cn.afterturn.easypoi.excel.entity.ExcelToHtmlParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.resource.ClassPathResource;
import com.aspose.cells.*;
import com.spire.doc.Document;
import com.spire.doc.FileFormat;
import com.spire.doc.Section;
import com.spire.doc.documents.DocumentObjectType;
import com.spire.doc.fields.DocPicture;
import com.spire.doc.interfaces.ICompositeObject;
import com.spire.doc.interfaces.IDocumentObject;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 文件格式转换
 */
public class FileTransform {

    public FileTransform() {
        InputStream is = new ClassPathResource("license.xml").getStream();
        License aposeLic = new License();
        aposeLic.setLicense(is);
    }

    /**
     * word转html
     * @param file http文件流
     * @return html字符串
     */
    public String wordToHtml(MultipartFile file){
        String result = null;
        try {
            InputStream is  = file.getInputStream();
            Document doc = new Document(is);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            doc.saveToStream(baos, FileFormat.Html);
            result = IoUtil.toStr(baos, Charset.forName("utf8"));
            is.close();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * excel转html
     * @param file http文件流
     * @return html字符串
     */
    public String excelToHtml(MultipartFile file){
        try {
            String suffix = FileUtil.getSuffix(file.getOriginalFilename());
            byte[] bytes = file.getBytes();
            InputStream is = new ByteArrayInputStream(bytes);
            org.apache.poi.ss.usermodel.Workbook workbook;
            try {
                workbook = new HSSFWorkbook(is);
            }catch (OfficeXmlFileException e){
                workbook = new XSSFWorkbook(is);
            }
            ExcelToHtmlParams excelToHtmlParams = new ExcelToHtmlParams(workbook,true,"yes");
            is.close();
            return ExcelXorHtmlUtil.excelToHtml(excelToHtmlParams);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) throws FileNotFoundException {
        String a="<html>\n" +
                " <head></head>\n" +
                " <body style = 'font-family: SimSun'>\n" +
                "  <table data-sort=\"sortDisabled\" align=\"center\">\n" +
                "   <tbody>\n" +
                "    <tr class=\"firstRow\">\n" +
                "     <td valign=\"top\" rowspan=\"1\" colspan=\"6\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\" align=\"center\" width=\"831\"><h2>Quotaton Price Sheet</h2></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "     <td valign=\"top\" rowspan=\"1\" colspan=\"2\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\" width=\"257\">Customer:北京出口公司</td>\n" +
                "     <td width=\"110\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"77\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"181\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "     <td valign=\"top\" style=\"border-width: 1px; border-style: solid; word-break: break-all;\" rowspan=\"1\" colspan=\"6\" align=\"right\" width=\"70\">Date:2023-01-04</td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "     <td valign=\"top\" colspan=\"1\" rowspan=\"1\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td valign=\"top\" colspan=\"1\" rowspan=\"1\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td valign=\"top\" colspan=\"1\" rowspan=\"1\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td valign=\"top\" colspan=\"1\" rowspan=\"1\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td valign=\"top\" colspan=\"1\" rowspan=\"1\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td valign=\"top\" colspan=\"1\" rowspan=\"1\" style=\"border-width: 1px; border-style: solid;\" width=\"70\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "     <td width=\"71\" valign=\"top\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\"><span style=\"font-size: 12px;\">No.</span></td>\n" +
                "     <td width=\"165\" valign=\"top\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\"><span style=\"font-size: 12px;\">Commodities Description&nbsp;</span></td>\n" +
                "     <td width=\"110\" valign=\"top\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\"><span style=\"font-size: 12px;\">Part Number<br></span></td>\n" +
                "     <td width=\"57\" valign=\"top\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\"><span style=\"font-size: 12px;\">Qty.</span></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\"><span style=\"font-size: 12px;\">Unit Price (USD)&nbsp;</span></td>\n" +
                "     <td width=\"178\" valign=\"top\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\"><span style=\"font-size: 12px;\">Total Amount(USD)&nbsp;</span></td>\n" +
                "    </tr>\n" +
                "<tr islist=\"islist\" listname=\"commodityList\">\n" +
                " <td width=\"71\" valign=\"top\" style=\"border-width: 1px; border-style: solid; word-break: break-all;\">1</td>\n" +
                " <td width=\"165\" valign=\"top\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\">二硼化钛</td>\n" +
                " <td width=\"110\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">11.TIB202A</td>\n" +
                " <td width=\"57\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">0</td>\n" +
                " <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">0</td>\n" +
                " <td width=\"178\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">0</td>\n" +
                "</tr>\n" +
                "<tr islist=\"islist\" listname=\"commodityList\">\n" +
                " <td width=\"71\" valign=\"top\" style=\"border-width: 1px; border-style: solid; word-break: break-all;\">1</td>\n" +
                " <td width=\"165\" valign=\"top\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\">铋粉</td>\n" +
                " <td width=\"110\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">02.4BI020A</td>\n" +
                " <td width=\"57\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">20</td>\n" +
                " <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">8</td>\n" +
                " <td width=\"178\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">160</td>\n" +
                "</tr>\n" +
                "<tr islist=\"islist\" listname=\"commodityList\">\n" +
                " <td width=\"71\" valign=\"top\" style=\"border-width: 1px; border-style: solid; word-break: break-all;\">1</td>\n" +
                " <td width=\"165\" valign=\"top\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\">铋粉</td>\n" +
                " <td width=\"110\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">02.4BI005A</td>\n" +
                " <td width=\"57\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">10</td>\n" +
                " <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">15</td>\n" +
                " <td width=\"178\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\">150</td>\n" +
                "</tr>\n" +
                "    <tr>\n" +
                "     <td width=\"71\" valign=\"top\" style=\"border-width: 1px; border-style: solid; word-break: break-all;\"><p>2</p></td>\n" +
                "     <td width=\"165\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"110\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"57\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"178\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "     <td width=\"71\" valign=\"top\" style=\"border-width: 1px; border-style: solid; word-break: break-all;\">3</td>\n" +
                "     <td width=\"165\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"110\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"57\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"111\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "     <td width=\"178\" valign=\"top\" style=\"border-width: 1px; border-style: solid;\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr> <td valign=\"top\" rowspan=\"1\" colspan=\"6\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\" width=\"831\">Noted: Please Be noted the the period of validity of this quotation is 15 days since the date of&nbsp; the sheet.</td></tr>" +
                "    <tr> <td valign=\"top\" rowspan=\"1\" colspan=\"6\" style=\"word-break: break-all; border-width: 1px; border-style: solid;\" width=\"831\">Noted: Please Be noted the the period of validity of this quotation is 15 days since the date of&nbsp; the sheet.</td></tr>" +
                "   </tbody>\n" +
                "  </table>\n" +
                "  <p><br></p>\n" +
                "<img src='https://img1.baidu.com/it/u=29494151,4044058952&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=750'/>"+
                " </body>\n" +
                "</html>";

    }
    /**
     * html转word
     * @param htmlStr html字符串
     * @param outputStream 输出流
     */
    public  void htmlToWord(String htmlStr, OutputStream outputStream) throws IOException {
//        Document doc = new Document();
        ByteArrayInputStream inputStream = IoUtil.toStream(htmlStr, StandardCharsets.UTF_8);
//        doc.loadFromStream(inputStream, FileFormat.Html, XHTMLValidationType.None);
        POIFSFileSystem poifs = new POIFSFileSystem();
        DirectoryEntry directory = poifs.getRoot();
        DocumentEntry documentEntry = directory.createDocument("WordDocument", inputStream);
        poifs.writeFilesystem(outputStream);
//        doc.saveToStream(outputStream,FileFormat.Doc);
    }


    /**
     * html转excel
     * @param htmlStr html字符串
     * @param outputStream 输出流
     */
    public void htmlToExcel(String htmlStr, OutputStream outputStream){
        ByteArrayInputStream bais = IoUtil.toStream(htmlStr, Charset.forName("utf8"));
        LoadOptions lo = new LoadOptions(LoadFormat.HTML);
        Workbook wb = null;
        try {
            wb = new Workbook(bais, lo);
            wb.save(outputStream,new XlsSaveOptions(SaveFormat.EXCEL_97_TO_2003));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void html2Pdf1(String html,OutputStream os) {
        //新建Document对象
        Document doc = new Document();
        //添加section
        Section sec = doc.addSection();

        sec.addParagraph().appendHTML(html);
        //将文档另存为PDF
        doc.saveToFile(os, FileFormat.PDF);
        doc.dispose();
    }
    public void html2Pdf(String html,OutputStream os) {
        //新建Document对象
        Document doc = new Document();
        //添加section
        Section sec = doc.addSection();
        sec.addParagraph().appendHTML(html);
        //将文档另存为PDF
        doc.saveToFile(os, FileFormat.PDF);
        doc.dispose();
    }

    public void htmlToExcel2(String htmlStr, OutputStream outputStream){
        try {
            org.apache.poi.ss.usermodel.Workbook workbook = ExcelXorHtmlUtil.htmlToExcel(htmlStr, ExcelType.HSSF);
            workbook.write(outputStream);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getDocImg(Document document) throws IOException {
        Queue nodes = new LinkedList();
        nodes.add(document); //创建List对象
        List<BufferedImage> images = new ArrayList(); //遍历文档中的子对象
        while (nodes.size() > 0) {
            ICompositeObject node = (ICompositeObject) nodes.poll();
            for (int i = 0; i < node.getChildObjects().getCount(); i++) {
                IDocumentObject child = node.getChildObjects().get(i); if (child instanceof ICompositeObject) {
                    nodes.add((ICompositeObject) child); //获取图片并添加到List
                    if (child.getDocumentObjectType() == DocumentObjectType.Picture) {
                        DocPicture picture = (DocPicture) child;
                        images.add(picture.getImage());
                    }
                }
            }
        } //将图片保存为PNG格式文件
        for (int i = 0; i < images.size(); i++) {
            File file = new File(String.format("图片-%d.png", i));
//            BufferedImage
            String base64 = ImgUtil.toBase64(images.get(i), "jpg");
            System.out.println();
        }
    }
}
