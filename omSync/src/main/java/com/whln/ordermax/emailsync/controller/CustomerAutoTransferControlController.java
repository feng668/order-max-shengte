package com.whln.ordermax.emailsync.controller;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.emailsync.service.CustomerAutoTransferControlService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/customerAutoTransfer")
public class CustomerAutoTransferControlController {

    @Resource
    private CustomerAutoTransferControlService customerAutoTransferControlService;

    @PostMapping("/start")
    public Result start(){
        return customerAutoTransferControlService.start();
    }

    @PostMapping("/stop")
    public Result stop(){
        return customerAutoTransferControlService.stop();
    }
}
