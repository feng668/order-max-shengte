package com.whln.ordermax.emailsync.service;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.emailsync.sche.CustomerAutoTransferTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class CustomerAutoTransferControlService {
    

    @Resource
    private CustomerAutoTransferTask customerAutoTransferTask;


    public Result start() {
        log.info("开启客户移入公海任务");
        try {
            customerAutoTransferTask.start();
        }catch (Exception e){
            return Result.error("201","开启客户移入公海任务异常");
        }
        return Result.success();
    }

    public Result stop() {
        log.info("关闭客户移入公海任务");
        try {
            customerAutoTransferTask.stop();
        }catch (Exception e){
            return Result.error("201","关闭客户移入公海任务异常");
        }
        return Result.success();
    }
}
