package com.whln.ordermax.emailsync.config;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whln.ordermax.data.domain.CuCustomerTransferSetting;
import com.whln.ordermax.data.domain.SysUserEmail;
import com.whln.ordermax.data.repository.CuCustomerTransferSettingRepository;
import com.whln.ordermax.data.repository.SysUserEmailRepository;
import com.whln.ordermax.emailsync.sche.CustomerAutoTransferTask;
import com.whln.ordermax.emailsync.sche.EmailSyncTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
@Order(1)
@Slf4j
public class TaskAutoStart implements ApplicationRunner {

    @Resource
    private SysUserEmailRepository sysUserEmailRepository;

    @Resource
    private EmailSyncTask emailSyncTask;

    @Resource
    private CuCustomerTransferSettingRepository customerTransferSettingRepository;

    @Resource
    private CustomerAutoTransferTask customerAutoTransferTask;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("开始启动收件箱同步任务");
        List<SysUserEmail> userEmailList = sysUserEmailRepository.list(new LambdaQueryWrapper<SysUserEmail>().eq(SysUserEmail::getSycnStart, 1));
        if (userEmailList!=null&&userEmailList.size()>0){
            for (SysUserEmail sysUserEmail : userEmailList) {
                emailSyncTask.start(sysUserEmail);
            }
        }

        log.info("开始启动客户自动移入公海任务");
        List<CuCustomerTransferSetting> list = customerTransferSettingRepository.list(null);
        for (CuCustomerTransferSetting c:list){
            if (c!=null){
                Integer isActive = c.getIsActive();
                if (isActive==1){
                    customerAutoTransferTask.start();
                }
            }
        }
    }
}
