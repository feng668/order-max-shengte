package com.whln.ordermax.emailsync.sche;

import com.whln.ordermax.data.domain.CuCustomerTransferSetting;
import com.whln.ordermax.data.repository.CuCustomerTransferSettingRepository;
import com.whln.ordermax.emailsync.config.ScheduleConfig;
import com.whln.ordermax.emailsync.sche.base.BaseTask;
import com.whln.ordermax.emailsync.service.CustomerAutoTransferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ScheduledFuture;


@Component
@Slf4j
public class CustomerAutoTransferTask extends BaseTask {

    @Resource
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;    // 注入线程池任务调度类

    @Resource
    private CuCustomerTransferSettingRepository customerTransferSettingRepository;

    @Resource
    private CustomerAutoTransferService customerAutoTransferService;


    public void start(){
        CuCustomerTransferSetting customerTransferSetting = customerTransferSettingRepository.getOne(null);
        ScheduledFuture scheduledFuture = threadPoolTaskScheduler.scheduleAtFixedRate(() -> {
            customerAutoTransferService.sync();
        }, customerTransferSetting.getTransferInterval());
        ScheduleConfig.cache.put("CUSTOMER_AUTO_TRANSFER",scheduledFuture);
    }

    public void stop() {
        super.stop("CUSTOMER_AUTO_TRANSFER");
    }
}
