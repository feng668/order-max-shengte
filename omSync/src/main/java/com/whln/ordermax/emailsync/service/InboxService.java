package com.whln.ordermax.emailsync.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.repository.*;
import com.whln.ordermax.email.OmEmailHandle;
import com.whln.ordermax.email.entity.EmailInfo;
import com.whln.ordermax.email.entity.MyEmailAttachment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
@Slf4j
@Service
public class InboxService {

    @Resource
    private EmInboxRepository inboxRepository;

    @Resource
    private OmEmailHandle omEmailHandle;

    @Resource
    private SysUserRepository userRepository;

    @Resource
    private EmInboxAttachmentRepository inboxAttachmentRepository;

    private static final Snowflake SNOWFLAKE = IdUtil.getSnowflake(0, 0);

    @Value("${fileStore.emailReceived}")
    private String emailRootPath;

    @Resource
    private SysUserEmailRepository userEmailRepository;

    @Resource
    private SupContactsEmailRepository supContactsEmailRepository;

    @Resource
    private CuContactsEmailRepository cuContactsEmailRepository;


    public void sync(SysUserEmail userEmail) {
        log.info("pull email :{}", userEmail);
        userEmail = userEmailRepository.getById(userEmail.getId());
        //获取用户邮箱和密码
//        SysUserEmail userEmail = userEmailRepository.getById(userEmailId);
        List<String> exsitsMessageIdList = inboxRepository.listObjs(new QueryWrapper<EmInbox>().select("DISTINCT message_id"), o -> (String) o);
        //获取所有客户和供应商
        Map<String, String> supEmailIdMap = supContactsEmailRepository.listWithSupId().stream().collect(Collectors.toMap(SupContactsEmail::getEmailAddress, SupContactsEmail::getSupplierId, (x, y) -> x));
        Map<String, String> cusEmailIdMap = cuContactsEmailRepository.listWithCusId().stream().collect(Collectors.toMap(CuContactsEmail::getEmailAddress, CuContactsEmail::getCustomerId, (x, y) -> x));

        try {
            //获取邮箱收件服务器
            SysEmailServer emailServer = this.getServerByAddress(userEmail.getEmail());
            //获取当前邮箱所有收件箱
            List<EmailInfo> emailInfoList = omEmailHandle.getPopSSLInBoxByTime(userEmail.getEmail(), userEmail.getEmailPassword(), emailRootPath, emailServer.getReceiveAddress(), userEmail.getSyncCycleDay());
            for (EmailInfo emailInfo : emailInfoList) {
                if (!exsitsMessageIdList.contains(emailInfo.getMessageId()) && emailInfo.getMessageId() != null) {
                    //绑定客户或供应商 --暂无
                    EmInbox emInbox = BeanUtil.copyProperties(emailInfo, EmInbox.class);
                    String cusId = cusEmailIdMap.get(emailInfo.getOppositeEmail());
                    if (cusId != null) {
                        emInbox.setOppositeId(cusId);
                        emInbox.setOppositeType("CUST");
                    } else {
                        String supId = supEmailIdMap.get(emailInfo.getOppositeEmail());
                        if (supId != null) {
                            emInbox.setOppositeId(supId);
                            emInbox.setOppositeType("SUP");
                        }
                    }

                    emInbox.setId(SNOWFLAKE.nextIdStr());
                    emInbox.setOwnerId(userEmail.getUserId());
                    emInbox.setOwnerEmail(userEmail.getEmail());
                    //写入数据
                    inboxRepository.save(emInbox);
                    if (emailInfo.getAttachmentList() != null) {
                        for (MyEmailAttachment myEmailAttachment : emailInfo.getAttachmentList()) {
                            EmInboxAttachment emInboxAttachment = BeanUtil.copyProperties(myEmailAttachment, EmInboxAttachment.class);
                            emInboxAttachment.setInboxId(emInbox.getId());
                            inboxAttachmentRepository.save(emInboxAttachment);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //同步完成
    }

    public void syncAllInboxByEmailAddress(SysUser sysUser) {
        System.out.println("正在同步收件箱：" + sysUser.getRealname());
        List<SysUserEmail> userEmailList = userEmailRepository.listByEntity(new SysUserEmail() {{
            setUserId(sysUser.getId());
        }});
        List<String> exsitsMessageIdList = inboxRepository.listObjs(new QueryWrapper<EmInbox>().select("DISTINCT message_id"), o -> (String) o);
        //获取所有客户和供应商
        Map<String, String> supEmailIdMap = supContactsEmailRepository.listWithSupId().stream().collect(Collectors.toMap(SupContactsEmail::getEmailAddress, SupContactsEmail::getSupplierId));
        Map<String, String> cusEmailIdMap = cuContactsEmailRepository.listWithCusId().stream().collect(Collectors.toMap(CuContactsEmail::getEmailAddress, CuContactsEmail::getCustomerId));

        for (SysUserEmail userEmail : userEmailList) {
            try {
                //获取邮箱收件服务器
                SysEmailServer emailServer = this.getServerByAddress(userEmail.getEmail());
                //获取当前邮箱所有收件箱
                List<EmailInfo> emailInfoList = omEmailHandle.getPopSSLInBoxByTime(userEmail.getEmail(), userEmail.getEmailPassword(), emailRootPath, emailServer.getReceiveAddress(), userEmail.getSyncCycleDay());
                for (EmailInfo emailInfo : emailInfoList) {
                    if (!exsitsMessageIdList.contains(emailInfo.getMessageId()) && emailInfo.getMessageId() != null) {
                        //绑定客户或供应商 --暂无
                        EmInbox emInbox = BeanUtil.copyProperties(emailInfo, EmInbox.class);
                        String cusId = cusEmailIdMap.get(emailInfo.getOppositeEmail());
                        if (cusId != null) {
                            emInbox.setOppositeId(cusId);
                            emInbox.setOppositeType("CUST");
                        } else {
                            String supId = supEmailIdMap.get(emailInfo.getOppositeEmail());
                            if (supId != null) {
                                emInbox.setOppositeId(supId);
                                emInbox.setOppositeType("SUP");
                            }
                        }

                        emInbox.setId(SNOWFLAKE.nextIdStr());
                        emInbox.setOwnerId(userEmail.getUserId());
                        emInbox.setOwnerEmail(userEmail.getEmail());
                        //写入数据
                        inboxRepository.save(emInbox);
                        if (emailInfo.getAttachmentList() != null) {
                            for (MyEmailAttachment myEmailAttachment : emailInfo.getAttachmentList()) {
                                EmInboxAttachment emInboxAttachment = BeanUtil.copyProperties(myEmailAttachment, EmInboxAttachment.class);
                                emInboxAttachment.setInboxId(emInbox.getId());
                                inboxAttachmentRepository.save(emInboxAttachment);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Resource
    private SysEmailServerRepository emailServerRepository;

    private SysEmailServer getServerByAddress(String address) {
        Pattern p = Pattern.compile("^\\w+@(\\w+.[a-z]+)$");
        Matcher m = p.matcher(address);
        while (m.find()) {
            String result = String.valueOf(m.group(1));
            SysEmailServer emailDomain = emailServerRepository.getOne(new QueryWrapper<SysEmailServer>().eq("email_domain", result));
            return emailDomain;
        }
        return null;
    }

}
