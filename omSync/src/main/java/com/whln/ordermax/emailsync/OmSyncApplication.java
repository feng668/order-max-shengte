package com.whln.ordermax.emailsync;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableScheduling
@EnableTransactionManagement
@SpringBootApplication
@ComponentScan("com.whln.ordermax")
@MapperScan("com.whln.ordermax.data.mapper")
@EntityScan("com.whln.ordermax.data.domain")
public class OmSyncApplication {

    public static void main(String[] args) {
        SpringApplication.run(OmSyncApplication.class, args);
    }


}
