package com.whln.ordermax.emailsync.sche;

import com.whln.ordermax.data.domain.SysUserEmail;
import com.whln.ordermax.emailsync.config.ScheduleConfig;
import com.whln.ordermax.emailsync.sche.base.BaseTask;
import com.whln.ordermax.emailsync.service.InboxService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ScheduledFuture;



@Component
@Slf4j
public class EmailSyncTask extends BaseTask {

    @Resource
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Resource
    private InboxService inboxService;

    public void start(SysUserEmail userEmail) {
        ScheduledFuture scheduledFuture = threadPoolTaskScheduler.scheduleAtFixedRate(() -> {
            inboxService.sync(userEmail);
        }, userEmail.getSyncInterval());
        ScheduleConfig.cache.put(userEmail.getId(), scheduledFuture);
    }

}
