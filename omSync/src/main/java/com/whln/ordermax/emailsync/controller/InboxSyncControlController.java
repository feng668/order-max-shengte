package com.whln.ordermax.emailsync.controller;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SysUserEmail;
import com.whln.ordermax.emailsync.service.InboxSyncControlService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/inboxSync")
public class InboxSyncControlController {

    @Resource
    private InboxSyncControlService inboxSyncControlService;

    @PostMapping("/start")
    public Result start(@RequestBody SysUserEmail userEmail){
        return inboxSyncControlService.start(userEmail);
    }

    @PostMapping("/stop")
    public Result stop(@RequestBody SysUserEmail userEmail){
        return inboxSyncControlService.stop(userEmail);
    }
}
