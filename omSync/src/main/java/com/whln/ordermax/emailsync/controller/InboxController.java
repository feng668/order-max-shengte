package com.whln.ordermax.emailsync.controller;

import com.whln.ordermax.data.domain.SysUser;
import com.whln.ordermax.emailsync.service.InboxService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/inbox")
public class InboxController {

    @Resource
    private InboxService inboxService;

    @PostMapping("/syncAllByEmail")
    public void syncAllInboxByEmailAddress(@RequestBody SysUser sysUser){
        inboxService.syncAllInboxByEmailAddress(sysUser);
    }

}
