package com.whln.ordermax.emailsync.service;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SysUserEmail;
import com.whln.ordermax.emailsync.sche.EmailSyncTask;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class InboxSyncControlService {

    @Resource
    private EmailSyncTask emailSyncTask;

    public Result start(SysUserEmail userEmail) {
        System.out.println("开始同步邮箱："+userEmail.getEmail());
        try {
            emailSyncTask.start(userEmail);
        }catch (Exception e){
            return Result.error("201","邮箱同步服务异常");
        }
        return Result.success();
    }

    public Result stop(SysUserEmail userEmail) {
        System.out.println("关闭邮箱同步："+userEmail.getEmail());
        try {
            emailSyncTask.stop(userEmail.getId());
        }catch (Exception e){
            return Result.error("201","邮箱同步服务异常");
        }
        return Result.success();
    }
}
