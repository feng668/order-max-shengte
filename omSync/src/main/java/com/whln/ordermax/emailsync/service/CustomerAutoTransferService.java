package com.whln.ordermax.emailsync.service;

import com.whln.ordermax.data.domain.CuCustomerTransferRule;
import com.whln.ordermax.data.repository.CuCustomerRepository;
import com.whln.ordermax.data.repository.CuCustomerTransferRuleRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CustomerAutoTransferService {


    @Resource
    private CuCustomerTransferRuleRepository customerTransferRuleRepository;

    @Resource
    private CuCustomerRepository customerRepository;


    public void sync() {
        List<CuCustomerTransferRule> customerTransferRuleList = customerTransferRuleRepository.list();
        for (CuCustomerTransferRule customerTransferRule : customerTransferRuleList) {
            Integer category = customerTransferRule.getCategory();
            Integer status = customerTransferRule.getStatus();
            Long notUpDay = customerTransferRule.getNotUpDay();
            List<String> customerIdList = customerRepository.listIdByNotConclude(category, status, notUpDay);
            //移入公海
            customerRepository.transferPubByIdList(customerIdList);
        }
    }

}
