package com.whln.ordermax.signhandler;

import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.domain.entity.EmInbox;
import com.whln.ordermax.domain.param.EmailFlagParam;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
/**
 * @author liurun
 */
@Component
public class ImportantHandler implements InitializingBean,EmailHandler {
    @Override
    public void afterPropertiesSet() throws Exception {
        SignHandlerFactory.register(this);
    }

    @Override
    public EmInbox handler(EmInbox inboxOne, EmailFlagParam param) {
        if (ObjectUtil.isNotEmpty(param.getIsImportant())&&ObjectUtil.isNotEmpty(inboxOne)){
            if (ObjectUtil.equals(inboxOne.getIsImportant(),0)) {
                inboxOne.setIsImportant(YesOrNo.YES.getState());
            }else {
                inboxOne.setIsImportant(YesOrNo.NO.getState());
            }
        }
        return inboxOne;
    }
}
