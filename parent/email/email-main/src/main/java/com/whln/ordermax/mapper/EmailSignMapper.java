package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whln.ordermax.domain.entity.EmailSign;

/**
 * @author liurun
 * @date 2023-02-21 18:46
 */
public interface EmailSignMapper extends BaseMapper<EmailSign> {
}
