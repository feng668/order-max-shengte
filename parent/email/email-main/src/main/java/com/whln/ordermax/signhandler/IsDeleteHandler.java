package com.whln.ordermax.signhandler;

import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.domain.entity.EmInbox;
import com.whln.ordermax.domain.param.EmailFlagParam;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * @author liurun
 */
@Component
public class IsDeleteHandler implements InitializingBean,EmailHandler {
    @Override
    public void afterPropertiesSet() throws Exception {
        SignHandlerFactory.register(this);
    }

    @Override
    public EmInbox handler(EmInbox inboxOne, EmailFlagParam param) {
        if (ObjectUtil.isNotEmpty(param.getIsDelete())&&ObjectUtil.isNotEmpty(inboxOne)) {
            if (ObjectUtil.equals(inboxOne.getIsDelete(),0)) {
                inboxOne.setIsDelete(YesOrNo.YES.getState());
            } else {
                inboxOne.setIsDelete(YesOrNo.NO.getState());
            }
        }
        return inboxOne;
    }
}
