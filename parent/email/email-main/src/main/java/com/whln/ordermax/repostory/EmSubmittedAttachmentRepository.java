package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.EmSubmittedAttachment;
import com.whln.ordermax.mapper.EmSubmittedAttachmentMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class EmSubmittedAttachmentRepository extends ServiceImpl<EmSubmittedAttachmentMapper, EmSubmittedAttachment>{

    public IPage<EmSubmittedAttachment> pageByEntity(EmSubmittedAttachment entity, Page<EmSubmittedAttachment> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<EmSubmittedAttachment> listByEntity(EmSubmittedAttachment entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<EmSubmittedAttachment> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




