package com.whln.ordermax.email.entity;

import cn.hutool.json.JSONArray;

import java.time.LocalDateTime;
import java.util.List;

public class EmailInfo {
    /*发送人姓名*/
    private String oppositeSendName;
    /*对方邮箱*/
    private String oppositeEmail;
    /*主题*/
    private String theme;
    /*正文*/
    private String bodyText;
    /*正文路径*/
    private String bodyPath;
    /*接收时间*/
    private LocalDateTime receiveTime;

    private List<MyEmailAttachment> attachmentList;
    /*邮件消息ID*/
    private String messageId;
    /*接收人数组*/
    private JSONArray recivedArr;

    public String getOppositeSendName() {
        return oppositeSendName;
    }

    public void setOppositeSendName(String oppositeSendName) {
        this.oppositeSendName = oppositeSendName;
    }

    public String getOppositeEmail() {
        return oppositeEmail;
    }

    public void setOppositeEmail(String oppositeEmail) {
        this.oppositeEmail = oppositeEmail;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public LocalDateTime getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(LocalDateTime receiveTime) {
        this.receiveTime = receiveTime;
    }

    public List<MyEmailAttachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<MyEmailAttachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getBodyPath() {
        return bodyPath;
    }

    public void setBodyPath(String bodyPath) {
        this.bodyPath = bodyPath;
    }

    public JSONArray getRecivedArr() {
        return recivedArr;
    }

    public void setRecivedArr(JSONArray recivedArr) {
        this.recivedArr = recivedArr;
    }
}
