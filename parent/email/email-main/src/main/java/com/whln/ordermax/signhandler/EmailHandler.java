package com.whln.ordermax.signhandler;


import com.whln.ordermax.domain.entity.EmInbox;
import com.whln.ordermax.domain.param.EmailFlagParam;

/**
 * 处理邮件标记的责任链父接口
 * @author liurun
 */
public interface EmailHandler {
    /**
     * 处理邮件标记等
     * @param emInbox
     * @param param
     * @return
     */
    EmInbox handler(EmInbox emInbox, EmailFlagParam param);

}
