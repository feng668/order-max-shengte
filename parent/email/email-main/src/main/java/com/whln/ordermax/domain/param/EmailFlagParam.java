package com.whln.ordermax.domain.param;

import cn.hutool.core.util.ObjectUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("邮件标记参数")
@Data
public class EmailFlagParam {
    @ApiModelProperty("邮件id")
    private String id;
    @ApiModelProperty(value = "是否已读，0未读，1已读,不对这个状态做操作时不传", example = "1")
    private Integer readeStatus;
    @ApiModelProperty(value = "星标，0未标，1已标，不对这个状态做操作时不传", example = "1")
    private Integer isFlag;
    @ApiModelProperty(value = "是否删除，0未删，1已删，不对这个状态做操作时不传", example = "1")
    private Integer isDelete;
    @ApiModelProperty(value = "是否重要，0 默认，1重要，不对这个状态做操作时不传", example = "1")
    private Integer isImportant;

    public boolean paramCheck() {
        return ObjectUtil.isEmpty(id) ||
                (ObjectUtil.isEmpty(readeStatus) && ObjectUtil.isEmpty(isDelete) && ObjectUtil.isEmpty(isFlag) && ObjectUtil.isEmpty(isImportant));
    }
}
