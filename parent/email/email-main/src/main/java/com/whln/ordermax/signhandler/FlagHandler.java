package com.whln.ordermax.signhandler;

import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.domain.entity.EmInbox;
import com.whln.ordermax.domain.param.EmailFlagParam;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * @author liurun
 */
@Component
public class FlagHandler implements InitializingBean,EmailHandler {
    @Override
    public void afterPropertiesSet() throws Exception {
        SignHandlerFactory.register(this);
    }

    @Override
    public EmInbox handler(EmInbox inboxOne, EmailFlagParam param) {
        if (ObjectUtil.isNotEmpty(param.getIsFlag())&&ObjectUtil.isNotEmpty(inboxOne)) {
            if (inboxOne.getIsFlag() == 0) {
                inboxOne.setIsFlag(YesOrNo.YES.getState());
            } else {
                inboxOne.setIsFlag(YesOrNo.NO.getState());
            }
        }
        return inboxOne;
    }
}
