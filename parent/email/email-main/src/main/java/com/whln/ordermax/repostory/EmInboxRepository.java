package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.EmInbox;
import com.whln.ordermax.mapper.EmInboxMapper;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

/**
 *
 */
@Repository
public class EmInboxRepository extends ServiceImpl<EmInboxMapper, EmInbox>{

    public IPage<EmInbox> pageByEntity(EmInbox entity, Page<EmInbox> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<EmInbox> listByEntity(EmInbox entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<EmInbox> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    /**
     * 修改邮箱绑定关系
     * @param oppositeId 发送者所属单位id
     * @param emailMapList 邮件信息map的集合 包含发送者联系人id和邮箱地址
     * @param oppositeType 发送者类别 客户 供应商
     * @return
     */
    public Boolean updateInboxBind(String oppositeId, List<HashMap<String, String>> emailMapList,String oppositeType) {
        if (emailMapList!=null&&emailMapList.size()>0){
            return baseMapper.updateInboxBind(oppositeId,emailMapList,oppositeType);
        }
        return false;
    }

    public List<EmInbox> classifyByTypeCustomer(EmInbox entity) {
        return baseMapper.classifyByTypeCustomer(entity);
    }

    public EmInbox select(LambdaQueryWrapper<EmInbox> emInboxLambdaQueryWrapper) {
        return baseMapper.selectOne(emInboxLambdaQueryWrapper);
    }
}




