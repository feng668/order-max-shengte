package com.whln.ordermax.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whln.ordermax.domain.entity.EmailSign;
import com.whln.ordermax.domain.entity.SysUser;
import com.whln.ordermax.domain.param.EmailSignParam;
import com.whln.ordermax.domain.vo.EmailSignVO;
import com.whln.ordermax.domain.vo.SignResult;
import com.whln.ordermax.mapper.EmailSignMapper;
import com.whln.ordermax.repostory.SysUserRepository;
import com.whln.ordermax.service.EmailSignService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-02-21 18:48
 */
@AllArgsConstructor
@Component
public class EmailSignServiceImpl implements EmailSignService {
    private final EmailSignMapper emailSignMapper;
    private final SysUserRepository sysUserRepository;

    @Override
    public List<EmailSignVO> list(EmailSignParam param) {
        LambdaQueryWrapper<EmailSign> wrapper = new LambdaQueryWrapper<EmailSign>()
                .eq(EmailSign::getRelationId, param.getRelationId());
        List<EmailSign> emailSigns = emailSignMapper.selectList(wrapper);
        SysUser sysUser = sysUserRepository.getUserByUserId(param.getRelationId());
        Map<String, Object> map = BeanUtil.beanToMap(sysUser);
       /* emailSigns.forEach(item -> {
            map.forEach((k, v) -> {
                String s1 = "%" + k + "%";
                String s = item.getSign().replaceAll(s1, v.toString());
                item.setSign(s);
            });
        });*/
        List<EmailSignVO> emailSignVOList = BeanUtil.copyToList(emailSigns, EmailSignVO.class);
        List<EmailSignVO> collect = emailSignVOList.stream().peek(item -> {
            String sign = item.getSign().trim();
            ArrayList<SignResult> objects = new ArrayList<>();
            String[] split = sign.split(",");
            List<String> strings = Arrays.asList(split);

            for (String s : strings) {
                Object o = map.get(s);
                if (ObjectUtil.isNotEmpty(o)) {
                    objects.add( new SignResult(s, o.toString()));
                }
            }
            item.setResult(objects);
        }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public EmailSignVO get(EmailSignParam param) {
        EmailSign emailSign = emailSignMapper.selectById(param.getId());
        SysUser sysUser = sysUserRepository.getUserByUserId(param.getRelationId());
        Map<String, Object> map = BeanUtil.beanToMap(sysUser);
        String sign = emailSign.getSign();
        map.forEach((k, v) -> {
            String s = sign.replaceAll(k, v.toString());
            emailSign.setSign(s);
        });

        return BeanUtil.copyProperties(emailSign, EmailSignVO.class);
    }


    @Override
    public void deleteSign(EmailSignParam param) {
        emailSignMapper.deleteById(param.getId());
    }

    @Override
    public void updateSign(EmailSignVO param) {
        emailSignMapper.updateById(BeanUtil.copyProperties(param, EmailSign.class));
    }

    @Override
    public void add(EmailSignVO param) {
        if (ObjectUtil.isEmpty(param.getId())) {
        int insert = emailSignMapper.insert(BeanUtil.copyProperties(param, EmailSign.class));

        }else {
            emailSignMapper.updateById(BeanUtil.copyProperties(param, EmailSign.class));
        }

    }
}
