package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.EmInboxAttachment;
import com.whln.ordermax.mapper.EmInboxAttachmentMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class EmInboxAttachmentRepository extends ServiceImpl<EmInboxAttachmentMapper, EmInboxAttachment>{

    public IPage<EmInboxAttachment> pageByEntity(EmInboxAttachment entity, Page<EmInboxAttachment> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<EmInboxAttachment> listByEntity(EmInboxAttachment entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<EmInboxAttachment> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




