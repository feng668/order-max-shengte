package com.whln.ordermax.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

/**
* 已发邮件
* @TableName em_submitted
*/
@ApiModel("已发邮件")
@TableName("em_submitted")
@Data
public class EmSubmitted {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;

    @ApiModelProperty("是否删除 0未删除 1已删除")
    private String isDelete;
    /**
    * 我方邮箱
    */
    @ApiModelProperty("我方邮箱")
    private String ownerEmail;
    /**
    * 主题
    */
    @ApiModelProperty("主题")
    private String theme;
    /**
    * 正文
    */
    @ApiModelProperty("正文")
    @NotBlank(message = "消息内容不能为空")
    private String bodyText;
    @ApiModelProperty("正文路径")
    private String bodyPath;
    /**
    * 业务类型
    */
    @ApiModelProperty("业务类型")
    private String oppositeType;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ApiModelProperty("状态 1 草稿")
    @JsonIgnore
    private Integer status;
    @ApiModelProperty("死否发送成功（1发送成功，2发送失败）")
    private Integer isSuccuss;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("发送时间")
    private LocalDateTime sendTime;
    @ApiModelProperty("对方邮箱")
    private String oppositeEmail;

    /*附加属性*/
    @TableField(exist = false)
    @ApiModelProperty("群发的收件人邮箱")
    List<EmSubmittedEmails> submittedEmailsList;

    @TableField(exist = false)
    @ApiModelProperty("附件")
    List<EmSubmittedAttachment> attachmentList;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
