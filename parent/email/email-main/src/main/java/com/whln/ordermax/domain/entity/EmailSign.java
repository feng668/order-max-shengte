package com.whln.ordermax.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-02-21 18:45
 */
@Data
@TableName("email_sign")
public class EmailSign {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String relationId;
    private String sign;
    private String title;
}
