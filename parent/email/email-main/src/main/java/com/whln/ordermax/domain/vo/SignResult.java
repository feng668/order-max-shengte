package com.whln.ordermax.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liurun
 * @date 2023-02-22 20:09
 */
@AllArgsConstructor
@Data
public class SignResult implements Serializable {
    private static final long serialVersionUID = 4380715416180027923L;
    private String label;
    private String value;
}
