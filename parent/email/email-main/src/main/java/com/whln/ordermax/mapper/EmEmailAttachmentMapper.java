package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.EmEmailAttachment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EmEmailAttachment
 */
public interface EmEmailAttachmentMapper extends BaseMapper<EmEmailAttachment> {

    List<EmEmailAttachment> listByEntity(@Param("entity")EmEmailAttachment entity);

    IPage<EmEmailAttachment> listByEntity(@Param("entity")EmEmailAttachment entity, Page<EmEmailAttachment> page);

    Integer insertBatch(@Param("entitylist")List<EmEmailAttachment> entitylist);

}




