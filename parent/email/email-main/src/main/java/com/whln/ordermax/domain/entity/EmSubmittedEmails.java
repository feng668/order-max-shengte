package com.whln.ordermax.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
* 已发邮件邮箱
* @TableName em_submitted_emails
*/
@ApiModel("已发邮件邮箱")
@TableName("em_submitted_emails")
@Data
public class EmSubmittedEmails {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 已发邮件id
    */
    @ApiModelProperty("已发邮件id")
    private String submittedId;
    /**
    * 对方id
    */
    @ApiModelProperty("对方id")
    private String oppositeId;
    /**
    * 对方邮箱
    */
    @ApiModelProperty("对方邮箱")
    private String oppositeEmail;
    @ApiModelProperty("发送类型（SEND发送人,CC转抄人,BCC密送人）")
    private String sendType;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
