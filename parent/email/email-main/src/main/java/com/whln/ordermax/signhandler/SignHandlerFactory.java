package com.whln.ordermax.signhandler;

import com.whln.ordermax.domain.entity.EmInbox;
import com.whln.ordermax.domain.param.EmailFlagParam;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liurun
 */
@Component
public class SignHandlerFactory {
    private static final List<EmailHandler> EMAIL_HANDLERS = new ArrayList<>();

    public static void register(EmailHandler handler) {
        EMAIL_HANDLERS.add(handler);
    }

    public void handler(EmInbox emInbox, EmailFlagParam param) {
        for (EmailHandler handler : EMAIL_HANDLERS) {
            handler.handler(emInbox, param);
        }
    }
}
