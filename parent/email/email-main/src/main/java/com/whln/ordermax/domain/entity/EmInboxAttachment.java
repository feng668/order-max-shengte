package com.whln.ordermax.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
* 收件箱附件
* @TableName em_inbox_attachment
*/
@ApiModel("收件箱附件")
@TableName("em_inbox_attachment")
@Data
public class EmInboxAttachment {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 收件箱id
    */
    @ApiModelProperty("收件箱id")
    private String inboxId;
    /**
    * 附件名称
    */
    @ApiModelProperty("附件名称")
    private String attachmentName;
    /**
    * 文件类型
    */
    @ApiModelProperty("文件类型")
    private String fileType;
    /**
    * 路径
    */
    @ApiModelProperty("路径")
    private String filePath;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;private String ownerIds;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
