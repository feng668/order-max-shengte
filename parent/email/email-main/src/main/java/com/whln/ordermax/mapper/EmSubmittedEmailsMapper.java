package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.EmSubmittedEmails;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EmSubmittedEmails
 */
public interface EmSubmittedEmailsMapper extends BaseMapper<EmSubmittedEmails> {

    List<EmSubmittedEmails> listByEntity(@Param("entity")EmSubmittedEmails entity);

    IPage<EmSubmittedEmails> listByEntity(@Param("entity")EmSubmittedEmails entity, Page<EmSubmittedEmails> page);

    Integer insertBatch(@Param("entitylist")List<EmSubmittedEmails> entitylist);

}




