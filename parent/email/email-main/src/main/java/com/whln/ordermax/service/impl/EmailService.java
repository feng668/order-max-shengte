package com.whln.ordermax.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.domain.entity.*;
import com.whln.ordermax.email.OmEmailHandle;
import com.whln.ordermax.mapper.EmSubmittedAttachmentMapper;
import com.whln.ordermax.repostory.*;
import com.whln.ordermax.signhandler.SignHandlerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 收件箱业务处理
 */
@Service
public class EmailService {
    @Resource
    private EmInboxRepository inboxRepository;

    @Resource
    private EmSubmittedRepository submittedRepository;

    @Resource
    private EmSubmittedEmailsRepository submittedEmailsRepository;

    @Resource
    private SysUserRepository userRepository;

    @Resource
    private OmEmailHandle emailHandle;

    @Resource
    private EmSubmittedAttachmentRepository attachmentRepository;

    @Resource
    private EmInboxAttachmentRepository inboxAttachmentRepository;

    @Resource
    private SysUserEmailRepository userEmailRepository;

    @Resource
    private SysEmailServerRepository emailServerRepository;

    @Resource
    private SysService sysService;

    @Value("${filestore.static}")
    private String staticPath;
    @Value("${filestore.upload}")
    private String uploadPath;

    @Resource
    private SignHandlerFactory handlerFactory;

    @Resource
    private EmSubmittedAttachmentMapper emSubmittedAttachmentMapper;

   /* public Result<Integer> countNotRead() {
        String userId = AuthService.getUserIdFromAuthentication();
        QueryWrapper<EmInbox> wrapper = new QueryWrapper<>();
        wrapper.eq("is_read", 0);
        wrapper.eq("is_delete", 0);
        //让admin看到所有人的收件箱
        if (!ShiroManager.getSubject().hasRole("webadmin")) {
            wrapper.eq("owner_id", userId);
        }
        Integer notReadCount = inboxRepository.count(wrapper);
        return Result.success(notReadCount);
    }*/

   /* @Resource
    CuCustomerRepository cuCustomerRepository;

    public Result<List<Map<String, Object>>> classifyInboxByTypeCustomer() {
        String userId = AuthService.getUserIdFromAuthentication();
        EmInbox entity = new EmInbox();
        GetCustomerInfoQuery getCustomerInfo = new GetCustomerInfoQuery();
        if (!ShiroManager.getSubject().hasRole("webadmin")) {
            entity.setOwnerId(userId);
            getCustomerInfo.setOwnerId(userId);
        }
        List<EmailSendTypeVO> customerInfo = cuCustomerRepository.getCustomerInfo(getCustomerInfo);
        Map<String, EmailSendTypeVO> collect = customerInfo.stream().collect(Collectors.toMap(EmailSendTypeVO::getId, Function.identity(), (x, y) -> {
            x.setEmailAddress(x.getEmailAddress() + "," + y.getEmailAddress());
            return x;
        }));
        Map<String, List<EmailSendTypeVO>> collect1 = collect.values().stream().collect(Collectors.groupingBy(EmailSendTypeVO::getType));
        List<Map<String, Object>> result = new ArrayList() {{
            add(new HashMap<String, Object>() {{
                put("type", "CUS");
                put("name", "客户");
                put("chirds", collect1.get("CUS"));
            }});
            add(new HashMap<String, Object>() {{
                put("type", "SUP");
                put("name", "供应商");
                put("chirds", collect1.get("SUP"));
            }});
        }};
        return Result.success(result);
    }

    public Result<IPage<EmInbox>> pageInbox(EmInbox entity, Page<EmInbox> page) {
        try {
            ShiroManager.getSubject().checkRole("webadmin");
        } catch (AuthorizationException e) {
            entity.setOwnerId(ShiroManager.getUserId());
        }
        if (ObjectUtil.isNotEmpty(entity.getEmailAddress())) {
            String[] split = entity.getEmailAddress().split(",");
            entity.setEmails(Arrays.asList(split));
        }
        return Result.success(inboxRepository.pageByEntity(entity, page));
    }
*/
    public Result readInbox(String inboxId) {
        inboxRepository.update(new UpdateWrapper<EmInbox>().set("is_read", 1).eq("id", inboxId));
        return Result.success();
    }

 /*   public Result<IPage<EmSubmitted>> pageSubmitted(EmSubmitted entity, Page<EmSubmitted> page) {
        String userId = AuthService.getUserIdFromAuthentication();
        entity.setOwnerId(userId);
        return Result.success(submittedRepository.pageByEntity(entity, page));
    }*/

    public Result<Map<String, List<EmSubmittedEmails>>> submittedEmails(String submittedId) {
        List<EmSubmittedEmails> submittedEmailsList = submittedEmailsRepository.listByEntity(new EmSubmittedEmails() {{
            setSubmittedId(submittedId);
        }});
        Map<String, List<EmSubmittedEmails>> sendTypeMap = submittedEmailsList.stream().collect(Collectors.groupingBy(EmSubmittedEmails::getSendType));
        return Result.success(sendTypeMap);
    }


   /* @Transactional(rollbackFor = Exception.class)
    public Result sendAndSave(EmSubmitted entity) {
        String userId = AuthService.getUserIdFromAuthentication();
        LocalDateTime sendTime = LocalDateTime.now();
        SysUser my = userRepository.getById(userId);
        String emailPassword;
        if (entity.getOwnerEmail() == null) {
            SysUserEmail userEmail = userEmailRepository.getOne(new QueryWrapper<SysUserEmail>().eq("user_id", userId).eq("is_default", 1));
            if (ObjectUtil.isEmpty(userEmail)) {
                return Result.error("201", "未设定默认邮箱");
            }
            entity.setOwnerEmail(userEmail.getEmail());
            emailPassword = userEmail.getEmailPassword();
        } else {
            SysUserEmail userEmail = userEmailRepository.getOne(new QueryWrapper<SysUserEmail>().eq("user_id", userId).eq("email", entity.getOwnerEmail()));
            if (ObjectUtil.isEmpty(userEmail)) {
                return Result.error("201", "邮箱不存在");
            }
            emailPassword = userEmail.getEmailPassword();
        }
        List<String> sendEmailList = new ArrayList<>();
        List<String> ccEmailList = new ArrayList<>();
        List<String> bccEmailList = new ArrayList<>();
        String submittedId = sysService.nextId();
        entity.setId(submittedId);
        entity.setSubmittedEmailsList(
                entity.getSubmittedEmailsList().stream().filter(item -> ObjectUtil.isNotEmpty(item.getOppositeEmail()))
                        .map(
                                se -> {
                                    se.setSubmittedId(submittedId);
                                    switch (se.getSendType()) {
                                        case "SEND":
                                            sendEmailList.add(se.getOppositeEmail());
                                            break;
                                        case "CC":
                                            ccEmailList.add(se.getOppositeEmail());
                                            break;
                                        case "BCC":
                                            bccEmailList.add(se.getOppositeEmail());
                                            break;
                                        default:
                                            sendEmailList.add(se.getOppositeEmail());
                                            break;
                                    }
                                    return se;
                                }
                        ).collect(Collectors.toList())
        );
        String emailDomain = emailHandle.getEmailDomainByAddress(entity.getOwnerEmail());
        if (emailDomain == null) {
            return Result.error("201", "此邮箱不符合要求");
        }
        SysEmailServer emailServer = emailServerRepository.getOne(new QueryWrapper<SysEmailServer>().eq("email_domain", emailDomain));
        List<EmSubmittedAttachment> attachmentList = entity.getAttachmentList();
        List<EmailAttachment> emailAttachmentList = null;
        if (ObjectUtil.isNotEmpty(attachmentList)) {
            emailAttachmentList = attachmentList
                    .stream().map(a -> {
                        EmailAttachment attachment = BeanUtil.copyProperties(a, EmailAttachment.class);
                        attachment.setPath(uploadPath + a.getFilePath());
                        return attachment;
                    }).collect(Collectors.toList());
        }
        Integer isSuccuss = 1;
        try {
            emailHandle.sendSLLMail(entity.getOwnerEmail(),
                    emailPassword,
                    my.getRealname(),
                    sendEmailList,
                    ccEmailList,
                    bccEmailList,
                    entity.getTheme(),
                    entity.getBodyText(),
                    emailAttachmentList,
                    emailServer.getSendAddress());
        } catch (EmailException e) {
            e.printStackTrace();
            isSuccuss = 0;
        }
        entity.setIsSuccuss(isSuccuss);
        entity.setOwnerId(userId);
        entity.setSendTime(sendTime);
        boolean b = submittedRepository.save(entity);
        if (CollectionUtil.isNotEmpty(entity.getSubmittedEmailsList())) {
            submittedEmailsRepository.saveOrUpdateBatch(entity.getSubmittedEmailsList());
        }
        if (CollectionUtil.isNotEmpty(entity.getAttachmentList())) {
            attachmentRepository.saveOrUpdateBatch(entity.getAttachmentList());
        }
        return b && isSuccuss == 1 ? Result.success() : Result.error("201", "发送失败");
    }

    public Result deleteInbox(String id) {
        String userId = AuthService.getUserIdFromAuthentication();
        LambdaUpdateWrapper<EmInbox> lambdaQueryWrapper = new UpdateWrapper<EmInbox>().lambda()
                .set(EmInbox::getIsDelete, 1)
                .eq(EmInbox::getId, id);
        if (!ShiroManager.getSubject().hasRole("webadmin")) {
            lambdaQueryWrapper.eq(EmInbox::getOwnerId, userId);
        }
        boolean b = inboxRepository.update(lambdaQueryWrapper);
        return b ? Result.success() : Result.error("201", "删除失败");
    }


    @Transactional(rollbackFor = Exception.class)
    public Result deleteRecycle(String id) {
        String userId = AuthService.getUserIdFromAuthentication();
        LambdaUpdateWrapper<EmInbox> lambdaQueryWrapper = new UpdateWrapper<EmInbox>().lambda()
                .eq(EmInbox::getId, id);
        if (!ShiroManager.getSubject().hasRole("webadmin")) {
            lambdaQueryWrapper.eq(EmInbox::getOwnerId, userId);
        }
        boolean b = inboxRepository.remove(lambdaQueryWrapper);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result batchDeleteInbox(List<String> ids) {
        String userId = AuthService.getUserIdFromAuthentication();
        boolean b = inboxRepository.update(
                new UpdateWrapper<EmInbox>().lambda()
                        .set(EmInbox::getIsDelete, 1)
                        .in(EmInbox::getId, ids)
                        .eq(EmInbox::getOwnerId, userId)
        );
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result batchDeleteRecycle(List<String> ids) {
        String userId = AuthService.getUserIdFromAuthentication();
        boolean b = inboxRepository.remove(
                new QueryWrapper<EmInbox>().lambda()
                        .in(EmInbox::getId, ids)
                        .eq(EmInbox::getOwnerId, userId)
        );
        return b ? Result.success() : Result.error("201", "删除失败");
    }


    public Result flagInbox(EmailFlagParam param) {
        if (param.paramCheck()) {
            return Result.error("201", "参数错误");
        }
        String userId = AuthService.getUserIdFromAuthentication();
        LambdaQueryWrapper<EmInbox> lambdaQueryWrapper = new QueryWrapper<EmInbox>().lambda()
                .eq(EmInbox::getId, param.getId());
        if (!ShiroManager.hasRole("webadmin")) {
            lambdaQueryWrapper.eq(EmInbox::getOwnerId, userId);
        }
        EmInbox inboxOne = inboxRepository.getOne(lambdaQueryWrapper);
        handlerFactory.handler(inboxOne, param);
        LambdaUpdateWrapper<EmInbox> wrapper = new UpdateWrapper<EmInbox>().lambda()
                .set(EmInbox::getIsFlag, inboxOne.getIsFlag())
                .set(EmInbox::getIsRead, inboxOne.getIsRead())
                .set(EmInbox::getIsImportant, inboxOne.getIsImportant())
                .eq(EmInbox::getId, param.getId());
        boolean b = inboxRepository.update(wrapper);
        return b ? Result.success() : Result.error("201", "标记失败");
    }

    public Result batchFlagInbox(List<String> ids) {
        String userId = AuthService.getUserIdFromAuthentication();
        boolean b = inboxRepository.update(
                new UpdateWrapper<EmInbox>().lambda()
                        .set(EmInbox::getIsFlag, 1)
                        .in(EmInbox::getId, ids)
                        .eq(EmInbox::getOwnerId, userId)
        );
        return b ? Result.success() : Result.error("201", "删除失败");
    }
*/

    public Result listAttachmentByInboxId(String inboxId, Integer type) {
        if (ObjectUtil.equals(type, 2)) {
            List<EmSubmittedAttachment> emSubmittedAttachments = emSubmittedAttachmentMapper.selectList(new LambdaQueryWrapper<EmSubmittedAttachment>()
                    .eq(ObjectUtil.isNotEmpty(inboxId), EmSubmittedAttachment::getSubmittedId, inboxId));
            return Result.success(emSubmittedAttachments);
        }

        return Result.success(inboxAttachmentRepository.listByEntity(new EmInboxAttachment() {{
            setInboxId(inboxId);
        }}));


    }

    @Resource
    private RestTemplate restTemplate;


    public static ThreadPoolExecutor executorService = new ThreadPoolExecutor(5, 5, 60L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(1));

 /*   public Result syncAllInbox() {
        String userId = AuthService.getUserIdFromAuthentication();
        SysUser user = userRepository.getById(userId);
        executorService.execute(() ->
                restTemplate.postForLocation("http://127.0.0.1:12888/syncAllByEmail", user)
        );
        return Result.success();
    }

    public Result<Void> save(EmSubmitted entity) {
        String userId = AuthService.getUserIdFromAuthentication();
        LocalDateTime sendTime = LocalDateTime.now();
        String submittedId = sysService.nextId();

        entity.setOwnerId(userId);
        entity.setSendTime(sendTime);
        boolean b;
        if (ObjectUtil.isNotEmpty(entity.getId())) {
            b = submittedRepository.updateById(entity);
        } else {
            entity.setId(submittedId);
            b = submittedRepository.save(entity);
        }
        if (CollectionUtil.isNotEmpty(entity.getSubmittedEmailsList())) {
            List<EmSubmittedEmails> collect = entity.getSubmittedEmailsList().stream().map(item -> {
                item.setSubmittedId(submittedId);
                return item;
            }).collect(Collectors.toList());
            submittedEmailsRepository.saveOrUpdateBatch(collect);
        }
        if (CollectionUtil.isNotEmpty(entity.getAttachmentList())) {
            List<EmSubmittedAttachment> collect = entity.getAttachmentList().stream().map(item -> {
                item.setSubmittedId(submittedId);
                return item;
            }).collect(Collectors.toList());
            attachmentRepository.saveOrUpdateBatch(collect);
        }
        return Result.success();

    }*/

    public Result list(PageEntity<EmSubmitted> entity) {
        IPage<EmSubmitted> emSubmittedIPage = submittedRepository.pageByEntity(entity.getEntity(), new Page<>(entity.getCurrent(), entity.getSize()));
        List<EmSubmitted> emSubmitteds = emSubmittedIPage.getRecords();
        if (ObjectUtil.isNotEmpty(emSubmitteds)) {
            Set<String> collect = emSubmitteds.stream().map(EmSubmitted::getId).collect(Collectors.toSet());
            List<EmSubmittedAttachment> emSubmittedAttachments = attachmentRepository.list(new LambdaQueryWrapper<EmSubmittedAttachment>()
                    .in(EmSubmittedAttachment::getSubmittedId, collect));
            List<EmSubmittedEmails> emSubmittedEmails = submittedEmailsRepository.list(new LambdaQueryWrapper<EmSubmittedEmails>()
                    .in(EmSubmittedEmails::getSubmittedId, collect));

            Map<String, List<EmSubmittedAttachment>> groupAttachment = emSubmittedAttachments.stream().collect(Collectors.groupingBy(EmSubmittedAttachment::getSubmittedId));
            Map<String, List<EmSubmittedEmails>> groupSubmittedEmails = emSubmittedEmails.stream().collect(Collectors.groupingBy(EmSubmittedEmails::getSubmittedId));
            for (EmSubmitted e : emSubmitteds) {
                e.setSubmittedEmailsList(groupSubmittedEmails.get(e.getId()));
                e.setAttachmentList(groupAttachment.get(e.getId()));
            }
        }
        return Result.success(emSubmittedIPage);
    }

    public Result<EmSubmitted> get(String id) {
        EmSubmitted emSubmitted = submittedRepository.getOne(new LambdaQueryWrapper<EmSubmitted>().eq(EmSubmitted::getId, id));
        List<EmSubmittedAttachment> emSubmittedAttachments = attachmentRepository.list(new LambdaQueryWrapper<EmSubmittedAttachment>()
                .eq(EmSubmittedAttachment::getSubmittedId, emSubmitted.getId()));
        List<EmSubmittedEmails> emSubmittedEmails = submittedEmailsRepository.list(new LambdaQueryWrapper<EmSubmittedEmails>()
                .eq(EmSubmittedEmails::getSubmittedId, emSubmitted.getId()));

        emSubmitted.setSubmittedEmailsList(emSubmittedEmails);
        emSubmitted.setAttachmentList(emSubmittedAttachments);
        return Result.success(emSubmitted);
    }

    /*public Result<Void> deleteEmSubmitted(DeleteQuery param) {
        submittedRepository.removeById(param.getId());
        return Result.success();
    }

    public void trashCanToRecive(EmBoxParam param) {
        submittedRepository.update(new LambdaUpdateWrapper<EmSubmitted>()
                .eq(EmSubmitted::getId, param.getId())
                .set(EmSubmitted::getIsDelete, 0)
        );
    }*/
}