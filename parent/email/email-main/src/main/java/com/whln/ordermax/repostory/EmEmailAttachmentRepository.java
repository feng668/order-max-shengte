package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.EmEmailAttachment;
import com.whln.ordermax.mapper.EmEmailAttachmentMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class EmEmailAttachmentRepository extends ServiceImpl<EmEmailAttachmentMapper, EmEmailAttachment>{

    public IPage<EmEmailAttachment> pageByEntity(EmEmailAttachment entity, Page<EmEmailAttachment> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<EmEmailAttachment> listByEntity(EmEmailAttachment entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<EmEmailAttachment> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




