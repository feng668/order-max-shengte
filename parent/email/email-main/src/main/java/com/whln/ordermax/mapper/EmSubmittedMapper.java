package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.EmSubmitted;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EmSubmitted
 */
public interface EmSubmittedMapper extends BaseMapper<EmSubmitted> {

    List<EmSubmitted> listByEntity(@Param("entity")EmSubmitted entity);

    IPage<EmSubmitted> listByEntity(@Param("entity")EmSubmitted entity, Page<EmSubmitted> page);

    Integer insertBatch(@Param("entitylist")List<EmSubmitted> entitylist);

}




