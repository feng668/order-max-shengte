package com.whln.ordermax.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-02-21 18:51
 */
@Data
@ApiModel("邮箱签名")
public class EmailSignVO {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("签名关联的id")
    private String relationId;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("签名内容")
    private String sign;
    @ApiModelProperty("字段名和对应的值")
    List<SignResult> result;
}
