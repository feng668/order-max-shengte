package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.SysCompany;
import com.whln.ordermax.mapper.SysCompanyMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class SysCompanyRepository extends ServiceImpl<SysCompanyMapper, SysCompany>{

    public IPage<SysCompany> pageByEntity(SysCompany entity, Page<SysCompany> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SysCompany> listByEntity(SysCompany entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SysCompany> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




