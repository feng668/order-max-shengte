package com.whln.ordermax.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.whln.ordermax.domain.param.UserBindLabelParam;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-06 16:10
 */
@Data
@TableName("customer_label_relation")
public class CustomerLabelRelation {
    @TableId(type=IdType.ASSIGN_ID)
    private String id;
    /**
     * 用户id
     */
    private String userId;
    /**
     * 标签id
     */
    private String labelId;

    public static CustomerLabelRelation build(UserBindLabelParam param) {
        CustomerLabelRelation userLabelRelation = new CustomerLabelRelation();
        userLabelRelation.setLabelId(param.getLabelId());
        userLabelRelation.setUserId(param.getCustomerId());
        return userLabelRelation;
    }
}
