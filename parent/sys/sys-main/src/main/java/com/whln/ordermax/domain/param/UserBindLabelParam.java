package com.whln.ordermax.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author liurun
 * @date 2023-03-07 9:41
 */
@ApiModel("用户绑定标签参数")
@Data
public class UserBindLabelParam implements Serializable {
    private static final long serialVersionUID = -2750343808837919941L;
    @ApiModelProperty("用户id")
    @NotBlank(message = "用户id不能为空")
    private String customerId;
    @ApiModelProperty("标签id")
    private String labelId;
}
