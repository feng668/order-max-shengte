package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysRole
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<SysRole> listByEntity(@Param("entity")SysRole entity);
    IPage<SysRole> listByEntity(@Param("entity")SysRole entity, Page<SysRole> page);
    List<SysRole> listRolesByUserId(@Param("userId") String userId);
}




