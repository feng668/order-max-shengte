package com.whln.ordermax.domain.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-06 15:49
 */
@ApiModel("用户标签查询参数")
@Data
public class UserLabelQuery {
    @ApiModelProperty("标签组id")
    private String groupId;
    @ApiModelProperty("用户id")
    private String userId;
}
