package com.whln.ordermax.contoller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.domain.entity.SysRole;
import com.whln.ordermax.domain.entity.SysUser;
import com.whln.ordermax.service.impl.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Api(tags = "角色")
@RestController
@RequestMapping("/sysRole")
public class SysRoleController {

    @Resource
    private SysRoleService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result listAll(@RequestBody SysRole entity) {
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result pageAll(@RequestBody PageEntity<SysRole> pageEntity) {
        return baseService.pageAll(pageEntity.getEntity(), new Page(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody SysRole entity) {
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id) {

        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids) {

        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "配置权限")
    @PostMapping("/setPowers")
    public Result setPowers(@RequestBody() SysRole sysRole) {

        return baseService.setPowers(sysRole);
    }
    @GetMapping("/queryUserByRoleId")
    @ApiOperation("根据角色id获取有这个角色的用户")
    @ApiImplicitParam(value = "角色id roleId",example = "1")
    public Result<List<SysUser>> queryUserByRoleId(@RequestParam("roleId") String roleId){
        List<SysUser> users = baseService.queryUserByRoleId(roleId);
        return Result.success(users);
    }
}