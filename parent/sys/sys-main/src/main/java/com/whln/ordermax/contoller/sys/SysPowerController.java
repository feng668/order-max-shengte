package com.whln.ordermax.contoller.sys;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.domain.entity.SysPower;
import com.whln.ordermax.service.impl.SysPowerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Api(tags = "权限")
@RestController
@RequestMapping("/sysPower")
public class SysPowerController {

    @Resource
    private SysPowerService baseService;


    @ApiOperation(value = "查询树形数据")
    @GetMapping("/treeData")
    public Result treeData() {

        return baseService.treeData();
    }

    @ApiOperation(value = "通过角色id查询权限树形数据")
    @PostMapping("/treeDataByRoleId")
    public Result treeDataByRoleId(@RequestParam("roleId") String roleId) {

        return baseService.treeDataByRoleId(roleId);
    }

    @ApiOperation(value = "通过父级id查询字段数据")
    @PostMapping("/listFieldByParentId")
    public Result listFieldByParentId(@RequestParam("parentId") String parentId) {

        return baseService.listFieldByParentId(parentId);
    }

    @ApiOperation(value = "通过父级id查询角色的字段数据")
    @PostMapping("/listFieldByParentIdRoleId")
    public Result<List<Map>> listFieldByParentIdRoleId(@RequestParam("parentId") String parentId, @RequestParam("roleId")String roleId) {
        return baseService.listFieldByParentIdRoleId(parentId,roleId);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/saveAll")
    public Result saveAll(@RequestBody() List<SysPower> entityList){
        return baseService.saveAll(entityList);
    }

   /* @ApiOperation(value = "获取当前模块拥有的属性权限")
    @PostMapping("/listModelQueryField")
    public
    Result
            <Map<String, List<SysPower>>> listModelQueryField(@RequestParam("modelCode") String modelCode) {
        return baseService.listModelQueryFieldByUserId(modelCode);
    }*/
}