package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.SysUserEmail;
import com.whln.ordermax.mapper.SysUserEmailMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class SysUserEmailRepository extends ServiceImpl<SysUserEmailMapper, SysUserEmail>{

    public IPage<SysUserEmail> pageByEntity(SysUserEmail entity, Page<SysUserEmail> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SysUserEmail> listByEntity(SysUserEmail entity) {
        return baseMapper.listByEntity(entity);
    }

    public SysUserEmail getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<SysUserEmail> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public boolean setDefault(String userId, String id) {
        return baseMapper.setDefault(userId,id);
    }
}




