package com.whln.ordermax.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.domain.entity.SysRole;
import com.whln.ordermax.domain.entity.SysRolePower;
import com.whln.ordermax.domain.entity.SysUser;
import com.whln.ordermax.domain.entity.SysUserRole;
import com.whln.ordermax.repostory.SysRolePowerRepository;
import com.whln.ordermax.repostory.SysRoleRepository;
import com.whln.ordermax.repostory.SysUserRepository;
import com.whln.ordermax.repostory.SysUserRoleRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class SysRoleService {
    @Resource
    private SysRoleRepository baseRepository;
    @Resource
    private SysUserRepository sysUserRepository;

    @Resource
    private SysUserRoleRepository sysUserRoleRepository;

    @Resource
    private SysRolePowerRepository sysRolePowerRepository;

    public Result listAll(SysRole entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    public Result pageAll(SysRole entity, Page<SysRole> page) {
        IPage<SysRole> sysRolePage = baseRepository.listByEntity(entity, page);
        return Result.success(sysRolePage);
    }


    public Result save(SysRole entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        //先删除与这个角色关联的用户，然后重新关联
        sysUserRoleRepository.remove(new LambdaUpdateWrapper<SysUserRole>().eq(SysUserRole::getRoleId, entity.getId()));
        if (ObjectUtil.isNotEmpty(entity.getUserIds())) {
            Set<SysUserRole> sysUserRoleSet = entity.getUserIds().stream().map(item -> {
                SysUserRole sysUserRole = new SysUserRole();
                sysUserRole.setRoleId(entity.getId());
                sysUserRole.setUserId(item);
                return sysUserRole;
            }).collect(Collectors.toSet());
            sysUserRoleRepository.saveBatch(sysUserRoleSet);
        }
        setPowers(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result setPowers(SysRole sysRole) {
        String roleId = sysRole.getId();
        List<String> powerIds = sysRole.getPowerIds();
        if (ObjectUtil.isEmpty(powerIds)) {
            return Result.success();
        }
        List<SysRolePower> sysUserRoleList = powerIds.stream().map(powerId -> {
            SysRolePower srp = new SysRolePower();
            srp.setRoleId(roleId);
            srp.setPowerId(powerId);
            return srp;
        }).collect(Collectors.toList());

        boolean b = sysRolePowerRepository.remove(new LambdaQueryWrapper<SysRolePower>().eq(SysRolePower::getRoleId, roleId));
        b &= sysRolePowerRepository.insertBatch(sysUserRoleList);
        return b ? Result.success() : Result.error("201", "操作失败");
    }

    public List<SysUser> queryUserByRoleId(String roleId) {
        List<String> userIds = sysUserRoleRepository.listObjs(new LambdaQueryWrapper<SysUserRole>()
                .select(SysUserRole::getUserId)
                .eq(SysUserRole::getRoleId, roleId), item -> Objects.toString(roleId));
        if (ObjectUtil.isEmpty(userIds)) {
            return new ArrayList<>(0);
        }
        return sysUserRepository.list(new LambdaQueryWrapper<SysUser>().in(SysUser::getId, userIds));
    }

}