package com.whln.ordermax.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.whln.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author liurun
 * @date 2023-03-06 15:05
 */

@EqualsAndHashCode(callSuper = true)
@TableName("customer_label_group")
@Data
public class CustomerLabelGroup extends BaseEntity {
    /**
     * id
     */
    @TableId(type=IdType.ASSIGN_ID)
    private String id;
    /**
     * 组名
     */
    private String groupName;
}
