package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whln.ordermax.domain.entity.SysRolePower;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysRolePower
 */
public interface SysRolePowerMapper extends BaseMapper<SysRolePower> {
    Integer insertBatch(@Param("entitylist") List<SysRolePower> entitylist);

}




