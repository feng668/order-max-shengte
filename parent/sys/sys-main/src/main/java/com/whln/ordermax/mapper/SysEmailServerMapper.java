package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.SysEmailServer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysEmailServer
 */
public interface SysEmailServerMapper extends BaseMapper<SysEmailServer> {

    List<SysEmailServer> listByEntity(@Param("entity")SysEmailServer entity);

    IPage<SysEmailServer> listByEntity(@Param("entity")SysEmailServer entity, Page<SysEmailServer> page);

    SysEmailServer getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<SysEmailServer> entitylist);

}




