package com.whln.ordermax.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 角色-菜单
 * @TableName sys_role_power
 */
@TableName(value ="sys_role_power")
@Data
public class SysRolePower implements Serializable {
    /**
     * 菜单编号
     */
    private String powerId;

    /**
     * 角色编号
     */
    private String roleId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}