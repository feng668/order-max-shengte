package com.whln.ordermax.domain.param;

import com.whln.ordermax.domain.vo.CustomerLabelVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-21 11:23
 */
@ApiModel("客户标签保存参数")
@Data
public class CustomerLabelParam {
    @ApiModelProperty("客户标签保存")
    List<CustomerLabelVO> customerLabelList;
}
