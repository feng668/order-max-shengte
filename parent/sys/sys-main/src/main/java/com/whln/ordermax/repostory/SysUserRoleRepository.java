package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.SysUserRole;
import com.whln.ordermax.mapper.SysUserRoleMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class SysUserRoleRepository extends ServiceImpl<SysUserRoleMapper, SysUserRole>{

    public Boolean insertBatch(List<SysUserRole> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




