package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.NrBill;
import com.whln.ordermax.mapper.NrBillMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class NrBillRepository extends ServiceImpl<NrBillMapper, NrBill>{

    public IPage<NrBill> pageByEntity(NrBill entity, Page<NrBill> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<NrBill> listByEntity(NrBill entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<NrBill> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




