package com.whln.ordermax.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.domain.entity.CustomerLabel;
import com.whln.ordermax.domain.entity.CustomerLabelGroup;
import com.whln.ordermax.domain.entity.CustomerLabelRelation;
import com.whln.ordermax.domain.param.CustomerLabelParam;
import com.whln.ordermax.domain.param.UserBindLabelParam;
import com.whln.ordermax.domain.query.UserLabelQuery;
import com.whln.ordermax.domain.vo.CustomerLabelVO;
import com.whln.ordermax.repostory.UserLabelGroupRepository;
import com.whln.ordermax.repostory.UserLabelRelationRepository;
import com.whln.ordermax.repostory.UserLabelRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-03-06 15:47
 */
@Service
@AllArgsConstructor
public class CustomerLabelService {
    private final UserLabelRepository userLabelRepository;
    private final UserLabelGroupRepository userLabelGroupRepository;
    private final UserLabelRelationRepository userLabelRelationRepository;

    public List<CustomerLabelVO> list(UserLabelQuery query) {
        List<CustomerLabel> list = userLabelRepository.list(new LambdaQueryWrapper<CustomerLabel>()
                .eq(ObjectUtil.isNotEmpty(query.getUserId()), CustomerLabel::getGroupId, query.getGroupId()));

        Set<String> groupIds = list.stream().map(CustomerLabel::getGroupId).collect(Collectors.toSet());
        List<CustomerLabelGroup> labelGroups = userLabelGroupRepository.list(new LambdaQueryWrapper<CustomerLabelGroup>()
                .in(CustomerLabelGroup::getId, groupIds));
        Map<String, String> labelGroupsToMap = labelGroups.stream()
                .collect(Collectors.toMap(CustomerLabelGroup::getId, CustomerLabelGroup::getGroupName));
        List<CustomerLabelVO> userLabelVOList = BeanUtil.copyToList(list, CustomerLabelVO.class);

        return userLabelVOList.stream().peek(item -> {
            item.setGroupName(labelGroupsToMap.get(item.getGroupId()));
        }).collect(Collectors.toList());
    }

    public List<CustomerLabelVO> listUserLabel(UserLabelQuery query) {
        List<CustomerLabelRelation> userLabelRelations = userLabelRelationRepository
                .list(new LambdaQueryWrapper<CustomerLabelRelation>()
                        .eq(ObjectUtil.isNotEmpty(query.getUserId()), CustomerLabelRelation::getUserId, query.getUserId()));
        Set<String> labelIds = userLabelRelations.stream().map(CustomerLabelRelation::getLabelId).collect(Collectors.toSet());
        List<CustomerLabel> list = userLabelRepository.list(new LambdaQueryWrapper<CustomerLabel>()
                .in( CustomerLabel::getId, labelIds));
        Set<String> groupIds = list.stream().map(CustomerLabel::getGroupId).collect(Collectors.toSet());
        List<CustomerLabelGroup> labelGroups = userLabelGroupRepository.list(new LambdaQueryWrapper<CustomerLabelGroup>()
                .in(CustomerLabelGroup::getId, groupIds));
        Map<String, String> labelGroupsToMap = labelGroups.stream()
                .collect(Collectors.toMap(CustomerLabelGroup::getId, CustomerLabelGroup::getGroupName));
        List<CustomerLabelVO> userLabelVOList = BeanUtil.copyToList(list, CustomerLabelVO.class);

        return userLabelVOList.stream().peek(item -> {
            item.setGroupName(labelGroupsToMap.get(item.getGroupId()));
        }).collect(Collectors.toList());
    }

    public void save( CustomerLabelParam param) {
        List<CustomerLabel> userLabel = BeanUtil.copyToList(param.getCustomerLabelList(), CustomerLabel.class);
        userLabelRepository.saveOrUpdateBatch(userLabel);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(List<String> ids) {
        userLabelRepository.removeByIds(ids);
        userLabelRelationRepository.remove(new LambdaQueryWrapper<CustomerLabelRelation>()
                .in(CustomerLabelRelation::getLabelId, ids));
    }

    public void bindUserLabel(UserBindLabelParam param) {
        CustomerLabel byId = userLabelRepository.getById(param.getLabelId());
        if (ObjectUtil.isEmpty(byId)) {
            throw new BusinessException("标签不存在");
        }
        CustomerLabelRelation userLabelRelation = CustomerLabelRelation.build(param);
        userLabelRelationRepository.save(userLabelRelation);
    }
}
