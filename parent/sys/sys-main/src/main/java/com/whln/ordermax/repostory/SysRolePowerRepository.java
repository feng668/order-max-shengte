package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.SysRolePower;
import com.whln.ordermax.mapper.SysRolePowerMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class SysRolePowerRepository extends ServiceImpl<SysRolePowerMapper, SysRolePower>{


    public Boolean insertBatch(List<SysRolePower> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




