package com.whln.ordermax.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whln.ordermax.domain.entity.CustomerLabelGroup;
import com.whln.ordermax.domain.param.UserLabelGroupParam;
import com.whln.ordermax.domain.vo.UserLabelGroupVO;
import com.whln.ordermax.repostory.UserLabelGroupRepository;
import com.whln.ordermax.service.UserLabelGroupService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 16:31
 */
@Service
@AllArgsConstructor
public class UserLabelGroupServiceImpl implements UserLabelGroupService {
    private final UserLabelGroupRepository userLabelGroupRepository;

    @Override
    public void save(UserLabelGroupVO userLabelGroupVO) {
        CustomerLabelGroup userLabelGroup = BeanUtil.copyProperties(userLabelGroupVO, CustomerLabelGroup.class);
        if (ObjectUtil.isNotEmpty(userLabelGroupVO.getId())) {
            userLabelGroupRepository.updateById(userLabelGroup);
        } else {
            userLabelGroupRepository.save(userLabelGroup);
        }
    }

    @Override
    public void deleteGroup(List<String> ids) {
        userLabelGroupRepository.removeByIds(ids);
    }

    @Override
    public List<UserLabelGroupVO> list(UserLabelGroupParam param) {
        List<CustomerLabelGroup> list = userLabelGroupRepository.list(new LambdaQueryWrapper<CustomerLabelGroup>()
                .or()
                .like(ObjectUtil.isNotEmpty(param.getName()), CustomerLabelGroup::getGroupName, param.getName())
        );
        return BeanUtil.copyToList(list, UserLabelGroupVO.class);
    }
}
