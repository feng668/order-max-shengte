package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.CustomerLabelGroup;
import com.whln.ordermax.mapper.UserLabelGroupMapper;
import org.springframework.stereotype.Repository;

/**
 * @author liurun
 * @date 2023-03-06 15:44
 */
@Repository
public class UserLabelGroupRepository extends ServiceImpl<UserLabelGroupMapper, CustomerLabelGroup> {
}
