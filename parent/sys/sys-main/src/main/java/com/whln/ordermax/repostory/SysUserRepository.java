package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.SysUser;
import com.whln.ordermax.mapper.SysUserMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class SysUserRepository extends ServiceImpl<SysUserMapper, SysUser> {


    public IPage<SysUser> pageByEntity(SysUser entity, Page<SysUser> page) {
        return baseMapper.listByEntity(entity, page);
    }



    public List<SysUser> listByEntity(SysUser entity) {
        return baseMapper.listByEntity(entity);
    }

    public SysUser getUserByUserId(String userName) {
        return baseMapper.getUserByUserId(userName);
    }

    public SysUser getUserByUsername(String name) {
        return baseMapper.selectOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUsername, name));
    }
}




