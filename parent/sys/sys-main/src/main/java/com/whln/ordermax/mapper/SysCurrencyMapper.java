package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.SysCurrency;
import com.whln.ordermax.domain.param.ExcelExportParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.ordermax.data.domain.SysCurrency
 */
public interface SysCurrencyMapper extends BaseMapper<SysCurrency> {

    List<SysCurrency> listByEntity(@Param("entity")SysCurrency entity);

    IPage<SysCurrency> listByEntity(@Param("entity")SysCurrency entity, Page<SysCurrency> page);

    SysCurrency getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<SysCurrency> entitylist);

    List<SysCurrency> listNewst(@Param("entity")SysCurrency entity);

    List<SysCurrency> listHistory(@Param("entity")SysCurrency entity);

    /**
     * 导出货币汇率记录
     * @param param
     * @return
     */
    List<SysCurrency> excel(ExcelExportParam param);
}




