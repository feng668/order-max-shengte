package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.CustomerLabelRelation;
import com.whln.ordermax.mapper.UserLabelRelationMapper;
import org.springframework.stereotype.Repository;

/**
 * @author liurun
 * @date 2023-03-06 16:12
 */
@Repository
public class UserLabelRelationRepository extends ServiceImpl<UserLabelRelationMapper, CustomerLabelRelation> {
}
