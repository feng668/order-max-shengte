package com.whln.ordermax.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
* 编号规则单据
* @TableName nr_role_node
*/
@ApiModel("编号规则")
@TableName("nr_role_node")
@Data
public class NrRoleNode {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 单据id
    */
    @ApiModelProperty("单据id")
    private String billId;
    /**
    * 节点类型
    */
    @ApiModelProperty("节点类型")
    private String nodeType;
    /**
    * 属性代码
    */
    @ApiModelProperty("属性代码")
    private String attributeCode;
    /**
     * 属性名称
     */
    @ApiModelProperty("属性名称")
    private String attributeName;
    /**
    * 节点值
    */
    @ApiModelProperty("节点值")
    private String nodeValue;
    /**
    * 节点排序
    */
    @ApiModelProperty("节点排序")
    private Integer nodeSort;
    /**
    * 流水号长度
    */
    @ApiModelProperty("流水号长度")
    private Integer nodeLength;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
