package com.whln.ordermax.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 13:54
 */
@ApiModel("导出参数")
@Data
public class ExcelExportParam {
    /**
     * 货币类型
     */
    @ApiModelProperty(value = "货币类型")
    private List<String> types;
}
