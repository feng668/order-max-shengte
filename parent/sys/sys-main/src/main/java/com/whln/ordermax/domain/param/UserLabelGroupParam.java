package com.whln.ordermax.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-07 9:31
 */
@Data
@ApiModel("用户组查询参数")
public class UserLabelGroupParam {
    @ApiModelProperty("组名")
    private String name;
}
