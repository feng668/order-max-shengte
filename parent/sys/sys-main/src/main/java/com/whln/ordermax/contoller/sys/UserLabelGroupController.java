package com.whln.ordermax.contoller.sys;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.domain.param.UserLabelGroupParam;
import com.whln.ordermax.domain.vo.UserLabelGroupVO;
import com.whln.ordermax.service.UserLabelGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 16:30
 */
@Api(tags = "客户标签组相关")
@RequestMapping("/labelGroup")
@AllArgsConstructor
@RestController
@Validated
public class UserLabelGroupController {
    private final UserLabelGroupService userLabelGroupService;

    @PostMapping("/save")
    @ApiOperation("保存标签组")
    public Result<Void> save(@RequestBody UserLabelGroupVO userLabelGroupVO) {
        userLabelGroupService.save(userLabelGroupVO);
        return Result.success();
    }

    @PostMapping("/deleteGroup")
    public Result<Void> deleteGroup(List<String> ids) {
        userLabelGroupService.deleteGroup(ids);
        return Result.success();
    }

    @PostMapping("/queryGroupList")
    public Result<List<UserLabelGroupVO>> queryGroupList(UserLabelGroupParam param) {
        List<UserLabelGroupVO> list = userLabelGroupService.list(param);
        return Result.success(list);
    }

}
