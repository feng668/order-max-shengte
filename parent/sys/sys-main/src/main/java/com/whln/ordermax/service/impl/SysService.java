package com.whln.ordermax.service.impl;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.whln.ordermax.common.enums.JudgeType;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.utils.SnowflakeIdWorker;
import com.whln.ordermax.repostory.NrBillRepository;
import com.whln.ordermax.repostory.NrRoleNodeRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SysService {

    private IdentifierGenerator identifierGenerator = new DefaultIdentifierGenerator();

    @Resource
    private NrRoleNodeRepository roleNodeRepository;

    @Resource
    private NrBillRepository nrBillRepository;

    /*@Resource
    private DynamicRepository dynamicRepository;*/

    public String nextId() {
        return SnowflakeIdWorker.generateId().toString();
    }

    /*public Result<String> nextBillNo(NumReqEntity entity) {
        NrBill nrBill = nrBillRepository.getById(entity.getNrBillId());
        Integer creatNum = entity.getCreatNum();
        if (creatNum == null || creatNum == 0) {
            creatNum = 1;
        }
        List<NrRoleNode> roleNodeList = roleNodeRepository.list(
                new QueryWrapper<NrRoleNode>().lambda()
                        .eq(NrRoleNode::getBillId, entity.getNrBillId())
                        .orderByAsc(NrRoleNode::getNodeSort)
        );
        if (ObjectUtil.isEmpty(roleNodeList)) {
            return Result.success("");
        }
        Map<String, Object> data = entity.getAttrData();
        String matchInfo = "";
        String matchResult = "";
        String initialValue = "";
        Integer fixedLength = 0;
        for (NrRoleNode node : roleNodeList) {
            String info = "";
            String result = "";
            switch (node.getNodeType()) {
                case "FIXED":
                    info = node.getNodeValue();
                    result = info;
                    break;
                case "ATTRIBUTE":
                    Object attribute = data.get(node.getAttributeCode());
                    if (attribute == null) {
                        return Result.error("201", "缺少属性：" + node.getAttributeName());
                    }
                    info = attribute.toString();
                    result = info;
                    if (info == null || info.equals("")) {
                        return Result.error("201", "缺少属性：" + node.getAttributeName());
                    }
                    break;
                case "SERIAL":
                    info += "(";
                    for (int i = 0; i < node.getNodeLength(); i++) {
                        info += "[0-9]";
                        result = "{}";
                        if (i < node.getNodeLength() - 1) {
                            initialValue += "0";
                        } else {
                            initialValue += "1";
                        }
                    }
                    info += ")";
                    fixedLength = node.getNodeLength();
                    break;
                case "CURRENT_TIME":
                    info = LocalDateTime.now().format(DateTimeFormatter.ofPattern(node.getNodeValue()));
                    result = info;
                    break;
            }
            matchInfo += info;
            matchResult += result;
        }
        String maxNo = dynamicRepository.getMaxNo(nrBill.getBillNoField(), nrBill.getTableName(), matchInfo);
        String result = null;
        if (null == maxNo) {
            result = StrUtil.format(matchResult, initialValue);
        } else {
            Pattern p = Pattern.compile(matchInfo);
            Matcher matcher = p.matcher(maxNo);
            while (matcher.find()) {
                Integer serial = Integer.valueOf(matcher.group(1));
                serial += 1;
                String serialStr = "";
                for (int i = 0; i < fixedLength - String.valueOf(serial).length(); i++) {
                    serialStr += "0";
                }
                serialStr += serial;
                result = StrUtil.format(matchResult, serialStr);
            }
        }

        return Result.success(result);
    }*/

/*    public Result<List<String>> nextBillNoList(NumReqEntity entity) {
        NrBill nrBill = nrBillRepository.getById(entity.getNrBillId());
        Integer creatNum = entity.getCreatNum();
        if (creatNum == null || creatNum == 0) {
            creatNum = 1;
        }
        List<NrRoleNode> roleNodeList = roleNodeRepository.list(
                new QueryWrapper<NrRoleNode>().lambda()
                        .eq(NrRoleNode::getBillId, entity.getNrBillId())
                        .orderByAsc(NrRoleNode::getNodeSort)
        );
        Map<String, Object> data = entity.getAttrData();
        String matchInfo = "";
        String matchResult = "";
        String initialValue = "";
        Integer fixedLength = 0;
        for (NrRoleNode node : roleNodeList) {
            String info = "";
            String result = "";
            switch (node.getNodeType()) {
                case "FIXED":
                    info = node.getNodeValue();
                    result = info;
                    break;
                case "ATTRIBUTE":
                    Object attribute = data.get(node.getAttributeCode());
                    if (attribute == null) {
                        return Result.error("201", "缺少属性：" + node.getAttributeName());
                    }
                    info = attribute.toString();
                    result = info;
                    if (info == null || info.equals("")) {
                        return Result.error("201", "缺少属性：" + node.getAttributeName());
                    }
                    break;
                case "SERIAL":
                    info += "(";
                    for (int i = 0; i < node.getNodeLength(); i++) {
                        info += "[0-9]";
                        result = "{}";
                        if (i < node.getNodeLength() - 1) {
                            initialValue += "0";
                        } else {
                            initialValue += "1";
                        }
                    }
                    info += ")";
                    fixedLength = node.getNodeLength();
                    break;
                case "CURRENT_TIME":
                    info = LocalDateTime.now().format(DateTimeFormatter.ofPattern(node.getNodeValue()));
                    result = info;
                    break;
            }
            matchInfo += info;
            matchResult += result;
        }
        String maxNo = dynamicRepository.getMaxNo(nrBill.getBillNoField(), nrBill.getTableName(), matchInfo);
        Pattern p = Pattern.compile(matchInfo);


        Integer initialValueNum = 0;
        List<String> resultList = new ArrayList<>();
        if (null == maxNo) {
            initialValueNum = 1;
        } else {
            Integer maxSerial = 0;
            Matcher m = p.matcher(maxNo);
            while (m.find()) {
                maxSerial = Integer.valueOf(m.group(1));
            }
            initialValueNum = Integer.valueOf(maxSerial) + 1;
        }
        for (int i = 0; i < creatNum; i++) {

            Integer serial = initialValueNum + i;
            String serialStr = "";
            for (int j = 0; j < fixedLength - String.valueOf(serial).length(); j++) {
                serialStr += "0";
            }
            serialStr += serial;
            String result = StrUtil.format(matchResult, serialStr);
            resultList.add(result);
        }
        return Result.success(resultList);
    }*/


    public Result<List<Map<String, String>>> listJudgeType() {

        List<Map<String, String>> collect = Arrays.stream(JudgeType.values())
                .map(e ->
                        new HashMap<String, String>(6) {{
                            put("value", e.getCode());
                            put("label", e.getName());
                        }}
                ).collect(Collectors.toList());
        return Result.success(collect);
    }


    /*public Result<String> getBorderId(String id, String billCode, String borderType) {
        String tableName = ModelMessage.valueOf(billCode).getTableName();
        return Result.success(dynamicRepository.getBorderIdByBillId(id, tableName, borderType));
    }
*/
    public Result<List<Map<String, String>>> listModels() {
        List<Map<String, String>> list = Arrays.stream(ModelMessage.values()).map(m -> new HashMap<String, String>() {{
            put("code", m.getCode());
            put("name", m.getName());
        }}).collect(Collectors.toList());
        return Result.success(list);
    }
}
