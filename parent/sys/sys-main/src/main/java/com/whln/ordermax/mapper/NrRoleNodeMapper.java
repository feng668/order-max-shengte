package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.NrRoleNode;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.NrRoleNode
 */
public interface NrRoleNodeMapper extends BaseMapper<NrRoleNode> {

    List<NrRoleNode> listByEntity(@Param("entity")NrRoleNode entity);

    IPage<NrRoleNode> listByEntity(@Param("entity")NrRoleNode entity, Page<NrRoleNode> page);

    Integer insertBatch(@Param("entitylist")List<NrRoleNode> entitylist);

}




