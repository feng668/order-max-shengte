package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.SysCompany;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysCompany
 */
public interface SysCompanyMapper extends BaseMapper<SysCompany> {

    List<SysCompany> listByEntity(@Param("entity")SysCompany entity);

    IPage<SysCompany> listByEntity(@Param("entity")SysCompany entity, Page<SysCompany> page);

    Integer insertBatch(@Param("entitylist")List<SysCompany> entitylist);

}




