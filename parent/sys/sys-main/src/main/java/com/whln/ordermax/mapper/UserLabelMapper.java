package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whln.ordermax.domain.entity.CustomerLabel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liurun
 * @date 2023-03-06 15:07
 */
@Mapper
public interface UserLabelMapper extends BaseMapper<CustomerLabel> {
}
