package com.whln.ordermax.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
* 权限
* @TableName sys_power
*/
@ApiModel("权限")
@TableName("sys_power")
@Data
public class SysPower {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * 
    */
    private String id;
    /**
    * 资源路径
    */
    @ApiModelProperty("资源路径")
    private String name;
    /**
    * 点击后前往的地址
    */
    @ApiModelProperty("点击后前往的地址")
    private String url;
    /**
    * 父编号
    */
    @ApiModelProperty("父编号")
    private String parentId;
    /**
    * 父编号列表
    */
    @ApiModelProperty("父编号列表")
    private String parentIds;
    /**
    * 权限字符串
    */
    @ApiModelProperty("权限字符串")
    private String permission;
    /**
    * 是否显示
    */
    @ApiModelProperty("是否显示")
    private Boolean enabled;
    /**
    * 排序
    */
    @ApiModelProperty("排序")
    private Integer sort;
    /**
     * 权限类别
     */
    @ApiModelProperty("权限类别")
    private String category;
    /**
    * 图标
    */
    @ApiModelProperty("图标")
    private String menuIcon;
    /**
    * 摘要
    */
    @ApiModelProperty("摘要")
    private String remarks;
    /**
    * 
    */
    @ApiModelProperty("")
    private String createBy;
    /**
    * 
    */
    @ApiModelProperty("")
    private LocalDateTime createDate;
    /**
    * 
    */
    @ApiModelProperty("")
    private LocalDateTime updateDate;
    /**
    * 别名
    */
    @ApiModelProperty("别名")
    private String alias;

    private String fieldSource;


    @TableField(exist = false)
    @ApiModelProperty("子节点")
    private List<SysPower> chirds;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
