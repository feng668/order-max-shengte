package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.SysDepartment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysDepartment
 */
public interface SysDepartmentMapper extends BaseMapper<SysDepartment> {

    List<SysDepartment> listByEntity(@Param("entity")SysDepartment entity);

    IPage<SysDepartment> listByEntity(@Param("entity")SysDepartment entity, Page<SysDepartment> page);

    Integer insertBatch(@Param("entitylist")List<SysDepartment> entitylist);

}




