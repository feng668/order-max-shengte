package com.whln.ordermax.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.utils.TreeUtil;
import com.whln.ordermax.domain.entity.SysPower;
import com.whln.ordermax.repostory.SysPowerRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class SysPowerService {
    @Resource
    private SysPowerRepository baseRepository;

    public Result pageAll(SysPower entity, Page<SysPower> page) {
        return Result.success(baseRepository.page(page));
    }

    public Result save(SysPower entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result treeData() {
//        List<SysPower> sysPowerList = baseRepository.list().stream().sorted(Comparator.comparing(p -> p.getParentId()==null?0:p.getParentId().split("/").length,Comparator.reverseOrder())).collect(Collectors.toList());
        List<SysPower> sysPowerList = baseRepository.list(new QueryWrapper<SysPower>().orderByDesc("parent_ids").ne("category","FIELD"));
        Map<String, List<SysPower>> parentIdMap = sysPowerList.stream().collect(Collectors.groupingBy(p -> p.getParentId()==null?"ROOT":p.getParentId()));
        for (SysPower sysPower : sysPowerList) {
            sysPower.setChirds(parentIdMap.get(sysPower.getId()));
            List<SysPower> parentList = parentIdMap.get(sysPower.getParentId()==null?"ROOT":sysPower.getParentId());
            /*if (!parentList.contains(sysPower)){
                parentList.add(sysPower);
            }*/
            parentIdMap.put(sysPower.getParentId(),parentList);
        }
        return Result.success(parentIdMap.get("ROOT"));
    }

    public Result treeDataByRoleId(String roleId) {
        List<SysPower> sysPowerList = baseRepository.listPowersByRoleId(roleId);
        Map<String, List<SysPower>> parentIdMap = sysPowerList.stream().collect(Collectors.groupingBy(p -> p.getParentId()==null?"ROOT":p.getParentId()));
        for (SysPower sysPower : sysPowerList) {
            sysPower.setChirds(parentIdMap.get(sysPower.getId()));
            List<SysPower> parentList = parentIdMap.get(sysPower.getParentId()==null?"ROOT":sysPower.getParentId());
            /*if (!parentList.contains(sysPower)){
                parentList.add(sysPower);
            }*/
            parentIdMap.put(sysPower.getParentId(),parentList);
        }
        return Result.success(parentIdMap.get("ROOT"));
    }

    public Result saveAll(List<SysPower> entityList) {
        if (CollectionUtil.isNotEmpty(entityList)){
            List<SysPower> allList = TreeUtil.treeToList(entityList);
            baseRepository.saveOrUpdateBatch(allList);
        }
        return Result.success();
    }

    public Result<List<SysPower>> listFieldByParentId(String parentId) {
        return Result.success(baseRepository.listFieldByParentId(parentId));
    }

    public Result<List<Map>> listFieldByParentIdRoleId(String parentId, String roleId) {
        return Result.success(baseRepository.listFieldByParentIdRoleId(parentId,roleId));
    }

    /*public Result<Map<String, List<SysPower>>> listModelQueryFieldByUserId(String modelCode) {
        String userId = AuthService.getUserIdFromAuthentication();
        return Result.success(
                baseRepository.listModelQueryFieldByUserId(userId, modelCode)
                        .stream()
                        .collect(Collectors.groupingBy(SysPower::getFieldSource))
        );
    }*/
}