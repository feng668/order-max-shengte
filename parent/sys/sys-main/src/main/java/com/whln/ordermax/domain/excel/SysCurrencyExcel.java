package com.whln.ordermax.domain.excel;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 币种
 *
 * @TableName sys_currency
 */
@ApiModel("币种")
@Data
public class SysCurrencyExcel {

    /**
     * id
     */
    @ExcelIgnore
    private String id;
    /**
     * 币种编码
     */
    @ExcelProperty(value = "币种名称", index = 0)
    @ApiModelProperty("币种名称")
    private String currencyName;
    /**
     * 币种名称
     */
    @ExcelProperty(value = "币种编码", index = 1)
    @ApiModelProperty("币种编码")
    private String currencyCode;
    /**
     * 汇率
     */
    @ExcelProperty(value = "汇率", index = 2)
    @ApiModelProperty("汇率")
    private Double rate;
    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间", index = 3)
    @ApiModelProperty("创建时间")
    private String createTime;
    /**
     * 更新时间
     */
    @ExcelProperty(value = "更新时间", index = 4)
    @ApiModelProperty("更新时间")
    private String updateTime;
    /**
     * 拥有人id
     */
    @ExcelIgnore

    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ExcelIgnore
    @ApiModelProperty("期")
    private Integer stageNum;

    /* 附加属性 */
    @ApiModelProperty("上期汇率")
    @ExcelIgnore
    @TableField(exist = false)
    private Double lastRate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
