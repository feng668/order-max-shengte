package com.whln.ordermax.contoller.sys;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.domain.param.CustomerLabelParam;
import com.whln.ordermax.domain.param.UserBindLabelParam;
import com.whln.ordermax.domain.query.UserLabelQuery;
import com.whln.ordermax.domain.vo.CustomerLabelVO;
import com.whln.ordermax.service.impl.CustomerLabelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 15:46
 */
@Api(tags = "客户标签操作")
@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/userLabel")
public class UserLabelController {
    private final CustomerLabelService userLabelService;

    @ApiOperation("用户标签列表")
    @PostMapping("/list")
    public Result<List<CustomerLabelVO>> list(@RequestBody UserLabelQuery param) {
        List<CustomerLabelVO> list = userLabelService.list(param);
        return Result.success(list);
    }
    @ApiOperation("用户标签列表")
    @PostMapping("/listUserLabel")
    public Result<List<CustomerLabelVO>> listUserLabel(@RequestBody UserLabelQuery param) {
        List<CustomerLabelVO> list = userLabelService.listUserLabel(param);
        return Result.success(list);
    }
    @ApiOperation("删除用户标签")
    @ApiImplicitParam(value = "ids 数组", example = "['324324','42342']")
    @PostMapping("/deleteUserLabel")
    public Result<List<CustomerLabelVO>> deleteUserLabel(List<String> ids) {
        userLabelService.delete(ids);
        return Result.success();
    }

    @ApiOperation("保存用户标签")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody CustomerLabelParam param) {
        userLabelService.save(param);
        return Result.success();
    }

    @ApiOperation("用户绑定标签")
    @PostMapping("/bindUserLabel")
    public Result<Void> bindUserLabel(@RequestBody UserBindLabelParam param){
        userLabelService.bindUserLabel(param);
        return Result.success();
    }
}
