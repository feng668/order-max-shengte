package com.whln.ordermax.contoller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.domain.entity.SysEmailServer;
import com.whln.ordermax.service.impl.SysEmailServerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  邮箱服务器控制器
 */
@Api(tags = "邮箱服务器")
@RestController
@RequestMapping("/sysEmailServer")
public class SysEmailServerController{

    @Resource
    private SysEmailServerService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<SysEmailServer>> listAll(@RequestBody SysEmailServer entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<SysEmailServer>> pageAll(@RequestBody PageEntity<SysEmailServer> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<SysEmailServer> getAllById(@RequestParam("id") String id){
        return baseService.getAllById(id);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() SysEmailServer entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }


}