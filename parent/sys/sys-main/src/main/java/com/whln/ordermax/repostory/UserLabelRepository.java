package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.CustomerLabel;
import com.whln.ordermax.mapper.UserLabelMapper;
import org.springframework.stereotype.Repository;

/**
 * @author liurun
 * @date 2023-03-06 15:43
 */
@Repository
public class UserLabelRepository extends ServiceImpl<UserLabelMapper, CustomerLabel> {
}
