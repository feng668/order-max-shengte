package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whln.ordermax.domain.entity.CustomerLabelGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liurun
 * @date 2023-03-06 15:41
 */
@Mapper
public interface UserLabelGroupMapper extends BaseMapper<CustomerLabelGroup> {
}
