package com.whln.ordermax.service;


import com.whln.ordermax.domain.param.UserLabelGroupParam;
import com.whln.ordermax.domain.vo.UserLabelGroupVO;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-07 9:26
 */
public interface UserLabelGroupService {
    /**
     * 保存用户组
     *
     * @param userLabelGroupVO
     */
    void save(UserLabelGroupVO userLabelGroupVO);

    void deleteGroup(List<String> ids);

    /**
     * 查询用户组列表
     * @return
     */
    List<UserLabelGroupVO> list(UserLabelGroupParam pram);
}
