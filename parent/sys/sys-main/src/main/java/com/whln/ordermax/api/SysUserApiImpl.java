package com.whln.ordermax.api;

import cn.hutool.core.bean.BeanUtil;
import com.whln.api.SysUserApi;
import com.whln.dto.SysUserDTO;
import com.whln.ordermax.domain.entity.SysUser;
import com.whln.ordermax.repostory.SysUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author liurun
 * @date 2023-03-21 10:39
 */
@AllArgsConstructor
@Service
public class SysUserApiImpl implements SysUserApi {
    private final SysUserRepository sysUserRepository;
    @Override
    public SysUserDTO getUserByUsername(String name) {
        SysUser userByUsername = sysUserRepository.getUserByUsername(name);
        return BeanUtil.copyProperties(userByUsername,SysUserDTO.class);
    }
}
