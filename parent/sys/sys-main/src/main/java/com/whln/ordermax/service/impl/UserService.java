package com.whln.ordermax.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.utils.PasswordUtil;
import com.whln.ordermax.domain.entity.*;
import com.whln.ordermax.repostory.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Resource
    private SysUserRepository sysUserRepository;

    @Resource
    private SysRoleRepository sysRoleRepository;

    @Resource
    private SysPowerRepository sysPowerRepository;

    @Resource
    private SysUserRoleRepository sysUserRoleRepository;

    @Resource
    private SysUserEmailRepository userEmailRepository;

    @Resource
    private SysDepartmentRepository sysDepartmentRepository;

/*
    public Result getCurrentUserInfo(){
        String userId = ShiroManager.getUserId();
        SysUser user = sysUserRepository.getUserByUserId(userId);

        //获取角色
        List<SysRole> roleList = sysRoleRepository.listRolesByUserId(user.getId());

        //获取权限树
        List<SysPower> sysPowerList = sysPowerRepository.listPowersByUserId(user.getId());
        Map<String, List<SysPower>> parentIdMap = sysPowerList.stream().collect(Collectors.groupingBy(p -> p.getParentId()==null?"ROOT":p.getParentId()));
        for (SysPower sysPower : sysPowerList) {
            sysPower.setChirds(parentIdMap.get(sysPower.getId()));
            List<SysPower> parentList = parentIdMap.get(sysPower.getParentId()==null?"ROOT":sysPower.getParentId());
            */
/*if (!parentList.contains(sysPower)){
                parentList.add(sysPower);
            }*//*

            parentIdMap.put(sysPower.getParentId(),parentList);
        }
        List<SysPower> powerTree = parentIdMap.get("ROOT");
        Map result = new HashMap();
        result.put("user",user);
        result.put("roles",roleList);
        result.put("powerTree",powerTree);
        return Result.success(result);
    }
*/

    public Result pageAll(SysUser entity, Page<SysUser> page) {
        if (ObjectUtil.isNotEmpty(entity)&&ObjectUtil.isNotEmpty(entity.getDepartmentId())) {
            Set<String> ids = new HashSet<>();
            Set<String> departmentIds = getDepartmentIds(entity.getDepartmentId());
            entity.setDepartmentIds(departmentIds);
        }
        return Result.success(sysUserRepository.pageByEntity(entity,page));
    }

    /**
     * 获取部门加上子部门的id
     *
     * @param id  部门id
     * @return Set 部门id加上子部门的id
     */
    private Set<String> getDepartmentIds(String id) {
        Set<String> ids = new HashSet<>();
        List<SysDepartment> list = sysDepartmentRepository
                .list(new LambdaQueryWrapper<SysDepartment>()
                        .eq(SysDepartment::getParentId, id));
        List<String> stringStream = list.stream()
                .map(SysDepartment::getId)
                .collect(Collectors.toList());
        ids.add(id);
        for (String s : stringStream) {
            Set<String> departmentIds = getDepartmentIds(s);
            ids.addAll(departmentIds);
        }
        return ids;
    }
    public Result listAll(SysUser entity) {
        return Result.success(sysUserRepository.listByEntity(entity));
    }

    @Transactional
    public Result save(SysUser entity) {
        if (StrUtil.isNotEmpty(entity.getPassword())){
            entity.setPassword(PasswordUtil.encode(entity.getPassword()));
        }
        QueryWrapper<SysUser> wrapper = new QueryWrapper<SysUser>();
        wrapper.eq("username",entity.getUsername());
        if (entity.getId()!=null){
            wrapper.ne("id", entity.getId());
        }
        long usernameCount = sysUserRepository.count(wrapper);
        if (usernameCount>0){
            return Result.error("205","已存在当前用户名");
        }
        boolean b = sysUserRepository.saveOrUpdate(entity);
        setRoles(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    public Result delete(String id) {
        boolean b = sysUserRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    public Result batchDelete(List<String> ids) {
        boolean b = sysUserRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

    @Transactional
    public Result setRoles(SysUser sysUser) {
        String userId = sysUser.getId();
        List<String> roleIds = sysUser.getRoleIds();
        boolean b = sysUserRoleRepository.remove(new QueryWrapper<SysUserRole>().eq("user_id", userId));
        if (roleIds!=null){
            List<SysUserRole> sysUserRoleList = roleIds.stream().map(roleId -> {
                SysUserRole sur = new SysUserRole();
                sur.setUserId(userId);
                sur.setRoleId(roleId);
                return sur;
            }).collect(Collectors.toList());
            b &= sysUserRoleRepository.insertBatch(sysUserRoleList);
        }
        return b? Result.success(): Result.error("201","操作失败");
    }

    public Result getRoles(String userId) {
        List<SysRole> sysRoles = sysRoleRepository.listRolesByUserId(userId);
        return Result.success(sysRoles);
    }

    public Result setMail(SysUser sysUser) {
        if (!StrUtil.isAllNotEmpty(sysUser.getEmail(),sysUser.getEmailPassword()));
        boolean b = sysUserRepository.updateById(new SysUser() {{
            setId(sysUser.getId());
            setEmail(sysUser.getEmail());
            setEmailPassword(sysUser.getEmailPassword());
        }});
        return b? Result.success(): Result.error("201","修改失败");
    }

    public Result saveMail(SysUserEmail userEmail) {
        //用户第一个邮箱设为默认
        Long userExsitsEmailCount = userEmailRepository.count(new QueryWrapper<SysUserEmail>().eq("user_id", userEmail.getUserId()));
        if (userExsitsEmailCount==0){
            userEmail.setIsDefault(1);
        }
        return userEmailRepository.saveOrUpdate(userEmail)? Result.success(): Result.error("201","操作失败");
    }

    public Result deleteMail(String id) {
        return userEmailRepository.removeById(id)? Result.success(): Result.error("201","操作失败");
    }

   /* public Result<List<SysUserEmail>> listMail() {
       *//* String userId = AuthService.getUserIdFromAuthentication();
        return Result.success(userEmailRepository.listByEntity(
                new SysUserEmail(){{
                    setUserId(userId);
                }}
        ));*//*
        return null;
    }*/

    /*public Result setDefaultMail(String id) {
//        String userId = AuthService.getUserIdFromAuthentication();
        return userEmailRepository.setDefault(userId,id)? Result.success(): Result.error("201","操作失败");
    }*/
}
