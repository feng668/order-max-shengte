package com.whln.ordermax.contoller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.domain.entity.SysRole;
import com.whln.ordermax.domain.entity.SysUser;
import com.whln.ordermax.domain.entity.SysUserEmail;
import com.whln.ordermax.service.impl.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "用户管理")
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    /*@ApiOperation(value = "获取当前用户信息、角色、权限")
    @PostMapping("/current")
    public Result current(){
        return userService.getCurrentUserInfo();
    }
*/
    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<SysUser>> pageAll(@RequestBody PageEntity<SysUser> pageEntity) {
        return userService.pageAll(pageEntity.getEntity(), new Page(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<SysUser>> listAll(@RequestBody SysUser entity){
        return userService.listAll(entity);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody SysUser entity){

        return userService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return userService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){

        return userService.batchDelete(ids);
    }
    @ApiOperation(value = "配置角色")
    @PostMapping("/setRoles")
    public Result setRoles(@RequestBody() SysUser sysUser){

        return userService.setRoles(sysUser);
    }

    @ApiOperation(value = "获取用户角色")
    @PostMapping("/getRoles")
    public Result<List<SysRole>> getRoles(@RequestParam("userId") String userId){
        return userService.getRoles(userId);
    }

    @ApiOperation(value = "配置邮箱|N")
    @PostMapping("/setMail")
    public Result setMail(@RequestBody SysUser sysUser){
        return userService.setMail(sysUser);
    }

    @ApiOperation(value = "保存邮箱|N")
    @PostMapping("/saveMail")
    public Result saveMail(@RequestBody SysUserEmail userEmail){
        return userService.saveMail(userEmail);
    }

    @ApiOperation(value = "删除邮箱|N")
    @PostMapping("/deleteMail")
    public Result deleteMail(@RequestParam("id") String id){
        return userService.deleteMail(id);
    }

/*
    @ApiOperation(value = "查看邮箱")
    @PostMapping("/listMail")
    public Result<List<SysUserEmail>> listMail(){
        return userService.listMail();
    }
*/

   /* @ApiOperation(value = "设置默认邮箱")
    @PostMapping("/setDefaultMail")
    public Result setDefaultMail(@RequestParam("id")String id){
        return userService.setDefaultMail(id);
    }*/
}
