package com.whln.ordermax.contoller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.domain.entity.SysCompany;
import com.whln.ordermax.service.impl.SysCompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  用户公司控制器
 */
@Api(tags = "用户公司")
@RestController
@RequestMapping("/SysCompany")
public class SysCompanyController{

    @Resource
    private SysCompanyService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<SysCompany>> listAll(@RequestBody SysCompany entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<SysCompany>> pageAll(@RequestBody PageEntity<SysCompany> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存公司及部门")
    @PostMapping("/save")
    public Result save(@RequestBody() SysCompany entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}