package com.whln.ordermax.repostory;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.domain.entity.SysDepartment;
import com.whln.ordermax.mapper.SysDepartmentMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class SysDepartmentRepository extends ServiceImpl<SysDepartmentMapper, SysDepartment>{

    public IPage<SysDepartment> pageByEntity(SysDepartment entity, Page<SysDepartment> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SysDepartment> listByEntity(SysDepartment entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SysDepartment> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




