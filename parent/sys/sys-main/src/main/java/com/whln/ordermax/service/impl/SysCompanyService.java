package com.whln.ordermax.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.utils.TreeUtil;
import com.whln.ordermax.domain.entity.SysCompany;
import com.whln.ordermax.domain.entity.SysDepartment;
import com.whln.ordermax.repostory.SysCompanyRepository;
import com.whln.ordermax.repostory.SysDepartmentRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  用户公司业务处理
 */
@Service
public class SysCompanyService{
    @Resource
    private SysCompanyRepository baseRepository;

    @Resource
    private SysDepartmentRepository departmentRepository;

    /**
    *  查询所有
    */
    public Result listAll(SysCompany entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(SysCompany entity, Page<SysCompany> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(SysCompany entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        if (CollectionUtil.isNotEmpty(entity.getDepartmentDelIdList())){
            LambdaQueryWrapper<SysDepartment> wrapper = new LambdaQueryWrapper<SysDepartment>();
            wrapper.in(SysDepartment::getId,entity.getDepartmentDelIdList());
            for (String delId : entity.getDepartmentDelIdList()) {
                wrapper.or().like(SysDepartment::getParentIds,delId);
            }
            b &= departmentRepository.remove(wrapper);
        }

        if (CollectionUtil.isNotEmpty(entity.getDepartmentList())){
            List<SysDepartment> allList = TreeUtil.treeToList(entity.getDepartmentList());
            b &= departmentRepository.saveOrUpdateBatch(allList);
        }
        return b? Result.success(): Result.error("201","保存失败");
    }


    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

}