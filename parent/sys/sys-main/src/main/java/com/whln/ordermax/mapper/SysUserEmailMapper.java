package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.SysUserEmail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysUserEmail
 */
public interface SysUserEmailMapper extends BaseMapper<SysUserEmail> {

    List<SysUserEmail> listByEntity(@Param("entity")SysUserEmail entity);

    IPage<SysUserEmail> listByEntity(@Param("entity")SysUserEmail entity, Page<SysUserEmail> page);

    SysUserEmail getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<SysUserEmail> entitylist);

    boolean setDefault(@Param("userId")String userId,@Param("id")String id);
}




