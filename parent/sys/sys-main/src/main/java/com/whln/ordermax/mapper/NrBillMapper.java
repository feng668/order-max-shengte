package com.whln.ordermax.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.domain.entity.NrBill;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.NrBill
 */
public interface NrBillMapper extends BaseMapper<NrBill> {

    List<NrBill> listByEntity(@Param("entity")NrBill entity);

    IPage<NrBill> listByEntity(@Param("entity")NrBill entity, Page<NrBill> page);

    Integer insertBatch(@Param("entitylist")List<NrBill> entitylist);

}




