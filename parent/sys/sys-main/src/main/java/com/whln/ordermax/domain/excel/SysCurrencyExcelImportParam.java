package com.whln.ordermax.domain.excel;

import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 20:27
 */
@Data
public class SysCurrencyExcelImportParam {
    List<SysCurrencyExcel> list;
}
