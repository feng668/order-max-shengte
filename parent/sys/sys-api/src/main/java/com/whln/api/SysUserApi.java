package com.whln.api;

import com.whln.dto.SysUserDTO;

/**
 * @author liurun
 * @date 2023-03-21 10:20
 */
public interface SysUserApi {
    SysUserDTO getUserByUsername(String name);
}
