package com.whln.api;

import com.whln.dto.SysRoleDTO;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-21 10:13
 */
public interface SysRoleApi {
    List<SysRoleDTO> listByUserId(String userId);
}
