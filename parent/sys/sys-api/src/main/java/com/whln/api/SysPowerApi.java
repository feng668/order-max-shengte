package com.whln.api;

import com.whln.dto.SysPowerDTO;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-21 10:13
 */
public interface SysPowerApi {
    List<SysPowerDTO> listByUserId(String userId);
}
