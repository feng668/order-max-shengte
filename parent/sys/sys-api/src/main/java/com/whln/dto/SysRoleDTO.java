package com.whln.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-21 10:14
 */
@Data
public class SysRoleDTO implements Serializable {
    private static final long serialVersionUID = -91893728639653198L;

    /**
     * 编号
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty("编号")
    private String id;
    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    private String name;
    /**
     * 英文名称
     */
    @ApiModelProperty("英文名称")
    private String code;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty("备注信息")
    private String remarks;
    @TableField(exist = false)
    private List<String> userIds;
    @ApiModelProperty("权限id集合")
    @TableField(exist = false)
    private List<String> powerIds;
    @TableField(exist = false)
    private String searchInfo;

}
