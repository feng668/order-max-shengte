package com.whln.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * @author liurun
 * @date 2023-03-21 10:20
 */
@Data
public class SysUserDTO {
    @TableId(type = IdType.ASSIGN_ID)
    /**
     * 主键
     */
    private String id;
    /**
     * 员工编号
     */
    @ApiModelProperty("员工编号")
    private String empId;
    /**
     * 真实名称
     */
    @ApiModelProperty("真实名称")
    private String realname;
    /**
     * 用户名
     */
    @ApiModelProperty("用户名")
    private String username;
    /**
     * 头像
     */
    @ApiModelProperty("头像")
    private String portrait;
    /**
     * 密码
     */
    @ApiModelProperty("密码")
    private String password;
    /**
     * 邮件
     */
    @ApiModelProperty("邮件")
    private String email;

    private String emailPassword;
    /**
     * 联系电话
     */
    @ApiModelProperty("联系电话")
    private String phone;
    /**
     * 系统用户的状态
     */
    @ApiModelProperty("系统用户的状态")
    private String status;
    /**
     *
     */
    @ApiModelProperty("")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
     *
     */
    @ApiModelProperty("")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     *
     */
    @ApiModelProperty("")
    private String remarks;
    /**
     * 用户是否显示，0不显示，1显示
     */
    @ApiModelProperty("用户是否显示，0不显示，1显示")
    private Integer isShow;
    /**
     * 过期时间
     */
    @ApiModelProperty("过期时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime expiredDate;
    /**
     * 绑定登录的IP地址
     */
    @ApiModelProperty("绑定登录的IP地址")
    private String bindIp;
    /**
     * 上次登录时间
     */
    @ApiModelProperty("上次登录时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastLoginDate;
    /**
     * 当前登录时间
     */
    @ApiModelProperty("当前登录时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime loginDate;
    /**
     * 登录次数
     */
    @ApiModelProperty("登录次数")
    private Integer loginCount;
    /**
     * 登录IP地址
     */
    @ApiModelProperty("登录IP地址")
    private String loginIp;
    /**
     * 登录城市
     */
    @ApiModelProperty("登录城市")
    private String loginCity;

    /**
     * 员工编号
     */
    @ApiModelProperty("员工编号")
    private String stafferNo;
    /**
     * 身份证号
     */
    @ApiModelProperty("身份证号")
    private String stafferCode;
    /**
     * 英文名
     */
    @ApiModelProperty("英文名")
    private String enName;
    /**
     * 职位
     */
    @ApiModelProperty("职位")
    private String position;
    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private Integer gender;
    /**
     * 部门id
     */
    @ApiModelProperty("部门id")
    private String departmentId;
    /**
     * 公司id
     */
    @ApiModelProperty("公司id")
    private String companyId;

    /*附加属性*/
    @TableField(exist = false)
    @ApiModelProperty("角色id集")
    private List<String> roleIds;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("部门名称|回显")
    private String dptName;
    @TableField(exist = false)
    @ApiModelProperty("部门全称|回显")
    private String fullDptName;
    @TableField(exist = false)
    /**
     * 部门id set
     */
    private Set<String> departmentIds;
    @TableField(exist = false)
    private String searchInfo; //只用来查询
}
