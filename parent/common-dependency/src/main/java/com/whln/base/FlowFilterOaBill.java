package com.whln.base;

import lombok.Data;

@Data
public class FlowFilterOaBill extends FilterOaBill {
    private String flowStatus;
}
