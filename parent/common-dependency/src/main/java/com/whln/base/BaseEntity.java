package com.whln.base;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-03-06 15:02
 */
@Data
public class BaseEntity {
    /**
     * 创建时间
     */
    protected LocalDateTime createTime;
    /**
     * 创建人
     */
    protected String createBy;
    /**
     * 修改时间
     */
    protected LocalDateTime updateTime;
    /**
     * 修改人
     */
    protected String updateBy;

}
