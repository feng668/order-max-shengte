package com.whln.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.whln.ordermax.common.entity.FilterCondition;
import lombok.Data;

import java.util.List;

@Data
public class FilterBill extends BillBase{

    @TableField(exist = false)
    private List<FilterCondition> filterConditionList;
}
