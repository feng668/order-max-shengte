package com.whln.base;

import lombok.Data;

@Data
public class FlowComFilterOaBill extends ComFilterOaBill {
    private String flowStatus;
}
