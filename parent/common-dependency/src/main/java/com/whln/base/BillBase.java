package com.whln.base;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-03 17:52
 */
@Data
public class BillBase {
    @TableField(exist = false)
    protected String lastBillId;
    @TableField(exist = false)
    protected String lastBillCode;
    @TableField(exist = false)
    protected String currentBillCode;
    @TableField(exist = false)
    protected String currentBillId;
}
