package com.whln.config.typeHandler;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@MappedTypes(JSONObject.class)
@MappedJdbcTypes(JdbcType.VARCHAR)
public class JsonTypeHandler extends BaseTypeHandler {

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i, Object parameter, JdbcType jdbcType) throws SQLException {
        String str = ObjToStr(parameter);
        ps.setString(i, str);
    }

    @Override
    public Object getNullableResult(ResultSet rs, String columnName) throws SQLException {
        byte[] bytes = rs.getBytes(columnName);
        return !rs.wasNull() && bytes != null ? strToObj(new String(bytes)) : null;
    }

    @Override
    public Object getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        byte[] bytes = rs.getBytes(columnIndex);
        return !rs.wasNull() && bytes != null ? strToObj(new String(bytes)) : null;
    }

    @Override
    public Object getNullableResult(CallableStatement cs, int columnIndex) throws SQLException {
        byte[] bytes = cs.getBytes(columnIndex);
        return !cs.wasNull() && bytes != null ? strToObj(new String(bytes)) : null;
    }

    public static String ObjToStr(Object obj) {
        return JSONUtil.toJsonStr(obj);
    }

    public static Object strToObj(String str) {
        return JSONUtil.parseObj(str);
    }
}