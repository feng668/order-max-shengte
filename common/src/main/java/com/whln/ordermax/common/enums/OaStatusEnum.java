package com.whln.ordermax.common.enums;

/**
 * @author liurun
 * @date 2023-04-08 10:34
 */
public enum OaStatusEnum {
  PASS("通过",1)
            ;
    private String name;
    private Integer value;


    OaStatusEnum(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
