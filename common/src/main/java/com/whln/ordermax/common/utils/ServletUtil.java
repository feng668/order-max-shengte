package com.whln.ordermax.common.utils;

import com.whln.ordermax.common.entity.Result;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletUtil {
    public static void writeResponse(HttpServletResponse httpServletResponse, int status, Result responseResult) throws IOException {
        httpServletResponse.setStatus(status);
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        httpServletResponse.getWriter().print(responseResult.toJsonString());
        httpServletResponse.getWriter().flush();
    }

    public static void writeResponse(HttpServletResponse httpServletResponse, Result responseResult) throws IOException {
        httpServletResponse.setContentType("application/json;charset=UTF-8");
        httpServletResponse.getWriter().write(responseResult.toJsonString());
        httpServletResponse.getWriter().flush();
    }

}
