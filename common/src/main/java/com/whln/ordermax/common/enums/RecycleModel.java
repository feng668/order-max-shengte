package com.whln.ordermax.common.enums;

import cn.hutool.core.util.ObjectUtil;

public enum RecycleModel {
    CM_COMMODITY(
            "CM_COMMODITY",
            "CmCommodity",
            null,
            "商品",
            "cm_commodity",
            "bill_no",
            "com.whln.ordermax.data.domain.CmCommodity",
            "com.whln.ordermax.data.repository.CmCommodityRepository",
            null,
//            "whln.ordermax.data.mapper.TrDespatchAdvice",
//            "whln.ordermax.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.commodity.CmCommodityService"
    ),
    SUP_SUPPLIER(
            "SUP_SUPPLIER",
            "SupSupplier",
            null,
            "供应商",
            "sup_supplier",
            "bill_no",
            "com.whln.ordermax.data.domain.SupSupplier",
            "com.whln.ordermax.data.repository.SupSupplierRepository",
            null,
//            "whln.ordermax.data.mapper.TrDespatchAdvice",
//            "whln.ordermax.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.supplier.SupSupplierService"
    ),
    CU_CUSTOMER(
            "CU_CUSTOMER",
            "CuCustomer",
            null,
            "客户",
            "cu_customer",
            "bill_no",
            "com.whln.ordermax.data.domain.CuCustomer",
            "com.whln.ordermax.data.repository.CuCustomerRepository",
            null,
//            "whln.ordermax.data.mapper.TrDespatchAdvice",
//            "whln.ordermax.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.customer.CuCustomerService"
    ),
    PR_PRICING(
            "PR_PRICING",
            "PrPricingOrder",
            null,
            "核价单",
            "pr_pricing_order",
            "bill_no",
            "com.whln.ordermax.data.domain.PrPricingOrder",
            "com.whln.ordermax.data.repository.PrPricingOrderRepository",
            null,
//            "whln.ordermax.data.mapper.TrDespatchAdvice",
//            "whln.ordermax.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.pricingOrder.PrPricingOrderService"
    ),TR_DESPATCH_ADVICE(
            "TR_DESPATCH_ADVICE",
            "TrDespatchAdvice",
            "TrDespatchAdviceCommodity",
            "发运通知",
            "tr_despatch_advice",
            "transportation_no",
            "com.whln.ordermax.data.domain.TrDespatchAdvice",
            "com.whln.ordermax.data.repository.TrDespatchAdviceRepository",
            null,
//            "whln.ordermax.data.mapper.TrDespatchAdvice",
//            "whln.ordermax.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.transportation.TrDespatchAdviceService"
    ),
    TR_TRANSPORTATION(
            "TR_TRANSPORTATION",
            "TrTransportation",
            "TrTransportationCommodity",
            "出运计划",
            "tr_transportation",
            "transportation_no",
            "com.whln.ordermax.data.domain.TrTransportation",
            "com.whln.ordermax.data.repository.TrTransportationRepository",
            null,
            "com.whln.ordermax.data.mapper.TrTransportationMapper",
//            "whln.ordermax.data.mapper.TrTransportationCommodity"
//            null,
            null,
            "com.whln.ordermax.api.service.transportation.TrTransportationService"
    ),
    PD_PRODUCTION_TASK(
            "PD_PRODUCTION_TASK",
            "PdProductionTask",
            null,
            "生产任务单",
            "pd_production_task",
            "bill_no",
            "com.whln.ordermax.data.domain.PdProductionTask",
            "com.whln.ordermax.data.repository.PdProductionTaskRepository",
            null,
//            "whln.ordermax.data.mapper.PdProductionTask",
//            "whln.ordermax.data.mapper.PdProductionTaskCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.product.PdProductionTaskService"
    ),
    PD_QUALITY(
            "PD_QUALITY",
            "PdQuality",
            null,
            "质检单",
            "pd_quality",
            "bill_no",
            "com.whln.ordermax.data.domain.PdQuality",
            "com.whln.ordermax.data.repository.PdQualityRepository",
            null,
            null,
            null,
            "com.whln.ordermax.api.service.product.PdQualityService"
    ),
    //    acceptance
    ACCEPTANCE(
            "ACCEPTANCE",
            "Acceptance",
            null,
            "验收单",
            "pd_acceptance",
            "bill_no",
            "com.whln.ordermax.data.domain.PdAcceptance",
            "com.whln.ordermax.data.repository.AcceptanceRepository",
            null,
            null,
            null,
            "com.whln.ordermax.api.service.product.AcceptanceService"
    )
    ,
    PO_INVOICE(
            "PO_INVOICE",
            "PoInvoice",
            "PoInvoiceCommodity",
            "采购发票",
            "po_invoice",
            "request_no",
            "com.whln.ordermax.data.domain.PoInvoice",
            "com.whln.ordermax.data.repository.PoInvoiceRepository",
            null,
//            "whln.ordermax.data.mapper.PoInvoice",
//            "whln.ordermax.data.mapper.PoInvoiceCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.poOrder.PoInvoiceService"
    ),
    ST_IN_ORDER(
            "ST_IN_ORDER",
            "StInOrder",
            "StInOrderCommodity",
            "入库单",
            "st_in_order",
            "bill_no",
            "com.whln.ordermax.data.domain.StInOrder",
            "com.whln.ordermax.data.repository.StInOrderRepository",
            "销售合同.xlsx",
//            "whln.ordermax.data.mapper.StInOrder",
//            "whln.ordermax.data.mapper.StInOrderCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.stock.StInOrderService"
    ),
    SO_SO_ORDER(
            "SO_SO_ORDER",
            "SoSoOrder",
            "SoSoOrderCommodity",
            "外销合同",
            "so_so_order",
            "order_no",
            "com.whln.ordermax.data.domain.SoSoOrder",
            "com.whln.ordermax.data.repository.SoSoOrderRepository",
            "销售合同.xlsx",
            "com.whln.ordermax.data.mapper.SoSoOrderMapper",
            "com.whln.ordermax.data.mapper.SoSoOrderCommodityMapper",
            "com.whln.ordermax.api.service.soOrder.SoSoOrderService"
    ),
    SO_SI_ORDER(
            "SO_SI_ORDER",
            "SoSiOrder",
            "SoSiOrderCommodity",
            "内销合同",
            "so_si_order",
            "order_no",
            "com.whln.ordermax.data.domain.SoSiOrder",
            "com.whln.ordermax.data.repository.SoSiOrderRepository",
            "销售合同.xlsx",
//            "whln.ordermax.data.mapper.SoSiOrder",
//            "whln.ordermax.data.mapper.SoSiOrderCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.soOrder.SoSiOrderService"
    ),
    ST_OUT_ORDER(
            "ST_OUT_ORDER",
            "StOutOrder",
            "StOutOrderCommodity",
            "出库单",
            "st_out_order",
            "bill_no",
            "com.whln.ordermax.data.domain.StOutOrder",
            "com.whln.ordermax.data.repository.StOutOrderRepository",
            null,
            "com.whln.ordermax.data.mapper.StOutOrderMapper",
//            "whln.ordermax.data.mapper.StOutOrderCommodity"
//            null,
            null,
            "com.whln.ordermax.api.service.stock.StOutOrderService"
    ),
    EO_ENQUIRY_ORDER(
            "EO_ENQUIRY_ORDER",
            "EoEnquiryOrder",
            "EoEnquiryOrderCommodity",
            "询价单",
            "eo_enquiry_order",
            "bill_no",
            "com.whln.ordermax.data.domain.EoEnquiryOrder",
            "com.whln.ordermax.data.repository.EoEnquiryOrderRepository",
            null,
            "com.whln.ordermax.data.mapper.EoEnquiryOrderMapper",
//            "whln.ordermax.data.mapper.EoEnquiryOrderCommodity"
//            null,
            null,
            "com.whln.ordermax.api.service.enquiry.EoEnquiryOrderService"
    ),
    PO_CONTRACT(
            "PO_CONTRACT",
            "PoContract",
            "PoContractCommodity",
            "采购合同",
            "po_contract",
            "request_no",
            "com.whln.ordermax.data.domain.PoContract",
            "com.whln.ordermax.data.repository.PoContractRepository",
            null,
            "com.whln.ordermax.data.mapper.PoContractMapper",
//            "whln.ordermax.data.mapper.PoContractCommodity"
//            null,
            null,
            "com.whln.ordermax.api.service.poOrder.PoContractService"
    ),

    PO_REQUEST(
            "PO_REQUEST",
            "PoRequest",
            "PoRequestCommodity",
            "采购申请单",
            "po_request",
            "request_no",
            "com.whln.ordermax.data.domain.PoRequest",
            "com.whln.ordermax.data.repository.PoRequestRepository",
            null,
            "com.whln.ordermax.data.mapper.PoRequestMapper",
//            "whln.ordermax.data.mapper.PoRequestCommodity"
//            null,
            null,
            "com.whln.ordermax.api.service.poOrder.PoRequestService"
    ),

    TR_IN_DISPATCH(
            "TR_IN_DISPATCH",
            "TrInDispatch",
            "TrInDispatchCommodity",
            "内销发货单",
            "tr_in_dispatch",
            "transportation_no",
            "com.whln.ordermax.data.domain.TrInDispatch",
            "com.whln.ordermax.data.repository.TrInDispatchRepository",
            "销售合同.xlsx",
//            "whln.ordermax.data.mapper.TrInDispatch",
//            "whln.ordermax.data.mapper.TrInDispatchCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.transportation.TrInDispatchService"
    ),
    QO_QUOTE_ORDER(
            "QO_QUOTE_ORDER",
            "QoQuoteOrder",
            "QoQuoteCommodity",
            "外销报价单",
            "qo_quote_order",
            "quotation_no",
            "com.whln.ordermax.data.domain.QoQuoteOrder",
            "com.whln.ordermax.data.repository.QoQuoteOrderRepository",
            "报价单.xls",
//            "whln.ordermax.data.mapper.QoQuoteOrder",
//            "whln.ordermax.data.mapper.QoQuoteOrderCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.quote.QoQuoteOrderService"
    ),
    /* QO_QUOTE_ORDER_COMMODITY(
             "QO_QUOTE_ORDER",
             "QoQuoteOrder",
             "QoQuoteCommodity",
             "外销报价单",
             "qo_quote_order",
             "quotation_no",
             "com.whln.ordermax.data.domain.QoQuoteCommodity",
             "com.whln.ordermax.data.repository.QoQuoteCommodityRepository",
             "报价单.xls",
 //            "whln.ordermax.data.mapper.QoQuoteOrder",
 //            "whln.ordermax.data.mapper.QoQuoteOrderCommodity"
             null,
             null
     ),*/
    QO_IN_QUOTE_ORDER(
            "QO_IN_QUOTE_ORDER",
            "QoInQuoteOrder",
            "QoInQuoteOrderCommodity",
            "内销报价单",
            "qo_in_quote_order",
            "quotation_no",
            "com.whln.ordermax.data.domain.QoInQuoteOrder",
            "com.whln.ordermax.data.repository.QoInQuoteOrderRepository",
            null,
//            "whln.ordermax.data.mapper.QoInQuoteOrder",
//            "whln.ordermax.data.mapper.QoInQuoteOrderCommodity"
            null,
            null,
            "com.whln.ordermax.api.service.quote.QoInQuoteOrderService"
    ),
    TR_CUSTOMS_DECLARATION(
            "TR_CUSTOMS_DECLARATION",
            "TrCustomsDeclaration",
            "TrCustomsDeclarationCommodity",
            "报关单证",
            "tr_customs_declaration",
            "transportation_no",
            "com.whln.ordermax.data.domain.TrCustomsDeclaration",
            "com.whln.ordermax.data.repository.TrCustomsDeclarationRepository",
            "报关单证.xls",
            "com.whln.ordermax.data.mapper.TrCustomsDeclarationMapper",
//            "whln.ordermax.data.mapper.TrCustomsDeclarationCommodity"
//            null,
            null,
            "com.whln.ordermax.api.service.transportation.TrCustomsDeclarationService"
    );

    private String code;

    private String entityName;

    private String commodityEntityName;

    private String name;


    /**
     * 表名
     */
    private String tableName;

    /**
     * 编号属性
     */
    private String serialAttribute;

    /**
     * 实体类
     */
    private String entityClass;

    /**
     * 方法接口
     */
    private String repositoryClass;
    private String serviceClass;

    private String templatePath;

    private String mMapperXPath;

    private String cmMapperXPath;

    RecycleModel(String code, String entityName, String commodityEntityName, String name, String tableName, String serialAttribute, String entityClass, String repositoryClass, String templatePath, String mMapperXPath, String cmMapperXPath, String serviceClass) {
        this.serviceClass = serviceClass;
        this.code = code;
        this.entityName = entityName;
        this.commodityEntityName = commodityEntityName;
        this.name = name;
        this.tableName = tableName;
        this.serialAttribute = serialAttribute;
        this.entityClass = entityClass;
        this.repositoryClass = repositoryClass;
        this.templatePath = templatePath;
        this.mMapperXPath = mMapperXPath;
        this.cmMapperXPath = cmMapperXPath;
    }

    public String getCode() {
        return code;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getCommodityEntityName() {
        return commodityEntityName;
    }

    public String getName() {
        return name;
    }

    public String getTableName() {
        return tableName;
    }

    public String getSerialAttribute() {
        return serialAttribute;
    }

    public String getEntityClass() {
        return entityClass;
    }

    public String getRepositoryClass() {
        return repositoryClass;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public String getmMapperXPath() {
        return mMapperXPath;
    }

    public String getCmMapperXPath() {
        return cmMapperXPath;
    }
    public static RecycleModel getByCode(String code){
        RecycleModel[] values = values();
        for (RecycleModel m : values) {
            if (ObjectUtil.equals(m.getCode(), code)) {
                return m;
            }
        }
        return null;
    }
}
