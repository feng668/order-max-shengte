package com.whln.ordermax.common.enums;

/**
 * @author liurun
 */

public enum YesOrNo {
   YES("yes",1),
    NO("no", 0),
    ;
    private String name;
    private Integer state;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    YesOrNo(String name, Integer state) {
        this.name = name;
        this.state = state;
    }
}
