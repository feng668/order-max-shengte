package com.whln.ordermax.common.annotaions;

import com.whln.ordermax.common.enums.ModelMessage;

import java.lang.annotation.*;

/**
 * @author liurun
 * @date 2023-03-01 16:24
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BillCode {
    ModelMessage value();
}
