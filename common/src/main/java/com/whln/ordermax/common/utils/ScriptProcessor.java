package com.whln.ordermax.common.utils;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class ScriptProcessor {

    private ScriptProcessor() {
        ScriptEngineManager sem = new ScriptEngineManager();
        this.engine = sem.getEngineByExtension("js");
    }

    private ScriptEngine engine;

    private static class ScriptProcessorHolder {
        private static final ScriptProcessor INSTANCE = new ScriptProcessor();
    }

    public static ScriptProcessor getProcessor() {
        return ScriptProcessorHolder.INSTANCE;
    }

    public <T> T exec(String jsCode,Class<T> tClass){
        try {
            return (T) engine.eval(jsCode);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean execCondition(String conditionCode){
        return exec(conditionCode,Boolean.class);
    }

}
