package com.whln.ordermax.common.utils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class OmServletUtil {

    public static String getBody(HttpServletRequest request){
        try {
            return new String(new OmRequestWrapper(request).getRequestBody());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
