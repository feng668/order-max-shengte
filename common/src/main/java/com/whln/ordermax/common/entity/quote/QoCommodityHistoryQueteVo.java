package com.whln.ordermax.common.entity.quote;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@ApiModel("商品历史报价")
@Data
public class QoCommodityHistoryQueteVo {

    @ApiModelProperty("报价单编号")
    private String quotationNo;
    @ApiModelProperty("计量单位")
    private String quantityUnit;
    @ApiModelProperty("中文规格")
    private String specification;
    @ApiModelProperty("英文规格")
    private String enSpecification;
    @ApiModelProperty("报价日期")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate quotationDate;

    @ApiModelProperty("商品编号")
    private String partNo;

    @ApiModelProperty("商品中文名称")
    private String cnName;

    @ApiModelProperty("商品英文名")
    private String enName;
    @ApiModelProperty("供应商id")
    private String supplierId;

    @ApiModelProperty("净重")
    private BigDecimal cmNetWeight;


    @ApiModelProperty("价格")
    private BigDecimal price;
}
