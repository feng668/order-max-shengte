package com.whln.ordermax.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@ApiModel("分页接收模型")
@Data
@Slf4j
public class PageEntity<T>{

    @ApiModelProperty("当前页")
    private Integer current = 1;

    @ApiModelProperty("页大小")
    private Integer size = 10;

    private T entity;
}
