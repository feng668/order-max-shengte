package com.whln.ordermax.common.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-24 11:33
 */
public class CollUtil {
    public static <T> List<List<T>> collSub(List<T> collect, int pageSize) {
        List<List<T>> l = new ArrayList<>();
        int size = collect.size();
        int i = size % pageSize;
        int totalPage = size / pageSize;
        if (i > 0) {
            totalPage += 1;
        }
        for (int a = 1; a <= totalPage; a++) {
            int startSize = (a - 1) * pageSize;
            int toSize = startSize + pageSize;
            if (toSize > size) {
                toSize = size;
            }
            List<T> strings = collect.subList(startSize, toSize);
            l.add(strings);
        }
        return l;
    }
}
