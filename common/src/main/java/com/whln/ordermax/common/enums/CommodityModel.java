package com.whln.ordermax.common.enums;

//用于商品属性过滤,code必须与ModelMessage中对应的模块相等
public enum CommodityModel {

    ST_IN_ORDER(
            "ST_IN_ORDER",
            null,
            "StInOrderCommodity",
            "入库单商品",
            "st_in_order_commodity",
            "bill_id",
            "com.whln.dyx.data.domain.StInOrderCommodity",
            "com.whln.dyx.data.repository.StInOrderCommodityRepository",
            "销售合同.xlsx",
//            "whln.dyx.data.mapper.StInOrder",
//            "whln.dyx.data.mapper.StInOrderCommodity"
            null,
            null
    ),
    ST_OUT_ORDER(
            "ST_OUT_ORDER",
            null,
            "StOutOrderCommodity",
            "出库单商品",
            "st_out_order_commodity",
            "bill_id",
            "com.whln.dyx.data.domain.StOutOrderCommodity",
            "com.whln.dyx.data.repository.StOutOrderCommodityRepository",
            null,
            "com.whln.dyx.data.mapper.StOutOrderCommodityMapper",
//            "whln.dyx.data.mapper.StOutOrderCommodity"
//            null,
            null
    ),
    PD_PRODUCTION_TASK(
            "PD_PRODUCTION_TASK",
            null,
            "PdProductionCommodityBom",
            "生产任务单",
            "pd_production_task",
            "bill_no",
            "com.whln.dyx.data.domain.PdProductionTask",
            "com.whln.dyx.data.repository.PdProductionCommodityBomRepository",
            null,
//            "whln.dyx.data.mapper.PdProductionTask",
//            "whln.dyx.data.mapper.PdProductionTaskCommodity"
            "com.whln.ordermax.data.mapper.PdProductionTaskMapper",
            null
    ),
    PO_INVOICE(
            "PO_INVOICE",
            null,
            "PoInvoiceCommodity",
            "采购发票商品",
            "po_invoice_commodity",
            "request_no",
            "com.whln.ordermax.data.domain.PoInvoiceCommodity",
            "com.whln.dyx.data.repository.PoInvoiceCommodityRepository",
            null,
//            "whln.dyx.data.mapper.PoInvoice",
//            "whln.dyx.data.mapper.PoInvoiceCommodity"
            null,
            null
    ),
    PO_CONTRACT(
            "PO_CONTRACT",
            null,
            "PoContractCommodity",
            "采购合同商品",
            "po_contract_commodity",
            "bill_id",
            "com.whln.dyx.data.domain.PoContractCommodity",
            "com.whln.dyx.data.repository.PoContractCommodityRepository",
            null,
            "com.whln.dyx.data.mapper.PoContractMapper",
//            "whln.dyx.data.mapper.PoContractCommodity"
//            null,
            null
    ),
    TR_DESPATCH_ADVICE(
            "TR_DESPATCH_ADVICE",
            null,
            "TrDespatchAdviceCommodity",
            "发运通知",
            "tr_despatch_advice_commodity",
            "bill_id",
            "com.whln.ordermax.data.domain.TrDespatchAdviceCommodity",
            "com.whln.dyx.data.repository.TrDespatchAdviceCommodityRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    TR_CUSTOMS_DECLARATION(
            "TR_CUSTOMS_DECLARATION",
            null,
            "TrCustomsDeclarationCommodity",
            "报关单证",
            "tr_customs_declaration_commodity",
            "bill_id",
            "com.whln.ordermax.data.domain.TrCustomsDeclarationCommodity",
            "com.whln.dyx.data.repository.TrCustomsDeclarationCommodityRepository",
            "报关单证.xls",
            "com.whln.dyx.data.mapper.TrCustomsDeclarationCommodityMapper",
//            "whln.dyx.data.mapper.TrCustomsDeclarationCommodity"
//            null,
            null
    ),
    TR_TRANSPORTATION(
            "TR_TRANSPORTATION",
            null,
            "TrTransportationCommodity",
            "出运计划",
            "tr_transportation_commodity",
            "bill_id",
            "com.whln.ordermax.data.domain.TrTransportationCommodity",
            "com.whln.dyx.data.repository.TrTransportationCommodityRepository",
            null,
            "com.whln.dyx.data.mapper.TrTransportationMapper",
//            "whln.dyx.data.mapper.TrTransportationCommodity"
//            null,
            null
    ),
    TR_IN_DISPATCH(
            "TR_IN_DISPATCH",
            null,
            "TrInDispatchCommodity",
            "内销发货单",
            "tr_in_dispatch_commodity",
            "bill_id",
            "com.whln.ordermax.data.domain.TrInDispatchCommodity",
            "com.whln.dyx.data.repository.TrInDispatchCommodityRepository",
            "销售合同.xlsx",
//            "whln.dyx.data.mapper.TrInDispatch",
//            "whln.dyx.data.mapper.TrInDispatchCommodity"
            null,
            null
    ),
    SO_SI_ORDER(
            "SO_SI_ORDER",
            null,
            "SoSiOrderCommodity",
            "内销合同商品",
            "so_si_order_commodity",
            "bill_no",
            "com.whln.ordermax.data.domain.SoSiOrderCommodity",
            "com.whln.dyx.data.repository.SoSiOrderCommodityRepository",
            null,
//            "whln.dyx.data.mapper.SoSiOrder",
//            "whln.dyx.data.mapper.SoSiOrderCommodity"
            null,
            null
    ),
    SO_SI_INVOICE(
            "SO_SI_INVOICE",
            "SoSiInvoice",
            "SoSiInvoiceCommodity",
            "内销发票",
            "so_si_invoice",
            "billNo",
            "com.whln.ordermax.data.domain.SoSiInvoice",
            "com.whln.ordermax.data.repository.SoSiInvoiceRepository",
            "销售合同.xlsx",
//            "whln.ordermax.data.mapper.SoSiOrder",
//            "whln.ordermax.data.mapper.SoSiOrderCommodity"
            null,
            null
    ),
    QO_IN_QUOTE_ORDER(
            "QO_IN_QUOTE_ORDER",
            "QoInQuoteCommodity",
            "QoInQuoteCommodity",
            "内销报价单",
            "qo_in_quote_commodity",
            "bill_no",
            "com.whln.ordermax.data.domain.QoInQuoteCommodity",
            "com.whln.dyx.data.repository.QoInQuoteCommodityRepository",
            null,
//            "whln.dyx.data.mapper.QoInQuoteOrder",
//            "whln.dyx.data.mapper.QoInQuoteOrderCommodity"
            null,
            null
    ),
    LOGISTICS(
            "LOGISTICS",
            "ListLogistics",
            "ListLogistics",
            "物流查询",
            "ListLogistics",
            null,
            "com.whln.ordermax.data.domain.PrPricingOrder",
            "com.whln.ordermax.data.repository.PrPricingOrderRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    CM_COMMODITY(
            "CM_COMMODITY_PART",
            "CmCommodity",
            "CmCommodity",
            "货代公司",
            "cm_commodity",
            "part_no",
            "com.whln.ordermax.data.domain.CmCommodity",
            "com.whln.ordermax.data.repository.CmCommodityRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    QO_QUOTE_ORDER_COMMODITY(
            "QO_QUOTE_ORDER",
            "QoQuoteOrder",
            "QoQuoteCommodity",
            "外销报价单",
            "qo_quote_order",
            "quotation_no",
            "com.whln.ordermax.data.domain.QoQuoteCommodity",
            "com.whln.dyx.data.repository.QoQuoteCommodityRepository",
            null,
//            "whln.dyx.data.mapper.QoQuoteOrder",
//            "whln.dyx.data.mapper.QoQuoteOrderCommodity"
            null,
            null
    ),
    ;

    private String code;

    private String entityName;

    private String commodityEntityName;

    private String name;


    /**
     * 表名
     */
    private String tableName;

    /**
     * 编号属性
     */
    private String serialAttribute;

    /**
     * 实体类
     */
    private String entityClass;

    /**
     * 方法接口
     */
    private String repositoryClass;

    private String templatePath;

    private String mMapperXPath;

    private String cmMapperXPath;

    CommodityModel(String code, String entityName, String commodityEntityName, String name, String tableName, String serialAttribute, String entityClass, String repositoryClass, String templatePath, String mMapperXPath, String cmMapperXPath) {
        this.code = code;
        this.entityName = entityName;
        this.commodityEntityName = commodityEntityName;
        this.name = name;
        this.tableName = tableName;
        this.serialAttribute = serialAttribute;
        this.entityClass = entityClass;
        this.repositoryClass = repositoryClass;
        this.templatePath = templatePath;
        this.mMapperXPath = mMapperXPath;
        this.cmMapperXPath = cmMapperXPath;
    }

    public String getCode() {
        return code;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getCommodityEntityName() {
        return commodityEntityName;
    }

    public String getName() {
        return name;
    }

    public String getTableName() {
        return tableName;
    }

    public String getSerialAttribute() {
        return serialAttribute;
    }

    public String getEntityClass() {
        return entityClass;
    }

    public String getRepositoryClass() {
        return repositoryClass;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public String getmMapperXPath() {
        return mMapperXPath;
    }

    public String getCmMapperXPath() {
        return cmMapperXPath;
    }
}
