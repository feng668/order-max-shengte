package com.whln.ordermax.common.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.MD5;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;

@Slf4j
public class PasswordUtil {
    public static void main(String[] args) {
        String encode = PasswordUtil.encode("whln2023");
        boolean match = PasswordUtil.match("123456", "cd72ced51a147210cd21bc4549bac65a");
        System.out.println(encode);
    }
    public static MD5 md5 = new MD5();

    public PasswordUtil() {
    }

    public static String encode(String password) {
        byte[] bytes = password.getBytes(StandardCharsets.UTF_16LE);
        String result = md5.digestHex(bytes).concat(password);
        return md5.digestHex(result.getBytes(StandardCharsets.UTF_16LE));
    }

    public static boolean match(String password, String encodedPassword) {
        String hash = encode(password);

        return StrUtil.equalsAnyIgnoreCase(hash, new CharSequence[]{encodedPassword});
    }
}
