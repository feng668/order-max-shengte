package com.whln.ordermax.common.utils;

import cn.hutool.core.map.MapUtil;
import org.springframework.util.StreamUtils;
import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Map;


public class OmRequestWrapper extends HttpServletRequestWrapper {

    private  byte[] requestBody;

    private Map<String, String[]> parameterMap;

    public OmRequestWrapper(HttpServletRequest request) throws IOException {

        super(request);
        parameterMap = request.getParameterMap();
        requestBody = StreamUtils.copyToByteArray(request.getInputStream());

    }

    public String getBodyStr(){
        return new String(requestBody);
    }

    @Override
    public String[] getParameterValues(String name) {
        if(MapUtil.isNotEmpty(parameterMap)) {
            return parameterMap.get(name);
        }
        return null;
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return this.parameterMap;
    }

    @Override

    public BufferedReader getReader() {

        return new BufferedReader(new InputStreamReader(getInputStream(), StandardCharsets.UTF_8));

    }

    @Override

    public ServletInputStream getInputStream() {

        if (requestBody == null) {

            requestBody = new byte[0];

        }

        final ByteArrayInputStream bais = new ByteArrayInputStream(requestBody);

        return new ServletInputStream() {

            @Override

            public boolean isFinished() {

                return false;

            }

            @Override

            public boolean isReady() {

                return false;

            }

            @Override

            public void setReadListener(ReadListener readListener) {

            }

            @Override

            public int read() throws IOException {

                return bais.read();

            }

        };

    }

    public byte[] getRequestBody() {
        return requestBody;
    }
}