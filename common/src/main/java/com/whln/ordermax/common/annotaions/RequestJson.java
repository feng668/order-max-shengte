package com.whln.ordermax.common.annotaions;

/**
 * @author liurun
 * @date 2023-02-27 11:00
 */
public @interface RequestJson {
    /**
     * 字段名，不填则默认参数名
     *
     * @return
     */
    String fieldName() default "";

    /**
     * 默认值，不填则默认为null。
     *
     * @return
     */
    String defaultValue() default "";
}
