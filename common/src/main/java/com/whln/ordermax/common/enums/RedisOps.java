package com.whln.ordermax.common.enums;

/**
 * redis操作
 * @author liurun
 * @date 2023-04-11 18:48
 */
public enum RedisOps {
    /**
     * redis
     */
    SAVE(),
    DELETE(),
    BATCH_DELETE(),
    UPDATE(),
    SELECT(),

    ;
}
