package com.whln.ordermax.common.utils;

import cn.hutool.core.collection.CollectionUtil;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TreeUtil<T> {

    /*public static List<Map<String,Object>> getTree(List<Map<String,Object>> mapList){
        mapList = mapList.stream().map(MapUtil::toCamelCaseMap).collect(Collectors.toList());
        Map<String, List<Map<String,Object>>> parentIdMap = mapList.stream().collect(Collectors.groupingBy(p -> p.get("parentId")==null?"ROOT":p.get("parentId").toString()));
        for (Map map : mapList) {
            map.put("chirds",parentIdMap.get(map.get("id")));
            List<Map<String,Object>> parentList = parentIdMap.get(map.get("parentId")==null?"ROOT":map.get("parentId"));
            *//*if (!parentList.contains(sysPower)){
                parentList.add(sysPower);
            }*//*
            parentIdMap.put(map.get("parentId")==null?"ROOT":map.get("parentId").toString(),parentList);
        }
        return parentIdMap.get("ROOT");
    }*/

    /**
     * 将集合转换为属性结构
     * @param list 集合
     * @param <T> 实体类型,需带有id，parentId，chirds属性
     * @return 树形结构集合
     */
    public static  <T> List<T> getTree(List<T> list){

        Map<String, List<T>> parentIdMap = list.stream().collect(Collectors.groupingBy(p -> {
            String key = "ROOT";
            try {
                key = p.getClass().getMethod("getParentId").invoke(p) == null ? "ROOT" : p.getClass().getMethod("getParentId").invoke(p).toString();
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                e.printStackTrace();
            }
            return key;
        }));
        for (T t : list) {
            try {
                t.getClass().getMethod("setChirds",List.class).invoke(t,parentIdMap.get(t.getClass().getMethod("getId").invoke(t)));
                List<T> parentList = parentIdMap.get(t.getClass().getMethod("getParentId").invoke(t) == null ? "ROOT" : t.getClass().getMethod("getParentId").invoke(t).toString());
                parentIdMap.put(t.getClass().getMethod("getParentId").invoke(t) == null ? "ROOT" : t.getClass().getMethod("getParentId").invoke(t).toString(),parentList);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
        return parentIdMap.get("ROOT");
    }

    /**
     * 将集合转换为属性结构
     * @param treeList 树形结构集合
     * @param <T> 实体类型,需带有id，parentId，chirds属性
     * @return 集合
     */
    public static  <T> List<T> treeToList(List<T> treeList){
        List<T> allList = new ArrayList<>();
        while (CollectionUtil.isNotEmpty(treeList)){
            allList.addAll(treeList);
            List<T> indexList = new ArrayList<>();
            for (T t : treeList) {
                List<T> chirds = null;
                try {
                    chirds = (List<T>) t.getClass().getMethod("getChirds").invoke(t);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }

                if (CollectionUtil.isNotEmpty(chirds)){
                    indexList.addAll(chirds);
                }
            }
            treeList = indexList;
        }
        return allList;
    }
}
