package com.whln.ordermax.common.entity;


import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import com.whln.ordermax.common.enums.RequestError;

/**
 * @author liurun
 */
@ApiModel("响应模型")
@Getter
public class Result<T> {

    private String code;

    private String msg;

    private T data;

    /*返回成功的结果*/
    public static <T> Result<T> success(T data) {
        Result<T> result = new Result<>();
        result.code = "200";
        result.msg = "ok";
        result.data = data;
        return result;
    }

    public static <T> Result<T> success() {
        Result<T> result = new Result<>();
        result.code = "200";
        result.msg = "操作成功";
        return result;
    }
    public static <T> Result<T> success(String code ,String  msg ,T data) {
        Result<T> result = new Result<>();
        result.code = code;
        result.msg = msg;
        result.data = data;
        return result;
    }
    /*返回失败的结果*/
    public static <T> Result error() {
        Result<T> result = new Result<>();
        result.code = "201";
        result.msg = "操作失败";
        return result;
    }

    public static <T> Result<T> error(Integer code, String msg) {
        Result<T> result = new Result<>();
        result.code = code.toString();
        result.msg = msg;
        return result;
    }

    public static <T> Result<T> error(String code, String msg) {
        Result<T> result = new Result<>();
        result.code = code;
        result.msg = msg;
        return result;
    }

    public static <T> Result<T> error(String msg) {
        Result<T> result = new Result<>();
        result.code = "201";
        result.msg = msg;
        return result;
    }

    public static <T> Result error(RequestError error, String msg) {
        Result<T> result = new Result<>();
        result.code = error.getCode();
        result.msg = msg;
        return result;
    }

    public String toJsonString() {
        return JSONUtil.toJsonStr(this);
    }

    public String getStr(String key) {
        JSONObject dataObj = JSONUtil.parseObj(data);
        return dataObj.getStr(key);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
