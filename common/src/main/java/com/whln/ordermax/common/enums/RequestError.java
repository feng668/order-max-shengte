package com.whln.ordermax.common.enums;

public enum RequestError {
    SUCCESS("000000", "成功"),
    SYSTEM_EXCEPTION("000001", "系统异常"),
    NETWORK_EXCEPTION("000011", "网络异常"),
    NETWORK_TIMEOUT("000012", "网络请求超时"),
    DATABASE_EXCEPTION("000021", "数据库异常"),
    PARAM_MISSING("000101", "缺少参数"),
    PARAM_INVAILD("000102", "无效参数"),
    OPERATION_NOT_SUPPORT("000201", "不支持的操作"),
    METHOD_NOT_SUPPORT("000202", "不支持的方法"),
    ACCESS_DENIED("000401", "访问被拒绝"),
    BAD_CREDENTIAL("000402", "无效的凭证"),
    ACCOUNT_ERROR("000403", "账号错误"),
    PASSWORD_ERROR("000404", "密码错误"),
    FAIL("FFFFFF", "失败");

    private String code;
    private String msg;

    private RequestError(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }
}
