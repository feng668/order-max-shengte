package com.whln.ordermax.common.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

public class OmQrCodeUtil {
    private static String base64Url = "data:image/png;base64,";
    public static String createQRCode(String json) throws IOException, WriterException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();

        HashMap<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");

        BitMatrix bitMatrix = qrCodeWriter.encode(json, BarcodeFormat.QR_CODE, 90, 90, hints);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", outputStream);
        Base64.Encoder encoder = Base64.getEncoder();
        String text = encoder.encodeToString(outputStream.toByteArray());
        return base64Url + text;
    }
    public static void writeToStream(String content, String format, OutputStream os,int width, int height){
        try {
            //com.google.zxing.EncodeHintType：编码提示类型,枚举类型
            Map<EncodeHintType, Object> hints = new HashMap(3);

            //EncodeHintType.CHARACTER_SET：设置字符编码类型
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");

            //EncodeHintType.ERROR_CORRECTION：设置误差校正
            //ErrorCorrectionLevel：误差校正等级，L = ~7% correction、M = ~15% correction、Q = ~25% correction、H = ~30% correction
            //不设置时，默认为 L 等级，等级不一样，生成的图案不同，但扫描的结果是一样的
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);

            //EncodeHintType.MARGIN：设置二维码边距，单位像素，值越小，二维码距离四周越近

            hints.put(EncodeHintType.MARGIN,-1);
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(new String(content.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1), BarcodeFormat.QR_CODE, width, height,hints);
            MatrixToImageWriter.writeToStream(bitMatrix,format,os);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String writeToBase64(String content, String format,int width, int height){
        try(ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            //com.google.zxing.EncodeHintType：编码提示类型,枚举类型
            Map<EncodeHintType, Object> hints = new HashMap(3);

            //EncodeHintType.CHARACTER_SET：设置字符编码类型
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");

            //EncodeHintType.ERROR_CORRECTION：设置误差校正
            //ErrorCorrectionLevel：误差校正等级，L = ~7% correction、M = ~15% correction、Q = ~25% correction、H = ~30% correction
            //不设置时，默认为 L 等级，等级不一样，生成的图案不同，但扫描的结果是一样的
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);

            //EncodeHintType.MARGIN：设置二维码边距，单位像素，值越小，二维码距离四周越近

            hints.put(EncodeHintType.MARGIN, 1);
            BitMatrix bitMatrix = qrCodeWriter.encode(new String(content.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1), BarcodeFormat.QR_CODE, width, height,hints);
            MatrixToImageWriter.writeToStream(bitMatrix,format,out);
            byte[] bytes = out.toByteArray();
            return Base64.getEncoder().encodeToString(bytes);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
