package com.whln.ordermax.common.utils;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;


public class LogisticsUtil {
    private String token;

    public LogisticsUtil(String token) {
        this.token = token;
    }

    public List<JSONObject> traceSearch(String cpCode, String mailNo){
        try {
            Connection.Response response = Jsoup
                    .connect("https://eolink.o.apispace.com/wlgj1/paidtobuy_api/trace_search")
                    .method(Connection.Method.POST)
                    .ignoreContentType(true)
                    .headers(
                            new HashMap<String,String>(){{
                                put("X-APISpace-Token",token);
                                put("Authorization-Type","apikey");
                                put("Content-Type","application/json");
                            }}
                    )
                    .requestBody(
                            JSONUtil.toJsonStr(
                                    new HashMap<String,String>(){{
                                        put("cpCode",cpCode);
                                        put("mailNo",mailNo);
                                    }}
                            )
                    )
                    .execute();
            JSONObject bodyObj = JSONUtil.parseObj(response.body());
            if (bodyObj.getJSONObject("logisticsTrace")!=null){
                return bodyObj.getJSONObject("logisticsTrace").getBeanList("logisticsTraceDetailList",JSONObject.class);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<JSONObject> traceSearchInternational(String cpCode, String mailNo){
        try {
            //直接查询单号
            Connection.Response response = Jsoup
                    .connect("https://eolink.o.apispace.com/internationallogistics/trace")
                    .method(Connection.Method.POST)
                    .ignoreContentType(true)
                    .headers(
                            new HashMap<String,String>(){{
                                put("X-APISpace-Token",token);
                                put("Authorization-Type","apikey");
                                put("Content-Type","application/json");
                            }}
                    )
                    .requestBody(
                            JSONUtil.toJsonStr(
                                    new HashMap<String,String>(){{
                                        put("cpCode",cpCode);
                                        put("mailNo",mailNo);
                                    }}
                            )
                    )
                    .execute();
            JSONObject bodyObj = JSONUtil.parseObj(response.body());
            if (bodyObj.getJSONObject("internationalLogisticsTrace")!=null){
                return bodyObj.getJSONObject("logisticsTrace").getBeanList("logisticsTraceDetailList",JSONObject.class);
            }else {         //查不到则先订阅
                Jsoup
                        .connect("https://eolink.o.apispace.com/internationallogistics/subscribe")
                        .method(Connection.Method.POST)
                        .ignoreContentType(true)
                        .headers(
                                new HashMap<String,String>(){{
                                    put("X-APISpace-Token",token);
                                    put("Authorization-Type","apikey");
                                    put("Content-Type","application/json");
                                }}
                        )
                        .requestBody(
                                JSONUtil.toJsonStr(
                                        new HashMap<String,String>(){{
                                            put("cpCode",cpCode);
                                            put("mailNo",mailNo);
                                        }}
                                )
                        )
                        .execute();
                response = Jsoup
                        .connect("https://eolink.o.apispace.com/internationallogistics/trace")
                        .method(Connection.Method.POST)
                        .ignoreContentType(true)
                        .headers(
                                new HashMap<String,String>(){{
                                    put("X-APISpace-Token",token);
                                    put("Authorization-Type","apikey");
                                    put("Content-Type","application/json");
                                }}
                        )
                        .requestBody(
                                JSONUtil.toJsonStr(
                                        new HashMap<String,String>(){{
                                            put("cpCode",cpCode);
                                            put("mailNo",mailNo);
                                        }}
                                )
                        )
                        .execute();
                bodyObj = JSONUtil.parseObj(response.body());
                if (bodyObj.getJSONObject("internationalLogisticsTrace")!=null) {
                    return bodyObj.getJSONObject("logisticsTrace").getBeanList("logisticsTraceDetailList", JSONObject.class);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
