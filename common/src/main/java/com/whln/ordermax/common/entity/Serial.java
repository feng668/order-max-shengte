package com.whln.ordermax.common.entity;

import lombok.Data;

@Data
public class Serial {


    /**序列号*/
    private String serialNumber;
    /**公钥*/
    public static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCzASwbiuUx/Ebw5KPJwRLycBxDSWFwI+Es9ySMb6gbYnOUfOYxDStiM6rkZfwdCHLHMcp8YHxpV/0ggRVdjTN1/cae1hga90S1TIPyaMZlCQ2uES/NYz6Sc/b+1L1Akjm/Sgfn4NM2EXOFdX2e2dmd/xEFYaHYGyStjEbdHjJ9gQIDAQAB";
    /**私钥*/
    private String privateKey;
    /**有结束时间(13位时间戳)*/
    private String effectiveEndTime;
    /**MAC地址*/
    private String mac;
}
