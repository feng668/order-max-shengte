package com.whln.ordermax.common;

/**
 * @author liurun
 * @date 2023-03-11 9:42
 */
public interface LocalDateFormatConstants {
    String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    String DEFAULT_TIME_FORMAT = "HH:mm:ss";
}
