package com.whln.ordermax.common.enums;

public enum JudgeType {

    EQ("EQ", "等于","= '{}'","'{}' == '{}'"),
    NE("NE", "不等于","!= '{}'","'{}' != '{}'"),
    GT("GT", "大于","> '{}'","'{}' > '{}'"),
    GE("GE", "大于或等于",">= '{}'","'{}' >= '{}'"),
    LT("LT", "小于","< '{}'","'{}' < '{}'"),
    LE("LE", "小于或等于","<= '{}'","'{}' <= '{}'"),
    INCLUDE("INCLUDE" ,"包含","LIKE '%{}%'","'{}'.indexOf('{}')>-1"),
    NOT_INCLUDE("NOT_INCLUDE" ,"不包含","NOT LIKE '%{}%'","'{}'.indexOf('{}')==-1"),
    RANGE("RANGE" ,"范围","LIKE '{}%'","'{}'.startsWith('{}')"),
    IS_NULL("IS_NULL" ,"为空","IS NULL","{}"),
    IS_NOT_NULL("IS_NOT_NULL" ,"不为空","IS NOT NULL","{}"),
    BETWEEN("BETWEEN" ,"介于","BETWEEN '{}' AND '{}'","{}"),
    NOT_BETWEEN("NOT_BETWEEN" ,"不介于","NOT BETWEEN '{}' AND '{}'","{}"),
    IN("IN" ,"在列表","IN ({})","{}"),
    NOT_IN("NOT_IN" ,"不在列表","NOT IN ({})","{}");

    private String code;
    private String name;
    private String sqlTemp;
    private String jsTemp;

    JudgeType(String code, String name, String sqlTemp, String jsTemp) {
        this.code = code;
        this.name = name;
        this.sqlTemp = sqlTemp;
        this.jsTemp = jsTemp;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getSqlTemp() {
        return sqlTemp;
    }

    public String getJsTemp() {
        return jsTemp;
    }
}
