package com.whln.ordermax.common.entity;

import lombok.Data;

import java.util.List;

@Data
public class FilterCondition {

    private String joinType;

    private String attr;

    private String judgeType;

    private String value;

    private String attrSource;

    private List<FilterCondition> mutiConditionList;
}
