package com.whln.ordermax.common.enums;

public enum ModelMessage1 {
    FN_PROFIT(
            "FN_PROFIT",
            "FnProfit",
            null,
            "利润中心",
            "fn_profit",
            "transportation_id",
            "com.whln.ordermax.data.domain.FnProfit",
            "com.whln.ordermax.data.repository.FnProfitCostRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    COST_CENTER(
            "COST_CENTER",
            "TrTransportationCost",
            null,
            "费用中心",
            "tr_transportation_cost",
            "transportation_id",
            "com.whln.ordermax.data.domain.TrTransportationCost",
            "com.whln.ordermax.data.repository.TrTransportationCostRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    FN_PAYMENT(
            "FN_PAYMENT",
            "FnPayment",
            null,
            "付款单",
            "fn_payment",
            "order_id",
            "com.whln.ordermax.data.domain.FnPayment",
            "com.whln.ordermax.data.repository.FnPaymentRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    FN_IN_ACCOUNT(
            "FN_IN_ACCOUNT",
            "FnInAccount",
            null,
            "入账登记",
            "st_warehouse",
            "order_id",
            "com.whln.ordermax.data.domain.FnInAccount",
            "com.whln.ordermax.data.repository.FnInAccountRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    ST_WAREHOUSE(
            "ST_WAREHOUSE",
            "StWarehouse",
            null,
            "库存占用单",
            "st_warehouse",
            "order_id",
            "com.whln.ordermax.data.domain.StWarehouse",
            "com.whln.ordermax.data.repository.StWarehouseRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    ST_WAREHOUSE_GRAB(
            "ST_WAREHOUSE_GRAB",
            "StInventoryGrab",
            null,
            "库存占用单",
            "st_inventory_grab",
            "order_id",
            "com.whln.ordermax.data.domain.StInventoryGrab",
            "com.whln.ordermax.data.repository.StInventoryGrabRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    ST_WAREHOUSE_IMMEDIATE(
            "ST_WAREHOUSE_IMMEDIATE",
            "StInventory",
            null,
            "库存明细",
            "sup_supplier",
            "supplier_no",
            "com.whln.ordermax.data.domain.StInventory",
            "com.whln.ordermax.data.repository.StInventoryRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "getSoSiOrder"
    ),
    ST_WAREHOUSE_IMMEDIATE1(
            "ST_WAREHOUSE_IMMEDIATE",
            "StInventory",
            null,
            "库存明细",
            "sup_supplier",
            "supplier_no",
            "com.whln.ordermax.data.domain.StInventory",
            "com.whln.ordermax.data.repository.StInventoryRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "getSoSoOrder"
    ),
    ST_WAREHOUSE_IMMEDIATE2(
            "ST_WAREHOUSE_IMMEDIATE",
            "StInventory",
            null,
            "库存明细",
            "sup_supplier",
            "supplier_no",
            "com.whln.ordermax.data.domain.StInventory",
            "com.whln.ordermax.data.repository.StInventoryRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "getStInventory"
    ),
    ST_WAREHOUSE_TOTAL(
            "ST_WAREHOUSE_TOTAL",
            "StInventory",
            null,
            "库存明细",
            "sup_supplier",
            "supplier_no",
            "com.whln.ordermax.data.domain.StInventory",
            "com.whln.ordermax.data.repository.StInventoryRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "pageTotalByStageType"
    ),
    ST_WAREHOUSE_DETAIL(
            "ST_WAREHOUSE_DETAIL",
            "StInventory",
            null,
            "库存明细",
            "sup_supplier",
            "supplier_no",
            "com.whln.ordermax.data.domain.StInventory",
            "com.whln.ordermax.data.repository.StInventoryRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null,
            "page"
    ),
    SUP_SUPPLIER(
            "SUP_SUPPLIER",
            "SupSupplier",
            "SupSupplier",
            "供应商",
            "sup_supplier",
            "supplier_no",
            "com.whln.ordermax.data.domain.SupSupplier",
            "com.whln.ordermax.data.repository.SupSupplierRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    PR_PRICING_ORDER(
            "PR_PRICING_ORDER",
            "PrPricingOrder",
            null,
            "商品分类",
            "pr_pricing_order",
            null,
            "com.whln.ordermax.data.domain.PrPricingOrder",
            "com.whln.ordermax.data.repository.PrPricingOrderRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    CM_COMMODITY_CATEGORY(
            "CM_COMMODITY_CATEGORY",
            "CmCommodityCategory",
            null,
            "商品分类",
            "cm_commodity_category",
            null,
            "com.whln.ordermax.data.domain.CmCommodityCategory",
            "com.whln.ordermax.data.repository.CmCommodityCategoryRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    CU_CUSTOMER(
            "CU_CUSTOMER",
            "CuCustomer",
            null,
            "货代公司",
            "cu_customer",
            "customer_no",
            "com.whln.ordermax.data.domain.CuCustomer",
            "com.whln.ordermax.data.repository.CuCustomerRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),   CM_COMMODITY(
            "CM_COMMODITY",
            "CmCommodity",
            "CmCommodity",
            "货代公司",
            "cm_commodity",
            "part_no",
            "com.whln.ordermax.data.domain.CmCommodity",
            "com.whln.ordermax.data.repository.CmCommodityRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    FORWARDER_COMPANY(
            "PHY_FORWARDER_COMPANY",
            "PhyForwarderCompany",
            null,
            "货代公司",
            "phy_forwarder_company",
            "forwarder_company_no",
            "com.whln.ordermax.data.domain.PhyForwarderCompany",
            "com.whln.ordermax.data.repository.PhyForwarderCompanyRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    TR_DESPATCH_ADVICE(
            "TR_DESPATCH_ADVICE",
            "TrDespatchAdvice",
            "TrDespatchAdviceCommodity",
            "发运通知",
            "tr_despatch_advice",
            "transportation_no",
            "com.whln.dyx.data.domain.TrDespatchAdvice",
            "com.whln.dyx.data.repository.TrDespatchAdviceRepository",
            null,
//            "whln.dyx.data.mapper.TrDespatchAdvice",
//            "whln.dyx.data.mapper.TrDespatchAdviceCommodity"
            null,
            null
    ),
    TR_TRANSPORTATION(
            "TR_TRANSPORTATION",
            "TrTransportation",
            "TrTransportationCommodity",
            "出运计划",
            "tr_transportation",
            "transportation_no",
            "com.whln.dyx.data.domain.TrTransportation",
            "com.whln.dyx.data.repository.TrTransportationRepository",
            null,
            "com.whln.dyx.data.mapper.TrTransportationMapper",
//            "whln.dyx.data.mapper.TrTransportationCommodity"
//            null,
            null
    ),
    PD_PRODUCTION_TASK(
            "PD_PRODUCTION_TASK",
            "PdProductionTask",
            null,
            "生产任务单",
            "pd_production_task",
            "bill_no",
            "com.whln.dyx.data.domain.PdProductionTask",
            "com.whln.dyx.data.repository.PdProductionTaskRepository",
            null,
//            "whln.dyx.data.mapper.PdProductionTask",
//            "whln.dyx.data.mapper.PdProductionTaskCommodity"
            null,
            null
    ),
    PD_QUALITY(
            "PD_QUALITY",
            "PdQuality",
            null,
            "质检单",
            "pd_quality",
            "bill_no",
            "com.whln.dyx.data.domain.PdQuality",
            "com.whln.dyx.data.repository.PdQualityRepository",
            null,
            null,
            null
    ),
    //    acceptance
    ACCEPTANCE(
            "ACCEPTANCE",
            "Acceptance",
            null,
            "验收单",
            "acceptance",
            "bill_no",
            "com.whln.dyx.data.domain.Acceptance",
            "com.whln.dyx.data.repository.AcceptanceRepository",
            null,
            null,
            null
    )
    ,
    PO_INVOICE(
            "PO_INVOICE",
            "PoInvoice",
            "PoInvoiceCommodity",
            "采购发票",
            "po_invoice",
            "request_no",
            "com.whln.dyx.data.domain.PoInvoice",
            "com.whln.dyx.data.repository.PoInvoiceRepository",
            null,
//            "whln.dyx.data.mapper.PoInvoice",
//            "whln.dyx.data.mapper.PoInvoiceCommodity"
            null,
            null
    ),
    ST_IN_ORDER(
            "ST_IN_ORDER",
            "StInOrder",
            "StInOrderCommodity",
            "入库单",
            "st_in_order",
            "bill_no",
            "com.whln.dyx.data.domain.StInOrder",
            "com.whln.dyx.data.repository.StInOrderRepository",
            "销售合同.xlsx",
//            "whln.dyx.data.mapper.StInOrder",
//            "whln.dyx.data.mapper.StInOrderCommodity"
            null,
            null
    ),
    SO_SO_ORDER(
            "SO_SO_ORDER",
            "SoSoOrder",
            "SoSoOrderCommodity",
            "外销合同",
            "so_so_order",
            "order_no",
            "com.whln.dyx.data.domain.SoSoOrder",
            "com.whln.dyx.data.repository.SoSoOrderRepository",
            "销售合同.xlsx",
            "com.whln.dyx.data.mapper.SoSoOrderMapper",
            "com.whln.dyx.data.mapper.SoSoOrderCommodityMapper"
    ),
    SO_SI_ORDER(
            "SO_SI_ORDER",
            "SoSiOrder",
            "SoSiOrderCommodity",
            "内销合同",
            "so_si_order",
            "order_no",
            "com.whln.dyx.data.domain.SoSiOrder",
            "com.whln.dyx.data.repository.SoSiOrderRepository",
            "销售合同.xlsx",
//            "whln.dyx.data.mapper.SoSiOrder",
//            "whln.dyx.data.mapper.SoSiOrderCommodity"
            null,
            null
    ), SO_SI_INVOICE(
            "SO_SI_INVOICE",
            "SoSiInvoice",
            "SoSiInvoiceCommodity",
            "内销发票",
            "so_si_invoice",
            "billNo",
            "com.whln.ordermax.data.domain.SoSiInvoice",
            "com.whln.ordermax.data.repository.SoSiInvoiceRepository",
            "销售合同.xlsx",
//            "whln.ordermax.data.mapper.SoSiOrder",
//            "whln.ordermax.data.mapper.SoSiOrderCommodity"
            null,
            null
    ),
    ST_OUT_ORDER(
            "ST_OUT_ORDER",
            "StOutOrder",
            "StOutOrderCommodity",
            "出库单",
            "st_out_order",
            "bill_no",
            "com.whln.dyx.data.domain.StOutOrder",
            "com.whln.dyx.data.repository.StOutOrderRepository",
            null,
            "com.whln.dyx.data.mapper.StOutOrderMapper",
//            "whln.dyx.data.mapper.StOutOrderCommodity"
//            null,
            null
    ),
    EO_ENQUIRY_ORDER(
            "EO_ENQUIRY_ORDER",
            "EoEnquiryOrder",
            "EoEnquiryOrderCommodity",
            "询价单",
            "eo_enquiry_order",
            "bill_no",
            "com.whln.dyx.data.domain.EoEnquiryOrder",
            "com.whln.dyx.data.repository.EoEnquiryOrderRepository",
            null,
            "com.whln.dyx.data.mapper.EoEnquiryOrderMapper",
//            "whln.dyx.data.mapper.EoEnquiryOrderCommodity"
//            null,
            null
    ),
    PO_CONTRACT(
            "PO_CONTRACT",
            "PoContract",
            "PoContractCommodity",
            "采购合同",
            "po_contract",
            "request_no",
            "com.whln.dyx.data.domain.PoContract",
            "com.whln.dyx.data.repository.PoContractRepository",
            null,
            "com.whln.dyx.data.mapper.PoContractMapper",
//            "whln.dyx.data.mapper.PoContractCommodity"
//            null,
            null
    ),

    PO_REQUEST(
            "PO_REQUEST",
            "PoRequest",
            "PoRequestCommodity",
            "采购申请单",
            "po_request",
            "request_no",
            "com.whln.dyx.data.domain.PoRequest",
            "com.whln.dyx.data.repository.PoRequestRepository",
            null,
            "com.whln.dyx.data.mapper.PoRequestMapper",
//            "whln.dyx.data.mapper.PoRequestCommodity"
//            null,
            null
    ),

    TR_IN_DISPATCH(
            "TR_IN_DISPATCH",
            "TrInDispatch",
            "TrInDispatchCommodity",
            "内销发货单",
            "tr_in_dispatch",
            "transportation_no",
            "com.whln.dyx.data.domain.TrInDispatch",
            "com.whln.dyx.data.repository.TrInDispatchRepository",
            "销售合同.xlsx",
//            "whln.dyx.data.mapper.TrInDispatch",
//            "whln.dyx.data.mapper.TrInDispatchCommodity"
            null,
            null
    ),
    QO_QUOTE_ORDER(
            "QO_QUOTE_ORDER",
            "QoQuoteOrder",
            "QoQuoteCommodity",
            "外销报价单",
            "qo_quote_order",
            "quotation_no",
            "com.whln.dyx.data.domain.QoQuoteOrder",
            "com.whln.dyx.data.repository.QoQuoteOrderRepository",
            "报价单.xls",
//            "whln.dyx.data.mapper.QoQuoteOrder",
//            "whln.dyx.data.mapper.QoQuoteOrderCommodity"
            null,
            null
    ),
    /* QO_QUOTE_ORDER_COMMODITY(
             "QO_QUOTE_ORDER",
             "QoQuoteOrder",
             "QoQuoteCommodity",
             "外销报价单",
             "qo_quote_order",
             "quotation_no",
             "com.whln.ordermax.data.domain.QoQuoteCommodity",
             "com.whln.dyx.data.repository.QoQuoteCommodityRepository",
             "报价单.xls",
 //            "whln.dyx.data.mapper.QoQuoteOrder",
 //            "whln.dyx.data.mapper.QoQuoteOrderCommodity"
             null,
             null
     ),*/
    QO_IN_QUOTE_ORDER(
            "QO_IN_QUOTE_ORDER",
            "QoInQuoteOrder",
            "QoInQuoteOrderCommodity",
            "内销报价单",
            "qo_in_quote_order",
            "quotation_no",
            "com.whln.dyx.data.domain.QoInQuoteOrder",
            "com.whln.dyx.data.repository.QoInQuoteOrderRepository",
            null,
//            "whln.dyx.data.mapper.QoInQuoteOrder",
//            "whln.dyx.data.mapper.QoInQuoteOrderCommodity"
            null,
            null
    ),
    TR_CUSTOMS_DECLARATION(
            "TR_CUSTOMS_DECLARATION",
            "TrCustomsDeclaration",
            "TrCustomsDeclarationCommodity",
            "报关单证",
            "tr_customs_declaration",
            "transportation_no",
            "com.whln.dyx.data.domain.TrCustomsDeclaration",
            "com.whln.dyx.data.repository.TrCustomsDeclarationRepository",
            "报关单证.xls",
            "com.whln.dyx.data.mapper.TrCustomsDeclarationMapper",
//            "whln.dyx.data.mapper.TrCustomsDeclarationCommodity"
//            null,
            null
    );

    private String code;

    private String entityName;

    private String commodityEntityName;

    private String name;


    /**
     * 表名
     */
    private String tableName;

    /**
     * 编号属性
     */
    private String serialAttribute;

    /**
     * 实体类
     */
    private String entityClass;

    /**
     * 方法接口
     */
    private String repositoryClass;

    private String templatePath;

    private String mMapperXPath;

    private String cmMapperXPath;
    private String methodName;

    ModelMessage1(String code, String entityName, String commodityEntityName, String name, String tableName, String serialAttribute, String entityClass, String repositoryClass, String templatePath, String mMapperXPath, String cmMapperXPath) {
        this.code = code;
        this.entityName = entityName;
        this.commodityEntityName = commodityEntityName;
        this.name = name;
        this.tableName = tableName;
        this.serialAttribute = serialAttribute;
        this.entityClass = entityClass;
        this.repositoryClass = repositoryClass;
        this.templatePath = templatePath;
        this.mMapperXPath = mMapperXPath;
        this.cmMapperXPath = cmMapperXPath;
    }
    ModelMessage1(String code, String entityName, String commodityEntityName, String name, String tableName, String serialAttribute, String entityClass, String repositoryClass, String templatePath, String mMapperXPath, String cmMapperXPath,String methodName) {
        this.code = code;
        this.entityName = entityName;
        this.commodityEntityName = commodityEntityName;
        this.name = name;
        this.tableName = tableName;
        this.serialAttribute = serialAttribute;
        this.entityClass = entityClass;
        this.repositoryClass = repositoryClass;
        this.templatePath = templatePath;
        this.mMapperXPath = mMapperXPath;
        this.cmMapperXPath = cmMapperXPath;
        this.methodName = methodName;
    }
    public String getCode() {
        return code;
    }

    public String getEntityName() {
        return entityName;
    }

    public String getCommodityEntityName() {
        return commodityEntityName;
    }

    public String getName() {
        return name;
    }

    public String getTableName() {
        return tableName;
    }

    public String getSerialAttribute() {
        return serialAttribute;
    }

    public String getEntityClass() {
        return entityClass;
    }

    public String getRepositoryClass() {
        return repositoryClass;
    }

    public String getTemplatePath() {
        return templatePath;
    }

    public String getmMapperXPath() {
        return mMapperXPath;
    }

    public String getCmMapperXPath() {
        return cmMapperXPath;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public void setCommodityEntityName(String commodityEntityName) {
        this.commodityEntityName = commodityEntityName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public void setSerialAttribute(String serialAttribute) {
        this.serialAttribute = serialAttribute;
    }

    public void setEntityClass(String entityClass) {
        this.entityClass = entityClass;
    }

    public void setRepositoryClass(String repositoryClass) {
        this.repositoryClass = repositoryClass;
    }

    public void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public void setmMapperXPath(String mMapperXPath) {
        this.mMapperXPath = mMapperXPath;
    }

    public void setCmMapperXPath(String cmMapperXPath) {
        this.cmMapperXPath = cmMapperXPath;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
