package com.whln.ordermax.common.utils;

import java.io.File;
import java.io.FileWriter;

public class OmFileUtil {

    public static Boolean writeStr(String filePath,String context) {
        FileWriter writer;
        Boolean isWriteSuccess = false;
        try {
            File file = new File(filePath);
            if (!file.exists()){
                isWriteSuccess &= file.createNewFile();
            }
            writer = new FileWriter(filePath);
            writer.write(context);
            writer.flush();
            writer.close();
            isWriteSuccess = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isWriteSuccess;
    }

    public static Boolean deleteFile(String filePath) {
        Boolean isDelete;
        try {
            File file = new File(filePath);
            if (!file.exists()){
                return true;
            }
            file.delete();
            isDelete = true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return isDelete;
    }
}
