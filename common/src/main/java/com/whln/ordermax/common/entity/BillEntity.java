package com.whln.ordermax.common.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("单据系统属性")
public class BillEntity {

    @ApiModelProperty("单据编码")
    private String code;

    @ApiModelProperty("单据分类名称")
    private String name;

    /** 表名 */
    private String tableName;

    /** 编号属性 */
    private String serialAttribute;

    /** 实体类 */
    private Class entityClass;

    /** 方法接口 */
    private Class repositoryClass;

    /** 下级单据 */
    private List<BillEntity> underEntity;

    private String templatePath;

    public BillEntity() {
    }

    public BillEntity(String code, String name, String tableName, String serialAttribute, Class entityClass, Class repositoryClass) {
        this.code = code;
        this.name = name;
        this.tableName = tableName;
        this.serialAttribute = serialAttribute;
        this.entityClass = entityClass;
        this.repositoryClass = repositoryClass;
    }

    public BillEntity(String code, String name, String tableName, String serialAttribute, Class entityClass, Class repositoryClass, String templatePath) {
        this.code = code;
        this.name = name;
        this.tableName = tableName;
        this.serialAttribute = serialAttribute;
        this.entityClass = entityClass;
        this.repositoryClass = repositoryClass;
        this.underEntity = underEntity;
        this.templatePath = templatePath;
    }
}
