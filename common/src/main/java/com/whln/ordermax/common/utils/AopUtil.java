package com.whln.ordermax.common.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

public class AopUtil {

    public static Object getArgByName(ProceedingJoinPoint pjp,String argName){
        Object[] args = pjp.getArgs();
        LocalVariableTableParameterNameDiscoverer u = new LocalVariableTableParameterNameDiscoverer();
        String[] paramNames = u.getParameterNames(((MethodSignature) pjp.getSignature()).getMethod());
        for (int i = 0; i < paramNames.length; i++) {
            if (argName.equals(paramNames[i])) {
                Object entity = args[i];
                return entity;
            }
        }
        return null;
    }
}
