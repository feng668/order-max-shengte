package com.whln.ordermax.common.enums;

import cn.hutool.core.util.ObjectUtil;

import java.util.Arrays;
import java.util.Optional;

/**
 *
 * @author liurun
 * @date 2023-02-21 9:49
 */
public enum ModuleEnum {
    /**
     * 打印模板相关
     */
    TR_DESPATCH_ADVICE("发运通知","TR_DESPATCH_ADVICE","class whln.dyx.data.repository.TrDespatchAdviceRepository"),
    TR_TRANSPORTATION("出运计划","TR_TRANSPORTATION","class whln.dyx.data.repository.TrTransportationRepository"),
    PD_PRODUCTION_TASK("生产任务单","PD_PRODUCTION_TASK","class whln.dyx.data.repository.PdProductionTaskRepository"),
    PO_INVOICE("采购发票","PO_INVOICE","class whln.dyx.data.repository.PoInvoiceRepository"),
    ST_IN_ORDER("入库单","ST_IN_ORDER","class whln.dyx.data.repository.StInOrderRepository"),
    SO_SO_ORDER("外销合同","SO_SO_ORDER","class whln.dyx.data.repository.SoSoOrderRepository"),
    SO_SI_ORDER("内销合同","SO_SI_ORDER","class whln.dyx.data.repository.SoSiOrderRepository"),
    ST_OUT_ORDER("出库单","ST_OUT_ORDER","class whln.dyx.data.repository.StOutOrderRepository"),
    EO_ENQUIRY_ORDER("询价单","EO_ENQUIRY_ORDER","class whln.dyx.data.repository.EoEnquiryOrderRepository"),
    PO_CONTRACT("采购合同","PO_CONTRACT","class whln.dyx.data.repository.PoContractRepository"),
    PO_REQUEST("采购申请单","PO_REQUEST","class whln.dyx.data.repository.PoRequestRepository"),
    TR_IN_DISPATCH("内销发货单","TR_IN_DISPATCH","class whln.dyx.data.repository.TrInDispatchRepository"),
    QO_QUOTE_ORDER("外销报价单","QO_QUOTE_ORDER","class whln.dyx.data.repository.QoQuoteOrderRepository"),
    QO_IN_QUOTE_ORDER("内销报价单","QO_IN_QUOTE_ORDER","class whln.dyx.data.repository.QoInQuoteOrderRepository"),
    TR_CUSTOMS_DECLARATION("报关单证","TR_CUSTOMS_DECLARATION","class whln.dyx.data.repository.TrCustomsDeclarationRepository"),



    ;
    /**
     * 模块名
     */
    private String moduleName;
    /**
     * 对应模块名的值
     */
    private String value;
    private String repositoryClass;

    ModuleEnum(String moduleName, String value, String repositoryClass) {
        this.moduleName = moduleName;
        this.value = value;
        this.repositoryClass = repositoryClass;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRepositoryClass() {
        return repositoryClass;
    }

    public void setRepositoryClass(String repositoryClass) {
        this.repositoryClass = repositoryClass;
    }
    public static String getRepositoryClassByValue(String value){
        ModuleEnum[] values = values();
        Optional<ModuleEnum> first = Arrays.stream(values)
                .filter(item -> ObjectUtil.equal(item.getValue(), value))
                .findFirst();
        return first.get().getRepositoryClass();
    }
}
