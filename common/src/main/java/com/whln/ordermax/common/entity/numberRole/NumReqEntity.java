package com.whln.ordermax.common.entity.numberRole;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

@ApiModel("编号规则请求模型")
@Data
public class NumReqEntity {
    @ApiModelProperty("规则单据id")
    private String nrBillId;

    @ApiModelProperty("属性数据")
    private Map<String,Object> attrData;

    @ApiModelProperty("生成的编号数量")
    private Integer creatNum;

}
