package com.whln.ordermax.common.utils;


import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import com.whln.ordermax.common.entity.Serial;

/**
 * 序列号工具类
 * @author yuanzf
 *
 */
public class SerialUtil {

    /**
     * 根据序列号、公钥校验序列号是否有效
     * @param serialNumber 序列号
     * @return
     */
    public static Serial explainSerialNumber(String serialNumber) {
        Serial serial = new Serial();
        String[] serialList = SecureUtil.rsa(null,Serial.publicKey).decryptStrFromBcd(serialNumber,KeyType.PublicKey).split(",");
        serial.setMac(serialList[0]);
        serial.setEffectiveEndTime(serialList[1]);
        return serial;

    }
}
