package com.whln.ordermax.common.annotaions;

import com.whln.ordermax.common.enums.RedisKey;
import com.whln.ordermax.common.enums.RedisOps;

import java.lang.annotation.*;

/**
 * @author liurun
 * @date 2023-03-21 16:12
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Cache {
    /**
     * 超时时间
     */
    long timeout() default -1L;

    String value() default "";
    /**
     * 要操作的参数名，如要保存entity  或者删除时的id
     */
    String cacheParam();

    /**
     * 缓存的key
     */
    RedisKey key();

    /**
     * 操作类型
     */
    RedisOps ops();
}
