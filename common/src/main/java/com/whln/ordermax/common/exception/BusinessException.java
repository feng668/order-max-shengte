package com.whln.ordermax.common.exception;

/**
 * @author liurun
 * @date 2023-02-20 10:20
 */
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 8225449964207346072L;

    public BusinessException(String message){
        super(message);
    }
}
