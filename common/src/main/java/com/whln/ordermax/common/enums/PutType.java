package com.whln.ordermax.common.enums;

/**
 * 入库类型
 *
 * @author liurun
 * @date 2023-04-25 16:16
 */
public enum PutType {
    PUR("PUR",1),
    DEFAULT("DEFAULT",1),

    ;
    private String code;
    private Integer type;

    PutType(String code, Integer type) {
        this.code = code;
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
