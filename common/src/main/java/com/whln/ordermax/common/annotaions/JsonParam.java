package com.whln.ordermax.common.annotaions;

import java.lang.annotation.*;

/**
 * @author liurun
 * @date 2023-02-27 10:42
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JsonParam {
    String value() default "";
    String name() default "";
    boolean required() default true;
    String defaultValue() default "";
}
