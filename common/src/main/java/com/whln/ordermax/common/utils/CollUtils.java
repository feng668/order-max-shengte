package com.whln.ordermax.common.utils;

import java.util.*;

/**
 * @author liurun
 * @date 2023-02-21 15:01
 */
public class CollUtils {
    @SafeVarargs
    public static  <T> Set<T> toSet(T...args){
        return new HashSet<>(Arrays.asList(args));
    }
    @SafeVarargs
    public static  <T> Collection<T> toCollection(T...args){
        return new HashSet<>(Arrays.asList(args));
    }
    public static <T> List<List<T>> collSub(List<T> collect, int pageSize) {
        List<List<T>> l = new ArrayList<>();
        int size = collect.size();
        int totalPage = size / pageSize;
        int i = size % pageSize;
        if (i > 0) {
            totalPage += 1;
        }
        for (int a = 1; a <= totalPage; a++) {
            int startSize = (a - 1) * pageSize;
            int toSize = startSize + pageSize;
            if (toSize > size) {
                toSize = size;
            }
            List<T> strings = collect.subList(startSize, toSize);
            l.add(strings);
        }
        return l;
    }
}
