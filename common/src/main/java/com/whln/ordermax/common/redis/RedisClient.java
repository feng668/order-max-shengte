package com.whln.ordermax.common.redis;

import cn.hutool.core.util.ObjectUtil;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author liurun
 */
@SuppressWarnings("unused")
@Component
public class RedisClient<K, V> {

    @Lazy
    @Resource
    private RedisTemplate<K, V> redisTemplate;

    private static final double SIZE = Math.pow(2, 32);


    /**
     * 写入缓存
     *
     * @param key
     * @param offset 位 8Bit=1Byte
     * @return
     */
    public boolean setBit(String key, long offset, boolean isShow) {

        try {
            ValueOperations<K, V> operations = redisTemplate.opsForValue();
            operations.setBit(key, offset, isShow);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    /**
     * 写入缓存
     *
     * @param key
     * @param offset
     * @return
     */
    public Boolean getBit(K key, long offset) {
        try {
            ValueOperations<K, V> operations = redisTemplate.opsForValue();
            return operations.getBit(key, offset);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * 写入缓存
     *
     * @param key
     * @param value
     * @return
     */
    public boolean set(final K key, V value) {
        try {
            ValueOperations<K, V> operations = redisTemplate.opsForValue();
            operations.set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 写入缓存设置时效时间
     *
     * @param key 键
     * @param value 值
     * @param expireTime 过期时间  单位毫秒
     * @return 是否成功
     */
    public boolean set(final K key, V value, Long expireTime) {
        try {
            ValueOperations<K, V> operations = redisTemplate.opsForValue();
            operations.set(key, value, expireTime, TimeUnit.MILLISECONDS);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 批量删除对应的value
     *
     * @param keys 键  多个
     */
    @SafeVarargs
    public final void remove(final K... keys) {
        for (K key : keys) {
            remove(key);
        }
    }


    /**
     * 删除对应的value
     *
     * @param key
     */
    public void remove(final K key) {
        if (exists(key)) {
            redisTemplate.delete(key);
        }
    }

    /**
     * 判断缓存中是否有对应的value
     *
     * @param key
     * @return boolean 是否存在
     */
    public Boolean exists(final K key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 读取缓存
     *
     * @param key
     * @return
     */
    public V get(final K key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 哈希 添加
     *
     * @param key
     * @param hashKey
     * @param value
     */
    public void hmSet(K key, Object hashKey, V value) {
        HashOperations<K, Object, V> hash = redisTemplate.opsForHash();
        hash.put(key, hashKey, value);
    }

    /**
     * 哈希获取数据
     *
     * @param key
     * @param hashKey
     * @return
     */
    public V hmGet(K key, Object hashKey) {
        HashOperations<K, Object, V> hash = redisTemplate.opsForHash();
        return hash.get(key, hashKey);
    }

    /**
     * 列表添加
     *
     */
    public void rPush(K k, V v) {
        ListOperations<K, V> list = redisTemplate.opsForList();
        list.rightPush(k, v);
    }
    public void rPushAll(K k, List<V> vList) {
        ListOperations<K, V> list = redisTemplate.opsForList();
        list.rightPushAll(k, vList);
    }
    /**
     * 列表获取
     *
     * @param start 开始索引
     * @param end   结束下标   -1 表示最后一个
     * @return
     */
    public List<V> lRange(K k, long start, long end) {
        ListOperations<K, V> list = redisTemplate.opsForList();
        return list.range(k, start, end);
    }

    /*
     * 修改list
     * index：要修改的元素的索引
     * value：将要修改的元素改成这个值
     */
    public void lChange(K key, long index, V value) {
        redisTemplate.opsForList().set(key, index, value);
    }

    /*
     * 删除list元素
     * value：要删除的元素的值
     * count：删除的元素个数。
     * 当count=0，删除list中所有值为value的元素；
     * 当count>0，从头开始，删除list中值为value的元素，删除的个数为count个
     * 当count<0，从尾开始，删除list中值为value的元素，删除的个数为|count|(count的绝对值)个
     */
    public void lDelete(K key, int count, V value) {
        redisTemplate.opsForList().remove(key, count, value);
    }

    /**
     * 集合添加
     *
     * @param key
     * @param value
     */
    public void add(K key, V value) {
        SetOperations<K, V> set = redisTemplate.opsForSet();
        set.add(key, value);
    }

    /**
     * 集合获取
     *
     * @param key
     * @return
     */
    public Set<V> setMembers(K key) {
        SetOperations<K, V> set = redisTemplate.opsForSet();
        return set.members(key);
    }

    /**
     * 有序集合添加
     *
     * @param key
     * @param value
     * @param scoure
     */
    public void zAdd(K key, V value, double scoure) {
        ZSetOperations<K, V> zset = redisTemplate.opsForZSet();
        zset.add(key, value, scoure);
    }

    /**
     * 有序集合获取
     *
     * @param key
     * @param scoure
     * @param scoure1
     * @return
     */
    public Set<V> rangeByScore(K key, double scoure, double scoure1) {
        ZSetOperations<K, V> zset = redisTemplate.opsForZSet();
        redisTemplate.opsForValue();
        return zset.rangeByScore(key, scoure, scoure1);
    }

    /**
     * 有序集合获取排名
     *
     * @param key   集合名称
     * @param value 值
     */
    public Long zRank(K key, V value) {
        ZSetOperations<K, V> zset = redisTemplate.opsForZSet();
        return zset.rank(key, value);
    }


    /**
     * 有序集合获取排名
     *
     * @param key
     */
    public Set<ZSetOperations.TypedTuple<V>> zRankWithScore(K key, long start, long end) {
        ZSetOperations<K, V> zset = redisTemplate.opsForZSet();
        Set<ZSetOperations.TypedTuple<V>> ret = zset.rangeWithScores(key, start, end);
        return ret;
    }

    /**
     * 有序集合添加
     *
     * @param key
     * @param value
     */
    public Double zSetScore(K key, V value) {
        ZSetOperations<K, V> zset = redisTemplate.opsForZSet();
        return zset.score(key, value);
    }


    /**
     * 有序集合添加分数
     *
     * @param key
     * @param value
     * @param scoure
     */
    public void incrementScore(K key, V value, double scoure) {
        ZSetOperations<K, V> zset = redisTemplate.opsForZSet();
        zset.incrementScore(key, value, scoure);
    }


    /**
     * 有序集合获取排名
     *
     * @param key
     */
    public Set<ZSetOperations.TypedTuple<V>> reverseZRankWithScore(K key, long start, long end) {
        ZSetOperations<K, V> zset = redisTemplate.opsForZSet();
        Set<ZSetOperations.TypedTuple<V>> ret = zset.reverseRangeByScoreWithScores(key, start, end);
        return ret;
    }

    /**
     * 有序集合获取排名
     *
     * @param key
     */
    public Set<ZSetOperations.TypedTuple<V>> reverseZRankWithRank(K key, long start, long end) {
        ZSetOperations<K, V> zset = redisTemplate.opsForZSet();
        Set<ZSetOperations.TypedTuple<V>> ret = zset.reverseRangeWithScores(key, start, end);
        return ret;
    }


    public void refresh(K s, Long userTokenTimeout) {
        redisTemplate.expire(s, userTokenTimeout, TimeUnit.SECONDS);
    }

    public void flushAll(K k) {
        Set<K> keys = redisTemplate.keys(k);
        if (ObjectUtil.isNotEmpty(keys)){
            redisTemplate.delete(keys);
        }
    }
}
