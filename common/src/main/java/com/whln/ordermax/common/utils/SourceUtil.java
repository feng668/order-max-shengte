package com.whln.ordermax.common.utils;

import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.apache.commons.io.IOUtils;

import java.io.IOException;

public class SourceUtil {

    public static JSONObject parseJson(String path){
        try {
            return JSONUtil.parseObj(IOUtils.toString(new ClassPathResource(path).getStream(),"utf-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    public static JSONObject getModelMap(){
        return parseJson("static/model.json");
    }
}
