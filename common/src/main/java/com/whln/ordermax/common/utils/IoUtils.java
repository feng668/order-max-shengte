package com.whln.ordermax.common.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * @author liurun
 * @date 2023-02-22 10:30
 */
public class IoUtils {
    public static void write(OutputStream outputStream, String content) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(outputStream);
        writer.write(content);
        writer.close();
    }

}
