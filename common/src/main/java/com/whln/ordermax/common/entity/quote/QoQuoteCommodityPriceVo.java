package com.whln.ordermax.common.entity.quote;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* 报价单商品价格
* @TableName qo_quote_commodity_price
*/
@ApiModel("报价单商品价格")
@Data
public class QoQuoteCommodityPriceVo {

//    @TableId(type = IdType.ASSIGN_ID)
    /**
     * 报价单id
     */
    @ApiModelProperty("报价单id")
    private String quoteId;
    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    private String commodityId;
    /**
     * 价位id
     */
    @ApiModelProperty("价位id")
    private String priceLevelId;
    /**
     * 价格
     */
    @ApiModelProperty("价格")
    private Double price;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 客户id
     */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
     * 报价日期
     */
    @ApiModelProperty("报价日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate quotationDate;

    @ApiModelProperty("价位名称|回显")
    private String priceLevelName;

    @ApiModelProperty("价位编码|回显")
    private String priceLevelCoding;

    private static final long serialVersionUID = 1L;
}
