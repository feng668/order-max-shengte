package com.whln.ordermax.common.enums;

/**
 * redis的键
 * @author liurun
 * @date 2023-04-11 18:14
 */
public enum RedisKey {
    SYS_ROLE_ALL("所有角色", "SYS_ROLE_ALL"),
    ORDER_PLANE("orderPlane","ORDER_PLANE"),
    ORDER_PLAND("orderPland","ORDER_PLAND")
    ;
    private String name;
    private String key;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    RedisKey(String name, String key) {
        this.name = name;
        this.key = key;
    }
}
