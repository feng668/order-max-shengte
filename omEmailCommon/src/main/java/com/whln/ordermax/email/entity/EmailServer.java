package com.whln.ordermax.email.entity;

public class EmailServer {

    private String receiveAddress;//邮件接收地址
    private String sendAddress;//邮件发送地址

    public EmailServer(String receiveAddress, String sendAddress) {
        this.receiveAddress = receiveAddress;
        this.sendAddress = sendAddress;
    }

    public String getSendAddress() {
        return sendAddress;
    }

    public String getReceiveAddress() {
        return receiveAddress;
    }
}
