package com.whln.ordermax.email.mail;

import java.util.Date;

/**
 * 
 * @author shengte
 *
 */
public class EmailInfo {

	private String emailcode; // ID
	private String sender;
	private String Title;// 转码后的标题
	private String receiver;// 收件人
	private Date accepttime;// 收件日期
	private String msgBody;
	private int Checkstatus;

	public String getMsgBody() {
		return msgBody;
	}

	public void setMsgBody(String msgBody) {
		this.msgBody = msgBody;
	}

	public int getCheckstatus() {
		return Checkstatus;
	}

	public void setCheckstatus(int checkstatus) {
		Checkstatus = checkstatus;
	}

	public String getEmailcode() {
		return emailcode;
	}

	public void setEmailcode(String emailcode) {
		this.emailcode = emailcode;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public Date getAccepttime() {
		return accepttime;
	}

	public void setAccepttime(Date accepttime) {
		this.accepttime = accepttime;
	}

	@Override
	public String toString() {
		return "emailcode" + this.emailcode + "; sender:" + this.sender + "";
	}

}
