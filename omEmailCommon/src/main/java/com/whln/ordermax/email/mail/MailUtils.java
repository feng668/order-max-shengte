package com.whln.ordermax.email.mail;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.MimeUtility;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;


public class MailUtils {

	/**
	 * * 得到邮件正文内容 邮件的正文可能是多种类型:
	 * <ul>
	 * <li>text/plain</li>
	 * <li>text/html</li>
	 * <li>multipart/alternative</li>
	 * <li>multipart/related:内有内嵌的文件,噢噢~</li>
	 * <li>mutilpart/*</li>
	 * <li>message/rfc822</li>
	 * </ul>
	 * @param part
	 * @param userName
	 * @return
	 */
	public static String getBody(Part part, String userName) {
		StringBuffer sb = new StringBuffer();
		sb.append("");
		try {
			System.out.println("The type of the part is:" + part.getContentType());
			if (part.isMimeType("text/plain") || part.isMimeType("text/html")) {
				sb.append(part.getContent());
			} else if (part.isMimeType("multipart/*")) {
				if (part.isMimeType("multipart/alternative")) {
					Multipart mp = (Multipart) part.getContent();
					int index = 0;// 兼容不正确的格式,返回第一个部分
					if (mp.getCount() > 1)
						index = 1;// 第2个部分为html格式的哦~
					System.out.println("Now will choose the index(start from 0):" + index);
					Part tmp = mp.getBodyPart(index);
					sb.append(tmp.getContent());
				} else if (part.isMimeType("multipart/related")) {
					Multipart mp = (Multipart) part.getContent();
					Part tmp = mp.getBodyPart(0);
					String body = MailUtils.getBody(tmp, userName);
					int count = mp.getCount();
					for (int k = 1; count > 1 && k < count; k++) {
						Part att = mp.getBodyPart(k);
						String attname = att.getFileName();
						attname = MimeUtility.decodeText(attname);
						try {
							File attFile = new File(Constant.tomcat_AttHome_Key, userName.concat(attname));
							FileOutputStream fileoutput = new FileOutputStream(attFile);
							InputStream is = att.getInputStream();
							BufferedOutputStream outs = new BufferedOutputStream(fileoutput);
							byte b[] = new byte[att.getSize()];
							is.read(b);
							outs.write(b);
							outs.close();
						} catch (Exception e) {
							System.out.println("Error occurred when to get the photos from server");
						}
						String Content_ID[] = att.getHeader("Content-ID");
						if (Content_ID != null && Content_ID.length > 0) {
							String cid_name = Content_ID[0].replaceAll("<", "").replaceAll(">", "");
							body = body.replaceAll("cid:" + cid_name,
									Constant.server_attHome_Key.concat("/").concat(userName.concat(attname)));
						}
					}

					sb.append(body);
					return sb.toString();
				} else {
					/**
					 * 其他multipart/*格式的如mixed格式,那么第一个部分包含了body,用递归解析第一个部分就可以了
					 */
					Multipart mp = (Multipart) part.getContent();
					Part tmp = mp.getBodyPart(0);
					return MailUtils.getBody(tmp, userName);
				}
			} else if (part.isMimeType("message/rfc822")) {
				return MailUtils.getBody((Message) part.getContent(), userName);
			} else {
				/**
				 * 否则的话,死马当成活马医,直接解析第一部分,呜呜~
				 */
				Object obj = part.getContent();
				if (obj instanceof String) {
					sb.append(obj);
				} else {
					Multipart mp = (Multipart) obj;
					Part tmp = mp.getBodyPart(0);
					return MailUtils.getBody(tmp, userName);
				}
			}
		} catch (Exception e) {
			return "解析正文错误!";
		}
		return sb.toString();
	}

	public static String getBody(Message msg, String userName) {
		return MailUtils.getBody((Part) msg, userName);
	}

	/**
	 * 判断一封邮件是否含有附件
	 * @param msg
	 * @return
	 */
	public static boolean hasAttachment(Message msg) {
		Part part = (Part) msg;
		try {
			if (part.isMimeType("multipart/*")) {
				Multipart mp = (Multipart) part.getContent();
				int count = mp.getCount();
				for (int i = 0; i < count; i++) {
					Part tmp = mp.getBodyPart(i);
					if (MailUtils.isAttachment(tmp))
						return true;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * 判断一个邮件体是否是附件
	 * @param part
	 * @return
	 */
	public static boolean isAttachment(Part part) {
		try {
			String disposition = part.getDisposition();
			if ((disposition != null) && ((disposition.equals(Part.ATTACHMENT)) || (disposition.equals(Part.INLINE))))
				return true;
		} catch (Exception e) {
			String contentType = "";
			try {
				contentType = part.getContentType();
			} catch (MessagingException e1) {
				return false;
			}
			if (contentType.startsWith("application/octet-stream"))
				return true;
			return false;
		}
		return false;
	}
}
