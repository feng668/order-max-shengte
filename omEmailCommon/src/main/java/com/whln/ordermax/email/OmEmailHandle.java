package com.whln.ordermax.email;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.whln.ordermax.email.entity.EmailInfo;
import com.whln.ordermax.email.entity.MyEmailAttachment;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.SearchTerm;
import javax.mail.search.SentDateTerm;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.Security;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class OmEmailHandle {
    /**
     * @param from           发件人邮箱地址
     * @param password       发件人邮箱授权码
     * @param fromName       发件人名称
     * @param toList         群发地址
     * @param ccList         抄送人地址
     * @param bccList        密送人地址
     * @param subject        主题
     * @param htmlMsg        正文
     * @param attachmentList 附件
     * @param sendServer     邮件服务器
     * @throws EmailException
     */
    public void sendSLLMail(String from, String password, String fromName, List<String> toList, List<String> ccList, List<String> bccList, String subject, String htmlMsg, List<EmailAttachment> attachmentList, String sendServer) throws EmailException {

        if (toList == null || toList.size() == 0 || subject == null || subject == null) {
            throw new EmailException();
        }
        Properties properties = System.getProperties();
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.fallback", "false");
        properties.setProperty("mail.smtp.auth", "true");
        try {
            HtmlEmail email = new HtmlEmail();
            String[] tos = toList.toArray(new String[toList.size()]);

            email.setHostName(sendServer);
            email.setSSLOnConnect(true);
            email.setSmtpPort(465);
            // 字符编码集的设置
            email.setCharset("utf-8");
            // 收件人的邮箱
            email.addTo(tos);
            if (CollectionUtil.isNotEmpty(ccList)) {
                email.addCc(ccList.toArray(new String[ccList.size()]));
            }
            if (CollectionUtil.isNotEmpty(bccList)) {
                email.addBcc(bccList.toArray(new String[bccList.size()]));
            }

            // 发送人的邮箱以及发件人名称
            email.setFrom(from, fromName);
            // 如果需要认证信息的话，设置认证：用户名-密码。分别为发件人在邮件服务器上的注册名称和密码
            email.setAuthentication(from, password);
            // 要发送的邮件主题
            email.setSubject(subject);
            // 要发送的信息，由于使用了HtmlEmail，可以在邮件内容中使用HTML标签
            email.setHtmlMsg(htmlMsg);
            // 添加附件
            if (attachmentList != null && attachmentList.size() > 0) {
                for (EmailAttachment attachment : attachmentList) {
                    email.attach(attachment);
                }
            }


            String result1 = email.send();
            log.info(result1);

        } catch (Exception e) {
            e.printStackTrace();
            throw new EmailException();
        }
    }

    /**
     * 验证邮箱发送服务器可用
     *
     * @param smtpServer   smtp服务器
     * @param emailAddress 邮箱地址
     * @param password     邮箱授权码
     * @throws MessagingException
     */
    public void verifySmtpConnect(String smtpServer, String emailAddress, String password) throws MessagingException {
        //初始化默认参数
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.host", smtpServer);
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.from", emailAddress);
        //获取Session对象
        Session session = Session.getInstance(props, null);
        //开启后有调试信息
        session.setDebug(true);

        //通过MimeMessage来创建Message接口的子类
        MimeMessage message = new MimeMessage(session);
        Transport transport = session.getTransport();
        transport.connect(emailAddress, password);
    }

    /**
     * 验证邮箱接收服务器可用
     *
     * @param popServer    pop服务器
     * @param emailAddress 邮箱地址
     * @param password     邮箱授权码
     * @throws MessagingException
     */
    public void verifyPopConnect(String popServer, String emailAddress, String password) throws MessagingException {
        // 准备链接服务器的会话信息
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
//        OAuth2SaslClientFactory.init();
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        Properties props = System.getProperties();
        props.setProperty("mail.pop3.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.pop3.socketFactory.fallback", "false");
        props.setProperty("mail.pop3.port", "465");
        props.setProperty("mail.pop3.socketFactory.port", "995");
        // 建立Session实例对象
        Session session = Session.getInstance(props);
        URLName urln = new URLName("pop3", popServer, 465, null, emailAddress, password);
        Store store = session.getStore(urln);
        store.connect();
    }

    /**
     * 通过邮件地址获取邮件域名
     *
     * @param address 邮件地址
     * @return 邮件域名
     */
    public String getEmailDomainByAddress(String address) {
        Pattern p = Pattern.compile("^\\w+@(\\w+.[a-z]+)$");
        Matcher m = p.matcher(address);
        while (m.find()) {
            return String.valueOf(m.group(1));
        }
        return null;
    }

    /**
     * 单个邮箱邮件同步
     *
     * @param address       邮箱地址
     * @param password      邮箱授权码
     * @param fileRootPath  文件保存根路径
     * @param recieveServer 接收服务器
     * @param syncDays      同步时间范围
     * @return
     * @throws Exception
     */
    public List<EmailInfo> getPopSSLInBoxByTime(String address, String password, String fileRootPath, String recieveServer, Integer syncDays) throws Exception {

        // 准备链接服务器的会话信息
        Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
//        OAuth2SaslClientFactory.init();
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        Properties props = System.getProperties();
        props.setProperty("mail.pop3.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.pop3.socketFactory.fallback", "false");
        props.setProperty("mail.pop3.port", "465");
        props.setProperty("mail.pop3.socketFactory.port", "995");

        // 建立Session实例对象
        Session session = Session.getInstance(props);
        URLName urln = new URLName("pop3", recieveServer, 465, null, address, password);
        // 建立IMAP协议的Store对象

        Store store = session.getStore(urln);
        try {
            store.connect();
        } catch (AuthenticationFailedException e) {
            log.error("无法连接到邮件平台：{}", address);
            e.printStackTrace();
        }

        //按时间检索邮件
        SearchTerm st = new SentDateTerm(ComparisonTerm.GE, Date.from(LocalDateTime.now().minusDays(syncDays).atZone(ZoneId.of("UTC")).toInstant()));
        // 得到收件箱
        Folder folder = store.getFolder("INBOX");
        // 以读写模式打开收件箱
        folder.open(Folder.READ_ONLY);
        // 得到收件箱的邮件列表
        Message[] messages = folder.search(st);
//        Message[] messages = folder.getMessages();
        log.info("收件箱中{}天内共{}封邮件!", syncDays, messages.length);
        log.info("收件箱中共{}封未读邮件!", folder.getUnreadMessageCount());
        log.info("收件箱中共{}封新邮件!", folder.getNewMessageCount());
        log.info("收件箱中共{}封已删除邮件!", folder.getDeletedMessageCount());
        log.info("------------------------开始解析邮件----------------------------------");
        List<EmailInfo> emailInfoList = parseMessage(fileRootPath, address, Arrays.asList(messages));
        log.info("------------------------邮件解析结束----------------------------------");

        // 关闭资源
        folder.close(false);
        store.close();
        return emailInfoList;
    }


    /**
     * 解析邮件
     *
     * @param messages 要解析的邮件列表
     */
    private List<EmailInfo> parseMessage(String rootPath, String emailAddress, List<Message> messages) throws MessagingException, IOException {
        if (messages == null || messages.size() < 1) {
            throw new MessagingException("未找到要解析的邮件!");
        }
        List<EmailInfo> emailInfoList = new ArrayList<>();
        for (Message m : messages) {
            MimeMessage msg = (MimeMessage) m;
            EmailInfo emailInfo = new EmailInfo();
            if (msg != null) {
                try {
                    //获取messageId
                    emailInfo.setMessageId(msg.getMessageID());
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
                try {
                    //获取主题
                    emailInfo.setTheme(getSubject(msg));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
                String fromAddress = null;
                try {
                    //获取发件人邮箱
                    fromAddress = getFromAddress(msg);
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                String fromPersonal = null;
                try {
                    //获取发件人名称
                    fromPersonal = getFromPersonal(msg);
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray recived = getRecived(msg);
                    emailInfo.setRecivedArr(recived);
                    System.out.println();
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                emailInfo.setOppositeEmail(fromAddress);
                emailInfo.setOppositeSendName(fromPersonal);
                StringBuffer content = new StringBuffer(30);
                try {
                    getMailTextContent(msg, content);
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //获取邮件正文
                String contentStr = new String(content);
                String smallContentStr = StrUtil.sub(HtmlUtil.cleanHtmlTag(contentStr), 0, 40);
                emailInfo.setBodyText(smallContentStr);
//                emailInfo.setBodyText(contentStr);
                try {
                    String bodyPath = saveBodyFile(contentStr, rootPath, emailAddress, msg.getMessageID(), "body");
                    emailInfo.setBodyPath(bodyPath);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
                try {
                    if (ObjectUtil.isNotEmpty(msg.getSentDate())) {
                        emailInfo.setReceiveTime(LocalDateTime.ofInstant(msg.getSentDate().toInstant(), ZoneId.systemDefault()));
                    }
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
                boolean isContainerAttachment = false;
                try {
                    isContainerAttachment = isContainAttachment(msg);
                } catch (MessagingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (isContainerAttachment) {
                    List<MyEmailAttachment> myEmailAttachments = null;//保存附件
                    try {
                        myEmailAttachments = saveAttachment(msg, rootPath, emailAddress, msg.getMessageID());
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    emailInfo.setAttachmentList(myEmailAttachments);
                }
            }
            emailInfoList.add(emailInfo);
        }
        return emailInfoList;
    }

    /**
     * 获得邮件主题
     *
     * @param msg 邮件内容
     * @return 解码后的邮件主题
     */
    private String getSubject(MimeMessage msg) throws UnsupportedEncodingException, MessagingException {
        return MimeUtility.decodeText(msg.getSubject());
    }


    private JSONArray getRecived(MimeMessage msg) throws MessagingException, UnsupportedEncodingException {
        String from = "";
        Address[] sendArr = msg.getRecipients(Message.RecipientType.TO);
        Address[] ccArr = msg.getRecipients(Message.RecipientType.CC);
        Address[] bccArr = msg.getRecipients(Message.RecipientType.BCC);
//        List<JSONObject> result = new ArrayList<>();
        JSONArray result = new JSONArray();
        if (sendArr != null && sendArr.length > 0) {
            for (Address address : sendArr) {
                InternetAddress internetAddress = (InternetAddress) address;
                result.add(new JSONObject() {{
                    put("address", internetAddress.getAddress());
                    put("type", "SEND");
                }});
            }
        }
        if (ccArr != null && ccArr.length > 0) {
            for (Address address : ccArr) {
                InternetAddress internetAddress = (InternetAddress) address;
                result.add(new JSONObject() {{
                    put("address", internetAddress.getAddress());
                    put("type", "CC");
                }});
            }
        }
        if (bccArr != null && bccArr.length > 0) {
            for (Address address : bccArr) {
                InternetAddress internetAddress = (InternetAddress) address;
                result.add(new JSONObject() {{
                    put("address", internetAddress.getAddress());
                    put("type", "BCC");
                }});
            }
        }

        return result;
    }


    private String getFromAddress(MimeMessage msg) throws MessagingException, UnsupportedEncodingException {
        Address[] froms = msg.getFrom();
        if (froms.length < 1)
            throw new MessagingException("没有发件人!");
        InternetAddress address = (InternetAddress) froms[0];
        return address.getAddress();
    }

    private String getFromPersonal(MimeMessage msg) throws MessagingException, UnsupportedEncodingException {
        Address[] froms = msg.getFrom();
        if (froms.length < 1)
            throw new MessagingException("没有发件人!");

        InternetAddress address = (InternetAddress) froms[0];
        String person = address.getPersonal();

        if (person != null)
            try {
                person = MimeUtility.decodeText(person);
            } catch (UnsupportedEncodingException e) {
                person = "";
            }
        else
            person = "";

        return person;
    }


    /**
     * 判断邮件中是否包含附件
     *
     * @return 邮件中存在附件返回true，不存在返回false
     * @throws MessagingException
     * @throws IOException
     */
    private boolean isContainAttachment(Part part) throws MessagingException, IOException {
        boolean flag = false;
        if (part.isMimeType("multipart/*")) {
            MimeMultipart multipart = (MimeMultipart) part.getContent();
            int partCount = multipart.getCount();
            for (int i = 0; i < partCount; i++) {
                BodyPart bodyPart = multipart.getBodyPart(i);
                String disp = bodyPart.getDisposition();
                if (disp != null && (disp.equalsIgnoreCase(Part.ATTACHMENT) || disp.equalsIgnoreCase(Part.INLINE))) {
                    flag = true;
                } else if (bodyPart.isMimeType("multipart/*")) {
                    flag = isContainAttachment(bodyPart);
                } else {
                    String contentType = bodyPart.getContentType();
                    if (contentType.indexOf("application") != -1) {
                        flag = true;
                    }

                    if (contentType.indexOf("name") != -1) {
                        flag = true;
                    }
                }

                if (flag) break;
            }
        } else if (part.isMimeType("message/rfc822")) {
            flag = isContainAttachment((Part) part.getContent());
        }
        return flag;
    }

    /**
     * 判断邮件是否已读
     *
     * @param msg 邮件内容
     * @return 如果邮件已读返回true, 否则返回false
     * @throws MessagingException
     */
    private boolean isSeen(MimeMessage msg) throws MessagingException {
        return msg.getFlags().contains(Flags.Flag.SEEN);
    }

    /**
     * 判断邮件是否需要阅读回执
     *
     * @param msg 邮件内容
     * @return 需要回执返回true, 否则返回false
     * @throws MessagingException
     */
    private boolean isReplySign(MimeMessage msg) throws MessagingException {
        boolean replySign = false;
        String[] headers = msg.getHeader("Disposition-Notification-To");
        if (headers != null)
            replySign = true;
        return replySign;
    }

    /**
     * 获得邮件的优先级
     *
     * @param msg 邮件内容
     * @return 1(High):紧急  3:普通(Normal)  5:低(Low)
     * @throws MessagingException
     */
    private String getPriority(MimeMessage msg) throws MessagingException {
        String priority = "普通";
        String[] headers = msg.getHeader("X-Priority");
        if (headers != null) {
            String headerPriority = headers[0];
            if (headerPriority.indexOf("1") != -1 || headerPriority.indexOf("High") != -1)
                priority = "紧急";
            else if (headerPriority.indexOf("5") != -1 || headerPriority.indexOf("Low") != -1)
                priority = "低";
            else
                priority = "普通";
        }
        return priority;
    }

    /**
     * 获得邮件文本内容
     *
     * @param part    邮件体
     * @param content 存储邮件文本内容的字符串
     * @throws MessagingException
     * @throws IOException
     */
    private void getMailTextContent(Part part, StringBuffer content) throws MessagingException, IOException {
        //如果是文本类型的附件，通过getContent方法可以取到文本内容，但这不是我们需要的结果，所以在这里要做判断
        boolean isContainTextAttach = part.getContentType().indexOf("name") > 0;
        if (part.isMimeType("text/*") && !isContainTextAttach) {
            content.append(part.getContent().toString());
        } else if (part.isMimeType("message/rfc822")) {
            getMailTextContent((Part) part.getContent(), content);
        } else if (part.isMimeType("multipart/*")) {
            Multipart multipart = (Multipart) part.getContent();
            int partCount = multipart.getCount();
            for (int i = 0; i < partCount; i++) {
                BodyPart bodyPart = multipart.getBodyPart(i);
                getMailTextContent(bodyPart, content);
            }
        }
    }


    /**
     * 保存附件
     *
     * @param part 邮件中多个组合体中的其中一个组合体
     * @throws UnsupportedEncodingException
     * @throws MessagingException
     * @throws FileNotFoundException
     * @throws IOException
     */
    private List<MyEmailAttachment> saveAttachment(Part part, String rootPath, String emailAddress, String messageId) throws UnsupportedEncodingException, MessagingException,
            FileNotFoundException, IOException {
        List<MyEmailAttachment> attachmentList = new ArrayList<>();
        if (part.isMimeType("multipart/*")) {
            Multipart multipart = (Multipart) part.getContent();    //复杂体邮件
            //复杂体邮件包含多个邮件体
            int partCount = multipart.getCount();
            for (int i = 0; i < partCount; i++) {
                //获得复杂体邮件中其中一个邮件体
                BodyPart bodyPart = multipart.getBodyPart(i);
                //某一个邮件体也有可能是由多个邮件体组成的复杂体
                String disp = bodyPart.getDisposition();
                String attachmentName = decodeText(bodyPart.getFileName());
                String fileType = getFileType(attachmentName);
                if (disp != null && (disp.equalsIgnoreCase(Part.ATTACHMENT) || disp.equalsIgnoreCase(Part.INLINE))) {
                    InputStream is = bodyPart.getInputStream();
                    String filePath = saveFile(is, rootPath, emailAddress, messageId, decodeText(bodyPart.getFileName()));
                    attachmentList.add(new MyEmailAttachment() {{
                        setAttachmentName(attachmentName);
                        setFileType(fileType);
                        setFilePath(filePath);
                    }});
                } else if (bodyPart.isMimeType("multipart/*")) {
                    saveAttachment(bodyPart, rootPath, emailAddress, messageId);
                } else {
                    String contentType = bodyPart.getContentType();
                    if (contentType.indexOf("name") != -1 || contentType.indexOf("application") != -1) {
                        String filePath = saveFile(bodyPart.getInputStream(), rootPath, emailAddress, messageId, decodeText(bodyPart.getFileName()));
                        attachmentList.add(new MyEmailAttachment() {{
                            setAttachmentName(attachmentName);
                            setFileType(fileType);
                            setFilePath(filePath);
                        }});
                    }
                }
//                return attachmentList;
            }
        } else if (part.isMimeType("message/rfc822")) {
            saveAttachment((Part) part.getContent(), rootPath, emailAddress, messageId);
        }
        return attachmentList;
    }

    /**
     * 读取输入流中的数据保存至指定目录
     *
     * @param is       输入流
     * @param fileName 文件名
     * @throws FileNotFoundException
     * @throws IOException
     */
    private String saveFile(InputStream is, String rootPath, String emailAddress, String messageId, String fileName) throws FileNotFoundException, IOException {
        try {
            String s = File.separator;
            String filePath = rootPath;
            if (!StrUtil.endWith(filePath, s)) {
                filePath = filePath.concat(s);
            }
            // 根据时间生成路径
            LocalDateTime date = LocalDateTime.now();
            messageId = messageId.replaceAll("[<>]", "");
            String subDir = "email" + s + emailAddress + s + messageId + s;
            String allPath = filePath + subDir;
            File dir = FileUtil.mkdir(allPath);
            String storePath = subDir.concat(fileName);
            BufferedInputStream bis = new BufferedInputStream(is);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(allPath + fileName));
            int len = -1;
            while ((len = bis.read()) != -1) {
                bos.write(len);
                bos.flush();
            }
            is.close();
            bos.close();
            bis.close();
            return storePath;
        } catch (Exception e) {
            return null;
        }
    }

    private String saveBodyFile(String content, String rootPath, String emailAddress, String messageId, String fileName) throws IOException {
        String s = File.separator;
        String filePath = rootPath;
        if (!StrUtil.endWith(filePath, s)) {
            filePath = filePath.concat(s);
        }
        LocalDateTime date = LocalDateTime.now();
        try {
            messageId = messageId.replaceAll("[<>]", "");
        } catch (Exception e) {
            messageId = "";
        }
        String subDir = emailAddress + s + messageId + s;
        String allPath = filePath + subDir;
        File dir = new File(allPath);
        if (!dir.exists()||!dir.isDirectory()){
            dir.mkdirs();
        }
        String storePath = subDir.concat(fileName);
        try {
            FileUtil.writeString(content, allPath + fileName, StandardCharsets.UTF_8);
        } catch (Exception e) {
          log.error("path:{}---fileName:{}",allPath,fileName);
        }
        return storePath;

    }


    /**
     * 文本解码
     *
     * @param encodeText 解码MimeUtility.encodeText(String text)方法编码后的文本
     * @return 解码后的文本
     * @throws UnsupportedEncodingException
     */
    private String decodeText(String encodeText) throws UnsupportedEncodingException {
        if (encodeText == null || "".equals(encodeText)) {
            return "";
        } else {
            return MimeUtility.decodeText(encodeText);
        }
    }


    private String getFileType(String fileName) {
        Pattern p = Pattern.compile("^[()\\-\\u4e00-\\u9fa5a-zA-Z0-9_]+.([a-z]+)$");
        Matcher m = p.matcher(fileName);
        while (m.find()) {
            String result = String.valueOf(m.group(1));
            return result;
        }
        return null;
    }

}

