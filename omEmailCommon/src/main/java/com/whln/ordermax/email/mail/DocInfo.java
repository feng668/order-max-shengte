package com.whln.ordermax.email.mail;

import java.util.Date;

public class DocInfo {

	String docName;
	String url;
	String businessLine;
	// receivedMode(Constant.OWNER_TYPE_EMAIL);
	Date eceivedTime;
	Date receivedTime;
	String ownerEmail;
	String ownerName;
	
	
	public Date getReceivedTime() {
		return receivedTime;
	}
	public void setReceivedTime(Date receivedTime) {
		this.receivedTime = receivedTime;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getBusinessLine() {
		return businessLine;
	}
	public void setBusinessLine(String businessLine) {
		this.businessLine = businessLine;
	}
	public Date getEceivedTime() {
		return eceivedTime;
	}
	public void setEceivedTime(Date eceivedTime) {
		this.eceivedTime = eceivedTime;
	}
	public String getOwnerEmail() {
		return ownerEmail;
	}
	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}
	
	

}
