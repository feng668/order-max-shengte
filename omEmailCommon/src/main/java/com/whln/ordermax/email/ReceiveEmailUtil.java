package com.whln.ordermax.email;

import com.sun.mail.imap.IMAPStore;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.*;

/**
 * @author liurun
 * @date 2023-04-04 15:14
 */
public class ReceiveEmailUtil {
    public static Message[] receiveEmail(String email,String password,String serverAddress,String serverPort,  Date start, Date end,
                                          HashMap<String, String> iam) {
        // 准备连接服务器的会话信息
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imap");
        props.setProperty("mail.imap.host", "imap.qq.com");
        props.setProperty("mail.imap.port", "993");
        props.setProperty("mail.imap.auth.login.disable", "true");
        props.setProperty("mail.imap.ssl.enable", "true");

        Session session = null;
        // 获取收件箱
        IMAPStore store = null;
        Folder folder = null;
        Thread t =  Thread.currentThread();
        ClassLoader ccl = t.getContextClassLoader();
        t.setContextClassLoader(Session.class.getClassLoader());
        try {
            session = Session.getInstance(props, new Authenticator() {
                @Override
                public PasswordAuthentication getPasswordAuthentication() {
                    //发件人邮件用户名、授权码
                    return new PasswordAuthentication(email, password);
                }
            });
            store = (IMAPStore) session.getStore("imap");
            store.connect(serverAddress, email,password);
            //带上IMAP ID信息，由key和value组成，例如name，version，vendor，support-email等。
            store.id(iam);
            folder = store.getFolder("INBOX");
            // 以读写方式打开
            folder.open(Folder.READ_ONLY);
            Message[] messages = getEmail(folder, start, end);

            folder.close(true);
            store.close();
            return messages;
        } catch (MessagingException e) {
            e.printStackTrace();
        }finally  {
            t.setContextClassLoader(ccl);
        }
        return null;
    }

    /**
     * 获取邮件列表
     *
     * @param folder
     * @param start
     * @param end
     * @return
     */
    private static Message[] getEmail(Folder folder, Date start, Date end) throws MessagingException {
        Message[] messages;
        int num = folder.getMessageCount();
        if ((start == null) && (end == null)) {
            // 获取最后一封邮件
//            int emailIndex = Math.max((num - 1), 1);
            messages = folder.getMessages(num, num);
        } else {
            int emailIndex = Math.max((num - 99), 1);
            Message[] allMessages = folder.getMessages(emailIndex, num);
            messages = Arrays.stream(allMessages).filter(m -> {
                try {
                    return betweenDate(m, start, end);
                } catch (MessagingException e) {
                    e.printStackTrace();
                }
                return false;
            }).toArray(Message[]::new);
        }
        return messages;
    }

    /**
     * 筛选时间段内邮件
     *
     * @param message
     * @param start
     * @param end
     * @return
     */
    private static boolean betweenDate(Message message, Date start, Date end) throws MessagingException {
        if (message.getReceivedDate() == null) {
//            log.error("receiveDate null {}", message.getSubject());
            return false;
        }
        return message.getReceivedDate().before(end) &&
                message.getReceivedDate().after(start) ||
                message.getReceivedDate().equals(start);
    }


    /**
     * 处理邮件地址和用户名
     *
     * @param original
     * @return
     */
    private HashMap<String, List<String>> addressConvert(Address[] original) {
        HashMap<String, List<String>> res = new HashMap<>();
        if (original == null) {
            res.put("name", null);
            res.put("address", null);
            return res;
        }
        List<String> name = new ArrayList<>();
        List<String> address = new ArrayList<>();
        Arrays.stream(original).forEach(t -> {
            InternetAddress ter = (InternetAddress) t;
            address.add(ter.getAddress());
            name.add(ter.getPersonal());
        });
        res.put("name", name);
        res.put("address", address);
        return res;
    }

    /**
     * 递归处理邮件正文
     *
     * @param part
     * @return
     */
    private String getBody(Part part) throws MessagingException, IOException {
        if (part.isMimeType("text/*")) {
            // Part是文本:
            return part.getContent().toString();
        }
        if (part.isMimeType("multipart/*")) {
            // Part是一个Multipart对象:
            Multipart multipart = (Multipart) part.getContent();
            // 循环解析每个子Part:
            for (int i = 0; i < multipart.getCount(); i++) {
                BodyPart bodyPart = multipart.getBodyPart(i);
                String body = getBody(bodyPart);
                if (!body.isEmpty()) {
                    return body;
                }
            }
        }
        return "";
    }
    public Date getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);

        return todayStart.getTime();
    }
    /**
     * 返回明天
     *
     * @param today
     * @return
     */
    public Date tomorrow(Date today) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);
        calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
        return calendar.getTime();
    }
}
