#! /bin/bash
cd ../../
git pull origin dev
mvn clean package
cd service-impl
cp -f target/service-impl-1.0.jar docker/java/app.jar
echo "当前文件夹:"+"$pwd"
cd docker
docker-compose restart
