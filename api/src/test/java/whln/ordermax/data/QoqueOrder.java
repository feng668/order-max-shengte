package whln.ordermax.data;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whln.ordermax.api.OrderMaxApiApplication;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.repository.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-24 19:01
 */
@ActiveProfiles("prod")
@SpringBootTest(classes = OrderMaxApiApplication.class)
public class QoqueOrder {
    @Resource
    QoQuoteOrderRepository qoQuoteOrderRepository;
    @Resource
    QoInQuoteOrderRepository qoInQuoteOrderRepository;
    @Resource
    QoQuoteCommodityRepository quoteCommodityRepository;
    @Resource
    QoInQuoteCommodityRepository qoInQuoteCommodityRepository;
    @Resource
    CmCommodityRepository commodityRepository;

    @Test
    public void qoQueOrder() {
        String[] split = getCommodityIds();
        QoQuoteOrder one = qoQuoteOrderRepository.getOne(new LambdaQueryWrapper<QoQuoteOrder>().eq(QoQuoteOrder::getId, "1633014607335337986"));
        QoQuoteCommodity qoQuoteCommodity = quoteCommodityRepository.getOne(new LambdaQueryWrapper<QoQuoteCommodity>().eq(QoQuoteCommodity::getId, "1639247183137968186"));
        List<QoQuoteOrder> qoQuoteOrderList = new ArrayList<>();
        List<QoQuoteCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            QoQuoteOrder qoQuoteOrder = BeanUtil.copyProperties(one, QoQuoteOrder.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 15; j++) {
                QoQuoteCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, QoQuoteCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }

        List<List<QoQuoteOrder>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<QoQuoteOrder> l : lists) {
            qoQuoteOrderRepository.saveBatch(l);
        }
        List<List<QoQuoteCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<QoQuoteCommodity> l : qoQuoteCommodityList1) {
            quoteCommodityRepository.saveBatch(l);
        }
    }

    @Test
    public void qoQueiNOrder() {
        String[] split = getCommodityIds();
        QoInQuoteOrder one = qoInQuoteOrderRepository.getOne(new LambdaQueryWrapper<QoInQuoteOrder>().eq(QoInQuoteOrder::getId, "1633084290524762114"));
        QoInQuoteCommodity qoQuoteCommodity = qoInQuoteCommodityRepository.getOne(new LambdaQueryWrapper<QoInQuoteCommodity>().eq(QoInQuoteCommodity::getId, "1633084348674592769"));
        List<QoInQuoteOrder> qoQuoteOrderList = new ArrayList<>();
        List<QoInQuoteCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            QoInQuoteOrder qoQuoteOrder = BeanUtil.copyProperties(one, QoInQuoteOrder.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 40; j++) {
                QoInQuoteCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, QoInQuoteCommodity.class);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }

        List<List<QoInQuoteCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<QoInQuoteCommodity> l : qoQuoteCommodityList1) {
            qoInQuoteCommodityRepository.saveBatch(l);
        }

        List<List<QoInQuoteOrder>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<QoInQuoteOrder> l : lists) {
            qoInQuoteOrderRepository.saveBatch(l);
        }
    }

    @Resource
    PoContractRepository poContractRepository;
    @Resource
    PoContractCommodityRepository poContractCommodityRepository;
    @Resource
    SysService sysService;

    @Test
    public void poContract() {
        String[] split = getCommodityIds();

        PoContract one = poContractRepository.getOne(new LambdaQueryWrapper<PoContract>().eq(PoContract::getId, "1590215636007505921"));
        PoContractCommodity qoQuoteCommodity = poContractCommodityRepository.getOne(new LambdaQueryWrapper<PoContractCommodity>().eq(PoContractCommodity::getId, "1591330025527795714"));
        List<PoContract> qoQuoteOrderList = new ArrayList<>();
        List<PoContractCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            PoContract qoQuoteOrder = BeanUtil.copyProperties(one, PoContract.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 40; j++) {
                PoContractCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, PoContractCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }

        List<List<PoContractCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<PoContractCommodity> l : qoQuoteCommodityList1) {
            poContractCommodityRepository.saveBatch(l);
        }


        List<List<PoContract>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<PoContract> l : lists) {
            poContractRepository.saveBatch(l);
        }

    }
    @Resource
    PoInvoiceRepository poInvoiceRepository;
    @Resource
    PoInvoiceCommodityRepository poInvoiceCommodityRepository;
    @Test
    public void poInvoice() {
        String[] split = getCommodityIds();

        PoInvoice one = poInvoiceRepository.getOne(new LambdaQueryWrapper<PoInvoice>().eq(PoInvoice::getId, "1603716010039300098"));
        PoInvoiceCommodity qoQuoteCommodity = poInvoiceCommodityRepository.getOne(new LambdaQueryWrapper<PoInvoiceCommodity>().eq(PoInvoiceCommodity::getId, "1599034400626909186"));
        List<PoInvoice> qoQuoteOrderList = new ArrayList<>();
        List<PoInvoiceCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            PoInvoice qoQuoteOrder = BeanUtil.copyProperties(one, PoInvoice.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setContractNo(qoQuoteOrder.getContractNo() + i);
            for (int j = 0; j < 5; j++) {
                PoInvoiceCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, PoInvoiceCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }

        List<List<PoInvoiceCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<PoInvoiceCommodity> l : qoQuoteCommodityList1) {
            poInvoiceCommodityRepository.saveBatch(l);
        }


        List<List<PoInvoice>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<PoInvoice> l : lists) {
            poInvoiceRepository.saveBatch(l);
        }

    }
    @Resource
    PoRequestRepository poRequestRepository;
    @Resource
    PoRequestCommodityRepository poRequestCommodityRepository;
    @Test
    public void poRequest() {
        String[] split = getCommodityIds();

        PoRequest one = poRequestRepository.getOne(new LambdaQueryWrapper<PoRequest>().eq(PoRequest::getId, "1593198831334273026"));
        PoRequestCommodity qoQuoteCommodity = poRequestCommodityRepository.getOne(new LambdaQueryWrapper<PoRequestCommodity>().eq(PoRequestCommodity::getId, "1592844651067314177"));
        List<PoRequest> qoQuoteOrderList = new ArrayList<>();
        List<PoRequestCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            PoRequest qoQuoteOrder = BeanUtil.copyProperties(one, PoRequest.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 5; j++) {
                PoRequestCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, PoRequestCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }

        List<List<PoRequestCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<PoRequestCommodity> l : qoQuoteCommodityList1) {
            poRequestCommodityRepository.saveBatch(l);
        }


        List<List<PoRequest>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<PoRequest> l : lists) {
            poRequestRepository.saveBatch(l);
        }

    }

@Resource
    PrPricingOrderRepository prPricingOrderRepository;
    @Resource
    PrPricingCommodityRepository prPricingCommodityRepository;
    @Test
    public void pricing() {
        String[] split = getCommodityIds();

        PrPricingOrder one = prPricingOrderRepository.getOne(new LambdaQueryWrapper<PrPricingOrder>().eq(PrPricingOrder::getId, "15921406552728535059999"));
        PrPricingCommodity qoQuoteCommodity = prPricingCommodityRepository.getOne(new LambdaQueryWrapper<PrPricingCommodity>().eq(PrPricingCommodity::getId, "1639242242394562579"));
        List<PrPricingOrder> qoQuoteOrderList = new ArrayList<>();
        List<PrPricingCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            PrPricingOrder qoQuoteOrder = BeanUtil.copyProperties(one, PrPricingOrder.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 15; j++) {
                PrPricingCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, PrPricingCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }

        List<List<PrPricingCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<PrPricingCommodity> l : qoQuoteCommodityList1) {
            prPricingCommodityRepository.saveBatch(l);
        }


        List<List<PrPricingOrder>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<PrPricingOrder> l : lists) {
            prPricingOrderRepository.saveBatch(l);
        }

    }

 /*   @Resource
    SoSoOrderRepository soSoOrderRepository;
    @Resource
    SoSoOrderCommodityRepository soSoOrderCommodityRepository;
    @Test
    public void sosoOrder() {
        String[] split = getCommodityIds();

        SoSoOrder one = soSoOrderRepository.getOne(new LambdaQueryWrapper<SoSoOrder>().eq(SoSoOrder::getId, "1639229287577911298"));
        SoSoOrderCommodity qoQuoteCommodity = soSoOrderCommodityRepository.getOne(new LambdaQueryWrapper<SoSoOrderCommodity>().eq(SoSoOrderCommodity::getId, "1632575684461178882"));
        List<SoSoOrder> qoQuoteOrderList = new ArrayList<>();
        List<SoSoOrderCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            SoSoOrder qoQuoteOrder = BeanUtil.copyProperties(one, SoSoOrder.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 5; j++) {
                SoSoOrderCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, SoSoOrderCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }

        List<List<SoSoOrderCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<SoSoOrderCommodity> l : qoQuoteCommodityList1) {
            soSoOrderCommodityRepository.saveBatch(l);
        }


        List<List<SoSoOrder>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<SoSoOrder> l : lists) {
            soSoOrderRepository.saveBatch(l);
        }

    }*/
    @Resource
    SoSiOrderRepository soSiOrderRepository;
    @Resource
    SoSiOrderCommodityRepository soSiOrderCommodityRepository;
   /* @Test
    public void sosiOrder() {
        String[] split = getCommodityIds();

        SoSiOrder one = soSiOrderRepository.getOne(new LambdaQueryWrapper<SoSiOrder>().eq(SoSiOrder::getId, "1636966240184426498"));
        SoSiOrderCommodity qoQuoteCommodity = soSiOrderCommodityRepository.getOne(new LambdaQueryWrapper<SoSiOrderCommodity>().eq(SoSiOrderCommodity::getId, "1635579114595233794"));
        List<SoSiOrder> qoQuoteOrderList = new ArrayList<>();
        List<SoSiOrderCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            SoSiOrder qoQuoteOrder = BeanUtil.copyProperties(one, SoSiOrder.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 5; j++) {
                SoSiOrderCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, SoSiOrderCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }

        List<List<SoSiOrderCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<SoSiOrderCommodity> l : qoQuoteCommodityList1) {
            soSiOrderCommodityRepository.saveBatch(l);
        }


        List<List<SoSiOrder>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<SoSiOrder> l : lists) {
            soSiOrderRepository.saveBatch(l);
        }

    }*/
    @Resource
    StInOrderRepository stInOrderRepository;
    @Resource
    StInOrderCommodityRepository stInOrderCommodityRepository;
    @Test
    public void StInOrder1() {
        String[] split = getCommodityIds();

        StInOrder one = stInOrderRepository.getOne(new LambdaQueryWrapper<StInOrder>().eq(StInOrder::getId, "1638525150703886338"));
        StInOrderCommodity qoQuoteCommodity = stInOrderCommodityRepository.getOne(new LambdaQueryWrapper<StInOrderCommodity>().eq(StInOrderCommodity::getId, "1638525219486277634"));
        List<StInOrder> qoQuoteOrderList = new ArrayList<>();
        List<StInOrderCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            StInOrder qoQuoteOrder = BeanUtil.copyProperties(one, StInOrder.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 15; j++) {
                StInOrderCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, StInOrderCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }



        List<List<StInOrder>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<StInOrder> l : lists) {
            stInOrderRepository.saveBatch(l);
        }
        List<List<StInOrderCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<StInOrderCommodity> l : qoQuoteCommodityList1) {
            stInOrderCommodityRepository.saveBatch(l);
        }

    }

    @Resource
    StOutOrderRepository stOutOrderRepository;
    @Resource
    StOutOrderCommodityRepository stOutOrderCommodityRepository;

    @Test
    public void StOutOrder1() {
        String[] split = getCommodityIds();
        StOutOrder one = stOutOrderRepository.getOne(new LambdaQueryWrapper<StOutOrder>().eq(StOutOrder::getId, "1638525433345449986"));
        StOutOrderCommodity qoQuoteCommodity = stOutOrderCommodityRepository.getOne(new LambdaQueryWrapper<StOutOrderCommodity>().eq(StOutOrderCommodity::getId, "1638525481600917505"));
        List<StOutOrder> qoQuoteOrderList = new ArrayList<>();
        List<StOutOrderCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            StOutOrder qoQuoteOrder = BeanUtil.copyProperties(one, StOutOrder.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 10; j++) {
                StOutOrderCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, StOutOrderCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }


        List<List<StOutOrder>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<StOutOrder> l : lists) {
            stOutOrderRepository.saveBatch(l);
        }
        List<List<StOutOrderCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<StOutOrderCommodity> l : qoQuoteCommodityList1) {
            stOutOrderCommodityRepository.saveBatch(l);
        }



    }

    @Resource
    PdProductionTaskRepository pdProductionTaskRepository;
    @Resource
    PdProductTaskCommodityRepository pdProductTaskCommodityRepository;
    @Test
    public void pdproduction() {
        String[] split = getCommodityIds();

        PdProductionTask one = pdProductionTaskRepository.getOne(new LambdaQueryWrapper<PdProductionTask>().eq(PdProductionTask::getId, "1593126649916792834"));
        PdProductTaskCommodity qoQuoteCommodity = pdProductTaskCommodityRepository.getOne(new LambdaQueryWrapper<PdProductTaskCommodity>().eq(PdProductTaskCommodity::getId, "1571812807853125633"));
        List<PdProductionTask> qoQuoteOrderList = new ArrayList<>();
        List<PdProductTaskCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            PdProductionTask qoQuoteOrder = BeanUtil.copyProperties(one, PdProductionTask.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 5; j++) {
                PdProductTaskCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, PdProductTaskCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setTaskId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }


        List<List<PdProductionTask>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<PdProductionTask> l : lists) {
            pdProductionTaskRepository.saveBatch(l);
        }
        List<List<PdProductTaskCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<PdProductTaskCommodity> l : qoQuoteCommodityList1) {
            pdProductTaskCommodityRepository.saveBatch(l);
        }
    }
    @Resource
    PdQualityRepository pdQualityRepository;
    @Test
    public void pd_quality() {

        PdQuality one = pdQualityRepository.getOne(new LambdaQueryWrapper<PdQuality>().eq(PdQuality::getId, "1631631750104842243"));
        List<PdQuality> qoQuoteOrderList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            PdQuality qoQuoteOrder = BeanUtil.copyProperties(one, PdQuality.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            qoQuoteOrder.setProductionTaskId(sysService.nextId());
            qoQuoteOrderList.add(qoQuoteOrder);
        }


        List<List<PdQuality>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<PdQuality> l : lists) {
            pdQualityRepository.saveBatch(l);
        }
    }


    @Resource
    TrInDispatchRepository trInDispatchRepository;
    @Resource
    TrInDispatchCommodityRepository trInDispatchCommodityRepository;
    @Test
    public void tr_in_dispatch() {
        String[] split = getCommodityIds();

        TrInDispatch one = trInDispatchRepository.getOne(new LambdaQueryWrapper<TrInDispatch>().eq(TrInDispatch::getId, "1633286780587307009"));
        TrInDispatchCommodity qoQuoteCommodity = trInDispatchCommodityRepository.getOne(new LambdaQueryWrapper<TrInDispatchCommodity>().eq(TrInDispatchCommodity::getId, "1637754179105492994"));
        List<TrInDispatch> qoQuoteOrderList = new ArrayList<>();
        List<TrInDispatchCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            TrInDispatch qoQuoteOrder = BeanUtil.copyProperties(one, TrInDispatch.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 15; j++) {
                TrInDispatchCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, TrInDispatchCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }
        List<List<TrInDispatch>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<TrInDispatch> l : lists) {
            trInDispatchRepository.saveBatch(l);
        }
        List<List<TrInDispatchCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<TrInDispatchCommodity> l : qoQuoteCommodityList1) {
            trInDispatchCommodityRepository.saveBatch(l);
        }




    }


    @Resource
    TrTransportationRepository trTransportationRepository;
    @Resource
    TrTransportationCommodityRepository trTransportationCommodityRepository;
    @Test
    public void tr_transportation() {
        String[] split = getCommodityIds();

        TrTransportation one = trTransportationRepository.getOne(new LambdaQueryWrapper<TrTransportation>().eq(TrTransportation::getId, "1632736607419154434"));
        TrTransportationCommodity qoQuoteCommodity = trTransportationCommodityRepository.getOne(new LambdaQueryWrapper<TrTransportationCommodity>().eq(TrTransportationCommodity::getId, "1632575684461178882"));
        List<TrTransportation> qoQuoteOrderList = new ArrayList<>();
        List<TrTransportationCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            TrTransportation qoQuoteOrder = BeanUtil.copyProperties(one, TrTransportation.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 40; j++) {
                TrTransportationCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, TrTransportationCommodity.class);
                qoQuoteCommodity1.setCommodityId(split[j]);
                qoQuoteCommodity1.setId(sysService.nextId());
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }
        List<List<TrTransportation>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<TrTransportation> l : lists) {
            trTransportationRepository.saveBatch(l);
        }
        List<List<TrTransportationCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<TrTransportationCommodity> l : qoQuoteCommodityList1) {
            trTransportationCommodityRepository.saveBatch(l);
        }
    }


    @Resource
    AcceptanceRepository acceptanceRepository;
    @Test
    public void pd_acceptance() {
        PdAcceptance one = acceptanceRepository.getOne(new LambdaQueryWrapper<PdAcceptance>().eq(PdAcceptance::getId, "1635261280866488321"));
        List<PdAcceptance> qoQuoteOrderList = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            PdAcceptance qoQuoteOrder = BeanUtil.copyProperties(one, PdAcceptance.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            qoQuoteOrder.setAcceptanceId(sysService.nextId());
            qoQuoteOrderList.add(qoQuoteOrder);
        }


        List<List<PdAcceptance>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<PdAcceptance> l : lists) {
            acceptanceRepository.saveBatch(l);
        }
    }


    private String[] getCommodityIds() {
        String s = "fs151512ds19387\n" +
                "fs151512ds19388\n" +
                "fs151512ds19389\n" +
                "fs151512ds1939\n" +
                "fs151512ds19390\n" +
                "fs151512ds19391\n" +
                "fs151512ds19392\n" +
                "fs151512ds19393\n" +
                "fs151512ds19394\n" +
                "fs151512ds19395\n" +
                "fs151512ds19396\n" +
                "fs151512ds19397\n" +
                "fs151512ds19398\n" +
                "fs151512ds19399\n" +
                "fs151512ds194\n" +
                "fs151512ds1940\n" +
                "fs151512ds19400\n" +
                "fs151512ds19401\n" +
                "fs151512ds19402\n" +
                "fs151512ds19403\n" +
                "fs151512ds19404\n" +
                "fs151512ds19405\n" +
                "fs151512ds19406\n" +
                "fs151512ds19407\n" +
                "fs151512ds19408\n" +
                "fs151512ds19409\n" +
                "fs151512ds1941\n" +
                "fs151512ds19410\n" +
                "fs151512ds19411\n" +
                "fs151512ds19412\n" +
                "fs151512ds19413\n" +
                "fs151512ds19414\n" +
                "fs151512ds19415\n" +
                "fs151512ds19416\n" +
                "fs151512ds19417\n" +
                "fs151512ds19418\n" +
                "fs151512ds19419\n" +
                "fs151512ds1942\n" +
                "fs151512ds19420\n" +
                "fs151512ds19421";
        String[] split = s.split("\n");
        return split;
    }

}
