package whln.ordermax.data;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.OrderMaxApiApplication;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.CmCommodity;
import com.whln.ordermax.data.domain.CuCustomer;
import com.whln.ordermax.data.domain.QoQuoteCommodity;
import com.whln.ordermax.data.domain.QoQuoteOrder;
import com.whln.ordermax.data.mapper.CmCommodityMapper;
import com.whln.ordermax.data.repository.CmCommodityRepository;
import com.whln.ordermax.data.repository.CuCustomerRepository;
import com.whln.ordermax.data.repository.QoQuoteCommodityRepository;
import com.whln.ordermax.data.repository.QoQuoteOrderRepository;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-24 17:23
 */
@ActiveProfiles("prod")
@SpringBootTest(classes = OrderMaxApiApplication.class)
public class CreateDataTest {
    @Resource
    CmCommodityRepository commodityRepository;

    @Test
    public void commodity() {
        List<CmCommodity> list = new ArrayList<>(10000);
        for (int i = 0; i < 10000; i++) {
            CmCommodity c = new CmCommodity();
            c.setId("fs151512ds1" + i);
            c.setCnName("铅粒" + i);
            c.setEnName("Lead Shot" + i);
            c.setSpecification("3N" + i);
            c.setEnSpecification("3N" + i);
            c.setOwnerId("sfsdfsfsf410" + i);
            c.setNetWeight(new BigDecimal("2.0000"));
            c.setGrossWeight(new BigDecimal("2.0000"));
            c.setQuantityUnit("千克" + i);
            c.setCreateDate(LocalDate.now());
            c.setUpdateDate(LocalDateTime.now());
            c.setMiniName("AA" + i);

            c.setCategoryId("sdfdsfs" + i);
            c.setPartNo("01.3PB222A" + i);
            c.setNum(new BigDecimal("2022"));
            c.setAmount(new BigDecimal("2121"));
            c.setArticleType("COMMODITY");
            list.add(c);
        }
        List<List<CmCommodity>> lists = CollUtils.collSub(list, 500);
        System.out.println(lists.size());
        for (List<CmCommodity> l : lists) {
            commodityRepository.saveBatch(l);
        }
//        commodityRepository.saveBatch(c);
    }

    @Resource
    CuCustomerRepository cuCustomerRepository;

    @Test
    public void customer() {
        CuCustomer one = cuCustomerRepository.getOne(new LambdaQueryWrapper<CuCustomer>().eq(CuCustomer::getId, "1563060493994676225"));
        List<CuCustomer> cuCustomers = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            CuCustomer cuCustomer = BeanUtil.copyProperties(one, CuCustomer.class);
            cuCustomer.setId(cuCustomer.getId() + i);
            cuCustomer.setCnName(cuCustomer.getCnName() + i);
            cuCustomers.add(cuCustomer);
        }
        List<List<CuCustomer>> lists = CollUtils.collSub(cuCustomers, 100);
        for (List<CuCustomer> l : lists) {
            cuCustomerRepository.saveBatch(l);
        }
    }

    @Resource
    QoQuoteOrderRepository qoQuoteOrderRepository;
    @Resource
    QoQuoteCommodityRepository quoteCommodityRepository;
    @Resource
    CmCommodityMapper cmCommodityMapper;
    @Test
    public void qoQueOrder() {
        List<CmCommodity> list = commodityRepository.list(new LambdaQueryWrapper<CmCommodity>().last("limit 40"));
        /*List<CmCommodity> cmCommodityList = cmCommodityPage.getRecords();
        QoQuoteOrder one = qoQuoteOrderRepository.getOne(new LambdaQueryWrapper<QoQuoteOrder>().eq(QoQuoteOrder::getId, "1632573447013904386"));
        QoQuoteCommodity qoQuoteCommodity = quoteCommodityRepository.getOne(new LambdaQueryWrapper<QoQuoteCommodity>().eq(QoQuoteCommodity::getId, "1632573903211573249"));
        List<QoQuoteOrder> qoQuoteOrderList = new ArrayList<>();
        List<QoQuoteCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i =0;i<10000;i++){
            QoQuoteOrder qoQuoteOrder = BeanUtil.copyProperties(one, QoQuoteOrder.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 40; i++) {
                QoQuoteCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, QoQuoteCommodity.class);
                CmCommodity cmCommodity = cmCommodityList.get(j);
                qoQuoteCommodity1.setCommodityId(cmCommodity.getId());
                qoQuoteCommodity1.setId(qoQuoteCommodity1.getId() + i + j);
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }
        List<List<QoQuoteOrder>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<QoQuoteOrder> l:lists){
            qoQuoteOrderRepository.saveBatch(l);
        }
        List<List<QoQuoteCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<QoQuoteCommodity> l:qoQuoteCommodityList1){
            quoteCommodityRepository.saveBatch(l);
        }*/
    }

    @Test
    public void qoQueInOrder() {
        Page<CmCommodity> cmCommodityPage = cmCommodityMapper.listByEntity(new CmCommodity(), new Page<>(1, 40));
        List<CmCommodity> cmCommodityList = cmCommodityPage.getRecords();
        QoQuoteOrder one = qoQuoteOrderRepository.getOne(new LambdaQueryWrapper<QoQuoteOrder>().eq(QoQuoteOrder::getId, "1632573447013904386"));
        QoQuoteCommodity qoQuoteCommodity = quoteCommodityRepository.getOne(new LambdaQueryWrapper<QoQuoteCommodity>().eq(QoQuoteCommodity::getId, "1632573903211573249"));
        List<QoQuoteOrder> qoQuoteOrderList = new ArrayList<>();
        List<QoQuoteCommodity> qoQuoteCommodityList = new ArrayList<>();
        for (int i =0;i<10000;i++){
            QoQuoteOrder qoQuoteOrder = BeanUtil.copyProperties(one, QoQuoteOrder.class);
            qoQuoteOrder.setId(one.getId() + i);
            qoQuoteOrder.setBillNo(qoQuoteOrder.getBillNo() + i);
            for (int j = 0; j < 40; i++) {
                QoQuoteCommodity qoQuoteCommodity1 = BeanUtil.copyProperties(qoQuoteCommodity, QoQuoteCommodity.class);
                CmCommodity cmCommodity = cmCommodityList.get(j);
                qoQuoteCommodity1.setCommodityId(cmCommodity.getId());
                qoQuoteCommodity1.setId(qoQuoteCommodity1.getId() + i + j);
                qoQuoteCommodity1.setBillId(qoQuoteOrder.getId());
                qoQuoteCommodityList.add(qoQuoteCommodity1);
            }
            qoQuoteOrderList.add(qoQuoteOrder);
        }
        List<List<QoQuoteOrder>> lists = CollUtils.collSub(qoQuoteOrderList, 1000);
        for (List<QoQuoteOrder> l:lists){
            qoQuoteOrderRepository.saveBatch(l);
        }
        List<List<QoQuoteCommodity>> qoQuoteCommodityList1 = CollUtils.collSub(qoQuoteCommodityList, 1000);
        for (List<QoQuoteCommodity> l:qoQuoteCommodityList1){
            quoteCommodityRepository.saveBatch(l);
        }
    }
}
