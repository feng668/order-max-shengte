package whln.ordermax.api;

import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.json.JSONUtil;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.common.utils.TreeUtil;
import com.whln.ordermax.data.domain.SysPower;
import com.whln.ordermax.data.repository.SysPowerRepository;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
@SpringBootTest
public class SysPowerTest {

    @Resource
    private SysPowerRepository sysPowerRepository;

    @Resource
    private SysService sysService;

    @Test
    void contextLoads() throws IOException {
        List<SysPower> sysPowerList = JSONUtil.toList(IOUtils.toString(new ClassPathResource("powerTree.json").getStream(), "utf-8"), SysPower.class);
        List<SysPower> OpList = JSONUtil.toList(IOUtils.toString(new ClassPathResource("powerOpera.json").getStream(), "utf-8"), SysPower.class);
        List<SysPower> resultTree = this.parseChirds(sysPowerList, null, null, null);
        List<SysPower> resultList = TreeUtil.treeToList(resultTree);
        sysPowerRepository.saveBatch(resultList);
    }

    private List<SysPower> parseChirds(List<SysPower> chirds, String parentId, String parentIds,String parentCode) throws IOException {
        if (chirds!=null&&chirds.size()>0){
            for (SysPower chird : chirds) {
                chird.setId(sysService.nextId());
                chird.setParentId(parentId);
                if (parentId==null){
                    chird.setParentIds(null);
                }else if (parentIds==null){
                    chird.setParentIds(parentId);
                }else {
                    chird.setParentIds(parentIds+"/"+parentId);
                }
                chird.setChildren(parseChirds(chird.getChildren(),chird.getId(),chird.getParentIds(),chird.getPermission()));
            }
            return chirds;
        }else if (parentCode != null){
            List<SysPower> OpList = JSONUtil.toList(IOUtils.toString(new ClassPathResource("powerOpera.json").getStream(), "utf-8"), SysPower.class);
            for (SysPower sysPower : OpList) {
                sysPower.setId(sysService.nextId());
                sysPower.setParentId(parentId);
                sysPower.setPermission(parentCode+":"+sysPower.getPermission());
                if (parentId==null){
                    sysPower.setParentIds(null);
                }else if (parentIds==null){
                    sysPower.setParentIds(parentId);
                }else {
                    sysPower.setParentIds(parentIds+"/"+parentId);
                }
            }
            return OpList;
        }
        return null;
    }

}
