package whln.ordermax.api;

import cn.hutool.core.util.StrUtil;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Test2 {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<String>() {{
            add("billNo");
            add("receiptsNo");
            add("customerBank");
            add("customerBankAccount");
            add("companyBank");
            add("companyBankAccount");
            add("note");
            add("createDate");
            add("custCnName");
            add("cpnCnName");
        }};
        String result = "<if test=\"entity.searchInfo!=null and entity.searchInfo!=''\">\nAND \n(\n";
        result += strings.stream().map(s -> StrUtil.toUnderlineCase(s) + " LIKE concat('%',#{entity.searchInfo},'%')\n").collect(Collectors.joining(" OR "));
        result += ")\n</if>";

        System.out.println(result);
    }
}
