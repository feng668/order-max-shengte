package whln.ordermax.api;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;

public class IdTest {

    public static void main(String[] args) {
        IdentifierGenerator identifierGenerator = new DefaultIdentifierGenerator();

        for (int i = 0; i < 50; i++) {
            String id = identifierGenerator.nextId(new Object()).toString();
            System.out.println(id);
        }
    }
}
