-- 2023-03-31
alter table sys_user
    add COLUMN education VARCHAR(64) DEFAULT null
alter table sys_role modify column code VARCHAR (64) not null
alter table cu_customer
    add COLUMN is_delete int(11) default 0
alter table sys_power CHANGE column url path varchar (64)

--   2023-4-3
alter table sys_power
    add column component varchar(64) COMMENT '组件';
alter table sys_power
    add column hidden int(64) COMMENT '是否隐藏  0否 1是';
alter table sys_power
    add column keepAlive int(11) COMMENT '是否缓存  0否 1是';
alter table sys_power
    add column redirect varchar(64) COMMENT '重定向地址';
alter table sys_power
    add column title varchar(64) COMMENT '标题';

CREATE TABLE `commodity_sup`
(
    `id`           varchar(64)    NOT NULL,
    `commodity_id` varchar(64)    NOT NULL,
    `supplier_id`  varchar(64)    NOT NULL,
    `price`        decimal(26, 4) NOT NULL,
    PRIMARY KEY (`id`)
);

ALTER TABLE `sys_power`
    MODIFY COLUMN `sort` int(11) NULL DEFAULT 0 COMMENT '排序' AFTER `enabled`;