[
    {
        value: "SF",
        label: "顺丰快递"
    },
    {
        value: "YTO",
        label: "圆通快递"
    },
    {
        value: "ZTO",
        label: "中通快递"
    },
    {
        value: "YUNDA",
        label: "韵达快递"
    },
    {
        value: "YDKY",
        label: "韵达快运"
    },
    {
        value: "STO",
        label: "申通快递"
    },
    {
        value: "TTKDEX",
        label: "天天快递"
    },
    {
        value: "JD",
        label: "京东快递"
    },
    {
        value: "EMS",
        label: "中国邮政EMS"
    },
    {
        value: "DBKD",
        label: "德邦快递"
    },
    {
        value: "DBL",
        label: "德邦物流"
    },
    {
        value: "DBKY",
        label: "德邦快运"
    },
    {
        value: "SURE",
        label: "速尔快运"
    },
    {
        value: "UC",
        label: "优速快递"
    },
    {
        value: "JT",
        label: "极兔速递"
    },
    {
        value: "KYE",
        label: "跨越速运"
    },
    {
        value: "CP471906",
        label: "顺心捷达"
    },
    {
        value: "ANE56",
        label: "安能物流"
    },
    {
        value: "BESTQJT",
        label: "百世快运"
    },
    {
        value: "FW",
        label: "丰网速递"
    },
    {
        value: "YMDD",
        label: "壹米滴答"
    },
    {
        value: "JYM",
        label: "加运美"
    },
    {
        value: "ZTOKY",
        label: "中通快运"
    },
    {
        value: "CP449455",
        label: "京广速递"
    },
    {
        value: "ZJS",
        label: "宅急送"
    },
    {
        value: "FAST",
        label: "快捷快递"
    },
    {
        value: "FEDEX",
        label: "联邦快递"
    },
    {
        value: "RRS",
        label: "日日顺"
    },
    {
        value: "SNWL",
        label: "苏宁快递"
    },
    {
        value: "ZMKM",
        label: "丹鸟"
    },
    {
        value: "ZMKMKD",
        label: "丹鸟快递"
    },
    {
        value: "RFD",
        label: "如风达"
    },
    {
        value: "YCKY",
        label: "远成快运"
    },
    {
        value: "JYXP",
        label: "九曳供应链"
    },
    {
        value: "CP468398",
        label: "圆通承诺达"
    },
    {
        value: "GTO",
        label: "国通快递"
    },
    {
        value: "QFKD",
        label: "全峰快递"
    },
    {
        value: "SDSD",
        label: "山东递速"
    },
    {
        value: "XFWL",
        label: "信丰物流"
    },
    {
        value: "CP443514",
        label: "百世云配"
    },
    {
        value: "LE10032270",
        label: "韵达同城"
    },
    {
        value: "PADTF",
        label: "平安达腾飞"
    },
    {
        value: "HOAU",
        label: "天地华宇"
    },
    {
        value: "STOKY",
        label: "申通快运"
    },
    {
        value: "ZTKY",
        label: "中铁物流/" +
            "飞豹快递"
    },
    {
        value: "EWINSHINE",
        label: "万象物流"
    },
    {
        value: "QRT",
        label: "全日通"
    },
    {
        value: "GZFY",
        label: "凡宇快递"
    },
    {
        value: "XBWL",
        label: "新邦物流"
    },
    {
        value: "CRE",
        label: "中铁快运"
    },
    {
        value: "LTS",
        label: "联昊通"
    },
    {
        value: "SHENGHUI",
        label: "盛辉物流"
    },
    {
        value: "HUANGMAJIA",
        label: "黄马甲配送"
    },
    {
        value: "CHENGBANG",
        label: "晟邦物流"
    },
    {
        value: "GZLT",
        label: "飞远配送"
    },
    {
        value: "HZABC",
        label: "飞远(" +
            "爱彼西)配送"
    },
    {
        value: "YCT",
        label: "黑猫宅急便"
    },
    {
        value: "COE",
        label: "COE"
    },
    {
        value: "DTW",
        label: "大田物流"
    },
    {
        value: "AIR",
        label: "亚风速递"
    },
    {
        value: "WJ",
        label: "万家物流"
    },
    {
        value: "D4PX",
        label: "递四方"
    },
    {
        value: "UPS",
        label: "UPS"
    },
    {
        value: "SUIJIAWL",
        label: "穗佳物流"
    },
    {
        value: "SUTENG",
        label: "速腾快递"
    },
    {
        value: "ANNTO",
        label: "安得智联"
    },
    {
        value: "DISU",
        label: "D速"
    },
    {
        value: "ZZJHTD",
        label: "郑州建华"
    },
    {
        value: "ZTOGJ",
        label: "中通国际"
    },
    {
        value: "SXJH",
        label: "建华快递"
    },
    {
        value: "ZL",
        label: "四川增联供应链"
    },
    {
        value: "JUSDA",
        label: "准时达"
    },
    {
        value: "YDGJ",
        label: "韵达国际"
    },
    {
        value: "FENGNIAO",
        label: "蜂鸟配送"
    },
    {
        value: "FJGJ",
        label: "泛捷国际物流"
    },
    {
        value: "CVP",
        label: "潮优配"
    },
    {
        value: "LSWL",
        label: "林氏物流"
    },
    {
        value: "CQLY",
        label: "重庆铃宇消费品供应链"
    },
    {
        value: "XINZHIHUI",
        label: "新智慧"
    },
    {
        value: "STOINTL",
        label: "申通国际"
    },
    {
        value: "HBZL",
        label: "湖北众联"
    },
    {
        value: "GOODKUAIDI",
        label: "GOOD快递"
    },
    {
        value: "EWE_GLOBAL",
        label: "EWE " +
            "GLOBAL"
    },
    {
        value: "WDSWL",
        label: "鸿达顺物流"
    },
    {
        value: "AXD",
        label: "安鲜达"
    },
    {
        value: "GDNFCMFXWL",
        label: "广东南方传媒发行物流"
    },
    {
        value: "XLOBO",
        label: "贝海国际速递"
    },
    {
        value: "ESDEX",
        label: "卓志速运"
    },
    {
        value: "YTOGJ",
        label: "圆通国际"
    },
    {
        value: "STWL",
        label: "速通物流"
    },
    {
        value: "WAJ",
        label: "沃埃家"
    },
    {
        value: "UBON",
        label: "优邦速运"
    },
    {
        value: "WND",
        label: "WnDirect"
    },
    {
        value: "SXXF",
        label: "山西馨富"
    },
    {
        value: "YC",
        label: "远长物流"
    },
    {
        value: "BSGJ",
        label: "百世国际"
    },
    {
        value: "YUD",
        label: "长发快递"
    },
    {
        value: "KMYRX",
        label: "昆明云瑞祥"
    },
    {
        value: "DHL",
        label: "DHL全球"
    },
    {
        value: "CN7000001011758",
        label: "泰进物流"
    },
    {
        value: "BYZH",
        label: "贝业智慧物流"
    },
    {
        value: "JQWL",
        label: "浙江鉴强物流"
    },
    {
        value: "ESB",
        label: "E速宝"
    },
    {
        value: "SZXK",
        label: "苏州熙康"
    },
    {
        value: "DDS",
        label: "点点送"
    },
    {
        value: "SUYUN123",
        label: "58速运"
    },
    {
        value: "SEND2CHINA",
        label: "Send2China"
    },
    {
        value: "CYEXP",
        label: "长宇快递"
    },
    {
        value: "TTKEU",
        label: "天天欧洲物流"
    },
    {
        value: "SQWL",
        label: "商桥物流"
    },
    {
        value: "WORLDCPS",
        label: "天马物流"
    },
    {
        value: "KXTX",
        label: "卡行天下"
    },
    {
        value: "AOYOUZGKY",
        label: "澳邮中国快运"
    },
    {
        value: "EFS",
        label: "EFSPOST"
    },
    {
        value: "AXWL",
        label: "安迅物流"
    },
    {
        value: "RUSTON",
        label: "俄速通"
    },
    {
        value: "PAKD",
        label: "平安快递"
    },
    {
        value: "TDWL_123456",
        label: "腾达物流"
    },
    {
        value: "XZB",
        label: "鑫泽邦物流"
    },
    {
        value: "HCT",
        label: "新竹物流"
    },
    {
        value: "MGSD",
        label: "美国速递"
    },
    {
        value: "SCJXWLYXGS",
        label: "余氏东风"
    },
    {
        value: "DD",
        label: "当当宅配"
    },
    {
        value: "AST",
        label: "安世通"
    },
    {
        value: "FTDKD",
        label: "富腾达快递"
    },
    {
        value: "EHAOYAO",
        label: "好药师物流"
    },
    {
        value: "RBYZEMS",
        label: "日本邮政EMS"
    },
    {
        value: "CGKD",
        label: "程光快递"
    },
    {
        value: "DOD",
        label: "门对门"
    },
    {
        value: "YDTKD",
        label: "易达通快递"
    },
    {
        value: "BHWL",
        label: "保宏物流"
    },
    {
        value: "XAJ",
        label: "新安居仓配"
    },
    {
        value: "CSZX",
        label: "城市之星"
    },
    {
        value: "XYWL",
        label: "秀驿物流"
    },
    {
        value: "LTJZPS",
        label: "蓝豚家装配送"
    },
    {
        value: "DJ56",
        label: "东骏快捷"
    },
    {
        value: "NEDA",
        label: "能达速递"
    },
    {
        value: "CHS",
        label: "重庆中环"
    },
    {
        value: "YXWLJT",
        label: "宇鑫物流"
    }
]