package com.whln.ordermax.api.controller.auth;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.api.shiro.realm.LoginType;
import com.whln.ordermax.api.shiro.realm.UserToken;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CuContactsEmail;
import com.whln.ordermax.data.domain.CuCustomer;
import com.whln.ordermax.data.domain.CuCustomerContacts;
import com.whln.ordermax.data.domain.SysUser;
import com.whln.ordermax.data.repository.CuContactsEmailRepository;
import com.whln.ordermax.data.repository.CuCustomerContactsRepository;
import com.whln.ordermax.data.repository.CuCustomerRepository;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Resource
    CuCustomerRepository cuCustomerRepository;
    @Resource
    CuCustomerContactsRepository cuCustomerContactsRepository;
    @Resource
    CuContactsEmailRepository cuContactsEmailRepository;
    @Resource
    SysService sysService;
    @Resource
    private ShiroManager shiroManager;

    @ApiOperation(value = "刷新token")
    @PostMapping("/refreshToken")
    public Result refreshToken(HttpServletRequest request) {
        String authorization = request.getHeader("Authorization");
        Serializable serializable = shiroManager.refreshToken(authorization);
        return Result.success(serializable);
    }

    @ApiOperation(value = "刷新token")
    @PostMapping("/login")
    public Result login(@RequestBody SysUser user) {
        return shiroManager.login(new UserToken(LoginType.USER_PASSWORD, user.getUsername(), user.getPassword()));
    }
    //为了改变客户id适配金蝶  慎用
    private void customerId() {
        List<CuCustomer> cuCustomers = cuCustomerRepository.list();
        List<CuCustomerContacts> contactsList = cuCustomerContactsRepository.list();
        List<CuContactsEmail> cuContactsEmailList = cuContactsEmailRepository.list();

        List<CuCustomerContacts> newContactsList = new ArrayList<>();
        List<CuContactsEmail> cuContactsEmails = new ArrayList<>();

        Map<String, List<CuCustomerContacts>> contactByCustomerId = contactsList.stream().filter(item->ObjectUtil.isNotEmpty(item.getCustomerId())).collect(Collectors.groupingBy(CuCustomerContacts::getCustomerId));
        Map<String, List<CuContactsEmail>> collect1 = cuContactsEmailList.stream().filter(item->ObjectUtil.isNotEmpty(item.getContactId())).collect(Collectors.groupingBy(CuContactsEmail::getContactId));
        List<CuCustomer> newCustomer = cuCustomers.stream().map(e -> {
            CuCustomer cuCustomer = BeanUtil.copyProperties(e, CuCustomer.class);
            cuCustomer.setId(sysService.nextId());
            List<CuCustomerContacts> contactsList1 = contactByCustomerId.get(e.getId());
            if (ObjectUtil.isNotEmpty(contactsList1)) {
                List<CuCustomerContacts> collect = contactsList1.stream().map(i -> {
                    i.setCustomerId(cuCustomer.getId());
                    CuCustomerContacts contacts = BeanUtil.copyProperties(i, CuCustomerContacts.class);
                    contacts.setId(sysService.nextId());
                    List<CuContactsEmail> cuContactsEmails1 = collect1.get(i.getId());
                    if (ObjectUtil.isNotEmpty(cuContactsEmails1)) {
                        List<CuContactsEmail> collect2 = cuContactsEmails1.stream().peek(c -> {
                            c.setId(sysService.nextId());
                            c.setContactId(contacts.getId());
                            c.setCustomerId(cuCustomer.getId());
                        }).collect(Collectors.toList());
                        cuContactsEmails.addAll(collect2);
                    }
                    return contacts;
                }).collect(Collectors.toList());
                newContactsList.addAll(collect);
            }
            return cuCustomer;
        }).collect(Collectors.toList());
        cuCustomerRepository.removeByIds(cuCustomers.stream().map(CuCustomer::getId).collect(Collectors.toList()));
        cuCustomerRepository.saveBatch(newCustomer);
        cuCustomerContactsRepository.removeByIds(contactsList.stream().map(CuCustomerContacts::getId).collect(Collectors.toList()));
        cuCustomerContactsRepository.saveBatch(newContactsList);
        cuContactsEmailRepository.removeByIds(cuContactsEmailList.stream().map(CuContactsEmail::getId).collect(Collectors.toList()));
        cuContactsEmailRepository.saveBatch(cuContactsEmails);
    }

    @ApiOperation(value = "刷新token")
    @PostMapping("/logout")
    public Result<Void> logout() {
        shiroManager.logout();
        return Result.success();
    }
}
