package com.whln.ordermax.api.Interceptor;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.PluginUtils;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.whln.ordermax.data.service.MapCache;
import com.whln.ordermax.common.entity.FilterCondition;
import com.whln.ordermax.common.enums.JudgeType;
import com.whln.ordermax.data.domain.base.FilterBill;
import org.apache.ibatis.binding.MapperMethod;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.context.ApplicationContext;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 过滤条件拦截
 */
public class FilterConditionControlInnerInterceptor implements InnerInterceptor {

    private final ApplicationContext applicationContext;
    private SqlSessionFactory sqlSessionFactory;

    public FilterConditionControlInnerInterceptor(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }


    @Override
    public void beforeQuery(Executor executor, MappedStatement mappedStatement, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
        this.sqlSessionFactory = applicationContext.getBean(SqlSessionFactory.class);
        String mapperId = mappedStatement.getId();

        int lastIndex = mapperId.lastIndexOf(".");
        String methodName = mapperId.substring(lastIndex + 1);
        if (ObjectUtil.equals("listByEntity", methodName)) {
            MapperMethod.ParamMap<?> paramMap = (MapperMethod.ParamMap<?>) parameter;
            Object entityObj = paramMap.get("entity");
            if (ObjectUtil.isEmpty(entityObj) || !(entityObj instanceof FilterBill)) {
                return;
            }
            FilterBill entity = (FilterBill) entityObj;

            List<FilterCondition> filterConditionList = entity.getFilterConditionList();
            if (filterConditionList == null || filterConditionList.size() == 0) {
                return;
            }

            String filterConditionStr = "";
            Map<String, String> entityComMapperXMap = applicationContext.getBean(MapCache.class).entityComMapperXMap();
            String mapperName = entityComMapperXMap.get(entity.getClass().getSimpleName());
            filterConditionStr = setMutiConditionList(filterConditionList, filterConditionStr, mapperName);

            // 改变sql语句
            PluginUtils.MPBoundSql mpBs = PluginUtils.mpBoundSql(boundSql);
            String oldSql = mpBs.sql();
            mpBs.sql(StrUtil.concat(true, oldSql, " and (", filterConditionStr, ")"));
        }
    }

    private String setMutiConditionList(List<FilterCondition> filterConditionList, String filterConditionStr, String mapperName) {
        for (int i = 0; i < filterConditionList.size(); i++) {
            if (i != 0) {
                filterConditionStr += " " + filterConditionList.get(i).getJoinType();
            }
            if (filterConditionList.get(i).getMutiConditionList() != null && filterConditionList.get(i).getMutiConditionList().size() > 0) {
                filterConditionStr += "(";
                filterConditionStr = this.setMutiConditionList(filterConditionList.get(i).getMutiConditionList(), filterConditionStr, mapperName);
                filterConditionStr += ")";
            } else {
                String onceCondition;
                switch (filterConditionList.get(i).getJudgeType()) {
                    case "BETWEEN":
                    case "NOT_BETWEEN":
                        onceCondition = filterConditionStr += StrUtil.concat(true, " ", StrUtil.toUnderlineCase(filterConditionList.get(i).getAttr()), " ", StrUtil.format(JudgeType.valueOf(JudgeType.class, filterConditionList.get(i).getJudgeType()).getSqlTemp(), filterConditionList.get(i).getValue().split(",")));
                        break;
                    case "IN":
                    case "NOT_IN":
                        onceCondition = filterConditionStr += StrUtil.concat(true, " ", StrUtil.toUnderlineCase(filterConditionList.get(i).getAttr()), " ", StrUtil.format(JudgeType.valueOf(JudgeType.class, filterConditionList.get(i).getJudgeType()).getSqlTemp(), CollectionUtil.join(Arrays.stream(filterConditionList.get(i).getValue().split(",")).map(v -> "'" + v + "'").collect(Collectors.toList()), ",")));
                        break;
                    default:
                        onceCondition = StrUtil.concat(true, " ", StrUtil.toUnderlineCase(filterConditionList.get(i).getAttr()), " ", StrUtil.format(JudgeType.valueOf(JudgeType.class, filterConditionList.get(i).getJudgeType()).getSqlTemp(), filterConditionList.get(i).getValue()));
                        break;
                }
                switch (filterConditionList.get(i).getAttrSource()) {
                    case "M":
                        break;
                    case "CM":
                        Configuration con = sqlSessionFactory.getConfiguration();
                        Map<String, XNode> sqlFragments = con.getSqlFragments();
                        XNode xNode = sqlFragments.get(StrUtil.format("{}.Base_select", mapperName));
                        String selectStr = xNode.getStringBody();
                        selectStr = selectStr.replace("*", "bill_id") + " where " + onceCondition;
                        onceCondition = StrUtil.format(" id in ({})", selectStr);
                        break;
                }
                filterConditionStr += onceCondition;
            }
        }
        return filterConditionStr;
    }

}
