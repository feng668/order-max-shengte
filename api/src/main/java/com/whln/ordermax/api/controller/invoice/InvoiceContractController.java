package com.whln.ordermax.api.controller.invoice;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.whln.ordermax.api.service.invoice.InvoiceContractService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.InvoiceContract;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liurun
 * @since 2023-04-23
 */
@AllArgsConstructor
@RestController
@RequestMapping("/invoice-contract")
public class InvoiceContractController {
    private final InvoiceContractService baseService;

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public void save(@RequestBody InvoiceContract entity) {
        baseService.save(entity);
    }

    @ApiOperation(value = "更新")
    @PostMapping("/update")
    public void update(@RequestBody InvoiceContract entity) {
        baseService.update(entity, new LambdaUpdateWrapper<InvoiceContract>().eq(InvoiceContract::getId, entity.getId()));
    }
    @ApiOperation(value = "删除")
    @PostMapping("/deleteById")
    public void delete(String id) {
        baseService.removeById(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public void batchDelete(List<String> id) {
        baseService.removeByIds(id);
    }

    @ApiOperation(value = "查询详情")
    @GetMapping("/get")
    public Result<InvoiceContract> get(String id) {
        InvoiceContract byId = baseService.getById(id);
        return Result.success(byId);
    }
}
