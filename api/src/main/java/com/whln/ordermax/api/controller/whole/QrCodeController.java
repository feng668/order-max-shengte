package com.whln.ordermax.api.controller.whole;

import cn.hutool.json.JSONObject;
import com.whln.ordermax.api.service.whole.QrCodeService;
import com.whln.ordermax.common.entity.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 订单进度控制器
 */
@Api(tags = "二维码生成")
@RestController
@RequestMapping("/qrCode")
public class QrCodeController {

    @Resource
    private QrCodeService baseService;

    @ApiOperation(value = "保存进度", notes =
            "content:内容"+
            "imgType:图片格式"+
            "width:宽度"+
            "height:高度"
    )
    @PostMapping("/create")
    public Result create(@RequestBody JSONObject entity) {
        return baseService.create(entity);
    }


}