package com.whln.ordermax.api.service.pricingOrder;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.param.PrPricingOrderBatchParam;
import com.whln.ordermax.data.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 核价单业务处理
 */
@Service
public class PrPricingOrderService {

    @Resource
    private PrPricingOrderRepository prPricingOrderRepository;

    @Resource
    private PrPricingCommodityRepository pricingCommodityRepository;

    @Resource
    private CmCommodityRepository cmCommodityRepository;

    @Resource
    private PrPricingBomsRepository pricingBomsRepository;

    @Resource
    private CmCommodityBomRepository commodityBomRepository;

    /**
     * 查询所有
     */
    public Result listAll(PrPricingOrder entity) {
        return Result.success(prPricingOrderRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(PrPricingOrder entity, Page<PrPricingOrder> page) {
        return Result.success(prPricingOrderRepository.pageByEntity(entity, page));
    }

    /**
     * 查询单条
     */
    public Result<PrPricingOrder> getAllById(String id) {
        return Result.success(prPricingOrderRepository.getAllById(id));
    }


    /**
     * 核价单生成商品档案
     */
    public CmCommodity toCommodity(PrPricingOrder param) {
        SysUser user = ShiroManager.getUser();
        param.setOwnerId(param.getOwnerId());
        param.setOwnerName(user.getRealname());
        CmCommodity newCommodity = CmCommodity.prPricingOrderToCommodity(param);
        CmCommodity one = cmCommodityRepository.getOne(new LambdaQueryWrapper<CmCommodity>().eq(CmCommodity::getPartNo, newCommodity.getPartNo()));
        if (ObjectUtil.isNotEmpty(one)) {
            throw new BusinessException("商品编号已存在");
        }
        cmCommodityRepository.saveOrUpdate(newCommodity);
        newCommodity.setCommodityId(newCommodity.getId());
        if (ObjectUtil.isNotEmpty(newCommodity.getId())) {
            commodityBomRepository.remove(new LambdaUpdateWrapper<CmCommodityBom>().eq(CmCommodityBom::getCommodityId, newCommodity.getId()));
        }
        List<PrPricingCommodity> commodityList = param.getCommodityList();
        if (ObjectUtil.isNotEmpty(commodityList)) {
            List<CmCommodityBom> newBomList = commodityList.stream()
                    .map(item -> {
                        CmCommodityBom cmCommodityBom = new CmCommodityBom();
                        cmCommodityBom.setCommodityId(newCommodity.getId());
                        cmCommodityBom.setPartId(item.getCommodityId());
                        cmCommodityBom.setPartNum(item.getQuotationNum());
                        return cmCommodityBom;
                    })
                    .collect(Collectors.toList());
            commodityBomRepository.saveBatch(newBomList);
        }
        return newCommodity;
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Void> save(PrPricingOrder entity) {
        PrPricingOrder prPricingOrder = prPricingOrderRepository.getOne(new LambdaQueryWrapper<PrPricingOrder>().eq(PrPricingOrder::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(prPricingOrder) && !ObjectUtil.equals(entity.getId(), prPricingOrder.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = prPricingOrderRepository.saveOrUpdate(entity);

        List<PrPricingCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream().map(PrPricingCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        pricingCommodityRepository.remove(new LambdaUpdateWrapper<PrPricingCommodity>()
                .eq(PrPricingCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(idList), PrPricingCommodity::getId, idList));
        if (CollectionUtil.isNotEmpty(commodityList)) {
            pricingCommodityRepository.saveOrUpdateBatch(entity.getCommodityList());
        }
        if (CollectionUtil.isNotEmpty(entity.getBomsDelIdList())) {
            pricingBomsRepository.removeByIds(entity.getBomsDelIdList());
        }
        if (CollectionUtil.isNotEmpty(entity.getBomsList())) {
            pricingBomsRepository.saveOrUpdateBatch(entity.getBomsList());
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    public void batchSave(PrPricingOrderBatchParam param) {
        List<PrPricingOrder> pricingOrderList = param.getPricingOrderList();
        /*pricingOrderList.stream().map(i -> {
            List<CmCommodityBom> list = commodityBomRepository.list(new LambdaQueryWrapper<CmCommodityBom>().eq(CmCommodityBom::getCommodityId, i.getCommodityId()));
            List<PrPricingBoms> bomList = PrPricingBoms.build(list);

        })*/
        prPricingOrderRepository.saveBatch(pricingOrderList);
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        PrPricingOrder one = prPricingOrderRepository.getOne(new LambdaQueryWrapper<PrPricingOrder>().eq(PrPricingOrder::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            prPricingOrderRepository.removeById(id);
            pricingCommodityRepository.remove(new QueryWrapper<PrPricingCommodity>().eq("bill_id", id));
            pricingBomsRepository.remove(new QueryWrapper<PrPricingBoms>().eq("bill_id", id));
        } else {
            prPricingOrderRepository.update(new LambdaUpdateWrapper<PrPricingOrder>().eq(PrPricingOrder::getId, id).set(PrPricingOrder::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<PrPricingOrder> enquiryOrders = prPricingOrderRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = enquiryOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = enquiryOrders.stream()
                .map(PrPricingOrder::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                prPricingOrderRepository.removeByIds(ids);
                pricingCommodityRepository.remove(new LambdaQueryWrapper<PrPricingCommodity>().in(PrPricingCommodity::getBillId, ids));
                pricingBomsRepository.remove(new LambdaQueryWrapper<PrPricingBoms>().in(PrPricingBoms::getBillId, ids));
            } else {
                prPricingOrderRepository.update(new LambdaUpdateWrapper<PrPricingOrder>().in(PrPricingOrder::getId, ids).set(PrPricingOrder::getIsDelete, 1));
            }
        }

        return Result.success();
    }

    public Result listCom(String id) {
        return Result.success(pricingCommodityRepository.listByEntity(
                new PrPricingCommodity() {{
                    setBillId(id);
                }}
        ));
    }

    public Result listBom(String id) {
        return Result.success(pricingBomsRepository.listByEntity(
                new PrPricingBoms() {{
                    setBillId(id);
                }}
        ));
    }

    public Result<List<PrPricingBoms>> listBomsByCommodity(List<PrPricingCommodity> entityList) {
        return Result.success(pricingBomsRepository.listBomsByCommodity(entityList));
    }

}