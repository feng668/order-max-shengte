package com.whln.ordermax.api.designpattern.chain.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.api.designpattern.chain.impl.ordrplan.OrderPlanChainFactory;
import com.whln.ordermax.api.designpattern.chain.impl.ordrplan.OrderPlanHandlerChain;
import com.whln.ordermax.data.domain.OrderPlan;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-02-25 16:12
 */
@Component
public class OrderPlanSetChild extends OrderPlanHandlerChain<List<OrderPlan>, OrderPlan> implements InitializingBean {
    @Override
    public void afterPropertiesSet() throws Exception {
        OrderPlanChainFactory.register(this);
    }

    @Override
    public OrderPlan handle(List<OrderPlan> orderPlans) {
        OrderPlan orderPlan = orderPlans.stream().filter(item -> ObjectUtil.isEmpty(item.getLastBillId())).findFirst().get();
        return buildTree(orderPlans, orderPlan);
    }

    public OrderPlan buildTree(List<OrderPlan> pNode, OrderPlan root) {
        String billId = root.getBillId();
        return buildChildTree(root, pNode, CollUtil.toList(billId));

    }

    /**
     * 备份
     * @param lastNode
     * @param rootList
     * @return
     */
    /*public OrderPlan buildChildTree1(OrderPlan pNode, List<OrderPlan> rootList, Map<String, List<OrderPlan>> stringListMap) {

        List<OrderPlan> orderPlans = stringListMap.get(pNode.getLastBillId()+pNode.getBillCode());
        pNode.setOrders(BeanUtil.copyToList(orderPlans, OrderPlan.class));

        Map<String, List<OrderPlan>> collect = rootList.stream().collect(Collectors.groupingBy(OrderPlan::getBillCode));

        List<OrderPlan> chirds = pNode.getChirds();
        List<OrderPlan> childs = new ArrayList<>();
        for (OrderPlan o : chirds) {
            List<OrderPlan> orderPlans1 = collect.get(o.getBillCode());
            if (ObjectUtil.isNotEmpty(orderPlans1)) {
                List<OrderPlan> collect1 = orderPlans1.stream().filter(i -> {
                    return ObjectUtil.equals(i.getLastBillId(), pNode.getBillId());
                }).map(item -> {
                    o.setBillId(item.getBillId());
                    o.setBillStatus(item.getBillStatus());
                    o.setLastBillId(item.getLastBillId());
                    o.setLastBillCode(item.getLastBillCode());
                    return o;
                }).collect(Collectors.toList());
            }
            OrderPlan orderPlan = BeanUtil.copyProperties(o, OrderPlan.class);
            buildChildTree(orderPlan, rootList, stringListMap);
            childs.add(orderPlan);


        }
        pNode.addChild(childs);
        return pNode;
    }
    */
    public OrderPlan buildChildTree(OrderPlan lastNode, List<OrderPlan> rootList,List<String> ids) {
        List<OrderPlan> orderPlanList = rootList.stream().filter(item -> {
            return ids.contains(item.getBillId())&&ObjectUtil.equals(item.getLastBillCode(),lastNode.getLastBillCode());
        }).collect(Collectors.toList());
        lastNode.setOrders(BeanUtil.copyToList(orderPlanList, OrderPlan.class));
        Map<String, List<OrderPlan>> collect = rootList.stream().collect(Collectors.groupingBy(OrderPlan::getBillCode));

        List<OrderPlan> chirds = lastNode.getChirds();
        List<OrderPlan> childs = new ArrayList<>();
        for (OrderPlan o : chirds) {
            List<OrderPlan> orderPlans1 = collect.get(o.getBillCode());
            if (ObjectUtil.isNotEmpty(orderPlans1)) {
                List<OrderPlan> collect1 = orderPlans1.stream().filter(i -> {
                    return ids.contains(i.getLastBillId());
                }).map(item -> {
                    o.setBillId(item.getBillId());
                    o.setBillCode(item.getBillCode());
                    o.setBillStatus(item.getBillStatus());
                    o.setLastBillId(item.getLastBillId());
                    o.setLastBillCode(item.getLastBillCode());
                    return o;
                }).collect(Collectors.toList());
                if (ObjectUtil.isNotEmpty(collect1)) {
                    OrderPlan orderPlan1 = collect1.get(0);
                    OrderPlan orderPlan = BeanUtil.copyProperties(orderPlan1, OrderPlan.class);
                    List<String> nextIds= orderPlans1.stream().map(OrderPlan::getBillId).collect(Collectors.toList());
                    buildChildTree(orderPlan, rootList,nextIds);
                    childs.add(orderPlan);
                }
            }else {
                childs.add(o);
            }


        }
        lastNode.addChild(childs);
        return lastNode;
    }

}
