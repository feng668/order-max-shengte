package com.whln.ordermax.api.service.template;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelXorHtmlUtil;
import cn.afterturn.easypoi.excel.entity.ExcelToHtmlParams;
import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.spire.doc.Document;
import com.spire.doc.FileFormat;
import com.spire.doc.documents.XHTMLValidationType;
import com.whln.ordermax.api.config.FreeMarkerRender;
import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategy;
import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategyFactory;
import com.whln.ordermax.api.service.auth.AuthService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.utils.IoUtils;
import com.whln.ordermax.common.utils.OmQrCodeUtil;
import com.whln.ordermax.data.domain.EmInboxAttachment;
import com.whln.ordermax.data.domain.TmPrintTemplate;
import com.whln.ordermax.data.domain.param.RenderTemplateParam;
import com.whln.ordermax.data.mapper.EmInboxAttachmentMapper;
import com.whln.ordermax.data.repository.QoQuoteCommodityRepository;
import com.whln.ordermax.data.repository.QoQuoteOrderRepository;
import com.whln.ordermax.data.repository.TmPrintTemplateRepository;
import com.whln.ordermax.data.service.MapCache;
import com.whln.ordermax.fileOperate.FileTransform;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.*;


/**
 * 报关代码业务处理
 */
@Service
public class PrintTemplateService {

    @Resource
    private QoQuoteOrderRepository qoQuoteOrderRepository;

    @Resource
    private QoQuoteCommodityRepository commodityRepository;

    @Resource
    private FileTransform fileTransform;

    @Value("${filestore.root}")
    private String rootPath;

    @Value("${filestore.printtemplate}")
    private String basePath;

    @Resource
    private MapCache mapService;

    @Resource
    private FreeMarkerConfigurer freeMarkerConfigurer;

    @Resource
    private TemplateStrategyFactory templateStrategyFactory;

    public void showQueteExcel(String queteId, HttpServletResponse response) throws Exception {

//        获取模板文档
        File rootFile = new File(ResourceUtils.getURL("classpath:").getPath()); //获取项目的根目录\
//        TemplateExportParams params = new TemplateExportParams(
//                ResourceUtils.getURL("classpath:").getPath()+"/word_template/QT 报价单(Quotation) 模板.xls");
        TemplateExportParams params = new TemplateExportParams(rootPath + "template/报价单.xls");
//        File templateFile = new File(rootFile, "/word_template/QT 报价单(Quotation) 模板.xls");
//        准备数据
        Map<String, Object> map = qoQuoteOrderRepository.getByIdWithInfo(queteId);


        List<Map> commodityList = commodityRepository.getMapByIdWithInfo(queteId);
        for (int i = 0; i < commodityList.size(); i++) {
            commodityList.get(i).put("index", i + 1);
            commodityList.get(i).put("amo", (Integer) commodityList.get(i).get("quotation_num") * (Double) commodityList.get(i).get("price"));
        }
        map.put("commodityList", commodityList);

//        模板文档结合数据
        Workbook workbook = ExcelExportUtil.exportExcel(params, map);
        Sheet sheet = workbook.getSheet("Sheet1");
        int lastRow = sheet.getLastRowNum();
        for (int i = 0; i < lastRow; i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                sheet.createRow(i);
            }
        }

        ExcelToHtmlParams excelToHtmlParams = new ExcelToHtmlParams(workbook, true, "yes");
        String excelToHtml = ExcelXorHtmlUtil.excelToHtml(excelToHtmlParams);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        response.getOutputStream().write(excelToHtml.getBytes(StandardCharsets.UTF_8));
    }

    public void downloadQueteExcel(String queteId, HttpServletResponse response) throws Exception {
        //        获取模板文档
//        TemplateExportParams params = new TemplateExportParams(
//                ResourceUtils.getURL("classpath:").getPath()+"/word_template/QT 报价单(Quotation) 模板.xls");
        TemplateExportParams params = new TemplateExportParams(rootPath + "template/QT 报价单(Quotation) 模板.xls");
//        File templateFile = new File(rootFile, "/word_template/QT 报价单(Quotation) 模板.xls");
//        准备数据
        Map<String, Object> map = qoQuoteOrderRepository.getByIdWithInfo(queteId);


        List<Map> commodityList = commodityRepository.getMapByIdWithInfo(queteId);
        for (int i = 0; i < commodityList.size(); i++) {
            commodityList.get(i).put("index", i + 1);
            commodityList.get(i).put("amo", (Integer) commodityList.get(i).get("quotation_num") * (Double) commodityList.get(i).get("price"));
        }
        map.put("commodityList", commodityList);

//        模板文档结合数据
        Workbook workbook = ExcelExportUtil.exportExcel(params, map);

//        ExcelToHtmlParams excelToHtmlParams = new ExcelToHtmlParams(WorkbookFactory.create(POICacheManager.getFile("D:/IdeaProjects/ywc/api/target/classes/word_template/QT 报价单(Quotation) 模板.xls")),true,"yes");
//        ExcelToHtmlParams excelToHtmlParams = new ExcelToHtmlParams(workbook,true,"yes");
//        String s = ExcelXorHtmlUtil.excelToHtml(excelToHtmlParams);
//        System.out.println(s);
//        byte[] bytes = ExcelXorHtmlUtil.excelToHtml(excelToHtmlParams).getBytes();
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("text/html;charset=UTF-8");
//        response.getOutputStream().write(bytes);

        String filename = "报价单.xls";
        response.setHeader("content-disposition", "attachment;filename=" + new String(filename.getBytes(), "ISO8859-1"));
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        workbook.write(response.getOutputStream());
    }


    public void showExcel(Map body, HttpServletResponse response) throws Exception {
        //获取模板文档
        String billCode = body.get("billCode").toString();
        Map<String, String> codeTemplatePathMap = mapService.codeTemplatePathMap();
        String templatePath = codeTemplatePathMap.get(billCode);
        TemplateExportParams params = new TemplateExportParams(rootPath + "template/" + templatePath);
        Map entity = (Map) body.get("entity");
        Workbook workbook = ExcelExportUtil.exportExcel(params, entity);
        Sheet sheet = workbook.getSheet("Sheet1");
        int lastRow = sheet.getLastRowNum();
        for (int i = 0; i < lastRow; i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                sheet.createRow(i);
            }
        }
        ExcelToHtmlParams excelToHtmlParams = new ExcelToHtmlParams(workbook, true, "yes");
        byte[] bytes = ExcelXorHtmlUtil.excelToHtml(excelToHtmlParams).getBytes();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
    }


    public void showQueteExcel2(String queteId, HttpServletResponse response) throws Exception {
        //        获取模板文档
        File rootFile = new File(ResourceUtils.getURL("classpath:").getPath()); //获取项目的根目录\
//        TemplateExportParams params = new TemplateExportParams(
//                ResourceUtils.getURL("classpath:").getPath()+"/word_template/QT 报价单(Quotation) 模板.xls");
        TemplateExportParams params = new TemplateExportParams(rootPath + "template/报价单.xls");
//        File templateFile = new File(rootFile, "/word_template/QT 报价单(Quotation) 模板.xls");
//        准备数据
        Map<String, Object> map = qoQuoteOrderRepository.getByIdWithInfo(queteId);


        List<Map> commodityList = commodityRepository.getMapByIdWithInfo(queteId);
        for (int i = 0; i < commodityList.size(); i++) {
            commodityList.get(i).put("index", i + 1);
            commodityList.get(i).put("amo", (Integer) commodityList.get(i).get("quotation_num") * (Double) commodityList.get(i).get("price"));
        }
        map.put("commodityList", commodityList);

//        模板文档结合数据
        Workbook workbook = ExcelExportUtil.exportExcel(params, map);
        Sheet sheet = workbook.getSheet("Sheet1");
        int lastRow = sheet.getLastRowNum();
        for (int i = 0; i < lastRow; i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                sheet.createRow(i);
            }
        }

        System.out.println();
        // 配置对象
        Configuration configuration = freeMarkerConfigurer.getConfiguration();
//        configuration.setClassicCompatible(true);
        configuration.setDirectoryForTemplateLoading(new File("D:\\template\\"));

        // 获取模板
        Template template = configuration.getTemplate("aaa\\text2.ftl");

        // 输出为字符串
        StringWriter resultStr = new StringWriter();

        // 渲染模板
        template.process(map, resultStr);
        // 关闭流
        resultStr.close();

        System.out.println(resultStr);

//        ExcelToHtmlParams excelToHtmlParams = new ExcelToHtmlParams(WorkbookFactory.create(POICacheManager.getFile("D:/IdeaProjects/ywc/api/target/classes/word_template/QT 报价单(Quotation) 模板.xls")),true,"yes");
        ExcelToHtmlParams excelToHtmlParams = new ExcelToHtmlParams(workbook, true, "yes");
        String s = ExcelXorHtmlUtil.excelToHtml(excelToHtmlParams);
        System.out.println(s);
        byte[] bytes = ExcelXorHtmlUtil.excelToHtml(excelToHtmlParams).getBytes();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write(resultStr.toString());
    }

    @Resource
    private TmPrintTemplateRepository tmPrintTemplateRepository;

    @Value("${filestore.printtemplate}")
    private String printTemplatePath;
    @Value("${filestore.upload}")
    private String filePathUpload;

    public void preview(Map body, HttpServletResponse response) throws Exception {
        //        获取模板文档
        String templateId = body.get("templateId").toString();
        TmPrintTemplate printTemplate = tmPrintTemplateRepository.getAllById(templateId);

        // 配置对象
        Configuration configuration = freeMarkerConfigurer.getConfiguration();
        configuration.setDirectoryForTemplateLoading(new File(printTemplatePath));
        // 获取模板
        Template template = null;
        try {
            template = configuration.getTemplate(printTemplate.getFltPath());
        } catch (TemplateNotFoundException e) {
            throw new BusinessException("模板文件丢失");
        }
        //获取数据
        Map<String, Object> entity = (Map<String, Object>) body.get("entity");

        // 输出为字符串
        StringWriter resultStr = new StringWriter();
        // 渲染模板
        template.process(entity, resultStr);
        // 关闭流
        resultStr.close();

        String result = resultStr.toString();

        org.jsoup.nodes.Document html = Jsoup.parse(result);
        Element htmlBody = html.body();
        Elements td = htmlBody.select("td");

        Elements qrImgList = htmlBody.select("[isqr='isqr']");
        if (qrImgList.size() > 0) {
            for (Element element : qrImgList) {
                String qrcontent = element.attr("qrcontent");
                String codeBase64 = OmQrCodeUtil.writeToBase64(qrcontent, "png", 100, 100);
                element.attr("src", "data:image/JPG;base64," + codeBase64);
            }
            result = html.toString();
        }


        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write(result);
    }

    public Result<EmInboxAttachment> saveFileByTempId(Map body, HttpServletResponse response) throws Exception {
//        获取模板文档
        String templateId = body.get("templateId").toString();
        TmPrintTemplate printTemplate = tmPrintTemplateRepository.getAllById(templateId);

        // 配置对象
        Configuration configuration = freeMarkerConfigurer.getConfiguration();
        configuration.setDirectoryForTemplateLoading(new File(printTemplatePath));
        // 获取模板
        Template template = configuration.getTemplate(printTemplate.getFltPath());
        //获取数据
        Map entity = (Map) body.get("entity");

        // 输出为字符串
        StringWriter resultStr = new StringWriter();
        // 渲染模板
        template.process(entity, resultStr);
        // 关闭流
        resultStr.close();
        String content = resultStr.toString();
        String outputType = body.get("outputType").toString();
//        response.setHeader("content-disposition", "attachment;filename=" + "文件");
        String s = UUID.randomUUID().toString();
        EmInboxAttachment emInboxAttachment = new EmInboxAttachment();

        emInboxAttachment.setCreateTime(LocalDateTime.now());
        emInboxAttachment.setOwnerId(AuthService.getUserIdFromAuthentication());

        String sep = File.separator;
        String subDir = LocalDateTimeUtil.format(LocalDateTime.now(), "yyyy" + sep + "MM" + sep + "dd" + sep + "HH" + sep + "mm" + sep);
        File dir = FileUtil.mkdir(filePathUpload + subDir);
        switch (outputType) {
            case "WORD":
                s += ".docx";
                String relativePath = StrUtil.concat(true, sep, subDir, s);
                emInboxAttachment.setAttachmentName(s);
                emInboxAttachment.setFileType("docx");
                emInboxAttachment.setFilePath(relativePath);
                File file1 = new File(filePathUpload + "/" + relativePath);
                if (!file1.exists()) {
                    file1.createNewFile();
                }
                fileTransform.htmlToWord(content, new FileOutputStream(filePathUpload + "/" + relativePath));
                break;
            case "EXCEL":
                s += ".xlsx";
                relativePath = StrUtil.concat(true, sep, subDir, s);
                emInboxAttachment.setAttachmentName(s);
                emInboxAttachment.setFileType("xlsx");
                emInboxAttachment.setFilePath(relativePath);
                File file = FileUtil.writeString(content, filePathUpload + "/" + relativePath, Charset.defaultCharset());
                break;
//                fileTransform.htmlToExcel(content, response);
//                break;
            case "PDF":
                s += ".pdf";
                relativePath = StrUtil.concat(true, sep, subDir, s);
                emInboxAttachment.setAttachmentName(s);
                emInboxAttachment.setFileType("pdf");
                emInboxAttachment.setFilePath(relativePath);
                file1 = new File(filePathUpload + "/" + relativePath);
                if (!file1.exists()) {
                    file1.createNewFile();
                }
                fileTransform.html2Pdf(content, new FileOutputStream(filePathUpload + "/" + relativePath));
                break;
            default:
                break;
        }
        int insert = emInboxAttachmentMapper.insert(emInboxAttachment);

        return Result.success(emInboxAttachment);
    }

    @Resource
    EmInboxAttachmentMapper emInboxAttachmentMapper;

    public void downloadByTempId(Map body, HttpServletResponse response) throws Exception {
//        获取模板文档
        String templateId = body.get("templateId").toString();
        TmPrintTemplate printTemplate = tmPrintTemplateRepository.getAllById(templateId);

        // 配置对象
        Configuration configuration = freeMarkerConfigurer.getConfiguration();
        configuration.setDirectoryForTemplateLoading(new File(printTemplatePath));
        // 获取模板
        Template template = configuration.getTemplate(printTemplate.getFltPath());
        //获取数据
        Map entity = (Map) body.get("entity");

        // 输出为字符串
        StringWriter resultStr = new StringWriter();

        // 渲染模板
        template.process(entity, resultStr);
        // 关闭流
        resultStr.close();

        System.out.println(resultStr);
        String content = resultStr.toString();
        String outputType = body.get("outputType").toString();
//        response.setHeader("content-disposition", "attachment;filename=" + "文件");
        response.setHeader("Accept-Charset", "utf-8");
        switch (outputType) {
            case "WORD":
                response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                fileTransform.htmlToWord(content, response.getOutputStream());
                break;
            case "EXCEL":
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                response.setCharacterEncoding("UTF-8");
                response.setContentType("text/html;charset=utf-8");
                PrintWriter writer = response.getWriter();
                writer.write(content);
                return;
//                fileTransform.htmlToExcel(content, response);
//                break;
            case "PDF":
                fileTransform.html2Pdf(content, response.getOutputStream());
                break;
            default:
                break;
        }
    }

    public void downloadWord(String content, String outputType, HttpServletResponse response) throws Exception {
        response.setHeader("content-disposition", "attachment;filename=" + "文件");

        switch (outputType) {
            case "WORD":
                response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                fileTransform.htmlToWord(content, response.getOutputStream());
                break;
            case "EXCEL":
                response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                fileTransform.htmlToExcel(content, response.getOutputStream());
                break;
            default:
                break;
        }
    }

    public void downloadWord(HttpServletResponse response) throws Exception {
        //        获取模板文档
        /*String templateId = body.get("templateId").toString();
        TmPrintTemplate printTemplate = tmPrintTemplateRepository.getAllById(templateId);

        // 配置对象
        Configuration configuration = freeMarkerConfigurer.getConfiguration();
        configuration.setDirectoryForTemplateLoading(new File(printTemplatePath));
        // 获取模板
        Template template = configuration.getTemplate(printTemplate.getFltPath());
        //获取数据
        Map entity = (Map) body.get("entity");

        // 输出为字符串
        StringWriter resultStr = new StringWriter();


        // 渲染模板
        template.process(entity, resultStr);
        // 关闭流
        resultStr.close();

        ByteArrayInputStream inputStream = IoUtil.toStream(resultStr.toString(), Charset.forName("utf8"));*/
        Document doc = new Document();
//        doc.loadFromStream(inputStream, FileFormat.Html, XHTMLValidationType.None);
        doc.loadFromFile("C:\\Users\\zz\\Desktop\\tem20.html", FileFormat.Html, XHTMLValidationType.None);
        response.setHeader("content-disposition", "attachment;filename=" + "文件");
        response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        doc.saveToStream(response.getOutputStream(), FileFormat.Docx_2013);

//        response.getWriter().write(resultStr.toString());
    }


    public Result htmlTemlateFromFile(MultipartFile file, HttpServletResponse response) throws Exception {
        String originalFilename = file.getOriginalFilename();
        String suffix = FileUtil.getSuffix(originalFilename);
        String htmlStr;
        if (StrUtil.equalsAny(suffix, true, "doc", "docx")) {
            htmlStr = fileTransform.wordToHtml(file);
        } else if (StrUtil.equalsAny(suffix, true, "xls", "xlsx")) {
            htmlStr = fileTransform.excelToHtml(file);
        } else {
            return Result.error("201", "文件类型不正确");
        }
        return Result.success(htmlStr);
    }


    public void getTemplate(String module, String fileName, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        StringBuilder sb = readFileToString(basePath + "/" + module + "/" + fileName);
        ServletOutputStream outputStream = response.getOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(outputStream);
        writer.write(sb.toString());
        writer.close();
    }

    private StringBuilder readFileToString(String path) throws IOException {
        File file = new File(path);
        InputStreamReader reader = null;
        try {
            reader = new InputStreamReader(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new BusinessException("未找到对应模板");
        }
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder sb = new StringBuilder();

        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line);
        }
        bufferedReader.close();
        return sb;
    }


    public void renderTemplate(RenderTemplateParam param, HttpServletResponse response) throws IOException {
        TmPrintTemplate tmPrintTemplate = tmPrintTemplateRepository.getAllById(param.getTemplateId());
        TemplateStrategy<RenderTemplateParam, TmPrintTemplate, String> strategy = templateStrategyFactory.getStrategy(tmPrintTemplate.getBillCode());
        String html = strategy.handle(param, tmPrintTemplate);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        IoUtils.write(response.getOutputStream(), html);
    }

    @Resource
    FreeMarkerRender freeMarkerRender;

    public static void main(String[] args) throws FileNotFoundException {
        OmQrCodeUtil.writeToStream("abvsdvbds", "jpg", new FileOutputStream("d://assa.jpg"), 200, 200);
    }
    public void previewQRCode(Map body, HttpServletResponse response) throws IOException, TemplateException {
        //        获取模板文档
        String templateId = body.get("templateId").toString();
        TmPrintTemplate printTemplate = tmPrintTemplateRepository.getAllById(templateId);
        // 配置对象
        Configuration configuration = freeMarkerConfigurer.getConfiguration();
        configuration.setDirectoryForTemplateLoading(new File(printTemplatePath));
        String bodyStr = JSON.toJSONString(body);
        JSONObject jsonObject = JSON.parseObject(bodyStr);
        JSONObject entity = jsonObject.getJSONObject("entity");
        JSONArray lotList = entity.getJSONArray("lotList");

        // 获取模板
        Template template = null;
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("commodityList", lotList);
            template = configuration.getTemplate(printTemplate.getHtmlPath());
            String s = template.toString();
            org.jsoup.nodes.Document html = Jsoup.parse(s);
            html.charset(Charset.defaultCharset());
            Element htmlBody = html.body();
            Elements table = htmlBody.select("table");
            table.attr("style", " border: 1px solid #000000;");
            String parseStr = "\n<#if commodityList?? && (commodityList?size > 0)>\n<#list commodityList as t>\n" + table.toString() + "\n</#list>\n</#if>\n";
            table.before("\n<!--#ftl#\n" + parseStr + "\n#ftl#-->");
            table.remove();
            String s1 = html.toString().replaceAll("<!--#ftl#", "").replaceAll("#ftl#-->", "");
            System.out.println(s1);
            Optional<String> tplText = freeMarkerRender.getTplText(s1, map);
            String s2 = tplText.get();
            org.jsoup.nodes.Document resultHtml = Jsoup.parse(s2);
            Element body1 = resultHtml.body();

            Elements qrcode = body1.select("[qrcode='qrcode']");
            for (Element q : qrcode) {
                String val = q.val();
                if (ObjectUtil.isNotEmpty(val)) {
                    String base64 =QrCodeUtil.generateAsBase64(val, new QrConfig(90, 90), "png");
//                    String base64 = OmQrCodeUtil.createQRCode(val);
                    q.after("<img style='width:90px;height:90px' src='" + base64 + "'/>");
                }
                q.remove();

            }
            Elements button = body1.select("button");
            for (Element b : button) {
                String val = b.val();
                if (ObjectUtil.isNotEmpty(b)&&ObjectUtil.isNotEmpty(val)) {
                    String style = b.attr("style");
                    b.after("<span '"+style+"'>" + val + "</span>");
                    b.remove();
                }
            }
            Elements span = body1.select("span");
            for (Element e : span) {
                Element parent = e.parent();
                if (parent.tagName().equals("button")) {
                    String val = parent.val();
                    e.text(val);
                }
            }
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter writer = response.getWriter();
            writer.write(resultHtml.outerHtml());
        } catch (TemplateNotFoundException e) {
            throw new BusinessException("模板文件丢失");
        }
    }
}