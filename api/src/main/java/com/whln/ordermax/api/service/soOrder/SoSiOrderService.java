package com.whln.ordermax.api.service.soOrder;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowCommodity;
import com.whln.ordermax.api.annotations.OaBillSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.OaRule;
import com.whln.ordermax.data.domain.OrderPlan;
import com.whln.ordermax.data.domain.SoSiOrder;
import com.whln.ordermax.data.domain.SoSiOrderCommodity;
import com.whln.ordermax.data.repository.OaRuleRepository;
import com.whln.ordermax.data.repository.OrderPlanRepository;
import com.whln.ordermax.data.repository.SoSiOrderCommodityRepository;
import com.whln.ordermax.data.repository.SoSiOrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 销售合同业务处理
 */
@BillCode(ModelMessage.SO_SI_ORDER)
@Service
public class SoSiOrderService {
    @Resource
    private SoSiOrderRepository baseRepository;
    @Resource
    private OrderPlanRepository orderPlanRepository;
    @Resource
    private SoSiOrderCommodityRepository commodityRepository;

    /**
     * 查询所有
     */
    public Result listAll(SoSiOrder entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(SoSiOrder entity, Page<SoSiOrder> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    @Resource
    private OaRuleRepository oaRuleRepository;

    /**
     * 插入或更新
     */
    @OaBillSave
    @FlowCommodity
    @Transactional(rollbackFor = Exception.class)
    public Result save(SoSiOrder entity) {
        SoSiOrder soSiOrder = baseRepository.getOne(new LambdaQueryWrapper<SoSiOrder>().eq(SoSiOrder::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(soSiOrder) && !ObjectUtil.equals(entity.getId(), soSiOrder.getId())) {
            throw new BusinessException("编号已存在");
        }
        List<OaRule> list = oaRuleRepository.list(new LambdaQueryWrapper<OaRule>().eq(OaRule::getBillCode, ModelMessage.SO_SI_ORDER.getCode()));
        if (ObjectUtil.isEmpty(list)){
            entity.setOaStatus(2);
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<SoSiOrderCommodity> commodityList = entity.getCommodityList();
        List<String> commodityIdList = commodityList.stream().map(SoSiOrderCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<SoSiOrderCommodity>()
                .eq(SoSiOrderCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(commodityIdList), SoSiOrderCommodity::getId, commodityIdList)
        );
        if (CollectionUtil.isNotEmpty(commodityList)) {
            commodityRepository.saveOrUpdateBatch(commodityList.stream()
                    .peek(e->e.setBillId(entity.getId()))
                    .collect(Collectors.toList()));
        }

        OrderPlan orderPlan = new OrderPlan();
        orderPlan.setBillCode(ModelMessage.SO_SI_ORDER.getCode());
        orderPlan.setBillId(entity.getId());
        orderPlan.setBillStatus(1);
        orderPlan.setTemId(3);
        orderPlan.setLastBillId(entity.getId());
        orderPlan.setLastBillCode(ModelMessage.SO_SI_ORDER.getCode());
        orderPlan.setBillNo(entity.getBillNo());
        orderPlan.setRootBillId(entity.getId());
        orderPlan.setCreateTime(LocalDateTime.now());
        orderPlan.setUpdateTime(LocalDateTime.now());
        orderPlanRepository.save(orderPlan);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> delete(String id) {
        SoSiOrder one = baseRepository.getOne(new LambdaQueryWrapper<SoSiOrder>().eq(SoSiOrder::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            commodityRepository.remove(new QueryWrapper<SoSiOrderCommodity>().eq("bill_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<SoSiOrder>().eq(SoSiOrder::getId, id).set(SoSiOrder::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<SoSiOrder> quoteOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = quoteOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = quoteOrders.stream()
                .map(SoSiOrder::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new QueryWrapper<SoSiOrderCommodity>().in("bill_id", ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<SoSiOrder>().in(SoSiOrder::getId, ids).set(SoSiOrder::getIsDelete, 1));
            }
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result listCom(SoSiOrderCommodity entity) {
        return Result.success(commodityRepository.listByEntity(entity));
    }


    public SoSiOrder getSoSiOrder(String id) {
        return baseRepository.getSoSiOrder(id);
    }
}