package com.whln.ordermax.api.service.finance;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.customer.CuBigCustomerNormService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.FnProfit;
import com.whln.ordermax.data.repository.FnProfitRepository;
import com.whln.ordermax.data.repository.OrderPlanRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 *  利润中心业务处理
 */
@Service
public class FnProfitService{
    @Resource
    private FnProfitRepository baseRepository;

    @Resource
    private OrderPlanRepository orderPlanRepository;

    @Resource
    private CuBigCustomerNormService bigCustomerNormService;

    /**
    *  查询所有
    */
    public Result listAll(FnProfit entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(FnProfit entity, Page<FnProfit> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    @Transactional(rollbackFor = Exception.class)
    public Result save(FnProfit entity) {
        FnProfit fnProfit = baseRepository.getOne(new LambdaQueryWrapper<FnProfit>().eq(FnProfit::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(fnProfit)&&!ObjectUtil.equals(entity.getId(),fnProfit.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }
}