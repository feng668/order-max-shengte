package com.whln.ordermax.api.controller.quote;

import com.whln.ordermax.api.service.quote.QoQuoteCommodityPriceService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.QoQuoteCommodityPrice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  报价单商品价格控制器
 */
@Api(tags = "报价单商品价格")
@RestController
@RequestMapping("/QoQuoteCommodityPrice")
public class QoQuoteCommodityPriceController{

    @Resource
    private QoQuoteCommodityPriceService baseService;

    @ApiOperation(value = "通过商品id和客户id获取最近一次报价|暂不使用")
    @PostMapping("/listNewPriceByComIdAndCustId")
    public Result<List<QoQuoteCommodityPrice>> listByComIdAndCustId(@RequestParam("commodityId") String commodityId, @RequestParam("customerId") String customerId){
        return baseService.listNewPriceByComIdAndCustId(commodityId,customerId);
    }

}