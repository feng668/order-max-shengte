package com.whln.ordermax.api.controller.stock;

import com.whln.ordermax.api.service.stock.StInOrderCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.StInOrderCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  入库单商品控制器
 */
@Api(tags = "入库单商品")
@RestController
@RequestMapping("/StInOrderCommodity")
public class StInOrderCommodityController{

    @Resource
    private StInOrderCommodityService baseService;

    @ApiOperation(value = "根据入库单id查询")
    @PostMapping("/list")
    public Result<List<StInOrderCommodity>> listAll(@RequestBody StInOrderCommodity entity){
        return baseService.listAll(entity);
    }

}