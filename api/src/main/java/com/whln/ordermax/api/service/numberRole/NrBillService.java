package com.whln.ordermax.api.service.numberRole;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.NrBill;
import com.whln.ordermax.data.domain.NrRoleNode;
import com.whln.ordermax.data.repository.NrBillRepository;
import com.whln.ordermax.data.repository.NrRoleNodeRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 *  编号规则单据业务处理
 */
@Service
public class NrBillService{
    @Resource
    private NrBillRepository baseRepository;

    @Resource
    private NrRoleNodeRepository roleNodeRepository;

    /**
    *  查询所有
    */
    public Result listAll(NrBill entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(NrBill entity, Page<NrBill> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(NrBill entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
     *  插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    public Result saveRole(NrBill entity) {
        boolean b = true;
        if (CollectionUtil.isNotEmpty(entity.getNrRoleDelIdList())){
            roleNodeRepository.removeByIds(entity.getNrRoleDelIdList());
        }
        if (CollectionUtil.isNotEmpty(entity.getRoleNodeList())){
            roleNodeRepository.saveOrUpdateBatch(entity.getRoleNodeList());
        }
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        roleNodeRepository.remove(new QueryWrapper<NrRoleNode>().eq("bill_id",id));
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

}