package com.whln.ordermax.api.service.whole;

import cn.hutool.json.JSONObject;
import org.springframework.stereotype.Service;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.utils.OmQrCodeUtil;

/**
 * 订单进度业务处理
 */
@Service
public class QrCodeService {

    public Result create(JSONObject entity) {
        return Result.success(
                OmQrCodeUtil.writeToBase64(
                        entity.getStr("content"),
                        entity.getStr("imgType"),
                        entity.getInt("width"),
                        entity.getInt("height"))
        );
    }
}