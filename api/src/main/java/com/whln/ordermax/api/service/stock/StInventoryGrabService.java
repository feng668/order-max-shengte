package com.whln.ordermax.api.service.stock;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.StInventoryGrab;
import com.whln.ordermax.data.repository.StInventoryGrabRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  业务处理
 */
@Service
public class StInventoryGrabService{
    @Resource
    private StInventoryGrabRepository baseRepository;


    /**
    *  查询所有
    */
    public Result listAll(StInventoryGrab entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(StInventoryGrab entity, Page<StInventoryGrab> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  查询单条
    */
    public Result<StInventoryGrab> getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }

    /**
    *  插入或更新
    */
    public Result save(StInventoryGrab entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

    public Result<List<StInventoryGrab>> listComGrabByTransportationId(JSONObject entity) {
        String billId = entity.getStr("billId");
        String commodityId = entity.getStr("commodityId");
        if (!StrUtil.isAllNotEmpty(billId,commodityId)){
            throw new BusinessException("缺少必要参数");
        }
        return Result.success(baseRepository.listComGrabByTransportationId(billId,commodityId));
    }
}