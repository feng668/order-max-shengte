package com.whln.ordermax.api.service.sys;

import com.whln.ordermax.data.domain.param.DeleteParam;
import com.whln.ordermax.data.domain.param.UserLabelGroupParam;
import com.whln.ordermax.data.domain.vo.CustomerLabelGroupVO;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-07 9:26
 */
public interface UserLabelGroupService {
    /**
     * 保存用户组
     *
     * @param userLabelGroupVO
     */
    void save(CustomerLabelGroupVO userLabelGroupVO);

    void deleteGroup(DeleteParam param);

    /**
     * 查询用户组列表
     * @return
     */
    List<CustomerLabelGroupVO> list(UserLabelGroupParam pram);
}
