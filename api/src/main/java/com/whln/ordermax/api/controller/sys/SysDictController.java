package com.whln.ordermax.api.controller.sys;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SysDict;
import com.whln.ordermax.data.repository.SysDictRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liurun
 * @date 2023-04-21 20:22
 */
@Api(tags = "字典")
@RestController
@RequestMapping("/sysDict")
public class SysDictController {
    @Resource
    SysDictRepository sysDictRepository;


    @ApiOperation("查询所有数据字典")
    @GetMapping("listSysDict")
    public Result<List<SysDict>> listSysDict() {
        List<SysDict> sysDicts = sysDictRepository.listSysDict();
        return Result.success(sysDicts);
    }

    @ApiOperation("删除")
    @PostMapping("/delete")
    public Result<Void> delete(String id) {
        sysDictRepository.removeById(id);
        return Result.success();
    }

    @ApiOperation("更新")
    @PostMapping("/update")
    public Result<Void> update(@RequestBody SysDict sysDict) {
        sysDictRepository.updateById(sysDict);
        return Result.success();
    }

    @ApiOperation("保存")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody SysDict sysDict) {
        sysDictRepository.save(sysDict);
        return Result.success();
    }
}
