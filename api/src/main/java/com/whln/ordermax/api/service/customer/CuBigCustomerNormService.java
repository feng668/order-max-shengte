package com.whln.ordermax.api.service.customer;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CuBigCustomerNorm;
import com.whln.ordermax.data.domain.CuCustomer;
import com.whln.ordermax.data.domain.FnProfit;
import com.whln.ordermax.data.domain.TrTransportation;
import com.whln.ordermax.data.repository.CuBigCustomerNormRepository;
import com.whln.ordermax.data.repository.CuCustomerRepository;
import com.whln.ordermax.data.repository.FnProfitRepository;
import com.whln.ordermax.data.repository.TrTransportationRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * 大客户指标业务处理
 */
@Service
public class CuBigCustomerNormService {
    @Resource
    private CuBigCustomerNormRepository baseRepository;

    /**
     * 查询所有
     */
    public Result listAll(CuBigCustomerNorm entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(CuBigCustomerNorm entity, Page<CuBigCustomerNorm> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 查询单条
     */
    public Result<CuBigCustomerNorm> getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }

    /**
     * 插入或更新
     */
    public Result save(CuBigCustomerNorm entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 批量删除
     */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    @Resource
    private TrTransportationRepository transportationRepository;

    @Resource
    private FnProfitRepository profitRepository;

    @Resource
    private CuCustomerRepository customerRepository;

    public Result judgeBigCustomer(String customerId) {
        CuCustomer customer = customerRepository.getById(customerId);
        if (!ObjectUtil.isAllNotEmpty(
                customer.getFutureSaleAmo(),
                customer.getFutureSaleProfit(),
                customer.getIndustryForce(),
                customer.getFutureWinRatio())
        ) {
            return Result.error("201", "当前客户未设定大客户标准值");
        }
        CuBigCustomerNorm cuBigCustomerNorm = baseRepository.getOne(null);
        if (cuBigCustomerNorm == null) {
            return Result.error("201", "缺少大客户标准设定");
        }
        List<TrTransportation> transportationList = transportationRepository.list(
                new QueryWrapper<TrTransportation>()
                        .eq("customer_id", customerId)
        );

        BigDecimal totalSaleAmo = transportationList.stream().map(TrTransportation::getTotalAmo).reduce(BigDecimal.ZERO, BigDecimal::add);
        List<FnProfit> profitList = profitRepository.list(
                new QueryWrapper<FnProfit>()
                        .eq("customer_id", customerId)
        );
        BigDecimal totalProfitAmo = profitList.stream().map(FnProfit::getProfitAmo).reduce(BigDecimal.ZERO, BigDecimal::add);
        boolean isBigCusntomer = totalProfitAmo.compareTo(cuBigCustomerNorm.getSaleAmo()) >= 0
                && totalProfitAmo.compareTo(cuBigCustomerNorm.getSaleProfit()) >= 0
                && customer.getFutureSaleAmo().compareTo(cuBigCustomerNorm.getFutureSaleAmo()) >= 0
                && customer.getFutureSaleProfit().compareTo(cuBigCustomerNorm.getFutureSaleProfit()) >= 0
                && customer.getIndustryForce().compareTo(cuBigCustomerNorm.getIndustryForce()) >= 0
                && customer.getFutureWinRatio().compareTo(cuBigCustomerNorm.getFutureWinRatio()) >= 0;
        customer.setSaleAmo(totalSaleAmo);
        customer.setSaleProfit(totalProfitAmo);

        if (isBigCusntomer) {
            customer.setCategory(1);

        }
        customerRepository.updateById(customer);
        return Result.success();
    }

}