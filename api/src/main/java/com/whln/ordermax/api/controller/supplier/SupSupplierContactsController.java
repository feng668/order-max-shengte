package com.whln.ordermax.api.controller.supplier;

import com.whln.ordermax.api.service.supplier.SupSupplierContactsService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SupSupplierContacts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  客户联系人控制器
 */
@Api(tags = "供应商联系人")
@RestController
@RequestMapping("/SupSupplierContacts")
public class SupSupplierContactsController{

    @Resource
    private SupSupplierContactsService baseService;

    @ApiOperation(value = "通过供应商id查询联系人信息")
    @PostMapping("/listBySupplierId")
    public Result<List<SupSupplierContacts>> listBySupplierId(@RequestBody SupSupplierContacts entity){
        return baseService.listAll(entity);
    }

}