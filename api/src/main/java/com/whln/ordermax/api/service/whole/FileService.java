package com.whln.ordermax.api.service.whole;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.ImmutableMap;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.utils.OmFileUtil;
import com.whln.ordermax.common.utils.QRCodeImageUtil;
import com.whln.ordermax.data.domain.param.FileDownloadParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 报价单业务处理
 */
@Service
@Slf4j
public class FileService {
    @Value("${filestore.root}")
    private String rootPath;

    @Value("${filestore.upload}")
    private String uploadPath;

    @Value("${filestore.emailsubmitted}")
    private String emailSubmittedPath;

    @Resource
    private SysService sysService;

    public Result<Map<String, Object>> submittedUpload(MultipartFile file, String emailAddress, String submittedId) {
        if (file.isEmpty()) {
            return Result.error("201", "缺少文件");
        }
        String originalFilename = file.getOriginalFilename();
        Long fileSize = file.getSize();
        String fileName = originalFilename;
        if (StrUtil.isBlank(fileName)) {
            return Result.error("201", "缺少文件名");
        }
        String s = File.separator;
        String filePath = emailSubmittedPath;
        if (!StrUtil.endWith(filePath, s)) {
            filePath = filePath.concat(s);
        }
        try {
            String subDir = StrUtil.join(s, emailAddress, submittedId);
            String suffix = FileUtil.getSuffix(originalFilename);
            String storePath = StrUtil.join(s, subDir, fileName);
            String relativePath = StrUtil.join(s, filePath, storePath);
            File dest = new File(relativePath);
            file.transferTo(dest);
            return Result.success(
                    new HashMap<String, Object>() {{
                        put("originalFilename", originalFilename);
                        put("fileName", fileName);
                        put("fileType", suffix);
                        put("storePath", storePath);
                        put("fileSize", fileSize);
                    }});
        } catch (IOException e) {
            log.error(e.toString(), e);
            Result.error("201", "文件上传失败");
        }
        return Result.success();
    }

    public Result upload(MultipartFile file) {
        if (file.isEmpty()) {
            return Result.error("201", "缺少文件");
        }
        String originalFilename = file.getOriginalFilename();
        Long fileSize = file.getSize();
        String fileName = originalFilename;
        if (StrUtil.isBlank(fileName)) {
            return Result.error("201", "缺少文件名");
        }

        String s = File.separator;
        String filePath = uploadPath;
        if (!StrUtil.endWith(filePath, s)) {
            filePath = filePath.concat(s);
        }
        // 根据时间生成路径
        LocalDateTime date = LocalDateTime.now();
        String subDir = LocalDateTimeUtil.format(date, "yyyy" + s + "MM" + s + "dd" + s + "HH" + s + "mm" + s);
        try {
            File dir = FileUtil.mkdir(filePath + subDir);
            // 重新生成文件名
            fileName = sysService.nextId();
            String suffix = FileUtil.getSuffix(originalFilename);
            if (StrUtil.isNotBlank(suffix)) {
                fileName = fileName.concat(".").concat(suffix);
            }
            String storePath = subDir.concat(fileName);
            filePath = dir.getPath().concat(s).concat(fileName);
            String relativePath = StrUtil.concat(true, "upload", s, subDir, fileName);
            File dest = new File(filePath);
            file.transferTo(dest);
            log.info("文件上传成功:{}", storePath);
            Map<String, Object> resultData = ImmutableMap.of(
                    "originalFilename", originalFilename,
                    "fileName", fileName,
                    "fileType", suffix,
                    "storePath", relativePath,
                    "fileSize", fileSize
            );
            return Result.success(resultData);
        } catch (IOException e) {
            log.error(e.toString(), e);
            Result.error("201", "文件上传失败");
        }
        return Result.success();
    }

    /**
     * 删除文件
     */
    public Boolean deleteFile(String uri) {
        return OmFileUtil.deleteFile(uploadPath + uri);
    }

    /**
     * 删除多个文件
     */
    public Boolean deleteFile(List<String> uriList) {
        for (String uri : uriList) {
            OmFileUtil.deleteFile(uploadPath + uri);
        }
        return true;
    }


    public void download(FileDownloadParam param, HttpServletResponse response) throws IOException {
        byte[] bytes = FileUtil.readBytes(rootPath + param.getFilePath());
        response.addHeader("Content-Length", "" + bytes.length);
        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment");
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(bytes);
    }

    public Result<List<String>> qrcode(List<String> datas) {
        List<String> paths = new ArrayList<>();
        for (String s : datas) {
            String s1 = toFile(s);
            paths.add(s1);
        }
        return Result.success(paths);
    }

    private String toFile(String content) {
        String s = File.separator;
        String filePath = uploadPath;
        if (!StrUtil.endWith(filePath, s)) {
            filePath = filePath.concat(s);
        }
        // 根据时间生成路径
        LocalDateTime date = LocalDateTime.now();
        String subDir = LocalDateTimeUtil.format(date, "yyyy" + s + "MM" + s + "dd" + s + "HH" + s + "mm" + s);

        File dir = FileUtil.mkdir(filePath + subDir);
        // 重新生成文件名
        String fileName = sysService.nextId();
        String storePath = subDir.concat(fileName);
        filePath = dir.getPath().concat(s).concat(fileName);
        String relativePath = StrUtil.concat(true, s, subDir, fileName);
        File dest = new File(filePath + ".jpg");
        QRCodeImageUtil.generateQRCodeImage(content, dest, 100);
        return relativePath;

    }

    public void findQrcode(String path, HttpServletResponse response) throws IOException {
        byte[] bytes = FileUtil.readBytes(uploadPath + path);
        response.addHeader("Content-Length", "" + bytes.length);
        response.setContentType("application/octet-stream");
        response.addHeader("Content-Disposition", "attachment");
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(bytes);
    }
}