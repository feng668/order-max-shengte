package com.whln.ordermax.api.controller.physical;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.physical.PhyCompanyContactsService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.PhyCompanyContacts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  货代公司联系人控制器
 */
@Api(tags = "货代公司联系人")
@RestController
@RequestMapping("/PhyCompanyContacts")
public class PhyCompanyContactsController{

    @Resource
    private PhyCompanyContactsService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<PhyCompanyContacts>> listAll(@RequestBody PhyCompanyContacts entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<PhyCompanyContacts>> pageAll(@RequestBody PageEntity<PhyCompanyContacts> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() PhyCompanyContacts entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}