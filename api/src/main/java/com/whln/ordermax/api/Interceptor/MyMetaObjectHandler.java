package com.whln.ordermax.api.Interceptor;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.whln.ordermax.common.utils.SnowflakeIdWorker;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

/**
 * @author liurun
 * @date 2023-04-13 21:47
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        Object id = this.getFieldValByName("id", metaObject);
        if (ObjectUtil.isEmpty(id)) {
            this.setFieldValByName("id", SnowflakeIdWorker.generateId().toString(), metaObject);
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }

}
