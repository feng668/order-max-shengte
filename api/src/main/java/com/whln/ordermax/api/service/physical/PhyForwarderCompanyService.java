package com.whln.ordermax.api.service.physical;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.PhyCompanyContacts;
import com.whln.ordermax.data.domain.PhyForwarderCompany;
import com.whln.ordermax.data.repository.PhyCompanyContactsRepository;
import com.whln.ordermax.data.repository.PhyForwarderCompanyRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  货代公司业务处理
 */
@Service
public class PhyForwarderCompanyService{
    @Resource
    private PhyForwarderCompanyRepository baseRepository;

    @Resource
    private PhyCompanyContactsRepository contactsRepository;

    /**
    *  查询所有
    */
    public Result listAll(PhyForwarderCompany entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(PhyForwarderCompany entity, Page<PhyForwarderCompany> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(PhyForwarderCompany entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        if (CollectionUtil.isNotEmpty(entity.getContactsDelIdList())){
            contactsRepository.removeByIds(entity.getContactsDelIdList());
        }
        if (CollectionUtil.isNotEmpty(entity.getContactsList())){
            contactsRepository.saveOrUpdateBatch(entity.getContactsList());
        }
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        contactsRepository.remove(new QueryWrapper<PhyCompanyContacts>().eq("company_id",id));
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        contactsRepository.remove(new QueryWrapper<PhyCompanyContacts>().in("company_id",ids));
        return b? Result.success(): Result.error("201","删除失败");
    }

    public Result<List<PhyCompanyContacts>> listContactsByCompanyId(String companyId) {
        return Result.success(contactsRepository.listByEntity(new PhyCompanyContacts(){{
            setCompanyId(companyId);
        }}));
    }
}