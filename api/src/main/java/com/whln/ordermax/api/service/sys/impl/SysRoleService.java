package com.whln.ordermax.api.service.sys.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.annotaions.Cache;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.RedisKey;
import com.whln.ordermax.common.enums.RedisOps;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.redis.RedisClient;
import com.whln.ordermax.data.domain.SysRole;
import com.whln.ordermax.data.domain.SysRolePower;
import com.whln.ordermax.data.domain.SysUser;
import com.whln.ordermax.data.domain.SysUserRole;
import com.whln.ordermax.data.repository.SysRolePowerRepository;
import com.whln.ordermax.data.repository.SysRoleRepository;
import com.whln.ordermax.data.repository.SysUserRepository;
import com.whln.ordermax.data.repository.SysUserRoleRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class SysRoleService {
    @Resource
    private SysRoleRepository baseRepository;
    @Resource
    private SysUserRepository sysUserRepository;

    @Resource
    private SysUserRoleRepository sysUserRoleRepository;

    @Resource
    private SysRolePowerRepository sysRolePowerRepository;

    @Resource
    private RedisClient<String, SysRole> redisClient;

    public Result listAll(SysRole entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }


    public Result pageAll(SysRole entity, Page<SysRole> page) {
        List<SysUser> userList = sysUserRepository.list();

        List<SysRole> list = redisClient.lRange(RedisKey.SYS_ROLE_ALL.getKey(), 0, -1);
        Map<String, List<SysRole>> collect = list.stream().peek(e -> {
            if (ObjectUtil.isEmpty(e.getParentId())) {
                e.setParentId("root");
            }
        }).collect(Collectors.groupingBy(SysRole::getParentId));
        Set<String> roleIdSet = list.stream().map(SysRole::getId).collect(Collectors.toSet());
        List<SysUserRole> sysUserRoleList = sysUserRoleRepository.list(new LambdaQueryWrapper<SysUserRole>().in(SysUserRole::getRoleId, roleIdSet));
        Map<String, List<SysUserRole>> sysUserRoleListGroupByRoleId = sysUserRoleList.stream().collect(Collectors.groupingBy(SysUserRole::getRoleId));
        for (SysRole sysRole:list){
            List<SysUserRole> sysUserRoles = sysUserRoleListGroupByRoleId.get(sysRole.getId());
            if (ObjectUtil.isNotEmpty(sysUserRoles)) {
                List<String> userIdList = sysUserRoles.stream().map(SysUserRole::getUserId).collect(Collectors.toList());
                List<SysUser> roleUserList = userList.stream().filter(e -> userIdList.contains(e.getId())).collect(Collectors.toList());
                sysRole.setUsers(roleUserList);
                sysRole.setUserIds(roleUserList.stream().map(SysUser::getId).collect(Collectors.toList()));
            }
        }

        List<SysRole> sysRoleList = collect.get("root");
        for (SysRole sysRole : sysRoleList) {
            setChildren(sysRole, collect);
        }
        return Result.success(sysRoleList);
    }

    private void setChildren(SysRole sysRole, Map<String, List<SysRole>> collect) {
        List<SysRole> sysRoleList = collect.get(sysRole.getId());
        sysRole.setChildren(sysRoleList);
        if (ObjectUtil.isNotEmpty(sysRoleList)) {
            for (SysRole s : sysRoleList) {
                setChildren(s, collect);
            }
        }
    }

    @Cache(key = RedisKey.SYS_ROLE_ALL, ops = RedisOps.SAVE,cacheParam = "entity")
    public Result save(SysRole entity) {
        baseRepository.saveOrUpdate(entity);

        //先删除与这个角色关联的用户，然后重新关联
        if (ObjectUtil.isNotEmpty(entity.getUserIds())) {
            sysUserRoleRepository.remove(new LambdaUpdateWrapper<SysUserRole>()
                    .eq(SysUserRole::getRoleId, entity.getId())
                    .notIn(SysUserRole::getUserId,entity.getUserIds())
            );
            Set<SysUserRole> sysUserRoleSet = entity.getUserIds().stream().map(item -> {
                SysUserRole sysUserRole = new SysUserRole();
                sysUserRole.setRoleId(entity.getId());
                sysUserRole.setUserId(item);
                return sysUserRole;
            }).collect(Collectors.toSet());
            sysUserRoleRepository.saveBatch(sysUserRoleSet);
        }
        setPowers(entity);
        return Result.success();
    }
    @Cache(key = RedisKey.SYS_ROLE_ALL, ops = RedisOps.DELETE,cacheParam = "id")
    public Result delete(String id) {
        SysRole one = baseRepository.getOne(new LambdaQueryWrapper<SysRole>().eq(SysRole::getId, id));
        if (ObjectUtil.equals(one.getCode(), "webadmin")) {
            throw new BusinessException("管理员角色不允许删除");
        }
        boolean b = baseRepository.removeById(id);
        return b ? Result.success() : Result.error("201", "删除失败");
    }
    @Cache(key = RedisKey.SYS_ROLE_ALL, ops = RedisOps.BATCH_DELETE,cacheParam = "ids")
    public Result batchDelete(List<String> ids) {
        if (ObjectUtil.isEmpty(ids)) {
            return Result.success();
        }
        List<SysRole> sysRoleList = baseRepository.list(new LambdaQueryWrapper<SysRole>().in(SysRole::getId, ids));
        List<SysRole> webadmin = sysRoleList.stream().filter(item -> ObjectUtil.equals(item.getCode(), "webadmin")).collect(Collectors.toList());
        if (ObjectUtil.isNotEmpty(webadmin)) {
            throw new BusinessException("管理员角色不允许删除");
        }
        boolean b = baseRepository.removeByIds(ids);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result setPowers(SysRole sysRole) {
        String roleId = sysRole.getId();
        List<String> powerIds = sysRole.getPowerIds();
        if (ObjectUtil.isEmpty(powerIds)) {
            return Result.success();
        }
        List<SysRolePower> sysUserRoleList = powerIds.stream().map(powerId -> {
            SysRolePower srp = new SysRolePower();
            srp.setRoleId(roleId);
            srp.setPowerId(powerId);
            return srp;
        }).collect(Collectors.toList());
        sysRolePowerRepository.remove(new LambdaQueryWrapper<SysRolePower>().eq(SysRolePower::getRoleId, roleId));
        sysRolePowerRepository.insertBatch(sysUserRoleList);
        return Result.success();
    }

    public List<SysUser> queryUserByRoleId(String roleId) {
        List<String> userIds = sysUserRoleRepository.listObjs(new LambdaQueryWrapper<SysUserRole>()
                .select(SysUserRole::getUserId)
                .eq(SysUserRole::getRoleId, roleId), item -> Objects.toString(roleId));
        if (ObjectUtil.isEmpty(userIds)) {
            return new ArrayList<>(0);
        }
        return sysUserRepository.list(new LambdaQueryWrapper<SysUser>().in(SysUser::getId, userIds));
    }

}