//package com.whln.ordermax.api.Interceptor;
//
//import cn.hutool.core.util.ObjectUtil;
//import com.whln.ordermax.data.service.MapCache;
//import com.whln.ordermax.api.shiro.utils.ShiroManager;
//import com.whln.ordermax.data.repository.SysPowerRepository;
//import org.apache.ibatis.cache.CacheKey;
//import org.apache.ibatis.executor.Executor;
//import org.apache.ibatis.mapping.BoundSql;
//import org.apache.ibatis.mapping.MappedStatement;
//import org.apache.ibatis.plugin.Interceptor;
//import org.apache.ibatis.plugin.Intercepts;
//import org.apache.ibatis.plugin.Invocation;
//import org.apache.ibatis.plugin.Signature;
//import org.apache.ibatis.session.ResultHandler;
//import org.apache.ibatis.session.RowBounds;
//import org.springframework.context.ApplicationContext;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.lang.reflect.Field;
//import java.util.List;
//import java.util.Map;
//import java.util.stream.Collectors;
//
///**
// * @author liurun
// * @date 2023-04-19 13:36
// */
//
//
//@Intercepts({
//        @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class,
//                RowBounds.class, ResultHandler.class}),
//        @Signature(
//                method = "query",
//                type = Executor.class,
//                args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class}
//        ),
//})
//@Component
//public class DesensitizationInterceptor implements Interceptor {
//    @Resource
//    private ApplicationContext applicationContext;
//
//    @Override
//    public Object intercept(Invocation invocation) throws Throwable {
//        MappedStatement arg = (MappedStatement) invocation.getArgs()[0];
//        String mapperId = arg.getId();
//        Map<String, Map<String, String>> mapperModelMap;
//        int lastIndex = mapperId.lastIndexOf(".");
//        String mapperIpath = mapperId.substring(0, lastIndex);
//        String methodName = mapperId.substring(lastIndex + 1);
//
//        MapService mapService = applicationContext.getBean(MapService.class);
//        if (ObjectUtil.equals(methodName, "listByEntity") || ObjectUtil.equals("pageTotalByStageType", methodName) || methodName.startsWith("get") || ObjectUtil.equals(methodName, "page")) {
//            //获取所属模块
//            mapperModelMap = mapService.mapperModelMap1();
//        } else if (ObjectUtil.equals(methodName, "commodityList") || methodName.lastIndexOf("List") != -1) {
//            mapperModelMap = mapService.commodityModelMap();
//        } else {
//            return invocation.proceed();
//        }
//        Map<String, String> mapperModel = mapperModelMap.get(mapperIpath);
//        if (ObjectUtil.isEmpty(mapperModel)) {
//            mapperModel = mapperModelMap.get(mapperIpath + "." + methodName);
//        }
//        if (ObjectUtil.isNotEmpty(mapperModel)) {
//            //获取当前用户可访问的属性
//            if (ShiroManager.getSubject().hasRole("webadmin")) {
//                return invocation.proceed();
//            }
//            SysPowerRepository sysPowerRepository = applicationContext.getBean(SysPowerRepository.class);
//            String modelCode = mapperModel.get("modelCode");
//            String fieldSource = mapperModel.get("fieldSource");
//            List<String> list = sysPowerRepository.getModelFieldListByUserId(ShiroManager.getUserId(), modelCode, fieldSource);
//            list.add("billId");
//            list.add("billNo");
//            list.add("id");
//
//            Object proceed = invocation.proceed();
//            if (ObjectUtil.isEmpty(list)) {
//                return proceed;
//            }
//            if (proceed instanceof List){
//                List<?> proceed1 = (List<?>) proceed;
//                List<?> collect = proceed1.stream().peek(e -> {
//                    Field[] declaredFields = e.getClass().getDeclaredFields();
//                    for ()
//
//                }).collect(Collectors.toList());
//                return collect;
//            }
//
//        }
//        return invocation.proceed();
//    }
//}