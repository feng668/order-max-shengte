package com.whln.ordermax.api.service.stock;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CmCommodity;
import com.whln.ordermax.data.domain.StInventoryRemain;
import com.whln.ordermax.data.domain.param.StInventoryListParam;
import com.whln.ordermax.data.domain.vo.BaseInfoVO;
import com.whln.ordermax.data.domain.vo.BaseTotalVO;
import com.whln.ordermax.data.domain.vo.CmCommodityVO;
import com.whln.ordermax.data.repository.CmCommodityRepository;
import com.whln.ordermax.data.repository.StInOrderCommodityRepository;
import com.whln.ordermax.data.repository.StInventoryRemainRepository;
import com.whln.ordermax.data.repository.StInventoryRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 入库单业务处理
 */
@Service
public class StInventoryService {
    @Resource
    private StInventoryRepository baseRepository;

    @Resource
    private StInOrderCommodityRepository commodityRepository;
    @Resource
    private CmCommodityRepository baseCommodityRepository;

    @Resource
    private StInventoryRemainRepository remainRepository;


    /* public Result<IPage<StInventoryVO>> pageDetail(PageEntity<StInventoryListParam> page) {
         return Result.success(baseRepository.pageByEntity(page.getEntity(), new Page<>(page.getCurrent(), page.getSize())));
     }*/
    public Result<IPage<Map>> pageDetail(Map entity, Page<Map> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    public Result<IPage<BaseTotalVO>> pageTotalByStageType(JSONObject entity, Page<Map> page) {
        String stageType = entity.getStr("stageType".trim());
        if (stageType == null) {
            return Result.error("201", "缺少期类型");
        }
        String dateFormatSql = "";
        switch (stageType) {
            case "YEAR":
                dateFormatSql = "date_format(t_t.proceed_date, '%Y')";
                break;
            case "QUARTER":
                dateFormatSql = "concat(date_format(t_t.proceed_date, '%Y'),'-第',QUARTER(t_t.proceed_date),'季度')";
                break;
            case "MONTH":
                dateFormatSql = "date_format(t_t.proceed_date, '%Y-%m')";
                break;
            case "DAY":
                dateFormatSql = "date_format(t_t.proceed_date, '%Y-%m-%d')";
                break;
        }
        entity.set("dateFormatSql", dateFormatSql);
        return Result.success(baseRepository.pageTotalByStageType(entity, page));
    }


    public Result<List<StInventoryRemain>> listRemain(StInventoryRemain entity) {
        Page<BaseInfoVO> baseInfoVOPage = baseRepository.baseInfo(entity, null);
        List<BaseInfoVO> records = baseInfoVOPage.getRecords();
        //找出对应商品的库存情况
        List<BaseInfoVO> baseInfoVOStream = records.stream()
                .filter(i -> ObjectUtil.equals(i.getCommodityId(), entity.getCommodityId()))
                .collect(Collectors.toList());
        Set<String> commodityIdSet = baseInfoVOStream.stream()
                .map(BaseInfoVO::getCommodityId)
                .collect(Collectors.toSet());
        if (ObjectUtil.isNotEmpty(commodityIdSet)) {
            List<CmCommodity> cmCommodityList = baseCommodityRepository.listByIds(commodityIdSet);
            Map<String, CmCommodity> commodityMap = cmCommodityList.stream().collect(Collectors.toMap(CmCommodity::getId, Function.identity()));
            List<StInventoryRemain> remainList = baseInfoVOStream.stream().map(item -> {
                StInventoryRemain stInventoryRemain = new StInventoryRemain();
                CmCommodity cmCommodity = commodityMap.get(item.getCommodityId());
                BeanUtil.copyProperties(cmCommodity, stInventoryRemain);
                stInventoryRemain.setWarehouseId(item.getWarehouseId());
                stInventoryRemain.setWarehouseName(item.getWarehouseName());
                stInventoryRemain.setWarehouseNo(item.getWarehouseNo());
                stInventoryRemain.setNum(item.getNum());
                stInventoryRemain.setGrabableNum(item.getFreeNum());
                stInventoryRemain.setGrabNum(item.getGrabNum());
                stInventoryRemain.setCommodityId(item.getCommodityId());
                stInventoryRemain.setCommodityBatchNo(item.getCommodityBatchNo());
                return stInventoryRemain;
            }).collect(Collectors.toList());
            return Result.success(remainList);
        }
        return Result.success(new ArrayList<>(0));
    }


    public Result<IPage<StInventoryRemain>> pageRemain(StInventoryRemain entity, Page<StInventoryRemain> page) {
        return Result.success(remainRepository.pageByEntity(entity, page));
    }

    public IPage<CmCommodityVO> list(StInventoryListParam entity, Page<StInventoryListParam> page) {
        return baseRepository.listStInventory(entity, page);
    }

    public  Result<Page<CmCommodityVO>> baseInfo(StInventoryRemain entity, Page<StInventoryListParam> page) {
        return Result.success(baseRepository.baseInfoByCommodity(entity, page));
    }
}