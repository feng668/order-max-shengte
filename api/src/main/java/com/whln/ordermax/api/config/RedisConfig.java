package com.whln.ordermax.api.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Slf4j
@Order(0)
@Configuration
public class RedisConfig {
    @Value("${spring.redis.host}")
    private String host = "127.0.0.1";
    @Value("${spring.redis.port}")
    private Integer port = 6379;

    // 0 - never expire
    private Integer expire = -1;

    //timeout for jedis try to connect to redis server, not expire time! In milliseconds
    @Value("${spring.redis.timeout}")
    private Integer timeout = 10000;
    @Value("${spring.redis.password}")
    private String password = "123456";
    @Value("${spring.redis.database}")
    private int database = 0;
    @Bean
    public JedisPool init(){
        if (password != null && !"".equals(password)) {
            return new JedisPool(new JedisPoolConfig(), host, port, timeout, password, database);
        } else if (timeout != 0) {
            return new JedisPool(new JedisPoolConfig(), host, port, timeout);
        } else {
            return new JedisPool(new JedisPoolConfig(), host, port);
        }
    }


}
