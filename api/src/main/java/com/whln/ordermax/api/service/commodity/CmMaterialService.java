package com.whln.ordermax.api.service.commodity;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.whln.ordermax.api.service.whole.FileService;
import org.springframework.stereotype.Service;
import com.whln.ordermax.data.domain.CmCommodityBom;
import com.whln.ordermax.data.domain.CmMaterial;
import com.whln.ordermax.data.domain.CmMaterialBom;
import com.whln.ordermax.data.mapper.CmMaterialBomMapper;
import com.whln.ordermax.data.mapper.CmMaterialMapper;
import com.whln.ordermax.data.repository.CmCommodityImgRepository;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CmMaterialService {
    @Resource
    private CmMaterialMapper cmMaterialMapper;
    @Resource
    private CmMaterialBomMapper cmMaterialBomMapper;
    @Resource
    private FileService fileService;
    @Resource
    private CmCommodityImgRepository imgRepository;

    public void save(CmMaterial entity){
        int i = cmMaterialMapper.insert(entity);

        if (CollectionUtil.isNotEmpty(entity.getImgDelUriList())) {
            fileService.deleteFile(entity.getImgDelUriList());
        }
        if (CollectionUtil.isNotEmpty(entity.getImgDelIdList())) {
            imgRepository.removeByIds(entity.getImgDelIdList());
        }
        if (CollectionUtil.isNotEmpty(entity.getImgList())) {
            imgRepository.saveOrUpdateBatch(entity.getImgList());
        }
        if (CollectionUtil.isNotEmpty(entity.getPartDelIdList())) {
            cmMaterialMapper.delete(new QueryWrapper<CmMaterial>().eq("id", entity.getId()).in("part_id", entity.getPartDelIdList()));
        }
        List<CmCommodityBom> bomList = entity.getBomList();
       for (int j=0;j<bomList.size();j++){
           CmCommodityBom item = bomList.get(j);
           CmMaterialBom bom = new CmMaterialBom();
           bom.setPartId(item.getPartId());
           bom.setPartNum(item.getPartNum());
           bom.setMaterialId(item.getCommodityId());
           cmMaterialBomMapper.insert(bom);
       }
    }

    public void delete(String id) {
        cmMaterialMapper.deleteById(id);
    }

    public void update(CmMaterial param) {
        cmMaterialMapper.updateById(param);
    }

    public CmMaterial get(String id) {
        return cmMaterialMapper.selectOne(new LambdaQueryWrapper<CmMaterial>()
                .eq(CmMaterial::getId,id));

    }
}
