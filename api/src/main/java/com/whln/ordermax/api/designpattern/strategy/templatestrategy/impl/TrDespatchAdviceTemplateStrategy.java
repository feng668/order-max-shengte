package com.whln.ordermax.api.designpattern.strategy.templatestrategy.impl;

import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategy;
import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategyFactory;
import com.whln.ordermax.common.enums.ModuleEnum;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.TmPrintTemplate;
import com.whln.ordermax.data.domain.TrDespatchAdvice;
import com.whln.ordermax.data.domain.param.RenderTemplateParam;
import com.whln.ordermax.data.repository.TrDespatchAdviceRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author liurun
 * @date 2023-02-21 9:38
 */
@Component
@AllArgsConstructor
public class TrDespatchAdviceTemplateStrategy extends TemplateStrategy<RenderTemplateParam, TmPrintTemplate,String>{
    private final TrDespatchAdviceRepository repository;

    @Override
    public void afterPropertiesSet() throws Exception {
        TemplateStrategyFactory.register(ModuleEnum.TR_DESPATCH_ADVICE.getValue(), this);
    }

    @Override
    public String handle(RenderTemplateParam param, TmPrintTemplate templateData) {
        try {
            String content = getTemplateContent(templateData.getHtmlPath());
            String template = transitionHtml2(content);
            TrDespatchAdvice trDespatchAdvice = repository.getAllById(param.getId());
            return render(trDespatchAdvice, template);
        } catch (IOException e) {
            throw new BusinessException("读取模板出错了");
        }
    }


}
