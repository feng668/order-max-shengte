package com.whln.ordermax.api.controller.pricingOrder;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.pricingOrder.PrPricingOrderService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CmCommodity;
import com.whln.ordermax.data.domain.PrPricingBoms;
import com.whln.ordermax.data.domain.PrPricingCommodity;
import com.whln.ordermax.data.domain.PrPricingOrder;
import com.whln.ordermax.data.domain.param.PrPricingOrderBatchParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  核价单控制器
 */
@Api(tags = "核价单")
@RestController
@RequestMapping("/prPricingOrder")
public class PrPricingOrderController{

    @Resource
    private PrPricingOrderService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<PrPricingOrder>> listAll(@RequestBody PrPricingOrder entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<PrPricingOrder>> pageAll(@RequestBody PageEntity<PrPricingOrder> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<PrPricingOrder> getAllById(@RequestParam("id") String id){
        return baseService.getAllById(id);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody @Validated PrPricingOrder entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "批量保存")
    @PostMapping("/batchSave")
    public Result batchSave(@RequestBody @Validated PrPricingOrderBatchParam param){
        baseService.batchSave(param);
        return Result.success();
    }

    @ApiOperation(value = "核价单生成商品档案")
    @PostMapping("/toCommodity")
    public Result toCommodity(@RequestBody @Validated PrPricingOrder param){
        CmCommodity cmCommodity = baseService.toCommodity(param);
        return Result.success(cmCommodity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "获取商品")
    @PostMapping("/listCom")
    public Result listCommodity(@RequestParam("id") String id){
        return baseService.listCom(id);
    }

    @ApiOperation(value = "获取bom")
    @PostMapping("/listBom")
    public Result listBom(@RequestParam("id") String id){
        return baseService.listBom(id);
    }

    @ApiOperation(value = "通过批量的商品id和数量获取bom和数量")
    @PostMapping("/listBomsByCommodity")
    public Result<List<PrPricingBoms>> listBomsByCommodity(@RequestBody List<PrPricingCommodity> entityList){
        return baseService.listBomsByCommodity(entityList);
    }

}