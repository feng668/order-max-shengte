package com.whln.ordermax.api.aspect;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.data.service.MapCache;
import com.whln.ordermax.data.domain.FlFlowBill;
import com.whln.ordermax.data.domain.FlFlowBillCommodity;
import com.whln.ordermax.data.repository.DynamicRepository;
import com.whln.ordermax.data.repository.FlFlowBillCommodityRepository;
import com.whln.ordermax.data.repository.FlFlowBillRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

/**
 * 流程控制切面
 */
@Aspect
@Component
@Order(3)
public class FlowControlAspect {

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private DynamicRepository dynamicRepository;

    @Pointcut("execution(* com.whln.ordermax.api.service..*.save(..))||(execution(* com.whln.ordermax.api.service..*.batchSave(..))&&@annotation(com.whln.ordermax.api.annotations.FlowControlSave))")
    public void point() {
    }


    @Resource
    private FlFlowBillRepository flowBillRepository;

    @Resource
    private FlFlowBillCommodityRepository flowBillCommodityRepository;

    @Around("point() && @annotation(com.whln.ordermax.api.annotations.FlowControlSave)||@annotation(com.whln.ordermax.api.annotations.FlowCommodity)")
    @Transactional(rollbackFor = Exception.class)
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        MapCache mapService = applicationContext.getBean(MapCache.class);
        //获取用户属性权限
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        JSONObject paramMap = JSONUtil.parseObj(ServletUtil.getBody(request));

        String saveType = paramMap.getStr("saveType");

        if (saveType == null || !saveType.equals("flow")) {
            return pjp.proceed();
        }
        //用的最多通知的签名
        Signature signature = pjp.getSignature();
        MethodSignature msg = (MethodSignature) signature;
        Object target = pjp.getTarget();
        //获取注解标注的方法
        Method method = target.getClass().getMethod(msg.getName(), msg.getParameterTypes());
        //通过方法获取注解
        FlowControlSave flowControlSave = method.getAnnotation(FlowControlSave.class);

        String sourceBillIds = paramMap.getStr("sourceBillIds");
        String sourceBillCode = paramMap.getStr("sourceBillCode");
        String flowBillCode = paramMap.getStr("flowBillCode");
        List<FlFlowBillCommodity> commodityFlowList = paramMap.getBeanList("commodityFlowList", FlFlowBillCommodity.class);
        if (ObjectUtil.isEmpty(commodityFlowList)) {
            return pjp.proceed();
        }
        HashMap<String, Integer> billIdFlowStatusMap = new HashMap<>(5);
        for (FlFlowBillCommodity flowBillCommodity : commodityFlowList) {
            flowBillCommodity.setBillCode(sourceBillCode);
            flowBillCommodity.setFlowBillCode(flowBillCode);
            if (flowBillCommodity.getFlowRemainNum().compareTo(BigDecimal.ZERO) > 0) {
                flowBillCommodity.setFlowStatus(0);
                billIdFlowStatusMap.put(flowBillCommodity.getBillId(), 0);
            } else {
                flowBillCommodity.setFlowStatus(1);
            }
            flowBillCommodityRepository.saveOrUpdate(flowBillCommodity);
        }
        for (String sourceBillId : sourceBillIds.split(",")) {
            FlFlowBill flowBill = new FlFlowBill();
            flowBill.setBillId(sourceBillId);
            flowBill.setBillCode(sourceBillCode);
            flowBill.setFlowBillCode(flowBillCode);
            Integer flowStatus = billIdFlowStatusMap.get(sourceBillId);
            if (flowStatus != null) {
                flowBill.setFlowStatus(flowStatus);
            } else {
                flowBill.setFlowStatus(1);
            }
            flowBillRepository.saveOrUpdate(flowBill);
        }
        return pjp.proceed();
    }
}
