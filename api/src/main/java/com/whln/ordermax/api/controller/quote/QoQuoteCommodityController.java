package com.whln.ordermax.api.controller.quote;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.whln.ordermax.api.service.quote.QoQuoteCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.entity.quote.QoCommodityHistoryQueteVo;
import com.whln.ordermax.data.domain.QoQuoteCommodity;
import com.whln.ordermax.data.domain.param.ListHistoryPriceByComIdParam;
import com.whln.ordermax.data.domain.vo.LastPriceByComVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 报价商品清单控制器
 */
@Api(tags = "报价商品清单")
@RestController
@RequestMapping("/QoQuoteCommodity")
public class QoQuoteCommodityController {

    @Resource
    private QoQuoteCommodityService baseService;

    @ApiOperation(value = "根据报价单id查询")
    @PostMapping("/list")
    public Result<List<QoQuoteCommodity>> listAll(@RequestBody QoQuoteCommodity entity) {
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "通过商品id和客户id获取最近一次报价")
    @PostMapping("/getLastPriceByComIdAndCustId")
    public Result<LastPriceByComVO> getLastPriceByComIdAndCustId(@RequestParam("commodityId") String commodityId, @RequestParam("customerId") String customerId) {
        return baseService.getLastPriceByComIdAndCustId(commodityId, customerId);
    }

    @ApiOperation(value = "通过商品id和客户id获取客户对此商品的往期报价")
    @PostMapping("/listHistoryPriceByComIdCustId")
    public Result<IPage<QoCommodityHistoryQueteVo>> listHistoryPriceByComIdCustId(ListHistoryPriceByComIdParam param) {
        return baseService.listHistoryPriceByComIdCustId(param);
    }

}