package com.whln.ordermax.api.controller.whole;

import com.whln.ordermax.api.service.whole.EventCalendarService;
import com.whln.ordermax.common.entity.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "事件日历")
@RestController
@RequestMapping("/eventCalendar")
@Slf4j
public class EventCalendarController {

    @Resource
    private EventCalendarService baseService;

    @ApiOperation(value = "根据起止日期获取事件")
    @PostMapping("/getEventsByDateRange")
    public Result getEventsByDateRange(@RequestParam("startDate") String startDate, @RequestParam("endDate")String endDate){
        return baseService.getEventsByDateRange(startDate,endDate);
    }

    @ApiOperation(value = "根据单据id和模块代码获取单据信息")
    @PostMapping("/getBillInfo")
    public Result getBillInfo(@RequestParam("id") String id, @RequestParam("billCode")String billCode){
        return baseService.getBillInfo(id,billCode);
    }
}
