package com.whln.ordermax.api.designpattern.strategy.templatestrategy;

import org.springframework.stereotype.Component;
import com.whln.ordermax.data.domain.TmPrintTemplate;
import com.whln.ordermax.data.domain.param.RenderTemplateParam;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liurun
 * @date 2023-02-21 9:42
 */
@Component
public class TemplateStrategyFactory {
    private static final Map<String, TemplateStrategy<RenderTemplateParam, TmPrintTemplate,String>> STRATEGY = new HashMap<>();
    public static void register(String modular,TemplateStrategy<RenderTemplateParam, TmPrintTemplate,String> templateStrategy){
        STRATEGY.put(modular, templateStrategy);
    }
    public TemplateStrategy<RenderTemplateParam, TmPrintTemplate,String> getStrategy(String modular){
        return STRATEGY.get(modular);
    }
}
