package com.whln.ordermax.api.controller.enquiry;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.enquiry.EoEnquiryOrderService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.EoEnquiryCommodity;
import com.whln.ordermax.data.domain.EoEnquiryOrder;
import com.whln.ordermax.data.domain.EoEnquiryPlans;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *  询价单控制器
 */
@Api(tags = "询价单")
@RestController
@RequestMapping("/enquiryOrder")
public class EoEnquiryOrderController{

    @Resource
    private EoEnquiryOrderService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<EoEnquiryOrder>> listAll(@RequestBody EoEnquiryOrder entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<EoEnquiryOrder>> pageAll(@RequestBody PageEntity<EoEnquiryOrder> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody()@Validated EoEnquiryOrder entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "批量保存")
    @PostMapping("/batchSave")
    public Result<Void> batchSave(@RequestBody List<EoEnquiryCommodity> entity){
        baseService.batchSave(entity);
        return Result.success();
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result<Void> batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "根据询价单id查询")
    @PostMapping("/listCom")
    public Result<List<EoEnquiryCommodity>> listCom(@RequestBody EoEnquiryCommodity entity){
        return baseService.listCom(entity);
    }

    @ApiOperation(value = "记录询价单进度")
    @PostMapping("/addPlan")
    public Result<Void> addPlan(@RequestBody EoEnquiryPlans entity){
        return baseService.addPlan(entity);
    }

    @ApiOperation(value = "分页查询进度列表")
    @PostMapping("/pageAllPlan")
    public Result<Page<Map<String, Object>>> pageAllPlan(@RequestBody PageEntity<EoEnquiryOrder> pageEntity){
        return baseService.pageAllPlan(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "修改询价单进度|暂弃用")
    @PostMapping("/updatePlan")
    public Result updatePlan(@RequestBody EoEnquiryOrder entity){
        return baseService.updatePlan(entity);
    }

}