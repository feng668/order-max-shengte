package com.whln.ordermax.api.annotations;

import java.lang.annotation.*;

/**
 * @author liurun
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)//注解保留时间
@Target({ElementType.TYPE, ElementType.METHOD})//描述注解使用范围
public @interface FlowControlSave {
    int status() default 0;
    int value() default  1;

}
