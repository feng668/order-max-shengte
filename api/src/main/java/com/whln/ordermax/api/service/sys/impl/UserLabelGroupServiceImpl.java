package com.whln.ordermax.api.service.sys.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whln.ordermax.api.service.sys.UserLabelGroupService;
import com.whln.ordermax.data.domain.CustomerLabelGroup;
import com.whln.ordermax.data.domain.param.DeleteParam;
import com.whln.ordermax.data.domain.param.UserLabelGroupParam;
import com.whln.ordermax.data.domain.vo.CustomerLabelGroupVO;
import com.whln.ordermax.data.repository.CustomerLabelGroupRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 16:31
 */
@Service
@AllArgsConstructor
public class UserLabelGroupServiceImpl implements UserLabelGroupService {
    private final CustomerLabelGroupRepository userLabelGroupRepository;

    @Override
    public void save(CustomerLabelGroupVO userLabelGroupVO) {
        CustomerLabelGroup userLabelGroup = BeanUtil.copyProperties(userLabelGroupVO, CustomerLabelGroup.class);
        userLabelGroupRepository.saveOrUpdate(userLabelGroup);
    }

    @Override
    public void deleteGroup(DeleteParam param) {
        userLabelGroupRepository.removeById(param.getId());
    }

    @Override
    public List<CustomerLabelGroupVO> list(UserLabelGroupParam param) {
        List<CustomerLabelGroup> list = userLabelGroupRepository.list(new LambdaQueryWrapper<CustomerLabelGroup>()
                .or()
                .like(ObjectUtil.isNotEmpty(param.getName()), CustomerLabelGroup::getGroupName, param.getName())
        );
        return BeanUtil.copyToList(list, CustomerLabelGroupVO.class);
    }
}
