package com.whln.ordermax.api.filter;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.net.NetUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.util.ResourceUtils;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.entity.Serial;
import com.whln.ordermax.common.utils.SerialUtil;
import com.whln.ordermax.common.utils.ServletUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.Timestamp;

//@Component
@Order(1)
@Slf4j
public class SerialFilter implements Filter {


    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;

        String path = request.getRequestURI().substring(request.getContextPath().length());
        if (!"/seria/activate".equals(path)){
            try {
                String serialNumber = FileUtil.readString(new File(ResourceUtils.getURL("classpath:").getPath().replaceAll("/classes", "") + "serialNumber.lic"), "utf8");


                //*解释序列号,参数是引用类型，调用解释序列号方法后可直接从对象中获取MAC地址、有效结束时间*//*
                Serial serial = SerialUtil.explainSerialNumber(serialNumber);
                //*获取MAC地址*//*
                String mac = serial.getMac();
                //*获取有效结束时间*//*
                String effectiveEndTime = serial.getEffectiveEndTime();
                //*获取本机MAC地址*//*
                InetAddress inetAddress = InetAddress.getLocalHost();
                String localMac = NetUtil.getMacAddress(inetAddress);
                //*获取当前时间戳*//*
                long time = new Timestamp(System.currentTimeMillis()).getTime();
                //*判断有效结束时间是否大于当前时间*//*
                if(!localMac.equalsIgnoreCase(mac)){
                    ServletUtil.writeResponse(response, 600, Result.error("600", "当前机器未注册序列号，请联系管理员！"));
                    return;
                }else if (Long.parseLong(effectiveEndTime)<time){
                    ServletUtil.writeResponse(response, 600, Result.error("600", "当前机器的序列号已过期，请联系管理员！"));
                    return;
                }

            }catch (FileNotFoundException e){
                Result result = Result.error("600", "序列号不存在，请进行授权！");
                ServletUtil.writeResponse(response, 600, result);
            }
        }

        chain.doFilter(req, res);
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }
}
