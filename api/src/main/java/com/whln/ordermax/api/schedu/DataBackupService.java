package com.whln.ordermax.api.schedu;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
@Component
public class DataBackupService {
    @Resource(name = "taskExecutor")
    ThreadPoolTaskExecutor executor;
    @Value("${mysql.host}")
    private String host;
    @Value("${mysql.username}")
    private String userName;
    @Value("${mysql.password}")
    private String password;
    @Value("${mysql.port}")
    private int port;
    @Value("${mysql.database}")
    private String database;
    public static final String CMD = "mysqldump --databases -h%s -P%s -u%s -p%s %s";

    /**
     * 备份数据库 ,控制台执行命令格式
     * mysql的bin目录/mysqldump --databases  -h主机ip -P端口  -u用户名 -p密码 数据库名
     */
    public void backup() {
        try {
            String cmd = String.format(CMD,host,port,userName,password,database);
            Process process = Runtime.getRuntime().exec(cmd);
            executor.execute(new InputStreamProcessor(process));
            executor.execute(new ErrorInputStream(process));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static class InputStreamProcessor implements Runnable{
        private final Process process;

        private final String path = String.format("/%s_back.sql",System.currentTimeMillis());

        public InputStreamProcessor(Process process) {
            this.process = process;
        }

        @Override
        public void run() {
            try (InputStream is = process.getInputStream();
                 FileOutputStream out = new FileOutputStream(path);){
                byte[] b = new byte[1024];
                int len = -1;
                while ((len = is.read(b)) != -1) {
                    out.write(b, 0, len);
                }
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    static class ErrorInputStream implements Runnable{
        private final Process process; // 控制台errorStream

        private final String path = String.format("d:\\%s_error_wms_ts_test.sql",System.currentTimeMillis());

        public ErrorInputStream(Process process) {
            this.process = process;
        }

        @Override
        public void run() {
            try (InputStream is = process.getErrorStream();
                 FileOutputStream out = new FileOutputStream(path);){
                byte[] b = new byte[1024];
                int len = -1;
                while ((len = is.read(b)) != -1) {
                    out.write(b, 0, len);
                }
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }


}