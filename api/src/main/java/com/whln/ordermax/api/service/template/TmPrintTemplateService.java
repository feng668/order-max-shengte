package com.whln.ordermax.api.service.template;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.TmPrintTemplate;
import com.whln.ordermax.data.repository.TmPrintTemplateRepository;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

/**
 * 打印模板业务处理
 */
@Service
public class TmPrintTemplateService {
    @Resource
    private TmPrintTemplateRepository baseRepository;

    @Value("${filestore.printtemplate}")
    private String printTemplatePath;

    /**
     * 查询所有
     */
    public Result listAll(TmPrintTemplate entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(TmPrintTemplate entity, Page<TmPrintTemplate> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 查询单条
     */
    public Result<TmPrintTemplate> getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }

    @Resource
    private SysService sysService;

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    public Result save(TmPrintTemplate entity) {
        String ftlStr = transitionHtml2(entity.getHtmlStr());
        if (ObjectUtil.isEmpty(entity.getId())) {
            entity.setId(sysService.nextId());
        }
        String s = File.separator;
        String htmlPath = StrUtil.concat(true, entity.getBillCode(), s, entity.getId(), ".html");
        String ftlPath = StrUtil.concat(true, entity.getBillCode(), s, entity.getId(), ".ftl");
        FileUtil.writeString(entity.getHtmlStr(), printTemplatePath + htmlPath, StandardCharsets.UTF_8);
        FileUtil.writeString(ftlStr, printTemplatePath + ftlPath, StandardCharsets.UTF_8);
        entity.setHtmlPath(htmlPath);
        entity.setFltPath(ftlPath);
        boolean b = baseRepository.saveOrUpdate(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }


    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        TmPrintTemplate tmPrintTemplate = baseRepository.getAllById(id);
        boolean b = baseRepository.removeById(id);
        FileUtil.del(printTemplatePath + tmPrintTemplate.getHtmlPath());
        FileUtil.del(printTemplatePath + tmPrintTemplate.getFltPath());
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<TmPrintTemplate> tmPrintTemplateList = baseRepository.list(new QueryWrapper<TmPrintTemplate>().in("id", ids));
        boolean b = baseRepository.removeByIds(ids);
        for (TmPrintTemplate tmPrintTemplate : tmPrintTemplateList) {
            FileUtil.del(printTemplatePath + tmPrintTemplate.getHtmlPath());
            FileUtil.del(printTemplatePath + tmPrintTemplate.getFltPath());
        }
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 转换html内容为ftl内容
     */
    private String transitionHtml(String htmlMsg) {
        Document html = Jsoup.parse(htmlMsg);

        Element body = html.body();
        Elements attrs = body.select("[isattr='isattr']");
        for (Element attr : attrs) {
            attr.after(attr.val());
            attr.remove();
        }
        Elements list = body.select("[islist='islist']");
        HashMap<String, String> replaceMap = new HashMap<>();
        for (Element element : list) {
            String listName = element.attr("listName");
            String elementStr = element.toString();
            String parseStr = "\n<#list " + listName + " as t>\n" + elementStr + "\n</#list>";
            element.after("<!--#ftl#" + parseStr + "#ftl#-->");
            element.remove();

            System.out.println();
        }

        String ftlStr = html.toString();
        ftlStr = ftlStr.replaceAll("<!--#ftl#", "");
        ftlStr = ftlStr.replaceAll("#ftl#-->", "");
        return ftlStr;
    }

    /**
     * 转换html内容为ftl内容
     */
    private String transitionHtml2(String htmlMsg) {
        Document html = Jsoup.parse(htmlMsg);
        Element body = html.body();
        Elements table = body.select("table");
        for (Element e : table) {
            String nowStyle = e.attr("style");
            String addStyle = "border-collapse:collapse;border-spacing:0;";
            if (nowStyle.contains(addStyle)) {
                continue;
            }
            String sp = "";
            if (!"".equals(nowStyle)) {
                sp = ";";
            }
            String s = nowStyle + sp + addStyle;
            e.attr("style", s);
        }

        Elements handerForList = body.select("[islist='islist']");
        for (Element element : handerForList) {
            Element p = element.parent();
            Element td = p.parent();
            Element tr = td.parent();
            if ("td".equals(p.tagName()) || "tr".equals(td.tagName())) {
                td.attr("islist", "islist");
                td.attr("listname", element.attr("listname"));
            } else if ("p".equals(p.tagName()) || "td".equals(td.tagName()) || "tr".equals(tr.tagName())) {
                td.attr("islist", "islist");
                td.attr("listname", element.attr("listname"));
            } else {
                throw new BusinessException("商品变量位置不正确");
            }

        }
        Elements attrs = body.select("[isattr='isattr']");
        for (Element attr : attrs) {
            attr.after(attr.val());
            attr.remove();
        }
        Elements list = body.select("[islist='islist']");
        HashMap<String, String> replaceMap = new HashMap<>();
        for (Element element : list) {
            String listName = element.attr("listName");
            String elementStr = element.toString();
            String parseStr = "\n<#if " + listName + "?? && (" + listName + "?size > 0)>\n<#list " + listName + " as t>\n" + elementStr + "\n</#list>\n</#if>";
            element.after("<!--#ftl#" + parseStr + "#ftl#-->");
            element.remove();

        }

        Elements qr = body.select("[isqr='isqr']");
        for (Element element : qr) {
            String elementStr = element.text();
            element.after("<img isqr='isqr' src='' qrcontent='" + elementStr + "'>");
            element.remove();

        }

        Elements qrlist = body.select("[isqrlist='isqrlist']");
        for (Element element : qrlist) {
            String listName = element.attr("listName");
            String elementStr = element.toString();
            String parseStr = "\n<#if " + listName + "?? && (" + listName + "?size > 0)>\n<#list " + listName + " as t>\n" + elementStr + "<br>\n</#list>\n</#if>";
            element.after("<!--#ftl#" + parseStr + "#ftl#-->");
            element.remove();

        }
        Elements tds = body.select("td");
        for (Element e : tds) {
            String nowStyle = e.attr("style");
            String addStyle = "border:1px  solid #000000";
            if (nowStyle.contains(addStyle)) {
                continue;
            }
            String sp = "";
            if (!"".equals(nowStyle)) {
                sp = ";";
            }
            String s = nowStyle + sp + addStyle;
            e.attr("style", s);
        }
        String ftlStr = html.toString();
        ftlStr = ftlStr.replaceAll("<!--#ftl#", "");
        ftlStr = ftlStr.replaceAll("#ftl#-->", "");
        return ftlStr;
    }

}