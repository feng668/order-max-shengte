package com.whln.ordermax.api.service.supplier;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SupContactsEmail;
import com.whln.ordermax.data.repository.SupContactsEmailRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 供应商联系人邮箱业务处理
 */
@Service
public class SupContactsEmailService {
    @Resource
    private SupContactsEmailRepository baseRepository;

    /**
     * 查询所有
     */
    public Result<List<SupContactsEmail>> listAll(SupContactsEmail entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result<IPage<SupContactsEmail>> pageAll(SupContactsEmail entity, Page<SupContactsEmail> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    public Result<Void> save(SupContactsEmail entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 批量删除
     */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

}