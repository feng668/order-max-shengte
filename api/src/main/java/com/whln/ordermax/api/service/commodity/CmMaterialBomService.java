package com.whln.ordermax.api.service.commodity;

import org.springframework.stereotype.Service;
import com.whln.ordermax.data.domain.CmMaterialBom;
import com.whln.ordermax.data.mapper.CmMaterialBomMapper;

import javax.annotation.Resource;

@Service
public class CmMaterialBomService {
    @Resource
    private CmMaterialBomMapper cmMaterialBomMapper;
    public void save(CmMaterialBom cmMaterialBom){
        cmMaterialBomMapper.insert(cmMaterialBom);
    }
}
