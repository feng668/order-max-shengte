package com.whln.ordermax.api.controller.sys;

import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.entity.numberRole.NumReqEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Api(tags = "系统通用接口")
@RestController
@RequestMapping("/system")
public class SysController {

    @Resource
    private SysService sysService;

    @ApiOperation(value = "获取新的id")
    @GetMapping("/nextId")
    public Result<String> nextId(){
        return Result.success(sysService.nextId());
    }

    @ApiOperation(value = "获取新的单据编号")
    @PostMapping("/nextBillNo")
    public Result<String> nextBillNo(@RequestBody NumReqEntity entity){
        return sysService.nextBillNo(entity);
    }

    @ApiOperation(value = "批量获取新的单据编号")
    @PostMapping("/nextBillNoList")
    public Result<List<String>> nextBillNoList(@RequestBody NumReqEntity entity){
        return sysService.nextBillNoList(entity);
    }

    @ApiOperation(value = "获取过滤判断类型")
    @PostMapping("/listJudgeType")
    public Result<List<Map<String,String>>> listJudgeType(){
        return sysService.listJudgeType();
    }

    @ApiOperation(value = "获取单据相邻id")
    @PostMapping("/getBorderId")
    public Result<String> getBorderId(@RequestParam("id")String id, @RequestParam("billCode")String billCode, @RequestParam("borderType")String borderType){
        return sysService.getBorderId(id, billCode, borderType);
    }

    @ApiOperation(value = "查询单据类别")
    @PostMapping("/listModels")
    public Result<List<Map<String, String>>> listModels(){
        return sysService.listModels();
    }
}
