package com.whln.ordermax.api.shiro.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@Slf4j
@Component
@WebListener
public class CustomSessionListener implements HttpSessionListener {
    private int onlineCount = 0;//记录session的数量

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        onlineCount++;
        log.info("当前用户数量："+onlineCount);
        System.out.println("【HttpSessionListener监听器】 sessionCreated, onlineCount:" + onlineCount);
        se.getSession().getSessionContext().getSession("c2abc1c8-1dde-4c21-83a3-378854073d3c");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        if (onlineCount > 0) {
            onlineCount--;
        }
        System.out.println("【HttpSessionListener监听器】 sessionDestroyed, onlineCount:" + onlineCount);
        se.getSession().getServletContext().setAttribute("onlineCount", onlineCount);
    }
}
