package com.whln.ordermax.api.designpattern.strategy.redisops;

import com.whln.ordermax.api.designpattern.strategy.StrategyHandler;
import com.whln.ordermax.common.redis.RedisClient;
import org.springframework.beans.factory.InitializingBean;

import javax.annotation.Resource;

/**
 * @author liurun
 * @date 2023-02-21 9:35
 */
public abstract class RedisStrategy<P,T,R> implements StrategyHandler<P,T, R> , InitializingBean {
    @Resource
    protected RedisOpsFactory factory;

    @Resource
    protected RedisClient<String, Object> redisClient;

    /**
     * 处理逻辑
     */
    @Override
    public abstract R handle(P param, T template) throws Throwable;


}
