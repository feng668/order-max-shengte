package com.whln.ordermax.api.controller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.sys.impl.SysUserEmailService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SysEmailServer;
import com.whln.ordermax.data.domain.SysUserEmail;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  控制器
 */
@Api(tags = "用户邮件管理")
@RestController
@RequestMapping("/sysUserEmail")
public class SysUserEmailController{

    @Resource
    private SysUserEmailService baseService;


    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<SysUserEmail>> pageAll(@RequestBody PageEntity<SysUserEmail> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<SysUserEmail> getAllById(@RequestParam("id") String id){
        return baseService.getAllById(id);
    }

    @ApiOperation(value = "验证邮箱")
    @PostMapping("/verify")
    public Result verify(@RequestBody() SysUserEmail entity){
        return baseService.verify(entity);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() SysUserEmail entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "获取收发服务器地址")
    @PostMapping("/getServer")
    public Result<SysEmailServer> getServer(@RequestParam("email") String email){
        return baseService.getServer(email);
    }

    @ApiOperation(value = "开启或关闭邮件同步")
    @PostMapping("/contrSync")
    public Result contrSync(@RequestBody SysUserEmail userEmail){
        return baseService.contrSync(userEmail);
    }

}