package com.whln.ordermax.api.aspect;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.annotations.OrderPlanSave;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.data.domain.OrderPlan;
import com.whln.ordermax.data.repository.OrderPlanRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 进度跟踪
 */
@Aspect
@Component
@Order(2)
public class OrderPlanControlAspect {

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private OrderPlanRepository orderPlanRepository;


    @Pointcut("execution(* save(..))&&within(com.whln.ordermax.api.service..*)")
    public void point() {
    }


    @Around("point() &&( @annotation(com.whln.ordermax.api.annotations.FlowControlSave) || @annotation(com.whln.ordermax.api.annotations.OrderPlanSave) )")
    @Transactional(rollbackFor = Exception.class)
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        //获取用户属性权限
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        JSONObject entity = JSONUtil.parseObj(ServletUtil.getBody(request));
        //todo 待完成orderplan的temId
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        FlowControlSave flowControlSave = method.getAnnotation(FlowControlSave.class);
        int status = 0;
        if (ObjectUtil.isNotEmpty(flowControlSave)) {
            status = flowControlSave.status();
        }
        OrderPlanSave orderPlanSave = method.getAnnotation(OrderPlanSave.class);
        if (ObjectUtil.isNotEmpty(orderPlanSave)&&ObjectUtil.equals(status, 0)) {
            status = orderPlanSave.status();
        }
        Class<?> targetClass = pjp.getTarget().getClass();
        String currentClassName = targetClass.getSimpleName();
        BillCode billCodeAnnotation = targetClass.getAnnotation(BillCode.class);
        String billCode = null;
        Object saveType = entity.get("saveType");
        if (ObjectUtil.isEmpty(saveType) && ObjectUtil.isNotEmpty(billCodeAnnotation) && ObjectUtil.isNotEmpty(billCodeAnnotation.value())) {
            ModelMessage value = billCodeAnnotation.value();
            billCode = value.getCode();
        }

        Object result = pjp.proceed();

        if (ObjectUtil.isEmpty(saveType)) {
            OrderPlan orderPlan = new OrderPlan();
            orderPlan.setBillId(entity.getStr("id"));
            orderPlan.setRootBillId(entity.getStr("id"));
            orderPlan.setBillNo(entity.getStr("billNo"));
            orderPlan.setBillCode(billCode);
            orderPlan.setBillStatus(1);
            orderPlan.setTemId(1);
            orderPlan.setLastBillId(entity.getStr("id"));
            orderPlan.setLastBillCode(billCode);
            savePlan(orderPlan, billCode, status);
            return result;
        }
        String lastBillId = entity.getStr("lastBillId");
        if (StrUtil.isEmpty(lastBillId) && !ObjectUtil.equals("SoSoOrderService", currentClassName)) {
            return result;
        }

        //如果是销售合同，则直接保存进度
        if (ObjectUtil.equals("SoSoOrderService", currentClassName)) {
            OrderPlan orderPlan = new OrderPlan();
            orderPlan.setBillId(entity.getStr("id"));
            orderPlan.setLastBillId(entity.getStr("id"));
            orderPlan.setBillCode("SO_SO_ORDER");
            orderPlan.setBillNo(entity.getStr("billNo"));
            orderPlan.setLastBillCode("SO_SO_ORDER");
            savePlan(orderPlan, billCode, status);
        } else {
            if (!StrUtil.isAllNotEmpty(entity.getStr("lastBillCode"), entity.getStr("id"), entity.getStr("currentBillCode"))) {
                return result;
            }
            OrderPlan orderPlan = new OrderPlan();
            orderPlan.setLastBillId(lastBillId);
            orderPlan.setLastBillCode(entity.getStr("lastBillCode"));
            orderPlan.setBillId(entity.getStr("id"));
            orderPlan.setBillCode(entity.getStr("currentBillCode"));
            orderPlan.setBillNo(entity.getStr("billNo"));
            savePlan(orderPlan, billCode, status);
        }

        return result;
    }

    private void savePlan(OrderPlan orderPlan, String billCode, int status) {
        OrderPlan existPlan = orderPlanRepository.getOne(
                new QueryWrapper<OrderPlan>()
                        .eq("bill_id", orderPlan.getBillId())
                        .eq("bill_code", orderPlan.getBillCode())
        );
        if (existPlan != null) {
            return;
        }

        if (!ObjectUtil.equals(orderPlan.getBillId(), orderPlan.getLastBillId())) {
            orderPlanRepository.update(new LambdaUpdateWrapper<OrderPlan>()
                    .eq(OrderPlan::getLastBillId, orderPlan.getLastBillCode())
                    .eq(OrderPlan::getLastBillCode, orderPlan.getLastBillCode())
                    .set(OrderPlan::getBillStatus, 2)
            );
        }
        if (!"SO_SO_ORDER".equals(orderPlan.getBillCode()) && ObjectUtil.isEmpty(billCode)) {
            OrderPlan lastOrderPlan = orderPlanRepository.getOne(
                    new QueryWrapper<OrderPlan>()
                            .eq("bill_id", orderPlan.getLastBillId())
                            .eq("bill_code", orderPlan.getLastBillCode())
            );
            orderPlanRepository.update(
                    new UpdateWrapper<OrderPlan>()
                            .set("bill_status", 2)
                            .eq("bill_id", lastOrderPlan.getBillId())
                            .eq("bill_code", lastOrderPlan.getBillCode())
            );
            if ("PD_PRODUCTION_TASK".equals(orderPlan.getBillCode())) {
                orderPlan.setTemId(2);
                orderPlanRepository.update(
                        new UpdateWrapper<OrderPlan>()
                                .set("tem_id", 2)
                                .eq("root_bill_id", lastOrderPlan.getRootBillId())
                );
            }
            orderPlan.setRootBillId(lastOrderPlan.getRootBillId());
            orderPlan.setBillStatus(1);
        } else {
            orderPlan.setRootBillId(orderPlan.getBillId());
            orderPlan.setBillStatus(1);
        }
        if (ObjectUtil.isEmpty(orderPlan.getBillCode())) {
            orderPlan.setBillCode(billCode);
        }
        if (!ObjectUtil.equals(status, 0)) {
            orderPlan.setBillStatus(2);
        }
        orderPlanRepository.saveOrUpdate(orderPlan);
    }
}
