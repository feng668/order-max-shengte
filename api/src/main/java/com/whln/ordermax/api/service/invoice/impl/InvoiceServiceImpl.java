package com.whln.ordermax.api.service.invoice.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.api.service.invoice.InvoiceContractService;
import com.whln.ordermax.api.service.invoice.InvoiceService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.Invoice;
import com.whln.ordermax.data.domain.InvoiceContract;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.InvoiceMapper;
import com.whln.ordermax.data.service.MapCache;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liurun
 * @since 2023-04-23
 */
@AllArgsConstructor
@Service
public class InvoiceServiceImpl extends ServiceImpl<InvoiceMapper, Invoice> implements InvoiceService {
    private final InvoiceContractService invoiceContractService;
    private final MapCache mapCache;
    private final ApplicationContext applicationContext;
    @Override
    public Page<Invoice> pageAll(Invoice entity, Page<Invoice> page) {
      Page<Invoice> resultPage = baseMapper.listByEntity(entity, page);
        List<Invoice> records = resultPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)){
            String billCode = entity.getBillCode();
            if (ObjectUtil.isEmpty(billCode)) {
                throw new BusinessException("单据类型不能为空");
            }
            Set<String> invoiceIdList = records.stream().map(Invoice::getId).collect(Collectors.toSet());
            List<InvoiceContract> invoiceContractList = invoiceContractService.list(new LambdaQueryWrapper<InvoiceContract>()
                    .eq(InvoiceContract::getBillCode, billCode)
                    .in(InvoiceContract::getInvoiceId, invoiceIdList));
            Map<String, List<String>> collect = invoiceContractList.stream().collect(Collectors.groupingBy(InvoiceContract::getInvoiceId,Collectors.mapping(InvoiceContract::getBillId,Collectors.toList())));

            Set<String> billIds = invoiceContractList.stream().map(InvoiceContract::getBillId).collect(Collectors.toSet());
            Map<String, String> billCodeWithCommodity = mapCache.codeFullCommodityRepositoryMap();
            String s = billCodeWithCommodity.get(billCode);
            List<Object> commodityList;
            try {
                Object bean = applicationContext.getBean(Class.forName(s));
                BillQuery build = BillQuery.builder().billIds(billIds).build();
                commodityList = ReflectUtil.invoke(bean, "commodityList", build);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw new BusinessException("单据类型错误");
            }
            for (Invoice i:records){
                List<String> list = collect.get(i.getId());
                if (ObjectUtil.isNotEmpty(list)) {
                    List<Object> billCommodityList = commodityList.stream().filter(e -> {
                        Object billId1 = ReflectUtil.getFieldValue(e, "billId");
                        if (ObjectUtil.isEmpty(billId1)) {
                            throw new BusinessException("单据下商品数据错误");
                        }
                        return list.contains(billId1.toString());
                    }).collect(Collectors.toList());
                    i.setCommodityList(billCommodityList);
                }
            }
            resultPage.setRecords(records);
        }

        return resultPage;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(Invoice entity) {
        baseMapper.insert(entity);
        List<InvoiceContract> invoiceContractList = getInvoiceContracts(entity);
        invoiceContractService.saveBatch(invoiceContractList);
    }



    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(Invoice entity) {
        LambdaUpdateWrapper<Invoice> wrapper = new LambdaUpdateWrapper<Invoice>().eq(Invoice::getId, entity.getId());
        baseMapper.update(entity, wrapper);
        invoiceContractService.remove(new LambdaUpdateWrapper<InvoiceContract>().eq(InvoiceContract::getInvoiceId, entity.getId()));
        invoiceContractService.saveBatch(getInvoiceContracts(entity));
    }
    private List<InvoiceContract> getInvoiceContracts(Invoice entity) {
        List<String> billList = entity.getBillList();
        String billCode = entity.getBillCode();
        Map<String, String> repositoryMap = mapCache.codeFullRepositoryMap();
        try {
            UpdateWrapper<Object> objectUpdateWrapper = new UpdateWrapper<>();
            objectUpdateWrapper.set("invoice_status", 1);
            objectUpdateWrapper.in("id", billList);
            Object bean = applicationContext.getBean(Class.forName(repositoryMap.get(billCode)));
            ReflectUtil.invoke(bean, "update", objectUpdateWrapper);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            throw new BusinessException("单据类型错误");
        }
        return billList.stream().map(e -> {
            InvoiceContract invoiceContract = new InvoiceContract();
            invoiceContract.setBillCode(entity.getBillCode());
            invoiceContract.setBillId(e);
            return invoiceContract;
        }).collect(Collectors.toList());
    }

    @Override
    public void delete(String id) {
        Invoice one = this.getOne(new LambdaQueryWrapper<Invoice>().eq(Invoice::getId, id));
        if ((ShiroManager.hasRole("webadmin")||ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId()))&& ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseMapper.deleteById(id);
            invoiceContractService.remove(new LambdaUpdateWrapper<InvoiceContract>().eq(InvoiceContract::getInvoiceId,id));
        }else {
            baseMapper.update(null,new LambdaUpdateWrapper<Invoice>().eq(Invoice::getId, id).set(Invoice::getIsDelete, 1));
        }
    }
}
