package com.whln.ordermax.api.service.email.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.api.config.FreeMarkerRender;
import com.whln.ordermax.api.service.email.EmailSignService;
import com.whln.ordermax.data.domain.EmailSign;
import com.whln.ordermax.data.domain.SysUser;
import com.whln.ordermax.data.domain.param.EmailSignParam;
import com.whln.ordermax.data.domain.vo.EmailSignVO;
import com.whln.ordermax.data.domain.vo.SignResult;
import com.whln.ordermax.data.mapper.EmailSignMapper;
import com.whln.ordermax.data.repository.SysUserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-02-21 18:48
 */
@AllArgsConstructor
@Component
public class EmailSignServiceImpl extends ServiceImpl<EmailSignMapper, EmailSign> implements EmailSignService {
    private final EmailSignMapper emailSignMapper;
    private final SysUserRepository sysUserRepository;

    private final FreeMarkerRender freeMarkerRender;

    @Override
    public List<EmailSignVO> list(EmailSignParam param) {
        LambdaQueryWrapper<EmailSign> wrapper = new LambdaQueryWrapper<EmailSign>()
                .eq(EmailSign::getRelationId, param.getRelationId());
        List<EmailSign> emailSigns = emailSignMapper.selectList(wrapper);
        SysUser sysUser = sysUserRepository.getUserByUserId(param.getRelationId());
        Map<String, Object> map = BeanUtil.beanToMap(sysUser);
        List<EmailSignVO> emailSignVOList = BeanUtil.copyToList(emailSigns, EmailSignVO.class);
        return emailSignVOList.stream().peek(item -> {
            String sign = item.getSign().trim();
            if (ObjectUtil.isNotEmpty(item.getHtml())) {
                Optional<String> tplText = freeMarkerRender.getTplText(item.getHtml(), sysUser);
                item.setHtml(tplText.get());
            }
            ArrayList<SignResult> objects = new ArrayList<>();
            String[] split = sign.split(",");
            for (String s : split) {
                Object o = map.get(s);
                if (ObjectUtil.isNotEmpty(o)) {
                    objects.add(new SignResult(s, o.toString()));
                }
            }
            item.setResult(objects);
        }).collect(Collectors.toList());
    }

    @Override
    public EmailSignVO get(EmailSignParam param) {
        EmailSign emailSign = emailSignMapper.selectById(param.getId());
        SysUser sysUser = sysUserRepository.getUserByUserId(param.getRelationId());
        Map<String, Object> map = BeanUtil.beanToMap(sysUser);
        String sign = emailSign.getSign();
        if (ObjectUtil.isNotEmpty(emailSign.getHtml())) {
            Optional<String> tplText = freeMarkerRender.getTplText(emailSign.getHtml(), sysUser);
            emailSign.setHtml(tplText.get());
        }
        map.forEach((k, v) -> {
            String s = sign.replaceAll(k, v.toString());
            emailSign.setSign(s);
        });

        return BeanUtil.copyProperties(emailSign, EmailSignVO.class);
    }


    @Override
    public void deleteSign(EmailSignParam param) {
        emailSignMapper.deleteById(param.getId());
    }

    @Override
    public void updateSign(EmailSignVO param) {
        emailSignMapper.updateById(BeanUtil.copyProperties(param, EmailSign.class));
    }

    @Override
    public void add(EmailSignVO param) {
        saveOrUpdate(BeanUtil.copyProperties(param, EmailSign.class));
    }
}
