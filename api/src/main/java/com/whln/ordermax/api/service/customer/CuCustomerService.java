package com.whln.ordermax.api.service.customer;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.auth.AuthService;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.PageWithUserIgnore;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.vo.CuCustomerVO;
import com.whln.ordermax.data.domain.vo.EmailHistoryVO;
import com.whln.ordermax.data.repository.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 客户业务处理
 */
@Slf4j
@AllArgsConstructor
@Service
public class CuCustomerService {
    private final CuCustomerRepository baseRepository;

    private final CuCustomerContactsRepository contactsRepository;
    private final CuContactsEmailRepository cuContactsEmailRepository;

    private final CuContactsEmailRepository emailRepository;

    private final EmInboxRepository inboxRepository;

    private final CuCustomerOwnerRepository customerOwnerRepository;
    private final  SysService sysService;
    private final  SysUserRepository sysUserRepository;

    /**
     * 查询所有
     */
    public Result listAll(CuCustomer entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */


    public Result<IPage<CuCustomer>> pageAll(CuCustomer entity, Page<CuCustomer> page) {
        if (ShiroManager.hasRole("supplier")) {
            throw new BusinessException("无权限查看");
        }
        if (!ShiroManager.getSubject().hasRole("webadmin")) {
            entity.setOwnerId(ShiroManager.getUserId());
        }
        IPage<CuCustomer> resultPage = baseRepository.pageByEntity(entity, page);
        return Result.success(resultPage);
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> save(CuCustomer entity) {
        CuCustomer cuCustomer = baseRepository.getOne(new LambdaQueryWrapper<CuCustomer>().eq(CuCustomer::getCustomerNo, entity.getCustomerNo()));
        if (ObjectUtil.isNotEmpty(cuCustomer)&&!ObjectUtil.equals(entity.getId(),cuCustomer.getId())) {
            throw new BusinessException("编号已存在");
        }
        SysUser owner = sysUserRepository.getOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getId, entity.getOwnerId()));
        if (ObjectUtil.isEmpty(owner)) {
            throw new BusinessException("拥有人不存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        if (CollectionUtil.isNotEmpty(entity.getContactsDelIdList())) {
            contactsRepository.removeByIds(entity.getContactsDelIdList());
            cuContactsEmailRepository.remove(new LambdaUpdateWrapper<CuContactsEmail>()
                    .in(CuContactsEmail::getContactId, entity.getContactsDelIdList()));
        }
        List<CuCustomerContacts> contactsList = entity.getContactsList();
        if (CollectionUtil.isNotEmpty(contactsList)) {
            contactsList = contactsList.stream().peek(item -> {
                item.setCustomerId(entity.getId());
            }).collect(Collectors.toList());
            contactsRepository.saveOrUpdateBatch(contactsList);
        }

        if (CollectionUtil.isNotEmpty(entity.getEmailDelIds())) {
            emailRepository.removeByIds(entity.getEmailDelIds());
        }
        if (CollectionUtil.isNotEmpty(entity.getEmails())) {
            List<HashMap<String, String>> emailMapList = entity.getEmails().stream().map(e -> {
                emailRepository.saveOrUpdate(e);
                return new HashMap<String, String>(2) {{
                    put("opppsiteContactsId", e.getContactId());
                    put("oppositeEmail", e.getEmailAddress());
                }};
            }).collect(Collectors.toList());
            //绑定邮箱
            inboxRepository.updateInboxBind(entity.getId(), emailMapList, "CUST");
        }
        Map<String, Object> message = new HashMap<>();

        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> delete(String id) {
        baseRepository.update(new LambdaUpdateWrapper<CuCustomer>().set(CuCustomer::getIsDelete, 1).eq(CuCustomer::getId, id));
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> batchDelete(List<String> ids) {
        baseRepository.update(new LambdaUpdateWrapper<CuCustomer>().set(CuCustomer::getIsDelete, 1).in(CuCustomer::getId, ids));
        return Result.success();
    }

    @Resource
    private DynamicRepository dynamicRepository;

    public Result<List<Map<String, Object>>> listFollow(String id) {
        return Result.success(dynamicRepository.listFollowByCustomerId(id));
    }

    @PageWithUserIgnore
    public Result<IPage<CuCustomer>> pagePub(CuCustomer entity, Page<CuCustomer> page) {
        entity.setIsPub(1);
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    public Result<Page<CuCustomer>> claim(String customerId) {
        String userId = AuthService.getUserIdFromAuthentication();
        return baseRepository.updateById(
                new CuCustomer() {{
                    setId(customerId);
                    setOwnerId(userId);
                    setIsPub(0);
                }}
        ) ? Result.success() : Result.error("201", "操作失败");
    }

    public Result<List<Map>> listCommodityByCustomerId(String id, String searchInfo, String billCode) {
        return Result.success(baseRepository.listCommodityByCustomerId(id, searchInfo, billCode));
    }

    public Result<Page<CuCustomer>> transferPub(String customerId) {
        return baseRepository.update(new UpdateWrapper<CuCustomer>().set("is_pub", true).eq("id", customerId)) ? Result.success() : Result.error("201", "操作失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Void> saveSharer(JSONObject entity) {
        String id = entity.getStr("id");
        List<String> sharerIdList = entity.getBeanList("sharerIdList", String.class);
        try {
            for (String sharerId : sharerIdList) {
                customerOwnerRepository.save(new CuCustomerOwner() {{
                    setId(id);
                    setSharerId(sharerId);
                    setIsFull(0);
                }});
            }
        } catch (Exception e) {
            return Result.error("201", "操作失败");
        }
        return Result.success();
    }

    public List<EmailHistoryVO> emailHistory(String costomerId) {
        return baseRepository.emailHistory(costomerId);
    }


    public List<CuCustomerVO> batchImport(List<CuCustomerVO> cuCustomerVOList) {
        List<String> dataBase = baseRepository.listObjs(new LambdaQueryWrapper<CuCustomer>()
                .select(CuCustomer::getCustomerNo), Objects::toString);
        List<String> ownerNames = cuCustomerVOList.stream().map(CuCustomerVO::getOwnerName).collect(Collectors.toList());
        List<SysUser> sysUserList = sysUserRepository.list(new LambdaQueryWrapper<SysUser>().in(SysUser::getRealname, ownerNames));
        Map<String, String> nameIdMap = sysUserList.stream().collect(Collectors.toMap(SysUser::getRealname, SysUser::getId));
        List<CuCustomerVO> collect = cuCustomerVOList.stream()
                .filter(item -> !dataBase.contains(item.getCustomerNo()))
                .peek(item -> {
                    String sysUserId = nameIdMap.get(item.getOwnerName());
                    if (ObjectUtil.isNotEmpty(sysUserId)) {
                        item.setOwnerId(sysUserId);
                    }else {
                        item.setOwnerId(ShiroManager.getUserId());
                    }
                    item.setCreateDate(LocalDateTime.now());
                    item.setUpdateDate(LocalDateTime.now());
                })
                .collect(Collectors.toList());
        List<CuCustomer> cuCustomerList = new ArrayList<>();
        List<CuCustomerContacts> contacts = new ArrayList<>();
        List<CuContactsEmail> cuContactsEmails = new ArrayList<>();
        for (CuCustomerVO cuCustomerVO : collect) {
            CuCustomer cuCustomer = BeanUtil.copyProperties(cuCustomerVO, CuCustomer.class);
            cuCustomer.setId(sysService.nextId());
            cuCustomerList.add(cuCustomer);
            List<CuCustomerContacts> contactsList = cuCustomerVO.getContactsList();
            for (CuCustomerContacts cc : contactsList) {
                cc.setCustomerId(cuCustomer.getId());
                cc.setOwnerId(ShiroManager.getUserId());
                cc.setId(sysService.nextId());
                contacts.add(cc);
                List<CuContactsEmail> emails = cc.getEmails();
                for (CuContactsEmail ce : emails) {
                    ce.setCustomerId(cuCustomer.getId());
                    ce.setContactId(cc.getId());
                    cuContactsEmails.add(ce);
                }
            }
        }
        contactsRepository.saveBatch(contacts);
        baseRepository.saveBatch(cuCustomerList);
        if (CollectionUtil.isNotEmpty(cuContactsEmails)) {
            Map<String, List<CuContactsEmail>> collect1 = cuContactsEmails.stream().collect(Collectors.groupingBy(CuContactsEmail::getCustomerId));
            collect1.forEach((k, v) -> {
                List<HashMap<String, String>> emailMapList = v.stream().map(e -> {
                    emailRepository.saveOrUpdate(e);
                    return new HashMap<String, String>() {{
                        put("opppsiteContactsId", e.getContactId());
                        put("oppositeEmail", e.getEmailAddress());
                        put("customerId", e.getCustomerId());
                    }};
                }).collect(Collectors.toList());
                //绑定邮箱
                inboxRepository.updateInboxBind(k, emailMapList, "CUST");
            });
        }
        cuCustomerVOList.removeAll(collect);
        return cuCustomerVOList;
    }

    public List<CuCustomerVO> checkRepeat(List<CuCustomerVO> cuCustomerVOList) {
        List<String> dataBase = baseRepository.listObjs(new LambdaQueryWrapper<CuCustomer>()
                .select(CuCustomer::getCustomerNo), Objects::toString);
        return cuCustomerVOList.stream()
                .peek(item -> {
                    item.setId(null);
                    if (dataBase.contains(item.getCustomerNo())) {
                        item.setRepeat(1);
                    } else {
                        item.setRepeat(0);
                    }
                })
                .collect(Collectors.toList());
    }
}