package com.whln.ordermax.api.service.poOrder;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.annotations.OaBillSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.PoContract;
import com.whln.ordermax.data.domain.PoContractCommodity;
import com.whln.ordermax.data.mapper.PoContractCommodityMapper;
import com.whln.ordermax.data.repository.PoContractCommodityRepository;
import com.whln.ordermax.data.repository.PoContractRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 采购合同业务处理
 */
@BillCode(ModelMessage.PO_CONTRACT)
@Service
public class PoContractService {
    @Resource
    private PoContractRepository baseRepository;

    @Resource
    private PoContractCommodityRepository commodityRepository;


    /**
     * 查询所有
     */
    public Result listAll(PoContract entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    @Resource
    PoContractCommodityMapper poContractCommodityMapper;
    /**
     * 分页查询所有
     */
    public Result pageAll(PoContract entity, Page<PoContract> page) {
        IPage<PoContract> poContractIPage = baseRepository.pageByEntity(entity, page);
        return Result.success(poContractIPage);
    }

    /**
     * 插入或更新
     */
    @OaBillSave
    @Transactional(rollbackFor = Exception.class)
    @FlowControlSave()
    public Result<Void> save(PoContract entity) {
        PoContract poContract = baseRepository.getOne(new LambdaQueryWrapper<PoContract>().eq(PoContract::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(poContract)&&!ObjectUtil.equals(entity.getId(),poContract.getId())) {
            throw new BusinessException("编号已存在");
        }
        List<PoContractCommodity> commodityList = entity.getCommodityList();
        List<PoContractCommodity> nullBatchNo = commodityList.stream().filter(e -> ObjectUtil.isEmpty(e.getCommodityBatchNo())).collect(Collectors.toList());

        if (ObjectUtil.isNotEmpty(nullBatchNo)){
            throw new BusinessException("商品批号不能为空");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<String> idList = commodityList.stream().map(PoContractCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<PoContractCommodity>().eq(PoContractCommodity::getBillId, entity.getId()).notIn(ObjectUtil.isNotEmpty(idList), PoContractCommodity::getId, idList));
        if (CollectionUtil.isNotEmpty(commodityList)) {
            commodityRepository.saveOrUpdateBatch(commodityList);
        }
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        PoContract one = baseRepository.getOne(new LambdaQueryWrapper<PoContract>().eq(PoContract::getId, id));
        if ((ShiroManager.hasRole("webadmin")||ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId()))&& ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            commodityRepository.remove(new LambdaQueryWrapper<PoContractCommodity>().eq(PoContractCommodity::getBillId, id));
        }else {
            baseRepository.update(new LambdaUpdateWrapper<PoContract>().eq(PoContract::getId, id).set(PoContract::getIsDelete,1));
        }
        return  Result.success();
    }

    /**
    *  批量删除
    */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<PoContract> enquiryOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = enquiryOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = enquiryOrders.stream()
                .map(PoContract::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new LambdaQueryWrapper<PoContractCommodity>().in(PoContractCommodity::getBillId, ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<PoContract>().in(PoContract::getId, ids).set(PoContract::getIsDelete, 1));
            }
        }

        return Result.success();
    }

}