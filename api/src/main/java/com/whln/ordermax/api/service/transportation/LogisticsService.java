package com.whln.ordermax.api.service.transportation;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.vo.ListLogisticsStatVO;
import com.whln.ordermax.data.mapper.ListLogisticsMapper;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 订单进度业务处理
 */
@Service
public class LogisticsService {


    public String internalSearch(String trackingNo, String logisticsCompanyCode) {
        try {
            Connection.Response execute = Jsoup.connect("https://www.kuaidi100.com/chaxun").data(
                    new HashMap<String,String>(){{
                        put("com",logisticsCompanyCode);
                        put("nu",trackingNo);
                    }}
            ).method(Connection.Method.GET).ignoreContentType(true).execute();
            return execute.body();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String internalSearchFrame() {
        return "<!DOCTYPE HTML><html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"><head></head><body><iframe name=\"kuaidi100\" src=\"https://www.kuaidi100.com/frame/index.html\" width=\"960\" height=\"880\" marginwidth=\"0\" marginheight=\"0\" hspace=\"0\" vspace=\"0\" frameborder=\"0\" scrolling=\"no\"></iframe></body></html>";
    }
    @Resource
    ListLogisticsMapper listLogisticsMapper;
    public Result<List<ListLogisticsStatVO>> logisticsStat(Map<String,String> entity) {
        return Result.success( listLogisticsMapper.LogisticsStatList(entity));
    }
}