package com.whln.ordermax.api.controller.commodity;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.commodity.CmCommodityService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.CmCommodity;
import com.whln.ordermax.data.domain.CmCommodityBom;
import com.whln.ordermax.data.domain.CmCommodityImg;
import com.whln.ordermax.data.domain.CmCommodityStandard;
import com.whln.ordermax.data.domain.param.TransactionHistoryParam;
import com.whln.ordermax.data.domain.vo.CmCommodityCheckVO;
import com.whln.ordermax.data.domain.vo.CmCommodityVO;
import com.whln.ordermax.data.domain.vo.CommodityTransactionHistoryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Api(tags = "商品")
@RestController
@RequestMapping("/CmCommodity")
public class CmCommodityController {

    @Resource
    private CmCommodityService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<CmCommodity>> listAll(@RequestBody CmCommodity entity) {
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "报价单专用接口|通过客户id查询所有商品并携带客户最近一次报价")
    @PostMapping("/pageWithLastQoPriceByCustId")
    public Result<IPage<CmCommodity>> pageWithLastQoPriceByCustId(@RequestBody PageEntity<CmCommodity> pageEntity) {
        return baseService.pageWithLastQoPriceByCustId(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }


    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<CmCommodity>> pageAll(@RequestBody PageEntity<CmCommodity> pageEntity) {
        return baseService.pageAll(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "保存商品及商品图片")
    @PostMapping("/save")
    public Result save(@RequestBody()@Validated CmCommodity entity) throws BusinessException {
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除商品和商品图片")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id) {
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除和商品图片")
    @PostMapping("/batchDelete")
    public Result<Void> batchDelete(@RequestBody() List<String> ids) {
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "批量导入商品")
    @PostMapping("/batchImport")
    public Result<List<CmCommodity>> batchImport(@RequestBody List<CmCommodity> commodityList) {
        return Result.success(baseService.batchImport(commodityList));
    }

    @ApiOperation(value = "检查重复的商品编号")
    @PostMapping("/checkRepeat")
    public Result<CmCommodityCheckVO> checkRepeat(@RequestBody List<CmCommodityVO> cmCommodityList) {
        CmCommodityCheckVO cmCommodities = baseService.checkRepeat(cmCommodityList);
        return Result.success(cmCommodities);
    }

    @ApiOperation(value = "批量保存商品并返回商品信息")
    @PostMapping("/batchSaveAndReturnInfo")
    public Result<List<CmCommodity>> batchSaveAndReturnInfo(@RequestBody() List<Map<String, Object>> cmCommodityList) {
        return baseService.batchSaveAndReturnInfo(cmCommodityList);
    }

    @ApiOperation(value = "通过商品id查询查询图片路径")
    @PostMapping("/listImg")
    public Result<Map<String, List<CmCommodityImg>>> listImg(@RequestBody CmCommodityImg entity) {
        return baseService.listImg(entity);
    }


    @ApiOperation(value = "批量验证商品是否存在并分类返回")
    @PostMapping("/batchVerifyExsits")
    public Result batchVerifyExsits(@RequestBody() List<Map<String, Object>> cmCommodityList) {
        return baseService.batchVerifyExsits(cmCommodityList);
    }

    @ApiOperation(value = "查询bom")
    @PostMapping("/listBom")
    public Result<List<CmCommodityBom>> listBom(@RequestBody CmCommodityBom entity) {
        return baseService.listBom(entity);
    }

    @ApiOperation(value = "查询检验标准")
    @PostMapping("/listStandard")
    public Result<List<CmCommodityStandard>> listStandard(@RequestParam("id") String id) {
        return baseService.listStandard(id);
    }

    @ApiOperation(value = "查询交易历史")
    @PostMapping("/listDeal")
    public Result<Page<CommodityTransactionHistoryVO>> listDeal(@RequestBody TransactionHistoryParam param) {
        return baseService.listDeal(param);
    }


}