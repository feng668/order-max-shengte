package com.whln.ordermax.api.shiro.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.session.mgt.eis.SessionDAO;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class BDSessionListener implements SessionListener {
	SessionDAO sessionDAO;

	public BDSessionListener(SessionDAO sessionDAO) {
		this.sessionDAO = sessionDAO;
	}

	private final AtomicInteger sessionCount = new AtomicInteger(0);
	@Override
	public void onStart(Session session) {
		sessionCount.incrementAndGet();
	}

	@Override
	public void onStop(Session session) {
		sessionCount.decrementAndGet();
	}

	@Override
	public void onExpiration(Session session) {
		sessionCount.decrementAndGet();
	}

	public int getSessionCount() {
		return sessionCount.get();
	}
}
