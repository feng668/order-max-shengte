package com.whln.ordermax.api.service.product;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.annotations.OrderPlanSave;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.param.PdQualityBatchSaveParam;
import com.whln.ordermax.data.domain.vo.PdQualityStandardVO;
import com.whln.ordermax.data.mapper.PdQualityStandardMapper;
import com.whln.ordermax.data.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 质检单业务处理
 */
@BillCode(ModelMessage.PD_QUALITY)
@Service
public class PdQualityService {
    @Resource
    private PdQualityRepository baseRepository;

    @Resource
    private PdQualityStandardRepository standardRepository;
    @Resource
    private CmCommodityStandardRepository cmCommodityStandardRepository;
    @Resource
    private PdQualityStandardRepository pdQualityStandardRepository;
    @Resource
    private OrderPlanRepository orderPlanRepository;

    @Resource
    private PdQualityStandardMapper pdQualityStandardMapper;
    @Resource
    private PdQualityLotRepository pdQualityLotRepository;
    @Resource
    private SysService sysService;

    /**
     * 查询所有
     */
    public Result<List<PdQuality>> listAll(PdQuality entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }


    /**
     * 分页查询所有
     */
    public Result<IPage<PdQuality>> pageAll(PdQuality entity, Page<PdQuality> page) {
        IPage<PdQuality> iPage = baseRepository.pageByEntity(entity, page);
        List<PdQuality> records = iPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> pdQualityIds = records.stream()
                    .map(PdQuality::getId)
                    .collect(Collectors.toSet());
            List<PdQualityLot> lotList = pdQualityLotRepository.list(new LambdaQueryWrapper<PdQualityLot>().in(PdQualityLot::getBillId, pdQualityIds));
            Map<String, List<PdQualityLot>> lotMap = lotList.stream().collect(Collectors.groupingBy(PdQualityLot::getBillId));

            List<PdQualityStandard> pdQualityStandards = pdQualityStandardMapper
                    .selectList(new LambdaQueryWrapper<PdQualityStandard>()
                            .in(PdQualityStandard::getQualityId, pdQualityIds));
            Map<String, List<PdQualityStandard>> pdQualityStandardGroup =
                    pdQualityStandards.stream()
                            .collect(Collectors.groupingBy(PdQualityStandard::getQualityId));
            List<PdQuality> pdQualitySetStandardList = records.stream()
                    .peek(ite -> {
                        ite.setStandardList(pdQualityStandardGroup.get(ite.getId()));
                        ite.setLotList(lotMap.get(ite.getId()));
                    })
                    .collect(Collectors.toList());
            iPage.setRecords(pdQualitySetStandardList);
        }
        return Result.success(iPage);
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    @OrderPlanSave
    public Result save(PdQuality entity) {
        PdQuality pdQuality = baseRepository.getOne(new LambdaQueryWrapper<PdQuality>().eq(PdQuality::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(pdQuality) && !ObjectUtil.equals(entity.getId(), pdQuality.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<PdQualityStandard> standardList = entity.getStandardList();
        standardRepository.remove(new LambdaUpdateWrapper<PdQualityStandard>()
                .eq(PdQualityStandard::getQualityId, entity.getId())
               );
        if (CollectionUtil.isNotEmpty(standardList)) {
            standardRepository.saveBatch(standardList);
        }

        List<PdQualityLot> lotList = entity.getLotList();
        if (ObjectUtil.isEmpty(lotList)) {
            lotList = new ArrayList<>();
        }
        Set<String> lotIds = lotList.stream()
                .map(PdQualityLot::getId)
                .filter(ObjectUtil::isNotEmpty)
                .collect(Collectors.toSet());
        pdQualityLotRepository.remove(new LambdaUpdateWrapper<PdQualityLot>()
                .eq(PdQualityLot::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(lotIds), PdQualityLot::getId, lotIds)
        );
        this.pdQualityLotRepository.saveOrUpdateBatch(entity.getLotList());
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public void delete(String id) {
        if (ShiroManager.hasRole("webadmin")) {
            baseRepository.removeById(id);
            standardRepository.remove(new QueryWrapper<PdQualityStandard>().eq("quality_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<PdQuality>().eq(PdQuality::getId, id).set(PdQuality::getIsDelete, 1));
        }

    }


    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> batchDelete(List<String> ids) {
        List<PdQuality> enquiryOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = enquiryOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = enquiryOrders.stream()
                .map(PdQuality::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                standardRepository.remove(new QueryWrapper<PdQualityStandard>().in("bill_id", ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<PdQuality>().in(PdQuality::getId, ids).set(PdQuality::getIsDelete, 1));
            }
        }
        return Result.success();
    }

    @Resource
    PdProductionTaskRepository pdProductionTaskRepository;

    @FlowControlSave
    @Transactional(rollbackFor = Exception.class)
    public List<PdQuality> batchSave(PdQualityBatchSaveParam param) {
        /*PdProductionTask allById = pdProductionTaskRepository.getById(param.getLastBillId());
        if (ObjectUtil.equals(allById.getFlowStatus(), YesOrNo.YES.getState())) {
            throw new BusinessException("生产任务单已完成");
        }
        pdProductionTaskRepository.update(new LambdaUpdateWrapper<PdProductionTask>().set(PdProductionTask::getFlowStatus, 1).eq(PdProductionTask::getId,param.getLastBillId()));
       */
        List<PdQuality> pdQualityList = param.getEntityList();
        if (ObjectUtil.isEmpty(pdQualityList)) {
            throw new BusinessException("质检单数量不能小于0");
        }
        Set<String> commodityIds = pdQualityList.stream()
                .map(PdQuality::getCommodityId)
                .collect(Collectors.toSet());
        List<CmCommodityStandard> commodityStandardList = cmCommodityStandardRepository
                .list(new LambdaQueryWrapper<CmCommodityStandard>()
                        .select(CmCommodityStandard::getStandardCondition,
                                CmCommodityStandard::getFactor,
                                CmCommodityStandard::getCommodityId,
                                CmCommodityStandard::getId)
                        .in(CmCommodityStandard::getCommodityId, commodityIds)
                );

        Map<String, List<CmCommodityStandard>> groupByCommodityId =
                commodityStandardList.stream().collect(Collectors.groupingBy(CmCommodityStandard::getCommodityId));
        List<PdQualityStandard> pdQualityStandards = new ArrayList<>();
        String lastBillId = param.getLastBillId();
        String lastBillCode = param.getLastBillCode();
        OrderPlan lastOrderPlan = null;
        if (ObjectUtil.isNotEmpty(lastBillId)) {
            lastOrderPlan = orderPlanRepository.getOne(
                    new LambdaQueryWrapper<OrderPlan>()
                            .eq(OrderPlan::getBillId, lastBillId)
                            .eq(OrderPlan::getBillCode, lastBillCode));
            orderPlanRepository.update(new LambdaUpdateWrapper<OrderPlan>()
                    .eq(OrderPlan::getBillId, param.getLastBillId())
                    .eq(OrderPlan::getBillCode, param.getLastBillCode())
                    .set(OrderPlan::getBillStatus, 2));
        }
        List<OrderPlan> orderPlanList = new ArrayList<>();
        OrderPlan finalLastOrderPlan = lastOrderPlan;
        pdQualityList = pdQualityList.stream()
                .sorted(Comparator.comparingInt(PdQuality::getFurnaceNo))
                .peek(item -> {
                    item.setSortTime(LocalDateTime.now());
                    item.setId(sysService.nextId());
                    String pdQualityId = item.getId();
                    List<CmCommodityStandard> cmCommodityStandards = groupByCommodityId.get(item.getCommodityId());
                    if (ObjectUtil.isNotEmpty(cmCommodityStandards)) {
                        for (CmCommodityStandard c : cmCommodityStandards) {
                            if (c.check()) {
                                throw new BusinessException("检验标准数据异常");
                            }
                            PdQualityStandard pdQualityStandard = new PdQualityStandard();
                            pdQualityStandard.setQualityId(pdQualityId);
                            pdQualityStandard.setStandardCondition(c.getStandardCondition());
                            pdQualityStandard.setFactor(c.getFactor());
                            pdQualityStandards.add(pdQualityStandard);
                        }
                    }
                    OrderPlan orderPlan = new OrderPlan();
                    orderPlan.setLastBillId(ObjectUtil.isEmpty(lastBillId) ? item.getId() : lastBillId);
                    orderPlan.setLastBillCode(ObjectUtil.isEmpty(lastBillCode) ? ModelMessage.PD_QUALITY.getCode() : lastBillCode);
                    orderPlan.setBillId(item.getId());
                    orderPlan.setBillCode(ModelMessage.PD_QUALITY.getCode());
                    orderPlan.setRootBillId(ObjectUtil.isEmpty(finalLastOrderPlan) ? item.getId() : finalLastOrderPlan.getRootBillId());
                    orderPlan.setBillStatus(1);
                    orderPlan.setTemId(1);
                    orderPlan.setBillNo(item.getBillNo());
                    orderPlanList.add(orderPlan);
                })
                .collect(Collectors.toList());

        //取出所有lot   保存
        List<PdQualityLot> lotList = pdQualityList.stream().reduce(new ArrayList<>(), (list1, item) -> {
            if (ObjectUtil.isNotEmpty(item.getLotList())) {
                list1.addAll(item.getLotList());
            }
            return list1;
        }, (x, y) -> x);
        if (ObjectUtil.isNotEmpty(lotList)) {
            pdQualityLotRepository.saveOrUpdateBatch(lotList);
        }

        if (ObjectUtil.isNotEmpty(pdQualityStandards)) {
            pdQualityStandardRepository.saveBatch(pdQualityStandards);
        }
        baseRepository.saveBatch(pdQualityList);
        if (ObjectUtil.isNotEmpty(orderPlanList)) {
            orderPlanRepository.saveBatch(orderPlanList);
        }

        return pdQualityList;
    }


    public List<PdQualityStandardVO> getPdQualityStandard(String pdQualityId) {
        List<PdQualityStandard> list = standardRepository.list(new LambdaQueryWrapper<PdQualityStandard>().eq(PdQualityStandard::getQualityId, pdQualityId));
        return BeanUtil.copyToList(list, PdQualityStandardVO.class);
    }

}