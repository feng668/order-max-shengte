package com.whln.ordermax.api.controller.customer;

import com.whln.ordermax.api.service.customer.CuCustomerTransferSettingService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CuCustomerTransferRule;
import com.whln.ordermax.data.domain.CuCustomerTransferSetting;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  客户公海设置控制器
 */
@Api(tags = "客户公海设置")
@RestController
@RequestMapping("/customerTransferSetting")
public class CuCustomerTransferSettingController{

    @Resource
    private CuCustomerTransferSettingService baseService;

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<CuCustomerTransferSetting> getAllById(@RequestParam("id") String id){
        return baseService.getAll();
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() CuCustomerTransferSetting entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "查询公海规则")
    @PostMapping("/listRule")
    public Result<List<CuCustomerTransferRule>> listRule(){
        return baseService.listRule();
    }

    @ApiOperation(value = "开启或关闭公海自动转移")
    @PostMapping("/contrSync")
    public Result contrSync(@RequestBody CuCustomerTransferSetting entity){
        return baseService.contrSync(entity);
    }

}