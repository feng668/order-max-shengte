package com.whln.ordermax.api.service.quote;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.OaBillSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.CmCommodity;
import com.whln.ordermax.data.domain.QoQuoteCommodity;
import com.whln.ordermax.data.domain.QoQuoteOrder;
import com.whln.ordermax.data.domain.param.QoQuoteCommodityVOImportParam;
import com.whln.ordermax.data.domain.vo.QoQuoteCommodityImportVO;
import com.whln.ordermax.data.domain.vo.QoQuoteCommodityVO;
import com.whln.ordermax.data.repository.CmCommodityRepository;
import com.whln.ordermax.data.repository.QoQuoteCommodityRepository;
import com.whln.ordermax.data.repository.QoQuoteOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 报价单业务处理
 */
@AllArgsConstructor
@BillCode(ModelMessage.QO_QUOTE_ORDER)
@Service
public class QoQuoteOrderService {
    private final QoQuoteOrderRepository baseRepository;

    private final QoQuoteCommodityRepository quoteCommodityRepository;

    private final CmCommodityRepository commodityRepository;
    /**
     * 查询所有
     */
    public Result listAll(QoQuoteOrder entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(QoQuoteOrder entity, Page<QoQuoteOrder> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    @OaBillSave
    @Transactional(rollbackFor = Exception.class)
    public Result save(QoQuoteOrder entity) {
        QoQuoteOrder qoQuoteOrder = baseRepository.getOne(new LambdaQueryWrapper<QoQuoteOrder>().eq(QoQuoteOrder::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(qoQuoteOrder)&&!ObjectUtil.equals(entity.getId(),qoQuoteOrder.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);

        List<QoQuoteCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream().map(QoQuoteCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        quoteCommodityRepository.remove(new LambdaUpdateWrapper<QoQuoteCommodity>()
                .eq(QoQuoteCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(idList), QoQuoteCommodity::getId, idList)
        );
        if (CollectionUtil.isNotEmpty(commodityList)) {
            quoteCommodityRepository.saveOrUpdateBatch(commodityList.stream().peek(e->e.setBillId(entity.getId())).collect(Collectors.toList()));
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        QoQuoteOrder one = baseRepository.getOne(new LambdaQueryWrapper<QoQuoteOrder>().eq(QoQuoteOrder::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            quoteCommodityRepository.remove(new LambdaQueryWrapper<QoQuoteCommodity>().eq(QoQuoteCommodity::getBillId, id));
            baseRepository.removeById(id);
        } else {
            baseRepository.update(new LambdaUpdateWrapper<QoQuoteOrder>().eq(QoQuoteOrder::getId, id).set(QoQuoteOrder::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional
    public Result batchDelete(List<String> ids) {
        List<QoQuoteOrder> quoteOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = quoteOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = quoteOrders.stream()
                .map(QoQuoteOrder::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                quoteCommodityRepository.remove(new QueryWrapper<QoQuoteCommodity>().in("bill_id", ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<QoQuoteOrder>().in(QoQuoteOrder::getId, ids).set(QoQuoteOrder::getIsDelete, 1));
            }
        }
        return Result.success();
    }


    /**
     * 查询单条
     */
    public Result getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }



    public QoQuoteCommodityImportVO checkRepeat(List<QoQuoteCommodityVO> cmCommodityList) {
        List<QoQuoteCommodityVO> exits = new ArrayList<>(10);
        List<QoQuoteCommodityVO> noExits = new ArrayList<>(10);
        List<String> collect = cmCommodityList.stream()
                .map(QoQuoteCommodityVO::getPartNo)
                .collect(Collectors.toList());
        List<CmCommodity> commodityList = commodityRepository.list(new LambdaQueryWrapper<CmCommodity>()
                .in(CmCommodity::getPartNo, collect));
        Map<String, CmCommodity> cmCommodityMap = commodityList.stream()
                .collect(Collectors.toMap(CmCommodity::getPartNo, Function.identity()));
        List<String> partNos = commodityList.stream()
                .map(CmCommodity::getPartNo)
                .collect(Collectors.toList());

        for (QoQuoteCommodityVO c : cmCommodityList) {
            if (partNos.contains(c.getPartNo())) {
                CmCommodity cmCommodity = cmCommodityMap.get(c.getPartNo());
                c.setField(cmCommodity);
                exits.add(c);
            }else {
                noExits.add(c);
            }
        }
        QoQuoteCommodityImportVO cmCommodityCheckVO = new QoQuoteCommodityImportVO();
        cmCommodityCheckVO.setExits(exits);
        cmCommodityCheckVO.setNoExits(noExits);
        cmCommodityCheckVO.setExitsNum(exits.size());
        cmCommodityCheckVO.setNoExitsNum(noExits.size());
        return cmCommodityCheckVO;
    }

    public QoQuoteCommodityImportVO importCommodity(QoQuoteCommodityVOImportParam param) {
        QoQuoteOrder quoteOrder = baseRepository.getById(param.getBillId());
        if (ObjectUtil.isEmpty(quoteOrder)) {
            throw new BusinessException("报价单不存在");
        }
        List<QoQuoteCommodityVO> cmCommodityList = param.getCmCommodityList();
        List<String> collect = cmCommodityList.stream().map(QoQuoteCommodityVO::getPartNo).collect(Collectors.toList());
        List<CmCommodity> commodityList = commodityRepository.list(new LambdaQueryWrapper<CmCommodity>().in(CmCommodity::getPartNo, collect));
        Map<String, CmCommodity> cmCommodityMap = commodityList.stream().collect(Collectors.toMap(CmCommodity::getPartNo, Function.identity()));
        List<String> partNos = commodityList.stream().map(CmCommodity::getPartNo).collect(Collectors.toList());

        List<QoQuoteCommodityVO> exits = new ArrayList<>(10);
        List<QoQuoteCommodityVO> noExits = new ArrayList<>(10);
        for (QoQuoteCommodityVO commodityVO : param.getCmCommodityList()) {
            if (partNos.contains(commodityVO.getPartNo())) {
                commodityVO.setBillId(quoteOrder.getId());
                CmCommodity cmCommodity = cmCommodityMap.get(commodityVO.getPartNo());
                commodityVO.setField(cmCommodity);
                exits.add(commodityVO);
            } else {
                noExits.add(commodityVO);
            }
        }
        QoQuoteCommodityImportVO qoQuoteCommodityImportVO = new QoQuoteCommodityImportVO();
        qoQuoteCommodityImportVO.setExits(exits);
        qoQuoteCommodityImportVO.setNoExits(noExits);
        qoQuoteCommodityImportVO.setExitsNum(exits.size());
        qoQuoteCommodityImportVO.setNoExitsNum(noExits.size());
        List<CmCommodity> insertCommodity = BeanUtil.copyToList(noExits, CmCommodity.class);
        commodityRepository.saveBatch(insertCommodity);
        return qoQuoteCommodityImportVO;
    }
}