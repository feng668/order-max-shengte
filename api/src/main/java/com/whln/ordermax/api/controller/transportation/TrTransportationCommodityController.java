package com.whln.ordermax.api.controller.transportation;

import com.whln.ordermax.api.service.transportation.TrTransportationCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.TrTransportationCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  出运单商品控制器
 */
@Api(tags = "出运计划单商品")
@RestController
@RequestMapping("/TrTransportationCommodity")
public class TrTransportationCommodityController{

    @Resource
    private TrTransportationCommodityService baseService;

    @ApiOperation(value = "根据出运计划单id查询")
    @PostMapping("/list")
    public Result<List<TrTransportationCommodity>> listAll(@RequestBody TrTransportationCommodity entity){
        return baseService.listAll(entity);
    }

}