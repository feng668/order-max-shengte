package com.whln.ordermax.api.controller.email;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.email.EmailService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.EmInbox;
import com.whln.ordermax.data.domain.EmInboxAttachment;
import com.whln.ordermax.data.domain.EmSubmitted;
import com.whln.ordermax.data.domain.EmSubmittedEmails;
import com.whln.ordermax.data.domain.param.EmailFlagParam;
import com.whln.ordermax.data.domain.param.EmBoxParam;
import com.whln.ordermax.data.domain.query.DeleteQuery;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 收件箱控制器
 */
@Api(tags = "邮件管理")
@RestController
@RequestMapping("/email")
public class EmailController {

    @Resource
    private EmailService baseService;

    @ApiOperation(value = "查询收件箱未读数量")
    @PostMapping("/countNotRead")
    public Result<Integer> countNotRead() {
        return baseService.countNotRead();
    }

    @ApiOperation(value = "通过发送者类型分类收件箱邮件")
    @PostMapping("/classifyInboxByTypeCustomer")
    public Result<List<Map<String, Object>>> classifyInboxByTypeCustomer() {
        return baseService.classifyInboxByTypeCustomer();
    }


    @ApiOperation(value = "分页查询收件箱")
    @PostMapping("/pageInbox")
    public Result<IPage<EmInbox>> pageInbox(@RequestBody PageEntity<EmInbox> pageEntity) {
        return baseService.pageInbox(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "查询收件箱未读数量")
    @PostMapping("/readInbox")
    public Result readInbox(@RequestParam("inboxId") String inboxId) {
        return baseService.readInbox(inboxId);
    }

    @ApiOperation(value = "根据收件箱id查询附件")
    @PostMapping("/listAttachmentByInboxId")
    public Result<List<EmInboxAttachment>> listAttachmentByInboxId(@RequestParam(value = "inboxId") String inboxId, Integer type) {
        return baseService.listAttachmentByInboxId(inboxId, type);
    }

    @ApiOperation(value = "分页查询已发邮件")
    @PostMapping("/pageSubmitted")
    public Result<IPage<EmSubmitted>> pageSubmitted(@RequestBody PageEntity<EmSubmitted> pageEntity) {
        return baseService.pageSubmitted(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "查询已发邮件发送人")
    @PostMapping("/submittedEmails")
    public Result<Map<String, List<EmSubmittedEmails>>> submittedEmails(@RequestParam("submittedId") String submittedId) {
        return baseService.submittedEmails(submittedId);
    }

    @ApiOperation(value = "发送邮件并保存至发件箱")
    @PostMapping("/sendAndSave")
    public Result sendAndSave(@RequestBody @Validated EmSubmitted entity) {
        return baseService.sendAndSave(entity);
    }

    @ApiOperation(value = "保存至草稿")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody() EmSubmitted entity) {
        return baseService.save(entity);
    }

    @ApiOperation(value = "删除草稿")
    @PostMapping("/deleteEmSubmitted")
    public Result<Void> deleteEmSubmitted(@RequestBody() DeleteQuery param) {
        return baseService.deleteEmSubmitted(param);
    }

    @ApiOperation(value = "查询草稿")
    @PostMapping("/querySubmittedEmail")
    public Result querySubmittedEmail(@RequestBody() PageEntity<EmSubmitted> entity) {
        return baseService.list(entity);
    }

    @ApiOperation(value = "查询草稿详情")
    @GetMapping("/findSubmittedEmail")
    public Result<EmSubmitted> findSubmittedEmail(String id) {
        return baseService.get(id);
    }


    @ApiOperation(value = "单条删除收件箱邮件")
    @PostMapping("/deleteInbox")
    public Result<Void> deleteInbox(@RequestParam("id") String id) {
        return baseService.deleteInbox(id);
    }

    @ApiOperation(value = "单条删除回收站邮件")
    @PostMapping("/deleteRecycle")
    public Result deleteRecycle(@RequestParam("id") String id) {
        return baseService.deleteRecycle(id);
    }

    @ApiOperation(value = "批量删除收件箱邮件")
    @PostMapping("/batchDeleteInbox")
    public Result batchDeleteInbox(@RequestBody() List<String> ids) {
        return baseService.batchDeleteInbox(ids);
    }

    @ApiOperation(value = "批量删除回收站邮件")
    @PostMapping("/batchDeleteRecycle")
    public Result batchDeleteRecycle(@RequestBody() List<String> ids) {
        return baseService.batchDeleteRecycle(ids);
    }

    @ApiOperation(value = "标记收件箱邮件")
    @PostMapping("/flagInbox")
    public Result flagInbox(@RequestBody EmailFlagParam param) {
        return baseService.flagInbox(param);
    }

    @ApiOperation(value = "批量删除收件箱邮件")
    @PostMapping("/batchFlagInbox")
    public Result batchFlagInbox(@RequestBody List<String> ids) {
        return baseService.batchFlagInbox(ids);
    }

    @ApiOperation(value = "同步所有收件箱")
    @PostMapping("/syncAllInbox")
    public Result syncAllInbox() {
        return baseService.syncAllInbox();
    }

    @ApiOperation(value = "发件箱删除改为未删除")
    @PostMapping("/trashCanToRecive")
    public Result<Void> trashCanToRecive(@RequestBody EmBoxParam param) {
        baseService.trashCanToRecive(param);
        return Result.success();
    }

}