package com.whln.ordermax.api.init;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liurun
 * @date 2023-04-21 17:30
 */
@Order(2)
@Component
public class InitFactory<T> {
    private final List<InitData<T>> list = new ArrayList<>();

    public void register(InitData<T> initData) {
        list.add(initData);
    }

    public void start() {
        for (InitData<T> i:list){
            i.init();
        }
    }
}
