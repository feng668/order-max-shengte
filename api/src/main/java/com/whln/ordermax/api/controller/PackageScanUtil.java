package com.whln.ordermax.api.controller;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.data.domain.SysPower;
import com.whln.ordermax.data.repository.SysPowerRepository;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

@Component
public class PackageScanUtil {
    @Resource
    private SysService sysService;

    public List<SysPower> build(String parentId, String fieldSource, String entityName) throws ClassNotFoundException {
        Class<?> aClass = Class.forName("com.whln.ordermax.data.domain." + entityName);
        ResultMap acceptance = getField(aClass,parentId);
        List<SysPower> list = new ArrayList<>();
        SysPower one = sysPowerRepository.getOne(new LambdaQueryWrapper<SysPower>().eq(SysPower::getId, parentId));

        for (FieldInfo t : acceptance.getList()) {
            SysPower sysPower = new SysPower();
            sysPower.setId(sysService.nextId());
            sysPower.setParentIds(sysPower.getId() + "/" + parentId+"/"+one.getParentId());
            sysPower.setName(t.getDesc());
            sysPower.setParentId(parentId);
            sysPower.setPermission(t.getFieldName());
            sysPower.setFieldSource(fieldSource);
            sysPower.setEnabled(false);
            sysPower.setCategory("FIELD");
            sysPower.setMenuIcon("larry-icon larry-caidanguanli");
            list.add(sysPower);
        }
        if (ObjectUtil.isNotEmpty(acceptance.getPowers())) {
            list.addAll(acceptance.getPowers());
        }
        return list;
    }

    @Resource
    SysPowerRepository sysPowerRepository;

    public ResultMap getField(Class<?> aClass,String parentId) throws ClassNotFoundException {
        Field[] declaredFields = aClass.getDeclaredFields();
        ResultMap resultMap = new ResultMap();
        List<FieldInfo> list = new ArrayList<>();
        for (Field item : declaredFields) {
            String name = item.getName();
            if (name.equals("commodityList")) {
                List<SysPower> cm = sysPowerRepository.list(new LambdaQueryWrapper<SysPower>().eq(SysPower::getFieldSource, "CM").eq(SysPower::getSort,1));
                SysPower one = sysPowerRepository.getOne(new LambdaQueryWrapper<SysPower>().eq(SysPower::getId, parentId));
                List<SysPower> cMList = cm.stream().peek(item1 -> {
                    item1.setId(sysService.nextId());
                    item1.setParentId(parentId);
                    item1.setParentIds(item1.getId() + "/" + parentId + "/" + one.getParentId());
                    item1.setSort(null);
                }).collect(Collectors.toList());
                resultMap.setPowers(cMList);
            }

            TableField tableField = item.getAnnotation(TableField.class);
            if ((ObjectUtil.isNotEmpty(tableField) && !tableField.exist())) {
                continue;
            }
            if (name.contains("id")) {
                continue;
            }
            item.setAccessible(true);
            ApiModelProperty annotation = item.getAnnotation(ApiModelProperty.class);
            String desc = "";
            if (ObjectUtil.isNotEmpty(annotation)) {
                int i = annotation.value().indexOf("|");
                desc = annotation.value();
                if (desc.contains("id")) {
                    continue;
                }
                if (i != -1) {
                    desc = desc.substring(0, i);
                }

            }
            FieldInfo test = new FieldInfo();
            test.setFieldName(item.getName());
            test.setDesc(desc);
            list.add(test);
        }
        resultMap.setList(list);
        return resultMap;
    }


    /**
     * 扫描包获取包下所有类的字节码对象
     *
     * @param pack
     * @return
     */
    private static Set<Class<?>> scanPackageGetClass(String pack) {

        // 第一个class类的集合
        Set<Class<?>> classes = new LinkedHashSet<>();
        // 是否循环迭代
        boolean recursive = true;
        String packageDirName = pack.replace('.', '/');
        // 定义一个枚举的集合 并进行循环来处理这个目录下的things
        Enumeration<URL> dirs;
        try {
            dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
            // 循环迭代下去
            while (dirs.hasMoreElements()) {
                // 获取下一个元素
                URL url = dirs.nextElement();
                // 得到协议的名称
                String protocol = url.getProtocol();
                // 如果是以文件的形式保存在服务器上
                if ("file".equals(protocol)) {
//                    System.err.println("file类型的扫描");
                    // 获取包的物理路径
                    String filePath = URLDecoder.decode(url.getFile(), "UTF-8");
                    // 以文件的方式扫描整个包下的文件 并添加到集合中
                    findAndAddClassesInPackageByFile(pack, filePath,
                            recursive, classes);
                } else if ("jar".equals(protocol)) {
                    // 如果是jar包文件
                    // 定义一个JarFile
//                    System.err.println("jar类型的扫描");
                    JarFile jar;
                    try {
                        // 获取jar
                        jar = ((JarURLConnection) url.openConnection())
                                .getJarFile();
                        // 从此jar包 得到一个枚举类
                        Enumeration<JarEntry> entries = jar.entries();
                        // 同样的进行循环迭代
                        while (entries.hasMoreElements()) {
                            // 获取jar里的一个实体 可以是目录 和一些jar包里的其他文件 如META-INF等文件
                            JarEntry entry = entries.nextElement();
                            String name = entry.getName();
                            // 如果是以/开头的
                            if (name.charAt(0) == '/') {
                                // 获取后面的字符串
                                name = name.substring(1);
                            }
                            // 如果前半部分和定义的包名相同
                            if (name.startsWith(packageDirName)) {
                                int idx = name.lastIndexOf('/');
                                // 如果以"/"结尾 是一个包
                                if (idx != -1) {
                                    // 获取包名 把"/"替换成"."
                                    pack = name.substring(0, idx)
                                            .replace('/', '.');
                                }
                                // 如果可以迭代下去 并且是一个包
                                // 如果是一个.class文件 而且不是目录
                                if (name.endsWith(".class")
                                        && !entry.isDirectory()) {
                                    // 去掉后面的".class" 获取真正的类名
                                    String className = name.substring(pack.length() + 1, name.length() - 6);
                                    try {
                                        // 添加到classes
                                        classes.add(Class
                                                .forName(pack + '.'
                                                        + className));
                                    } catch (ClassNotFoundException e) {
                                        // log
                                        // .error("添加用户自定义视图类错误 找不到此类的.class文件");
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        // log.error("在扫描用户定义视图时从jar包获取文件出错");
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return classes;
    }

    @SuppressWarnings("Convert2streamapi")
    private static void findAndAddClassesInPackageByFile(String packageName,
                                                         String packagePath, final boolean recursive, Set<Class<?>> classes) {
        // 获取此包的目录 建立一个File
        File dir = new File(packagePath);
        // 如果不存在或者 也不是目录就直接返回
        if (!dir.exists() || !dir.isDirectory()) {
            // log.warn("用户定义包名 " + packageName + " 下没有任何文件");
            return;
        }
        // 如果存在 就获取包下的所有文件 包括目录
        File[] dirFiles = dir.listFiles(new FileFilter() {
            // 自定义过滤规则 如果可以循环(包含子目录) 或则是以.class结尾的文件(编译好的java类文件)
            @Override
            public boolean accept(File file) {
                return file.getName().endsWith(".class");
            }
        });
        // 循环所有文件
        for (File file : dirFiles) {
            // 如果是目录 则继续扫描
            if (file.isDirectory()) {
                findAndAddClassesInPackageByFile(packageName + "."
                                + file.getName(), file.getAbsolutePath(), recursive,
                        classes);
            } else {
                // 如果是java类文件 去掉后面的.class 只留下类名
                String className = file.getName().substring(0,
                        file.getName().length() - 6);
                try {
                    // 添加到集合中去
                    //classes.add(Class.forName(packageName + '.' + className));
                    //经过回复同学的提醒，这里用forName有一些不好，会触发static方法，没有使用classLoader的load干净
                    classes.add(Thread.currentThread().getContextClassLoader().loadClass(packageName + '.' + className));
                } catch (ClassNotFoundException e) {
                    // log.error("添加用户自定义视图类错误 找不到此类的.class文件");
                    e.printStackTrace();
                }
            }
        }
    }


}
