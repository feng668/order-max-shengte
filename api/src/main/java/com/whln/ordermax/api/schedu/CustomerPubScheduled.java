package com.whln.ordermax.api.schedu;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.whln.ordermax.data.domain.CuCustomer;
import com.whln.ordermax.data.repository.CuCustomerRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 18:12
 */
@Component
@Slf4j
@AllArgsConstructor
public class CustomerPubScheduled {
    private final CuCustomerRepository cuCustomerRepository;

    @Scheduled(cron = "0 0 0 * * ?")
    public void customerPub() {
        List<String> ids = cuCustomerRepository.queryPubCustomer();
        if (ObjectUtil.isNotEmpty(ids)) {
            cuCustomerRepository.update(new LambdaUpdateWrapper<CuCustomer>()
                    .in(CuCustomer::getId, ids)
                    .set(CuCustomer::getIsPub, 1)
            );
            log.info("客户转入公海：{}", JSONUtil.toJsonStr(ids));
        }
    }
}
