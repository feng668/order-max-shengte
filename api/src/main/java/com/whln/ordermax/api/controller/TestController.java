package com.whln.ordermax.api.controller;

import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.data.domain.SysPower;
import com.whln.ordermax.data.repository.SysPowerRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 单据附件控制器
 */
@Api(tags = "单据附件")
@RestController
@RequestMapping("/openApi")
public class TestController {


    @ApiOperation(value = "查询")
    @PostMapping("/headerTest")
    public void headerTest(HttpServletResponse response) {
        response.setHeader("abcbnm", "asdsd");
    }

    @Resource
    SysPowerRepository sysPowerRepository;
    @Resource
    PackageScanUtil packageScanUtil;
    @Resource
    SysService sysService;
    @PostMapping("/test")
    public void test(@RequestParam("parentId") String parentId,@RequestParam("fieldSource")
            String fieldSource,@RequestParam("entityName")  String entityName) throws ClassNotFoundException {
        List<SysPower> build = packageScanUtil.build(parentId, fieldSource, entityName);
        sysPowerRepository.saveBatch(build);
    }

}