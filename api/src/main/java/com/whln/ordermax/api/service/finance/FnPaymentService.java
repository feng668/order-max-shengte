package com.whln.ordermax.api.service.finance;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.FnPayment;
import com.whln.ordermax.data.repository.FnPaymentRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 付款单业务处理
 */
@Service
public class FnPaymentService {
    @Resource
    private FnPaymentRepository baseRepository;

    /**
     * 查询所有
     */
    public Result listAll(FnPayment entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(FnPayment entity, Page<FnPayment> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 查询单条
     */
    public Result<FnPayment> getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }

    /**
     * 插入或更新
     */
    public Result save(FnPayment entity) {
        FnPayment fnPayment = baseRepository.getOne(new LambdaQueryWrapper<FnPayment>().eq(FnPayment::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(fnPayment)&&!ObjectUtil.equals(entity.getId(),fnPayment.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    public Result delete(String id) {
        FnPayment one = baseRepository.getOne(new LambdaQueryWrapper<FnPayment>().eq(FnPayment::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
        } else {
            baseRepository.update(new LambdaUpdateWrapper<FnPayment>().eq(FnPayment::getId, id).set(FnPayment::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    public Result batchDelete(List<String> ids) {
        List<FnPayment> customsDeclarations = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = customsDeclarations.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = customsDeclarations.stream()
                .map(FnPayment::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                boolean b = baseRepository.removeByIds(ids);
            } else {
                baseRepository.update(new LambdaUpdateWrapper<FnPayment>().in(FnPayment::getId, ids).set(FnPayment::getIsDelete, 1));
            }
        }
        return Result.success();
    }

}