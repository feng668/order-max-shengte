package com.whln.ordermax.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.whln.ordermax.fileOperate.FileTransform;

@Configuration
public class FileOperateConfig {

    @Bean
    public FileTransform getFileTransform(){
        return new FileTransform();
    }
}
