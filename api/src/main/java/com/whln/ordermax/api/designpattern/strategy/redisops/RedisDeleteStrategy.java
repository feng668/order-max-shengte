package com.whln.ordermax.api.designpattern.strategy.redisops;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.whln.ordermax.common.annotaions.Cache;
import com.whln.ordermax.common.enums.RedisOps;
import com.whln.ordermax.common.utils.AopUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;

/**
 * @author liurun
 * @date 2023-04-11 19:43
 */
@Component
public class RedisDeleteStrategy extends RedisStrategy<ProceedingJoinPoint, Cache, Object> {


    @Override
    public Object handle(ProceedingJoinPoint pjp, Cache cache) throws Throwable {
        String key = cache.cacheParam();
        Object id = AopUtil.getArgByName(pjp, key);
        if (ObjectUtil.isEmpty(id) || !(id instanceof Serializable)) {
            return pjp.proceed();
        }
        List<Object> list = redisClient.lRange(key, 0, -1);
        for (Object o : list) {
            Object id1 = ReflectUtil.getFieldValue(o, "id");
            if (ObjectUtil.equals(id1, id)) {
                redisClient.lDelete(key, 1, o);
                break;
            }
        }
        return pjp.proceed();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        factory.register(RedisOps.DELETE, this);
    }
}
