package com.whln.ordermax.api.controller.customer;

import com.whln.ordermax.api.service.customer.CuCustomerFollowRecordService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.param.DeleteParam;
import com.whln.ordermax.data.domain.vo.CuCustomerFollowRecordVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author liurun
 * @date 2023-03-21 13:59
 */
@Api(tags = "客户跟进记录")
@RequestMapping("/CustomerFollowRecord")
@RestController
@AllArgsConstructor
public class CuCustomerFollowRecordController {

    private final CuCustomerFollowRecordService cuCustomerFollowRecordService;

    @ApiOperation("保存")
    @RequiresPermissions("sdfdsfdsf")
    @PostMapping("/save")
    public Result<Void> save(@Validated @RequestBody CuCustomerFollowRecordVO cuCustomerFollowRecordVO) {
        cuCustomerFollowRecordService.save(cuCustomerFollowRecordVO);
        return Result.success();
    }

    @ApiOperation("删除")
    @PostMapping("/delete")
    public Result<Void> delete(@RequestBody DeleteParam param) {
        cuCustomerFollowRecordService.deleteRecord(param);
        return Result.success();
    }
}
