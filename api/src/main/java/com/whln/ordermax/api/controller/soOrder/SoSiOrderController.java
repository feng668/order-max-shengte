package com.whln.ordermax.api.controller.soOrder;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.soOrder.SoSiOrderService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SoSiOrder;
import com.whln.ordermax.data.domain.SoSiOrderCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  销售合同控制器
 */
@Api(tags = "内销合同")
@RestController
@RequestMapping("/SoSiOrder")
public class SoSiOrderController {
    @Resource
    private SoSiOrderService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<SoSiOrder>> listAll(@RequestBody SoSiOrder entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<SoSiOrder>> pageAll(@RequestBody PageEntity<SoSiOrder> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody()@Validated SoSiOrder entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "查询内销合同详情")
    @GetMapping("/getSoSiOrder")
    public Result<SoSiOrder> getSoSiOrder(String id) {
        return Result.success(baseService.getSoSiOrder(id));
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "根据销售合同id查询")
    @PostMapping("/listCom")
    public Result<List<SoSiOrderCommodity>> listCom(@RequestBody SoSiOrderCommodity entity){
        return baseService.listCom(entity);
    }

}