package com.whln.ordermax.api.designpattern.signhandler;

import com.whln.ordermax.data.domain.EmInbox;
import com.whln.ordermax.data.domain.param.EmailFlagParam;

/**
 * 处理邮件标记的责任链父接口
 * @author liurun
 */
public interface EmailHandler {
    /**
     * 处理邮件标记等
     * @param emInbox
     * @param param
     * @return
     */
    EmInbox handler(EmInbox emInbox, EmailFlagParam param);

}
