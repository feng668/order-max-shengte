package com.whln.ordermax.api.service.customer;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.data.domain.CuCustomerFollowRecord;
import com.whln.ordermax.data.domain.param.DeleteParam;
import com.whln.ordermax.data.domain.vo.CuCustomerFollowRecordVO;
import com.whln.ordermax.data.repository.CuCustomerFollowRecordRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author liurun
 * @date 2023-03-21 13:50
 */
@AllArgsConstructor
@Service
public class CuCustomerFollowRecordService {
    private final CuCustomerFollowRecordRepository cuCustomerFollowRecordRepository;

    public void save(CuCustomerFollowRecordVO cuCustomerFollowRecordVO) {
        CuCustomerFollowRecord cuCustomerFollowRecord = BeanUtil.copyProperties(cuCustomerFollowRecordVO, CuCustomerFollowRecord.class);
        cuCustomerFollowRecordRepository.saveOrUpdate(cuCustomerFollowRecord);
    }

    public void deleteRecord(DeleteParam param) {
        if (ObjectUtil.isNotEmpty(param.getIds())) {
            return;
        }
        cuCustomerFollowRecordRepository.removeByIds(param.getIds());

    }
}
