package com.whln.ordermax.api.service.commodity;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CmCommodityImg;
import com.whln.ordermax.data.repository.CmCommodityImgRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *  商品图片业务处理
 */
@Service
public class CmCommodityImgService{

    @Resource
    private CmCommodityImgRepository baseRepository;

    /**
    *  查询所有
    */
    public Result getClassifyByCommodityId(CmCommodityImg entity) {
        if (StrUtil.isEmpty(entity.getCommodityId())){
            return Result.error("202","缺少商品id");
        }
        Map<String, List<CmCommodityImg>> imgTypeMap = baseRepository.listByEntity(entity).stream().collect(Collectors.groupingBy(CmCommodityImg::getImgType));
        return Result.success(imgTypeMap);
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(CmCommodityImg entity, Page<CmCommodityImg> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(CmCommodityImg entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }


}