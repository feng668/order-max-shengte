package com.whln.ordermax.api.designpattern.chain.impl.ordrplan;

import com.whln.ordermax.api.designpattern.chain.HandlerChain;

/**
 * @author liurun
 * @date 2023-02-24 17:05
 */
public abstract class OrderPlanHandlerChain<T,R> implements HandlerChain<T, R> {
}
