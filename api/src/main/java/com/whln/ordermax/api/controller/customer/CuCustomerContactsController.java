package com.whln.ordermax.api.controller.customer;

import com.whln.ordermax.api.service.customer.CuCustomerContactsService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CuCustomerContacts;
import com.whln.ordermax.data.domain.vo.CuCustomerContactsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  客户联系人控制器
 */
@Api(tags = "客户联系人")
@RestController
@RequestMapping("/CuCustomerContacts")
public class CuCustomerContactsController{

    @Resource
    private CuCustomerContactsService baseService;


    @ApiOperation(value = "通过客户id查询客户联系人")
    @PostMapping("/listByCustomerId")
    public Result<List<CuCustomerContactsVO>> listByCustomerId(@RequestBody CuCustomerContacts entity){
        return baseService.listByCustomerId(entity);
    }

}