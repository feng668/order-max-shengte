package com.whln.ordermax.api.service.enquiry;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.entity.numberRole.NumReqEntity;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.repository.EoEnquiryCommodityRepository;
import com.whln.ordermax.data.repository.EoEnquiryOrderRepository;
import com.whln.ordermax.data.repository.EoEnquiryPlansRepository;
import com.whln.ordermax.data.repository.NrBillRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 询价单业务处理
 */
@Service
public class EoEnquiryOrderService {
    @Resource
    private EoEnquiryOrderRepository baseRepository;

    @Resource
    private EoEnquiryCommodityRepository commodityRepository;

    @Resource
    private EoEnquiryPlansRepository plansRepository;
    @Resource
    private SysService sysService;
    @Resource
    private NrBillRepository nrBillRepository;

    /**
     * 查询所有
     */
    public Result listAll(EoEnquiryOrder entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(EoEnquiryOrder entity, Page<EoEnquiryOrder> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    public Result save(EoEnquiryOrder entity) {
        EoEnquiryOrder enquiryOrder = baseRepository.getOne(new LambdaQueryWrapper<EoEnquiryOrder>().eq(EoEnquiryOrder::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(enquiryOrder)&&!ObjectUtil.equals(entity.getId(),enquiryOrder.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<EoEnquiryCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream().map(EoEnquiryCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<EoEnquiryCommodity>()
                .eq(EoEnquiryCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(idList), EoEnquiryCommodity::getId, idList));
        if (CollectionUtil.isNotEmpty(commodityList)) {
            List<EoEnquiryCommodity> collect = commodityList.stream().peek(item -> {
                item.setBillId(entity.getId());
            }).collect(Collectors.toList());
            commodityRepository.saveOrUpdateBatch(collect);
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        EoEnquiryOrder one = baseRepository.getOne(new LambdaQueryWrapper<EoEnquiryOrder>().eq(EoEnquiryOrder::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            boolean b = baseRepository.removeById(id);
            commodityRepository.remove(new QueryWrapper<EoEnquiryCommodity>().eq("bill_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<EoEnquiryOrder>().eq(EoEnquiryOrder::getId, id).set(EoEnquiryOrder::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> batchDelete(List<String> ids) {
        List<EoEnquiryOrder> enquiryOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = enquiryOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = enquiryOrders.stream()
                .map(EoEnquiryOrder::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                commodityRepository.remove(new QueryWrapper<EoEnquiryCommodity>().in("bill_id", ids));
                baseRepository.removeByIds(ids);
            } else {
                baseRepository.update(new LambdaUpdateWrapper<EoEnquiryOrder>().in(EoEnquiryOrder::getId, ids).set(EoEnquiryOrder::getIsDelete, 1));
            }
        }

        return Result.success();
    }

    public Result<List<EoEnquiryCommodity>> listCom(EoEnquiryCommodity entity) {
        return Result.success(commodityRepository.listByEntity(entity));
    }

    public Result updatePlan(EoEnquiryOrder entity) {
        if (!ObjectUtil.isAllNotEmpty(entity.getId(), entity.getPlan())) {
            throw new BusinessException("缺少必要参数");
        }
        boolean b = baseRepository.update(new UpdateWrapper<EoEnquiryOrder>().set("plan", entity.getPlan()).eq("id", entity.getId()));
        return b ? Result.success() : Result.error("201", "操作失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result addPlan(EoEnquiryPlans entity) {
        if (StrUtil.isEmpty(entity.getEnquiryId())) {
            Result.error("201", "缺少询价单id");
        }
        if (ObjectUtil.isEmpty(entity.getPlan())) {
            return Result.error("201", "缺少进度状态");
        }
        EoEnquiryPlans maxSortPlan = plansRepository.getOne(
                new QueryWrapper<EoEnquiryPlans>()
                        .select("MAX(sort) AS sort")
                        .eq("enquiry_id", entity.getEnquiryId())
        );
        if (ObjectUtil.isNotEmpty(maxSortPlan)) {
            entity.setSort(maxSortPlan.getSort() + 1);
        }
        boolean b = plansRepository.save(entity);
        baseRepository.update(
                new UpdateWrapper<EoEnquiryOrder>()
                        .set("plan", entity.getPlan())
                        .eq("id", entity.getEnquiryId())
        );
        return b ? Result.success() : Result.error();
    }

    public Result<Page<Map<String, Object>>> pageAllPlan(EoEnquiryOrder entity, Page<EoEnquiryOrder> page) {
        Page<EoEnquiryOrder> idPage = baseRepository.page(page, new QueryWrapper<>());
        List<String> idList = idPage.getRecords().stream().map(EoEnquiryOrder::getId).collect(Collectors.toList());
        Map<String, List<EoEnquiryPlans>> plansListMap = plansRepository.listByEntity(
                new EoEnquiryPlans() {{
                    setEnquiryIdIn(idList);
                }}
        ).stream().collect(Collectors.groupingBy(EoEnquiryPlans::getEnquiryId));
        List<HashMap<String, Object>> result = idList.stream().map(i -> {
            return new HashMap<String, Object>() {{
                put("enquiryId", i);
                put("planList", plansListMap.get(i));
            }};
        }).collect(Collectors.toList());
        Page<Map<String, Object>> resultPage = new Page<>();
        BeanUtil.copyProperties(idPage, resultPage, "record");
        return Result.success(resultPage);
    }


    public void batchSave(List<EoEnquiryCommodity> entity) {
        EoEnquiryOrder eoEnquiryOrder = new EoEnquiryOrder();
        NrBill nrBill = nrBillRepository.getOne(new LambdaQueryWrapper<NrBill>().eq(NrBill::getBillCode, "EO_ENQUIRY_ORDER"));
        Map<String, Object> map = BeanUtil.beanToMap(eoEnquiryOrder);
        NumReqEntity n = new NumReqEntity();
        n.setAttrData(map);
        n.setCreatNum(1);
        n.setNrBillId(nrBill.getId());
        Result<String> stringResult = sysService.nextBillNo(n);
        eoEnquiryOrder.setBillNo(stringResult.getData());
        eoEnquiryOrder.setId(sysService.nextId());
        List<EoEnquiryCommodity> collect = entity.stream().peek(item -> {
            item.setBillId(eoEnquiryOrder.getId());
        }).collect(Collectors.toList());
        baseRepository.saveOrUpdate(eoEnquiryOrder);
        if (CollectionUtil.isNotEmpty(collect)) {
            commodityRepository.saveOrUpdateBatch(collect);
        }
    }
}