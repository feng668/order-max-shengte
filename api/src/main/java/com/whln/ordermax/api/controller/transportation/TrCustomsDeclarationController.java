package com.whln.ordermax.api.controller.transportation;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.transportation.TrCustomsDeclarationService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.TrCustomsDeclaration;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  报关单证控制器
 */
@Api(tags = "报关单证")
@RestController
@RequestMapping("/TrCustomsDeclaration")
public class TrCustomsDeclarationController{

    @Resource
    private TrCustomsDeclarationService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<TrCustomsDeclaration>> listAll(@RequestBody TrCustomsDeclaration entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<TrCustomsDeclaration>> pageAll(@RequestBody PageEntity<TrCustomsDeclaration> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody()@Validated TrCustomsDeclaration entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}