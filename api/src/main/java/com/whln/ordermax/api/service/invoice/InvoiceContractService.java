package com.whln.ordermax.api.service.invoice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.whln.ordermax.data.domain.InvoiceContract;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liurun
 * @since 2023-04-23
 */
public interface InvoiceContractService extends IService<InvoiceContract> {

}
