package com.whln.ordermax.api.controller.finance;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.finance.FnOffsetService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.FnOffset;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  收款核销控制器
 */
@Api(tags = "收款核销")
@RestController
@RequestMapping("/fnOffset")
public class FnOffsetController{

    @Resource
    private FnOffsetService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<FnOffset>> listAll(@RequestBody FnOffset entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<FnOffset>> pageAll(@RequestBody PageEntity<FnOffset> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() FnOffset entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody List<String> ids){
        return baseService.batchDelete(ids);
    }

}