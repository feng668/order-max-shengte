package com.whln.ordermax.api.controller.customer;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.customer.CuCustomerService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CuCustomer;
import com.whln.ordermax.data.domain.vo.CuCustomerVO;
import com.whln.ordermax.data.domain.vo.EmailHistoryVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 客户控制器
 */
@Api(tags = "客户")
@RestController
@RequestMapping("/CuCustomer")
public class CuCustomerController {

    @Resource
    private CuCustomerService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<CuCustomer>> listAll(@RequestBody CuCustomer entity) {
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<IPage<CuCustomer>> pageAll(@RequestBody PageEntity<CuCustomer> pageEntity) {
        return baseService.pageAll(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    //todo 抽空去掉
    @ApiOperation(value = "分页查询客户公海")
    @PostMapping("/pagePub")
    public Result<IPage<CuCustomer>> pagePub(@RequestBody PageEntity<CuCustomer> pageEntity) {
        return baseService.pagePub(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "客户转移公海")
    @PostMapping("/transferPub")
    public Result<Page<CuCustomer>> transferPub(@RequestParam("customerId") String customerId) {
        return baseService.transferPub(customerId);
    }

    @ApiOperation(value = "认领客户")
    @PostMapping("/claim")
    public Result<Page<CuCustomer>> claim(@RequestParam("customerId") String customerId) {
        return baseService.claim(customerId);
    }


    @ApiOperation(value = "指定客户共享人")
    @PostMapping("/saveSharer")
    public Result<Void> saveSharer(@RequestBody() JSONObject entity) {
        return baseService.saveSharer(entity);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody() @Validated CuCustomer entity) {
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id") String id) {
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result<Void> batchDelete(@RequestBody List<String> ids) {
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "查询跟进记录")
    @PostMapping("/listFollow")
    public Result<List<Map<String, Object>>> listFollow(@RequestParam("id") String id) {
        return baseService.listFollow(id);
    }

    @ApiOperation(value = "查询商品记录")
    @PostMapping("/listCommodityByCustomerId")
    public Result<List<Map>> listCommodityByCustomerId(@RequestParam("id") String id, @RequestParam("searchInfo") String searchInfo, @RequestParam("billCode") String billCode) {
        return baseService.listCommodityByCustomerId(id, searchInfo, billCode);
    }


    @ApiOperation(value = "批量导入客户")
    @PostMapping("/batchImport")
    public Result<List<CuCustomerVO>> batchImport(@RequestBody List<CuCustomerVO> cuCustomerVOList) {
        return Result.success(baseService.batchImport(cuCustomerVOList));
    }

    @ApiOperation(value = "检查重复的客户编号")
    @PostMapping("/checkRepeat")
    public Result<List<CuCustomerVO>> checkRepeat(@RequestBody List<CuCustomerVO> cuCustomerVOList) {
        List<CuCustomerVO> cmCommodities = baseService.checkRepeat(cuCustomerVOList);
        return Result.success(cmCommodities);
    }

    @ApiOperation("邮件来往")
    @GetMapping("/emailHistory")
    public Result<List<EmailHistoryVO>> emailHistory(String costomerId){
        List<EmailHistoryVO> emailHistoryVOList = baseService.emailHistory(costomerId);
        return Result.success(emailHistoryVOList);
    }
}