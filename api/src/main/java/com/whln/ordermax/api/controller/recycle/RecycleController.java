package com.whln.ordermax.api.controller.recycle;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.RecycleModel;
import com.whln.ordermax.data.domain.param.RecycleDeleteParam;
import com.whln.ordermax.data.domain.param.RecycleParam;
import com.whln.ordermax.data.domain.vo.RecycleModelVO;
import com.whln.ordermax.data.service.MapCache;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-03-25 16:36
 */
@Slf4j
@Api(tags = "回收站")
@RequestMapping("/recycle")
@RestController
public class RecycleController {
    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private MapCache mapCache;

    @ApiOperation("回收站列表")
    @PostMapping("/page")
    public Result<IPage<?>> page(@RequestBody RecycleParam param) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        RecycleModel modelMessage = RecycleModel.getByCode(param.getCode());
        String repositoryClass = modelMessage.getRepositoryClass();
        Object bean = applicationContext.getBean(Class.forName(repositoryClass));
        if (ObjectUtil.isNotEmpty(bean)) {
            String className = modelMessage.getEntityClass();
            Class<?> entityClass = Class.forName(className);
            Object o = entityClass.newInstance();
            ReflectUtil.setFieldValue(o, "isDelete", 1);
            ReflectUtil.setFieldValue(o, "searchInfo", param.getSearchInfo());
            return Result.success(ReflectUtil.invoke(bean, "pageByEntity", o, new Page<>(param.getCurrent(), param.getSize())));
        }
        return Result.success();
    }

    @ApiOperation("模块")
    @GetMapping("/codeList")
    public Result<List<RecycleModelVO>> getCodeList() {
        List<RecycleModelVO> collect = Arrays.stream(RecycleModel.values())
                .map(item -> new RecycleModelVO(item.getCode(), item.getName()))
                .collect(Collectors.toList());
        return Result.success(collect);
    }



    @ApiOperation("移出回收站")
    @PostMapping("/recovery")
    public Result<Void> recovery(@RequestBody RecycleParam param) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        RecycleModel modelMessage = RecycleModel.getByCode(param.getCode());
        String repositoryClass = modelMessage.getRepositoryClass();
        Object bean = applicationContext.getBean(Class.forName(repositoryClass));
        if (ObjectUtil.isNotEmpty(bean)) {
            String className = modelMessage.getEntityClass();
            Class<?> entityClass = Class.forName(className);
            Object o = entityClass.newInstance();
            ReflectUtil.setFieldValue(o, "id", param.getId());
            ReflectUtil.setFieldValue(o, "isDelete", 0);
            return Result.success(ReflectUtil.invoke(bean, "updateById", o));
        }
        return Result.success();
    }

    @Transactional(rollbackFor = Exception.class)
    @ApiOperation("删除")
    @PostMapping("/remove")
    public Result<Void> remove(@RequestBody RecycleParam param) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        try {
            ShiroManager.checkPermission("RECYCLE_DELETE");
        } catch (Exception e) {
            log.warn("没有回收站删除权限:{}", "RECYCLE_DELETE");
            return Result.error("没有删除权限");
        }
        Map<String, String> repositoryMap = mapCache.codeFullCommodityRepositoryMap();
        Set<String> billIdList = param.getRecycleDeleteParams().stream().map(RecycleDeleteParam::getId).collect(Collectors.toSet());
        String commodityRepositoryClass = repositoryMap.get(param.getCode());
        if (ObjectUtil.isNotEmpty(commodityRepositoryClass)) {
            Object bean = applicationContext.getBean(Class.forName(commodityRepositoryClass));
            UpdateWrapper<Object> objectUpdateWrapper = new UpdateWrapper<>();
            objectUpdateWrapper.in("bill_id", billIdList);
            ReflectUtil.invoke(bean, "remove", objectUpdateWrapper);
        }

        if (ObjectUtil.isNotEmpty(billIdList)) {
            RecycleModel modelMessage = RecycleModel.getByCode(param.getCode());
            String repositoryClass = modelMessage.getRepositoryClass();
            Object bean = applicationContext.getBean(Class.forName(repositoryClass));
            if (ObjectUtil.isNotEmpty(bean)) {
                UpdateWrapper<Object> objectUpdateWrapper1 = new UpdateWrapper<>();
                objectUpdateWrapper1.in("id", billIdList);
                ReflectUtil.invoke(bean, "remove", objectUpdateWrapper1);
            }
        }
        return Result.success();
    }
}
