package com.whln.ordermax.api.controller.oa;

import com.whln.ordermax.api.service.oa.OaRecordService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.OaRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  oa单据审核记录控制器
 */
@Api(tags = "oa单据审核记录")
@RestController
@RequestMapping("/OaRecord")
public class OaRecordController{

    @Resource
    private OaRecordService baseService;

    @ApiOperation(value = "通过单据id查询")
    @PostMapping("/list")
    public Result<List<OaRecord>> listAll(@RequestBody OaRecord entity){
        return baseService.listAll(entity);
    }

}