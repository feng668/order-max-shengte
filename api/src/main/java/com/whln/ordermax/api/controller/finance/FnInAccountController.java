package com.whln.ordermax.api.controller.finance;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.finance.FnInAccountService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.FnInAccount;
import com.whln.ordermax.data.domain.FnOffsetBill;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  入帐登记控制器
 */
@Api(tags = "入帐登记")
@RestController
@RequestMapping("/fnInAccount")
public class FnInAccountController{

    @Resource
    private FnInAccountService baseService;

    @ApiOperation(value = "查询已核销出运合同")
    @PostMapping("/listOffsetBillByInAccountId")
    public Result<List<FnOffsetBill>> listOffsetBillByInAccountId(@RequestParam("id") String id){
        return baseService.listOffsetBillByInAccountId(id);
    }

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<FnInAccount>> listAll(@RequestBody FnInAccount entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<FnInAccount>> pageAll(@RequestBody PageEntity<FnInAccount> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() FnInAccount entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}