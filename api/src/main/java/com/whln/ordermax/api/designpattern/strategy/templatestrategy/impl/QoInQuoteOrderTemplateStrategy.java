package com.whln.ordermax.api.designpattern.strategy.templatestrategy.impl;

import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategy;
import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategyFactory;
import com.whln.ordermax.common.enums.ModuleEnum;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.QoInQuoteOrder;
import com.whln.ordermax.data.domain.TmPrintTemplate;
import com.whln.ordermax.data.domain.param.RenderTemplateParam;
import com.whln.ordermax.data.repository.QoInQuoteOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author liurun
 * @date 2023-02-21 9:38
 */
@Component
@AllArgsConstructor
public class QoInQuoteOrderTemplateStrategy extends TemplateStrategy<RenderTemplateParam, TmPrintTemplate,String>{

    private final QoInQuoteOrderRepository repository;

    @Override
    public void afterPropertiesSet() throws Exception {
        TemplateStrategyFactory.register(ModuleEnum.QO_IN_QUOTE_ORDER.getValue(), this);
    }
    @Override
    public String handle(RenderTemplateParam param, TmPrintTemplate templateData) {
        try {
            String content = getTemplateContent(templateData.getHtmlPath());
            String template = transitionHtml2(content);
            QoInQuoteOrder qoQuoteOrder = repository.getAllById(param.getId());
            return render(qoQuoteOrder, template);
        } catch (IOException e) {
            throw new BusinessException("读取模板出错了");
        }
    }


}
