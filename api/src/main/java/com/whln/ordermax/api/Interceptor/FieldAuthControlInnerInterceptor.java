package com.whln.ordermax.api.Interceptor;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.PluginUtils;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.whln.ordermax.data.service.MapCache;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.data.repository.SysPowerRepository;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.subject.Subject;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.Map;

/**
 * 属性控制拦截
 */
public class FieldAuthControlInnerInterceptor implements InnerInterceptor {

    private ApplicationContext applicationContext;

    public FieldAuthControlInnerInterceptor(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }


    @Override
    public void beforeQuery(Executor executor, MappedStatement mappedStatement, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
        String mapperId = mappedStatement.getId();
        MapCache mapService = applicationContext.getBean(MapCache.class);
        Map<String, Map<String, String>> mapperModelMap;
        int lastIndex = mapperId.lastIndexOf(".");
        String mapperIpath = mapperId.substring(0, lastIndex);
        String methodName = mapperId.substring(lastIndex + 1);
        if (ObjectUtil.equals(methodName, "listByEntity") ||ObjectUtil.equals("pageTotalByStageType",methodName)||methodName.startsWith("get")|| ObjectUtil.equals(methodName, "page")) {
            //获取所属模块
            mapperModelMap = mapService.mapperModelMap1();
        } else if (ObjectUtil.equals(methodName, "commodityList") || methodName.lastIndexOf("List") != -1) {
            mapperModelMap = mapService.commodityModelMap();
        } else {
            return;
        }
        Map<String, String> mapperModel = mapperModelMap.get(mapperIpath);
        if (ObjectUtil.isEmpty(mapperModel)) {
            mapperModel=mapperModelMap.get(mapperIpath + "." + methodName);
        }
        if (ObjectUtil.isNotEmpty(mapperModel)) {
            //获取当前用户可访问的属性
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            HttpServletRequest request = attributes.getRequest();
            if (ShiroManager.getSubject().hasRole("webadmin")) {
                return;
            }
            SysPowerRepository sysPowerRepository = applicationContext.getBean(SysPowerRepository.class);
            String modelCode = mapperModel.get("modelCode");
            String fieldSource = mapperModel.get("fieldSource");
            String queryField = sysPowerRepository.getModelQueryFieldByUserId(ShiroManager.getUserId(), modelCode, fieldSource);
            // 改变sql语句
            PluginUtils.MPBoundSql mpBs = PluginUtils.mpBoundSql(boundSql);
            String oldSql = mpBs.sql();
            if (ObjectUtil.isNotEmpty(queryField)) {
                String replace = oldSql.replaceFirst("\\*", StrUtil.toUnderlineCase(queryField));
                String s = oldSql.replaceFirst("select \\*", StrUtil.toUnderlineCase(queryField));
                mpBs.sql(replace);
            }
        }

    }

    private void buildSql(BoundSql boundSql, Map<String, String> mapperModel) {
        if (ObjectUtil.isNotEmpty(mapperModel)) {
            //获取当前用户可访问的属性
            Subject subject = ShiroManager.getSubject();
            if (subject.hasRole("webadmin")) {
                return;
            }
            SysPowerRepository sysPowerRepository = applicationContext.getBean(SysPowerRepository.class);
            String modelCode = mapperModel.get("modelCode");
            String fieldSource = mapperModel.get("fieldSource");
            String queryField = sysPowerRepository.getModelQueryFieldByUserId(ShiroManager.getUserId(), modelCode, fieldSource);
            // 改变sql语句
            PluginUtils.MPBoundSql mpBs = PluginUtils.mpBoundSql(boundSql);
            String oldSql = mpBs.sql();
            if (ObjectUtil.isNotEmpty(queryField)) {
                String replace = oldSql.replaceFirst("\\*", StrUtil.toUnderlineCase(queryField));
                String s = oldSql.replaceFirst("select \\*", StrUtil.toUnderlineCase(queryField));
                mpBs.sql(replace);
            }
        }
    }

}
