package com.whln.ordermax.api.controller.sys;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SysDictDetail;
import com.whln.ordermax.data.repository.SysDictDetailRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author liurun
 * @date 2023-04-21 20:22
 */
@Api(tags = "字典详情")
@RestController
@RequestMapping("/sysDictDetail")
public class SysDictDetailController {
    @Resource
    SysDictDetailRepository sysDictDetailRepository;


    @ApiOperation("列表")
    @GetMapping("listSysDictDetail")
    public Result<List<SysDictDetail>> listSysDict() {
        List<SysDictDetail> sysDicts = sysDictDetailRepository.list();
        return Result.success(sysDicts);
    }

    @ApiOperation("删除")
    @PostMapping("/delete")
    public Result<Void> delete(String id) {
        sysDictDetailRepository.removeById(id);
        return Result.success();
    }

    @ApiOperation("更新")
    @PostMapping("/update")
    public Result<Void> update(@RequestBody SysDictDetail sysDict) {
        sysDictDetailRepository.updateById(sysDict);
        return Result.success();
    }
    @ApiOperation("保存")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody SysDictDetail sysDict) {
        sysDictDetailRepository.save(sysDict);
        return Result.success();
    }
}
