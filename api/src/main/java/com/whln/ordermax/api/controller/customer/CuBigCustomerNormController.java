package com.whln.ordermax.api.controller.customer;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.customer.CuBigCustomerNormService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CuBigCustomerNorm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  大客户指标控制器
 */
@Api(tags = "大客户指标")
@RestController
@RequestMapping("/cuBigCustomerNorm")
public class CuBigCustomerNormController{

    @Resource
    private CuBigCustomerNormService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<CuBigCustomerNorm>> listAll(@RequestBody CuBigCustomerNorm entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<CuBigCustomerNorm>> pageAll(@RequestBody PageEntity<CuBigCustomerNorm> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<CuBigCustomerNorm> getAllById(@RequestParam("id") String id){
        return baseService.getAllById(id);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() CuBigCustomerNorm entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}