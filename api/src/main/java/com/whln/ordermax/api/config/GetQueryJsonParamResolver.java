package com.whln.ordermax.api.config;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.ValueConstants;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import com.whln.ordermax.common.annotaions.JsonParam;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;

/**
 * @author liurun
 * @date 2023-02-27 11:04
 */
public class GetQueryJsonParamResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        Annotation[] annotations = parameter.getParameterAnnotations();
        for (int i = 0; i < annotations.length; i++) {
            Annotation item = annotations[i];
            if (item.annotationType().equals(JsonParam.class)){
                return true;
            }
        }
        return false;


    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        HttpServletRequest request = webRequest.getNativeRequest(HttpServletRequest.class);
        String parameterName = parameter.getParameterName();
        Annotation[] annotations = parameter.getParameterAnnotations();
        String params = request.getParameter(parameterName);
        if (ObjectUtil.isEmpty(params)){
            for (Annotation item : annotations) {
                if (item instanceof JsonParam) {
                    JsonParam param = (JsonParam) item;
                    params = request.getParameter(param.value());
                    if (ObjectUtil.isEmpty(params)) {
                        params = param.defaultValue();
                    }
                    if (ObjectUtil.isEmpty(params) && param.required() || params.equals(ValueConstants.DEFAULT_NONE)) {
                        throw new RuntimeException(parameterName + ":不能为空");
                    }
                }
            }
        }
        Class<?> parameterType = parameter.getParameterType();
        Object bean = JSONUtil.toBean(params, parameterType);
        return bean;
    }
}