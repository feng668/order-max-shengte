package com.whln.ordermax.api.designpattern.signhandler;

import cn.hutool.core.util.ObjectUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.data.domain.EmInbox;
import com.whln.ordermax.data.domain.param.EmailFlagParam;

/**
 * @author liurun
 */
@Component
public class FlagHandler implements InitializingBean,EmailHandler {
    @Override
    public void afterPropertiesSet() throws Exception {
        SignHandlerFactory.register(this);
    }

    @Override
    public EmInbox handler(EmInbox inboxOne, EmailFlagParam param) {
        if (ObjectUtil.isNotEmpty(param.getIsFlag())&&ObjectUtil.isNotEmpty(inboxOne)) {
            if (inboxOne.getIsFlag() == 0) {
                inboxOne.setIsFlag(YesOrNo.YES.getState());
            } else {
                inboxOne.setIsFlag(YesOrNo.NO.getState());
            }
        }
        return inboxOne;
    }
}
