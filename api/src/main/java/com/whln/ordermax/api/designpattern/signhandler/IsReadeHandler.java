package com.whln.ordermax.api.designpattern.signhandler;

import cn.hutool.core.util.ObjectUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.data.domain.EmInbox;
import com.whln.ordermax.data.domain.param.EmailFlagParam;

/**
 * @author liurun
 */
@Component
public class IsReadeHandler implements InitializingBean,EmailHandler {
    @Override
    public void afterPropertiesSet() throws Exception {
        SignHandlerFactory.register(this);
    }

    @Override
    public EmInbox handler(EmInbox inboxOne, EmailFlagParam param) {
        if (ObjectUtil.isNotEmpty(param.getReadeStatus())&&ObjectUtil.isNotEmpty(inboxOne)) {
            if (ObjectUtil.equals(inboxOne.getIsRead(),0)) {
                inboxOne.setIsRead(YesOrNo.YES.getState());
            } else {
                inboxOne.setIsRead(YesOrNo.NO.getState());
            }
        }
        return inboxOne;
    }
}
