package com.whln.ordermax.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    /**
     * 创建API应用
     * apiInfo() 增加API相关信息
     * 通过select()函数返回一个ApiSelectorBuilder实例,用来控制哪些接口暴露给Swagger来展现，
     * 指定扫描的包路径来定义指定要建立API的目录。
     *
     * @return
     */
    @Bean
    public Docket coreApiConfig() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(adminApiInfo())
//                .groupName("Order-Max订单管理系统--api文档")
                .select()
                //只显示admin下面的路径
//                .paths(Predicates.and(PathSelectors.regex("/admin/.*")))
                .build();
    }

    private ApiInfo adminApiInfo() {
        return new ApiInfoBuilder()
                .title("Order-Max订单管理系统--api文档")
                .description(
                        "上传文件公共路径：/api/fileStore/static/\n" +
                                "打印模板公共路径：/api/fileStore/printTemplate/\n" +
                                "邮件公共路径：/api/fileStore/emailFile/\n"
                )
                .version("1.0")
                .contact(new Contact("Li Shengwen", "http://1.117.88.252/", "lsw821626239@126.com"))
                .build();
    }
}