package com.whln.ordermax.api.init;

import com.whln.ordermax.common.redis.RedisClient;
import org.springframework.context.annotation.Lazy;

import javax.annotation.Resource;

/**
 * @author liurun
 * @date 2023-04-21 17:30
 */
public abstract class InitData<T> {
    @Resource
    protected InitFactory<T> initFactory;
    @Lazy
    @Resource
   protected RedisClient<String, T> redisClient;
    abstract void init();

}
