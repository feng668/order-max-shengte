package com.whln.ordermax.api.controller.template;

import com.whln.ordermax.api.service.template.PrintTemplateService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.EmInboxAttachment;
import com.whln.ordermax.data.domain.param.RenderTemplateParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;


@Api(tags = "打印模板渲染")
@RestController
@RequestMapping("/printTemplate")
public class PrintTemplateController {

    @Resource
    private PrintTemplateService printTemplateService;

    @ApiOperation(value = "获取模板内容")
    @GetMapping("/getTemplate/{module}/{fileName}")
    public void getTemplate(@PathVariable("module") String module,@PathVariable("fileName") String fileName
            ,HttpServletResponse response) throws IOException {
        printTemplateService.getTemplate(module, fileName, response);
    }

    @ApiOperation(value = "渲染模板内容")
    @PostMapping("/renderTemplate")
    public void renderTemplate(@RequestBody RenderTemplateParam param, HttpServletResponse response) throws IOException {
        printTemplateService.renderTemplate(param, response);
    }
    @ApiOperation(value = "显示报价单表格")
    @GetMapping(value = "/showQueteExcel")
    public void showQueteExcel(@RequestParam("queteId") String queteId, HttpServletResponse response) throws Exception {
        printTemplateService.showQueteExcel(queteId, response);
    }

    @ApiOperation(value = "下载报价单表格")
    @GetMapping(value = "/downloadQueteExcel",name = "下载报价单表格")
    public void downloadQueteExcel(@RequestParam("queteId")String queteId, HttpServletResponse response) throws Exception {
        printTemplateService.downloadQueteExcel(queteId, response);
    }

    @ApiOperation(value = "按模块显示模板数据")
    @PostMapping(value = "/showExcel")
    public void showExcel(@RequestBody() Map body, HttpServletResponse response) throws Exception {
        printTemplateService.showExcel(body, response);
    }

    @ApiOperation(value = "显示报价单表格v2")
    @GetMapping(value = "/showQueteExcel2")
    public void showQueteExcel2(@RequestParam("queteId")String queteId, HttpServletResponse response) throws Exception {
        printTemplateService.showQueteExcel2(queteId, response);
    }

    @ApiOperation(value = "按模块显示模板数据v2")
    @PostMapping(value = "/preview")
    public void preview(@RequestBody() Map body, HttpServletResponse response) throws Exception {
        printTemplateService.preview(body, response);
    }

    @ApiOperation(value = "二维码")
    @PostMapping(value = "/previewQRCode")
    public void previewQRCode(@RequestBody() Map body, HttpServletResponse response) throws Exception {
        printTemplateService.previewQRCode(body, response);
    }


    @ApiOperation(value = "按文件类型下载文件/v2")
    @PostMapping(value = "/downloadWord/byTemplateId")
    public void downloadByTempId(@RequestBody() Map body, HttpServletResponse response) throws Exception {
        printTemplateService.downloadByTempId(body, response);
    }

    @ApiOperation(value = "按文件类型保存文件")
    @PostMapping(value = "/saveFileByTempId/byTemplateId")
    public Result<EmInboxAttachment> saveFileByTempId(@RequestBody() Map body, HttpServletResponse response) throws Exception {
       return printTemplateService.saveFileByTempId(body, response);
    }
    @ApiOperation(value = "通过文件转换为html")
    @PostMapping(value = "/htmlTemlateFromFile")
    public Result htmlTemlateFromFile(@RequestParam("file") MultipartFile file, HttpServletResponse response) throws Exception {
        return printTemplateService.htmlTemlateFromFile(file,response);
    }


}
