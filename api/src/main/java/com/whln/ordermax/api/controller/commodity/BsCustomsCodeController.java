package com.whln.ordermax.api.controller.commodity;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.commodity.BsCustomsCodeService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.BsCustomsCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 *  报关代码
 */
@Api(tags = "报关代码")
@RestController
@RequestMapping("/BsCustomsCode")
public class BsCustomsCodeController{

    private IdentifierGenerator identifierGenerator = new DefaultIdentifierGenerator();

    @Resource
    private BsCustomsCodeService baseService;

    /**
     * 查询
     * @param entity
     * @return
     */
    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<BsCustomsCode>> listAll(@RequestBody BsCustomsCode entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<BsCustomsCode>> pageAll(@RequestBody PageEntity<BsCustomsCode> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "批量导入")
    @PostMapping("/batchSave")
    public Result pageAll(@RequestBody List<BsCustomsCode> bsCustomsCodeList){
        bsCustomsCodeList = bsCustomsCodeList.stream().map(c -> {
            c.setId(identifierGenerator.nextId(new Object()).toString());
            return c;
        }).collect(Collectors.toList());
        return baseService.batchSave(bsCustomsCodeList);
    }
}