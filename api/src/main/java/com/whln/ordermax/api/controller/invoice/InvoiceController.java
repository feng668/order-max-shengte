package com.whln.ordermax.api.controller.invoice;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.invoice.InvoiceService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.Invoice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liurun
 * @since 2023-04-23
 */
@Api(tags = "发票管理")
@RestController
@RequestMapping("/invoice")
public class InvoiceController {
    @Autowired
    InvoiceService baseService;

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<Invoice>> pageAll(@RequestBody PageEntity<Invoice> pageEntity) {
        Page<Invoice> page = baseService.pageAll(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
        return Result.success(page);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody Invoice entity) {
        baseService.add(entity);
        return Result.success();
    }

    @ApiOperation(value = "更新")
    @PostMapping("/update")
    public Result<Void> update(@RequestBody Invoice entity) {
        baseService.update(entity);

        return Result.success();
    }

    @ApiOperation(value = "删除")
    @PostMapping("/deleteById")
    public Result<Void> delete(String id) {
        baseService.delete(id);
        return Result.success();
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")

    public Result<Void> batchDelete(List<String> id) {
        baseService.removeByIds(id);
        return Result.success();
    }

    @ApiOperation(value = "查询详情")
    @GetMapping("/get")
    public Result<Invoice> get(String id) {
        Invoice byId = baseService.getById(id);
        return Result.success(byId);
    }
}
