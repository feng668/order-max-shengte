package com.whln.ordermax.api.designpattern.chain.impl.ordrplan;

import org.springframework.stereotype.Component;
import com.whln.ordermax.api.designpattern.chain.HandlerChain;
import com.whln.ordermax.data.domain.OrderPlan;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liurun
 * @date 2023-02-24 17:11
 */
@Component
public class OrderPlanChainFactory {
    public static final List<HandlerChain<List<OrderPlan>,OrderPlan>> ORDER_PLAN_CHAIN = new ArrayList<>();

    public  static void register(HandlerChain<List<OrderPlan>,OrderPlan> handler) {
        ORDER_PLAN_CHAIN.add(handler);
    }

    public OrderPlan start(List<OrderPlan> orderPlans) {
        for (HandlerChain<List<OrderPlan>,OrderPlan> h : ORDER_PLAN_CHAIN) {
            return h.handle(orderPlans);
        }
        return null;
    }
}
