package com.whln.ordermax.api.aspect;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whln.ordermax.data.service.MapCache;
import com.whln.ordermax.common.entity.FilterCondition;
import com.whln.ordermax.common.enums.JudgeType;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.utils.AopUtil;
import com.whln.ordermax.common.utils.ScriptProcessor;
import com.whln.ordermax.data.domain.OaRule;
import com.whln.ordermax.data.domain.OaRuleLevel;
import com.whln.ordermax.data.repository.OaRuleLevelRepository;
import com.whln.ordermax.data.repository.OaRuleRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Aspect
@Component
@Order(4)
public class OaBillSaveControlAspect {

    @Resource
    private MapCache mapCache;
    private final ScriptProcessor scriptProcessor = ScriptProcessor.getProcessor();
    @Resource
    private OaRuleRepository oaRuleRepository;

    @Resource
    private OaRuleLevelRepository oaRuleLevelRepository;

    @Pointcut("execution(* com.whln.ordermax.api.service..*.save(..))")
    public void point() {
    }


    @Around("point() && @annotation(com.whln.ordermax.api.annotations.OaBillSave)")
    @Transactional(rollbackFor = Exception.class)
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        Object entity = AopUtil.getArgByName(pjp, "entity");
        if (entity == null) {
            return pjp.proceed();
        }
        Map<String, String> entityCodeMap = mapCache.entityCodeMap();
        String entityClassName = entity.getClass().getSimpleName();
        String billCode = entityCodeMap.get(entityClassName);
        OaRule oaRule = oaRuleRepository.getOne(new LambdaQueryWrapper<OaRule>()
                .eq(OaRule::getBillCode, billCode)
                .eq(OaRule::getIsActive, 1));
        if (ObjectUtil.isEmpty(oaRule)) {
            ReflectUtil.setFieldValue(entity, "oaStatus", 2);
            return pjp.proceed();
        }
        JSONArray proceedOaConditionJsonArr = oaRule.getProceedOaCondition();
        if (proceedOaConditionJsonArr == null) {
            ReflectUtil.setFieldValue(entity, "oaStatus", 1);
            return pjp.proceed();
        }
        List<FilterCondition> proceedOaConditionList = JSONUtil.toList(proceedOaConditionJsonArr, FilterCondition.class);
        String proceedOaConditionStr = "";
        proceedOaConditionStr = setMutiConditionList(proceedOaConditionList, proceedOaConditionStr, entity);
        Boolean isProceedOa = scriptProcessor.execCondition(proceedOaConditionStr);
        if (isProceedOa) {
            oaStatusOps(entity);
        } else {
            ReflectUtil.setFieldValue(entity, "oaStatus", 2);
        }
        return pjp.proceed();
    }



    private void oaStatusOps(Object entity) {
        boolean oaStatus = ReflectUtil.hasField(entity.getClass(), "oaStatus");
        if (oaStatus) {
            List<OaRule> list = oaRuleRepository.list(new LambdaQueryWrapper<OaRule>().eq(OaRule::getBillCode, ModelMessage.SO_SI_ORDER.getCode()).eq(OaRule::getIsActive, 1));
            if (ObjectUtil.isEmpty(list)) {
                ReflectUtil.setFieldValue(entity, "oaStatus", 2);
            } else {
                List<String> oaRuleIdList = list.stream().map(OaRule::getId).collect(Collectors.toList());
                List<OaRuleLevel> list1 = oaRuleLevelRepository.list(new LambdaQueryWrapper<OaRuleLevel>().in(OaRuleLevel::getRuleId, oaRuleIdList));
                if (ObjectUtil.isEmpty(list1)) {
                    ReflectUtil.setFieldValue(entity, "oaStatus", 2);
                } else {
                    ReflectUtil.setFieldValue(entity, "oaStatus", 1);
                }
            }
        }
    }

    private String setMutiConditionList(List<FilterCondition> filterConditionList, String filterConditionStr, Object entity) {
        for (int i = 0; i < filterConditionList.size(); i++) {
            if (i != 0) {
                filterConditionStr += " " + ("AND".equals(filterConditionList.get(i).getJoinType()) ? "&&" : "||");
            }
            if (filterConditionList.get(i).getMutiConditionList() != null && filterConditionList.get(i).getMutiConditionList().size() > 0) {
                filterConditionStr += "(";
                filterConditionStr += this.setMutiConditionList(filterConditionList.get(i).getMutiConditionList(), filterConditionStr, entity);
                filterConditionStr += ")";
            } else {
                String onceCondition;
                Object fieldValueObj = ReflectUtil.getFieldValue(entity, filterConditionList.get(i).getAttr());
                String fieldValue = fieldValueObj == null ? null : fieldValueObj.toString();
                String[] conditionValueArr = filterConditionList.get(i).getValue().split(",");
                String jsCode = JudgeType.valueOf(JudgeType.class, filterConditionList.get(i).getJudgeType()).getJsTemp();
                switch (filterConditionList.get(i).getJudgeType()) {
                    case "BETWEEN":
                    case "NOT_BETWEEN":
                        onceCondition = filterConditionStr += StrUtil.concat(true,
                                "('",
                                fieldValue,
                                "'>=",
                                conditionValueArr[0],
                                "&&'",
                                fieldValue,
                                "'<=",
                                conditionValueArr[1],
                                ")"
                        );
                        break;
                    case "IN":
                    case "NOT_IN":
                        onceCondition = filterConditionStr += StrUtil.concat(true,
                                "('",
                                fieldValue,
                                "'.indexOf(['",
                                ArrayUtil.join(conditionValueArr, "','"),
                                "'])>-1)"
                        );
                        break;
                    case "IS_NULL":
                    case "IS_NOT_NULL":
                        if (fieldValue == null) {
                            onceCondition = filterConditionStr += StrUtil.format(jsCode, "true");
                        } else {
                            onceCondition = filterConditionStr += StrUtil.format(jsCode, "false");
                        }
                        break;
                    default:
                        onceCondition = filterConditionStr += StrUtil.format(
                                jsCode,
                                fieldValue,
                                filterConditionList.get(i).getValue()
                        );
                        break;
                }
                filterConditionStr += onceCondition;
            }
        }
        return filterConditionStr;
    }
}