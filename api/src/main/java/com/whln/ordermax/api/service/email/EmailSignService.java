package com.whln.ordermax.api.service.email;

import com.whln.ordermax.data.domain.param.EmailSignParam;
import com.whln.ordermax.data.domain.vo.EmailSignVO;

import java.util.List;

/**
 * @author liurun
 * @date 2023-02-21 18:47
 */
public interface EmailSignService {
    /**
     * 签名列表
     * @param param
     * @return
     */
    List<EmailSignVO> list(EmailSignParam param);

    /**
     * 查询签名详情
     * @param param
     * @return
     */
    EmailSignVO get(EmailSignParam param);

    /**
     * 删除签名
     * @param param
     */
    void deleteSign(EmailSignParam param);

    /**
     * 修改签名
     * @param param
     */
    void updateSign(EmailSignVO param);

    void add(EmailSignVO param);
}
