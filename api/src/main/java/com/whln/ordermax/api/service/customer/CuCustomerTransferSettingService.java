package com.whln.ordermax.api.service.customer;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CuCustomerTransferRule;
import com.whln.ordermax.data.domain.CuCustomerTransferSetting;
import com.whln.ordermax.data.repository.CuCustomerTransferRuleRepository;
import com.whln.ordermax.data.repository.CuCustomerTransferSettingRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

/**
 *  客户公海设置业务处理
 */
@Service
public class CuCustomerTransferSettingService{
    @Resource
    private CuCustomerTransferSettingRepository baseRepository;

    @Resource
    private CuCustomerTransferRuleRepository ruleRepository;

    @Resource
    private RestTemplate restTemplate;

    /**
    *  查询所有
    */
    public Result listAll(CuCustomerTransferSetting entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(CuCustomerTransferSetting entity, Page<CuCustomerTransferSetting> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  查询单条
    */
    public Result<CuCustomerTransferSetting> getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }

    /**
    *  插入或更新
    */
    public Result save(CuCustomerTransferSetting entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        if (CollectionUtil.isNotEmpty(entity.getRuleDelIdList())){
            ruleRepository.removeByIds(entity.getRuleDelIdList());
        }
        if (CollectionUtil.isNotEmpty(entity.getRuleList())){
            ruleRepository.saveOrUpdateBatch(entity.getRuleList());
        }
        return b? Result.success(): Result.error("201","保存失败");
    }


    public Result<CuCustomerTransferSetting> getAll() {
        return Result.success(baseRepository.getOne(null));
    }

    public Result<List<CuCustomerTransferRule>> listRule() {
        return Result.success(ruleRepository.list());
    }

    public Result contrSync(CuCustomerTransferSetting entity) {
        Result responseResult = new Result();
        if (entity.getIsActive()==1){
            try {
                responseResult = restTemplate.postForObject("http://127.0.0.1:12888/customerAutoTransfer/start", null, Result.class);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            try {
                responseResult = restTemplate.postForObject("http://127.0.0.1:12888/customerAutoTransfer/stop", null, Result.class);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        Boolean b = baseRepository.update(
                new UpdateWrapper<CuCustomerTransferSetting>()
                        .set("is_active", entity.getIsActive())
                        .eq("id", entity.getId())
        );
        return b? Result.success(): Result.error("201","操作失败");
    }
}