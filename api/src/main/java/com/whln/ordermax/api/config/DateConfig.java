package com.whln.ordermax.api.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.whln.ordermax.common.LocalDateFormatConstants;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


@Configuration
public class DateConfig {

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer customizer() {
        return b ->
                b.simpleDateFormat("yyyy-M-d H:m:s")
                        .serializationInclusion(JsonInclude.Include.NON_NULL)
                        .deserializerByType(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                        .deserializerByType(LocalTime.class, new LocalTimeDeserializer(DateTimeFormatter.ofPattern("HH:mm:ss")))
                        .deserializerByType(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                        .serializerByType(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                        .serializerByType(LocalTime.class, new LocalTimeSerializer(DateTimeFormatter.ofPattern("HH:mm:ss")))
                        .serializerByType(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    @Bean
    public Converter<String, LocalDateTime> stringToLocalDateTime() {
        return s -> LocalDateTime.parse(s, DateTimeFormatter.ofPattern(LocalDateFormatConstants.DEFAULT_DATE_TIME_FORMAT));
    }

    @Bean
    public Converter<LocalDateTime, String> localDateTimeToString() {
        return source -> source.format(DateTimeFormatter.ofPattern(LocalDateFormatConstants.DEFAULT_DATE_TIME_FORMAT));
    }
/*
     @Bean
    public Converter<LocalDate, String> localDateToString() {
        return source -> source.format(DateTimeFormatter.ofPattern(LocalDateFormatConstants.DEFAULT_DATE_FORMAT));
    }



   /* @Primary
    @Bean
    FastJsonHttpMessageConverter fastJsonHttpMessageConverter() {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        FastJsonConfig config = new FastJsonConfig();
        config.setDateFormat("yyyy-MM-dd HH:mm:ss");
        config.setCharset(StandardCharsets.UTF_8);
        config.setSerializerFeatures(
//                SerializerFeature.WriteClassName,
                SerializerFeature.WriteMapNullValue,
                SerializerFeature.PrettyFormat,
                SerializerFeature.WriteNullListAsEmpty,
                SerializerFeature.WriteNullStringAsEmpty
        );
        converter.setFastJsonConfig(config);
        return converter;
    }*/

   /* @Bean
    public Converter<LocalDate, String> localDateToString() {
        return source -> source.format(DateTimeFormatter.ofPattern(LocalDateFormatConstants.DEFAULT_DATE_FORMAT));
    }


    @Bean
    public Converter<String, LocalTime> stringToLocalTime() {
        return s -> LocalTime.parse(s, DateTimeFormatter.ofPattern(LocalDateFormatConstants.DEFAULT_TIME_FORMAT));
    }

    @Bean
    public Converter<LocalTime, String> localTimeToString() {
        return source -> source.format(DateTimeFormatter.ofPattern(LocalDateFormatConstants.DEFAULT_TIME_FORMAT));
    }*/


}
