package com.whln.ordermax.api.controller.stock;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.stock.StInventoryGrabService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.StInventoryGrab;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 控制器
 */
@Api(tags = "库存占用")
@RestController
@RequestMapping("/stInventoryGrab")
public class StInventoryGrabController {

    @Resource
    private StInventoryGrabService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<StInventoryGrab>> listAll(@RequestBody StInventoryGrab entity) {
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<StInventoryGrab>> pageAll(@RequestBody PageEntity<StInventoryGrab> pageEntity) {
        return baseService.pageAll(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<StInventoryGrab> getAllById(@RequestParam("id") String id) {
        return baseService.getAllById(id);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() StInventoryGrab entity) {
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id) {
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids) {
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "根据出运id查询商品占用单", notes =
                    "billId:发运通知单id" +
                    "commodityId:商品id"
    )
    @PostMapping("/listGrabByTransportationId")
    public Result<List<StInventoryGrab>> listComGrabByTransportationId(@RequestBody JSONObject entity) {
        return baseService.listComGrabByTransportationId(entity);
    }

}