package com.whln.ordermax.api.controller.product;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.product.AcceptanceService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.PdAcceptance;
import com.whln.ordermax.data.domain.param.AcceptanceBatchSaveParam;
import com.whln.ordermax.data.domain.vo.AcceptanceStandardVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 *  质检单控制器
 * @author liurun
 */
@Validated
@Api(tags = "验收单")
@RestController
@RequestMapping("/acceptance")
public class AcceptanceController {

    @Resource
    private AcceptanceService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<PdAcceptance>> listAll(@RequestBody PdAcceptance entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<PdAcceptance>> pageAll(@RequestBody PageEntity<PdAcceptance> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody @Validated PdAcceptance entity){
        return baseService.save(entity);
    }


    @ApiOperation(value = "查询检验单检验标准")
    @ApiImplicitParam(value = "生产质检单id",example = "15432151512")
    @GetMapping("/queryAcceptanceStandardList")
    public Result<List<AcceptanceStandardVO>> getPdQualityStandard(@NotNull(message = "生产单id不能为空")  String pdQualityId){
        List<AcceptanceStandardVO> pdQualityStandards = baseService.getAcceptanceStandardStandard(pdQualityId);
        return Result.success(pdQualityStandards);
    }
    @ApiOperation(value = "批量保存")
    @PostMapping("/batchSave")
    public Result<List<PdAcceptance>> batchSave(@RequestBody @Validated AcceptanceBatchSaveParam param){
        List<PdAcceptance> listResult = baseService.batchSave(param);
        return Result.success(listResult);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id") String id){
        baseService.delete(id);
        return Result.success();
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result<Void> batchDelete(@RequestBody()@Validated @Size(min = 1,message = "删除数量不能为空") List<String> ids){
        return baseService.batchDelete(ids);
    }

}