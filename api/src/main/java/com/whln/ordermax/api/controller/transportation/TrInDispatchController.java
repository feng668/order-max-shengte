package com.whln.ordermax.api.controller.transportation;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.transportation.TrInDispatchService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.TrInDispatch;
import com.whln.ordermax.data.domain.TrInDispatchCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  出运单控制器
 */
@Api(tags = "内销发货单")
@RestController
@RequestMapping("/trInDispatch")
public class TrInDispatchController {

    @Resource
    private TrInDispatchService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<TrInDispatch>> listAll(@RequestBody TrInDispatch entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<IPage<TrInDispatch>> pageAll(@RequestBody PageEntity<TrInDispatch> pageEntity){
        return Result.success(baseService.page(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize())));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody@Validated TrInDispatch entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "查询发货单商品")
    @PostMapping("/listCom")
    public Result<List<TrInDispatchCommodity>> listCom(@RequestBody TrInDispatchCommodity entity){
        return baseService.listCom(entity);
    }

}