package com.whln.ordermax.api.controller.finance;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.finance.FnPaymentRequesService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.FnPaymentBill;
import com.whln.ordermax.data.domain.FnPaymentRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  付款单控制器
 */
@Api(tags = "付款单")
@RestController
@RequestMapping("/fnPaymentRequest")
public class FnPaymentRequestController {

    @Resource
    private FnPaymentRequesService baseService;

    @ApiOperation(value = "查询已付款采购单")
    @PostMapping("/listPaymentBillByPaymentId")
    public Result<List<FnPaymentBill>> listPaymentBillByPaymentId(@RequestParam("id") String id){
        return baseService.listPaymentBillByPaymentId(id);
    }

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<FnPaymentRequest>> listAll(@RequestBody FnPaymentRequest entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<FnPaymentRequest>> pageAll(@RequestBody PageEntity<FnPaymentRequest> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() FnPaymentRequest entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}