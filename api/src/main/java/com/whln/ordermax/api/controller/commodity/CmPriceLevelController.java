package com.whln.ordermax.api.controller.commodity;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.commodity.CmPriceLevelService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CmPriceLevel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  商品价位控制器
 */
@Api(tags = "商品价位")
@RestController
@RequestMapping("/CmPriceLevel")
public class CmPriceLevelController{

    @Resource
    private CmPriceLevelService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<CmPriceLevel>> listAll(@RequestBody CmPriceLevel entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<CmPriceLevel>> pageAll(@RequestBody PageEntity<CmPriceLevel> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() CmPriceLevel entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}