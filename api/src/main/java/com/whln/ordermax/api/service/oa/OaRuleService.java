package com.whln.ordermax.api.service.oa;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.OaRule;
import com.whln.ordermax.data.domain.OaRuleLevel;
import com.whln.ordermax.data.domain.OaRuleLevelAssessor;
import com.whln.ordermax.data.domain.vo.OaRuleListResult;
import com.whln.ordermax.data.domain.vo.OaRuleVO;
import com.whln.ordermax.data.repository.OaRuleLevelAssessorRepository;
import com.whln.ordermax.data.repository.OaRuleLevelRepository;
import com.whln.ordermax.data.repository.OaRuleRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * oa定制规则业务处理
 */
@Service
public class OaRuleService {
    @Resource
    private OaRuleRepository baseRepository;

    @Resource
    private OaRuleLevelRepository levelRepository;

    @Resource
    private OaRuleLevelAssessorRepository assessorRepository;
    @Resource
    BacklogService backlogService;
    @Resource
    OaRuleRepository oaRuleRepository;

    /**
     * 查询所有
     */
    public Result<OaRuleListResult> list(OaRule entity) {
        List<OaRule> oaRules = baseRepository.listByEntity(entity);
        Result<Map> mapResult = backlogService.listCounts();
        Map<String, Integer> countMap = (Map<String, Integer>) mapResult.getData().get("countMap");
        OaRuleListResult oaRuleListResult = getOaRuleListResult(oaRules, countMap);
        return Result.success(oaRuleListResult);
    }

    public Result<OaRuleListResult> listByUser(OaRule entity) {
        String userId = ShiroManager.getUserId();
        OaRuleLevelAssessor oaRuleLevelAssessor = new OaRuleLevelAssessor();
        if (!ShiroManager.hasRole("webadmin")) {
            oaRuleLevelAssessor.setAssessorId(userId);
        }
        List<OaRuleLevelAssessor> assessorList = assessorRepository.listByEntity(oaRuleLevelAssessor);
        List<String> ruleIds = assessorList.stream().map(OaRuleLevelAssessor::getRuleId).collect(Collectors.toList());
        List<OaRule> oaRules = new ArrayList<>(0);
        if (ObjectUtil.isNotEmpty(ruleIds)) {
            oaRules = oaRuleRepository.list(new LambdaQueryWrapper<OaRule>().in(OaRule::getId, ruleIds));
        }
        //找出可见的模块
        Map<String, Integer> contMap = backlogService.queryOrderNum(1);
        OaRuleListResult oaRuleListResult = getOaRuleListResult(oaRules, contMap);
        return Result.success(oaRuleListResult);
    }

    private OaRuleListResult getOaRuleListResult(List<OaRule> oaRules, Map<String, Integer> contMap) {
        List<OaRule> result = oaRules.stream().peek(item -> {
            Integer integer = contMap.get(item.getBillCode());
            if (ObjectUtil.isNotEmpty(integer)){
            item.setNum(contMap.get(item.getBillCode()));

            }else {
                item.setNum(0);

            }

        }).collect(Collectors.toList());

        Integer total = 0;
        if (contMap.size() > 0) {
            total = contMap.values().stream().reduce(Integer::sum).get();
        }
        OaRuleListResult build = OaRuleListResult.builder()
                .total(total)
                .oaRuleVOList(BeanUtil.copyToList(result, OaRuleVO.class))
                .build();
        return build;
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(OaRule entity, Page<OaRule> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    public Result save(OaRule entity) {
        List<OaRuleLevel> levelList = entity.getLevelList();
        entity.setTotalLevel(levelList.size());
        boolean b = baseRepository.saveOrUpdate(entity);
        if (CollectionUtil.isNotEmpty(entity.getDelLevelList())) {
            levelRepository.removeByIds(entity.getDelLevelList());
        }
        if (CollectionUtil.isNotEmpty(entity.getLevelList())) {
            levelRepository.saveOrUpdateBatch(entity.getLevelList());
        }

        if (CollectionUtil.isNotEmpty(entity.getDelAssessorList())) {
            assessorRepository.removeByIds(entity.getDelAssessorList());
        }
        if (CollectionUtil.isNotEmpty(entity.getAssessorList())) {
            assessorRepository.saveOrUpdateBatch(entity.getAssessorList());
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        levelRepository.remove(new QueryWrapper<OaRuleLevel>().eq("rule_id", id));
        assessorRepository.remove(new QueryWrapper<OaRuleLevelAssessor>().eq("rule_id", id));
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<List<Map<String, String>>> batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        levelRepository.remove(new QueryWrapper<OaRuleLevel>().in("rule_id", ids));
        assessorRepository.remove(new QueryWrapper<OaRuleLevelAssessor>().in("rule_id", ids));
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result<List<Map<String, String>>> listBills() {
        Map<String, Integer> stringIntegerMap = backlogService.queryOrderNum(1);
//        List<BillEntity> list = Bills.billInfo.entrySet().stream().map(Map.Entry::getValue).collect(Collectors.toList());
        List<Map<String, String>> list = Arrays.stream(ModelMessage.values()).map(m -> new HashMap<String, String>() {{
            put("code", m.getCode());
            put("name", m.getName());
            Integer integer = stringIntegerMap.get(m.getCode());
            if (ObjectUtil.isNotEmpty(integer)) {
                put("num", integer.toString());
            }else {

                put("num", "0");
            }
        }}).collect(Collectors.toList());
        return Result.success(list);
    }

    public Result<List<OaRuleLevel>> listLevelByRuleId(String ruleId) {
        return Result.success(levelRepository.listByEntity(new OaRuleLevel() {{
            setRuleId(ruleId);
        }}));
    }
}