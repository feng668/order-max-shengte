package com.whln.ordermax.api.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.whln.ordermax.api.Interceptor.FieldAuthControlInnerInterceptor;
import com.whln.ordermax.api.Interceptor.FilterConditionControlInnerInterceptor;
import com.whln.ordermax.api.Interceptor.OrderInnerInterceptor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
public class MybatisPlusConfig {
    @Resource
    private ApplicationContext applicationContext;

    /*配置分页插件*/
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        paginationInnerInterceptor.setOverflow(false);
        interceptor.addInnerInterceptor(new FieldAuthControlInnerInterceptor(applicationContext));
        interceptor.addInnerInterceptor(new FilterConditionControlInnerInterceptor(applicationContext));
        interceptor.addInnerInterceptor(new OrderInnerInterceptor(applicationContext));
        interceptor.addInnerInterceptor(new BlockAttackInnerInterceptor());
        interceptor.addInnerInterceptor(paginationInnerInterceptor);
        return interceptor;
    }
}
