package com.whln.ordermax.api.shiro.realm;

import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.api.shiro.ByteSourceSerializable;
import com.whln.ordermax.api.shiro.ByteSourceUtils;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.SysPower;
import com.whln.ordermax.data.domain.SysRole;
import com.whln.ordermax.data.domain.SysUser;
import com.whln.ordermax.data.repository.SysPowerRepository;
import com.whln.ordermax.data.repository.SysRoleRepository;
import com.whln.ordermax.data.repository.SysUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 用户密码登录realm
 * @author liurun
 */
@Slf4j
public class UserPasswordRealm extends AuthorizingRealm {
    @Resource
    private SysUserRepository sysUserRepository;

    @Resource
    SysRoleRepository sysRoleRepository;

    @Resource
    SysPowerRepository sysPowerRepository;
    @Override
    public String getName() {
        return LoginType.USER_PASSWORD.getType();
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        if (token instanceof UserToken) {
            return ((UserToken) token).getLoginType() == LoginType.USER_PASSWORD;
        } else {
            return false;
        }
    }

    @Override
    public void setAuthorizationCacheName(String authorizationCacheName) {
        super.setAuthorizationCacheName(authorizationCacheName);
    }

    @Override
    protected void clearCachedAuthorizationInfo(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
    }

    /**
     * 认证信息.(身份验证) : Authentication 是用来验证用户身份
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
        log.info("---------------- 用户密码登录 ----------------------");
        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
        String name = token.getUsername();
        // 从数据库获取对应用户名密码的用户
       SysUser user= sysUserRepository.getUserByUsername(name);
        if (ObjectUtil.isEmpty(user)) {
            throw new BusinessException("用户不存在");
        }

        if (ObjectUtil.equals(user.getStatus(),0)) {
            throw new BusinessException("用户被禁用");
        }
        ByteSourceSerializable bytes = ByteSourceUtils.bytes(user.getId() + "salt");
        List<SysRole> sysRoleList = sysRoleRepository.listRolesByUserId(user.getId());
        user.setSysRoleList(sysRoleList);
        log.info("---------------- 认证完成 ----------------------");
        return new SimpleAuthenticationInfo(user, user.getPassword(),bytes, getName());
    }

    /**
     * 授权
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        log.info("---------------- 执行 Shiro 权限获取 ---------------------");
        Object principal = principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        if (principal instanceof SysUser) {
            SysUser sysUser = (SysUser) principal;
            if (ObjectUtil.isNotEmpty(sysUser)) {
                List<SysRole> roleList = sysRoleRepository.listRolesByUserId(sysUser.getId());

                if (ObjectUtil.isNotEmpty(roleList)) {
                    Set<String> roles = roleList.stream().map(SysRole::getCode).collect(Collectors.toSet());
                    info.addRoles(roles);
                }
                List<SysPower> sysPowers = sysPowerRepository.listPowersByUserId(sysUser.getId());
                if (ObjectUtil.isNotEmpty(sysPowers)) {
                    Set<String> collect = sysPowers.stream().map(SysPower::getPermission).collect(Collectors.toSet());
                    info.addStringPermissions(collect);
                }
            }

            log.info("用户{}获取到以下权限:{}",sysUser.getUsername(),info.getStringPermissions());
            log.info("用户{}获取到以下角色:{}",sysUser.getUsername(),info.getRoles());
        }

        return info;
    }
}
