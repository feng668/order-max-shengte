package com.whln.ordermax.api.init;

import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.common.enums.RedisKey;
import com.whln.ordermax.data.domain.SysDict;
import com.whln.ordermax.data.domain.SysDictDetail;
import com.whln.ordermax.data.repository.SysDictDetailRepository;
import com.whln.ordermax.data.repository.SysDictRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-04-21 17:29
 */
@Order(0)
@Component
public class SysDictInit extends InitData<Object> implements InitializingBean {
    @Resource
    private SysDictRepository sysDictRepository;
    @Resource
    private SysDictDetailRepository sysDictDetailRepository;
    @Resource
    private ServletContext servletContext;
    @Override
    void init() {
        List<SysDict> list = sysDictRepository.list();
        for (SysDict sysDict : list) {
            redisClient.rPush(RedisKey.SYS_DICT_ALL.getKey(),sysDict);
        }
        List<SysDictDetail> list1 = sysDictDetailRepository.list();
        for (SysDictDetail sysDictDetail : list1) {
            redisClient.rPush(RedisKey.SYS_DICT_DETAIL_ALL.getKey(),sysDictDetail);
        }
        Map<String, List<SysDictDetail>> collect = list1.stream().collect(Collectors.groupingBy(SysDictDetail::getDictId));
        for (SysDict sysDict:list){
            List<SysDictDetail> sysDictDetails = collect.get(sysDict.getId());
            if (ObjectUtil.isNotEmpty(sysDictDetails)) {
                sysDict.setDictDetailList(sysDictDetails);
            }
        }
//        servletContext.setAttribute("dict",list);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initFactory.register(this);
    }
}
