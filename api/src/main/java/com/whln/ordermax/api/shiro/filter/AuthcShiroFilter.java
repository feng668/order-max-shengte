package com.whln.ordermax.api.shiro.filter;


import cn.hutool.json.JSONUtil;
import com.whln.ordermax.common.entity.Result;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthcShiroFilter extends FormAuthenticationFilter {
    private static final String DEFAULT_LOGIN_URL = "/auth/login";
    private String loginUrl = DEFAULT_LOGIN_URL;

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        return super.isAccessAllowed(request, response, mappedValue);
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        super.setLoginUrl(loginUrl);

        if (isLoginRequest(request, response)) {
            if (isLoginSubmission(request, response)) {
                return executeLogin(request, response);
            } else {
                return true;
            }
        } else {
            // option请求处理
            HttpServletRequest req =  WebUtils.toHttp(request);
            HttpServletResponse resp = WebUtils.toHttp(response);
            if (req.getMethod().equals(RequestMethod.OPTIONS.name())) {
                resp.setStatus(HttpStatus.OK.value());
                return true;
            }
            //重定向到
            redirectToLogin(request,response);
            return false;
        }
    }

    @Override
    protected void redirectToLogin(ServletRequest request, ServletResponse response) throws IOException {
        Result<Object> error = Result.error(-1,"请登录之后再试！");
        String s = JSONUtil.toJsonStr(error);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(s);
//      WebUtils.issueRedirect(request, response, "/");

    }


    @Override
    protected boolean isLoginRequest(ServletRequest request, ServletResponse response) {
        return pathsMatch(loginUrl, request);
    }
/*
    *//**
     * 替代shiro重定向
     *
     * @param req
     * @param resp
     * @throws IOException
     *//*
    private void returnTokenInvalid(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setHeader("Access-Control-Allow-Origin", req.getHeader("Origin"));
        resp.setHeader("Access-Control-Allow-Credentials", "true");
        resp.setContentType("application/json; charset=utf-8");
        resp.setCharacterEncoding("UTF-8");
        Writer out = new BufferedWriter(new OutputStreamWriter(resp.getOutputStream()));
        out.write(JSONObject.toJSONString(new Result<>(ResultStatusCode.INVALID_TOKEN)));
        out.flush();
        out.close();
    }*/
}
