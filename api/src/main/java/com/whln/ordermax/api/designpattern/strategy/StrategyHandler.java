package com.whln.ordermax.api.designpattern.strategy;

/**
 * @author liurun
 * @date 2023-02-24 17:45
 */
public interface StrategyHandler<P, T, R> {

    /**
     * 策略模式处理方法
     * @param p 参数
     * @param t 参数
     * @return R 参数
     */
    R handle(P p, T t);
}
