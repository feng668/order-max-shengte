package com.whln.ordermax.api.controller.whole;

import cn.hutool.json.JSONObject;
import com.whln.ordermax.api.service.whole.BillFlowService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.FlFlowBill;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *  订单进度控制器
 */
@Api(tags = "流转通用接口")
@RestController
@RequestMapping("/billFlow")
public class BillFlowController {

    @Resource
    private BillFlowService baseService;

    @ApiOperation(value = "查询单据流转状态")
    @PostMapping("/listBillFlowStatus")
    public Result<List<FlFlowBill>> listBillFlowStatus(@RequestBody() FlFlowBill entity){
        return baseService.listBillFlowStatus(entity);
    }

    @ApiOperation(value = "查询单据商品详情及未流转数量")
    @PostMapping("/listBillCommodity")
    public Result<List<Map>> listBillCommodityWithRemainNum(@RequestBody() FlFlowBill entity){
        return baseService.listBillCommodityWithRemainNum(entity);
    }

    @ApiOperation(value = "查询未流转单据并携带未流转商品和剩余数量")
    @PostMapping("/listBillWithCommodity")
    public Result<List<Map<String, Object>>> listBillWithCommodity(@RequestBody() JSONObject entity){
        return baseService.listBillWithCommodity(entity);
    }

}