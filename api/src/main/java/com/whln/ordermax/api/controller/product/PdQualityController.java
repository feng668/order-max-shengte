package com.whln.ordermax.api.controller.product;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.product.PdQualityService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.PdQuality;
import com.whln.ordermax.data.domain.param.PdQualityBatchSaveParam;
import com.whln.ordermax.data.domain.vo.PdQualityStandardVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 *  质检单控制器
 * @author liurun
 */
@Validated
@Api(tags = "质检单")
@RestController
@RequestMapping("/pdQuality")
public class PdQualityController{

    @Resource
    private PdQualityService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<PdQuality>> listAll(@RequestBody PdQuality entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<IPage<PdQuality>> pageAll(@RequestBody PageEntity<PdQuality> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody @Validated PdQuality entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "查询生产单检验标准")
    @ApiImplicitParam(value = "生产质检单id",example = "15432151512")
    @GetMapping("/queryPdQualityStandardList")
    public Result<List<PdQualityStandardVO>> getPdQualityStandard(@NotNull(message = "生产单id不能为空")  String pdQualityId){
        List<PdQualityStandardVO> pdQualityStandards = baseService.getPdQualityStandard(pdQualityId);
        return Result.success(pdQualityStandards);
    }

    @ApiOperation(value = "批量保存")
    @PostMapping("/batchSave")
    public Result<List<PdQuality>> batchSave(@RequestBody @Validated PdQualityBatchSaveParam param){
        List<PdQuality> listResult = baseService.batchSave(param);
        return Result.success(listResult);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id") String id){
        baseService.delete(id);
        return Result.success();
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result<Void> batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}