package com.whln.ordermax.api.service.whole;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.BillAttachment;
import com.whln.ordermax.data.repository.BillAttachmentRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  单据附件业务处理
 */
@Service
public class BillAttachmentService{
    @Resource
    private BillAttachmentRepository baseRepository;

    /**
    *  查询所有
    */
    public Result listAll(BillAttachment entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(BillAttachment entity, Page<BillAttachment> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  查询单条
    */
    public Result<BillAttachment> getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }

    /**
    *  插入或更新
    */
    public Result save(BillAttachment entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

}