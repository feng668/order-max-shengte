package com.whln.ordermax.api.service.transportation;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.TrInDispatch;
import com.whln.ordermax.data.domain.TrInDispatchCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.TrInDispatchCommodityMapper;
import com.whln.ordermax.data.repository.TrInDispatchCommodityRepository;
import com.whln.ordermax.data.repository.TrInDispatchRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 出运单业务处理
 */
@BillCode(ModelMessage.TR_IN_DISPATCH)
@Service
public class TrInDispatchService {
    @Resource
    private TrInDispatchRepository baseRepository;

    @Resource
    private TrInDispatchCommodityRepository commodityRepository;
    @Resource
    private TrInDispatchCommodityMapper trInDispatchCommodityMapper;

    /**
     * 查询所有
     */
    public Result listAll(TrInDispatch entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(TrInDispatch entity, Page<TrInDispatch> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    public IPage<TrInDispatch> page(TrInDispatch entity, Page<TrInDispatch> page) {
        IPage<TrInDispatch> trInDispatchIPage = baseRepository.pageByEntity(entity, page);
        List<TrInDispatch> records = trInDispatchIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(TrInDispatch::getId).collect(Collectors.toSet());
            List<TrInDispatchCommodity> list = trInDispatchCommodityMapper.commodityList(BillQuery.builder().billIds(billIds).build());
            Map<String, List<TrInDispatchCommodity>> groupByBillId = list.stream().collect(Collectors.groupingBy(TrInDispatchCommodity::getBillId));
            List<TrInDispatch> result = records.stream().peek(item -> {
                List<TrInDispatchCommodity> commodities = groupByBillId.get(item.getId());
                item.setCommodityList(commodities);
            }).collect(Collectors.toList());
            trInDispatchIPage.setRecords(result);
        }
        return trInDispatchIPage;
    }


    /**
     * 插入或更新
     */
    @FlowControlSave
    @Transactional(rollbackFor = Exception.class)
    public Result save(TrInDispatch entity) {
        TrInDispatch t = baseRepository.getOne(new LambdaQueryWrapper<TrInDispatch>().eq(TrInDispatch::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(t)&&!ObjectUtil.equals(t.getId(),entity.getId())) {
            throw new BusinessException("单据编号已存在！");
        }
        boolean b = baseRepository.saveOrUpdate(entity);

        List<TrInDispatchCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream()
                .map(TrInDispatchCommodity::getId)
                .filter(ObjectUtil::isNotEmpty)
                .collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<TrInDispatchCommodity>()
                .eq(TrInDispatchCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(idList), TrInDispatchCommodity::getId, idList));
        if (CollectionUtil.isNotEmpty(commodityList)) {
            commodityRepository.saveOrUpdateBatch(commodityList);
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }


    /**
     * 单条删除
     */
    @Transactional
    public Result delete(String id) {
        TrInDispatch one = baseRepository.getOne(new LambdaQueryWrapper<TrInDispatch>().eq(TrInDispatch::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            commodityRepository.remove(new QueryWrapper<TrInDispatchCommodity>().eq("bill_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<TrInDispatch>().eq(TrInDispatch::getId, id).set(TrInDispatch::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional
    public Result batchDelete(List<String> ids) {

        List<TrInDispatch> customsDeclarations = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = customsDeclarations.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = customsDeclarations.stream()
                .map(TrInDispatch::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());
        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new QueryWrapper<TrInDispatchCommodity>().in("bill_id", ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<TrInDispatch>().in(TrInDispatch::getId, ids).set(TrInDispatch::getIsDelete, 1));
            }
        }
        return Result.success();
    }

    public Result<List<TrInDispatchCommodity>> listCom(TrInDispatchCommodity entity) {
        return Result.success(commodityRepository.listByEntity(entity));
    }
}