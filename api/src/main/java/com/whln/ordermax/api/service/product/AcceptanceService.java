package com.whln.ordermax.api.service.product;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.ann.OrderPlanSave;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.codeEnum.ModelMessage;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.param.AcceptanceBatchSaveParam;
import com.whln.ordermax.data.domain.vo.AcceptanceStandardVO;
import com.whln.ordermax.data.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 质检单业务处理
 *
 * @author liurun
 */
@BillCode(ModelMessage.ACCEPTANCE)
@Service
public class AcceptanceService {

    @Resource
    private AcceptanceRepository baseRepository;
    @Resource
    private AcceptanceStandardRepository acceptanceStandardRepository;

    @Resource
    private CmCommodityStandardRepository cmCommodityStandardRepository;

    @Resource
    private OrderPlanRepository orderPlanRepository;

    @Resource
    private AcceptanceLotRepository acceptanceLotRepository;

    /**
     * 查询所有
     */
    public Result<List<PdAcceptance>> listAll(PdAcceptance entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    public List<PdAcceptance> list(PdAcceptance entity) {
        return baseRepository.list(new LambdaQueryWrapper<PdAcceptance>()
                .eq(PdAcceptance::getAcceptanceId, entity.getId()));
    }

    /**
     * 分页查询所有
     */
    public Result<Page<PdAcceptance>> pageAll(PdAcceptance entity, Page<PdAcceptance> page) {
        Page<PdAcceptance> iPage = baseRepository.pageByEntity(entity, page);
        List<PdAcceptance> records = iPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> pdQualityIds = records.stream()
                    .map(PdAcceptance::getId)
                    .collect(Collectors.toSet());

            List<PdAcceptanceStandard> pdQualityStandards = acceptanceStandardRepository
                    .list(new LambdaQueryWrapper<PdAcceptanceStandard>()
                            .in(PdAcceptanceStandard::getAcceptanceId, pdQualityIds));
            Map<String, List<PdAcceptanceStandard>> pdQualityStandardGroup =
                    pdQualityStandards.stream()
                            .collect(Collectors.groupingBy(PdAcceptanceStandard::getAcceptanceId));
            List<PdAcceptanceLot> lotList = acceptanceLotRepository.list(new LambdaQueryWrapper<PdAcceptanceLot>().in(PdAcceptanceLot::getBillId,pdQualityIds));
            Map<String, List<PdAcceptanceLot>> lotGroup = lotList.stream().collect(Collectors.groupingBy(PdAcceptanceLot::getBillId));

            List<PdAcceptance> pdQualitySetStandardList = records.stream()
                    .peek(ite -> {
                                ite.setStandardList(pdQualityStandardGroup.get(ite.getId()));
                                List<PdAcceptanceLot> pdAcceptanceLots = lotGroup.get(ite.getId());
                                if (ObjectUtil.isNotEmpty(pdAcceptanceLots)) {
                                    Map<Integer, List<PdAcceptanceLot>> collect1 = pdAcceptanceLots.stream().collect(Collectors.groupingBy(PdAcceptanceLot::getType));
                                    ite.setInLotList(collect1.get(0));
                                    ite.setOutLotList(collect1.get(1));
                                }
                            }
                    )
                    .collect(Collectors.toList());
            iPage.setRecords(pdQualitySetStandardList);
        }
        return Result.success(iPage);
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    @OrderPlanSave
    public Result<Void> save(PdAcceptance entity) {
        PdAcceptance acceptance = baseRepository.getOne(new LambdaQueryWrapper<PdAcceptance>().eq(PdAcceptance::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(acceptance) && !ObjectUtil.equals(entity.getId(), acceptance.getId())) {
            throw new BusinessException("编号已存在");
        }
        entity.initNullList();
        boolean b = baseRepository.saveOrUpdate(entity);
        List<PdAcceptanceStandard> standardList = entity.getStandardList();
        List<String> ids = standardList.stream().peek(item -> {
            item.setAcceptanceId(entity.getId());
        }).map(PdAcceptanceStandard::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        acceptanceStandardRepository.remove(new LambdaUpdateWrapper<PdAcceptanceStandard>()
                .eq(PdAcceptanceStandard::getAcceptanceId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(ids), PdAcceptanceStandard::getId, ids)
        );

        List<PdAcceptanceLot> inLotList = entity.getInLotList().stream().peek(e -> {
            e.setType(0);
        }).collect(Collectors.toList());
        inLotList.addAll(entity.getOutLotList().stream().peek(e -> {
            e.setType(1);
        }).collect(Collectors.toList()));

        acceptanceLotRepository.remove(new LambdaUpdateWrapper<PdAcceptanceLot>()
                .eq(PdAcceptanceLot::getBillId, entity.getId())
        );
        acceptanceLotRepository.saveBatch(inLotList);
        acceptanceStandardRepository.saveBatch(standardList);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public void delete(String id) {
        PdAcceptance one = baseRepository.getOne(new LambdaQueryWrapper<PdAcceptance>().eq(PdAcceptance::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            acceptanceStandardRepository.remove(new LambdaUpdateWrapper<PdAcceptanceStandard>().eq(PdAcceptanceStandard::getAcceptanceId, id));
            acceptanceLotRepository.remove(new LambdaUpdateWrapper<PdAcceptanceLot>().eq(PdAcceptanceLot::getBillId, id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<PdAcceptance>().eq(PdAcceptance::getId, id).set(PdAcceptance::getIsDelete, 1));
        }
    }


    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> batchDelete(List<String> ids) {
        List<PdAcceptance> enquiryOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = enquiryOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = enquiryOrders.stream()
                .map(PdAcceptance::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                acceptanceStandardRepository.remove(new LambdaQueryWrapper<PdAcceptanceStandard>().in(PdAcceptanceStandard::getAcceptanceId, ids));
                acceptanceLotRepository.remove(new LambdaUpdateWrapper<PdAcceptanceLot>().in(PdAcceptanceLot::getBillId, ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<PdAcceptance>().in(PdAcceptance::getId, ids).set(PdAcceptance::getIsDelete, 1));
            }
        }
        return Result.success();
    }

    @Resource
    SupSupplierRepository supSupplierRepository;

    @FlowControlSave
    @Transactional(rollbackFor = Exception.class)
    public List<PdAcceptance> batchSave(AcceptanceBatchSaveParam param) {
        List<PdAcceptance> qualities = param.getEntityList();
        PdAcceptance pdAcceptance = qualities.get(0);
        SupSupplier one = supSupplierRepository.getOne(new LambdaQueryWrapper<SupSupplier>().eq(SupSupplier::getId, pdAcceptance.getSupplierId()));
        qualities = qualities.stream().peek(i -> i.setSupplierNo(one.getSupplierNo())).collect(Collectors.toList());
        Set<String> commodityIds = qualities.stream().map(PdAcceptance::getCommodityId).collect(Collectors.toSet());
        List<CmCommodityStandard> list = cmCommodityStandardRepository.list(new LambdaQueryWrapper<CmCommodityStandard>()
                .select(
                        CmCommodityStandard::getStandardCondition,
                        CmCommodityStandard::getFactor,
                        CmCommodityStandard::getCommodityId,
                        CmCommodityStandard::getId)
                .in(CmCommodityStandard::getCommodityId, commodityIds)
        );


        Map<String, List<CmCommodityStandard>> groupByCommodityId =
                list.stream().collect(Collectors.groupingBy(CmCommodityStandard::getCommodityId));

        List<PdAcceptanceStandard> acceptanceStandards = new ArrayList<>();
        for (PdAcceptance entity : qualities) {
            entity.setParentBillCode(param.getLastBillCode());
            entity.setParentBillId(param.getLastBillId());
            baseRepository.save(entity);
            List<PdAcceptanceLot> inLotList = entity.getInLotList();
            if (ObjectUtil.isEmpty(inLotList)) {
                inLotList = new ArrayList<>();
            }
            List<PdAcceptanceLot> outLotList = entity.getOutLotList();
            if (ObjectUtil.isEmpty(outLotList)) {
                outLotList = new ArrayList<>();
            }
            inLotList.addAll(outLotList);
            acceptanceLotRepository.saveBatch(inLotList);
            String id = entity.getId();
            List<CmCommodityStandard> cmCommodityStandards = groupByCommodityId.get(entity.getCommodityId());
            if (ObjectUtil.isNotEmpty(cmCommodityStandards)) {
                for (CmCommodityStandard c : cmCommodityStandards) {
                    if (c.check()) {
                        throw new BusinessException("检验标准数据异常");
                    }
                    PdAcceptanceStandard pdQualityStandard = new PdAcceptanceStandard();
                    pdQualityStandard.setAcceptanceId(id);
                    pdQualityStandard.setStandardCondition(c.getStandardCondition());
                    pdQualityStandard.setFactor(c.getFactor());
                    acceptanceStandards.add(pdQualityStandard);
                }
            }
            orderPlanRepository.update(new LambdaUpdateWrapper<OrderPlan>()
                    .eq(OrderPlan::getBillId, param.getLastBillId())
                    .eq(OrderPlan::getBillCode, param.getLastBillCode())
                    .set(OrderPlan::getBillStatus, 2)
            );

            String lastBillId = param.getLastBillId();
            String lastBillCode = param.getLastBillCode();
            OrderPlan lastOrderPlan = null;
            if (ObjectUtil.isNotEmpty(lastBillId)) {
                lastOrderPlan = orderPlanRepository.getOne(
                        new QueryWrapper<OrderPlan>()
                                .eq("bill_id", lastBillId)
                                .eq("bill_code", lastBillCode));
            }
            OrderPlan orderPlan = new OrderPlan();
            orderPlan.setLastBillId(ObjectUtil.isEmpty(lastBillId) ? entity.getId() : lastBillId);
            orderPlan.setLastBillCode(ObjectUtil.isEmpty(lastBillCode) ? ModelMessage.ACCEPTANCE.getCode() : lastBillCode);
            orderPlan.setBillId(entity.getId());
            orderPlan.setBillCode(ModelMessage.ACCEPTANCE.getCode());
            orderPlan.setRootBillId(ObjectUtil.isEmpty(lastOrderPlan) ?
                    entity.getId() : lastOrderPlan.getRootBillId());
            orderPlan.setBillStatus(1);
            orderPlan.setTemId(1);
            orderPlan.setBillNo(entity.getBillNo());
            orderPlanRepository.save(orderPlan);
        }

        if (ObjectUtil.isNotEmpty(acceptanceStandards)) {
            acceptanceStandardRepository.saveBatch(acceptanceStandards);
        }
        return qualities;
    }


    public List<AcceptanceStandardVO> getAcceptanceStandardStandard(String pdQualityId) {
        List<PdAcceptanceStandard> list = acceptanceStandardRepository.list(new LambdaQueryWrapper<PdAcceptanceStandard>().eq(PdAcceptanceStandard::getAcceptanceId, pdQualityId));
        return BeanUtil.copyToList(list, AcceptanceStandardVO.class);
    }

}