package com.whln.ordermax.api.service.sys.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.data.domain.CustomerLabel;
import com.whln.ordermax.data.domain.CustomerLabelGroup;
import com.whln.ordermax.data.domain.CustomerLabelRelation;
import com.whln.ordermax.data.domain.param.DeleteParam;
import com.whln.ordermax.data.domain.param.UserBindLabelParam;
import com.whln.ordermax.data.domain.query.CustomerLabelQuery;
import com.whln.ordermax.data.domain.vo.CustomerLabelVO;
import com.whln.ordermax.data.repository.CustomerLabelGroupRepository;
import com.whln.ordermax.data.repository.CustomerLabelRelationRepository;
import com.whln.ordermax.data.repository.CustomerLabelRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-03-06 15:47
 */
@Service
@AllArgsConstructor
public class CustomerLabelService {
    private final CustomerLabelRepository customerLabelRepository;
    private final CustomerLabelGroupRepository customerLabelGroupRepository;
    private final CustomerLabelRelationRepository customerLabelRelationRepository;

    public List<CustomerLabelVO> list(CustomerLabelQuery query) {
        List<CustomerLabel> list = customerLabelRepository.list(new LambdaQueryWrapper<CustomerLabel>()
                .eq(ObjectUtil.isNotEmpty(query.getGroupId()), CustomerLabel::getGroupId, query.getGroupId())
        );

        Set<String> groupIds = list.stream().map(CustomerLabel::getGroupId).collect(Collectors.toSet());
        List<CustomerLabelVO> userLabelVOList = BeanUtil.copyToList(list, CustomerLabelVO.class);
        if (ObjectUtil.isNotEmpty(groupIds)) {
            List<CustomerLabelGroup> labelGroups = customerLabelGroupRepository.list(new LambdaQueryWrapper<CustomerLabelGroup>()
                    .in(CustomerLabelGroup::getId, groupIds));
            Map<String, String> labelGroupsToMap = labelGroups.stream()
                    .collect(Collectors.toMap(CustomerLabelGroup::getId, CustomerLabelGroup::getGroupName));
            userLabelVOList = userLabelVOList.stream()
                    .peek(item -> item.setGroupName(labelGroupsToMap.get(item.getGroupId())))
                    .collect(Collectors.toList());
        }
        return userLabelVOList;
    }

    public List<CustomerLabelVO> listUserLabel(CustomerLabelQuery query) {
        List<CustomerLabelRelation> userLabelRelations = customerLabelRelationRepository
                .list(new LambdaQueryWrapper<CustomerLabelRelation>()
                        .eq(CustomerLabelRelation::getCustomerId, query.getCustomerId()));
        if (ObjectUtil.isNotEmpty(query.getCustomerId()) && ObjectUtil.isEmpty(userLabelRelations)) {
            return new ArrayList<>();
        }
        Set<String> labelIds = userLabelRelations.stream().map(CustomerLabelRelation::getLabelId)
                .collect(Collectors.toSet());
        List<CustomerLabel> list = customerLabelRepository.list(new LambdaQueryWrapper<CustomerLabel>()
                .eq(ObjectUtil.isNotEmpty(query.getGroupId()), CustomerLabel::getGroupId, query.getGroupId())
                .in(ObjectUtil.isNotEmpty(labelIds), CustomerLabel::getId, labelIds)
                .or()
                .like(ObjectUtil.isNotEmpty(query.getSearchInfo()), CustomerLabel::getLabelName, query.getSearchInfo())
        );

        Set<String> groupIds = list.stream().map(CustomerLabel::getGroupId).collect(Collectors.toSet());
        List<CustomerLabelGroup> labelGroups = customerLabelGroupRepository.list(new LambdaQueryWrapper<CustomerLabelGroup>()
                .in(ObjectUtil.isNotEmpty(groupIds), CustomerLabelGroup::getId, groupIds));
        Map<String, String> labelGroupsToMap = labelGroups.stream()
                .collect(Collectors.toMap(CustomerLabelGroup::getId, CustomerLabelGroup::getGroupName));
        List<CustomerLabelVO> userLabelVOList = BeanUtil.copyToList(list, CustomerLabelVO.class);

        return userLabelVOList.stream().peek(item -> item.setGroupName(labelGroupsToMap.get(item.getGroupId())))
                .collect(Collectors.toList());
    }

    public void save(CustomerLabelVO customerLabelVO) {
        if (ObjectUtil.isEmpty(customerLabelVO.getId())) {
            customerLabelVO.setCreateBy(ShiroManager.getUserId());
        }
        customerLabelVO.setUpdateBy(ShiroManager.getUserId());
        CustomerLabel userLabel = BeanUtil.copyProperties(customerLabelVO, CustomerLabel.class);
        customerLabelRepository.saveOrUpdate(userLabel);
    }

    @Transactional(rollbackFor = Exception.class)
    public void delete(DeleteParam param) {
        customerLabelRepository.removeById(param.getId());
        customerLabelRelationRepository.remove(new LambdaQueryWrapper<CustomerLabelRelation>()
                .eq(CustomerLabelRelation::getLabelId, param.getId()));
    }

    @Transactional(rollbackFor = Exception.class)
    public void bindUserLabel(UserBindLabelParam param) {
        customerLabelRelationRepository.remove(new LambdaUpdateWrapper<CustomerLabelRelation>()
                .eq(CustomerLabelRelation::getCustomerId, param.getCustomerId())
                .notIn(ObjectUtil.isNotEmpty(param.getLabelId()), CustomerLabelRelation::getLabelId, param.getLabelId())
        );

        List<CustomerLabelRelation> userLabelRelation = CustomerLabelRelation.build(param.getLabelId(), param.getCustomerId());
        customerLabelRelationRepository.saveOrUpdateBatch(userLabelRelation);
    }
}
