package com.whln.ordermax.api.service.whole;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.whln.ordermax.api.service.map.MapService;
import org.apache.ibatis.parsing.XNode;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.FlFlowBill;
import com.whln.ordermax.data.domain.FlFlowBillCommodity;
import com.whln.ordermax.data.repository.FlFlowBillCommodityRepository;
import com.whln.ordermax.data.repository.FlFlowBillRepository;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单进度业务处理
 */
@Service
public class BillFlowService {
    @Resource
    private FlFlowBillRepository flowBillRepository;

    @Resource
    private FlFlowBillCommodityRepository flowBillCommodityRepository;

    @Resource
    private SqlSessionFactory sqlSessionFactory;

    @Resource
    private MapCache mapCache;

    @Resource
    private ApplicationContext applicationContext;

    /**
     * 查询所有
     */
    public Result<List<FlFlowBill>> listBillFlowStatus(FlFlowBill entity) {
        if (!StrUtil.isAllNotEmpty(entity.getBillCode(), entity.getBillId())) {
            throw new BusinessException("缺少必要参数！");
        }
        return Result.success(flowBillRepository.listByEntity(entity));
    }

    public Result<List<Map>> listBillCommodityWithRemainNum(FlFlowBill entity) {
        if (!StrUtil.isAllNotEmpty(entity.getBillCode(), entity.getBillId())) {
            throw new BusinessException("缺少必要参数！");
        }
        String billCode = entity.getBillCode();
        Map<String, String> codeFullCommodityMapperMap = mapCache.codeFullCommodityMapperMap();
        String fullCommodityMapper = codeFullCommodityMapperMap.get(billCode);
        Configuration configuration = sqlSessionFactory.getConfiguration();
        Map<String, XNode> sqlFragments = configuration.getSqlFragments();
        XNode xNode = sqlFragments.get(fullCommodityMapper + ".Base_select");
        String tableBody = xNode.getStringBody();
        List<Map<String, Object>> resultList = flowBillRepository.listBillCommodityWithRemainNum(entity, tableBody);
        return Result.success(resultList.stream().map(m -> MapUtil.toCamelCaseMap(m)).collect(Collectors.toList()));
    }


    public Result<List<Map<String, Object>>> listBillCommodityWithRemainNum2(FlFlowBill entity) {
        try {
            if (!StrUtil.isAllNotEmpty(entity.getBillCode(), entity.getBillId())) {
                throw new BusinessException("缺少必要参数！");
            }
            Map<String, BigDecimal> idNumMap = flowBillCommodityRepository
                    .listByEntity(BeanUtil.copyProperties(entity, FlFlowBillCommodity.class))
                    .stream().collect(
                            Collectors.toMap(
                                    FlFlowBillCommodity::getBillCommodityDataId,
                                    FlFlowBillCommodity::getFlowRemainNum
                            )
                    );
            String billCode = entity.getBillCode();
            Map<String, String> codeFullCommodityRepositoryMap = mapCache.codeFullCommodityRepositoryMap();
            String fullRepositoryMapper = codeFullCommodityRepositoryMap.get(billCode);
            Object comRepository = applicationContext.getBean(Class.forName(fullRepositoryMapper));
            Object comEntity = new Object();
            ReflectUtil.setFieldValue(comEntity, "billId", entity.getBillId());
            List comList = ReflectUtil.invoke(comRepository, "listByEntity", comEntity);
            ArrayList<Map<String, Object>> resultList = new ArrayList<>();
            for (Object com : comList) {
                String comId = (String) ReflectUtil.getFieldValue(com, "id");
                BigDecimal remainNum = idNumMap.get(comId);
                if (remainNum.compareTo(BigDecimal.ZERO) > 0 || ObjectUtil.isEmpty(remainNum)) {
                    Map<String, Object> map = BeanUtil.beanToMap(com);
                    if (remainNum != null) {
                        map.put("flowRemainNum", remainNum);
                    } else {
                        map.put("flowRemainNum", ReflectUtil.getFieldValue(com, "quotation_num"));
                    }
                    resultList.add(map);
                }
            }
            return Result.success(resultList);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException("查询失败");
        }
    }

    public Result<List<Map<String, Object>>> listBillWithCommodity(JSONObject entity) {
        if (!StrUtil.isAllNotEmpty(entity.getStr("billCode"))) {
            throw new BusinessException("缺少必要参数！");
        }
        Configuration configuration = sqlSessionFactory.getConfiguration();
        Map<String, XNode> sqlFragments = configuration.getSqlFragments();

        String billCode = entity.getStr("billCode");
        Map<String, String> codeFullMapperMap = mapCache.codeFullMapperMap();
        String fullMapper = codeFullMapperMap.get(billCode);
        XNode mXNode = sqlFragments.get(fullMapper + ".Base_select");
        String mTableBody = mXNode.getStringBody();
        List<Map<String, Object>> billList = flowBillRepository.listNotFlowBill(entity, mTableBody);

        Map<String, String> codeFullCommodityMapperMap = mapCache.codeFullCommodityMapperMap();
        String fullCommodityMapper = codeFullCommodityMapperMap.get(billCode);
        XNode cmXNode = sqlFragments.get(fullCommodityMapper + ".Base_select");
        String cmTableBody = cmXNode.getStringBody();
        List<Map<String, Object>> commodityList = flowBillRepository.listBillCommodityWithRemainNum(entity.toBean(FlFlowBill.class), cmTableBody);
        Map<String, List<Map<String, Object>>> billIdCommodityMap = commodityList
                .stream()
                .map(m -> MapUtil.toCamelCaseMap(m))
                .collect(Collectors.groupingBy(m -> ObjectUtil.toString(m.get("billId"))));
        billList = billList.stream().map(m -> {
            m = MapUtil.toCamelCaseMap(m);
            m.put("commodityList", billIdCommodityMap.get(ObjectUtil.toString(m.get("id"))));
            return m;
        }).collect(Collectors.toList());
        return Result.success(billList);
    }
}