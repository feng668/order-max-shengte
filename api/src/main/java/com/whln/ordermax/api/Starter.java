package com.whln.ordermax.api;

import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author liurun
 * @date 2023-04-17 11:19
 */
@Slf4j
public class Starter extends JFrame {
    private static final long serialVersionUID = -6410618698178982213L;
    private Integer startStatus = -1;
    //启动按钮
    private final JButton startButton;
//    停止按钮
    private final JButton stopButton;

    private ConfigurableApplicationContext applicationContext;

    public Starter(final String[] args) {
        startButton = new JButton("启动");
        stopButton = new JButton("停止");
        this.setSize(new Dimension(400, 400));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
        startButton.setBounds(0, 0, 100, 100);
        stopButton.setBounds(105, 0, 100, 100);
        this.add(startButton);
        this.add(stopButton);
        this.setResizable(false);
        this.setTitle("服务端启动器");
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);

            }
        });
        addStartListener(args);
        stopButton.addActionListener(new AbstractAction() {
            private static final long serialVersionUID = 6130091734483994870L;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (ObjectUtil.isNotEmpty(applicationContext) && applicationContext.isRunning()) {
                    int i = JOptionPane.showConfirmDialog(null, "是否确认停止服务端?", "警告！", JOptionPane.YES_NO_OPTION);
                    System.out.println(i);
                    if (i == 0) {
                        applicationContext.close();
                        startStatus = -1;
                        startButton.setText("启动");
                    }
                }
            }
        });

    }

    private void addStartListener(String[] args) {
        startButton.addActionListener(new AbstractAction() {
            private static final long serialVersionUID = 6130091734483994870L;

            @Override
            public void actionPerformed(ActionEvent e) {
                System.setProperty("nashorn.args", "--no-deprecation-warning");
                if (ObjectUtil.equals(startStatus, 2) || ObjectUtil.equals(startStatus, 1)) {
                    log.warn("无需重复启动");
                    return;
                }
                startButton.setText("启动中");
                startStatus = 1;

                ExecutorService executorService = Executors.newFixedThreadPool(1);
                executorService.execute(() -> {
                    try {
                        applicationContext = SpringApplication.run(OrderMaxApiApplication.class, args);
                        startButton.setText("服务端运行中");
                        startStatus = 2;
                        openBrowse();
                    } catch (Exception ex) {
                        startButton.setText("启动");
                        startStatus = -1;
                    }

                });

            }
        });
    }

    public void openBrowse() {
        try {
            String url = "http://" + InetAddress.getLocalHost().getHostAddress();
            java.net.URI uri = java.net.URI.create(url);
            // 获取当前系统桌面扩展
            java.awt.Desktop dp = java.awt.Desktop.getDesktop();
            // 判断系统桌面是否支持要执行的功能
            if (dp.isSupported(java.awt.Desktop.Action.BROWSE)) {
                dp.browse(uri);
                // 获取系统默认浏览器打开链接
            }
        } catch (NullPointerException | IOException e1) {
            // 此为uri为空时抛出异常
            e1.printStackTrace();
        } // 此为无法获取系统默认浏览器

    }

    public static void main(String[] args) {
        new Starter(args);
    }
}
