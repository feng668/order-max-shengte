package com.whln.ordermax.api.service.sys.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.utils.TreeUtil;
import com.whln.ordermax.data.domain.SysDepartment;
import com.whln.ordermax.data.domain.SysUser;
import com.whln.ordermax.data.repository.SysDepartmentRepository;
import com.whln.ordermax.data.repository.SysUserRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 用户部门业务处理
 */
@Service
public class SysDepartmentService {
    @Resource
    private SysDepartmentRepository baseRepository;

    /**
     * 查询所有
     */
    public Result listAll(SysDepartment entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(SysDepartment entity, Page<SysDepartment> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    public Result save(SysDepartment entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    @Resource
    SysUserRepository sysUserRepository;

    /**
     * 批量删除
     */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result<List<SysDepartment>> treeData(String companyId) {
        if (ObjectUtil.isEmpty(companyId)) {
            String userId = ShiroManager.getUserId();
            companyId = sysUserRepository.getObj(new LambdaQueryWrapper<SysUser>().select(SysUser::getCompanyId).eq(SysUser::getId, userId), Objects::toString);
        }
        List<SysDepartment> list = baseRepository.list(new QueryWrapper<SysDepartment>().eq("sys_company_id", companyId).orderByDesc("parent_ids"));
        List<SysDepartment> tree = TreeUtil.getTree(list);
        return Result.success(tree);
    }

    public Boolean batchSave(List<SysDepartment> sysDepartmentList) {
        Boolean b = false;
        for (SysDepartment sysDepartment : sysDepartmentList) {
            b |= baseRepository.saveOrUpdate(sysDepartment);
        }
        return b;
    }
}