package com.whln.ordermax.api.designpattern.signhandler;

import org.springframework.stereotype.Component;
import com.whln.ordermax.data.domain.EmInbox;
import com.whln.ordermax.data.domain.param.EmailFlagParam;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liurun
 */
@Component
public class SignHandlerFactory {
    private static final List<EmailHandler> EMAIL_HANDLERS = new ArrayList<>();

    public static void register(EmailHandler handler) {
        EMAIL_HANDLERS.add(handler);
    }

    public void handler(EmInbox emInbox, EmailFlagParam param) {
        for (EmailHandler handler : EMAIL_HANDLERS) {
            handler.handler(emInbox, param);
        }
    }
}
