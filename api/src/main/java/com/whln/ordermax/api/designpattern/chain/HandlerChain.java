package com.whln.ordermax.api.designpattern.chain;

/**
 * 责任链
 * @author liurun
 * @date 2023-02-24 17:01
 */
public interface HandlerChain<T,R> {
    /**
     * 处理方法
     * @param orderPlans
     * @return
     */
    R handle(T orderPlans);

}
