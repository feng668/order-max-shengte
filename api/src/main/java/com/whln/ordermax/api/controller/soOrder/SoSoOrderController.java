package com.whln.ordermax.api.controller.soOrder;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.soOrder.SoSoOrderService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SoSoOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  销售合同控制器
 */
@Api(tags = "外销合同")
@RestController
@RequestMapping("/SoSoOrder")
public class SoSoOrderController{

    @Resource
    private SoSoOrderService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<SoSoOrder>> listAll(@RequestBody SoSoOrder entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
//    @PreAuthorize("hasAnyAuthority('SO_SO_ORDER:SHOW','AUTH:ADMIN')")
    public Result<IPage<SoSoOrder>> pageAll(@RequestBody PageEntity<SoSoOrder> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody()@Validated SoSoOrder entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<List<SoSoOrder>> getAllById(@RequestParam("id") String id){
        return baseService.getAllById(id);
    }

}