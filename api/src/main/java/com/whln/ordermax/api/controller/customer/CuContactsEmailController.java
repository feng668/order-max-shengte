package com.whln.ordermax.api.controller.customer;

import com.whln.ordermax.api.service.customer.CuContactsEmailService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CuContactsEmail;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  客户联系人邮箱控制器
 */
@Api(tags = "客户联系人邮箱")
@RestController
@RequestMapping("/CuContactsEmail")
public class CuContactsEmailController{

    @Resource
    private CuContactsEmailService baseService;

    @ApiOperation(value = "通过联系人id查询")
    @PostMapping("/list")
    public Result<List<CuContactsEmail>> listAll(@RequestBody CuContactsEmail entity){
        return baseService.listAll(entity);
    }

}