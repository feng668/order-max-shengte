package com.whln.ordermax.api.annotations;

import java.lang.annotation.*;

/**
 * @author zz
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface OrderPlanSave {
    int status() default 0;
}
