package com.whln.ordermax.api.controller.poOrder;

import com.whln.ordermax.api.service.poOrder.PoInvoiceCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.PoInvoiceCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  采购发票商品控制器
 */
@Api(tags = "采购发票商品")
@RestController
@RequestMapping("/PoInvoiceCommodity")
public class PoInvoiceCommodityController{

    @Resource
    private PoInvoiceCommodityService baseService;

    @ApiOperation(value = "根据发票id查询")
    @PostMapping("/list")
    public Result<List<PoInvoiceCommodity>> listAll(@RequestBody PoInvoiceCommodity entity){
        return baseService.listAll(entity);
    }

}