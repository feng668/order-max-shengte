package com.whln.ordermax.api.controller.supplier;

import com.whln.ordermax.api.service.supplier.SupContactsEmailService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SupContactsEmail;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  客户联系人邮箱控制器
 */
@Api(tags = "供应商联系人邮箱")
@RestController
@RequestMapping("/SupContactsEmail")
public class SupContactsEmailController{

    @Resource
    private SupContactsEmailService baseService;

    @ApiOperation(value = "通过联系人id查询联系人邮箱")
    @PostMapping("/listByContactsId")
    public Result<List<SupContactsEmail>> listByContactsId(@RequestBody SupContactsEmail entity){
        return baseService.listAll(entity);
    }

}