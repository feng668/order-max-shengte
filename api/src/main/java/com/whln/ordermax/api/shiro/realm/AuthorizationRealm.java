//package com.whln.ordermax.api.shiro.realm;
//
//
//import cn.hutool.core.util.ObjectUtil;
//import com.whln.ordermax.data.domain.SysPower;
//import com.whln.ordermax.data.domain.SysRole;
//import com.whln.ordermax.data.domain.SysUser;
//import com.whln.ordermax.data.repository.SysPowerRepository;
//import com.whln.ordermax.data.repository.SysRoleRepository;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.shiro.authc.AuthenticationException;
//import org.apache.shiro.authc.AuthenticationInfo;
//import org.apache.shiro.authc.AuthenticationToken;
//import org.apache.shiro.authz.AuthorizationInfo;
//import org.apache.shiro.authz.SimpleAuthorizationInfo;
//import org.apache.shiro.realm.AuthorizingRealm;
//import org.apache.shiro.subject.PrincipalCollection;
//
//import javax.annotation.Resource;
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
///**
// * 统一角色授权控制策略
// */
//@Slf4j
//public class AuthorizationRealm extends AuthorizingRealm {
//    @Resource
//    SysRoleRepository sysRoleRepository;
//
//    @Resource
//    SysPowerRepository sysPowerRepository;
//
//    /**
//     * 授权
//     */
//    @Override
//    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
//        log.info("---------------- 执行 Shiro 权限获取 ---------------------");
//        Object principal = principals.getPrimaryPrincipal();
//        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
//        if (principal instanceof SysUser) {
//            SysUser sysUser = (SysUser) principal;
//            if (ObjectUtil.isNotEmpty(sysUser)) {
//                List<SysRole> roleList = sysRoleRepository.listRolesByUserId(sysUser.getId());
//
//                if (ObjectUtil.isNotEmpty(roleList)) {
//                    Set<String> roles = roleList.stream().map(SysRole::getCode).collect(Collectors.toSet());
//                    info.addRoles(roles);
//                }
//                List<SysPower> sysPowers = sysPowerRepository.listPowersByUserId(sysUser.getId());
//                if (ObjectUtil.isNotEmpty(sysPowers)) {
//                    Set<String> collect = sysPowers.stream().map(SysPower::getPermission).collect(Collectors.toSet());
//                    info.addStringPermissions(collect);
//                }
//            }
//
//            log.info("用户{}获取到以下权限:{}",sysUser.getUsername(),info.getStringPermissions());
//            log.info("用户{}获取到以下角色:{}",sysUser.getUsername(),info.getRoles());
//        }
//
//        return info;
//    }
//
//    /**
//     * 认证信息.(身份验证) : Authentication 是用来验证用户身份
//     */
//    @Override
//    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
//        return null;
//    }
//}
