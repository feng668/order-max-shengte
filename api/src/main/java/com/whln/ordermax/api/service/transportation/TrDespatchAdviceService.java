package com.whln.ordermax.api.service.transportation;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.TrDespatchAdviceCommodityMapper;
import com.whln.ordermax.data.repository.TrDespatchAdviceCommodityRepository;
import com.whln.ordermax.data.repository.TrDespatchAdviceRepository;
import com.whln.ordermax.data.repository.TrTransportationCostRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 发运通知单业务处理
 */
@BillCode(ModelMessage.TR_DESPATCH_ADVICE)
@Service
public class TrDespatchAdviceService {

    @Resource
    private TrDespatchAdviceRepository baseRepository;

    @Resource
    private TrDespatchAdviceCommodityRepository commodityRepository;
    @Resource
    private TrTransportationCostRepository costRepository;

    /**
     * 查询所有
     */
    public Result listAll(TrDespatchAdvice entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    @Resource
    private TrDespatchAdviceCommodityMapper trDespatchAdviceCommodityMapper;

    /**
     * 分页查询所有
     */
    public Result pageAll(TrDespatchAdvice entity, Page<TrDespatchAdvice> page) {
        IPage<TrDespatchAdvice> trDespatchAdviceIPage = baseRepository.pageByEntity(entity, page);
        List<TrDespatchAdvice> records = trDespatchAdviceIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(TrDespatchAdvice::getId).collect(Collectors.toSet());
            BillQuery param = new BillQuery();
            param.setBillIds(billIds);
            List<TrDespatchAdviceCommodity> trDespatchAdviceCommodities = trDespatchAdviceCommodityMapper.commodityList(param);
            Map<String, List<TrDespatchAdviceCommodity>> collect = trDespatchAdviceCommodities
                    .stream()
                    .collect(Collectors.groupingBy(TrDespatchAdviceCommodity::getBillId));
            List<TrTransportationCost> trTransportationCosts = costRepository.list(new LambdaQueryWrapper<TrTransportationCost>().in(TrTransportationCost::getTransportationId, billIds));
            Map<String, List<TrTransportationCost>> trTransportationMapByBillId = trTransportationCosts.stream().collect(Collectors.groupingBy(TrTransportationCost::getTransportationId));
            for (TrDespatchAdvice trDespatchAdvice : records) {
                trDespatchAdvice.setCommodityList(collect.get(trDespatchAdvice.getId()));
                trDespatchAdvice.setCostList(trTransportationMapByBillId.get(trDespatchAdvice.getId()));
            }
            trDespatchAdviceIPage.setRecords(records);

        }


        return Result.success(trDespatchAdviceIPage);
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    @FlowControlSave()
    public Result save(TrDespatchAdvice entity) {
        TrDespatchAdvice t = baseRepository.getOne(new LambdaQueryWrapper<TrDespatchAdvice>().eq(TrDespatchAdvice::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(t) && !ObjectUtil.equals(t.getId(), entity.getId())) {
            throw new BusinessException("单据编号已存在！");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<TrDespatchAdviceCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream().map(TrDespatchAdviceCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<TrDespatchAdviceCommodity>()
                .eq(TrDespatchAdviceCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(idList), TrDespatchAdviceCommodity::getId, idList));

        if (CollectionUtil.isNotEmpty(commodityList)) {
            b &= commodityRepository.saveOrUpdateBatch(commodityList);
        }
        List<TrTransportationCost> costList = entity.getCostList();
        if (CollectionUtil.isNotEmpty(costList)) {
            List<TrTransportationCost> collect = costList.stream().peek(e -> {
                if(ObjectUtil.equals(e.getTransportationType(),1)&&ObjectUtil.equals(e.getTransportationType(),2)){
                    throw new BusinessException("出运类型错误");
                }
                e.setTransportationId(entity.getId());
                e.setTransportationNo(entity.getBillNo());
            }).collect(Collectors.toList());
            costRepository.saveOrUpdateBatch(collect);
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        TrDespatchAdvice one = baseRepository.getOne(new LambdaQueryWrapper<TrDespatchAdvice>().eq(TrDespatchAdvice::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            commodityRepository.remove(new QueryWrapper<TrDespatchAdviceCommodity>().eq("bill_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<TrDespatchAdvice>().eq(TrDespatchAdvice::getId, id).set(TrDespatchAdvice::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<TrDespatchAdvice> customsDeclarations = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = customsDeclarations.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = customsDeclarations.stream()
                .map(TrDespatchAdvice::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());
        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new LambdaQueryWrapper<TrDespatchAdviceCommodity>().in(TrDespatchAdviceCommodity::getBillId, ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<TrDespatchAdvice>().in(TrDespatchAdvice::getId, ids).set(TrDespatchAdvice::getIsDelete, 1));
            }
        }
        return Result.success();
    }

}