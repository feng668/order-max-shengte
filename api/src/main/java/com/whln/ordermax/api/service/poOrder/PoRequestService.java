package com.whln.ordermax.api.service.poOrder;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.annotations.OaBillSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.PoRequestCommodityMapper;
import com.whln.ordermax.data.repository.DynamicRepository;
import com.whln.ordermax.data.repository.PoRequestCommodityRepository;
import com.whln.ordermax.data.repository.PoRequestRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 采购申请单业务处理
 */
@BillCode(ModelMessage.PO_REQUEST)
@Service
public class PoRequestService {
    @Resource
    private PoRequestRepository baseRepository;

    @Resource
    private PoRequestCommodityRepository commodityRepository;

    @Resource
    private DynamicRepository dynamicRepository;

    @Resource
    private PoRequestCommodityMapper poRequestCommodityMapper;
    /**
     * 查询所有
     */
    public Result listAll(PoRequest entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }



    /**
     * 分页查询所有
     */
    public Result pageAll(PoRequest entity, Page<PoRequest> page) {
        IPage<PoRequest> poRequestIPage = baseRepository.pageByEntity(entity, page);
        List<PoRequest> records = poRequestIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(PoRequest::getId).collect(Collectors.toSet());
            BillQuery param = new BillQuery();
            param.setBillIds(billIds);
            List<PoRequestCommodity> poRequestCommodities = poRequestCommodityMapper.commodityList(param);
            Map<String, List<PoRequestCommodity>> collect =
                    poRequestCommodities.stream()
                            .collect(Collectors.groupingBy(PoRequestCommodity::getBillId));
            for (PoRequest p : records) {
                p.setCommodityList(collect.get(p.getId()));
            }
            poRequestIPage.setRecords(records);
        }
        return Result.success(poRequestIPage);
    }

    /**
     * 插入或更新
     */
    @OaBillSave
    @Transactional(rollbackFor = Exception.class)
    @FlowControlSave(2)
    public Result save(PoRequest entity) {
        PoRequest poRequest = baseRepository.getOne(new LambdaQueryWrapper<PoRequest>().eq(PoRequest::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(poRequest)&&!ObjectUtil.equals(entity.getId(),poRequest.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<PoRequestCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream()
                .map(PoRequestCommodity::getId)
                .filter(ObjectUtil::isNotEmpty)
                .collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<PoRequestCommodity>()
                .eq(PoRequestCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(idList), PoRequestCommodity::getId, idList));
        if (CollectionUtil.isNotEmpty(commodityList)) {
            commodityRepository.saveOrUpdateBatch(commodityList);
        }
        if (StrUtil.isNotEmpty(entity.getSourceBillId()) && CollectionUtil.isNotEmpty(entity.getCommodityFlowIdList())) {
            dynamicRepository.batchUpdateBillComFlowStatus(entity.getCommodityFlowIdList(), "so_so_order_commodity", 1);
            dynamicRepository.updateBillFlowStatusByComFlow(entity.getSourceBillId(), "so_so_order", "so_so_order_commodity", 1);
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        PoRequest one = baseRepository.getOne(new LambdaQueryWrapper<PoRequest>().eq(PoRequest::getId, id));
        if ((ShiroManager.hasRole("webadmin")||ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId()))&& ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            commodityRepository.remove(new QueryWrapper<PoRequestCommodity>().eq("bill_id", id));
        }else {
            baseRepository.update(new LambdaUpdateWrapper<PoRequest>().eq(PoRequest::getId, id).set(PoRequest::getIsDelete,1));
        }
        return  Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<PoRequest> enquiryOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = enquiryOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = enquiryOrders.stream()
                .map(PoRequest::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new LambdaQueryWrapper<PoRequestCommodity>().in(PoRequestCommodity::getBillId, ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<PoRequest>().in(PoRequest::getId, ids).set(PoRequest::getIsDelete, 1));
            }
        }
        return Result.success();
    }

}