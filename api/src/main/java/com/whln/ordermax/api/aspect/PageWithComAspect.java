package com.whln.ordermax.api.aspect;

import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.service.MapCache;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.base.ComFilterOaBill;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.ApplicationContext;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 携带商品展示拦截
 *
 */
@Aspect
@Component
@Order(1)
public class PageWithComAspect {

    @Resource
    private ApplicationContext applicationContext;


    @Pointcut("execution(* com.whln.ordermax.api.service..*.pageAll(..))")
    public void point() {
    }

    @Before("point()")
    public void before(JoinPoint joinPoint) throws Exception {
    }


    @After("point()")
    public void after(JoinPoint joinPoint) {
    }

//    @Around("point()")
    public Result around(ProceedingJoinPoint pjp) throws Throwable{
        Object[] args = pjp.getArgs();
        LocalVariableTableParameterNameDiscoverer discoverer = new LocalVariableTableParameterNameDiscoverer();
        String[] paramNames = discoverer.getParameterNames(((MethodSignature) pjp.getSignature()).getMethod());
        String commodityEntityClassName = "";
        for (int i = 0; i < paramNames.length; i++) {
            if ("entity".equals(paramNames[i])) {
                Object entity = args[i];
                if (!(entity instanceof ComFilterOaBill)) {
                    return (Result)pjp.proceed();
                }
                MapCache mapService = applicationContext.getBean(MapCache.class);
                Map<String, String> entityComEntityMap = mapService.entityComEntityMap();
                commodityEntityClassName = entityComEntityMap.get(entity.getClass().getSimpleName());
            }
        }
        //运行doSth()，返回值用一个Object类型来接收
        Result<Page<ComFilterOaBill>> result = (Result<Page<ComFilterOaBill>>)pjp.proceed();
        Page<ComFilterOaBill> page = result.getData();
        List<String> idList = page.getRecords().stream().map(ComFilterOaBill::getId).collect(Collectors.toList());

        if (idList.size()>0){
            Object queryParam = ReflectUtil.newInstance(commodityEntityClassName);
            ReflectUtil.invoke(queryParam,"setBillIdIn",idList);
        }

        ReflectUtil.setFieldValue(result,"data",page);
        return result;
    }
}