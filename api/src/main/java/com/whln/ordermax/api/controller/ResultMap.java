package com.whln.ordermax.api.controller;

import com.whln.ordermax.data.domain.SysPower;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-11 17:59
 */
@Data
public class ResultMap {
    List<FieldInfo> list;
    List<SysPower> powers;
}
