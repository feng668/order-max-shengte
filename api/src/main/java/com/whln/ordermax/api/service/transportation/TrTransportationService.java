package com.whln.ordermax.api.service.transportation;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.service.customer.CuBigCustomerNormService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.utils.LogisticsUtil;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.TrTransportationCommodityMapper;
import com.whln.ordermax.data.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 出运单业务处理
 */
@BillCode(ModelMessage.TR_TRANSPORTATION)
@Service
public class TrTransportationService {
    @Resource
    private TrTransportationRepository transportationRepository;

    @Resource
    private TrTransportationCommodityRepository trTransportationCommodityRepository;

    @Resource
    private TrTransportationCostRepository transportationCostRepository;
    @Resource
    private TrTransportationCommodityMapper trTransportationCommodityMapper;
    @Resource
    private DynamicRepository dynamicRepository;
    @Resource
    private CuBigCustomerNormService bigCustomerNormService;
    @Resource
    private PoContractCommodityRepository poContractCommodityRepository;

    @Resource
    private OrderPlanRepository orderPlanRepository;

    /**
     * 查询所有
     */
    public Result listAll(TrTransportation entity) {
        return Result.success(transportationRepository.listByEntity(entity));
    }


    /**
     * 分页查询所有
     */

    public Result pageAll(TrTransportation entity, Page<TrTransportation> page) {
        IPage<TrTransportation> pageResult = transportationRepository.pageByEntity(entity, page);
        List<TrTransportation> records = pageResult.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(TrTransportation::getId).collect(Collectors.toSet());
            BillQuery queryByBillIdParam = new BillQuery();
            queryByBillIdParam.setBillIds(billIds);

            List<TrTransportationCommodity> trTransportationCommodities = trTransportationCommodityMapper.commodityList(queryByBillIdParam);
            Map<String, List<TrTransportationCommodity>> groupByBillId = trTransportationCommodities.stream().collect(Collectors.groupingBy(TrTransportationCommodity::getBillId));
            List<TrTransportation> result = records.stream().peek(item -> {
                item.setCommodityList(groupByBillId.get(item.getId()));
            }).collect(Collectors.toList());
            pageResult.setRecords(result);
        }
        return Result.success(pageResult);
    }


    /**
     * 插入或更新
     */

    @Transactional(rollbackFor = Exception.class)
    @FlowControlSave(3)
    public Result save(TrTransportation entity) {
        TrTransportation t = transportationRepository.getOne(new LambdaQueryWrapper<TrTransportation>().eq(TrTransportation::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(t)&&!ObjectUtil.equals(t.getId(),entity.getId())) {
            throw new BusinessException("单据编号已存在！");
        }
        boolean b = transportationRepository.saveOrUpdate(entity);
        List<TrTransportationCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream()
                .map(TrTransportationCommodity::getId)
                .filter(ObjectUtil::isNotEmpty)
                .collect(Collectors.toList());
        trTransportationCommodityRepository.remove(new LambdaUpdateWrapper<TrTransportationCommodity>()
                .eq(TrTransportationCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(idList), TrTransportationCommodity::getId, idList)
        );

        if (CollectionUtil.isNotEmpty(commodityList)) {
            trTransportationCommodityRepository.saveOrUpdateBatch(entity.getCommodityList());
        }
        transportationCostRepository.remove(new LambdaUpdateWrapper<TrTransportationCost>().eq(TrTransportationCost::getTransportationId, entity.getId()));
        List<TrTransportationCost> costList = entity.getCostList();
        if (CollectionUtil.isNotEmpty(costList)) {
            List<TrTransportationCost> collect = costList.stream().peek(e -> {
                if(ObjectUtil.equals(e.getTransportationType(),1)&&ObjectUtil.equals(e.getTransportationType(),2)){
                    throw new BusinessException("出运类型错误");
                }
                e.setTransportationNo(entity.getBillNo());
                e.setTransportationId(entity.getId());
            }).collect(Collectors.toList());
            transportationCostRepository.saveOrUpdateBatch(collect);
        }
        if (StrUtil.isNotEmpty(entity.getSourceBillIds()) && CollectionUtil.isNotEmpty(entity.getCommodityFlowIdList())) {
            dynamicRepository.batchUpdateBillComFlowStatus(entity.getCommodityFlowIdList(), "so_so_order_commodity", 3);
            Optional.ofNullable(entity.getSourceBillIds()).ifPresent(s -> {
                for (String sourceBillId : StrUtil.split(s, ",")) {
                    dynamicRepository.updateBillFlowStatusByComFlow(sourceBillId, "so_so_order", "so_so_order_commodity", 3);
                }
            });
        }
        //进行大客户验证
        bigCustomerNormService.judgeBigCustomer(entity.getCustomerId());
//        this.judgeBigCustomer(entity.getCustomerId());
        return b ? Result.success() : Result.error("201", "保存失败");
    }


    /**
     * 单条删除
     */
    @Transactional
    public Result delete(String id) {
        TrTransportation one = transportationRepository.getOne(new LambdaQueryWrapper<TrTransportation>().eq(TrTransportation::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            transportationRepository.removeById(id);
            trTransportationCommodityRepository.remove(new QueryWrapper<TrTransportationCommodity>().eq("bill_id", id));
            transportationCostRepository.remove(new QueryWrapper<TrTransportationCost>().eq("transportation_id", id));
        } else {
            transportationRepository.update(new LambdaUpdateWrapper<TrTransportation>().eq(TrTransportation::getId, id).set(TrTransportation::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional
    public Result batchDelete(List<String> ids) {
        List<TrTransportation> customsDeclarations = transportationRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = customsDeclarations.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = customsDeclarations.stream()
                .map(TrTransportation::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());
        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                boolean b = transportationRepository.removeByIds(ids);
                trTransportationCommodityRepository.remove(new LambdaQueryWrapper<TrTransportationCommodity>().in(TrTransportationCommodity::getBillId, ids));
                transportationCostRepository.remove(new LambdaQueryWrapper<TrTransportationCost>().in(TrTransportationCost::getTransportationId, ids));
            } else {
                transportationRepository.update(new LambdaUpdateWrapper<TrTransportation>().in(TrTransportation::getId, ids).set(TrTransportation::getIsDelete, 1));
            }
        }
        return Result.success();
    }

    public Result<List<TrTransportationCost>> listCost(String id) {
        return Result.success(
                transportationCostRepository.listByEntity(
                        new TrTransportationCost() {{
                            setTransportationId(id);
                        }}
                )
        );
    }

    public Result<List<JSONObject>> traceSearch(TrTransportation entity) {
        if (StrUtil.isEmpty(entity.getCpCode())) {
            return Result.error("201", "缺少物流公司");
        }
        if (StrUtil.isEmpty(entity.getTraceNo())) {
            return Result.error("201", "缺少运单号");
        }
        return Result.success(
                new LogisticsUtil("salvsojatbiwp6fzvqq3j9uixo2pjv7q")
                        .traceSearch(entity.getCpCode(), entity.getTraceNo())
        );
    }

    public Result<List<JSONObject>> traceSearchInternational(TrTransportation entity) {
        if (StrUtil.isEmpty(entity.getCpCode())) {
            return Result.error("201", "缺少物流公司");
        }
        if (StrUtil.isEmpty(entity.getTraceNo())) {
            return Result.error("201", "缺少运单号");
        }
        return Result.success(
                new LogisticsUtil("salvsojatbiwp6fzvqq3j9uixo2pjv7q")
                        .traceSearchInternational(entity.getCpCode(), entity.getTraceNo())
        );
    }


    public Result createProfit(String transportationId) {
        TrTransportation transportation = transportationRepository.getAllById(transportationId);
        //获取出运商品
        List<TrTransportationCommodity> trCommodityList = trTransportationCommodityRepository.listByEntity(new TrTransportationCommodity() {{
            setBillId(transportationId);
        }});
        Map<String, BigDecimal> trCommodityNums = new HashMap<>();
        List<String> trCommodityIds = trCommodityList.stream().map(c -> {
            trCommodityNums.put(c.getCommodityId(), c.getQuotationNum());
            return c.getCommodityId();
        }).collect(Collectors.toList());

        //获取进度
        Map<String, List<String>> billIdGroup = orderPlanRepository.listByBill(new OrderPlan() {{
            setBillId(transportationId);
            setBillCode("TR_TRANSPORTATION");
        }}).stream().collect(Collectors.groupingBy(OrderPlan::getBillCode, Collectors.mapping(OrderPlan::getBillId, Collectors.toList())));
        List<String> poContractIdList = billIdGroup.get("PO_CONTRACT");
        if (CollectionUtil.isEmpty(poContractIdList)) {
            return Result.error("201", "缺少采购合同进度");
        }
        //获取采购商品
        List<PoContractCommodity> poCommodityList = poContractCommodityRepository.list(
                new QueryWrapper<PoContractCommodity>()
                        .in("bill_id", poContractIdList)
                        .in("commodity_id", trCommodityIds)
        );
        //获取其他费用合计
        List<TrTransportationCost> costList = transportationCostRepository.list(
                new QueryWrapper<TrTransportationCost>()
                        .eq("transportation_id", transportationId)
        );
        List<String> costIdList = new ArrayList();
        Double costAmo = costList.stream().map(c -> {
            costIdList.add(c.getId());
            return c.getLocalCurrencyAmo();
        }).mapToDouble(c -> c).sum();
        BigDecimal purchaseAmo = poCommodityList.stream().map(p -> p.getPrice().multiply(trCommodityNums.get(p.getCommodityId()))).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal saleAmo = trCommodityList.stream().map(TrTransportationCommodity::getAmo).reduce(BigDecimal.ZERO, BigDecimal::add);
        FnProfit fnProfit = new FnProfit();
        fnProfit.setBillNo(transportation.getBillNo());
        fnProfit.setTransportationId(transportationId);
        fnProfit.setPurchaseAmo(purchaseAmo);
        fnProfit.setSaleAmo(saleAmo);
        fnProfit.setCostAmo(BigDecimal.valueOf(costAmo));
        fnProfit.setCustomerId(transportation.getCustomerId());
        fnProfit.setBindingBill(new JSONObject() {{
            set("PO_CONTRACT", poContractIdList);
            set("TRANSPORTATION_ID", transportationId);
            set("COST", costIdList);
        }});
        return Result.success(fnProfit);
    }

    public Result<List<TrTransportationCost>> listCostByIds(List<String> ids) {
        return Result.success(
                transportationCostRepository.listByEntity(
                        new TrTransportationCost() {{
                            setIdIn(ids);
                        }}
                )
        );
    }
}