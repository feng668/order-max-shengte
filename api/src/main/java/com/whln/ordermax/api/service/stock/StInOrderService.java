package com.whln.ordermax.api.service.stock;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.CmCommodity;
import com.whln.ordermax.data.domain.StInOrder;
import com.whln.ordermax.data.domain.StInOrderCommodity;
import com.whln.ordermax.data.domain.StInventoryRemain;
import com.whln.ordermax.data.domain.param.BaseCommodityNumParam;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.domain.vo.BaseInfoVO;
import com.whln.ordermax.data.domain.vo.CmCommodityVO;
import com.whln.ordermax.data.domain.vo.CommodityBaseNumVO;
import com.whln.ordermax.data.mapper.StInOrderCommodityMapper;
import com.whln.ordermax.data.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 入库单业务处理
 */
@Slf4j
@BillCode(ModelMessage.ST_IN_ORDER)
@Service
public class StInOrderService {
    @Resource
    private StInOrderRepository baseRepository;

    @Resource
    private StInOrderCommodityRepository stInOrderCommodityRepository;

    @Resource
    private StInventoryRemainRepository inventoryRemainRepository;
    @Resource
    private StInOrderCommodityMapper stInOrderCommodityMapper;
    @Resource
    private StOutOrderCommodityRepository stOutOrderCommodityRepository;
    @Resource
    private StWarehouseRepository stWarehouseRepository;

    /**
     * 查询所有
     */
    public Result listAll(StInOrder entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }


    /**
     * 分页查询所有
     */
    public Result pageAll(StInOrder entity, Page<StInOrder> page) {
        IPage<StInOrder> stInOrderIPage = baseRepository.pageByEntity(entity, page);
        List<StInOrder> records = stInOrderIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> collect = records.stream().map(StInOrder::getId).collect(Collectors.toSet());
            BillQuery param = new BillQuery();
            param.setBillIds(collect);
            List<StInOrderCommodity> stOutOrderCommodities = stInOrderCommodityMapper.commodityList(param);
            Map<String, List<StInOrderCommodity>> groupByBillId =
                    stOutOrderCommodities.stream().collect(Collectors.groupingBy(StInOrderCommodity::getBillId));
            for (StInOrder s : records) {
                s.setCommodityList(groupByBillId.get(s.getId()));
            }
            stInOrderIPage.setRecords(records);
        }
        return Result.success(stInOrderIPage);
    }



    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    @FlowControlSave(status = 1)
    public Result save(StInOrder entity) {
        StInOrder stInOrder = baseRepository.getOne(new LambdaQueryWrapper<StInOrder>().eq(StInOrder::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(stInOrder) && !ObjectUtil.equals(entity.getId(), stInOrder.getId())) {
            throw new BusinessException("编号已存在");
        }
        if (ObjectUtil.isEmpty(entity.getProceedDate())) {
            entity.setProceedDate(LocalDate.now());
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<String> commodityIdList = new ArrayList<>();
        List<StInOrderCommodity> stInOrderCommodityList = new ArrayList<>();
        List<StInOrderCommodity> commodityList = entity.getCommodityList();
        List<String> billCommodityIdList = commodityList.stream()
                .filter(ObjectUtil::isNotEmpty)
                .map(StInOrderCommodity::getCommodityId)
                .collect(Collectors.toList());
        stInOrderCommodityRepository.remove(new LambdaUpdateWrapper<StInOrderCommodity>()
                .eq(StInOrderCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(billCommodityIdList), StInOrderCommodity::getCommodityId, billCommodityIdList)
        );
        if (CollectionUtil.isNotEmpty(commodityList)) {

            for (StInOrderCommodity commodity : commodityList) {
                if (ObjectUtil.isEmpty(commodity.getWarehouseId())) {
                    throw new BusinessException("有商品仓库为空");
                }
                if (ObjectUtil.isEmpty(commodity.getCommodityBatchNo())) {
                    throw new BusinessException("有商品批号为空");
                }
                commodity.setBillId(entity.getId());
                stInOrderCommodityList.add(commodity);
                commodityIdList.add(commodity.getCommodityId());
            }
            stInOrderCommodityRepository.saveOrUpdateBatch(stInOrderCommodityList);
            List<StInventoryRemain> stInventoryRemainList = inventoryRemainRepository
                    .listRemainNumByOrder(commodityIdList, entity.getWarehouseId());
            inventoryRemainRepository.saveOrUpdateBatchByUnique(stInventoryRemainList);
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional
    public Result delete(String id) {
        StInOrder one = baseRepository.getOne(new LambdaQueryWrapper<StInOrder>().eq(StInOrder::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            stInOrderCommodityRepository.remove(new QueryWrapper<StInOrderCommodity>().eq("bill_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<StInOrder>().eq(StInOrder::getId, id).set(StInOrder::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional
    public Result batchDelete(List<String> ids) {
        List<StInOrder> soSoOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = soSoOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = soSoOrders.stream()
                .map(StInOrder::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                stInOrderCommodityRepository.remove(new LambdaQueryWrapper<StInOrderCommodity>().in(StInOrderCommodity::getBillId, ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<StInOrder>().in(StInOrder::getId, ids).set(StInOrder::getIsDelete, 1));
            }
        }
        return Result.success();
    }

    @Resource
    StInventoryRepository stInventoryRepository;
    @Resource
    CmCommodityRepository commodityRepository;

    public Result<CommodityBaseNumVO> getCommodityBatchNo(BaseCommodityNumParam param) {
        StInventoryRemain stInventoryRemain = new StInventoryRemain();
        stInventoryRemain.setCommodityId(param.getCommodityId());
        stInventoryRemain.setSearchInfo(param.getSearchInfo());
        Page<BaseInfoVO> baseInfoVOPage = stInventoryRepository.baseInfo(stInventoryRemain, null);
        List<BaseInfoVO> records = baseInfoVOPage.getRecords();
        if (ObjectUtil.isEmpty(records)) {
            return Result.success();
        }
        List<String> commodityIdList = records.stream().map(BaseInfoVO::getCommodityId).collect(Collectors.toList());
        List<CmCommodity> commodityList = commodityRepository.list(new LambdaQueryWrapper<CmCommodity>().in(CmCommodity::getId, commodityIdList));
        Map<String, CmCommodity> commodityToMap = commodityList.stream().collect(Collectors.toMap(CmCommodity::getId, Function.identity()));
        List<StInOrderCommodity> list = stInOrderCommodityRepository.list(new LambdaQueryWrapper<StInOrderCommodity>().in(StInOrderCommodity::getCommodityId, commodityIdList));
        Map<String, StInOrderCommodity> collect1 = list.stream().collect(Collectors.toMap(StInOrderCommodity::getCommodityId, Function.identity(), (x, y) -> x));
        List<CmCommodityVO> resultList = records.stream().map(e -> {
            CmCommodity cmCommodity = commodityToMap.get(e.getCommodityId());
            CmCommodityVO commodityVO = BeanUtil.copyProperties(cmCommodity, CmCommodityVO.class);
            StInOrderCommodity stInOrderCommodity = collect1.get(e.getCommodityId());
            if (ObjectUtil.isNotEmpty(stInOrderCommodity)) {
                commodityVO.setSingleBoxNet(stInOrderCommodity.getSingleBoxNet());
                commodityVO.setSingleBoxVolume(stInOrderCommodity.getSingleBoxVolume());
                commodityVO.setSingleBoxWeight(stInOrderCommodity.getSingleBoxWeight());
            }
            commodityVO.setWarehouseId(e.getWarehouseId());
            commodityVO.setWarehouseNo(e.getWarehouseNo());
            commodityVO.setWarehouseName(e.getWarehouseName());
            commodityVO.setQuotationNum(e.getFreeNum());
            commodityVO.setCommodityBatchNo(e.getCommodityBatchNo());
            return commodityVO;
        }).collect(Collectors.toList());
        int total = resultList.size();
        List<CmCommodityVO> collect = resultList.stream().skip((param.getCurrent() - 1) * param.getSize()).limit(param.getSize()).collect(Collectors.toList());
        CommodityBaseNumVO build = CommodityBaseNumVO.builder().total(total).list(collect).build();

        return Result.success(build);
    }

}