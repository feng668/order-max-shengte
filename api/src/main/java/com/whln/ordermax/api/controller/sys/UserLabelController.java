package com.whln.ordermax.api.controller.sys;

import com.whln.ordermax.api.service.sys.impl.CustomerLabelService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.param.DeleteParam;
import com.whln.ordermax.data.domain.param.UserBindLabelParam;
import com.whln.ordermax.data.domain.query.CustomerLabelQuery;
import com.whln.ordermax.data.domain.vo.CustomerLabelVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 15:46
 */
@Api(tags = "客户标签操作")
@RestController
@AllArgsConstructor
@Validated
@RequestMapping("/userLabel")
public class UserLabelController {
    private final CustomerLabelService userLabelService;

    @ApiOperation("客户标签列表")
    @PostMapping("/list")
    public Result<List<CustomerLabelVO>> list(@RequestBody CustomerLabelQuery param) {
        List<CustomerLabelVO> list = userLabelService.listUserLabel(param);
        return Result.success(list);
    }


    @ApiOperation("删除客户标签")
    @ApiImplicitParam(value = "ids 数组", example = "['324324','42342']")
    @PostMapping("/deleteUserLabel")
    public Result<List<CustomerLabelVO>> deleteUserLabel(@RequestBody DeleteParam param) {
        userLabelService.delete(param);
        return Result.success();
    }

    @ApiOperation("保存客户标签")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody CustomerLabelVO customerLabelVO) {
        userLabelService.save(customerLabelVO);
        return Result.success();
    }

    @ApiOperation("用户绑定标签")
    @PostMapping("/bindUserLabel")
    public Result<Void> bindUserLabel(@RequestBody @Validated UserBindLabelParam param) {
        userLabelService.bindUserLabel(param);
        return Result.success();
    }
}
