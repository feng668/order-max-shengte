package com.whln.ordermax.api.controller.physical;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.physical.PhyForwarderCompanyService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.PhyCompanyContacts;
import com.whln.ordermax.data.domain.PhyForwarderCompany;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 货代公司控制器
 * @author liurun
 */
@Api(tags = "货代公司")
@RestController
@RequestMapping("/forwarderCompany")
public class PhyForwarderCompanyController {

    @Resource
    private PhyForwarderCompanyService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<PhyForwarderCompany>> listAll(@RequestBody PhyForwarderCompany entity) {
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<PhyForwarderCompany>> pageAll(@RequestBody PageEntity<PhyForwarderCompany> pageEntity) {
        return baseService.pageAll(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody PhyForwarderCompany entity) {
        return baseService.save(entity);
    }
    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id) {
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody List<String> ids) {
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "查询")
    @PostMapping("/listContactsByCompanyId")
    public Result<List<PhyCompanyContacts>> listContactsByCompanyId(@RequestParam("companyId") String companyId) {
        return baseService.listContactsByCompanyId(companyId);
    }

}