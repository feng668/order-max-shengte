package com.whln.ordermax.api.service.commodity;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CmPriceLevel;
import com.whln.ordermax.data.domain.QoQuoteCommodityPrice;
import com.whln.ordermax.data.repository.CmPriceLevelRepository;
import com.whln.ordermax.data.repository.QoQuoteCommodityPriceRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  商品价位业务处理
 */
@Service
public class CmPriceLevelService{
    @Resource
    private CmPriceLevelRepository baseRepository;

    @Resource
    private QoQuoteCommodityPriceRepository commodityPriceRepository;

    /**
    *  查询所有
    */
    public Result listAll(CmPriceLevel entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(CmPriceLevel entity, Page<CmPriceLevel> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(CmPriceLevel entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        commodityPriceRepository.remove(new QueryWrapper<QoQuoteCommodityPrice>().eq("price_level_id", id));
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        commodityPriceRepository.remove(new QueryWrapper<QoQuoteCommodityPrice>().in("price_level_id", ids));

        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

}