package com.whln.ordermax.api.shiro.utils;

import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.api.shiro.realm.LoginType;
import com.whln.ordermax.api.shiro.realm.UserToken;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.redis.RedisClient;
import com.whln.ordermax.data.domain.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liurun
 */
@Component
public class ShiroManager {
    @Resource
    RedisClient<String, SysUser> redisClient;
    private static final Long USER_TOKEN_TIMEOUT = 60 * 60 * 1000 * 2L;

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    public static boolean hasRole(String role) {

        return getSubject().hasRole(role);
    }

    public static void checkPermission(String permission) {

        getSubject().checkPermission(permission);
    }

    public static SysUser getUser() {
        Object object = getSubject().getPrincipal();
        return (SysUser) object;
    }

    public static String getUserId() {
        return getUser().getId();
    }

    public void logout() {
        getSubject().logout();
    }

    public static void setUser(SysUser user) {
        Subject subject = SecurityUtils.getSubject();
        PrincipalCollection principalCollection = subject.getPrincipals();
        String realmName = principalCollection.getRealmNames().iterator().next();
        PrincipalCollection newPrincipalCollection = new SimplePrincipalCollection(user, realmName);
        subject.runAs(newPrincipalCollection);

    }

    public Result<Object> login(UserToken token) {
        //登录不在该处处理，交由shiro处理
        Subject subject = SecurityUtils.getSubject();
        token.setRememberMe(true);
        subject.login(token);
        if (subject.isAuthenticated()) {
            Map<String, Object> map = new HashMap<>(1);
            map.put("jwt", subject.getSession().getId().toString());
            map.put("user", ShiroManager.getUser());
            Serializable id = subject.getSession().getId();
            redisClient.set("user_token:" + id, getUser(), USER_TOKEN_TIMEOUT);
            subject.hasRole("webadmin");
            return Result.success(map);
        } else {
            throw new BusinessException("登陆失败");
        }
    }




    public Serializable refreshToken(String id) {
        SysUser user = redisClient.get("user_token:" + id);
        Subject subject = null;
        if (ObjectUtil.isNotEmpty(user)) {
            subject = SecurityUtils.getSubject();
            subject.login(new UserToken(LoginType.USER_PASSWORD,
                    user.getUsername(),
                    user.getPassword()));
            redisClient.remove("user_token:" + id);
        }
        Serializable token = ShiroManager.getSubject().getSession().getId();
        redisClient.set("user_token:" + token, (SysUser) subject.getPrincipal(), USER_TOKEN_TIMEOUT);
        return token;
    }
}
