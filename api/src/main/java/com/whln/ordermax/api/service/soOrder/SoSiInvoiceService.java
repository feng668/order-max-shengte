package com.whln.ordermax.api.service.soOrder;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowCommodity;
import com.whln.ordermax.api.annotations.OaBillSave;
import com.whln.ordermax.api.mq.JmsOrderSender;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.SoSiInvoice;
import com.whln.ordermax.data.domain.SoSiInvoiceCommodity;
import com.whln.ordermax.data.domain.SoSiOrder;
import com.whln.ordermax.data.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 销售合同业务处理
 */
@BillCode(ModelMessage.SO_SI_ORDER)
@Service
public class SoSiInvoiceService {

    @Resource
    private SoSiInvoiceRepository baseRepository;

    @Resource
    private SoSiInvoiceCommodityRepository commodityRepository;

    /**
     * 查询所有
     */
    public Result listAll(SoSiInvoice entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(SoSiInvoice entity, Page<SoSiInvoice> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    @Resource
    private OaRuleRepository oaRuleRepository;

    @Resource
    private OaRuleLevelRepository oaRuleLevelRepository;
    @Resource
    private SoSiOrderRepository soSiOrderRepository;

    /**
     * 插入或更新
     */
    @OaBillSave
    @FlowCommodity
    @Transactional(rollbackFor = Exception.class)
    public Result save(SoSiInvoice entity) {
        SoSiInvoice soSiInvoice = baseRepository.getOne(new LambdaQueryWrapper<SoSiInvoice>().eq(com.whln.ordermax.data.domain.SoSiInvoice::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(soSiInvoice) && !ObjectUtil.equals(entity.getId(), soSiInvoice.getId())) {
            throw new BusinessException("编号已存在");
        }
        String lastBillId = entity.getLastBillId();
        if (ObjectUtil.isNotEmpty(lastBillId)) {
            SoSiOrder soSiOrder = soSiOrderRepository.getById(lastBillId);
            if (ObjectUtil.isNotEmpty(soSiOrder)) {
                if (ObjectUtil.equals(soSiOrder.getInvoiceStatus(), YesOrNo.YES.getState())) {
                    throw new BusinessException("单据已经生成发票了");
                } else {
                    soSiOrderRepository.update(null, new LambdaUpdateWrapper<SoSiOrder>()
                            .eq(SoSiOrder::getId, lastBillId)
                            .set(SoSiOrder::getInvoiceStatus, YesOrNo.YES.getState())
                    );
                }
            }
        }

        boolean b = baseRepository.saveOrUpdate(entity);
        List<SoSiInvoiceCommodity> commodityList = entity.getCommodityList();
        List<String> commodityIdList = commodityList.stream().map(SoSiInvoiceCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<SoSiInvoiceCommodity>()
                .eq(SoSiInvoiceCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(commodityIdList), SoSiInvoiceCommodity::getId, commodityIdList)
        );
        if (CollectionUtil.isNotEmpty(commodityList)) {
            commodityRepository.saveOrUpdateBatch(commodityList.stream()
                    .peek(e -> e.setBillId(entity.getId()))
                    .collect(Collectors.toList()));
        }

       /* OrderPlan orderPlan = new OrderPlan();
        orderPlan.setBillCode(ModelMessage.SO_SI_ORDER.getCode());
        orderPlan.setBillId(entity.getId());
        orderPlan.setBillStatus(1);
        orderPlan.setTemId(3);
        orderPlan.setLastBillId(entity.getId());
        orderPlan.setLastBillCode(ModelMessage.SO_SI_ORDER.getCode());
        orderPlan.setBillNo(entity.getBillNo());
        orderPlan.setRootBillId(entity.getId());
        orderPlan.setCreateTime(LocalDateTime.now());
        orderPlan.setUpdateTime(LocalDateTime.now());
        orderPlanRepository.save(orderPlan);*/
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    @Resource
    private JmsOrderSender sender;

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> delete(String id) {
        SoSiInvoice one = baseRepository.getOne(new LambdaQueryWrapper<SoSiInvoice>().eq(SoSiInvoice::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            commodityRepository.remove(new LambdaQueryWrapper<SoSiInvoiceCommodity>().eq(SoSiInvoiceCommodity::getBillId, id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<SoSiInvoice>().eq(SoSiInvoice::getId, id).set(SoSiInvoice::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<SoSiInvoice> quoteOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = quoteOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = quoteOrders.stream()
                .map(SoSiInvoice::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new QueryWrapper<SoSiInvoiceCommodity>().in("bill_id", ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<SoSiInvoice>().in(SoSiInvoice::getId, ids).set(SoSiInvoice::getIsDelete, 1));
            }
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result listCom(SoSiInvoiceCommodity entity) {
        return Result.success(commodityRepository.listByEntity(entity));
    }


    public SoSiInvoice getSoSiOrder(String id) {
        return baseRepository.getSoSiInvoice(id);
    }
}