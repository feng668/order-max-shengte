package com.whln.ordermax.api.service.whole;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.whln.ordermax.data.service.MapCache;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.repository.DynamicRepository;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 日历
 */
@Service
public class EventCalendarService {


    @Resource
    private DynamicRepository dynamicRepository;


    public Result getEventsByDateRange(String startDate, String endDate) {
        Map<String, List<Map<String, Object>>> eventsMap = dynamicRepository.listEventByDateRange(startDate, endDate).stream().collect(Collectors.groupingBy(m -> m.get("date").toString()));
        List<Map<String, Object>> eventDateList = new ArrayList<>();
        for (LocalDate d = LocalDate.parse(startDate); d.isBefore(LocalDate.parse(endDate).plusDays(1)); d = d.plusDays(1)) {
            List<Map<String, Object>> eventList = eventsMap.get(d.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            String dateStr = d.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            eventDateList.add(
                    new HashMap<String, Object>() {{
                        put("date", dateStr);
                        put("eventList", eventList);
                    }}
            );
        }
        return Result.success(eventDateList);
    }

    @Resource
    private ApplicationContext applicationContext;

    public Result getBillInfo(String id, String billCode) {
        if (null == billCode) {
            Result.error("205", "缺少单据类型");
        }

        try {
            MapCache bean = applicationContext.getBean(MapCache.class);
            Map<String, String> codeFullRepositoryMap = bean.codeFullRepositoryMap();
            Object repository;
            String code = codeFullRepositoryMap.get(billCode);
            if (ObjectUtil.isEmpty(code)) {
                return Result.success();
            }
            repository = applicationContext.getBean(Class.forName(codeFullRepositoryMap.get(billCode)));
            Object resultInfo = ReflectUtil.invoke(repository, "getAllById", id);
            return Result.success(resultInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("201", "系统错误");
        }

    }
}