package com.whln.ordermax.api.service.invoice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.api.service.invoice.InvoiceContractService;
import com.whln.ordermax.data.domain.InvoiceContract;
import com.whln.ordermax.data.mapper.InvoiceContractMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liurun
 * @since 2023-04-23
 */
@Service
public class InvoiceContractServiceImpl extends ServiceImpl<InvoiceContractMapper, InvoiceContract> implements InvoiceContractService {

}
