package com.whln.ordermax.api.controller.auth;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @RequestMapping("/login")
    public String login() {
        return "ok";
    }

    @RequestMapping("/logout")
    public String logout() {
        return "OK";
    }
}
