package com.whln.ordermax.api.service.sys.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.auth.AuthService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.utils.PasswordUtil;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.param.SysUserImportVO;
import com.whln.ordermax.data.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Resource
    private SysUserRepository sysUserRepository;

    @Resource
    private SysRoleRepository sysRoleRepository;

    @Resource
    private SysPowerRepository sysPowerRepository;

    @Resource
    private SysUserRoleRepository sysUserRoleRepository;

    @Resource
    private SysUserEmailRepository userEmailRepository;

    @Resource
    private SysDepartmentRepository sysDepartmentRepository;

    public Result getCurrentUserInfo() {
        String userId = ShiroManager.getUserId();
        SysUser user = sysUserRepository.getUserByUserId(userId);

        //获取角色
        List<SysRole> roleList = sysRoleRepository.listRolesByUserId(user.getId());

        //获取权限树
        if (ShiroManager.hasRole("webadmin")) {
            userId = null;
        }
        List<SysPower> sysPowerList = sysPowerRepository.listPowersByUserId(userId);
        Map<String, List<SysPower>> parentIdMap = sysPowerList.stream().collect(Collectors.groupingBy(p -> p.getParentId() == null ? "ROOT" : p.getParentId()));
        for (SysPower sysPower : sysPowerList) {
            List<SysPower> sysPowers = parentIdMap.get(sysPower.getId());
            if (ObjectUtil.isNotEmpty(sysPowers)) {
                if (ObjectUtil.isNotEmpty(sysPowers)) {
                    sysPowers.sort((o1, o2) -> {
                        if (o1.getSort() == null) {
                            o1.setSort(0);
                        }
                        if (o2.getSort() == null) {
                            o2.setSort(0);
                        }
                        return o1.getSort() - o2.getSort();
                    });
                    sysPower.setChildren(sysPowers);
                }

            }
            List<SysPower> parentList = parentIdMap.get(sysPower.getParentId() == null ? "ROOT" : sysPower.getParentId());
            parentIdMap.put(sysPower.getParentId(), parentList);
        }
        List<SysPower> powerTree = parentIdMap.get("ROOT");
        Map<String, Object> result = new HashMap<>(3);
        if (ObjectUtil.isNotEmpty(powerTree)) {
            powerTree.sort((o1, o2) -> {
                if (o1.getSort() == null) {
                    o1.setSort(0);
                }
                if (o2.getSort() == null) {
                    o2.setSort(0);
                }
                return o1.getSort() - o2.getSort();
            });
            result.put("powerTree", powerTree);
        }

        result.put("user", user);
        result.put("roles", roleList);

        return Result.success(result);
    }

    public Result pageAll(SysUser entity, Page<SysUser> page) {
        if (ObjectUtil.isNotEmpty(entity) && ObjectUtil.isNotEmpty(entity.getDepartmentId())) {
            Set<String> departmentIds = getDepartmentIds(entity.getDepartmentId());
            entity.setDepartmentIds(departmentIds);
        }
        IPage<SysUser> sysUserIPage = sysUserRepository.pageByEntity(entity, page);
        return Result.success(sysUserIPage);
    }

    /**
     * 获取部门加上子部门的id
     *
     * @param id 部门id
     * @return Set 部门id加上子部门的id
     */
    private Set<String> getDepartmentIds(String id) {
        Set<String> ids = new HashSet<>();
        List<SysDepartment> list = sysDepartmentRepository
                .list(new LambdaQueryWrapper<SysDepartment>()
                        .eq(SysDepartment::getParentId, id));
        List<String> stringStream = list.stream()
                .map(SysDepartment::getId)
                .collect(Collectors.toList());
        ids.add(id);
        for (String s : stringStream) {
            Set<String> departmentIds = getDepartmentIds(s);
            ids.addAll(departmentIds);
        }
        return ids;
    }

    public Result listAll(SysUser entity) {
        return Result.success(sysUserRepository.listByEntity(entity));
    }

    @Transactional(rollbackFor = Exception.class)
    public Result save(SysUser entity) {
        if (ObjectUtil.isNotEmpty(entity.getId())) {
            SysUser one = sysUserRepository.getOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getId, entity.getId()));
            if (ObjectUtil.isNotEmpty(one)
                    && ObjectUtil.isNotEmpty(entity.getPassword())
                    && !ObjectUtil.equals(entity.getPassword(), one.getPassword())) {
                entity.setPassword(PasswordUtil.encode(entity.getPassword()));
            }
        } else {
            entity.setPassword(PasswordUtil.encode(entity.getPassword()));

        }

        QueryWrapper<SysUser> wrapper = new QueryWrapper<SysUser>();
        wrapper.eq("username", entity.getUsername());
        if (entity.getId() != null) {
            wrapper.ne("id", entity.getId());
        }
        int usernameCount = sysUserRepository.count(wrapper);
        if (usernameCount > 0) {
            return Result.error("205", "已存在当前用户名");
        }
        boolean b = sysUserRepository.saveOrUpdate(entity);
        setRoles(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    public Result delete(String id) {
        SysUser user = sysUserRepository.getById(id);
        if (ObjectUtil.isNotEmpty(user) && ObjectUtil.equals(user.getUsername(), "admin")) {
            throw new BusinessException("系统管理员账号不允许删除");
        }
        sysUserRepository.removeById(id);
        return Result.success();
    }

    public Result batchDelete(List<String> ids) {
        if (ObjectUtil.isNotEmpty(ids)) {
            List<SysUser> list = sysUserRepository.list(new LambdaQueryWrapper<SysUser>().in(SysUser::getId, ids));
            List<String> noAdminIdList = list.stream()
                    .filter(e -> ObjectUtil.notEqual(e.getUsername(), "admin"))
                    .map(SysUser::getId)
                    .collect(Collectors.toList());
            sysUserRepository.removeByIds(noAdminIdList);
        }
        return Result.success();
    }

    @Transactional(rollbackFor = Exception.class)
    public Result setRoles(SysUser sysUser) {
        String userId = sysUser.getId();

        List<String> roleIds = sysUser.getRoleIds();
        boolean b = sysUserRoleRepository.remove(new LambdaQueryWrapper<SysUserRole>().eq(SysUserRole::getUserId, userId));
        if (ObjectUtil.isNotEmpty(roleIds)) {
            List<SysUserRole> sysUserRoleList = roleIds.stream().map(roleId -> {
                SysUserRole sur = new SysUserRole();
                sur.setUserId(userId);
                sur.setRoleId(roleId);
                return sur;
            }).collect(Collectors.toList());
            b &= sysUserRoleRepository.insertBatch(sysUserRoleList);
        }
        return b ? Result.success() : Result.error("201", "操作失败");
    }

    public Result getRoles(String userId) {
        List<SysRole> sysRoles = sysRoleRepository.listRolesByUserId(userId);
        return Result.success(sysRoles);
    }

    public Result setMail(SysUser sysUser) {
        String userId = AuthService.getUserIdFromAuthentication();
        if (!StrUtil.isAllNotEmpty(sysUser.getEmail(), sysUser.getEmailPassword())) ;
        boolean b = sysUserRepository.updateById(new SysUser() {{
            setId(userId);
            setEmail(sysUser.getEmail());
            setEmailPassword(sysUser.getEmailPassword());
        }});
        return b ? Result.success() : Result.error("201", "修改失败");
    }

    public Result saveMail(SysUserEmail userEmail) {
        //用户第一个邮箱设为默认
        Integer userExsitsEmailCount = userEmailRepository.count(new QueryWrapper<SysUserEmail>().eq("user_id", userEmail.getUserId()));
        if (userExsitsEmailCount == 0) {
            userEmail.setIsDefault(1);
        }
        return userEmailRepository.saveOrUpdate(userEmail) ? Result.success() : Result.error("201", "操作失败");
    }

    public Result deleteMail(String id) {
        return userEmailRepository.removeById(id) ? Result.success() : Result.error("201", "操作失败");
    }

    public Result<List<SysUserEmail>> listMail() {
        String userId = AuthService.getUserIdFromAuthentication();
        return Result.success(userEmailRepository.listByEntity(
                new SysUserEmail() {{
                    setUserId(userId);
                }}
        ));
    }

    public Result setDefaultMail(String id) {
        String userId = AuthService.getUserIdFromAuthentication();
        return userEmailRepository.setDefault(userId, id) ? Result.success() : Result.error("201", "操作失败");
    }

    public SysUserImportVO checkRepeat(List<SysUser> sysUserList) {
        List<String> dataBase = sysUserRepository.listObjs(new LambdaQueryWrapper<SysUser>()
                .select(SysUser::getStafferNo), Objects::toString);
        List<String> userNames = sysUserRepository.listObjs(new LambdaQueryWrapper<SysUser>()
                .select(SysUser::getUsername), Objects::toString);
        List<String> repeatUserName = sysUserList.stream()
                .peek(e->{
                    boolean matches = Pattern.matches("[0-9a-zA-Z_]+$", e.getUsername());
                    if (!matches){
                        throw new BusinessException("用户登陆账号只能为数字和字母");
                    }
                })
                .collect(Collectors.toMap(SysUser::getUsername, e -> 1, Integer::sum))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey).collect(Collectors.toList());
        List<String> repeatUserNo = sysUserList.stream()
                .collect(Collectors.toMap(SysUser::getStafferNo, e -> 1, Integer::sum))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey).collect(Collectors.toList());

        if (ObjectUtil.isNotEmpty(repeatUserNo)) {
            throw new BusinessException("以下员工编号在文档中重复：" + String.join(",", repeatUserNo));
        }
        if (ObjectUtil.isNotEmpty(repeatUserName)) {
            throw new BusinessException("以下用户名在文档中重复：" + String.join(",", repeatUserName));
        }
        List<SysUser> exits = new ArrayList<>(10);
        List<SysUser> noExits = new ArrayList<>(10);
        for (SysUser c : sysUserList) {
            if (dataBase.contains(c.getStafferNo()) || userNames.contains(c.getUsername())) {
                exits.add(c);
            } else {
                noExits.add(c);
            }
        }

        SysUserImportVO cmCommodityCheckVO = new SysUserImportVO();
        cmCommodityCheckVO.setExits(exits);
        cmCommodityCheckVO.setNoExits(noExits);
        cmCommodityCheckVO.setExitsNum(exits.size());
        cmCommodityCheckVO.setNoExitsNum(noExits.size());

        return cmCommodityCheckVO;
    }

    public static void main(String[] args) {
        boolean sdfdsf123 = Pattern.matches("[0-9a-zA-Z_]+$", "#sgfdsdff");
        System.out.println(sdfdsf123);
    }
    @Transactional(rollbackFor = Exception.class)
    public List<SysUser> batchImport(List<SysUser> sysUserList) {
        List<String> dataBase = sysUserRepository.listObjs(new LambdaQueryWrapper<SysUser>()
                .select(SysUser::getStafferNo), Objects::toString);
        List<String> userNames = sysUserRepository.listObjs(new LambdaQueryWrapper<SysUser>()
                .select(SysUser::getUsername), Objects::toString);
        List<SysUser> notExits = sysUserList.stream()
                .filter(item -> !dataBase.contains(item.getStafferNo()) && !userNames.contains(item.getUsername()))
                .peek(item -> {
                    item.setPassword(PasswordUtil.encode("123456"));
                    item.setCreateDate(LocalDate.now());
                    item.setUpdateDate(LocalDateTime.now());
                })
                .collect(Collectors.toList());
        sysUserRepository.saveBatch(notExits);
        sysUserList.removeAll(notExits);
        return sysUserList;
    }
}
