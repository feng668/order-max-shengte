package com.whln.ordermax.api.designpattern.strategy.stinorder;

import com.whln.ordermax.data.domain.StInOrder;
import lombok.AllArgsConstructor;

/**
 * @author liurun
 * @date 2023-04-26 9:49
 */
@AllArgsConstructor
public class PurHandler extends StInorderStrategy<StInOrder,String ,Object>{
    @Override
    public Object handle(StInOrder stInOrder, String s) {
        return null;
    }
}
