package com.whln.ordermax.api.service.soOrder;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SoSoOrderCommodity;
import com.whln.ordermax.data.repository.SoSoOrderCommodityRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  销售合同商品清单业务处理
 */
@Service
public class SoSoOrderCommodityService{
    @Resource
    private SoSoOrderCommodityRepository baseRepository;

    /**
    *  查询所有
    */
    public Result listAll(SoSoOrderCommodity entity) {
        List<SoSoOrderCommodity> soSoOrderCommodities = baseRepository.listByEntity(entity);
        return Result.success(soSoOrderCommodities);
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(SoSoOrderCommodity entity, Page<SoSoOrderCommodity> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(SoSoOrderCommodity entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

}