package com.whln.ordermax.api.controller.commodity;

import com.whln.ordermax.api.service.commodity.CmMaterialBomService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import com.whln.ordermax.api.service.commodity.CmMaterialService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CmMaterial;

import javax.annotation.Resource;

/**
 *  原料控制器
 */
@Api(tags = "材料")
@RequestMapping("/cmMaterial")
@RestController
public class CmMaterialController {

    @Resource
    private CmMaterialService cmMaterialService;
    @Resource
    private CmMaterialBomService cmMaterialBomService;
    @PostMapping("/saveCmmaterial")
    public Result<Void> save(@RequestBody CmMaterial param){
        cmMaterialService.save(param);

        return Result.success();
    }
    @PostMapping("/deleteCmMaterial")
    public Result<Void> delete(String id){
        cmMaterialService.delete(id);

        return Result.success();
    }

    @PostMapping("/updateCmMaterial")
    public Result<Void> update(@RequestBody CmMaterial param){
        cmMaterialService.update(param);

        return Result.success();
    }
    @GetMapping("/findCmMaterial")
    public Result<CmMaterial> findCmMaterial(String id){
        CmMaterial cmMaterial = cmMaterialService.get(id);
        return Result.success(cmMaterial);
    }


}
