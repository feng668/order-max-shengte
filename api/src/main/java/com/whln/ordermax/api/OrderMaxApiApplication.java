package com.whln.ordermax.api;

import com.whln.ordermax.api.config.DateConfig;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableScheduling
@EnableTransactionManagement
@SpringBootApplication
@ComponentScan(value = "com.whln.ordermax",excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE,classes = {DateConfig.class }))
@MapperScan("com.whln.ordermax.data.mapper")
@EntityScan("com.whln.ordermax.data.domain")

public class OrderMaxApiApplication {
    public static void main(String[] args) {
        System.setProperty("nashorn.args","--no-deprecation-warning");
        SpringApplication.run(OrderMaxApiApplication.class, args);
    }
}
