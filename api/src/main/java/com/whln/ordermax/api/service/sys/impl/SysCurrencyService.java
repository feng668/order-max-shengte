package com.whln.ordermax.api.service.sys.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SysCurrency;
import com.whln.ordermax.data.domain.excel.AutoHeadColumnWidthStyleStrategy;
import com.whln.ordermax.data.domain.excel.SysCurrencyExcel;
import com.whln.ordermax.data.domain.excel.SysCurrencyExcelImportParam;
import com.whln.ordermax.data.domain.param.ExcelExportParam;
import com.whln.ordermax.data.repository.SysCurrencyRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 币种业务处理
 */
@Service
public class SysCurrencyService {
    @Resource
    private SysCurrencyRepository baseRepository;

    @Resource
    private SysService sysService;

    /**
     * 查询所有
     */
    public Result listAll(SysCurrency entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(SysCurrency entity, Page<SysCurrency> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 查询单条
     */
    public Result<SysCurrency> getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }

    /**
     * 插入或更新
     */
    public Result save(SysCurrency entity) {
        entity.setId(sysService.nextId());
        if (entity.getStageNum() != null) {
            entity.setStageNum(entity.getStageNum() + 1);
        }
        boolean b = baseRepository.save(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    public Result delete(String currencyCode) {
        boolean b = baseRepository.remove(new QueryWrapper<SysCurrency>().eq("currency_code", currencyCode));
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 批量删除
     */
    public Result batchDelete(List<String> currencyCodes) {
        boolean b = baseRepository.remove(new QueryWrapper<SysCurrency>().in("currency_code", currencyCodes));
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result<List<SysCurrency>> listNewst(SysCurrency entity) {
        return Result.success(baseRepository.listNewst(entity));
    }


    public Result<List<SysCurrency>> listHistory(SysCurrency entity) {
        return Result.success(baseRepository.listHistory(entity));
    }

    public void export(ExcelExportParam param, HttpServletResponse response) {
        List<SysCurrency> list = baseRepository.excel(param);
        List<SysCurrencyExcel> sysCurrencyExcels = BeanUtil.copyToList(list, SysCurrencyExcel.class);
        try {
            EasyExcel.write(response.getOutputStream(), SysCurrencyExcel.class)
                    .sheet("汇率导出")
                    .registerWriteHandler(new AutoHeadColumnWidthStyleStrategy())

                    .doWrite(sysCurrencyExcels);
            ;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void importExcel(SysCurrencyExcelImportParam param) {
           /* InputStream inputStream = file.getInputStream();
            AnalysisEventListener<SysCurrencyExcel> syncReadListener = new AnalysisEventListener<SysCurrencyExcel>() {
                @Override
                public void onException(Exception exception, AnalysisContext context) throws Exception {
                    Integer rowIndex = context.readRowHolder().getRowIndex();
                    throw new BusinessException("第" + rowIndex + "行数据错误");
                }

                @Override
                public void invoke(SysCurrencyExcel sysCurrencyExcel, AnalysisContext analysisContext) {
                    LocalDateTime now = LocalDateTime.now();
                    DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    String format = now.format(dateTimeFormatter);
                    sysCurrencyExcel.setCreateTime(format);
                    sysCurrencyExcel.setUpdateTime(format);

                }
                @Override
                public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                }

            };
            List<SysCurrencyExcel> list = EasyExcel.read(inputStream).autoCloseStream(true)
                    .headRowNumber(1)
                    .head(SysCurrencyExcel.class)
                    .registerReadListener(syncReadListener)
                    .sheet()
                    .doReadSync();*/
        List<SysCurrency> sysCurrencies = BeanUtil.copyToList(param.getList(), SysCurrency.class);
        //拿出现在导入的货币类型、查出数据库存在的相同货币的记录
        Set<String> names = sysCurrencies.stream().map(SysCurrency::getCurrencyCode).collect(Collectors.toSet());
        List<SysCurrency> sysCurrencyList = baseRepository.list(new LambdaQueryWrapper<SysCurrency>()
                .in(SysCurrency::getCurrencyCode, names));
        //以货币名字分组匹配找出最后的stage_num
        Map<String, List<SysCurrency>> sysCurrency = sysCurrencyList.stream()
                .collect(Collectors.groupingBy(SysCurrency::getCurrencyCode));
        Map<String, List<SysCurrency>> importSysCurrency = sysCurrencies.stream()
                .collect(Collectors.groupingBy(SysCurrency::getCurrencyCode));
        List<SysCurrency> currencyList = new ArrayList<>(5);
        importSysCurrency.forEach((k, v) -> {
            int lastStageNum = 0;
            List<SysCurrency> childSysCurrency = sysCurrency.get(k);
            //如果存在同类型的，则最后一个lastStagenum为记录中最大的+1
            if (ObjectUtil.isNotEmpty(childSysCurrency)) {
                lastStageNum = childSysCurrency.stream()
                        .map(SysCurrency::getStageNum)
                        .max(Comparator.comparingInt(o -> o)).get() + 1;
            }
            for (SysCurrency s : v) {
                s.setStageNum(lastStageNum);
                lastStageNum++;
            }
            currencyList.addAll(v);
        });
        baseRepository.saveBatch(currencyList);

    }
}