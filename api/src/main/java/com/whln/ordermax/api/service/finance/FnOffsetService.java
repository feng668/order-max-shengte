package com.whln.ordermax.api.service.finance;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.FnOffset;
import com.whln.ordermax.data.repository.FnOffsetRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  收款核销业务处理
 * @author liurun
 */
@Service
public class FnOffsetService{
    @Resource
    private FnOffsetRepository baseRepository;

    /**
    *  查询所有
    */
    public Result listAll(FnOffset entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(FnOffset entity, Page<FnOffset> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(FnOffset entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
       baseRepository.removeByIds(ids);
        return Result.success();
    }

}