package com.whln.ordermax.api.controller.poOrder;

import com.whln.ordermax.api.service.poOrder.PoContractCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.PoContractCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  AcceptanceLot商品控制器
 */
@Api(tags = "采购合同商品")
@RestController
@RequestMapping("/PoContractCommodity")
public class PoContractCommodityController{

    @Resource
    private PoContractCommodityService baseService;

    @ApiOperation(value = "根据合同id查询")
    @PostMapping("/list")
    public Result<List<PoContractCommodity>> listAll(@RequestBody PoContractCommodity entity){
        return baseService.listAll(entity);
    }

}