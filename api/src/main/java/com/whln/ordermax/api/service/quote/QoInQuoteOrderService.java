package com.whln.ordermax.api.service.quote;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.QoInQuoteCommodity;
import com.whln.ordermax.data.domain.QoInQuoteOrder;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.QoInQuoteCommodityMapper;
import com.whln.ordermax.data.mapper.QoInQuoteOrderMapper;
import com.whln.ordermax.data.repository.QoInQuoteCommodityRepository;
import com.whln.ordermax.data.repository.QoInQuoteOrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 报价单业务处理
 */
@BillCode(ModelMessage.QO_IN_QUOTE_ORDER)
@Service
@AllArgsConstructor
public class QoInQuoteOrderService {
    private final QoInQuoteOrderRepository baseRepository;
    private final QoInQuoteOrderMapper qoInQuoteOrderMapper;
    private final QoInQuoteCommodityRepository qoInQuoteCommodityRepository;

    private final QoInQuoteCommodityMapper qoInQuoteCommodityMapper;

    /**
     * 查询所有
     */
    public Result listAll(QoInQuoteOrder entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(QoInQuoteOrder entity, Page<QoInQuoteOrder> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    public Result save(QoInQuoteOrder entity) {
        QoInQuoteOrder qoInQuoteOrder = baseRepository.getOne(new LambdaQueryWrapper<QoInQuoteOrder>().eq(QoInQuoteOrder::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(qoInQuoteOrder) && !ObjectUtil.equals(entity.getId(), qoInQuoteOrder.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<QoInQuoteCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream().map(QoInQuoteCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        qoInQuoteCommodityRepository.remove(new LambdaUpdateWrapper<QoInQuoteCommodity>().eq(QoInQuoteCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(idList), QoInQuoteCommodity::getId, idList)
        );
        if (CollectionUtil.isNotEmpty(commodityList)) {
            qoInQuoteCommodityRepository.saveOrUpdateBatch(commodityList.stream().peek(e->e.setBillId(entity.getId())).collect(Collectors.toList()));
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional
    public Result delete(String id) {
        QoInQuoteOrder one = baseRepository.getOne(new LambdaQueryWrapper<QoInQuoteOrder>().eq(QoInQuoteOrder::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            qoInQuoteCommodityMapper.delete(new LambdaQueryWrapper<QoInQuoteCommodity>().eq(QoInQuoteCommodity::getBillId, id));
            qoInQuoteOrderMapper.deleteById(id);
        } else {
            QoInQuoteOrder qoInQuoteOrder = new QoInQuoteOrder();
            qoInQuoteOrder.setId(id);
            qoInQuoteOrder.setIsDelete(1);
            qoInQuoteOrderMapper.updateById(qoInQuoteOrder);
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(List<String> ids) {
        List<QoInQuoteOrder> qoInQuoteOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = qoInQuoteOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = qoInQuoteOrders.stream()
                .map(QoInQuoteOrder::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                qoInQuoteCommodityMapper.delete(new QueryWrapper<QoInQuoteCommodity>().in("bill_id", ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<QoInQuoteOrder>().in(QoInQuoteOrder::getId, ids).set(QoInQuoteOrder::getIsDelete, 1));
            }
        }

    }

    public Result<List<QoInQuoteCommodity>> listCom(QoInQuoteCommodity entity) {
        return Result.success(qoInQuoteCommodityRepository.listByEntity(entity));
    }


    public QoInQuoteOrder get(String id) {
        QoInQuoteOrder byId = baseRepository.getByIdWithInfo(id);
        BillQuery param = new BillQuery();
        param.setBillIds(CollUtils.toSet(id));
        List<QoInQuoteCommodity> list = qoInQuoteCommodityMapper.commodityList(param);
        byId.setCommodityList(list);
        return byId;
    }

    public QoInQuoteOrder getAllById(String id) {
        return baseRepository.getAllById(id);
    }
}