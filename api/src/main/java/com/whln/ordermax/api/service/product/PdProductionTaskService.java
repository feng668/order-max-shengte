package com.whln.ordermax.api.service.product;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.annotations.OrderPlanSave;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.utils.CollUtil;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.param.PdProductionBatchSaveParam;
import com.whln.ordermax.data.domain.param.ProductionTaskBomParam;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.PdProductionCommodityBomMapper;
import com.whln.ordermax.data.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 生产任务单业务处理
 */
@BillCode(ModelMessage.PD_PRODUCTION_TASK)
@Service
public class PdProductionTaskService {
    @Resource
    private PdProductionTaskRepository baseRepository;

    @Resource
    private PdProductionLotRepository lotRepository;
    @Resource
    private PdProductTaskCommodityRepository pdProductTaskCommodityRepository;
    @Resource
    private SysService sysService;

    @Resource
    private PdProductionCommodityBomRepository pdProductionCommodityBomRepository;

    @Resource
    private DynamicRepository dynamicRepository;

    @Resource
    private CmCommodityBomRepository commodityBomRepository;

    @Resource
    OrderPlanRepository orderPlanRepository;

    @Resource
    PdProductionCommodityBomMapper pdProductionCommodityBomMapper;

    /**
     * 查询所有
     */
    public Result listAll(PdProductionTask entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }


    /**
     * 分页查询所有
     */
    public Result pageAll(PdProductionTask entity, Page<PdProductionTask> page) {
        IPage<PdProductionTask> pdProductionTaskIPage = baseRepository.pageByEntity(entity, page);
        List<PdProductionTask> records = pdProductionTaskIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> productionTaskIds = records.stream().map(PdProductionTask::getId).collect(Collectors.toSet());
            BillQuery param = new BillQuery();
            param.setBillIds(productionTaskIds);
            List<PdProductionCommodityBom> list = pdProductionCommodityBomMapper.commodityList(param);
            Map<String, List<PdProductionCommodityBom>> groupByProductionTaskId = list.stream().collect(Collectors.groupingBy(PdProductionCommodityBom::getProductionId));

            for (PdProductionTask p : records) {
                p.setBomList(groupByProductionTaskId.get(p.getId()));
                p.setCommodityList(groupByProductionTaskId.get(p.getId()));
            }
            pdProductionTaskIPage.setRecords(records);
        }

        return Result.success(pdProductionTaskIPage);
    }

    /**
     * 插入或更新
     */
    @OrderPlanSave
    @Transactional(rollbackFor = Exception.class)
    public Result save(PdProductionTask entity) {
        PdProductionTask pdProductionTask = baseRepository.getOne(new LambdaQueryWrapper<PdProductionTask>().eq(PdProductionTask::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(pdProductionTask) && !ObjectUtil.equals(entity.getId(), pdProductionTask.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<PdProductionLot> lotList = entity.getLotList();
        List<String> collect = lotList.stream()
                .map(PdProductionLot::getId)
                .filter(ObjectUtil::isNotEmpty)
                .collect(Collectors.toList());
        lotRepository.remove(new LambdaQueryWrapper<PdProductionLot>()
                .eq(PdProductionLot::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(collect), PdProductionLot::getId, collect)
        );
        if (CollectionUtil.isNotEmpty(entity.getLotList())) {
            lotRepository.saveOrUpdateBatch(lotList);
        }
        //删除掉从页面中删除的bom
        List<PdProductionCommodityBom> bomList = entity.getBomList();
        Set<String> bomIds = bomList.stream()
                .peek(item -> {
                    if (ObjectUtil.isEmpty(item.getPartNum()) || item.getPartNum().compareTo(BigDecimal.ZERO) <= 0) {
                        throw new BusinessException("bom单耗不能小于1");
                    }
                    item.setProductionId(entity.getId());
                })
                .map(PdProductionCommodityBom::getId)
                .filter(ObjectUtil::isNotEmpty)
                .collect(Collectors.toSet());
        pdProductionCommodityBomRepository.remove(new LambdaQueryWrapper<PdProductionCommodityBom>()
                .eq(PdProductionCommodityBom::getProductionId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(bomIds), PdProductionCommodityBom::getId, bomIds));
        pdProductionCommodityBomRepository.saveOrUpdateBatch(bomList);
        return Result.success();
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        PdProductionTask one = baseRepository.getOne(new LambdaQueryWrapper<PdProductionTask>().eq(PdProductionTask::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            lotRepository.remove(new QueryWrapper<PdProductionLot>().eq("bill_id", id));
            pdProductionCommodityBomRepository.remove(new QueryWrapper<PdProductionCommodityBom>().eq("production_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<PdProductionTask>().eq(PdProductionTask::getId, id).set(PdProductionTask::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<PdProductionTask> enquiryOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = enquiryOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = enquiryOrders.stream()
                .map(PdProductionTask::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                lotRepository.remove(new LambdaQueryWrapper<PdProductionLot>().in(PdProductionLot::getBillId, ids));
                pdProductionCommodityBomRepository.remove(new LambdaUpdateWrapper<PdProductionCommodityBom>().in(PdProductionCommodityBom::getProductionId, ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<PdProductionTask>().in(PdProductionTask::getId, ids).set(PdProductionTask::getIsDelete, 1));
            }
        }
        return Result.success();
    }

    @FlowControlSave
    @Transactional(rollbackFor = Exception.class)
    public Result<List<PdProductionTask>> batchSave(PdProductionBatchSaveParam param) {
        List<String> commodityFlowIdList = new ArrayList<>();
        String sourceBillId = null;
        orderPlanRepository.update(new LambdaUpdateWrapper<OrderPlan>()
                .eq(OrderPlan::getBillId, param.getLastBillId())
                .eq(OrderPlan::getBillCode, param.getLastBillCode())
                .set(OrderPlan::getBillStatus, 2)
        );
        List<PdProductionTask> entityList = param.getEntityList();

        List<PdProductionTask> pdProductionTaskList = new ArrayList<>();
        List<PdProductionCommodityBom> pdProductionCommodityBomList = new ArrayList<>();
        List<OrderPlan> orderPlans = new ArrayList<>();
        OrderPlan lastOrderPlan = null;
        String lastBillId = param.getParentBillId();
        String lastBillCode = param.getParentBillCode();
        if (ObjectUtil.isNotEmpty(lastBillId)) {
            lastOrderPlan = orderPlanRepository.getOne(
                    new LambdaQueryWrapper<OrderPlan>()
                            .eq(OrderPlan::getBillId, lastBillId)
                            .eq(OrderPlan::getBillCode, lastBillCode)
            );
        }
        List<PdProductionLot> lots = new ArrayList<>();

        Set<String> commodityIds = entityList.stream().map(PdProductionTask::getCommodityId).collect(Collectors.toSet());
        BillQuery commodityBomQuery = BillQuery.builder().billIds(commodityIds).build();
        List<CmCommodityBom> cmCommodityBomList = commodityBomRepository.queryBom(commodityBomQuery);
        Map<String, List<CmCommodityBom>> groupByCommodityId = cmCommodityBomList.stream()
                .collect(Collectors.groupingBy(CmCommodityBom::getCommodityId));
        for (PdProductionTask entity : entityList) {
            entity.setId(sysService.nextId());
            entity.setCreateTime(LocalDateTime.now());
            entity.setUpdateTime(LocalDateTime.now());
            pdProductionTaskList.add(entity);
            List<CmCommodityBom> cmCommodityBoms = groupByCommodityId.get(entity.getCommodityId());
            if (ObjectUtil.isNotEmpty(cmCommodityBoms)) {
                List<PdProductionCommodityBom> pdProductionCommodityBoms = cmCommodityBoms.stream()
                        .map(item -> {
                            PdProductionCommodityBom bom = new PdProductionCommodityBom();
                            BigDecimal totalPartNum = item.getPartNum().multiply(entity.getProductNum());
                            bom.setTotalPartNum(totalPartNum);
                            bom.setProductionId(entity.getId());
                            bom.setPartId(item.getPartId());
                            bom.setPartNum(item.getPartNum());
                            return bom;
                        }).collect(Collectors.toList());
                if (ObjectUtil.isNotEmpty(pdProductionCommodityBoms)) {
                    pdProductionCommodityBomList.addAll(pdProductionCommodityBoms);
                }
            }
            List<PdProductionLot> lotList = entity.getLotList();
            if (lotList != null && lotList.size() > 0) {
                for (PdProductionLot lot : lotList) {
                    lot.setBillId(entity.getId());
                    lots.add(lot);
                }
            }
            if (StrUtil.isNotEmpty(entity.getCommodityFlowId())) {
                commodityFlowIdList.add(entity.getCommodityFlowId());
            }
            if (sourceBillId == null && entity.getSourceBillId() != null) {
                sourceBillId = entity.getSourceBillId();
            }
            OrderPlan orderPlan = new OrderPlan();
            orderPlan.setLastBillId(ObjectUtil.isEmpty(lastBillId) ? entity.getId() : lastBillId);
            orderPlan.setLastBillCode(ObjectUtil.isEmpty(lastBillCode) ? ModelMessage.PD_PRODUCTION_TASK.getCode() : lastBillCode);
            orderPlan.setBillId(entity.getId());
            orderPlan.setBillCode(ModelMessage.PD_PRODUCTION_TASK.getCode());
            orderPlan.setRootBillId(ObjectUtil.isEmpty(lastOrderPlan) ? entity.getId() : lastOrderPlan.getRootBillId());
            orderPlan.setBillStatus(1);
            orderPlan.setTemId(2);
            orderPlan.setBillNo(entity.getBillNo());
            orderPlans.add(orderPlan);
        }
        List<List<PdProductionLot>> lists = CollUtil.collSub(lots, 1000);
        for (List<PdProductionLot> p : lists) {
            lotRepository.saveBatch(p);
        }
        orderPlanRepository.saveBatch(orderPlans);
        pdProductionCommodityBomRepository.saveOrUpdateBatch(pdProductionCommodityBomList);
        baseRepository.saveBatch(pdProductionTaskList);
        if (StrUtil.isNotEmpty(sourceBillId) && CollectionUtil.isNotEmpty(commodityFlowIdList)) {
            dynamicRepository.batchUpdateBillComFlowStatus(commodityFlowIdList, "so_so_order_commodity", 2);
            dynamicRepository.updateBillFlowStatusByComFlow(sourceBillId, "so_so_order", "so_so_order_commodity", 2);
        }
        return Result.success(entityList);
    }

    public Result<List<PdProductionLot>> listLot(String id) {
        return Result.success(
                lotRepository.listByEntity(
                        new PdProductionLot() {{
                            setBillId(id);
                        }}
                )
        );
    }

    public Result<List<CmCommodityBom>> listBom(JSONObject entity) {
        String commodityId = entity.getStr("commodityId");
        Integer num = entity.getInt("num");
        if (!ObjectUtil.isAllNotEmpty(commodityId, num)) {
            return Result.error("201", "缺少参数");
        }
        return Result.success(commodityBomRepository.listByCommodityIdNum(commodityId, num));
    }

    public List<PdProductionCommodityBom> getProductionTaskBom(ProductionTaskBomParam param) {
        return pdProductionCommodityBomRepository.getProductionTaskBom(param);
    }
}