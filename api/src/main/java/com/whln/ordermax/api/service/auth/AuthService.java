package com.whln.ordermax.api.service.auth;


import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.data.domain.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 认证相关服务
 */
@Service
@Slf4j
public class AuthService {


    public static final Snowflake SNOWFLAKE = IdUtil.getSnowflake(0, 0);
    public static String getUserIdFromAuthentication() {
        try {
            SysUser user = ShiroManager.getUser();
            return user.getId();
        }catch (Exception e){
            return null;
        }
    }



}