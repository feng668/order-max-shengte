package com.whln.ordermax.api.service.commodity;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.utils.TreeUtil;
import com.whln.ordermax.data.domain.CmCommodityCategory;
import com.whln.ordermax.data.repository.CmCommodityCategoryRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *  商品业务处理
 */
@Service
public class CmCommodityCategoryService{
    @Resource
    private CmCommodityCategoryRepository baseRepository;

    /**
    *  查询所有
    */
    public Result listAll(CmCommodityCategory entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(CmCommodityCategory entity, Page<CmCommodityCategory> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(CmCommodityCategory entity) {
        CmCommodityCategory commodityCategory = baseRepository.getOne(new LambdaQueryWrapper<CmCommodityCategory>().eq(CmCommodityCategory::getCnName, entity.getCnName()));
        if (ObjectUtil.isNotEmpty(commodityCategory)) {
            throw new BusinessException("分类名已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.remove(new QueryWrapper<CmCommodityCategory>().eq("id",id).or().like("parent_ids",id));
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.remove(new QueryWrapper<CmCommodityCategory>().in("id",ids).or(w -> {
            for (String id : ids) {
                w.or().like("parent_ids",id);
            }
        }));
        return b? Result.success(): Result.error("201","删除失败");
    }


    public Result treeData(String articleType) {
        CmCommodityCategory c = new CmCommodityCategory();
        c.setArticleType(articleType);

        List<CmCommodityCategory> cmCommodityCategories = baseRepository.listByEntity(c);
        List<CmCommodityCategory> tree = TreeUtil.getTree(cmCommodityCategories);
        return Result.success(tree);
    }

    public Result saveAll(List<CmCommodityCategory> entityList) {
        if (CollectionUtil.isNotEmpty(entityList)){
            List<String> collect = entityList.stream()
                    .collect(Collectors.toMap(CmCommodityCategory::getCnName, e -> 1, Integer::sum))
                    .entrySet().stream().filter(e -> e.getValue() > 1)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            if (ObjectUtil.isNotEmpty(collect)) {
                throw new BusinessException("分类名重复:" + String.join(",", collect));
            }

            List<CmCommodityCategory> allList = TreeUtil.treeToList(entityList);
            baseRepository.saveOrUpdateBatch(allList);
        }
        return Result.success();
    }
}