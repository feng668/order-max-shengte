package com.whln.ordermax.api.config;

import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-02-20 10:21
 */
@RestControllerAdvice
public class CustomExceptionHandler {

    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    public Result<Void> handlerCustomerException(BusinessException ex) {
        ex.printStackTrace();
        return Result.error(201, ex.getMessage());
    }

    /**
     * BindException异常处理
     * <p>
     * 当BindingResult中存在错误信息时，会抛出BindException异常。
     */
    @ExceptionHandler({BindException.class})
    public Result<Object> handleBindExceptionException(BindException ex) {
        BindingResult bindingResult = ex.getBindingResult();
        return Result.error(bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage)
                .collect(Collectors.joining(";")));
    }

    /**
     * 参数为实体类
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Result<Object> handleValidException(MethodArgumentNotValidException e) {
        // 从异常对象中拿到ObjectError对象
        ObjectError objectError = e.getBindingResult().getAllErrors().get(0);
        // 然后提取错误提示信息进行返回
        return Result.error(201, objectError.getDefaultMessage());
    }

    /**
     * 参数为单个参数或多个参数
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    public Result<Void> handleConstraintViolationException(ConstraintViolationException e) {
        // 从异常对象中拿到ObjectError对象
        String message = e.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining(";"));

        return Result.error(message);
    }
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public Result<Void> handleConstraintViolationException(HttpMessageNotReadableException e) {
        e.printStackTrace();
        return Result.error("参数类型不匹配");
    }

 /*   @ExceptionHandler(value = Exception.class)
    public Result<Void> exception(Exception e) {
        e.printStackTrace();

        if (e instanceof NullPointerException) {
            return Result.error("请检查是否有关键数据为空");
        } else if (e instanceof NumberFormatException) {
            return Result.error("请检查是否有应该填数字的地方填入了字母或符号");
        } else {
            return Result.error("未知错误");
        }
    }*/
}
