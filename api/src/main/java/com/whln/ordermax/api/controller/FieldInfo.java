package com.whln.ordermax.api.controller;

import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-11 17:57
 */
@Data
public class FieldInfo {
    private String fieldName;
    private String desc;
}
