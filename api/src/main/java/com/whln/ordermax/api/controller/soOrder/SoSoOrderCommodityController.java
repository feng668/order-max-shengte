package com.whln.ordermax.api.controller.soOrder;

import com.whln.ordermax.api.service.soOrder.SoSoOrderCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SoSoOrderCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  销售合同商品清单控制器
 */
@Api(tags = "销售合同商品清单")
@RestController
@RequestMapping("/SoSoOrderCommodity")
public class SoSoOrderCommodityController{

    @Resource
    private SoSoOrderCommodityService baseService;

    @ApiOperation(value = "根据销售合同id查询")
    @PostMapping("/list")
    public Result<List<SoSoOrderCommodity>> listAll(@RequestBody SoSoOrderCommodity entity){
        return baseService.listAll(entity);
    }

}