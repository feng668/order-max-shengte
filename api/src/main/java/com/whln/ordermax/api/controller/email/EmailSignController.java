package com.whln.ordermax.api.controller.email;

import com.whln.ordermax.api.service.email.EmailSignService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.param.EmailSignParam;
import com.whln.ordermax.data.domain.vo.EmailSignVO;

import java.util.List;

/**
 * @author liurun
 * @date 2023-02-21 18:48
 */
@Api(tags = "邮件签名")
@RestController
@AllArgsConstructor
@RequestMapping("sign")
public class EmailSignController {
    private final EmailSignService emailSignService;
    @ApiOperation("签名列表")
    @PostMapping("/querySign")
    public Result< List<EmailSignVO>> list(@RequestBody EmailSignParam param){
        List<EmailSignVO> emailSignVOList = emailSignService.list(param);
        return Result.success(emailSignVOList);
    }

    @ApiOperation("查询签名")
    @PostMapping("/getSign")
    public Result<EmailSignVO> getSign(@RequestBody EmailSignParam param){
        EmailSignVO emailSignVOList = emailSignService.get(param);
        return Result.success(emailSignVOList);
    }

    @ApiOperation("删除签名")
    @PostMapping("/deleteSign")
    public Result<Void> deleteSign(@RequestBody EmailSignParam param){
        emailSignService.deleteSign(param);
        return Result.success();
    }
    @ApiOperation("更新签名")
    @PostMapping("/updateSign")
    public Result<Void> updateSign(@RequestBody EmailSignVO param){
        emailSignService.updateSign(param);
        return Result.success();
    }
    @ApiOperation("新增签名")
    @PostMapping("/addSign")
    public Result<Void> addSign(@RequestBody EmailSignVO param){
        emailSignService.add(param);
        return Result.success();
    }

}
