package com.whln.ordermax.api.shiro;

/**
 * @author liurun
 * @date 2023-04-20 17:46
 */
public class ByteSourceUtils {
    public static ByteSourceSerializable bytes(byte[] bytes){
        return new ByteSourceSerializable(bytes);
    }

    public static ByteSourceSerializable bytes(String string){
        return new ByteSourceSerializable(string);
    }
}
