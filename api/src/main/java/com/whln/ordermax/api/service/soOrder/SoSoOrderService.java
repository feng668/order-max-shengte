package com.whln.ordermax.api.service.soOrder;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowCommodity;
import com.whln.ordermax.api.annotations.OaBillSave;
import com.whln.ordermax.api.annotations.OrderPlanSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.SoSoOrder;
import com.whln.ordermax.data.domain.SoSoOrderCommodity;
import com.whln.ordermax.data.domain.StInventoryGrab;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.SoSoOrderCommodityMapper;
import com.whln.ordermax.data.repository.DynamicRepository;
import com.whln.ordermax.data.repository.SoSoOrderCommodityRepository;
import com.whln.ordermax.data.repository.SoSoOrderRepository;
import com.whln.ordermax.data.repository.StInventoryGrabRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *  销售合同业务处理
 */
@BillCode(ModelMessage.SO_SO_ORDER)
@Service
public class SoSoOrderService {
    @Resource
    private SoSoOrderRepository baseRepository;

    @Resource
    private SoSoOrderCommodityRepository commodityRepository;

    @Resource
    private DynamicRepository dynamicRepository;
    @Resource
    private SoSoOrderCommodityMapper soSoOrderCommodityMapper;


    @Resource
    private StInventoryGrabRepository inventoryGrabRepository;

    /**
    *  查询所有
    */
    public Result listAll(SoSoOrder entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }


    /**
     * 分页查询所有
     */
    public Result pageAll(SoSoOrder entity, Page<SoSoOrder> page) {
        IPage<SoSoOrder> soSoOrderIPage = baseRepository.pageByEntity(entity, page);
        List<SoSoOrder> records = soSoOrderIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(SoSoOrder::getId).collect(Collectors.toSet());
            List<SoSoOrderCommodity> list = soSoOrderCommodityMapper.commodityList(BillQuery.builder().billIds(billIds).build());
            Map<String, List<SoSoOrderCommodity>> groupByBillIds =
                    list.stream().collect(Collectors.groupingBy(SoSoOrderCommodity::getBillId));
            List<SoSoOrder> result = records.stream().peek(item -> {
                item.setCommodityList(groupByBillIds.get(item.getId()));

            }).collect(Collectors.toList());
            soSoOrderIPage.setRecords(result);
        }
        return Result.success(soSoOrderIPage);
    }
    public Result<IPage<SoSoOrder>> page(SoSoOrder entity, Page<SoSoOrder> page) {
        IPage<SoSoOrder> soSoOrderIPage = baseRepository.pageByEntity(entity, page);
        List<SoSoOrder> records = soSoOrderIPage.getRecords();
        Set<String> billIds = records.stream().map(SoSoOrder::getId).collect(Collectors.toSet());
        List<SoSoOrderCommodity> list = soSoOrderCommodityMapper.commodityList(BillQuery.builder().billIds(billIds).build());
        Map<String, List<SoSoOrderCommodity>> groupByBillIds =
                list.stream().collect(Collectors.groupingBy(SoSoOrderCommodity::getBillId));
        List<SoSoOrder> result = records.stream().peek(item -> {
            item.setCommodityList(groupByBillIds.get(item.getId()));

        }).collect(Collectors.toList());
        soSoOrderIPage.setRecords(result);
        return Result.success(soSoOrderIPage);
    }
    /**
    *  插入或更新
    */
    @FlowCommodity
    @Transactional(rollbackFor = Exception.class)
    @OaBillSave
    @OrderPlanSave
    public Result save(SoSoOrder entity) {
        SoSoOrder soSoOrder = baseRepository.getOne(new LambdaQueryWrapper<SoSoOrder>().eq(SoSoOrder::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(soSoOrder)&&!ObjectUtil.equals(entity.getId(),soSoOrder.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<SoSoOrderCommodity> commodityList = entity.getCommodityList();
        List<String> ids = commodityList.stream()
                .peek(e->{
                    if (ObjectUtil.isEmpty(e.getQuotationNum())||e.getQuotationNum().compareTo(BigDecimal.ZERO)<1){
                        throw new BusinessException("商品数量不能为空");
                    }
                })
                .map(SoSoOrderCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<SoSoOrderCommodity>()
                .eq(SoSoOrderCommodity::getBillId,entity.getId())
                .notIn(ObjectUtil.isNotEmpty(ids),SoSoOrderCommodity::getId,ids)
        );
        if (CollectionUtil.isNotEmpty(commodityList)){
            List<String> idList = commodityList.stream().map(SoSoOrderCommodity::getCommodityId).collect(Collectors.toList());
            inventoryGrabRepository.remove(
                    new LambdaQueryWrapper<StInventoryGrab>()
                            .eq(StInventoryGrab::getOrderId,entity.getId())
                            .notIn(ObjectUtil.isNotEmpty(idList),StInventoryGrab::getCommodityId,idList)
            );
            commodityRepository.saveOrUpdateBatch(commodityList.stream().peek(e->e.setBillId(entity.getId())).collect(Collectors.toList()));
        }
        if (CollectionUtil.isNotEmpty(entity.getInventoryGrabList())){
            inventoryGrabRepository.saveOrUpdateBatchByUnique(entity.getInventoryGrabList());
        }
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    @Transactional
    public Result delete(String id) {
        SoSoOrder one = baseRepository.getOne(new LambdaQueryWrapper<SoSoOrder>().eq(SoSoOrder::getId, id));
        if ((ShiroManager.hasRole("webadmin")||ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId()))&& ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            boolean b = baseRepository.removeById(id);
            commodityRepository.remove(new QueryWrapper<SoSoOrderCommodity>().eq("bill_id",id));
        }else {
            baseRepository.update(new LambdaUpdateWrapper<SoSoOrder>().eq(SoSoOrder::getId, id).set(SoSoOrder::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
    *  批量删除
    */
    @Transactional
    public Result batchDelete(List<String> ids) {
        List<SoSoOrder> soSoOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = soSoOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = soSoOrders.stream()
                .map(SoSoOrder::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new LambdaQueryWrapper<SoSoOrderCommodity>().in(SoSoOrderCommodity::getBillId, ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<SoSoOrder>().in(SoSoOrder::getId, ids).set(SoSoOrder::getIsDelete, 1));
            }
        }
        return Result.success();
    }

    /**
     *  查询单条
     */
    public Result getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }

}