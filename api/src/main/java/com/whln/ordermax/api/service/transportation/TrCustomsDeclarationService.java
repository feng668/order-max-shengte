package com.whln.ordermax.api.service.transportation;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.TrCustomsDeclaration;
import com.whln.ordermax.data.domain.TrCustomsDeclarationCommodity;
import com.whln.ordermax.data.repository.TrCustomsDeclarationCommodityRepository;
import com.whln.ordermax.data.repository.TrCustomsDeclarationRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 报关单证业务处理
 */
@BillCode(ModelMessage.TR_CUSTOMS_DECLARATION)
@Service
public class TrCustomsDeclarationService {
    @Resource
    private TrCustomsDeclarationRepository baseRepository;

    @Resource
    private TrCustomsDeclarationCommodityRepository commodityRepository;

    /**
     * 查询所有
     */
    public Result listAll(TrCustomsDeclaration entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(TrCustomsDeclaration entity, Page<TrCustomsDeclaration> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    @FlowControlSave(status = 1)
    public Result save(TrCustomsDeclaration entity) {
        TrCustomsDeclaration t = baseRepository.getOne(new LambdaQueryWrapper<TrCustomsDeclaration>().eq(TrCustomsDeclaration::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(t)&&!ObjectUtil.equals(t.getId(),entity.getId())) {
            throw new BusinessException("单据编号已存在！");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<TrCustomsDeclarationCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream().map(TrCustomsDeclarationCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<TrCustomsDeclarationCommodity>()
                .eq(TrCustomsDeclarationCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(idList), TrCustomsDeclarationCommodity::getId, idList));
        if (CollectionUtil.isNotEmpty(commodityList)) {
            commodityRepository.saveOrUpdateBatch(commodityList);
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional
    public Result delete(String id) {

        TrCustomsDeclaration one = baseRepository.getOne(new LambdaQueryWrapper<TrCustomsDeclaration>().eq(TrCustomsDeclaration::getId, id));
        if ((ShiroManager.hasRole("webadmin")|| ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId()))&& ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            commodityRepository.remove(new QueryWrapper<TrCustomsDeclarationCommodity>().eq("bill_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<TrCustomsDeclaration>().eq(TrCustomsDeclaration::getId, id).set(TrCustomsDeclaration::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional
    public Result batchDelete(List<String> ids) {
        List<TrCustomsDeclaration> customsDeclarations = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = customsDeclarations.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = customsDeclarations.stream()
                .map(TrCustomsDeclaration::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new QueryWrapper<TrCustomsDeclarationCommodity>().in("bill_id", ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<TrCustomsDeclaration>().in(TrCustomsDeclaration::getId, ids).set(TrCustomsDeclaration::getIsDelete, 1));
            }
        }
        return Result.success();
    }

}