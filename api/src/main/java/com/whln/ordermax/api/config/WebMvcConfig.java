package com.whln.ordermax.api.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.util.List;

@Slf4j
@Configuration
//@ConfigurationProperties(prefix = "fileStore")
public class WebMvcConfig implements WebMvcConfigurer {

    @Value("${filestore.root}")
    private String rootPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/fileStore/**")
                .addResourceLocations("file:"+rootPath);
        log.info("设置静态资源路径:{}", rootPath);
    }

    /*模板保存路径*/
    @Bean
    public FreeMarkerConfigurer freemarkerConfig() {
        FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
        configurer.setTemplateLoaderPaths("classpath:/templates");
        configurer.setDefaultEncoding("UTF-8");
        return configurer;
    }
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new GetQueryJsonParamResolver());
    }
}