package com.whln.ordermax.api.service.poOrder;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.annotations.OaBillSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.PoContract;
import com.whln.ordermax.data.domain.PoInvoice;
import com.whln.ordermax.data.domain.PoInvoiceCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.PoInvoiceCommodityMapper;
import com.whln.ordermax.data.repository.PoContractRepository;
import com.whln.ordermax.data.repository.PoInvoiceCommodityRepository;
import com.whln.ordermax.data.repository.PoInvoiceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 采购发票业务处理
 */
@BillCode(ModelMessage.PO_INVOICE)
@Service
public class PoInvoiceService {
    @Resource
    private PoInvoiceRepository baseRepository;

    @Resource
    private PoInvoiceCommodityRepository commodityRepository;

    @Resource
    PoInvoiceCommodityMapper poInvoiceCommodityMapper;

    @Resource
    private PoContractRepository poContractRepository;

    /**
     * 查询所有
     */
    public Result listAll(PoInvoice entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(PoInvoice entity, Page<PoInvoice> page) {
        IPage<PoInvoice> poInvoiceIPage = baseRepository.pageByEntity(entity, page);
        List<PoInvoice> records = poInvoiceIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(PoInvoice::getId).collect(Collectors.toSet());
            BillQuery param = new BillQuery();
            param.setBillIds(billIds);
            List<PoInvoiceCommodity> list = poInvoiceCommodityMapper.commodityList(param);
            Map<String, List<PoInvoiceCommodity>> collect = list.stream().collect(Collectors.groupingBy(PoInvoiceCommodity::getBillId));
            for (PoInvoice p : records) {
                p.setCommodityList(collect.get(p.getId()));

            }
            poInvoiceIPage.setRecords(records);
        }
        return Result.success(poInvoiceIPage);
    }

    /**
     * 插入或更新
     */

    @OaBillSave
    @Transactional(rollbackFor = Exception.class)
    @FlowControlSave()
    public Result save(PoInvoice entity) {
        PoInvoice poInvoice = baseRepository.getOne(new LambdaQueryWrapper<PoInvoice>().eq(PoInvoice::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(poInvoice) && !ObjectUtil.equals(entity.getId(), poInvoice.getId())) {
            throw new BusinessException("编号已存在");
        }
        String lastBillId = entity.getLastBillId();
        if (ObjectUtil.isNotEmpty(lastBillId)) {
            PoContract byId = poContractRepository.getById(lastBillId);
            if (ObjectUtil.isNotEmpty(byId)) {
                if (ObjectUtil.equals(byId.getInvoiceStatus(), YesOrNo.YES.getState())) {
                    throw new BusinessException("单据已生成发票");
                } else {
                    poContractRepository.update(null, new LambdaUpdateWrapper<PoContract>()
                            .eq(PoContract::getId, lastBillId)
                            .set(PoContract::getInvoiceStatus, YesOrNo.YES.getState()));
                }
            }
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        List<PoInvoiceCommodity> commodityList = entity.getCommodityList();
        List<String> idList = commodityList.stream().map(PoInvoiceCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<PoInvoiceCommodity>().eq(PoInvoiceCommodity::getBillId, entity.getId()).notIn(ObjectUtil.isNotEmpty(idList), PoInvoiceCommodity::getId, idList));

        if (CollectionUtil.isNotEmpty(entity.getCommodityList())) {
            commodityRepository.saveOrUpdateBatch(entity.getCommodityList());
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        PoInvoice one = baseRepository.getOne(new LambdaQueryWrapper<PoInvoice>().eq(PoInvoice::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            commodityRepository.remove(new QueryWrapper<PoInvoiceCommodity>().eq("bill_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<PoInvoice>().eq(PoInvoice::getId, id).set(PoInvoice::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<PoInvoice> enquiryOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = enquiryOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = enquiryOrders.stream()
                .map(PoInvoice::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new LambdaQueryWrapper<PoInvoiceCommodity>().in(PoInvoiceCommodity::getBillId, ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<PoInvoice>().in(PoInvoice::getId, ids).set(PoInvoice::getIsDelete, 1));
            }
        }

        return Result.success();
    }

}