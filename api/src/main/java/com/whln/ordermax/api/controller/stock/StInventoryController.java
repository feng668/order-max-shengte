package com.whln.ordermax.api.controller.stock;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.stock.StInventoryService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.StInventoryRemain;
import com.whln.ordermax.data.domain.param.StInventoryListParam;
import com.whln.ordermax.data.domain.vo.BaseTotalVO;
import com.whln.ordermax.data.domain.vo.CmCommodityVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *  入库单控制器
 */
@Api(tags = "库存")
@RestController
@RequestMapping("/inventory")
public class StInventoryController {

    @Resource
    private StInventoryService baseService;

    @ApiOperation(value = "查询明细")
    @PostMapping("/pageDetail")
    public Result<IPage<CmCommodityVO>> pageDetail(@RequestBody PageEntity<StInventoryListParam> pageEntity){
        return Result.success(baseService.list(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize())));
    }

    @ApiOperation(value = "查询总账")
    @PostMapping("/pageTotalByStageType")
    public Result<IPage<BaseTotalVO>> pageTotalByStageType(@RequestBody PageEntity<JSONObject> pageEntity){
        return baseService.pageTotalByStageType(pageEntity.getEntity(),new Page<Map>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "查询库存情况")
    @PostMapping("/listRemainByCommodityId")
    public Result<List<StInventoryRemain>> listRemain(@RequestBody StInventoryRemain entity){
        return baseService.listRemain(entity);
    }

   /* @ApiOperation(value = "分页查询库存情况")
    @PostMapping("/pageRemain")
    public Result<IPage<StInventoryRemain>> pageRemain(@RequestBody PageEntity<StInventoryRemain> pageEntity){
        return baseService.pageRemain(pageEntity.getEntity(),new Page<StInventoryRemain>(pageEntity.getCurrent(),pageEntity.getSize()));
    }*/
    @ApiOperation(value = "即时库存")
    @PostMapping("/pageRemain")
    public Result<Page<CmCommodityVO>> baseInfo(@RequestBody PageEntity<StInventoryRemain> entity){
        return baseService.baseInfo(entity.getEntity(),new Page<>(entity.getCurrent(),entity.getSize()));
    }
}