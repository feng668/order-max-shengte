package com.whln.ordermax.api.service.sys.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.auth.AuthService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.SysEmailServer;
import com.whln.ordermax.data.domain.SysUserEmail;
import com.whln.ordermax.data.repository.SysEmailServerRepository;
import com.whln.ordermax.data.repository.SysUserEmailRepository;
import com.whln.ordermax.email.OmEmailHandle;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *  业务处理
 */
@Service
public class SysUserEmailService{
    @Resource
    private SysUserEmailRepository baseRepository;

    @Resource
    private SysEmailServerRepository emailServerRepository;

    @Resource
    private RestTemplate restTemplate;

    @Resource
    private OmEmailHandle omEmailHandle;


    /**
    *  分页查询所有
    */
    public Result pageAll(SysUserEmail entity, Page<SysUserEmail> page) {
        String userIdFromAuthentication = AuthService.getUserIdFromAuthentication();
        if (!ShiroManager.getSubject().hasRole("webadmin")) {
            entity.setUserId(ShiroManager.getUserId());
        }
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  查询单条
    */
    public Result<SysUserEmail> getAllById(String id) {
        return Result.success(baseRepository.getAllById(id));
    }

    public Result verify(SysUserEmail entity) {
        String emailDomain = omEmailHandle.getEmailDomainByAddress(entity.getEmail());
        if (emailDomain == null) {
            throw new BusinessException("此邮箱不符合要求");
        }
        SysEmailServer emailServer = emailServerRepository.getOne(new LambdaQueryWrapper<SysEmailServer>().eq(SysEmailServer::getEmailDomain, emailDomain));
        if (emailServer==null){
            throw new BusinessException("未配置邮箱服务器");
        }
        try {
            omEmailHandle.verifySmtpConnect(emailServer.getSendAddress(), entity.getEmail(), entity.getEmailPassword());
            System.out.println("验证成功");
        } catch (AuthenticationFailedException e) {
            e.printStackTrace();
            throw new BusinessException("验证失败,请核对邮箱密码或邮箱服务器是否配置正确！");
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new BusinessException("验证失败");
        }
        try {
            omEmailHandle.verifyPopConnect(emailServer.getReceiveAddress(), entity.getEmail(),  entity.getEmailPassword());
        } catch (AuthenticationFailedException e) {
            e.printStackTrace();
            throw new BusinessException("验证失败,请核对邮箱密码或邮箱服务器是否配置正确！");
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new BusinessException("验证失败");
        }
        return Result.success();
    }

    /**
    *  插入或更新
    */
    public Result save(SysUserEmail entity) {
        SysUserEmail sysUserEmail = baseRepository.getOne(new LambdaQueryWrapper<SysUserEmail>().eq(SysUserEmail::getEmail, entity.getEmail()));
        if (ObjectUtil.isNotEmpty(sysUserEmail)&&ObjectUtil.isEmpty(entity.getId())&&!ObjectUtil.equals(entity.getId(),sysUserEmail.getId())) {
            throw new BusinessException("邮箱已存在");
        }
        if (ObjectUtil.isEmpty(entity.getSycnStart())){
            entity.setSycnStart(1);
        }
        //用户第一个邮箱设为默认
        Integer userExsitsEmailCount = baseRepository.count(new QueryWrapper<SysUserEmail>().eq("user_id", entity.getUserId()));
        if (userExsitsEmailCount==0){
            entity.setIsDefault(1);
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

    public Result<SysEmailServer> getServer(String email) {
        if (StrUtil.isEmpty(email)){
            Result.error("201","缺少邮箱地址");
        }
        Pattern p = Pattern.compile("^\\w+@(\\w+.[a-z]+)$");
        Matcher m = p.matcher(email);
        String emailDomain = null;
        while (m.find()) {
            emailDomain = String.valueOf(m.group(1));
        }
        if (StrUtil.isEmpty(emailDomain)){
            Result.error("201","邮箱地址格式不正确");
        }
        SysEmailServer emailServer = emailServerRepository.getOne(
                new QueryWrapper<SysEmailServer>()
                        .eq("email_domain", emailDomain)
        );
        if (ObjectUtil.isEmpty(emailServer)){
            emailServer = new SysEmailServer();
            emailServer.setEmailDomain(emailDomain);
        }
        return Result.success(
                emailServer

        );
    }

    public Result contrSync(SysUserEmail userEmail) {
        Result responseResult = new Result();
        if (userEmail.getSycnStart()==1){
            try {
                responseResult = restTemplate.postForObject("http://127.0.0.1:12888/inboxSync/start", userEmail, Result.class);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            try {
                responseResult = restTemplate.postForObject("http://127.0.0.1:12888/inboxSync/stop", userEmail, Result.class);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        Boolean b = baseRepository.update(
                new UpdateWrapper<SysUserEmail>()
                        .set("sycn_start", userEmail.getSycnStart())
                        .eq("id", userEmail.getId())
        );
        return b? Result.success(): Result.error("201","操作失败");
    }


}