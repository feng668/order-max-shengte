package com.whln.ordermax.api.service.customer;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CuContactsEmail;
import com.whln.ordermax.data.domain.CuCustomerContacts;
import com.whln.ordermax.data.domain.vo.CuContactsEmailVO;
import com.whln.ordermax.data.domain.vo.CuCustomerContactsVO;
import com.whln.ordermax.data.repository.CuContactsEmailRepository;
import com.whln.ordermax.data.repository.CuCustomerContactsRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 客户联系人业务处理
 */
@Service
public class CuCustomerContactsService {
    @Resource
    private CuCustomerContactsRepository baseRepository;
    @Resource
    private CuContactsEmailRepository cuContactsEmailRepository;

    /**
     * 查询所有
     */
    public Result listAll(CuCustomerContacts entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(CuCustomerContacts entity, Page<CuCustomerContacts> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    public Result save(CuCustomerContacts entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 批量删除
     */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 查询所有
     */
    public Result<List<CuCustomerContactsVO>> listByCustomerId(CuCustomerContacts entity) {
        List<CuCustomerContacts> cuCustomerContacts = baseRepository.listByEntity(entity);
        List<CuCustomerContactsVO> cuCustomerContactsVOList = BeanUtil.copyToList(cuCustomerContacts, CuCustomerContactsVO.class);
        List<String> contactIdList = cuCustomerContacts.stream().map(CuCustomerContacts::getId).collect(Collectors.toList());
        if (ObjectUtil.isNotEmpty(contactIdList)) {
            List<CuContactsEmail> list = cuContactsEmailRepository.list(new LambdaQueryWrapper<CuContactsEmail>().in(CuContactsEmail::getContactId, contactIdList));
            List<CuContactsEmailVO> cuContactsEmailVOList = BeanUtil.copyToList(list, CuContactsEmailVO.class);
            Map<String, List<CuContactsEmailVO>> contactGroupBy = cuContactsEmailVOList.stream().collect(Collectors.groupingBy(CuContactsEmailVO::getContactId));
            for (CuCustomerContactsVO c : cuCustomerContactsVOList) {
                c.setEmails(contactGroupBy.get(c.getId()));
            }
        }
        return Result.success(cuCustomerContactsVOList);
    }
}