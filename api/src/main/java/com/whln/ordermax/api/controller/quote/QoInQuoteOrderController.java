package com.whln.ordermax.api.controller.quote;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.quote.QoInQuoteOrderService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.QoInQuoteCommodity;
import com.whln.ordermax.data.domain.QoInQuoteOrder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 报价单控制器
 */
@Api(tags = "内销报价单")
@RestController
@RequestMapping("/qoInQuoteOrder")
public class QoInQuoteOrderController {

    @Resource
    private QoInQuoteOrderService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<QoInQuoteOrder>> listAll(@RequestBody QoInQuoteOrder entity) {
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<QoInQuoteOrder>> pageAll(@RequestBody PageEntity<QoInQuoteOrder> pageEntity) {
        return baseService.pageAll(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "详情")
    @GetMapping("/getQoInQuoteOrder")
    public Result<QoInQuoteOrder> getQoInQuoteOrder(@Validated @NotNull(message = "内销单id不能为空") String id) {
        QoInQuoteOrder qoInQuoteOrder = baseService.get(id);
        return Result.success(qoInQuoteOrder);
    }

    @ApiOperation(value = "保存报价单和报价商品")
    @PostMapping("/save")
    public Result save(@RequestBody @Validated QoInQuoteOrder entity) {
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id) {
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result<Void> batchDelete(@RequestBody() List<String> ids) {
        baseService.batchDelete(ids);
        return Result.success();
    }

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<QoInQuoteOrder> getAllById(@RequestParam("id") String id) {
        return Result.success(baseService.getAllById(id));
    }

    @ApiOperation(value = "根据报价单id查询商品")
    @PostMapping("/listCom")
    public Result<List<QoInQuoteCommodity>> listCom(@RequestBody QoInQuoteCommodity entity) {
        return baseService.listCom(entity);
    }

}