package com.whln.ordermax.api.controller.commodity;

import com.whln.ordermax.api.service.commodity.CmCommodityCategoryService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CmCommodityCategory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  商品控制器
 */
@Api(tags = "商品分类")
@RestController
@RequestMapping("/CmCommodityCategory")
public class CmCommodityCategoryController{

    @Resource
    private CmCommodityCategoryService baseService;

    @ApiOperation(value = "保存|暂不启用")
    @PostMapping("/save")
    public Result save(@RequestBody() CmCommodityCategory entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/saveAll")
    public Result saveAll(@RequestBody() List<CmCommodityCategory> entityList){
        return baseService.saveAll(entityList);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }


    @ApiOperation(value = "获取商品分类树")
    @PostMapping("/treeData")
    public Result treeData(@RequestParam("articleType")String articleType){
        return baseService.treeData(articleType);
    }
}