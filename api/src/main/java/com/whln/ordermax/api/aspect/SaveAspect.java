package com.whln.ordermax.api.aspect;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.redis.RedisClient;
import com.whln.ordermax.common.utils.AopUtil;
import com.whln.ordermax.data.domain.OaRule;
import com.whln.ordermax.data.domain.SysRole;
import com.whln.ordermax.data.repository.OaRuleRepository;
import com.whln.ordermax.data.repository.SysRoleRepository;
import com.whln.ordermax.data.repository.SysUserRoleRepository;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liurun
 * @date 2023-03-24 10:26
 */
@Aspect
@Component
public class SaveAspect {
    @Resource
    private SysRoleRepository sysRoleRepository;
    @Lazy
    @Resource
    private RedisClient<String, SysRole> redisClient;
    @Resource
    private SysUserRoleRepository sysUserRoleRepository;
    @Resource
    private OaRuleRepository oaRuleRepository;

    @Pointcut("execution(* save(..))&&within(com.whln.ordermax.api.service..*)")
    public void pointCut() {
    }

    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        Object entity = AopUtil.getArgByName(joinPoint, "entity");
        if (ObjectUtil.isEmpty(entity)) {
            return joinPoint.proceed();
        }
        Class<?> aClass = entity.getClass();
        Object targetClass = joinPoint.getTarget();
        boolean isActive = ReflectUtil.hasField(aClass, "oaIsActive");
        if (isActive) {
            BillCode annotation = targetClass.getClass().getAnnotation(BillCode.class);
            if (ObjectUtil.isNotEmpty(annotation)) {
                ModelMessage value = annotation.value();
                OaRule oaRule = oaRuleRepository.getOne(new LambdaQueryWrapper<OaRule>().eq(OaRule::getBillCode, value.getCode()));
                if (ObjectUtil.isEmpty(oaRule)) {
                    return joinPoint.proceed();
                }
                Object proceed = joinPoint.proceed();
                if (proceed instanceof Result) {
                    Result<Object> proceed1 = (Result<Object>) proceed;
                    Map<String, Object> map = new HashMap<>(1);
                    map.put("oaIsActive", oaRule.getIsActive());
                    boolean oaStatus = ReflectUtil.hasField(entity.getClass(), "oaStatus");
                    if (oaStatus){
                        Object oaStatus1 = ReflectUtil.getFieldValue(entity, "oaStatus");
                        map.put("oaStatus",oaStatus1);
                    }
                    proceed1.setData(map);
                    return proceed1;
                }
            }
        }
        return joinPoint.proceed();
    }


}
