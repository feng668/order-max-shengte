package com.whln.ordermax.api.service.commodity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.ImmutableMap;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.api.service.whole.FileService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.PageWithUserIgnore;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.utils.CollUtil;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.param.TransactionHistoryParam;
import com.whln.ordermax.data.domain.vo.CmCommodityCheckVO;
import com.whln.ordermax.data.domain.vo.CmCommodityVO;
import com.whln.ordermax.data.domain.vo.CommodityTransactionHistoryVO;
import com.whln.ordermax.data.mapper.DynamicMapper;
import com.whln.ordermax.data.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class CmCommodityService {
    @Resource
    private CmCommodityRepository baseRepository;

    @Resource
    private CmCommodityImgRepository imgRepository;

    @Resource
    private FileService fileService;

    @Resource
    private SysService sysService;

    @Resource
    private CmCommodityBomRepository bomRepository;

    @Resource
    private CmCommodityCategoryRepository categoryRepository;

    @Resource
    private CmCommodityStandardRepository standardRepository;

    @Resource
    private CmCommodityImgRepository commodityImgRepository;

    @Resource
    private DynamicRepository dynamicRepository;
    @Resource
    DynamicMapper dynamicMapper;

    @PageWithUserIgnore
    public Result listAll(CmCommodity entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    @PageWithUserIgnore
    public Result<Page<CmCommodity>> pageAll(CmCommodity entity, Page<CmCommodity> page) {
        if (StrUtil.isNotEmpty(entity.getCategoryId())) {
            List<String> categoryIdIn = categoryRepository.listObjs(new LambdaQueryWrapper<CmCommodityCategory>()
                            .like(CmCommodityCategory::getParentIds, entity.getCategoryId())
                            .or()
                            .eq(CmCommodityCategory::getId, entity.getCategoryId())
                    , Objects::toString
            );
            entity.setCategoryIdIn(categoryIdIn);
        }
        Page<CmCommodity> cmCommodityPage = baseRepository.pageByEntity(entity, page);
        return Result.success(cmCommodityPage);
    }

    @Transactional(rollbackFor = Exception.class)
    public Result save(CmCommodity entity) throws BusinessException {
        CmCommodity cmCommodity = baseRepository.getOne(new LambdaQueryWrapper<CmCommodity>().eq(CmCommodity::getPartNo, entity.getPartNo()));
        if (ObjectUtil.isNotEmpty(cmCommodity)&&!ObjectUtil.equals(entity.getId(),cmCommodity.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        if (CollectionUtil.isNotEmpty(entity.getImgDelUriList())) {
            fileService.deleteFile(entity.getImgDelUriList());
        }
        if (CollectionUtil.isNotEmpty(entity.getImgDelIdList())) {
            imgRepository.removeByIds(entity.getImgDelIdList());
        }
        if (CollectionUtil.isNotEmpty(entity.getImgList())) {
            imgRepository.saveOrUpdateBatch(entity.getImgList());
        }
        if (CollectionUtil.isNotEmpty(entity.getPartDelIdList())) {
            bomRepository.remove(new QueryWrapper<CmCommodityBom>().eq("commodity_id", entity.getId()).in("part_id", entity.getPartDelIdList()));
        }
        if (CollectionUtil.isNotEmpty(entity.getBomList())) {
            bomRepository.saveOrUpdateBatchByMutiId(entity.getBomList(),entity.getId());
        }
        if (CollectionUtil.isNotEmpty(entity.getStandardDelIdList())) {
            standardRepository.removeByIds(entity.getStandardDelIdList());
        }
        if (CollectionUtil.isNotEmpty(entity.getStandardList())) {
            standardRepository.saveOrUpdateBatch(entity.getStandardList());
        }

        return b ? Result.success() : Result.error("201", "保存失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        baseRepository.update(new LambdaUpdateWrapper<CmCommodity>().set(CmCommodity::getIsDelete, 1).eq(CmCommodity::getId, id));
       /* List<CmCommodityImg> imgList = imgRepository.list(new QueryWrapper<CmCommodityImg>().eq("commodity_id", id));
        imgRepository.remove(new QueryWrapper<CmCommodityImg>().eq("commodity_id", id));
        bomRepository.remove(new QueryWrapper<CmCommodityBom>().eq("commodity_id", id));
        standardRepository.remove(new QueryWrapper<CmCommodityStandard>().eq("commodity_id", id));
        for (CmCommodityImg img : imgList) {
            fileService.deleteFile(img.getUri());
        }*/
        return Result.success();
    }

    @Transactional(rollbackFor = Exception.class)
    public Result<Void> batchDelete(List<String> ids) {
        /*List<CmCommodityImg> imgList = imgRepository.list(new QueryWrapper<CmCommodityImg>().in("commodity_id", ids));
        imgRepository.remove(new QueryWrapper<CmCommodityImg>().in("commodity_id", ids));
        boolean b = baseRepository.removeByIds(ids);
        bomRepository.remove(new QueryWrapper<CmCommodityBom>().in("commodity_id", ids));
        standardRepository.remove(new QueryWrapper<CmCommodityStandard>().in("commodity_id", ids));
        for (CmCommodityImg img : imgList) {
            fileService.deleteFile(img.getUri());
        }*/
        baseRepository.update(new LambdaUpdateWrapper<CmCommodity>().set(CmCommodity::getIsDelete, 1).in(CmCommodity::getId, ids));
        return Result.success();
    }

    public CmCommodityCheckVO checkRepeat(List<CmCommodityVO> cmCommodityList) {
        CmCommodityCheckVO cmCommodityCheckVO = new CmCommodityCheckVO();
        //查出文档里面重复的商品编号
        List<CmCommodityVO> cmCommodityVOList = checkDocRepeat(cmCommodityList);
        if (ObjectUtil.isNotEmpty(cmCommodityVOList)) {
            cmCommodityCheckVO.setDocRepeat(cmCommodityVOList);
            cmCommodityCheckVO.setDocRepeatNum(cmCommodityVOList.size());
            cmCommodityList.removeAll(cmCommodityVOList);
        }
        /*ArrayList<String> reduce = cmCommodityList.stream().map(CmCommodityVO::getPartNo).reduce(new ArrayList<String>(), (list, str) -> {
            list.addAll(splitByComma(str));
            return list;
        }, (x, y) -> x);*/
        List<String> dataBase = baseRepository.listObjs(new LambdaQueryWrapper<CmCommodity>()
                .select(CmCommodity::getPartNo), Objects::toString);
       /* ArrayList<String> dataBasePartNos = dataBase.stream().reduce(new ArrayList<String>(), (list, item) -> {
            list.addAll(splitByComma(item));
            return list;
        }, (x, y) -> x);*/

        List<CmCommodityVO> exits = new ArrayList<>(10);
        List<CmCommodityVO> noExits = new ArrayList<>(10);
        for (CmCommodityVO c : cmCommodityList) {
            if (dataBase.contains(c.getPartNo())) {
                exits.add(c);
            } else {
                noExits.add(c);
            }
        }
        cmCommodityCheckVO.setExits(exits);
        cmCommodityCheckVO.setNoExits(noExits);
        cmCommodityCheckVO.setExitsNum(exits.size());
        cmCommodityCheckVO.setNoExitsNum(noExits.size());
        return cmCommodityCheckVO;
    }

    /**
     * 检查是否有重复的商品编号
     *
     * @param cmCommodityList 商品列表
     * @return
     */
    private List<CmCommodityVO> checkDocRepeat(List<CmCommodityVO> cmCommodityList) {
        List<String> repeatPartNos = cmCommodityList.stream() // list 对应的 Stream
                .collect(Collectors.toMap(CmCommodityVO::getPartNo, e -> 1, Integer::sum))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        List<CmCommodityVO> list = new ArrayList<>();
        if (ObjectUtil.isNotEmpty(repeatPartNos)) {
            for (CmCommodityVO c : cmCommodityList) {
                if (repeatPartNos.contains(c.getPartNo())) {
                    c.setRepeat(1);
                    list.add(c);
                }
            }
            return list;
        }
        return null;

    }


    @Transactional(rollbackFor = Exception.class)
    public List<CmCommodity> batchImport(List<CmCommodity> commodityList) {
        List<CmCommodityVO> repeat = checkDocRepeat(BeanUtil.copyToList(commodityList, CmCommodityVO.class));
        if (ObjectUtil.isNotEmpty(repeat)) {
            List<String> repeatPartNos = repeat.stream().map(CmCommodityVO::getPartNo).collect(Collectors.toList());
            String join = String.join(",", repeatPartNos);
            throw new BusinessException("商品编号：" + join + "重复");
        }
        List<String> dataBase = baseRepository.listObjs(new LambdaQueryWrapper<CmCommodity>()
                .select(CmCommodity::getPartNo), Objects::toString);
        List<CmCommodity> notExits = commodityList.stream()
                .filter(item -> !dataBase.contains(item.getPartNo()))
                .peek(item -> {
                    item.setOwnerId(ShiroManager.getUserId());
                    item.setCreateDate(LocalDate.now());
                    item.setUpdateDate(LocalDateTime.now());
                })
                .collect(Collectors.toList());
        List<List<CmCommodity>> lists = CollUtil.collSub(notExits, 1000);
        for (List<CmCommodity> list : lists) {
            baseRepository.saveBatch(list);
        }
        commodityList.removeAll(notExits);
        return commodityList;
    }

    public List<String> splitByComma(String str) {
        List<String> result = new ArrayList<>();
        String[] split = str.split(",");
        for (String s : split) {
            List<String> strings = splitBySlash(s);
            result.addAll(strings);
        }
        return result;
    }

    public List<String> splitBySlash(String str) {
        return new ArrayList<>(Arrays.asList(str.split("/")));
    }

    public Result batchVerifyExsits(List<Map<String, Object>> cmCommodityList) {
        Map<String, CmCommodity> exsitsPartNoMap = baseRepository.listByEntity(null).stream().collect(Collectors.toMap(CmCommodity::getPartNo, c -> c));
        List<Map<String, Object>> exsitsList = new ArrayList<>();
        List<Map<String, Object>> noExsitsList = new ArrayList<>();
        List<String> exsitsPartNoList = new ArrayList<>();
        for (Map<String, Object> map : cmCommodityList) {
            CmCommodity exsitsCommodity = exsitsPartNoMap.get(map.get("partNo").toString());
            if (exsitsCommodity != null) {
                map.putAll(BeanUtil.beanToMap(exsitsCommodity));
                if (exsitsPartNoList.contains(map.get("partNo").toString())) {
                    continue;
                }
                exsitsList.add(map);
                exsitsPartNoList.add(map.get("partNo").toString());
            } else {
                noExsitsList.add(map);
            }
        }
        return Result.success(ImmutableMap.of("exsitsList", exsitsList, "noExsitsList", noExsitsList));
    }

    public Result<List<CmCommodity>> batchSaveAndReturnInfo(List<Map<String, Object>> cmCommodityList) {
        List<CmCommodity> saveList = new ArrayList<>();
        for (Map<String, Object> map : cmCommodityList) {
            map.put("id", sysService.nextId());
            saveList.add(BeanUtil.mapToBean(map, CmCommodity.class, true));
        }
        baseRepository.insertBatch(saveList);
        return Result.success(saveList);
    }

    @PageWithUserIgnore
    public Result<IPage<CmCommodity>> pageWithLastQoPriceByCustId(CmCommodity entity, Page<CmCommodity> page) {
        return Result.success(baseRepository.pageWithLastQoPriceByCustId(entity, page));
    }

    public Result<List<CmCommodityBom>> listBom(CmCommodityBom entity) {
        if (entity.getCommodityId() == null) {
            throw new BusinessException("缺少商品id");
        }
        return Result.success(bomRepository.listByEntity(entity));
    }

    public Result<List<CmCommodityStandard>> listStandard(String id) {
        CmCommodityStandard cmCommodityStandard = new CmCommodityStandard();
        cmCommodityStandard.setCommodityId(id);
        return Result.success(standardRepository.listByEntity(cmCommodityStandard));
    }


    public Result<Page<CommodityTransactionHistoryVO>> listDeal(TransactionHistoryParam param) {
        return Result.success(dynamicRepository.listDealByCommodityId(param));
    }

    public Result<Map<String, List<CmCommodityImg>>> listImg(CmCommodityImg entity) {
        if (StrUtil.isEmpty(entity.getCommodityId())) {
            return Result.error("202", "缺少商品id");
        }
        Map<String, List<CmCommodityImg>> imgTypeMap = commodityImgRepository.listByEntity(entity).stream().collect(Collectors.groupingBy(CmCommodityImg::getImgType));
        return Result.success(imgTypeMap);
    }
}