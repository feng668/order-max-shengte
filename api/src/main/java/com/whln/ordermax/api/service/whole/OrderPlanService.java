package com.whln.ordermax.api.service.whole;

import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.designpattern.chain.impl.OrderPlanSetChild;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.RedisKey;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.redis.RedisClient;
import com.whln.ordermax.data.domain.OrderPlan;
import com.whln.ordermax.data.repository.OrderPlanRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单进度业务处理
 */
@Slf4j
@Service
public class OrderPlanService {
    @Resource
    private OrderPlanRepository baseRepository;
    @Resource
    private OrderPlanSetChild orderPlanSetChild;
    @Resource
    private RedisClient<String, OrderPlan> redisClient;

    /**
     * 查询所有
     */
    public Result listAll(OrderPlan entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(OrderPlan entity, Page<OrderPlan> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    public Result save(OrderPlan entity) {
        if (!"SO_SO_ORDER".equals(entity.getBillCode())) {
            OrderPlan lastOrderPlan = baseRepository.getOne(new QueryWrapper<OrderPlan>().eq("bill_id", entity.getLastBillId()).eq("bill_code", entity.getLastBillCode()));
            baseRepository.update(new UpdateWrapper<OrderPlan>().set("bill_status", 2)

                    .eq("bill_id", lastOrderPlan.getBillId()).eq("bill_code", lastOrderPlan.getBillCode()));
            if ("PD_PRODUCTION_TASK".equals(entity.getBillCode())) {
                entity.setTemId(2);
                baseRepository.update(new UpdateWrapper<OrderPlan>().set("tem_id", 2).eq("source_bill_id", lastOrderPlan.getRootBillId()));
            }
            entity.setRootBillId(lastOrderPlan.getRootBillId());
            entity.setBillStatus(1);
        } else {
            entity.setRootBillId(entity.getBillId());
            entity.setBillStatus(1);
        }
        Boolean b = baseRepository.saveOrUpdate(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b ? Result.success() : Result.error("201", "删除失败");
    }
   /**
     * 批量删除
     */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b ? Result.success() : Result.error("201", "删除失败");
    }


    private void queryOrderPlan(OrderPlan entity, List<OrderPlan> orderPlans) {
        OrderPlan orderPlan = baseRepository.listByBill1(entity);
        if (ObjectUtil.isNotEmpty(orderPlan)) {
            orderPlans.add(orderPlan);
            queryOrderPlan(orderPlan, orderPlans);
        }
    }

    public Result showPlan(OrderPlan entity) throws IOException {
        List<OrderPlan> planList = baseRepository.listByBill(entity);
        if (ObjectUtil.isEmpty(planList)) {
            queryOrderPlan(entity, planList);
        }
        Integer temId = 1;
        if (ObjectUtil.isNotEmpty(planList) && planList.size() > 0) {
            Integer temId1 = planList.get(0).getTemId();
            if (ObjectUtil.isNotEmpty(temId1)) {
                temId = temId1;
            }
        }
        JSONObject jsonObject;
        OrderPlan orderPlan;
        if (ObjectUtil.equals(temId, 3)) {
            orderPlan = redisClient.get(RedisKey.ORDER_PLANE.getKey());
            if (ObjectUtil.isEmpty(orderPlan)) {
                InputStream stream = new ClassPathResource("orderPlan/eplan.json").getStream();
                jsonObject = JSONUtil.parseObj(IOUtils.toString(stream, StandardCharsets.UTF_8));
                stream.close();
                orderPlan = jsonObject.toBean(OrderPlan.class);
                redisClient.set(RedisKey.ORDER_PLANE.getKey(), orderPlan);
            }
        } else {
            orderPlan = redisClient.get(RedisKey.ORDER_PLAND.getKey());
            if (ObjectUtil.isEmpty(orderPlan)) {
                InputStream stream = new ClassPathResource("orderPlan/dplan.json").getStream();
                jsonObject = JSONUtil.parseObj(IOUtils.toString(stream, StandardCharsets.UTF_8));
                orderPlan = jsonObject.toBean(OrderPlan.class);
                stream.close();
                redisClient.set(RedisKey.ORDER_PLAND.getKey(), orderPlan);
            }
        }
        if (ObjectUtil.isEmpty(planList)) {
            return Result.success(orderPlan);
        }
        Map<String, List<OrderPlan>> planMap = planList.stream()
                .collect(Collectors.groupingBy(OrderPlan::getBillCode));
        List<OrderPlan> exsitsPlans = planMap.get(orderPlan.getBillCode());
        if (exsitsPlans != null && exsitsPlans.size() > 0) {
            orderPlan.setOrders(exsitsPlans);
            orderPlan.setBillStatus(2);
            for (OrderPlan exsitsPlan : exsitsPlans) {
                if (exsitsPlan.getBillStatus() == 1) {
                    orderPlan.setBillStatus(1);
                    break;
                }
            }
        } else {
            orderPlan.setBillStatus(0);
        }
        String code = orderPlan.getBillCode();
        if (ObjectUtil.isNotEmpty(planList)) {
            OrderPlan orderPlan1 = null;
            try {
                orderPlan1 = planList.stream()
                        .filter(item -> ObjectUtil.equals(item.getLastBillId(),item.getBillId()))
                        .findAny().get();
            } catch (Exception e) {
                return Result.success();
            }

            orderPlan.setBillId(orderPlan1.getBillId());
            orderPlan.setBillNo(orderPlan1.getBillNo());
            OrderPlan orderPlan2 = orderPlanSetChild.buildTree(planList, orderPlan);

            if (ObjectUtil.equals(code,orderPlan1.getBillCode())) {
                List<OrderPlan> orderPlans = new ArrayList<>();
                OrderPlan o = new OrderPlan();
                o.setBillId(orderPlan1.getBillId());
                o.setBillNo(orderPlan1.getBillNo());
                o.setBillCode(orderPlan1.getBillCode());
                orderPlans.add(o);
                orderPlan2.setOrders(orderPlans);
            }
            return Result.success(orderPlan2);
        }
        throw new BusinessException("未知错误");
    }


   /* public Result showPlan2(OrderPlan entity) {
        try {
            List<OrderPlan> planList = baseRepository.listByBill(entity);
//            OrderPlan start = orderPlanChainFactory.start(planList);
            Integer temId = 1;
            if ( ObjectUtil.isNotEmpty(planList) && planList.size() > 0) {
                Integer temId1 = planList.get(0).getTemId();

                if (ObjectUtil.isNotEmpty(temId1)) {
                    temId = temId1;
                }
            }
//            planList = orderPlanChainFactory.start(planList);
            JSONObject jsonObject;
            OrderPlan orderPlan;
            if (ObjectUtil.equals(temId,1)) {
                jsonObject = JSONUtil.parseObj(IOUtils.toString(new ClassPathResource("orderPlan/cplan.json").getStream(), "utf-8"));
                orderPlan = jsonObject.toBean(OrderPlan.class);
            } else {
                jsonObject = JSONUtil.parseObj(IOUtils.toString(new ClassPathResource("orderPlan/dplan.json").getStream(), "utf-8"));
                orderPlan = jsonObject.toBean(OrderPlan.class);
            }
            Map<String, List<OrderPlan>> planMap = planList.stream()
                    .collect(Collectors.groupingBy(OrderPlan::getBillCode));
            List<OrderPlan> exsitsPlans = planMap.get(orderPlan.getBillCode());
            if (exsitsPlans != null && exsitsPlans.size() > 0) {
                orderPlan.setOrders(exsitsPlans);
                orderPlan.setBillStatus(2);
                for (OrderPlan exsitsPlan : exsitsPlans) {
                    if (exsitsPlan.getBillStatus() == 1) {
                        orderPlan.setBillStatus(1);
                        break;
                    }
                }
            } else {
                orderPlan.setBillStatus(0);
            }
            if (ObjectUtil.isNotEmpty(planList)) {
                OrderPlan orderPlan1 = planList.stream().filter(item -> {
                    return ObjectUtil.isNull(item.getLastBillId());
                }).findAny().get();
                orderPlan.setBillId(orderPlan1.getBillId());
            }

            orderPlan.setChirds(parseChilds2(orderPlan.getChirds(), planMap,orderPlan.getBillId()));
            return Result.success(orderPlan);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return Result.error("201", "配置文件不存在");
        } catch (IOException e) {
            e.printStackTrace();
            return Result.error("201", "文件流异常");
        }
    }*/

    private List<OrderPlan> parseChilds2(List<OrderPlan> chirds, Map<String, List<OrderPlan>> planMap, String prentBillId) {
        if (chirds.size() > 0) {
            for (OrderPlan orderPlan : chirds) {
                List<OrderPlan> exsitsPlans = planMap.get(orderPlan.getBillCode());
                if (exsitsPlans != null && exsitsPlans.size() > 0) {
                    List<OrderPlan> collect = exsitsPlans.stream().filter(item -> {
                        return ObjectUtil.equals(prentBillId, item.getLastBillId());
                    }).collect(Collectors.toList());
                    orderPlan.setOrders(collect);
                    orderPlan.setBillStatus(2);
                    for (OrderPlan exsitsPlan : exsitsPlans) {
                        if (ObjectUtil.equals(exsitsPlan.getBillStatus(), 1)) {
                            orderPlan.setBillStatus(1);
                            break;
                        }
                    }
                } else {
                    orderPlan.setBillStatus(0);
                }
                orderPlan.setChirds(parseChilds2(orderPlan.getChirds(), planMap, orderPlan.getBillId()));
            }
        }
        return chirds;
    }

    public Result finishPlan(OrderPlan entity) {
        boolean b = baseRepository.update(new UpdateWrapper<OrderPlan>().set("bill_status", 2).eq("bill_id", entity.getBillId()).eq("bill_code", entity.getBillCode()));
        return b ? Result.success() : Result.error("201", "操作失败");
    }
}