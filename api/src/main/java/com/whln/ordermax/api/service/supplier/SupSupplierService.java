package com.whln.ordermax.api.service.supplier;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.sys.impl.SysService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.param.SupSupplierImportVO;
import com.whln.ordermax.data.repository.EmInboxRepository;
import com.whln.ordermax.data.repository.SupContactsEmailRepository;
import com.whln.ordermax.data.repository.SupSupplierContactsRepository;
import com.whln.ordermax.data.repository.SupSupplierRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 供应商业务处理
 */
@Service
public class SupSupplierService {
    @Resource
    private SupSupplierRepository baseRepository;

    @Resource
    private SupSupplierContactsRepository contactsRepository;

    @Resource
    private SupContactsEmailRepository emailRepository;

    @Resource
    private EmInboxRepository inboxRepository;

    /**
     * 查询所有
     */
    public Result listAll(SupSupplier entity) {
//        customerId();
        return Result.success(baseRepository.listByEntity(entity));
    }

    @Resource
    SupSupplierContactsRepository supSupplierContactsRepository;
    @Resource
    SupContactsEmailRepository supContactsEmailRepository;
    //为了改变客户id适配金蝶  慎用
    private void customerId() {
        List<SupSupplier> cuCustomers = baseRepository.list();
        List<SupSupplierContacts> contactsList = supSupplierContactsRepository.list();
        List<SupContactsEmail> cuContactsEmailList = supContactsEmailRepository.list();

        List<SupSupplierContacts> newContactsList = new ArrayList<>();
        List<SupContactsEmail> cuContactsEmails = new ArrayList<>();

        Map<String, List<SupSupplierContacts>> contactByCustomerId = contactsList.stream().filter(item->ObjectUtil.isNotEmpty(item.getSupplierId())).collect(Collectors.groupingBy(SupSupplierContacts::getSupplierId));
        Map<String, List<SupContactsEmail>> collect1 = cuContactsEmailList.stream().filter(item->ObjectUtil.isNotEmpty(item.getContactId())).collect(Collectors.groupingBy(SupContactsEmail::getContactId));
        List<SupSupplier> newCustomer = cuCustomers.stream().map(e -> {
            SupSupplier cuCustomer = BeanUtil.copyProperties(e, SupSupplier.class);
            cuCustomer.setId(sysService.nextId());
            List<SupSupplierContacts> contactsList1 = contactByCustomerId.get(e.getId());
            if (ObjectUtil.isNotEmpty(contactsList1)) {
                List<SupSupplierContacts> collect = contactsList1.stream().map(i -> {
                    i.setSupplierId(cuCustomer.getId());
                    SupSupplierContacts contacts = BeanUtil.copyProperties(i, SupSupplierContacts.class);
                    contacts.setId(sysService.nextId());
                    List<SupContactsEmail> cuContactsEmails1 = collect1.get(i.getId());
                    if (ObjectUtil.isNotEmpty(cuContactsEmails1)) {
                        List<SupContactsEmail> collect2 = cuContactsEmails1.stream().peek(c -> {
                            c.setId(sysService.nextId());
                            c.setContactId(contacts.getId());
                            c.setSupplierId(cuCustomer.getId());
                        }).collect(Collectors.toList());
                        cuContactsEmails.addAll(collect2);
                    }
                    return contacts;
                }).collect(Collectors.toList());
                newContactsList.addAll(collect);
            }
            return cuCustomer;
        }).collect(Collectors.toList());
        baseRepository.removeByIds(cuCustomers.stream().map(SupSupplier::getId).collect(Collectors.toList()));
        baseRepository.saveBatch(newCustomer);
        supSupplierContactsRepository.removeByIds(contactsList.stream().map(SupSupplierContacts::getId).collect(Collectors.toList()));
        supSupplierContactsRepository.saveBatch(newContactsList);
        supContactsEmailRepository.removeByIds(cuContactsEmailList.stream().map(SupContactsEmail::getId).collect(Collectors.toList()));
        supContactsEmailRepository.saveBatch(cuContactsEmails);
    }
    /**
     * 分页查询所有
     */
    public Result pageAll(SupSupplier entity, Page<SupSupplier> page) {
        if (ShiroManager.hasRole("customer")) {
            throw new BusinessException("无权限查看");
        }
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    public Result save(SupSupplier entity) {
        SupSupplier supSupplier = baseRepository.getOne(new LambdaQueryWrapper<SupSupplier>().eq(SupSupplier::getSupplierNo, entity.getSupplierNo()));
        if (ObjectUtil.isNotEmpty(supSupplier)&&!ObjectUtil.equals(entity.getId(),supSupplier.getId())) {
            throw new BusinessException("编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        if (CollectionUtil.isNotEmpty(entity.getContactsDelIdList())) {
            contactsRepository.removeByIds(entity.getContactsDelIdList());
        }
        if (CollectionUtil.isNotEmpty(entity.getContactsList())) {
            contactsRepository.saveOrUpdateBatch(entity.getContactsList());
        }

        if (CollectionUtil.isNotEmpty(entity.getEmailDelIds())) {
            emailRepository.removeByIds(entity.getEmailDelIds());
        }
        if (CollectionUtil.isNotEmpty(entity.getEmails())) {
//            emailRepository.saveOrUpdateBatch(entity.getEmails());
            List<HashMap<String, String>> emailMapList = entity.getEmails().stream().map(e -> {
                emailRepository.saveOrUpdate(e);
                return new HashMap<String, String>() {{
                    put("opppsiteContactsId", e.getContactId());
                    put("oppositeEmail", e.getEmailAddress());
                }};
            }).collect(Collectors.toList());
            //绑定邮箱
            inboxRepository.updateInboxBind(entity.getId(), emailMapList, "SUP");
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    public Result delete(String id) {
        baseRepository.update(new LambdaUpdateWrapper<SupSupplier>().set(SupSupplier::getIsDelete, 1).eq(SupSupplier::getId, id));

        return Result.success();
    }

    /**
     * 批量删除
     */
    public Result batchDelete(List<String> ids) {
        baseRepository.update(new LambdaUpdateWrapper<SupSupplier>().set(SupSupplier::getIsDelete, 1).in(SupSupplier::getId, ids));

        return Result.success();
    }


    public SupSupplierImportVO checkRepeat(List<SupSupplier> cmCommodityList) {
        List<String> dataBase = baseRepository.listObjs(new LambdaQueryWrapper<SupSupplier>()
                .select(SupSupplier::getSupplierNo), Objects::toString);
        List<SupSupplier> exits = new ArrayList<>(10);
        List<SupSupplier> noExits = new ArrayList<>(10);
        for (SupSupplier c : cmCommodityList) {
            if (dataBase.contains(c.getSupplierNo())) {
                exits.add(c);
            } else {
                noExits.add(c);
            }
        }
        SupSupplierImportVO cmCommodityCheckVO = new SupSupplierImportVO();
        cmCommodityCheckVO.setExits(exits);
        cmCommodityCheckVO.setNoExits(noExits);
        cmCommodityCheckVO.setExitsNum(exits.size());
        cmCommodityCheckVO.setNoExitsNum(noExits.size());

        return cmCommodityCheckVO;
    }

    @Resource
    SysService sysService;

    @Transactional(rollbackFor = Exception.class)
    public List<SupSupplier> batchImport(List<SupSupplier> supSupplierList) {
        List<String> dataBase = baseRepository.listObjs(new LambdaQueryWrapper<SupSupplier>()
                .select(SupSupplier::getSupplierNo), Objects::toString);
        List<SupSupplier> notExits = supSupplierList.stream()
                .filter(item -> !dataBase.contains(item.getSupplierNo()))
                .peek(item -> {
                    item.setOwnerId(ShiroManager.getUserId());
                    item.setCreateDate(LocalDate.now());
                })
                .collect(Collectors.toList());
        List<SupSupplier> supplierList = new ArrayList<>();
        List<SupSupplierContacts> contacts = new ArrayList<>();
        List<SupContactsEmail> cuContactsEmails = new ArrayList<>();
        for (SupSupplier supSupplier : notExits) {
            SupSupplier supplier = BeanUtil.copyProperties(supSupplier, SupSupplier.class);
            supplier.setId(sysService.nextId());
            supplierList.add(supplier);
            List<SupSupplierContacts> contactsList = supSupplier.getContactsList();
            for (SupSupplierContacts cc : contactsList) {
                cc.setSupplierId(supplier.getId());
                cc.setId(sysService.nextId());
                cc.setCreateTime(LocalDateTime.now());
                cc.setUpdateTime(LocalDateTime.now());
                contacts.add(cc);
                List<SupContactsEmail> emails = cc.getEmails();
                for (SupContactsEmail ce : emails) {
                    ce.setSupplierId(supplier.getId());
                    ce.setContactId(cc.getId());
                    cuContactsEmails.add(ce);
                }
            }
        }
        if (CollectionUtil.isNotEmpty(cuContactsEmails)) {
            Map<String, List<SupContactsEmail>> collect1 = cuContactsEmails.stream().collect(Collectors.groupingBy(SupContactsEmail::getSupplierId));
            collect1.forEach((k, v) -> {
                List<HashMap<String, String>> emailMapList = v.stream().map(e -> {
                    emailRepository.saveOrUpdate(e);
                    return new HashMap<String, String>() {{
                        put("opppsiteContactsId", e.getContactId());
                        put("oppositeEmail", e.getEmailAddress());
                        put("supplierId", e.getSupplierId());
                    }};
                }).collect(Collectors.toList());
                //绑定邮箱
                inboxRepository.updateInboxBind(k, emailMapList, "SUP");
            });
        }
        baseRepository.saveBatch(supplierList);
        contactsRepository.saveBatch(contacts);
        supSupplierList.removeAll(notExits);
        return supSupplierList;
    }
}