package com.whln.ordermax.api.service.transportation;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.TrDespatchAdviceCommodity;
import com.whln.ordermax.data.repository.TrDespatchAdviceCommodityRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  发运通知单商品业务处理
 */
@Service
public class TrDespatchAdviceCommodityService{
    @Resource
    private TrDespatchAdviceCommodityRepository baseRepository;

    /**
    *  查询所有
    */
    public Result listAll(TrDespatchAdviceCommodity entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(TrDespatchAdviceCommodity entity, Page<TrDespatchAdviceCommodity> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(TrDespatchAdviceCommodity entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

}