package com.whln.ordermax.api.controller.oa;

import com.whln.ordermax.api.service.oa.BacklogService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.OaRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@Api(tags = "我的待办")
@RestController
@RequestMapping("/backlog")
public class BacklogController{

    @Resource
    private BacklogService baseService;

    @ApiOperation(value = "获取待办总数和各模块数量")
    @PostMapping("/listCounts")
    public Result<Map> listCounts(){
        return baseService.listCounts();
    }

    @ApiOperation(value = "获取待办信息")
    @PostMapping("/list")
    public Result list(@RequestBody  Map map){
        return baseService.list(map);
    }

    @ApiOperation(value = "获取待办信息")
    @PostMapping("/listByUser")
    public Result listByUser(@RequestBody()Map map){
        return baseService.list(map);
    }

    @ApiOperation(value = "单据审批通过")
    @PostMapping("/billPass")
    public Result billPass(@RequestBody() OaRecord entity){
        return baseService.billPass(entity);
    }

    @ApiOperation(value = "单据驳回")
    @PostMapping("/billReject")
    public Result billReject(@RequestBody() OaRecord entity){
        return baseService.billReject(entity);
    }

    @ApiOperation(value = "单据补充材料")
    @PostMapping("/billReplenish")
    public Result billReplenish(@RequestBody() OaRecord entity) throws ClassNotFoundException {
        return baseService.billReplenish(entity);
    }
}
