package com.whln.ordermax.api.controller.whole;

import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.api.controller.auth.Excel2HtmlUtil;
import com.whln.ordermax.api.controller.auth.WordToHtml;
import com.whln.ordermax.api.service.whole.FileService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.param.FileDownloadParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 *
 */
@Api(tags = "上传")
@RestController
@RequestMapping("/file")
@Slf4j
public class FileController {

    @Resource
    private FileService baseService;

    @ApiOperation(value = "静态资源上传")
    @PostMapping("/upload")
    public Result upload(@RequestParam("file") MultipartFile file) {
        return baseService.upload(file);
    }

    @ApiOperation(value = "文件下载")
    @PostMapping("/download")
    public void download(@RequestBody FileDownloadParam param, HttpServletResponse response) throws IOException {
        baseService.download(param, response);
    }


    @PostMapping("/submitted/upload")
    public Result submittedUpload(@RequestParam("file") MultipartFile file, @RequestParam("emailAddress") String emailAddress, @RequestParam("submittedId") String submittedId) {
        return baseService.submittedUpload(file, emailAddress, submittedId);
    }

    @ApiOperation("生成二维码返回路径")
    @PostMapping("/qrcode")
    public Result<List<String>> qrcode(@RequestParam("datas") List<String> datas) {
        return baseService.qrcode(datas);
    }

    @ApiOperation("返回二维码")
    @PostMapping("/findQrcode")
    public void findQrcode(@RequestParam("path") String path, HttpServletResponse response) throws IOException {
        baseService.findQrcode(path, response);
    }

    @Resource
    WordToHtml wordToHtml;

    @ApiOperation("excel转换为html")
    @PostMapping("/excelToHtml")
    public void excelToHtml(@RequestParam("file") MultipartFile file, HttpServletResponse response) throws Exception {
        ServletOutputStream outputStream = response.getOutputStream();
        String originalFilename = file.getOriginalFilename();
        String substring = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
        if (ObjectUtil.equals(substring, "xlsx") || ObjectUtil.equals(substring, "xls")) {
            InputStream inputStream = file.getInputStream();
            Excel2HtmlUtil.readExcelToHtml(inputStream, outputStream, true, substring, "excel转html");
        } else if (ObjectUtil.equals(substring, "docx") || ObjectUtil.equals(substring, "doc")) {
            wordToHtml.convert(file, originalFilename, response.getOutputStream());
        }
    }
}
