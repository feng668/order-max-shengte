package com.whln.ordermax.api.service.oa;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.auth.AuthService;
import com.whln.ordermax.data.service.MapCache;
import com.whln.ordermax.api.service.whole.OrderPlanService;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * oa待办
 */
@Slf4j
@Service
public class BacklogService {
    @Resource
    private OaRuleLevelAssessorRepository assessorRepository;

    @Resource
    private ApplicationContext applicationContext;


    @Resource
    private OaRecordRepository oaRecordRepository;

    @Resource
    private OaRuleLevelAssessorRepository ruleLevelAssessorRepository;

    @Resource
    private OaRuleRepository ruleRepository;

    @Resource
    private DynamicRepository dynamicRepository;

    @Resource
    private OaRuleLevelRepository ruleLevelRepository;

    @Resource
    private OrderPlanService orderPlanService;
    @Resource
    private MapCache mapService;

    public Result<Map> listCounts() {
        assessorRepository.count(new QueryWrapper<OaRuleLevelAssessor>().eq("1", 1));
        String userId = AuthService.getUserIdFromAuthentication();
        List<OaRuleLevelAssessor> ruleLevelList = assessorRepository.listByEntity(
                new OaRuleLevelAssessor() {{
                    if (!ShiroManager.hasRole("webadmin")) {
                        setAssessorId(userId);
                    }
                }}
        );
        Map<String, Integer> countMap = ruleLevelList.stream().map(l -> {
            String billCode = l.getBillCode();

            Integer countByOaLevel = 0;
            try {
                Map<String, String> codeFullRepositoryMap = mapService.codeFullRepositoryMap();
                Class<?> repositoryClazz = Class.forName(codeFullRepositoryMap.get(billCode));
                Object repsitory = applicationContext.getBean(repositoryClazz);
                countByOaLevel = (Integer) repositoryClazz.getMethod("count", Wrapper.class).invoke(
                        repsitory,
                        new QueryWrapper().last(
                                StrUtil.format(
                                        "where oa_level = {} " +
                                                "and oa_status = 1 " +
                                                "and id not in (" +
                                                "select " +
                                                "bill_id " +
                                                "from " +
                                                "oa_record " +
                                                "where " +
                                                "record_type in ('PASS','REJECT') " +
                                                "and operator_id = '{}' " +
                                                "and oa_level = {} " +
                                                "and oa_round = (select MAX(oa_round) from oa_record) " +
                                                "and bill_code = '{}'" +
                                                ")",
                                        l.getOaLevelNum(),
                                        userId,
                                        l.getOaLevelNum(),
                                        billCode
                                )
                        )
                );
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            Integer count = countByOaLevel;
            return new HashMap() {{
                put("billCode", billCode);
                put("countByOaLevel", count);
            }};
        }).collect(Collectors.toMap(m -> (String) m.get("billCode"), m -> (Integer) m.get("countByOaLevel"), (k1, k2) -> k1 + k2));
        return Result.success(new HashMap<String, Object>(5) {{
            put("total", countMap.values().stream().mapToInt(i -> i).sum());
            put("countMap", countMap);
        }});
    }

    public Map<String, Integer> queryOrderNum(Integer oaType) {
        String userId = ShiroManager.getUserId();
        OaRuleLevelAssessor oaRuleLevelAssessor = new OaRuleLevelAssessor();
        if (ObjectUtil.isNotEmpty(oaType)) {
            oaRuleLevelAssessor.setAssessorId(userId);
        }
        List<OaRuleLevelAssessor> ruleLevelList = assessorRepository.listByEntity(oaRuleLevelAssessor);
        Map<String, List<OaRuleLevelAssessor>> collect = ruleLevelList.stream().collect(Collectors.groupingBy(OaRuleLevelAssessor::getBillCode));
        List<Map<String, Object>> list = new ArrayList<>();
        collect.forEach((k, v) -> {
            String billCode = k;
            String collect1 = v.stream().map(i -> i.getOaLevelNum().toString()).collect(Collectors.joining(","));
            Integer countByOaLevel = 0;
            try {
                Map<String, String> codeFullRepositoryMap = mapService.codeFullRepositoryMap();
                String cls = codeFullRepositoryMap.get(billCode);
                if (ObjectUtil.isEmpty(cls)){
                    log.error("billCode错误");
                    throw new BusinessException("单据代码错误");
                }
                Class<?> repositoryClazz = Class.forName(cls);
                Object repsitory = applicationContext.getBean(repositoryClazz);
                String sql = "where 1=1 and oa_level in ({}) " +
                        "and oa_status =1 " +
                        "and id not in (" +
                        "select " +
                        "bill_id " +
                        "from " +
                        "oa_record " +
                        "where " +
                        "record_type not in ('PASS','REJECT') " +
                        "and operator_id = '{}' " +
                        "and oa_level in ({}) " +
                        "and oa_round = (select MAX(oa_round) from oa_record) " +
                        "and bill_code = '{}'" +
                        ")";
                String format = StrUtil.format(
                        sql,
                        collect1,
                        userId,
                        collect1,
                        billCode
                );
                countByOaLevel = (Integer) repositoryClazz.getMethod("count", Wrapper.class).invoke(
                        repsitory,
                        new QueryWrapper()
                                .last(format)
                );
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            Integer count = countByOaLevel;
            Map<String, Object> map1 = new HashMap<>();
            map1.put("billCode", k);
            map1.put("countByOaLevel", count);
            list.add(map1);
        });
        Map<String, Integer> collect1 = list.stream().collect(Collectors.toMap(m -> (String) m.get("billCode"), m -> (Integer) m.get("countByOaLevel"), (x, y) -> x));
        return collect1;
        /*return ruleLevelList.stream().map(l -> {
            String billCode = l.getBillCode();
            Integer countByOaLevel = 0;
            try {
                Map<String, String> codeFullRepositoryMap = mapService.codeFullRepositoryMap();
                Class<?> repositoryClazz = Class.forName(codeFullRepositoryMap.get(billCode));
                Object repsitory = applicationContext.getBean(repositoryClazz);
                String sql = "where 1=1 and oa_level = {} " +
                        "and oa_status =1 " +
                        "and id not in (" +
                        "select " +
                        "bill_id " +
                        "from " +
                        "oa_record " +
                        "where " +
                        "record_type not in ('PASS','REJECT') " +
                        "and operator_id = '{}' " +
                        "and oa_level = {} " +
                        "and oa_round = (select MAX(oa_round) from oa_record) " +
                        "and bill_code = '{}'" +
                        ")";
                String format = StrUtil.format(
                        sql,
                        l.getOaLevelNum(),
                        userId,
                        l.getOaLevelNum(),
                        billCode
                );
                countByOaLevel = (Integer) repositoryClazz.getMethod("count", Wrapper.class).invoke(
                        repsitory,
                        new QueryWrapper()
                                .last(format)
                );
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            Integer count = countByOaLevel;
            return new HashMap() {{
                put("billCode", billCode);
                put("countByOaLevel", count);
            }};
        }).collect(Collectors.toMap(m -> (String) m.get("billCode"), m -> (Integer) m.get("countByOaLevel"), (x,y)->x));*/
    }

    @Resource
    private OaRuleRepository oaRuleRepository;
    @Resource
    private OaRuleLevelRepository oaRuleLevelRepository;

    public Result list(Map map) {
        String billCode = (String) map.get("billCode");
        if (ObjectUtil.isEmpty(billCode)) {
            return Result.error("205", "缺少单据类型");
        }
        OaRule one = oaRuleRepository.getOne(new LambdaQueryWrapper<OaRule>().eq(OaRule::getBillCode, billCode));
        if (ObjectUtil.isEmpty(one)) {
            throw new BusinessException("审核规则不存在");
        }
        String userId = ShiroManager.getUserId();
        OaRuleLevelAssessor oaRuleLevelAssessor = new OaRuleLevelAssessor();
        oaRuleLevelAssessor.setAssessorId(userId);
        oaRuleLevelAssessor.setBillCode(billCode);
        oaRuleLevelAssessor.setRuleId(one.getId());
        List<OaRuleLevelAssessor> assessorList = assessorRepository.listByEntity(oaRuleLevelAssessor);
        Page pageList = null;
        if (assessorList.size() > 0) {
            OaRuleLevelAssessor ruleLevel = assessorList.get(0);
            try {
                Map<String, String> codeFullRepositoryMap = mapService.codeFullRepositoryMap();
                Map<String, String> codeFullEntityMap = mapService.codeFullEntityMap();
                Class<?> repositoryClazz = Class.forName(codeFullRepositoryMap.get(billCode));
                Class<?> entityClazz = Class.forName(codeFullEntityMap.get(billCode));
                Object repsitory = applicationContext.getBean(repositoryClazz);
                Map entity = (Map) map.get("entity");
                entity.put("getOaPendding", StrUtil.format(
                        "oa_level = {} " +
                                "and oa_status = 1 " +
                                "and id not in (select bill_id from oa_record where record_type not in ('PASS','REJECT') " +
                                "and operator_id = '{}' " +
                                "and oa_level = {} " +
                                "and oa_round = (" +
                                "select MAX(oa_round) from oa_record) " +
                                "and bill_code = '{}'" +
                                ")", ruleLevel.getOaLevelNum(), userId, ruleLevel.getOaLevelNum(), billCode
                ));
                pageList = (Page) repositoryClazz.getMethod("pageByEntity", entityClazz, Page.class).
                        invoke(repsitory, BeanUtil.mapToBean(entity, entityClazz, false), new Page<>((int) map.get("current"), (int) map.get("size")));
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return Result.success(pageList);
    }


    @Transactional(rollbackFor = Exception.class)
    public Result billPass(OaRecord entity) {
        String userId = ShiroManager.getUserId();

        entity.setOperatorId(userId);
        String billCode = entity.getBillCode();
        if (ObjectUtil.isEmpty(billCode)) {
            throw new BusinessException("缺少单据类型");
        }
        String billId = entity.getBillId();
        if (ObjectUtil.isEmpty(billId)) {
            throw new BusinessException("缺少单据id");
        }
        Integer oaLevel = entity.getOaLevel();
        if (null == oaLevel) {
            throw new BusinessException("缺少审批级别");
        }
        Integer oaRound = oaRecordRepository.getObj(
                new QueryWrapper<OaRecord>().select("IFNULL(MAX(oa_round),1) AS oa_round").eq("bill_id", billId)
                        .eq("bill_code", billCode), o -> Integer.valueOf(o.toString())
        );

        //获取审批级别名称
        OaRule rule = ruleRepository.getOne(new QueryWrapper<OaRule>().eq("bill_code", billCode));
        OaRuleLevel ruleLevel = ruleLevelRepository.getOne(
                new QueryWrapper<OaRuleLevel>()
                        .eq("rule_id", rule.getId())
                        .eq("oa_level_num", oaLevel)
        );
        List<OaRuleLevelAssessor> list = ruleLevelAssessorRepository.list(new LambdaQueryWrapper<OaRuleLevelAssessor>().eq(OaRuleLevelAssessor::getRoleLevelId, ruleLevel.getId()));
        Set<String> collect = list.stream().map(OaRuleLevelAssessor::getAssessorId).collect(Collectors.toSet());
        if (!collect.contains(userId)) {
            throw new BusinessException("当前用户没有这个单据审批的权限");
        }
        entity.setOaLevelName(ruleLevel.getOaLevelName());
        entity.setOaRound(oaRound);
        entity.setRecordType("PASS");
        Boolean b = oaRecordRepository.save(entity);
        int passCount = oaRecordRepository.count(
                new QueryWrapper<OaRecord>()
                        .eq("bill_id", billId)
                        .eq("bill_code", billCode)
                        .eq("oa_level", oaLevel)
                        .eq("oa_round", oaRound)
        );
        OaRule oaRule = ruleRepository.getOne(new QueryWrapper<OaRule>().eq("bill_code", billCode));
        if (ObjectUtil.isEmpty(oaRule)) {
            throw new BusinessException("当前单据没有审核规则");
        }
        int ndPassCount = ruleLevelAssessorRepository.count(
                new QueryWrapper<OaRuleLevelAssessor>()
                        .eq("rule_id", oaRule.getId())
                        .eq("oa_level_num", oaLevel)
        );
        Map<String, String> codeTableNameMap = mapService.codeTableNameMap();
        String tableName = codeTableNameMap.get(billCode);
        //获取单据
        //控制会签和非会签
        if ("all".equals(ruleLevel.getPassType())) {
            if (passCount >= ndPassCount) {
                if (oaRule.getTotalLevel() <= oaLevel) {
                    dynamicRepository.updateBillOaStatus(tableName, billId, "addOne", 2);
                    //审核通过即完成当前进度
                    orderPlanService.finishPlan(
                            new OrderPlan() {{
                                setBillId(billId);
                                setBillCode(billCode);
                            }}
                    );
                } else {
                    dynamicRepository.updateBillOaStatus(tableName, billId, "addOne", null);
                }
            }
        } else {
            if (oaRule.getTotalLevel() <= oaLevel) {
                dynamicRepository.updateBillOaStatus(tableName, billId, "addOne", 2);
                //审核通过即完成当前进度
                orderPlanService.finishPlan(
                        new OrderPlan() {{
                            setBillId(billId);
                            setBillCode(billCode);
                        }}
                );
            } else {
                dynamicRepository.updateBillOaStatus(tableName, billId, "addOne", null);
            }
        }


        return b ? Result.success() : Result.error("201", "操作失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result billReject(OaRecord entity) {
        String userId = AuthService.getUserIdFromAuthentication();
        entity.setOperatorId(userId);
        String billCode = entity.getBillCode();
        if (null == billCode) {
            Result.error("205", "缺少单据类型");
        }
        String billId = entity.getBillId();
        if (null == billId) {
            Result.error("205", "缺少单据id");
        }
        Integer oaLevel = entity.getOaLevel();
        if (null == oaLevel) {
            Result.error("205", "缺少审批级别");
        }
        Integer oaRound = oaRecordRepository.getObj(
                new QueryWrapper<OaRecord>().select("IFNULL(MAX(oa_round),1) AS oa_round").eq("bill_id", billId)
                        .eq("bill_code", billCode), o -> Integer.valueOf(o.toString())
        );
        entity.setOaRound(oaRound);

        //获取审批级别名称
        OaRule rule = ruleRepository.getOne(new LambdaQueryWrapper<OaRule>().eq(OaRule::getBillCode, billCode));
        OaRuleLevel ruleLevel = ruleLevelRepository.getOne(
                new QueryWrapper<OaRuleLevel>()
                        .eq("rule_id", rule.getId())
                        .eq("oa_level_num", oaLevel)
        );
        entity.setOaLevelName(ruleLevel.getOaLevelName());

        entity.setRecordType("REJECT");
        Boolean b = oaRecordRepository.save(entity);
        Map<String, String> codeTableNameMap = mapService.codeTableNameMap();
        dynamicRepository.updateBillOaStatus(codeTableNameMap.get(billCode), billId, "toZero", 0);

        return b ? Result.success() : Result.error("201", "操作失败");
    }

    @Transactional(rollbackFor = Exception.class)
    public Result billReplenish(OaRecord entity) throws ClassNotFoundException {
        String userId = AuthService.getUserIdFromAuthentication();
        entity.setOperatorId(userId);
        String billCode = entity.getBillCode();
        if (ObjectUtil.isEmpty(billCode)) {
            throw new BusinessException("缺少单据类型");
        }
        String billId = entity.getBillId();
        if (ObjectUtil.isEmpty(billId)) {
            throw new BusinessException("缺少单据id");
        }
        Integer oaLevel = entity.getOaLevel();
        if (ObjectUtil.isEmpty(oaLevel)) {
            throw new BusinessException("缺少审批级别");
        }
        Integer oaRound = oaRecordRepository.getObj(
                new QueryWrapper<OaRecord>().select("IFNULL(MAX(oa_round),1) AS oa_round").eq("bill_id", billId)
                        .eq("bill_code", billCode), o -> Integer.valueOf(o.toString())
        );
        entity.setOaRound(oaRound + 1);
        Map<String, String> codeFullRepositoryMap = mapService.codeFullRepositoryMap();
        String cls = codeFullRepositoryMap.get(billCode);
        if (ObjectUtil.isEmpty(cls)) {
            throw new BusinessException("单据类型错误");
        }
        Object bean = applicationContext.getBean(Class.forName(cls));
        Object bill = ReflectUtil.invoke(bean, "getById", entity.getBillId());
        Object ownerId = ReflectUtil.getFieldValue(bill, "ownerId");
        if (!ObjectUtil.equals(ownerId, userId)) {
            throw new BusinessException("只允许单据的拥有人提交");
        }
        entity.setRecordType("REPLENISH");
        Boolean b = oaRecordRepository.save(entity);
        Map<String, String> codeTableNameMap = mapService.codeTableNameMap();
        String s = codeTableNameMap.get(billCode);
        if (ObjectUtil.isEmpty(s)) {
            throw new BusinessException("单据类型不存在");
        }
        dynamicRepository.updateBillOaStatus(codeTableNameMap.get(billCode), billId, "toZero", 1);

        return b ? Result.success() : Result.error("201", "操作失败");
    }
}