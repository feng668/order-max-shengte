package com.whln.ordermax.api.service.finance;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.FnPaymentBill;
import com.whln.ordermax.data.domain.FnPaymentRequest;
import com.whln.ordermax.data.domain.PoContract;
import com.whln.ordermax.data.repository.FnPaymentBillRepository;
import com.whln.ordermax.data.repository.FnPaymentRequestRepository;
import com.whln.ordermax.data.repository.PoContractRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 *  付款单业务处理
 */
@Service
public class FnPaymentRequesService {
    @Resource
    private FnPaymentRequestRepository baseRepository;

    @Resource
    private FnPaymentBillRepository billRepository;

    @Resource
    private PoContractRepository poContractRepository;

    /**
    *  查询所有
    */
    public Result listAll(FnPaymentRequest entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(FnPaymentRequest entity, Page<FnPaymentRequest> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    @Transactional(rollbackFor = Exception.class)
    public Result save(FnPaymentRequest entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        if (CollectionUtil.isNotEmpty(entity.getBillDelIdList())){
            List<String> billIdList = billRepository.listObjs(
                    new QueryWrapper<FnPaymentBill>().select("bill_id")
                            .in("id", entity.getBillDelIdList())
                    , Object::toString
            );
            billRepository.removeByIds(entity.getBillDelIdList());
            poContractRepository.update(
                    new UpdateWrapper<PoContract>().lambda()
                            .set(PoContract::getIsPayment,0)
                            .in(PoContract::getId,billIdList)
            );
        }
        if (CollectionUtil.isNotEmpty(entity.getBillList())){
            List<String> billIdList = entity.getBillList().stream().map(FnPaymentBill::getBillId).collect(Collectors.toList());
            billRepository.saveOrUpdateBatch(entity.getBillList());
            poContractRepository.update(
                    new UpdateWrapper<PoContract>().lambda()
                            .set(PoContract::getIsPayment,1)
                            .in(PoContract::getId,billIdList)
            );
        }
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        baseRepository.removeById(id);

        List<String> billIdList = billRepository.listObjs(
                new LambdaQueryWrapper<FnPaymentBill>().select(FnPaymentBill::getBillId)
                        .eq(FnPaymentBill::getPaymentId, id)
                , Object::toString
        );
        billRepository.remove(new LambdaQueryWrapper<FnPaymentBill>().eq(FnPaymentBill::getPaymentId,id));
        poContractRepository.update(
                new UpdateWrapper<PoContract>().lambda()
                        .set(PoContract::getIsPayment,0)
                        .in(PoContract::getId,billIdList)
        );
        return Result.success();
    }

    /**
    *  批量删除
    */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);

        List<String> billIdList = billRepository.listObjs(
                new QueryWrapper<FnPaymentBill>().select("bill_id")
                        .in("payment_id", ids)
                , Object::toString
        );
        poContractRepository.update(
                new UpdateWrapper<PoContract>().lambda()
                        .set(PoContract::getIsPayment,0)
                        .in(PoContract::getId,billIdList)
        );
        billRepository.remove(new QueryWrapper<FnPaymentBill>().in("payment_id",ids));
        return b? Result.success(): Result.error("201","删除失败");
    }

    public Result<List<FnPaymentBill>> listPaymentBillByPaymentId(String id) {
        return Result.success(billRepository.listBySuperiorId(id));
    }
}