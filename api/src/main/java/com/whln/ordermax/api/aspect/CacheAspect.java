package com.whln.ordermax.api.aspect;

import cn.hutool.core.util.ObjectUtil;
import com.whln.ordermax.api.designpattern.strategy.redisops.RedisOpsFactory;
import com.whln.ordermax.common.annotaions.Cache;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;

/**
 * @author liurun
 * @date 2023-03-21 16:13
 */
@Aspect
@Component
@Order(3)
public class CacheAspect {
    @Pointcut("@annotation(com.whln.ordermax.common.annotaions.Cache)")
    public void point() {
    }

    @Resource
    private RedisOpsFactory redisOpsFactory;

    @Around("point()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) pjp.getSignature();
        Method method = methodSignature.getMethod();
        Cache cache = method.getAnnotation(Cache.class);
        if (ObjectUtil.isNotEmpty(cache)) {
            return redisOpsFactory.handler(pjp, cache);
        }
        return pjp.proceed();
    }
}
