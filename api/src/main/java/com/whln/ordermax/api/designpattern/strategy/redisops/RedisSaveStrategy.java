package com.whln.ordermax.api.designpattern.strategy.redisops;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.whln.ordermax.common.annotaions.Cache;
import com.whln.ordermax.common.enums.RedisOps;
import com.whln.ordermax.common.utils.AopUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-04-11 19:43
 */
@Component
public class RedisSaveStrategy extends RedisStrategy<ProceedingJoinPoint, Cache, Object> {


    @Override
    public Object handle(ProceedingJoinPoint pjp, Cache cache) throws Throwable {
        Object entity = AopUtil.getArgByName(pjp, cache.cacheParam());
        if (ObjectUtil.isEmpty(entity)||!(entity instanceof Serializable)) {
            return pjp.proceed();
        }
        boolean hasId = ReflectUtil.hasField(entity.getClass(), "id");
        if (hasId) {
            String key = cache.key().getKey();
            List<Object> list = redisClient.lRange(key, 0, -1);
            List<Object> idList = list.stream().map(item -> ReflectUtil.getFieldValue(item, "id")).collect(Collectors.toList());
            Object entityId = ReflectUtil.getFieldValue(entity, "id");
            if (ObjectUtil.isNotEmpty(entityId)) {
                if (idList.contains(entityId)) {
                    for (int i = 0; i < list.size(); i++) {
                        Object o = list.get(i);
                        Object id1 = ReflectUtil.getFieldValue(o, "id");
                        if (ObjectUtil.equals(id1, entityId)) {
                            redisClient.lChange(key, i, entity);
                            break;
                        }
                    }
                } else {
                    Object proceed = pjp.proceed();
                    redisClient.lPush(key, entity);
                    return proceed;
                }

            } else {
                Object proceed = pjp.proceed();
                redisClient.lPush(key, entity);
                return proceed;
            }
        }
        return pjp.proceed();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        factory.register(RedisOps.SAVE, this);
    }
}
