package com.whln.ordermax.api.controller.numberRole;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.numberRole.NrBillService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.NrBill;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  编号规则单据控制器
 */
@Api(tags = "编号规则单据")
@RestController
@RequestMapping("/nrBill")
public class NrBillController{

    @Resource
    private NrBillService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<NrBill>> listAll(@RequestBody NrBill entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<NrBill>> pageAll(@RequestBody PageEntity<NrBill> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() NrBill entity){
        return baseService.saveRole(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

}