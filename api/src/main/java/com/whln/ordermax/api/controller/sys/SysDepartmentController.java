package com.whln.ordermax.api.controller.sys;

import com.whln.ordermax.api.service.sys.impl.SysDepartmentService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SysDepartment;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户部门控制器
 */
@Api(tags = "用户部门")
@RestController
@RequestMapping("/SysDepartment")
public class SysDepartmentController {

    @Resource
    private SysDepartmentService baseService;

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() SysDepartment entity) {
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id) {
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids) {
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "获取部门树")
    @PostMapping("/treeData")
    public Result<List<SysDepartment>> treeData(@RequestParam("companyId") String companyId) {
        return baseService.treeData(companyId);
    }

    @ApiOperation(value = "批量保存")
    @PostMapping("/batchSave")
    public Result batchSave(@RequestBody List<SysDepartment> sysDepartmentList) {
        return baseService.batchSave(sysDepartmentList) ? Result.success() : Result.error("201", "保存失败");
    }

}