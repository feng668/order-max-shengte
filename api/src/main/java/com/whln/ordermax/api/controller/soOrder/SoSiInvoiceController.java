package com.whln.ordermax.api.controller.soOrder;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.soOrder.SoSiInvoiceService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SoSiInvoice;
import com.whln.ordermax.data.domain.SoSiInvoiceCommodity;
import com.whln.ordermax.data.domain.SoSiOrderCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  销售合同控制器
 */
@Api(tags = "内销发票")
@RestController
@RequestMapping("/SoSiInvoice")
public class SoSiInvoiceController {
    @Resource
    private SoSiInvoiceService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<SoSiInvoice>> listAll(@RequestBody SoSiInvoice entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<SoSiInvoice>> pageAll(@RequestBody PageEntity<SoSiInvoice> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody @Validated SoSiInvoice entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "查询内销发票详情")
    @GetMapping("/getSoSiOrder")
    public Result<SoSiInvoice> getSoSiInvoice(String id) {
        return Result.success(baseService.getSoSiOrder(id));
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result<Void> delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "根据发票id查询")
    @PostMapping("/listCom")
    public Result<List<SoSiOrderCommodity>> listCom(@RequestBody SoSiInvoiceCommodity entity){
        return baseService.listCom(entity);
    }

}