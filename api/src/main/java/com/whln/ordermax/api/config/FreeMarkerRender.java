package com.whln.ordermax.api.config;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.whln.ordermax.common.exception.BusinessException;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

/**
 * @descriptions: 一个模板工具
 * @author: xucl
 * @date: 2020/07/01 20:05
 * @version: 1.0
 */
@Component
@Slf4j
public class FreeMarkerRender implements InitializingBean {
    @Value("${filestore.printtemplate}")
    private String basePath;
    private Configuration cfg;

    @Override
    public void afterPropertiesSet() throws Exception {
        cfg = new Configuration(Configuration.getVersion());
        cfg.setDefaultEncoding(StandardCharsets.UTF_8.toString());
        try {
            cfg.setTemplateLoader(new FileTemplateLoader(new File(basePath), true));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 获取模板写入后的内容
     * @param templateContent 模板html里面的内容
     * @param param 需要渲染的参数
     * @return
     */
    public  Optional<String> getTplText(String templateContent, Object param) {
        try {
            StringWriter stringWriter = new StringWriter();
            Template template = new Template("模板", templateContent, cfg);
            template.process(param,stringWriter);
            return Optional.of(stringWriter.toString());
        } catch (Exception e) {
            throw new BusinessException("模板渲染异常");
        }
    }
}
