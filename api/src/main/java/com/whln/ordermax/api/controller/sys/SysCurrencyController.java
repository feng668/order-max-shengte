package com.whln.ordermax.api.controller.sys;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.sys.impl.SysCurrencyService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.SysCurrency;
import com.whln.ordermax.data.domain.excel.SysCurrencyExcelImportParam;
import com.whln.ordermax.data.domain.param.ExcelExportParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *  币种控制器
 */
@Api(tags = "币种")
@RestController
@RequestMapping("/sysCurrency")
public class SysCurrencyController{

    @Resource
    private SysCurrencyService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<SysCurrency>> listAll(@RequestBody SysCurrency entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<SysCurrency>> pageAll(@RequestBody PageEntity<SysCurrency> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<SysCurrency> getAllById(@RequestParam("id") String id){
        return baseService.getAllById(id);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() SysCurrency entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("currencyCode") String currencyCode){
        return baseService.delete(currencyCode);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> currencyCodes){
        return baseService.batchDelete(currencyCodes);
    }

    @ApiOperation(value = "查询最新期的币种")
    @PostMapping("/listNewst")
    public Result<List<SysCurrency>> listNewst(@RequestBody(required = false) SysCurrency entity){
        return baseService.listNewst(entity);
    }

    @ApiOperation(value = "查询当前币种往期信息")
    @PostMapping("/listHistory")
    public Result<List<SysCurrency>> listHistory(@RequestBody(required = false) SysCurrency entity){
        return baseService.listHistory(entity);
    }

    @ApiOperation(value = "导出")
    @PostMapping("/export")
    public void export(@RequestBody ExcelExportParam param, HttpServletResponse response){
         baseService.export(param,response);
    }

    @ApiOperation(value = "导入")
    @PostMapping("/importExcel")
    public Result<Void> importExcel(@RequestBody SysCurrencyExcelImportParam param){
        baseService.importExcel(param);
        return Result.success();
    }
}