package com.whln.ordermax.api.controller.numberRole;

import com.whln.ordermax.api.service.numberRole.NrRoleNodeService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.NrRoleNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  编号规则单据控制器
 */
@Api(tags = "编号规则")
@RestController
@RequestMapping("/nrRoleNode")
public class NrRoleNodeController{

    @Resource
    private NrRoleNodeService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<NrRoleNode>> listAll(@RequestBody NrRoleNode entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }
}