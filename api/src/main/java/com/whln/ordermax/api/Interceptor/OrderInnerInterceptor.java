package com.whln.ordermax.api.Interceptor;

import com.baomidou.mybatisplus.core.toolkit.PluginUtils;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.whln.ordermax.data.service.MapCache;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.context.ApplicationContext;
import java.sql.SQLException;
import java.util.Map;

/**排序拦截*/
public class OrderInnerInterceptor implements InnerInterceptor {

    private ApplicationContext applicationContext;

    public OrderInnerInterceptor(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }



    @Override
    public void beforeQuery(Executor executor, MappedStatement mappedStatement, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
        String mapperId = mappedStatement.getId();
        Map<String, Map<String, String>> mapperModelMap = applicationContext.getBean(MapCache.class).mapperModelMap();
        int lastIndex = mapperId.lastIndexOf(".");
        String mapperIpath = mapperId.substring(0, lastIndex);
        String methodName = mapperId.substring(lastIndex + 1);
        Map<String, String> mapperModel = mapperModelMap.get(mapperIpath);

        if (mapperModel == null) {
            return;
        }

        if (!methodName.equals("listByEntity")) {
            return;
        }

        PluginUtils.MPBoundSql mpBs = PluginUtils.mpBoundSql(boundSql);
        String oldSql = mpBs.sql();
//            mpBs.sql(oldSql+" order by id desc");
    }

}
