package com.whln.ordermax.api.controller.whole;

import com.whln.ordermax.api.service.whole.OrderPlanService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.OrderPlan;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

/**
 *  订单进度控制器
 */
@Api(tags = "订单进度")
@RestController
@RequestMapping("/orderPlan")
public class OrderPlanController{

    @Resource
    private OrderPlanService baseService;


    @ApiOperation(value = "保存进度")
    @PostMapping("/save")
    public Result save(@RequestBody() OrderPlan entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "查看进度")
    @PostMapping("/showPlan")
    public Result showPlan(@RequestBody OrderPlan entity) throws IOException {
        return baseService.showPlan(entity);
    }

    @ApiOperation(value = "完成进度")
    @PostMapping("/finishPlan")
    public Result finishPlan(@RequestBody() OrderPlan entity){
        return baseService.finishPlan(entity);
    }

}