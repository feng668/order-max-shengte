package com.whln.ordermax.api.controller.transportation;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.transportation.TrTransportationService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.TrTransportation;
import com.whln.ordermax.data.domain.TrTransportationCost;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  出运单控制器
 */
@Api(tags = "出运计划单")
@RestController
@RequestMapping("/TrTransportation")
public class TrTransportationController{

    @Resource
    private TrTransportationService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<TrTransportation>> listAll(@RequestBody TrTransportation entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<TrTransportation>> pageAll(@RequestBody PageEntity<TrTransportation> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody()@Validated TrTransportation entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "获取单据其他费用")
    @PostMapping("/listCost")
    public Result<List<TrTransportationCost>> listCost(@RequestParam("id")String id){
        return baseService.listCost(id);
    }

    @ApiOperation(value = "获取单据其他费用")
    @PostMapping("/listCostByIds")
    public Result<List<TrTransportationCost>> listCostByIds(@RequestBody() List<String> ids){
        return baseService.listCostByIds(ids);
    }

    @ApiOperation(value = "生成利潤")
    @PostMapping("/createProfit")
    public Result<List<JSONObject>> createProfit(@RequestParam("transportationId") String transportationId){
        return baseService.createProfit(transportationId);
    }

    @ApiOperation(value = "国内物流进度查询")
    @PostMapping("/traceSearch")
    public Result<List<JSONObject>> traceSearch(@RequestBody() TrTransportation entity){
        return baseService.traceSearch(entity);
    }

    @ApiOperation(value = "跨境物流进度查询")
    @PostMapping("/traceSearchInternational")
    public Result<List<JSONObject>> traceSearchInternational(@RequestBody() TrTransportation entity){
        return baseService.traceSearchInternational(entity);
    }

}