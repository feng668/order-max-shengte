package com.whln.ordermax.api.designpattern.strategy.redisops;

import com.whln.ordermax.common.annotaions.Cache;
import com.whln.ordermax.common.enums.RedisOps;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liurun
 * @date 2023-04-11 19:48
 */
@Component
public class RedisOpsFactory {
    private final Map<RedisOps, RedisStrategy<ProceedingJoinPoint, Cache, Object>> map = new HashMap<>();
    public void register(RedisOps ops,RedisStrategy<ProceedingJoinPoint, Cache, Object> strategyHandler){
        map.put(ops, strategyHandler);
    }

    public RedisStrategy<ProceedingJoinPoint, Cache, Object> get(RedisOps redisOps) {
        return map.get(redisOps);
    }

    public Object handler(ProceedingJoinPoint pjp,Cache cache) throws Throwable {
        return get(cache.ops()).handle(pjp, cache);
    }
}
