package com.whln.ordermax.api.controller.commodity;

import com.whln.ordermax.api.service.commodity.CmCommodityImgService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.CmCommodityImg;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  商品图片控制器
 */
@Api(tags = "商品图片")
@RestController
@RequestMapping("/cmCommodityImg")
public class CmCommodityImgController{

    @Resource
    private CmCommodityImgService baseService;

    @ApiOperation(value = "通过商品id查询查询图片路径")
    @PostMapping("/getClassifyByCommodityId")
    public Result<List<CmCommodityImg>> getClassifyByCommodityId(@RequestBody CmCommodityImg entity){
        return baseService.getClassifyByCommodityId(entity);
    }


}