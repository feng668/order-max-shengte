package com.whln.ordermax.api.controller.transportation;

import com.whln.ordermax.api.service.transportation.TrCustomsDeclarationCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.TrCustomsDeclarationCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  报关单商品控制器
 */
@Api(tags = "报关单商品")
@RestController
@RequestMapping("/TrCustomsDeclarationCommodity")
public class TrCustomsDeclarationCommodityController{

    @Resource
    private TrCustomsDeclarationCommodityService baseService;

    @ApiOperation(value = "根据报关单id查询")
    @PostMapping("/list")
    public Result<List<TrCustomsDeclarationCommodity>> listAll(@RequestBody TrCustomsDeclarationCommodity entity){
        return baseService.listAll(entity);
    }

}