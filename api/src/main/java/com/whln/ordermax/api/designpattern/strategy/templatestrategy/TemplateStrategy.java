package com.whln.ordermax.api.designpattern.strategy.templatestrategy;

import cn.hutool.core.bean.BeanUtil;
import com.whln.ordermax.api.config.FreeMarkerRender;
import com.whln.ordermax.api.designpattern.strategy.StrategyHandler;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.param.RenderTemplateParam;

import javax.annotation.Resource;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author liurun
 * @date 2023-02-21 9:35
 */
public abstract class TemplateStrategy<P,T,R> implements StrategyHandler<P,T, R>, InitializingBean {
    @Value("${filestore.printtemplate}")
    private String basePath;
    @Resource
    protected FreeMarkerRender freeMarkerRender;


    /**
     * 渲染模板
     * @param param
     * @param template
     * @see RenderTemplateParam
     * @return 渲染完成的html文本
     */
    @Override
    public abstract R handle(P param, T template);

    /**
     * 读取html文件
     *
     * @param path 文件相对路径 模块+文件名
     * @return String 模板的内容
     */
    protected String getTemplateContent(String path) throws IOException {
        File file = new File(basePath + "/" + path);
        InputStreamReader reader;
        try {
            reader = new InputStreamReader(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new BusinessException("未找到对应模板");
        }
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuilder sb = new StringBuilder();

        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line);
        }
        return sb.toString();
    }

    protected String transitionHtml2(String htmlMsg) {
        Document html = Jsoup.parse(htmlMsg);
        Element body = html.body();
        Elements handerForList = body.select("[islist='islist']");
        for (Element element : handerForList) {
            Element p = element.parent();
            Element td = p.parent();
            Element tr = td.parent();
            if ("td".equals(p.tagName()) || "tr".equals(td.tagName())) {
                td.attr("islist", "islist");
                td.attr("listname", element.attr("listname"));
            } else if ("p".equals(p.tagName()) || "td".equals(td.tagName()) || "tr".equals(tr.tagName())) {
                td.attr("islist", "islist");
                td.attr("listname", element.attr("listname"));
            } else {
                throw new BusinessException("商品变量位置不正确");
            }

        }
        Elements attrs = body.select("[isattr='isattr']");
        for (Element attr : attrs) {
            attr.after(attr.val());
            attr.remove();
        }
        Elements list = body.select("[islist='islist']");
        HashMap<String, String> replaceMap = new HashMap<>();
        for (Element element : list) {
            String listName = element.attr("listName");
            String elementStr = element.toString();
            String parseStr = "\n<#if " + listName + "?? && (" + listName + "?size > 0)>" +
                    "\n<#list " + listName + " as t>\n" + elementStr + "\n</#list>\n</#if>";
            element.after("<!--#ftl#" + parseStr + "#ftl#-->");
            element.remove();

        }

        Elements qr = body.select("[isqr='isqr']");
        for (Element element : qr) {
            String elementStr = element.text();
            element.after("<img isqr='isqr' src='' qrcontent='" + elementStr + "'>");
            element.remove();

        }

        Elements qrlist = body.select("[isqrlist='isqrlist']");
        for (Element element : qrlist) {
            String listName = element.attr("listName");
            String elementStr = element.toString();
            String parseStr = "\n<#if " + listName + "?? && (" +
                    listName + "?size > 0)>\n<#list " + listName + " as t>\n"
                    + elementStr + "<br>\n</#list>\n</#if>";
            element.after("<!--#ftl#" + parseStr + "#ftl#-->");
            element.remove();
        }

        String ftlStr = html.toString();
        ftlStr = ftlStr.replaceAll("<!--#ftl#", "");
        ftlStr = ftlStr.replaceAll("#ftl#-->", "");
        return ftlStr;
    }
    public String render(Object data,String template){
        Map<String, Object> map = BeanUtil.beanToMap(data);
        Optional<String> resultOption = freeMarkerRender.getTplText(template, map);
        return resultOption.get();
    }
}
