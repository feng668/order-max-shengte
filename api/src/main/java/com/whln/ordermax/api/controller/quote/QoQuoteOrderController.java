package com.whln.ordermax.api.controller.quote;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.quote.QoQuoteOrderService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.QoQuoteOrder;
import com.whln.ordermax.data.domain.param.QoQuoteCommodityVOImportParam;
import com.whln.ordermax.data.domain.vo.QoQuoteCommodityImportVO;
import com.whln.ordermax.data.domain.vo.QoQuoteCommodityVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  报价单控制器
 */
@Api(tags = "外销报价单")
@RestController
@RequestMapping("/QoQuoteOrder")
public class QoQuoteOrderController{

    @Resource
    private QoQuoteOrderService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<QoQuoteOrder>> listAll(@RequestBody QoQuoteOrder entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<QoQuoteOrder>> pageAll(@RequestBody PageEntity<QoQuoteOrder> pageEntity) {
        return baseService.pageAll(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "保存报价单和报价商品")
    @PostMapping("/save")
    public Result<Void> save(@RequestBody() @Validated QoQuoteOrder entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "查询单条")
    @PostMapping("/get")
    public Result<List<QoQuoteOrder>> getAllById(@RequestParam("id") String id){
        return baseService.getAllById(id);
    }

    @ApiOperation(value = "检查重复的商品编号")
    @PostMapping("/checkRepeat")
    public Result<QoQuoteCommodityImportVO> checkRepeat(@RequestBody List<QoQuoteCommodityVO> cmCommodityList) {
        return Result.success(baseService.checkRepeat(cmCommodityList));
    }

    @ApiOperation(value = "批量导入")
    @PostMapping("/importCommodity")
    public Result<QoQuoteCommodityImportVO> importCommodity(@RequestBody QoQuoteCommodityVOImportParam param) {
        QoQuoteCommodityImportVO qoQuoteCommodityImportVO = baseService.importCommodity(param);
        return Result.success(qoQuoteCommodityImportVO);
    }
}