package com.whln.ordermax.api.controller.poOrder;

import com.whln.ordermax.api.service.poOrder.PoRequestCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.PoRequestCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  采购申请单商品控制器
 */
@Api(tags = "采购申请单商品")
@RestController
@RequestMapping("/PoRequestCommodity")
public class PoRequestCommodityController{

    @Resource
    private PoRequestCommodityService baseService;

    @ApiOperation(value = "根据申请单id查询")
    @PostMapping("/list")
    public Result<List<PoRequestCommodity>> listAll(@RequestBody PoRequestCommodity entity){
        return baseService.listAll(entity);
    }

}