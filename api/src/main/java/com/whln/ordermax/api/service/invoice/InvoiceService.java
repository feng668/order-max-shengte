package com.whln.ordermax.api.service.invoice;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.whln.ordermax.data.domain.Invoice;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liurun
 * @since 2023-04-23
 */
public interface InvoiceService extends IService<Invoice> {

    Page<Invoice> pageAll(Invoice entity, Page<Invoice> page);

    void add(Invoice entity);

    void update(Invoice entity);

    void delete(String id);
}
