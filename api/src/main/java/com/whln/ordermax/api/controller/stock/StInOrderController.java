package com.whln.ordermax.api.controller.stock;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.stock.StInOrderService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.StInOrder;
import com.whln.ordermax.data.domain.param.BaseCommodityNumParam;
import com.whln.ordermax.data.domain.vo.CommodityBaseNumVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  入库单控制器
 */
@Api(tags = "入库单")
@RestController
@RequestMapping("/StInOrder")
public class StInOrderController{

    @Resource
    private StInOrderService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<StInOrder>> listAll(@RequestBody StInOrder entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<StInOrder>> pageAll(@RequestBody PageEntity<StInOrder> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody @Validated StInOrder entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "选择批号")
    @GetMapping("/getCommodityBatchNo")
    public Result<CommodityBaseNumVO> getCommodityBatchNo(@Validated BaseCommodityNumParam param){
        return baseService.getCommodityBatchNo(param);
    }

}