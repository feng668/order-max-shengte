package com.whln.ordermax.api.designpattern.strategy.templatestrategy.impl;

import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategy;
import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategyFactory;
import com.whln.ordermax.common.enums.ModuleEnum;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.TmPrintTemplate;
import com.whln.ordermax.data.domain.TrCustomsDeclaration;
import com.whln.ordermax.data.domain.param.RenderTemplateParam;
import com.whln.ordermax.data.repository.TrCustomsDeclarationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author liurun
 * @date 2023-02-21 9:38
 */
@AllArgsConstructor
@Component
public class TrCustomsDeclarationTemplateStrategy extends TemplateStrategy<RenderTemplateParam, TmPrintTemplate,String>{

    private final TrCustomsDeclarationRepository repository;

    @Override
    public void afterPropertiesSet() throws Exception {
        TemplateStrategyFactory.register(ModuleEnum.TR_CUSTOMS_DECLARATION.getValue(),this);
    }

    @Override
    public String handle(RenderTemplateParam param, TmPrintTemplate templateData) {
        try {
            String content = getTemplateContent(templateData.getHtmlPath());
            String template = transitionHtml2(content);
            TrCustomsDeclaration trCustomsDeclaration = repository.getAllById(param.getId());
            return render(trCustomsDeclaration, template);
        } catch (IOException e) {
            throw new BusinessException("读取模板出错了");
        }
    }


}
