package com.whln.ordermax.api.init;

import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.whln.ordermax.api.redis.RedisClient;
import com.whln.ordermax.api.service.map.MapService;
import com.whln.ordermax.common.enums.RedisKey;
import com.whln.ordermax.data.domain.OrderPlan;
import com.whln.ordermax.data.domain.SysRole;
import com.whln.ordermax.data.repository.SysRoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-30 10:50
 */
@Slf4j
@Order(5)
@Component
public class AppStartInit implements InitializingBean {
    @Resource
    MapCache mapService;
    @Resource
    SysRoleRepository sysRoleRepository;
    @Resource
    RedisClient<String, Object> redisClient;
    @Resource
    private SysDictInit sysDictInit;
    @Override
    public void afterPropertiesSet() throws Exception {
        redisClient.flushAll("*");
        loadRoleList();
        loadOrderPlanFile();
    }


    private void loadRoleList() {
        List<SysRole> list = sysRoleRepository.list();
        if (redisClient.exists(RedisKey.SYS_ROLE_ALL.getKey())) {
            redisClient.remove(RedisKey.SYS_ROLE_ALL.getKey());
        }
        for (SysRole s : list) {
            redisClient.lPush(RedisKey.SYS_ROLE_ALL.getKey(), s);
        }
        sysDictInit.init();

    }

    /**
     * 加载单据进度 的文件
     * @throws IOException
     */
    public void loadOrderPlanFile() throws IOException {
        log.info("加载单据进度文件");
        InputStream eplanInputStream = new ClassPathResource("orderPlan/eplan.json").getStream();
        JSONObject entries = JSONUtil.parseObj(IOUtils.toString(eplanInputStream, StandardCharsets.UTF_8));
        OrderPlan orderPlane = entries.toBean(OrderPlan.class);
        redisClient.set(RedisKey.ORDER_PLANE.getKey(), orderPlane);
        InputStream dplanInputStream = new ClassPathResource("orderPlan/dplan.json").getStream();
        JSONObject entriesd = JSONUtil.parseObj(IOUtils.toString(dplanInputStream, StandardCharsets.UTF_8));
        OrderPlan orderPland = entriesd.toBean(OrderPlan.class);
        redisClient.set(RedisKey.ORDER_PLAND.getKey(), orderPland);
    }

    private void mapCacheInit() throws IllegalAccessException, InvocationTargetException {
        Class<? extends MapCache> aClass = mapService.getClass();
        Method[] declaredMethods = aClass.getMethods();
        for (Method m : declaredMethods) {
            m.invoke(mapService);
        }
    }

}
