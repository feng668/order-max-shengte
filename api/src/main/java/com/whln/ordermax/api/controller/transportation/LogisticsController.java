package com.whln.ordermax.api.controller.transportation;

import com.whln.ordermax.api.service.transportation.LogisticsService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.vo.ListLogisticsStatVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *  订单进度控制器
 */
@Api(tags = "物流")
@RestController
@RequestMapping("/logistics")
public class LogisticsController {

    @Resource
    private LogisticsService baseService;


    @ApiOperation(value = "国内物流进度查询")
    @PostMapping("/internalSearch")
    public String internalSearch(@RequestParam("trackingNo") String trackingNo,@RequestParam("logisticsCompanyCode") String logisticsCompanyCode){
        return baseService.internalSearch(trackingNo,logisticsCompanyCode);
    }

    @ApiOperation(value = "物流查询窗口")
    @PostMapping("/internalSearchFrame")
    public String internalSearchFrame(){
        return baseService.internalSearchFrame();
    }

    @ApiOperation(value = "物流统计")
    @PostMapping("/logisticsStat")
    public Result<List<ListLogisticsStatVO>> logisticsStat(@RequestBody()Map<String,String> entity){
        return baseService.logisticsStat(entity);
    }



}