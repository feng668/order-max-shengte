package com.whln.ordermax.api.controller.stock;

import com.whln.ordermax.api.service.stock.StOutOrderCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.StOutOrderCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  出库单商品控制器
 */
@Api(tags = "出库单商品")

@RestController
@RequestMapping("/stOutOrderCommodity")
public class StOutOrderCommodityController{

    @Resource
    private StOutOrderCommodityService baseService;

    @ApiOperation(value = "根据出库单id查询")
    @PostMapping("/list")
    public Result<List<StOutOrderCommodity>> listAll(@RequestBody StOutOrderCommodity entity){
        return baseService.listAll(entity);
    }

}