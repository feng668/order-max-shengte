package com.whln.ordermax.api.controller.sys;

import com.whln.ordermax.api.service.sys.UserLabelGroupService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.param.DeleteParam;
import com.whln.ordermax.data.domain.param.UserLabelGroupParam;
import com.whln.ordermax.data.domain.vo.CustomerLabelGroupVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 16:30
 */
@Api(tags = "客户标签组相关")
@RequestMapping("/labelGroup")
@AllArgsConstructor
@RestController
@Validated
public class UserLabelGroupController {
    private final UserLabelGroupService userLabelGroupService;

    @PostMapping("/save")
    @ApiOperation("保存标签组")
    public Result<Void> save(@RequestBody CustomerLabelGroupVO userLabelGroupVO) {
        userLabelGroupService.save(userLabelGroupVO);
        return Result.success();
    }

    @ApiOperation("删除标签组")
    @PostMapping("/deleteGroup")
    public Result<Void> deleteGroup(@RequestBody DeleteParam param) {
        userLabelGroupService.deleteGroup(param);
        return Result.success();
    }

    @ApiOperation("查询标签组列表")
    @PostMapping("/queryGroupList")
    public Result<List<CustomerLabelGroupVO>> queryGroupList(UserLabelGroupParam param) {
        List<CustomerLabelGroupVO> list = userLabelGroupService.list(param);
        return Result.success(list);
    }

}
