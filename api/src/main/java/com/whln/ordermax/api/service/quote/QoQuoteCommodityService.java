package com.whln.ordermax.api.service.quote;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.entity.quote.QoCommodityHistoryQueteVo;
import com.whln.ordermax.data.domain.QoQuoteCommodity;
import com.whln.ordermax.data.domain.param.ListHistoryPriceByComIdParam;
import com.whln.ordermax.data.domain.vo.LastPriceByComVO;
import com.whln.ordermax.data.repository.QoQuoteCommodityRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 *  报价商品清单业务处理
 */
@Service
public class QoQuoteCommodityService{
    @Resource
    private QoQuoteCommodityRepository baseRepository;

    /**
    *  查询所有
    */
    public Result listAll(QoQuoteCommodity entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
    *  分页查询所有
    */
    public Result pageAll(QoQuoteCommodity entity, Page<QoQuoteCommodity> page) {
        return Result.success(baseRepository.pageByEntity(entity,page));
    }

    /**
    *  插入或更新
    */
    public Result save(QoQuoteCommodity entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        return b? Result.success(): Result.error("201","保存失败");
    }

    /**
    *  单条删除
    */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b? Result.success(): Result.error("201","删除失败");
    }

    /**
    *  批量删除
    */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b? Result.success(): Result.error("201","删除失败");
    }

    public Result<IPage<QoCommodityHistoryQueteVo>> listHistoryPriceByComIdCustId(ListHistoryPriceByComIdParam param) {
        return Result.success(baseRepository.listHistoryPriceByComIdCustId(param));
    }

    public Result<LastPriceByComVO> getLastPriceByComIdAndCustId(String commodityId, String customerId) {
        LastPriceByComVO lastPriceByComVO = baseRepository.getLastPriceByComIdAndCustId(customerId, commodityId);
        return Result.success(lastPriceByComVO);
    }
}