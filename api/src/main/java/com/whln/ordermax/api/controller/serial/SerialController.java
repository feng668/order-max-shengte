package com.whln.ordermax.api.controller.serial;

import cn.hutool.core.net.NetUtil;
import cn.hutool.json.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.entity.Serial;
import com.whln.ordermax.common.utils.OmFileUtil;
import com.whln.ordermax.common.utils.SerialUtil;

import java.net.InetAddress;
import java.sql.Timestamp;

@RestController
@RequestMapping("/seria")
@Api(tags = "序列号")
public class SerialController {

    @ApiOperation("激活")
    @PostMapping("/activate")
    public Result login(@RequestBody JSONObject reqData) {
        String activateCode = reqData.getStr("activateCode");
        Boolean result = false;

        try {
            Serial serial = SerialUtil.explainSerialNumber(activateCode);
            String mac = serial.getMac();
            String effectiveEndTime = serial.getEffectiveEndTime();
            InetAddress inetAddress = InetAddress.getLocalHost();
            String localMac = NetUtil.getLocalMacAddress();
            /*获取当前时间戳*/
            long time = new Timestamp(System.currentTimeMillis()).getTime();
            /*判断有效结束时间是否大于当前时间*/
            if(!localMac.equalsIgnoreCase(mac)){
                return Result.error("600", "序列号与当前机器不匹配！");
            }else if (Long.parseLong(effectiveEndTime)<time){
                return Result.error("600", "序列号已过期！");
            }
        }catch (Exception e){
            return Result.error("600", "序列号不正确！");
        }

        try {
            result = OmFileUtil.writeStr(ResourceUtils.getURL("classpath:").getPath().replaceAll("/classes","") + "serialNumber.lic", activateCode);
            System.out.println();
        }catch (Exception e){
            e.printStackTrace();
        }
        return result? Result.success(null): Result.error("605","序列号上传失败");
    }
}
