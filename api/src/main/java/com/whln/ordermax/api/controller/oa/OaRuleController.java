package com.whln.ordermax.api.controller.oa;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.oa.OaRuleService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.OaRule;
import com.whln.ordermax.data.domain.OaRuleLevel;
import com.whln.ordermax.data.domain.vo.OaRuleListResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 *  oa定制规则控制器
 */
@Api(tags = "oa定制规则")
@RestController
@RequestMapping("/oaRule")
public class OaRuleController {

    @Resource
    private OaRuleService baseService;

    @ApiOperation(value = "查询单据类别")
    @PostMapping("/listBills")
    public Result<List<Map<String, String>>> listBills(){
        return baseService.listBills();
    }

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<OaRuleListResult> listAll(@RequestBody OaRule entity){
        return baseService.list(entity);
    }

    @ApiOperation(value = "显示用户可见的")
    @PostMapping("/listByUser")
    public Result<OaRuleListResult> listByUser(@RequestBody OaRule entity){
        return baseService.listByUser(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<OaRule>> pageAll(@RequestBody PageEntity<OaRule> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "根据ruleId查询审批级别")
    @PostMapping("/listLevelByRuleId")
    public Result<List<OaRuleLevel>> listLevelByRuleId(@RequestParam("ruleId") String ruleId){
        return baseService.listLevelByRuleId(ruleId);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() OaRule entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}