package com.whln.ordermax.api.service.stock;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.annotations.FlowControlSave;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.YesOrNo;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.StInventoryRemain;
import com.whln.ordermax.data.domain.StOutOrder;
import com.whln.ordermax.data.domain.StOutOrderCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.domain.vo.BaseInfoVO;
import com.whln.ordermax.data.mapper.StOutOrderCommodityMapper;
import com.whln.ordermax.data.repository.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 出库单业务处理
 */
@BillCode(ModelMessage.ST_OUT_ORDER)
@Service
public class StOutOrderService {
    @Resource
    private StOutOrderRepository baseRepository;

    @Resource
    private StOutOrderCommodityRepository commodityRepository;

    @Resource
    private StInventoryRemainRepository inventoryRemainRepository;

    @Resource
    private StInventoryGrabRepository inventoryGrabRepository;
    @Resource
    private StOutOrderCommodityMapper stOutOrderCommodityMapper;

    /**
     * 查询所有
     */
    public Result listAll(StOutOrder entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }



    /**
     * 分页查询所有
     */
    public Result pageAll(StOutOrder entity, Page<StOutOrder> page) {
        IPage<StOutOrder> stOutOrderPage = baseRepository.pageByEntity(entity, page);
        List<StOutOrder> records = stOutOrderPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(StOutOrder::getId).collect(Collectors.toSet());
            BillQuery param = new BillQuery();
            param.setBillIds(billIds);
            List<StOutOrderCommodity> stOutOrderCommodities = stOutOrderCommodityMapper.commodityList(param);
            Map<String, List<StOutOrderCommodity>> groupByBillId =
                    stOutOrderCommodities.stream().collect(Collectors.groupingBy(StOutOrderCommodity::getBillId));
            for (StOutOrder s : records) {
                s.setCommodityList(groupByBillId.get(s.getId()));
            }
            stOutOrderPage.setRecords(records);
        }
        return Result.success(stOutOrderPage);
    }

    @Resource
    StInventoryRepository stInventoryRepository;

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    @FlowControlSave(status = 1)
    public Result save(StOutOrder entity) {
        StOutOrder stOutOrder = baseRepository.getOne(new LambdaQueryWrapper<StOutOrder>().eq(StOutOrder::getBillNo, entity.getBillNo()));
        if (ObjectUtil.isNotEmpty(stOutOrder)&&!ObjectUtil.equals(entity.getId(),stOutOrder.getId())) {
            throw new BusinessException("编号已存在");
        }
        if (ObjectUtil.isEmpty(entity.getProceedDate())) {
            entity.setProceedDate(LocalDate.now());
        }
        List<StOutOrderCommodity> outOrderCommodityList = entity.getCommodityList();
        List<String> ids = outOrderCommodityList.stream().map(StOutOrderCommodity::getId).filter(ObjectUtil::isNotEmpty).collect(Collectors.toList());
        commodityRepository.remove(new LambdaUpdateWrapper<StOutOrderCommodity>().eq(StOutOrderCommodity::getBillId, entity.getId())
                .notIn(ObjectUtil.isNotEmpty(ids), StOutOrderCommodity::getId, ids)
        );
        List<String> commodityIdList = new ArrayList<>();
        List<StOutOrderCommodity> stOutOrderCommodityList = new ArrayList<>();
        if (CollectionUtil.isNotEmpty(outOrderCommodityList)) {
            for (StOutOrderCommodity commodity : outOrderCommodityList) {
                if (ObjectUtil.isEmpty(commodity.getWarehouseId())) {
                    throw new BusinessException("有商品没有选择仓库");
                }
                if (ObjectUtil.isEmpty(commodity.getCommodityBatchNo())) {
                    throw new BusinessException("有商品批号为空");
                }
                commodity.setBillId(entity.getId());
                stOutOrderCommodityList.add(commodity);
                commodityIdList.add(commodity.getCommodityId());
            }
            List<StInventoryRemain> stInventoryRemainList = inventoryRemainRepository.listRemainNumByOrder(commodityIdList, entity.getWarehouseId());
            inventoryRemainRepository.saveOrUpdateBatchByUnique(stInventoryRemainList);

        }

//        List<String> commodityIdList = outOrderCommodityList.stream().map(StOutOrderCommodity::getCommodityId).collect(Collectors.toList());
        List<String> batchNoList = outOrderCommodityList.stream().map(StOutOrderCommodity::getCommodityBatchNo).collect(Collectors.toList());
        StInventoryRemain stInventoryRemain = new StInventoryRemain();
        stInventoryRemain.setCommodityIdList(commodityIdList);
        stInventoryRemain.setCommodityBatchNoList(batchNoList);
        Page<BaseInfoVO> baseInfoVOPage = stInventoryRepository.baseInfo(stInventoryRemain, null);
        List<BaseInfoVO> records = baseInfoVOPage.getRecords();
        Map<String, BaseInfoVO> collect = records.stream().collect(Collectors.toMap(item -> item.getWarehouseId() + item.getCommodityId() + item.getCommodityBatchNo(), Function.identity()));
        List<StOutOrderCommodity> notEnough = outOrderCommodityList.stream().filter(item -> {
            String key = item.getWarehouseId() + item.getCommodityId() + item.getCommodityBatchNo();
            BaseInfoVO baseInfoVO = collect.get(key);
            if (ObjectUtil.isEmpty(baseInfoVO)) {
                return true;
            }
            boolean b = baseInfoVO.getFreeNum().compareTo(item.getQuotationNum()) < 0;
            return b;
        }).collect(Collectors.toList());
        if (ObjectUtil.isNotEmpty(notEnough)) {
            throw new BusinessException("商品:" + cnNameStr + "库存不够");
        }

        baseRepository.saveOrUpdate(entity);

        if (CollectionUtil.isNotEmpty(entity.getInventoryGrabList())) {
            inventoryGrabRepository.saveOrUpdateBatchByUnique(entity.getInventoryGrabList());
        }
        commodityRepository.saveOrUpdateBatch(stOutOrderCommodityList);
        if (CollectionUtil.isNotEmpty(entity.getInventoryGrabDelIdList())) {
            inventoryGrabRepository.removeByIds(entity.getInventoryGrabDelIdList());
        }

        return Result.success();
    }

    /**
     * 单条删除
     */
    @Transactional
    public Result delete(String id) {
        StOutOrder one = baseRepository.getOne(new LambdaQueryWrapper<StOutOrder>().eq(StOutOrder::getId, id));
        if ((ShiroManager.hasRole("webadmin") || ObjectUtil.equals(ShiroManager.getUserId(), one.getOwnerId())) && ObjectUtil.equals(one.getIsDelete(), YesOrNo.YES)) {
            baseRepository.removeById(id);
            commodityRepository.remove(new QueryWrapper<StOutOrderCommodity>().eq("bill_id", id));
        } else {
            baseRepository.update(new LambdaUpdateWrapper<StOutOrder>().eq(StOutOrder::getId, id).set(StOutOrder::getIsDelete, 1));
        }
        return Result.success();
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        List<StOutOrder> soSoOrders = baseRepository.listByIds(ids);
        String userId = ShiroManager.getUserId();
        long noCurrentUser = soSoOrders.stream().filter(item -> !ObjectUtil.equals(userId, item.getOwnerId())).count();
        Set<Integer> noDelete = soSoOrders.stream()
                .map(StOutOrder::getIsDelete)
                .filter(item -> ObjectUtil.equal(item, YesOrNo.NO.getState()))
                .collect(Collectors.toSet());

        if ((ShiroManager.hasRole("webadmin") || noCurrentUser < 1)) {
            if (noDelete.size() == 0) {
                baseRepository.removeByIds(ids);
                commodityRepository.remove(new LambdaQueryWrapper<StOutOrderCommodity>().in(StOutOrderCommodity::getBillId, ids));
            } else {
                baseRepository.update(new LambdaUpdateWrapper<StOutOrder>().in(StOutOrder::getId, ids).set(StOutOrder::getIsDelete, 1));
            }
        }
        return Result.success();
    }

}