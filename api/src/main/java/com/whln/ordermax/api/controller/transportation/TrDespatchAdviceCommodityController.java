package com.whln.ordermax.api.controller.transportation;

import com.whln.ordermax.api.service.transportation.TrDespatchAdviceCommodityService;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.TrDespatchAdviceCommodity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 *  发运通知单商品控制器
 */
@Api(tags = "发运通知单商品")
@RestController
@RequestMapping("/TrDespatchAdviceCommodity")
public class TrDespatchAdviceCommodityController{

    @Resource
    private TrDespatchAdviceCommodityService baseService;

    @ApiOperation(value = "通过发运通知单id查询商品")
    @PostMapping("/list")
    public Result<List<TrDespatchAdviceCommodity>> listAll(@RequestBody TrDespatchAdviceCommodity entity){
        return baseService.listAll(entity);
    }

}