package com.whln.ordermax.api.service.finance;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.FnInAccount;
import com.whln.ordermax.data.domain.FnOffsetBill;
import com.whln.ordermax.data.domain.TrTransportation;
import com.whln.ordermax.data.repository.FnInAccountRepository;
import com.whln.ordermax.data.repository.FnOffsetBillRepository;
import com.whln.ordermax.data.repository.TrTransportationRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 入帐登记业务处理
 */
@Service
public class FnInAccountService {
    @Resource
    private FnInAccountRepository baseRepository;
    @Resource
    private FnOffsetBillRepository billRepository;

    @Resource
    private TrTransportationRepository transportationRepository;

    /**
     * 查询所有
     */
    public Result listAll(FnInAccount entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    public Result pageAll(FnInAccount entity, Page<FnInAccount> page) {
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    @Transactional(rollbackFor = Exception.class)
    public Result save(FnInAccount entity) {
        boolean b = baseRepository.saveOrUpdate(entity);
        if (CollectionUtil.isNotEmpty(entity.getBillDelIdList())) {
            List<String> billIdList = billRepository.listObjs(
                    new QueryWrapper<FnOffsetBill>().select("bill_id")
                            .in("id", entity.getBillDelIdList())
                    , Object::toString
            );
            billRepository.removeByIds(entity.getBillDelIdList());
            transportationRepository.update(
                    new UpdateWrapper<TrTransportation>().lambda()
                            .set(TrTransportation::getIsOffset, 0)
                            .in(TrTransportation::getId, billIdList)
            );
        }
        if (CollectionUtil.isNotEmpty(entity.getBillList())) {
            List<String> billIdList = entity.getBillList().stream().map(FnOffsetBill::getBillId).collect(Collectors.toList());
            billRepository.saveOrUpdateBatch(entity.getBillList());
            transportationRepository.update(
                    new UpdateWrapper<TrTransportation>().lambda()
                            .set(TrTransportation::getIsOffset, 1)
                            .in(TrTransportation::getId, billIdList)
            );
        }
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);

        List<String> billIdList = billRepository.listObjs(
                new LambdaQueryWrapper<FnOffsetBill>().select(FnOffsetBill::getBillId)
                        .eq(FnOffsetBill::getInAccountId, id)
                , Object::toString
        );
        transportationRepository.update(
                new UpdateWrapper<TrTransportation>().lambda()
                        .set(TrTransportation::getIsOffset, 0)
                        .in(TrTransportation::getId, billIdList)
        );
        billRepository.remove(new QueryWrapper<FnOffsetBill>().lambda().eq(FnOffsetBill::getInAccountId, id));
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 批量删除
     */
    @Transactional(rollbackFor = Exception.class)
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);

        List<String> billIdList = billRepository.listObjs(
                new QueryWrapper<FnOffsetBill>().select("bill_id")
                        .in("in_account_id", ids)
                , Object::toString
        );
        transportationRepository.update(
                new UpdateWrapper<TrTransportation>().lambda()
                        .set(TrTransportation::getIsOffset, 0)
                        .in(TrTransportation::getId, billIdList)
        );
        billRepository.remove(new QueryWrapper<FnOffsetBill>().in("in_account_id", ids));
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    public Result<List<FnOffsetBill>> listOffsetBillByInAccountId(String id) {
        return Result.success(billRepository.listBySuperiorId(id));
    }
}