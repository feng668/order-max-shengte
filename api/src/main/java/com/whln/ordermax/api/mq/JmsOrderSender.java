package com.whln.ordermax.api.mq;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Queue;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liurun
 * @date 2023-04-08 17:29
 */
@Component
public class JmsOrderSender {
    private final JmsTemplate jmsTemplate;
    @Qualifier("order.queue")
    private final Queue queue;
    private final ThreadPoolTaskExecutor executor;

    public JmsOrderSender(JmsTemplate jmsTemplate, Queue queue, @Qualifier("taskExecutor") ThreadPoolTaskExecutor executor) {
        this.jmsTemplate = jmsTemplate;
        this.queue = queue;
        this.executor = executor;
    }

    public void send(String billCode, Object entity) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("code", billCode);
        map.put("data", entity);
        MessageObject messageObject = new MessageObject();
        messageObject.setCode(billCode);
        messageObject.setData(entity);
        ActiveMQObjectMessage activeMQObjectMessage = new ActiveMQObjectMessage();
        try {
            activeMQObjectMessage.setObject(messageObject);
        } catch (JMSException e) {
            e.printStackTrace();
        }
        executor.execute(() -> jmsTemplate.convertAndSend(queue, activeMQObjectMessage));
    }

}
