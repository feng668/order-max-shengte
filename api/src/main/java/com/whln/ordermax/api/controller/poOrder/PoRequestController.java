package com.whln.ordermax.api.controller.poOrder;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.poOrder.PoRequestService;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.PoRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  采购申请单控制器
 */
@Api(tags = "采购申请单")
@RestController
@RequestMapping("/PoRequest")
public class PoRequestController{

    @Resource
    private PoRequestService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<PoRequest>> listAll(@RequestBody PoRequest entity){
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<PoRequest>> pageAll(@RequestBody PageEntity<PoRequest> pageEntity){
        return baseService.pageAll(pageEntity.getEntity(),new Page<>(pageEntity.getCurrent(),pageEntity.getSize()));
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody()@Validated PoRequest entity){
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id){
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids){
        return baseService.batchDelete(ids);
    }

}