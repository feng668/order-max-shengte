package com.whln.ordermax.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.whln.ordermax.email.OmEmailHandle;

@Configuration
public class EmailConfig {

    @Bean
    public OmEmailHandle getOmEmailHandle(){
        return new OmEmailHandle();
    }
}
