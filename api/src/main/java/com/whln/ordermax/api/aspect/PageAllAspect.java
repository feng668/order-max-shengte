package com.whln.ordermax.api.aspect;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.annotaions.PageWithUserIgnore;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.RedisKey;
import com.whln.ordermax.common.redis.RedisClient;
import com.whln.ordermax.data.domain.OaRule;
import com.whln.ordermax.data.domain.SysRole;
import com.whln.ordermax.data.domain.SysUser;
import com.whln.ordermax.data.domain.SysUserRole;
import com.whln.ordermax.data.domain.base.FilterBill;
import com.whln.ordermax.data.repository.OaRuleRepository;
import com.whln.ordermax.data.repository.SysRoleRepository;
import com.whln.ordermax.data.repository.SysUserRoleRepository;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-03-24 10:26
 */
@Aspect
@Component
public class PageAllAspect {
    @Resource
    private SysRoleRepository sysRoleRepository;
    @Lazy
    @Resource
    private RedisClient<String, SysRole> redisClient;
    @Resource
    private SysUserRoleRepository sysUserRoleRepository;
    @Resource
    private OaRuleRepository oaRuleRepository;

    @Pointcut("execution(* page*(..))&&within(com.whln.ordermax.api.service..*)")
    public void pointCut() {
    }

    public List<String> getChildRoleId(List<String> parentRoleIds, List<SysRole> allRoleList) {
        List<String> roleIdList = new ArrayList<>();
        Map<String, List<SysRole>> collect = allRoleList.stream()
                .filter(item -> ObjectUtil.isNotEmpty(item.getParentId()))
                .collect(Collectors.groupingBy(SysRole::getParentId, Hashtable::new, Collectors.toList()));
        if (ObjectUtil.isEmpty(parentRoleIds)) {
            return new ArrayList<>();
        }
        for (String s : parentRoleIds) {
            List<SysRole> sysRoleList = collect.get(s);
            if (ObjectUtil.isNotEmpty(sysRoleList)) {
                List<String> collect1 = sysRoleList.stream().map(SysRole::getId).collect(Collectors.toList());
                roleIdList.addAll(collect1);
                List<String> childRoleId = getChildRoleId(collect1, allRoleList);
                roleIdList.addAll(childRoleId);
            }
        }
        return roleIdList;
    }


    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        for (Object o : args) {
            if (ObjectUtil.isEmpty(o)) {
                continue;
            }
            Class<?> cls = o.getClass();
            boolean isDelete = ReflectUtil.hasField(cls, "isDelete");
            if (isDelete) {
                Field isDeleteField = cls.getDeclaredField("isDelete");
                isDeleteField.setAccessible(true);
                Object isDeleteValue = isDeleteField.get(o);
                if (ObjectUtil.isEmpty(isDeleteValue)) {
                    isDeleteField.set(o, 0);
                }
            }
            if (ShiroManager.hasRole("webadmin")) {
                return joinPoint.proceed();
            }
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            Method method = signature.getMethod();
            PageWithUserIgnore annotation = method.getAnnotation(PageWithUserIgnore.class);
            if (ObjectUtil.isEmpty(annotation)) {
                boolean hasOwnerIds = ReflectUtil.hasField(o.getClass(), "ownerIds");
                if (hasOwnerIds) {
                    SysUser user = ShiroManager.getUser();
                    List<SysRole> sysRoleList = user.getSysRoleList();
                    List<String> parentRoleIds = sysRoleList.stream().map(SysRole::getId).collect(Collectors.toList());
                    List<SysRole> allRole = redisClient.lRange(RedisKey.SYS_ROLE_ALL.getKey(), 0, -1);
                    if (ObjectUtil.isNotEmpty(sysRoleList)
                            && ObjectUtil.isNotEmpty(parentRoleIds)
                            && ObjectUtil.isNotEmpty(allRole)) {
                        List<String> childRoleId = getChildRoleId(parentRoleIds, allRole);
                        childRoleId.addAll(parentRoleIds);
                        List<String> userIds = sysUserRoleRepository.listObjs(new LambdaQueryWrapper<SysUserRole>().select(SysUserRole::getUserId).in(SysUserRole::getRoleId, childRoleId), Object::toString);
                        userIds.add(ShiroManager.getUserId());
                        ReflectUtil.setFieldValue(o, "ownerIds", userIds);
                    }
                }
            }
        }
        return joinPoint.proceed();
    }


    @AfterReturning(pointcut = "pointCut()", returning = "result")
    public Object after(JoinPoint joinPoint, Result<Page<FilterBill>> result) {
        Object target = joinPoint.getTarget();
        BillCode annotation = target.getClass().getAnnotation(BillCode.class);
        if (ObjectUtil.isNotEmpty(annotation)) {
            ModelMessage value = annotation.value();
            OaRule one = oaRuleRepository.getOne(new LambdaQueryWrapper<OaRule>().eq(OaRule::getBillCode, value.getCode()));
            if (ObjectUtil.isNotEmpty(one)) {
                Page<FilterBill> data = result.getData();
                List<FilterBill> records = data.getRecords();
                List<FilterBill> collect = records.stream()
                        .peek(item -> {
                            item.setOaIsActive(one.getIsActive());
                        }).collect(Collectors.toList());
                data.setRecords(collect);
                result.setData(data);
            }
        }
        return result;
    }

}
