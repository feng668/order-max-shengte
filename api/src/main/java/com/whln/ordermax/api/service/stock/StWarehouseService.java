package com.whln.ordermax.api.service.stock;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.shiro.utils.ShiroManager;
import com.whln.ordermax.common.annotaions.PageWithUserIgnore;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.StWarehouse;
import com.whln.ordermax.data.repository.StWarehouseRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 业务处理
 */
@Service
public class StWarehouseService {
    @Resource
    private StWarehouseRepository baseRepository;

    /**
     * 查询所有
     */
    public Result listAll(StWarehouse entity) {
        return Result.success(baseRepository.listByEntity(entity));
    }

    /**
     * 分页查询所有
     */
    @PageWithUserIgnore
    public Result pageAll(StWarehouse entity, Page<StWarehouse> page) {
        if (ShiroManager.hasRole("customer") || ShiroManager.hasRole("supplier")) {
            throw new BusinessException("无权限查看");
        }
        return Result.success(baseRepository.pageByEntity(entity, page));
    }

    /**
     * 插入或更新
     */
    public Result save(StWarehouse entity) {
        if (ObjectUtil.isEmpty(entity.getWarehouseNo())) {
            throw new BusinessException("仓库编号不能为空");
        }
        StWarehouse stWarehouse = baseRepository.getOne(new LambdaQueryWrapper<StWarehouse>()
                .eq(StWarehouse::getWarehouseNo, entity.getWarehouseNo()));
        if (ObjectUtil.isNotEmpty(stWarehouse) && !ObjectUtil.equals(entity.getId(), stWarehouse.getId())) {
            throw new BusinessException("仓库编号已存在");
        }
        boolean b = baseRepository.saveOrUpdate(entity);
        return b ? Result.success() : Result.error("201", "保存失败");
    }

    /**
     * 单条删除
     */
    public Result delete(String id) {
        boolean b = baseRepository.removeById(id);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

    /**
     * 批量删除
     */
    public Result batchDelete(List<String> ids) {
        boolean b = baseRepository.removeByIds(ids);
        return b ? Result.success() : Result.error("201", "删除失败");
    }

}