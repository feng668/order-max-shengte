package com.whln.ordermax.api.designpattern.strategy.stinorder;

import com.whln.ordermax.api.designpattern.strategy.StrategyHandler;
import com.whln.ordermax.data.repository.StInOrderRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.annotation.Resource;

/**
 * @author liurun
 * @date 2023-04-26 9:47
 */
@NoArgsConstructor
@AllArgsConstructor
public abstract class StInorderStrategy<P, T, R> implements StrategyHandler<P, T, R> {
    @Resource
    private StInOrderRepository stInOrderRepository;
}
