package com.whln.ordermax.api.controller.product;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.api.service.product.PdProductionTaskService;
import com.whln.ordermax.common.annotaions.BillCode;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.entity.PageEntity;
import com.whln.ordermax.common.entity.Result;
import com.whln.ordermax.data.domain.*;
import com.whln.ordermax.data.domain.param.PdProductionBatchSaveParam;
import com.whln.ordermax.data.domain.param.ProductionTaskBomParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 生产任务单控制器
 */
@BillCode(ModelMessage.PD_PRODUCTION_TASK)
@Api(tags = "生产任务单")
@RestController
@RequestMapping("/pdProductionTask")
public class PdProductionTaskController {

    @Resource
    private PdProductionTaskService baseService;

    @ApiOperation(value = "查询")
    @PostMapping("/list")
    public Result<List<PdProductionTask>> listAll(@RequestBody PdProductionTask entity) {
        return baseService.listAll(entity);
    }

    @ApiOperation(value = "分页查询")
    @PostMapping("/page")
    public Result<Page<PdProductionTask>> pageAll(@RequestBody PageEntity<PdProductionTask> pageEntity) {
        return baseService.pageAll(pageEntity.getEntity(), new Page<>(pageEntity.getCurrent(), pageEntity.getSize()));
    }

    @ApiOperation(value = "批量保存")
    @PostMapping("/batchSave")
    public Result<List<PdProductionTask>> batchSave(@RequestBody PdProductionBatchSaveParam param) {
        return baseService.batchSave(param);
    }

    @ApiOperation(value = "保存")
    @PostMapping("/save")
    public Result save(@RequestBody() PdProductionTask entity) {
        return baseService.save(entity);
    }

    @ApiOperation(value = "单条删除")
    @PostMapping("/delete")
    public Result delete(@RequestParam("id") String id) {
        return baseService.delete(id);
    }

    @ApiOperation(value = "批量删除")
    @PostMapping("/batchDelete")
    public Result batchDelete(@RequestBody() List<String> ids) {
        return baseService.batchDelete(ids);
    }

    @ApiOperation(value = "获取生产批号")
    @PostMapping("/listLot")
    public Result<List<PdProductionLot>> listLot(@RequestParam("id") String id) {
        return baseService.listLot(id);
    }

    @ApiOperation(value = "获取生产商品bom")
    @PostMapping("/listBom")
    public Result<List<CmCommodityBom>> listBom(@RequestBody() JSONObject entity) {
        return baseService.listBom(entity);
    }

    @ApiOperation(value = "获取生产商品bom")
    @PostMapping("/listProductionBom")
    public Result<List<PdProductionCommodityBom>> listBom(@RequestBody @Validated ProductionTaskBomParam param) {
        return Result.success(baseService.getProductionTaskBom(param));
    }

}