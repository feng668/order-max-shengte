package com.whln.ordermax.api.designpattern.strategy.templatestrategy.impl;

import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategy;
import com.whln.ordermax.api.designpattern.strategy.templatestrategy.TemplateStrategyFactory;
import com.whln.ordermax.common.enums.ModuleEnum;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.common.utils.OmQrCodeUtil;
import com.whln.ordermax.data.domain.PdProductionCommodityBom;
import com.whln.ordermax.data.domain.PdProductionTask;
import com.whln.ordermax.data.domain.TmPrintTemplate;
import com.whln.ordermax.data.domain.param.RenderTemplateParam;
import com.whln.ordermax.data.repository.PdProductionTaskRepository;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-02-21 9:38
 */
@Component
public class PdProductionTaskTemplateStrategy extends TemplateStrategy<RenderTemplateParam, TmPrintTemplate,String>{
    @Resource
    private PdProductionTaskRepository repository;
    @Override
    public void afterPropertiesSet() throws Exception {
        TemplateStrategyFactory.register(ModuleEnum.PD_PRODUCTION_TASK.getValue(), this);
    }
    @Override
    public String handle(RenderTemplateParam param, TmPrintTemplate templateData) {
        try {
            String content = getTemplateContent(templateData.getHtmlPath());
            String template = transitionHtml2(content);
            PdProductionTask pdProductionTask = repository.getAllById(param.getId());
            List<PdProductionCommodityBom> commodityList = pdProductionTask.getCommodityList();
            List<PdProductionCommodityBom> collect = commodityList.stream().peek(item -> {
                String sb = "data:image/jpeg;base64," +
                        item.getCcCustomsEnName() +
                        item.getCnName();
                String base64 = OmQrCodeUtil.writeToBase64(sb, "jpg", 50, 50);
                item.setQRCode(base64);
            }).collect(Collectors.toList());
            pdProductionTask.setCommodityList(collect);

            return render(pdProductionTask, template);
        } catch (IOException e) {
            throw new BusinessException("读取模板出错了");
        }
    }



}
