[
  {
    path: '/OA',
    component: Layout,
    redirect: '/OA/oa',
    meta: { title: 'OA', icon: 'oa' },
    children: [{
      path: 'oa',
      name: 'oa',
     
      component: () => import('@/views/OA/oa'),
      meta: { title: 'OA', icon: 'oa' },
    },

    {
      path: 'oaEdit',
      name: 'oaEdit',
      hidden:true,
      component: () => import('@/views/OA/oaEdit'),
      meta: { title: 'oaEdit', icon: 'children' },
    },
    ]
  },

  //邮件管理
  {
    path: '/mail',
    component: Layout,
    redirect: '/mail/inboxMail',
    meta: { title: 'mailbox', icon: 'mail' },
    children: [
            //收件箱
                {
                  path: 'inboxMail',
                  name: 'inboxMail',
                
                  component: () => import('@/views/mail/inboxMail'),
                  meta: { title: 'mail', icon: 'children' , showLeft:'1', roles:['']},
                  },
                  //地址簿
                  {
                    path: 'addressBook',
                    name: 'addressBook',
           
                    component: () => import('@/views/mail/addressBook'),
                    meta: { title: 'addressBook', icon: 'children' , showLeft:'1', roles:['']},
                    },
                    {
                      path: 'Massmail',
                      name: 'Massmail',
                    
                      component: () => import('@/views/mail/Massmail'),
                      meta: { title: 'Massmail', icon: 'children' ,  showLeft:'1',  roles:['']},
                      },
                      {
                        path: 'MassmailEdit',
                        name: 'MassmailEdit',
                        hidden:true,
                        component: () => import('@/views/mail/MassmailEdit'),
                        meta: { title: 'MassmailEdit', icon: 'children' ,  showLeft:'1',  roles:['']},
                        },
                      
                    
                      
                  {
                    path: 'inboxMailEdit',
                    name: 'inboxMailEdit',
                    hidden:true,
                    component: () => import('@/views/mail/inboxMailEdit'),
                    meta: { title: 'mailbox', icon: 'mail' ,roles:['']},
                    },
                    {
                      path: 'drafts',
                      name: 'drafts',
                      hidden:true,
                      component: () => import('@/views/mail/drafts'),
                      meta: { title: 'mailbox', icon: 'mail' ,roles:['']},
                      },
                    
                    {
                      path: 'xingMail',
                      name: 'xingMail',
                      hidden:true,
                      component: () => import('@/views/mail/xingMail'),
                      meta: { title: 'xingMail', icon: 'mail' ,roles:['']},
                      },
                    
                  {
                    path: 'sendMail',
                    name: 'sendMail',
                    hidden:true,
                    component: () => import('@/views/mail/sendMail'),
                    meta: { title: 'mailbox', icon: 'mail' ,roles:['']},
                    },

                    {
                      path: 'Submitted',
                      name: 'Submitted',
                      hidden:true,
                      component: () => import('@/views/mail/Submitted'),
                      meta: { title: 'Submitted', icon: 'mail' ,roles:['']},
                      },

                      {
                        path: 'recycle',
                        name: 'recycle',
                        hidden:true,
                        component: () => import('@/views/mail/recycle'),
                        meta: { title: 'recycle', icon: 'mail' ,roles:['']},
                        },
                      

                    
        //
    //
    ]
  },

  
  //客户
  {
    path: '/customer',
    component: Layout,
    redirect: '/customer/manageCus',
    meta: { title: 'ClientManagement', icon: 'customer' },
    children: [
     {
        path: 'manageCus',
        name: 'manageCus',
        component: () => import('@/views/customer/manageCus'),
        meta: { title: 'manageCus', icon: 'children' }
      } ,
      {
        path: 'manageCusEdit',
        name: 'manageCusEdit',
        hidden:true,
        component: () => import('@/views/customer/manageCusEdit'),
        meta: { title: 'manageCusEdit', KeepAlive: true, icon: 'children' }
      },
      {
        path: 'Customerhighseas',
        name: 'Customerhighseas',
   
        component: () => import('@/views/customer/Customerhighseas'),
        meta: { title: 'Customerhighseas', icon: 'children' }
        
      },

      {
        path: 'CustomerhighseasEdit',
        name: 'CustomerhighseasEdit',
        hidden:true,
        component: () => import('@/views/customer/CustomerhighseasEdit'),
        meta: { title: 'CustomerhighseasEdit',KeepAlive: true, icon: 'children' }
        
      },
      
    ]
  },
  
  {
    path: '/goods',
    component: Layout,
    redirect: '/goods/accessory',
    meta: { title: 'commodityManage', icon: 'good' },
    children: [
      {
      path: 'commodity',
      name: 'commodity',
      component: () => import('@/views/goods/commodity'),
      meta: { title: 'CommodityFiles', icon: 'children' },
      },
      {
        path: 'commodityEdit',
        name: 'commodityEdit',
        hidden:true,
        component: () => import('@/views/goods/commodityEdit'),
        meta: { title: 'commodityEdit', KeepAlive: true,icon: 'children' },
        },
      
      {
      path: 'accessory',
      name: 'accessory',
     
      component: () => import('@/views/goods/accessory'),
      meta: { title: 'AccessoriesFiles', icon: 'children' }
    },
    
    {
      path: 'accessoryEdit',
      name: 'accessoryEdit',
      hidden:true,
      component: () => import('@/views/goods/accessoryEdit'),
      meta: { title: 'accessoryEdit',KeepAlive: true, icon: 'children' }
    },
   
    {
      path: 'commodityClass',
      name: 'commodityClass',
      component: () => import('@/views/goods/commodityClass'),
      meta: { title: 'categories', icon: 'children' }
    },
    {
      path: 'accessoryClass',
      name: 'accessoryClass',
     
      component: () => import('@/views/goods/accessoryClass'),
      meta: { title: 'AccessoriesClass', icon: 'children' }
    },
    {
      path: 'priceControl',
      name: 'priceControl',
      component: () => import('@/views/goods/priceControl'),
      meta: { title: 'priceControl', icon: 'children' }
    },
    {
      path: 'priceControlEdit',
      name: 'priceControlEdit',
      hidden:true,
      component: () => import('@/views/goods/priceControlEdit'),
      meta: { title: 'priceControlEdit', KeepAlive:true, icon: 'children' }
    },
  ]
  },


  //询盘管理
{
  path: '/Enquiries',
  component: Layout,
  redirect: '/Enquiries/inquirysheet',
  meta: { title: 'Enquiries', icon: 'el-icon-ship' },
  children: [
    // {
    // path: 'index',
    // name: 'index',
    // component: () => import('@/views/Enquiries/index'),
    // meta: { title: 'Enquiries', icon: 'tongji' ,roles:[]},
    // },
    {
    path: 'inquirysheet',
    name: 'inquirysheet',
    component: () => import('@/views/Enquiries/inquirysheet'),
    meta: { title: 'inquirysheet', icon: 'children' ,roles:[]},
    },
    {
      path: 'inquirysheetEdit',
      name: 'inquirysheetEdit',
      hidden:true,
      component: () => import('@/views/Enquiries/inquirysheetEdit'),
      meta: { title: 'inquirysheetEdit', KeepAlive: true,icon: 'children' ,roles:[]},
      },
      {
        path: 'InquiryProgress',
        name: 'InquiryProgress',
        component: () => import('@/views/Enquiries/InquiryProgress'),
        meta: { title: 'InquiryProgress', icon: 'children' ,roles:[]},
        },
      
    
  ]
},





  //采购管理
  // {
  //   path: '/purManagement',
  //   component: Layout,
  //   redirect: '/purManagement',
  //   meta: { title: 'POmanage', icon: 'caigou' },
  //   children: [
  //     {
  //       path: 'SupplierFiles',
  //       name: 'SupplierFiles',
  //       component: () => import('@/views/purManagement/SupplierFiles'),
  //       meta: { title: 'SupplierFiles', icon: 'caigou' },
  //       },
  //       {
  //         path: 'SupplierFileEdit',
  //         name: 'SupplierFileEdit',
  //         hidden:true,
  //         component: () => import('@/views/purManagement/SupplierFileEdit'),
  //         meta: { title: 'SupplierFiles', icon: 'caigou' },
  //         },
  //     {
  //     path: 'purRequisition',
  //     name: 'purRequisition',
  //     component: () => import('@/views/purManagement/purRequisition'),
  //     meta: { title: 'PurchaseRequisition', icon: 'caigou' }
  //     },
  //     {
  //       path: 'purOrder',
  //       name: 'purOrder',
  //       component: () => import('@/views/purManagement/purOrder'),
  //       meta: { title: 'purchaseOrder', icon: 'caigou' }
  //       },
  //   ]
  // },

  {
    path: '/quota',
    component: Layout,
    redirect: '/quota/quotation',
    meta: { title: 'quote', icon: 'baojia' },
    children: [
      {
      path: 'quotation',
      name: 'quotation',
      component: () => import('@/views/quota/quotation'),
      meta: { title: 'quotation', icon: 'children' },
      },
      {
        path: 'quotEdit',
        name: 'quotEdit',
        hidden:true,
        component: () => import('@/views/quota/quotEdit'),
        meta: { title: 'quotEdit',KeepAlive: true, icon: 'children' }
        },
      {
      path: 'salesContract',
      name: 'salesContract',
      component: () => import('@/views/quota/salesContract'),
      meta: { title: 'salesContract', icon: 'children' }
      },
      {
        path: 'salesContractEdit',
        name: 'salesContractEdit',
        hidden:true,
        component: () => import('@/views/quota/salesContractEdit'),
        meta: { title: 'salesContractEdit',KeepAlive: true, icon: 'children' }
        },
      //
      {
        path: 'ContractProgress',
        name: 'ContractProgress',
        component: () => import('@/views/quota/ContractProgress'),
        meta: { title: 'ContractProgress', icon: 'children' }
        },

        {
          path: 'LogisticsProcedures',
          name: 'LogisticsProcedures',
          component: () => import('@/views/quota/LogisticsProcedures'),
          meta: { title: 'trackingInformation', icon: 'children' }
          },
        //LogisticsProcedures
       ]
  },
//内销管理
  {
    path: '/Domesticsales',
    component: Layout,
    redirect: '/Domesticsales/quotation',
    meta: { title: 'Domesticsales', icon: 'baojia' },
    children: [
      {
      path: 'quotation',
      name: 'DomesticQuotation',
      component: () => import('@/views/Domesticsales/quotation'),
      meta: { title: 'DomesticQuotation', icon: 'children' },
      },
      {
        path: 'qutaEdit',
        name: 'DomesticQuotationEdit',
        hidden:true,
        component: () => import('@/views/Domesticsales/qutaEdit'),
        meta: { title: 'DomesticQuotationEdit',KeepAlive: true, icon: 'children' },
        },

        {
          path: 'contract',
          name: 'DomesticSalesContract',
          
          component: () => import('@/views/Domesticsales/contract'),
          meta: { title: 'DomesticSalesContract', icon: 'children' },
          },

          {
            path: 'contractEdit',
            name: 'DomesticSalesContractEdit',
            hidden:true,
            component: () => import('@/views/Domesticsales/contractEdit'),
            meta: { title: 'DomesticSalesContractEdit',KeepAlive: true, icon: 'children' },
            },
            {
              path: 'DomesticInvoice',
              name: 'DomesticInvoice',
             
              component: () => import('@/views/Domesticsales/DomesticInvoice'),
              meta: { title: 'DomesticSalesDoc', icon: 'children' },
              },
              {
                path: 'DomesticInvoiceEdit',
                name: 'DomesticInvoiceEdit',
                hidden:true,
                component: () => import('@/views/Domesticsales/DomesticInvoiceEdit'),
                meta: { title: 'DomesticSalesDocEdit',KeepAlive: true, icon: 'children' },
                },
      
     ]
  },
  //OCplan
  
  //出运计划
  {
    path: '/ocPlan',
    component: Layout,
    redirect: '/ocPlan/ocPlan',
    meta: { title: 'ocPlan', icon: 'oc' },
    children: [
      {
        path: 'ocPlan',
        name: 'ocPlan',
        component: () => import('@/views/ocPlan/ocPlan'),
        meta: { title: 'ocPlan', icon: 'children' },
        },
        {
          path: 'ocPlanEdit',
          name: 'ocPlanEdit',
          hidden:true,
          component: () => import('@/views/ocPlan/ocPlanEdit'),
          meta: { title: 'ocPlanEdit', KeepAlive: true , icon: 'children' },
          },
          //单证
      {
        path: 'documentManage',
        name: 'documentManage',
        component: () => import('@/views/ocPlan/documentManage'),
        meta: { title: 'documentManage', icon: 'children' },
        },
        {
          path: 'documentManageEdit',
          name: 'documentManageEdit',
           hidden:true,
          component: () => import('@/views/ocPlan/documentManageEdit'),
          meta: { title: 'documentManageEdit',KeepAlive: true, icon: 'oc' },
          },
        
          //  发运通知
          {
            path: 'dispatchAdvice',
            name: 'dispatchAdvice',
            component: () => import('@/views/ocPlan/dispatchAdvice'),
            meta: { title: 'dispatchAdvice', icon: 'children' },
            },
            
          {
            path: 'dispatchAdviceEdit',
            name: 'dispatchAdviceEdit',
            hidden:true,
            component: () => import('@/views/ocPlan/dispatchAdviceEdit'),
            meta: { title: 'dispatchAdviceEdit',KeepAlive: true, icon: 'children' },
            },

            {
              path: 'forwardCompany',
              name: 'forwardCompany',
             
              component: () => import('@/views/ocPlan/forwardCompany'),
              meta: { title: 'forwardCompany', icon: 'children' },
              },
              {
                path: 'forwardCompanyEdit',
                name: 'forwardCompanyEdit',
                hidden:true,
                component: () => import('@/views/ocPlan/forwardCompanyEdit'),
                meta: { title: 'forwardCompanyEdit', KeepAlive: true, icon: 'children' },
                },
           
            //
    ]
  },
  //采购管理
  {
    path: '/purchasingManage',
    component: Layout,
    redirect: '/purchasingManage/SupplierFiles',
    meta: { title: 'POmanage', icon: 'caigou' },
    children: [
      {
        path: 'SupplierFiles',
        name: 'SupplierFiles',
        component: () => import('@/views/purchasingManage/SupplierFiles'),
        meta: { title: 'SupplierFiles', icon: 'children' },
        },
        {
          path: 'SupplierFileEdit',
          name: 'SupplierFileEdit',
          hidden:true,
          component: () => import('@/views/purchasingManage/SupplierFileEdit'),
          meta: { title: 'SupplierFileEdit',KeepAlive: true, icon: 'children' },
          },
      {
        path: 'ProcurementContract',
        name: 'ProcurementContract',
        component: () => import('@/views/purchasingManage/ProcurementContract'),
        meta: { title: 'ProcurementContract', icon: 'children' },
        },
        {
          path: 'ProcurementContractEdit',
          name: 'ProcurementContractEdit',
          hidden:true,
          component: () => import('@/views/purchasingManage/ProcurementContractEdit'),
          meta: { title: 'ProcurementContractEdit',KeepAlive: true, icon: 'children' },
          },

          {
            path: 'PurchaseSheet',
            name: 'PurchaseSheet',
            component: () => import('@/views/purchasingManage/PurchaseSheet'),
            meta: { title: 'PurchaseSheet',KeepAlive: true, icon: 'children' },
            },

            
          {
            path: 'PurchaseSheetEdit',
            name: 'PurchaseSheetEdit',
            hidden:true,
            component: () => import('@/views/purchasingManage/PurchaseSheetEdit'),
            meta: { title: 'PurchaseSheetEdit',KeepAlive: true, icon: 'children' },
            },

         
          
        {
          path: 'PurchaseInvoice',
          name: 'PurchaseInvoice',
          component: () => import('@/views/purchasingManage/PurchaseInvoice'),
          meta: { title: 'PurchaseInvoice', icon: 'children' },
          },
          
          {
            path: 'PurchaseInvoiceEdit',
            name: 'PurchaseInvoiceEdit',
            hidden:true,
            component: () => import('@/views/purchasingManage/PurchaseInvoiceEdit'),
            meta: { title: 'PurchaseInvoiceEdit',KeepAlive: true, icon: 'children' },
            },
          // {
          //   path: 'PurchaseRequisition',
          //   name: 'PurchaseRequisition',
          //   component: () => import('@/views/purchasingManage/PurchaseRequisition'),
          //   meta: { title: 'PurchaseRequisition', icon: 'children' },
          //   },
          //   //采购申请
          //   {
          //     path: 'PurchaseRequisitionEdit',
          //     name: 'PurchaseRequisitionEdit',
          //     hidden:true,
          //     component: () => import('@/views/purchasingManage/PurchaseRequisitionEdit'),
          //     meta: { title: 'PurchaseRequisitionEdit',KeepAlive: true, icon: 'children' },
          //     },
    ]
  },
  //进口管理
  
  // {
  //   path: '/ImportManage',
  //   component: Layout,
  //   redirect: '/ImportManage',
  //   meta: { title: 'ImportManage', icon: 'jinkou' },
  //   children: [
  //     {
  //       path: 'ImportManage',
  //       name: 'ImportManage',
  //       component: () => import('@/views/ImportManage/ImportManage'),
  //       meta: { title: 'Importcontract', icon: 'jinkou' },
  //       },
  //       {
  //         path: 'ImportManageEdit',
  //         name: 'ImportManageEdit',
  //         hidden:true,
  //         component: () => import('@/views/ImportManage/ImportManageEdit'),
  //         meta: { title: 'ImportManageEdit', icon: 'jinkou' },
  //         },
  //         {
  //           path: 'clearancedocument',
  //           name: 'clearancedocument',
  //           component: () => import('@/views/ImportManage/clearancedocument'),
  //           meta: { title: 'clearancedocument', icon: 'jinkou' },
  //           },
  //           {
  //             path: 'clearancedocumentEdit',
  //             name: 'clearancedocumentEdit',
  //             hidden:true,
  //             component: () => import('@/views/ImportManage/clearancedocumentEdit'),
  //             meta: { title: 'clearancedocumentEdit', icon: 'jinkou' },
  //             },
          
      
  //   ]
  // },
  //生产管理
  {
    path: '/productionManage',
    component: Layout,
    redirect: '/productionManage/MO',
    meta: { title: 'productionManage', icon: 'shengchan' },
    children: [
      {
        path: 'MO',
        name: 'MO',
        component: () => import('@/views/productionManage/MO'),
        meta: { title: 'MO', icon: 'children' },
        },
        {
          path: 'MOEdit',
          name: 'MOEdit',
          hidden:true,
          component: () => import('@/views/productionManage/MOEdit'),
          meta: { title: 'MOEdit', KeepAlive: true,icon: 'children' },
          },
          {
            path: 'Quality',
            name: 'Quality',
           
            component: () => import('@/views/productionManage/Quality'),
            meta: { title: 'Quality', icon: 'children' },
            },
            {
              path: 'QualityEdit',
              name: 'QualityEdit',
              hidden:true,
              component: () => import('@/views/productionManage/QualityEdit'),
              meta: { title: 'QualityEdit', KeepAlive: true,icon: 'children' },
              },
          //
      ]
  },
  //productionManage
  {
    path: '/organization',
    component: Layout,
    redirect: '/organization/user',
    meta: { title: 'organization', icon: 'zuzhi'},
    children: [
      {
      path: 'user',
      name: 'user',
      component: () => import('@/views/organization/user'),
      meta: { title: 'UserManagement', icon: 'children'},
      },
      {
        path: 'jurisdiction',
        name: 'jurisdiction',
        component: () => import('@/views/organization/jurisdiction'),
        meta: { title: 'RoleManagement', icon: 'children'},
        },
          {
            path: 'company',
            name: 'company',
            component: () => import('@/views/organization/company'),
            meta: { title: 'department', icon: 'children'},
            },

            {
              path: 'companyEdit',
              name: 'companyEdit',
              hidden:true,
              component: () => import('@/views/organization/companyEdit'),
              meta: { title: '部门编辑',KeepAlive: true, icon: 'children'},
              },


          {
            path: 'userEdit',
            name: 'userEdit',
            hidden:true,
            component: () => import('@/views/organization/userEdit'),
            meta: { title: '用户编辑', icon: 'children'},
            },
          {
            path: 'jurisdictionEdit',
            name: 'jurisdictionEdit',
            hidden:true,
            component: () => import('@/views/organization/jurisdictionEdit'),
            meta: { title: '角色编辑', icon: 'children'},
            //
          }]
  },
  //仓库

  {
    path: '/warehouse',
    component: Layout,
    redirect: '/warehouse',
    meta: { title: 'warehouse', icon: 'cangku'},
    children: [
      
      {
        path: 'inventorydetail',
        name: 'inventorydetail',
        component: () => import('@/views/warehouse/inventorydetail'),
        meta: { title: 'inventorydetail', icon: 'children'},
        },

        {
          path: 'InventoryGL',
          name: 'InventoryGL',
         
          component: () => import('@/views/warehouse/InventoryGL'),
          meta: { title: 'InventoryGL', icon: 'children'},
          },


      {
        path: 'RealtimeInventory',
        name: 'RealtimeInventory',
        component: () => import('@/views/warehouse/RealtimeInventory'),
        meta: { title: 'RealtimeInventory', icon: 'children'},
        },
        //库存占用单
        {
          path: 'InventoryOccupancy',
          name: 'InventoryOccupancy',
          component: () => import('@/views/warehouse/InventoryOccupancy'),
          meta: { title: 'InventoryOccupancy', icon: 'children'},
          },
        
      {
      path: 'OutboundOrder',
      name: 'OutboundOrder',
      component: () => import('@/views/warehouse/OutboundOrder'),
      meta: { title: 'OutboundOrder', icon: 'children'},
      },
      {
        path: 'OutboundOrderEdit',
        name: 'OutboundOrderEdit',
        hidden:true,
        component: () => import('@/views/warehouse/OutboundOrderEdit'),
        meta: { title: 'OutboundOrderEdit',KeepAlive: true, icon: 'children'},
        },
          {
            path: 'ReceiptDoc',
            name: 'ReceiptDoc',
            component: () => import('@/views/warehouse/ReceiptDoc'),
            meta: { title: 'ReceiptDoc', icon: 'children'},
            },

            {
              path: 'ReceiptDocEdit',
              name: 'ReceiptDocEdit',
              hidden:true,
              component: () => import('@/views/warehouse/ReceiptDocEdit'),
              meta: { title: 'ReceiptDocEdit', KeepAlive: true,icon: 'children'},
              },

              {
                path: 'PurchaseReceiptDoc',
                name: 'PurchaseReceiptDoc',
                component: () => import('@/views/warehouse/PurchaseReceiptDoc'),
                meta: { title: 'PurchaseReceiptDoc', icon: 'children'},
                },
    
                {
                  path: 'PurchaseReceiptDocEdit',
                  name: 'PurchaseReceiptDocEdit',
                  hidden:true,
                  component: () => import('@/views/warehouse/PurchaseReceiptDocEdit'),
                  meta: { title: 'PurchaseReceiptDocEdit', KeepAlive: true,icon: 'children'},
                  },
              

              {
                path: 'MaterialRequisition',
                name: 'MaterialRequisition',
               
                component: () => import('@/views/warehouse/MaterialRequisition'),
                meta: { title: 'MaterialRequisition', icon: 'children'},
                },
                {
                  path: 'MaterialRequisitionEdit',
                  name: 'MaterialRequisitionEdit',
                  hidden:true,
                  component: () => import('@/views/warehouse/MaterialRequisitionEdit'),
                  meta: { title: 'MaterialRequisitionEdit',KeepAlive: true, icon: 'children'},
                  },
              {
                path: 'otherWarehousing',
                name: 'otherWarehousing',
                
                component: () => import('@/views/warehouse/otherWarehousing'),
                meta: { title: 'otherWarehousing', icon: 'children'},
                },
                {
                  path: 'otherWarehousingEdit',
                  name: 'otherWarehousingEdit',
                  hidden:true,
                  component: () => import('@/views/warehouse/otherWarehousingEdit'),
                  meta: { title: 'otherWarehousingEdit',KeepAlive: true, icon: 'children'},
                  },
                {
                  path: 'otherIssue',
                  name: 'otherIssue',
                
                  component: () => import('@/views/warehouse/otherIssue'),
                  meta: { title: 'otherIssue', icon: 'children'},
                  },
                  {
                    path: 'otherIssueEdit',
                    name: 'otherIssueEdit',
                    hidden:true,
                    component: () => import('@/views/warehouse/otherIssueEdit'),
                    meta: { title: 'otherIssueEdit',KeepAlive: true, icon: 'children'},
                    },
              //InventorySettings
              //otherIssue
              {
                path: 'InventorySettings',
                name: 'InventorySettings',
               
                component: () => import('@/views/warehouse/InventorySettings'),
                meta: { title: 'InventorySettings', icon: 'children'},
                },

                {
                  path: 'InventorySettingsEdit',
                  name: 'InventorySettingsEdit',
                  hidden:true,
                  component: () => import('@/views/warehouse/InventorySettingsEdit'),
                  meta: { title: 'InventorySettingsEdit',KeepAlive: true, icon: 'children'},
                  },
                
                  
              //

]
  },

//财务管理

{
  path: '/financial',
  component: Layout,
  redirect: '/financial/Entryregistration',
  meta: { title: 'financial', icon: 'caiwu', roles:['admin','financial'] },
  children: [
    {
    path: 'Entryregistration',
    name: 'Entryregistration',
    component: () => import('@/views/financial/Entryregistration'),
    meta: { title: 'Entryregistration', icon: 'children' , roles:['admin','financial']},
    },
    {
    path: 'EntryregistrationEdit',
    name: 'EntryregistrationEdit',
    hidden:true,
    component: () => import('@/views/financial/EntryregistrationEdit'),
    meta: { title: 'EntryregistrationEdit', KeepAlive: true,icon: 'children', roles:['admin','financial']}
    },
    {
      path: 'PaymentDoc',
      name: 'PaymentDoc',
     
      component: () => import('@/views/financial/PaymentDoc'),
      meta: { title: 'PaymentDoc', icon: 'children', roles:['admin','financial']}
      },
      {
        path: 'PaymentDocEdit',
        name: 'PaymentDocEdit',
        hidden:true,
        component: () => import('@/views/financial/PaymentDocEdit'),
        meta: { title: 'PaymentDocEdit',KeepAlive: true, icon: 'caiwu', roles:['admin','financial']}
        },
        {
          path: 'CostCenter',
          name: 'CostCenter',
          component: () => import('@/views/financial/CostCenter'),
          meta: { title: 'CostCenter', icon: 'children', roles:['admin','financial']}
          },

          {
            path: 'CostCenterEdit',
            name: 'CostCenterEdit',
            hidden:true,
            component: () => import('@/views/financial/CostCenterEdit'),
            meta: { title: 'CostCenterEdit', KeepAlive: true,icon: 'children', roles:['admin','financial']}
            },
            {
              path: 'ProfitCenter',
              name: 'ProfitCenter',
              component: () => import('@/views/financial/ProfitCenter'),
              meta: { title: 'ProfitCenter', icon: 'children', roles:['admin','financial']}
              },
    
              {
                path: 'ProfitCenterEdit',
                name: 'ProfitCenterEdit',
                hidden:true,
                component: () => import('@/views/financial/ProfitCenterEdit'),
                meta: { title: 'ProfitCenterEdit', KeepAlive: true,icon: 'children', roles:['admin','financial']}
                },
            
          
        
    //
  ]
},

  {
    path: '/statistical',
    component: Layout,
    redirect: '/statistical',
    meta: { title: 'statistical', icon: 'el-icon-ship' },
    children: [
      {
      path: 'index',
      name: 'index',
      component: () => import('@/views/statistical/index'),
      meta: { title: 'statistical', icon: 'tongji' ,roles:[]},
      },]
  },

  {
    path: '/temp/downPdf',
    name:'downPdf',
    component: () => import('@/views/temp/downPdf'),
    hidden: true
  },

  
  {
    path: '/setting',
    component: Layout,
    redirect: '/setting/oaSet/oaEdit',
    meta: { title: 'Systemsettings', icon: 'shezhi' },
    children: [
     
  
      {
        path: 'oaEdit',
        name: 'oaEdit',
        component: () => import('@/views/setting/oaSet/oaEdit'),
        meta: { title: 'oaEdit', icon: 'children' },
      },
      {
        path: 'Highseasset',
        name: 'Highseasset',
        component: () => import('@/views/setting/Highseasset'),
        meta: { title: 'Highseasset', icon: 'children' ,roles:['admin']},
        },
        {
          path: 'HighseassetEdit',
          name: 'HighseassetEdit',
          hidden:true,
          component: () => import('@/views/setting/Highseasset/HighseassetEdit'),
          meta: { title: 'CustomerhighseasEdit', icon: 'children' ,roles:['admin']},
          },
        
      {
        path: 'Numberingrules',
        name: 'Numberingrules',
        component: () => import('@/views/setting/Numberingrules'),
        meta: { title: 'Numberingrules', icon: 'children' ,roles:['admin']},
        },
        
      {
        path: 'NumberingrulesEdit',
        name: 'NumberingrulesEdit',
         hidden:true,
        component: () => import('@/views/setting/NumberingrulesEdit'),
        meta: { title: 'NumberingrulesEdit', icon: 'children' ,roles:['admin']},
        },
        {
          path: 'eventCalendar',
          name: 'eventCalendar',
           hidden:true,
          component: () => import('@/views/setting/eventCalendar'),
          meta: { title: 'eventcalendar', icon: 'children' ,roles:['admin']},
          },
          {
            path: 'Expressinquiry',
            name: 'Expressinquiry',
            hidden:true,
            component: () => import('@/views/setting/Expressinquiry'),
            meta: { title: 'Expressinquiry', icon: 'children' ,roles:['admin']},
            },

            {
              path: 'PrintTemplate',
              name: 'PrintTemplate',
              component: () => import('@/views/setting/PrintTemplate/printTemplate'),
              meta: { title: 'PrintTemplate', icon: 'children'},
              },
              {
                path: 'printTemplateEdit',
                name: 'printTemplateEdit',
                hidden:true,
                component: () => import('@/views/setting/PrintTemplate/printTemplateEdit'),
                meta: { title: 'printTemplateEdit', icon: 'children'},
                },
              
              {
                path: 'MailManagement',
                name: 'MailManagement',
                component: () => import('@/views/setting/MailManagement/MailManagement'),
                meta: { title: 'MailManagement', icon: 'children'},
                },

                {
                  path: 'MailManagementEdit',
                  name: 'MailManagementEdit',
                  hidden:true,
                  component: () => import('@/views/setting/MailManagement/MailManagementEdit'),
                  meta: { title: 'MailManagementEdit', KeepAlive: true, icon: 'children'},
                  },
                  //邮箱服务器
                  {
                    path: 'Mailboxserver',
                    name: 'Mailboxserver',
                    component: () => import('@/views/setting/Mailboxserver/Mailboxserver'),
                    meta: { title: 'Mailboxserver', icon: 'children'},
                    },
    
                    {
                      path: 'MailboxserverEdit',
                      name: 'MailboxserverEdit',
                      hidden:true,
                      component: () => import('@/views/setting/Mailboxserver/MailboxserverEdit'),
                      meta: { title: 'MailboxserverEdit', KeepAlive: true, icon: 'children'},
                      },
                      {
                        path: 'SignatureSet',
                        name: 'SignatureSet',
                        component: () => import('@/views/setting/SignatureSet'),
                        meta: { title: 'SignatureSet', KeepAlive: true, icon: 'children'},
                        },
                        {
                          path: 'SignatureSetEdit',
                          name: 'SignatureSetEdit',
                          hidden:true,
                          component: () => import('@/views/setting/SignatureSet/SignatureSetEdit'),
                          meta: { title: 'SignatureSetEdit', KeepAlive: true, icon: 'children'},
                          },                      
                      {
                        path: 'Permissionconfig',
                        name: 'Permissionconfig',
                       
                        component: () => import('@/views/setting/Permissionconfig/index'),
                        meta: { title: 'Permissionconfig', icon: 'children'},
                        },

                        {
                          path: 'CurrencySet',
                          name: 'CurrencySet',
                         
                          component: () => import('@/views/setting/CurrencySet/index'),
                          meta: { title: 'CurrencySet', icon: 'children'},
                          },
                      
                
              

        
        //
    //
    ]
  },
   { path: '*', redirect: '/404', hidden: true }

];

export default router
