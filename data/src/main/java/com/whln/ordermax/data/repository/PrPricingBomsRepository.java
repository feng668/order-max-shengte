package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PrPricingBoms;
import com.whln.ordermax.data.domain.PrPricingCommodity;
import com.whln.ordermax.data.mapper.PrPricingBomsMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class PrPricingBomsRepository extends ServiceImpl<PrPricingBomsMapper, PrPricingBoms>{

    public IPage<PrPricingBoms> pageByEntity(PrPricingBoms entity, Page<PrPricingBoms> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PrPricingBoms> listByEntity(PrPricingBoms entity) {
        return baseMapper.listByEntity(entity);
    }

    public PrPricingBoms getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<PrPricingBoms> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<PrPricingBoms> listBomsByCommodity(List<PrPricingCommodity> entityList) {
        return baseMapper.listBomsByCommodity(entityList);
    }
}




