package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CuCustomerFollowRecord;
import com.whln.ordermax.data.mapper.CuCustomerFollowRecordMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author liurun
 */
@Repository
public class CuCustomerFollowRecordRepository extends ServiceImpl<CuCustomerFollowRecordMapper, CuCustomerFollowRecord>{

}




