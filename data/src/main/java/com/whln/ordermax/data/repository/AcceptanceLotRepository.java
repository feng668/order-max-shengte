package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PdAcceptanceLot;
import com.whln.ordermax.data.mapper.AcceptanceLotMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class AcceptanceLotRepository extends ServiceImpl<AcceptanceLotMapper, PdAcceptanceLot>{

    public IPage<PdAcceptanceLot> pageByEntity(PdAcceptanceLot entity, Page<PdAcceptanceLot> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PdAcceptanceLot> listByEntity(PdAcceptanceLot entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PdAcceptanceLot> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




