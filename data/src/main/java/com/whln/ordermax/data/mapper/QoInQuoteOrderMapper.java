package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.QoInQuoteOrder;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.QoInQuoteOrder
 */
public interface QoInQuoteOrderMapper extends BaseMapper<QoInQuoteOrder> {

    List<QoInQuoteOrder> listByEntity(@Param("entity")QoInQuoteOrder entity);

    IPage<QoInQuoteOrder> listByEntity(@Param("entity")QoInQuoteOrder entity, Page<QoInQuoteOrder> page);

    Integer insertBatch(@Param("entitylist")List<QoInQuoteOrder> entitylist);

    QoInQuoteOrder getByIdWithInfo(@Param("id")String queteId);

}




