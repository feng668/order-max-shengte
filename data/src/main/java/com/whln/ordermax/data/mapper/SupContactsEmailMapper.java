package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.SupContactsEmail;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SupContactsEmail
 */
public interface SupContactsEmailMapper extends BaseMapper<SupContactsEmail> {

    List<SupContactsEmail> listByEntity(@Param("entity")SupContactsEmail entity);

    IPage<SupContactsEmail> listByEntity(@Param("entity")SupContactsEmail entity, Page<SupContactsEmail> page);

    Integer insertBatch(@Param("entitylist")List<SupContactsEmail> entitylist);

    List<SupContactsEmail> listWithSupId();
}




