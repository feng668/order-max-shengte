package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CuCustomerTransferRule;
import com.whln.ordermax.data.mapper.CuCustomerTransferRuleMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class CuCustomerTransferRuleRepository extends ServiceImpl<CuCustomerTransferRuleMapper, CuCustomerTransferRule>{

    public IPage<CuCustomerTransferRule> pageByEntity(CuCustomerTransferRule entity, Page<CuCustomerTransferRule> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<CuCustomerTransferRule> listByEntity(CuCustomerTransferRule entity) {
        return baseMapper.listByEntity(entity);
    }

    public CuCustomerTransferRule getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<CuCustomerTransferRule> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




