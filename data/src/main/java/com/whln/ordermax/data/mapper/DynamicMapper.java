package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.param.TransactionHistoryParam;
import com.whln.ordermax.data.domain.vo.CommodityHistoryVo;
import com.whln.ordermax.data.domain.vo.CommodityTransactionHistoryVO;
import com.whln.ordermax.data.domain.vo.TransactionNumAnAmountVO;
import com.whln.ordermax.data.domain.vo.ListLogisticsStatVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Entity whln.dyx.data.domain.BsCustomsCode
 */
public interface DynamicMapper extends BaseMapper<Object> {

    List<Map> listAll(@Param("tableName")String tableName);

    String getMaxNo(
            @Param("billNoField")String billNoField,
            @Param("tableName")String tableName,
            @Param("matchInfo")String matchInfo
    );

    Boolean updateBillOaStatus(@Param("tableName")String tableName,@Param("id")String id,@Param("oaLevelOpera") String oaLevelOpera,@Param("oaStatus") Integer oaStatus);

    List<Map<String,Object>> listEventByDateRange(@Param("startDate")String startDate,@Param("endDate") String endDate);

    Page<CommodityTransactionHistoryVO> listDealByCommodityId(@Param("param") TransactionHistoryParam param, IPage<TransactionHistoryParam> page);
    List<CommodityHistoryVo> listDealByCommodityId1(@Param("ids")List<String> ids);

    List<Map<String, Object>> listFollowByCustomerId(@Param("id")String id);

    Boolean updateBillFlowStatus(@Param("billId")String billId, @Param("tableName") String tableName,@Param("flowNo")Integer flowNo);

    Boolean batchUpdateBillComFlowStatus(@Param("idList") List<String> idList, @Param("tableName") String tableName,@Param("flowNo")Integer flowNo);

    Boolean updateBillFlowStatusByComFlow(@Param("sourceBillId") String sourceBillId, @Param("tableName") String tableName, @Param("comTableName") String comTableName,@Param("flowNo")Integer flowNo);

    String getBorderIdByBillId(@Param("id")String id, @Param("tableName")String tableName, @Param("borderType")String borderType);

    List<ListLogisticsStatVO> listLogisticsStat(@Param("entity") Map<String, String> entity);

    /**
     * 查询商品销售总额和销售数量
     * @param commodityId
     * @return
     */
    List<TransactionNumAnAmountVO> transactionNumAnAmount(String commodityId);
}




