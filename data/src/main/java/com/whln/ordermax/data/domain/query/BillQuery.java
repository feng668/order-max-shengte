package com.whln.ordermax.data.domain.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * @author liurun
 * @date 2023-02-20 12:16
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BillQuery {
    /**
     * 商品的id
     */
    Collection<String> billIds;
}
