package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CustomerLabelRelation;
import com.whln.ordermax.data.mapper.UserLabelRelationMapper;
import org.springframework.stereotype.Repository;

/**
 * @author liurun
 * @date 2023-03-06 16:12
 */
@Repository
public class CustomerLabelRelationRepository extends ServiceImpl<UserLabelRelationMapper, CustomerLabelRelation> {
}
