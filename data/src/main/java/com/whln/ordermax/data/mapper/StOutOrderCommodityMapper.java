package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.StOutOrderCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.StOutOrderCommodity
 */
public interface StOutOrderCommodityMapper extends BaseMapper<StOutOrderCommodity> {

    List<StOutOrderCommodity> listByEntity(@Param("entity")StOutOrderCommodity entity);

    IPage<StOutOrderCommodity> listByEntity(@Param("entity")StOutOrderCommodity entity, Page<StOutOrderCommodity> page);

    Integer insertBatch(@Param("entitylist")List<StOutOrderCommodity> entitylist);

    List<StOutOrderCommodity> commodityList(BillQuery param);
}




