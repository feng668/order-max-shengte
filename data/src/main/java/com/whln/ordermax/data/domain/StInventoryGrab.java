package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
* 
* @TableName st_inventory_grab
*/
@ApiModel("")
@TableName("st_inventory_grab")
@Data
public class StInventoryGrab implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * 
    */
    private String id;
    /**
    * 销售合同id
    */
    @ApiModelProperty("销售合同id")
    private String orderId;
    /**
    * 商品id
    */
    @ApiModelProperty("商品id")
    private String commodityId;
    /**
    * 仓库id
    */
    @ApiModelProperty("仓库id")
    private String warehouseId;
    /**
    * 已占用数量
    */
    @ApiModelProperty("已占用数量")
    private Double grabNum;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    private Date updateTime;
    @ApiModelProperty("批号")
    private String commodityBatchNo;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;


    /*附加属性*/
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("仓库编号|回显")
    private String warehouseNo;
    @TableField(exist = false)
    @ApiModelProperty("仓库名称|回显")
    private String warehouseName;
    @TableField(exist = false)
    @ApiModelProperty("订单编号|回显")
    private String orderNo;
    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品英文规格|回显")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private Double cmNetWeight;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;
    @ApiModelProperty("简称")
    @TableField(exist = false)
    private String miniName;
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    @TableField(exist = false)
    private Integer contactTax;
    @TableField(exist = false)
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("元素")
    private String elements;
    @TableField(exist = false)
    @ApiModelProperty("纯度")
    private String purity;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
