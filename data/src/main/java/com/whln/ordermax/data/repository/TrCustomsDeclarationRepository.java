package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.TrCustomsDeclaration;
import com.whln.ordermax.data.domain.TrCustomsDeclarationCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.TrCustomsDeclarationCommodityMapper;
import com.whln.ordermax.data.mapper.TrCustomsDeclarationMapper;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Repository
@AllArgsConstructor
public class TrCustomsDeclarationRepository extends ServiceImpl<TrCustomsDeclarationMapper, TrCustomsDeclaration>{
    private final TrCustomsDeclarationCommodityMapper trCustomsDeclarationCommodityMapper;

    public IPage<TrCustomsDeclaration> pageByEntity(TrCustomsDeclaration entity, Page<TrCustomsDeclaration> page) {
        IPage<TrCustomsDeclaration> trCustomsDeclarationIPage = baseMapper.listByEntity(entity, page);
        List<TrCustomsDeclaration> records = trCustomsDeclarationIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records
                    .stream().map(TrCustomsDeclaration::getId)
                    .collect(Collectors.toSet());
            BillQuery param = new BillQuery();
            param.setBillIds(billIds);

            List<TrCustomsDeclarationCommodity> trCustomsDeclarationCommodityList = trCustomsDeclarationCommodityMapper
                    .commodityList(param);
            Map<String, List<TrCustomsDeclarationCommodity>> collect = trCustomsDeclarationCommodityList
                    .stream()
                    .collect(Collectors.groupingBy(TrCustomsDeclarationCommodity::getBillId));
            List<TrCustomsDeclaration> result = records.stream().peek(item -> {
                List<TrCustomsDeclarationCommodity> trCustomsDeclarationCommodityList1 = collect.get(item.getId());
                item.setCommodityList(trCustomsDeclarationCommodityList1);
            }).collect(Collectors.toList());
            trCustomsDeclarationIPage.setRecords(result);
        }
        return trCustomsDeclarationIPage;
    }

    public List<TrCustomsDeclaration> listByEntity(TrCustomsDeclaration entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<TrCustomsDeclaration> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public TrCustomsDeclaration getAllById(String id) {
        TrCustomsDeclaration allById = baseMapper.getAllById(id);
        BillQuery param = BillQuery.builder().billIds(CollUtils.toSet(allById.getId())).build();
        List<TrCustomsDeclarationCommodity> list = trCustomsDeclarationCommodityMapper.commodityList(param);
        allById.setCommodityList(list);
        return allById;
    }

}




