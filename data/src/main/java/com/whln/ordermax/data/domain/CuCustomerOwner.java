package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
* 客户
* @TableName cu_customer_owner
*/
@ApiModel("客户")
@TableName("cu_customer_owner")
@Data
public class CuCustomerOwner implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String sharerId;
    /**
    * 是否完全拥有人
    */
    @ApiModelProperty("是否完全拥有人")
    private Integer isFull;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
