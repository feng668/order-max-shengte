package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-03-20 13:38
 */
@ApiModel("邮件历史")
@Data
public class EmailHistoryVO {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("主题")
    private String theme;
    private String totalAmo;
    @ApiModelProperty("时间")
    private String billName;
    @ApiModelProperty("类型，发邮件  或者收邮件")
    private String type;
    @ApiModelProperty("billCode")
    private String billCode;
    @ApiModelProperty("拥有人名字")
    private String ownerName;
    @ApiModelProperty("时间")
    private LocalDateTime time;

}
