package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.CmCommodityCategory;
import com.whln.ordermax.data.mapper.CmCommodityCategoryMapper;

import java.util.List;

/**
 *
 */
@Repository
public class CmCommodityCategoryRepository extends ServiceImpl<CmCommodityCategoryMapper, CmCommodityCategory>{

    public IPage<CmCommodityCategory> pageByEntity(CmCommodityCategory entity, Page<CmCommodityCategory> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<CmCommodityCategory> listByEntity(CmCommodityCategory entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<CmCommodityCategory> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




