package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.SysUserRole;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysUserRole
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    Integer insertBatch(@Param("entitylist")List<SysUserRole> entitylist);

}




