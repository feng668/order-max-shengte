package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.PdAcceptance;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author liurun
 * @Entity whln.dyx.data.domain.Acceptance
 */
public interface AcceptanceMapper extends BaseMapper<PdAcceptance> {

    List<PdAcceptance> listByEntity(@Param("entity") PdAcceptance entity);

    Page<PdAcceptance> listByEntity(@Param("entity") PdAcceptance entity, Page<PdAcceptance> page);

    Integer insertBatch(@Param("entitylist")List<PdAcceptance> entitylist);

    PdAcceptance get(@Param("id") String id);
}




