package com.whln.ordermax.data.domain;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
* 商品检验标准
* @TableName cm_commodity_standard
*/
@ApiModel("商品检验标准")
@TableName("cm_commodity_standard")
@Data
public class CmCommodityStandard implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 商品id
    */
    @ApiModelProperty("商品id")
    private String commodityId;
    /**
    * 要素
    */
    @ApiModelProperty("要素")
    private String factor;
    /**
    * 标准条件
    */
    @ApiModelProperty("标准条件")
    private String standardCondition;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    public boolean check(){
        return ObjectUtil.isEmpty(factor) || ObjectUtil.isEmpty(standardCondition) || ObjectUtil.isEmpty(commodityId);
    }
}
