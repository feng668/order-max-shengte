package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.SupContactsEmail;
import com.whln.ordermax.data.mapper.SupContactsEmailMapper;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Repository
public class SupContactsEmailRepository extends ServiceImpl<SupContactsEmailMapper, SupContactsEmail>{

    public IPage<SupContactsEmail> pageByEntity(SupContactsEmail entity, Page<SupContactsEmail> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SupContactsEmail> listByEntity(SupContactsEmail entity) {
        if (ObjectUtil.isEmpty(entity.getContactId())){
            return new ArrayList<>();
        }
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SupContactsEmail> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<SupContactsEmail> listWithSupId() {
        return baseMapper.listWithSupId();
    }

}




