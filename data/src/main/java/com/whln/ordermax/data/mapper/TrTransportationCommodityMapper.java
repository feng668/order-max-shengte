package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.TrTransportationCommodity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.query.BillQuery;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.TrTransportationCommodity
 */
public interface TrTransportationCommodityMapper extends BaseMapper<TrTransportationCommodity> {

    List<TrTransportationCommodity> listByEntity(@Param("entity")TrTransportationCommodity entity);

    IPage<TrTransportationCommodity> listByEntity(@Param("entity")TrTransportationCommodity entity, Page<TrTransportationCommodity> page);

    Integer insertBatch(@Param("entitylist")List<TrTransportationCommodity> entitylist);

    List<TrTransportationCommodity> commodityList(BillQuery queryByBillIdParam);
}




