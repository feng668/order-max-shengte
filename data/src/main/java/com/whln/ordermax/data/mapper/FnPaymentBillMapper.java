package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.FnPaymentBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.FnPaymentBill
 */
public interface FnPaymentBillMapper extends BaseMapper<FnPaymentBill> {

    List<FnPaymentBill> listByEntity(@Param("entity")FnPaymentBill entity);

    IPage<FnPaymentBill> listByEntity(@Param("entity")FnPaymentBill entity, Page<FnPaymentBill> page);

    Integer insertBatch(@Param("entitylist")List<FnPaymentBill> entitylist);

    List<FnPaymentBill> listBySuperiorId(@Param("superiorId") String superiorId);
}




