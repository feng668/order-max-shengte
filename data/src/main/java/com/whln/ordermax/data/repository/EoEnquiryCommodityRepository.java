package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.EoEnquiryCommodity;
import com.whln.ordermax.data.mapper.EoEnquiryCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 * @author liurun
 */
@Repository
public class EoEnquiryCommodityRepository extends ServiceImpl<EoEnquiryCommodityMapper, EoEnquiryCommodity> {

    public IPage<EoEnquiryCommodity> pageByEntity(EoEnquiryCommodity entity, Page<EoEnquiryCommodity> page) {
        return baseMapper.listByEntity(entity, page);
    }

    public List<EoEnquiryCommodity> listByEntity(EoEnquiryCommodity entity) {
        List<EoEnquiryCommodity> eoEnquiryCommodities = baseMapper.listByEntity(entity);
        return eoEnquiryCommodities;
    }

    public Boolean insertBatch(List<EoEnquiryCommodity> entityList) {
        boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }
}




