package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CmMaterial;
import com.whln.ordermax.data.mapper.CmMaterialMapper;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository
public class CmMaterialRepository extends ServiceImpl<CmMaterialMapper, CmMaterial>{

    public IPage<CmMaterial> pageByEntity(CmMaterial entity, Page<CmMaterial> page) {
        return baseMapper.listByEntity(entity,page);
    }

//    public List<CmCommodity> listByEntity(CmCommodity entity) {
//        return baseMapper.listByEntity(entity);
//    }

    /*public Boolean insertBatch(List<CmCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public IPage<CmCommodity> pageWithLastQoPriceByCustId(CmCommodity entity, Page<CmCommodity> page) {
        return baseMapper.pageWithLastQoPriceByCustId(entity,page);
    }*/

}




