package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PrPricingOrder;
import com.whln.ordermax.data.mapper.PrPricingOrderMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class PrPricingOrderRepository extends ServiceImpl<PrPricingOrderMapper, PrPricingOrder>{

    public IPage<PrPricingOrder> pageByEntity(PrPricingOrder entity, Page<PrPricingOrder> page) {
        IPage<PrPricingOrder> prPricingOrderIPage = baseMapper.listByEntity(entity, page);

        return prPricingOrderIPage;
    }

    public List<PrPricingOrder> listByEntity(PrPricingOrder entity) {
        return baseMapper.listByEntity(entity);
    }

    public PrPricingOrder getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<PrPricingOrder> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




