package com.whln.ordermax.data.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
* oa单据审核记录
* @TableName oa_record
*/
@ApiModel("oa单据审核记录")
@TableName("oa_record")
@Data
public class OaRecord implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    /**
     * 单据id
     */
    @ApiModelProperty("单据id")
    private String billId;
    /**
     * 单据类别代码
     */
    @ApiModelProperty("单据类别代码")
    private String billCode;
    /**
     * 审批级别
     */
    @ApiModelProperty("审批级别")
    private Integer oaLevel;
    /**
     * 审批轮次
     */
    @ApiModelProperty("审批轮次")
    private Integer oaRound;
    /**
     * 审批级别名称
     */
    @ApiModelProperty("审批级别名称")
    private String oaLevelName;
    /**
     * 审批人id
     */
    @ApiModelProperty("操作人id")
    private String operatorId;
//    /**
//     * 单据拥有人id
//     */
//    @ApiModelProperty("单据拥有人id")
//    private String billOwnerId;
    /**
     * 操作类型
     */
    @ApiModelProperty("操作类型")
    private String recordType;
    /**
     * 备注信息
     */
    @ApiModelProperty("备注信息")
    private String remark;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


    /** 操作人姓名|回显 */
    @ApiModelProperty("审核人姓名|回显")
    private String operatorName;
    @ApiModelProperty("单据拥有人姓名|回显")
    private String billOwnerName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
