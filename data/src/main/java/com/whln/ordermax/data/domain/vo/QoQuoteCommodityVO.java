package com.whln.ordermax.data.domain.vo;

import cn.hutool.core.util.ObjectUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.CmCommodity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
* 外销报价商品清单
* @author zz
 * @TableName qo_quote_commodity
*/
@ApiModel("报价商品清单")
@Data
public class QoQuoteCommodityVO implements Serializable {

    /**
    * id
    */
    private String id;
    /**
     * 商品id
     */
    @ApiModelProperty("报价单id")
    private String billId;
    /**
    * 商品id
    */
    @ApiModelProperty("商品id")
    private String commodityId;
    /**
    * 单箱数量
    */
    @ApiModelProperty("单箱数量")
    private BigDecimal singleBoxNum;
    /**
    * 箱数
    */
    @ApiModelProperty("箱数")
    private BigDecimal containerNum;

    @ApiModelProperty("简称")

    private String miniName;


    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    private Integer contactTax;


    @NotNull(message = "元素不能为空")
    @ApiModelProperty("元素")
    private String elements;


    @ApiModelProperty("规格配比")
    private String ratio;


    @NotNull(message = "纯度不能为空")
    @ApiModelProperty("纯度")
    private String purity;
    /**
    * 报价数量
    */
    @ApiModelProperty("报价数量")
    private BigDecimal quotationNum;
    @ApiModelProperty("参考价格1")
    private String costPrice1;
    @ApiModelProperty("参考价格2")
    private String costPrice2;
    @ApiModelProperty("参考价格3")
   private String costPrice3;
    @ApiModelProperty("参考价格4")
    private String costPrice4;
    @ApiModelProperty("参考价格5")
    private String costPrice5;

    /**
    * 单价
    */
    @ApiModelProperty("单价")
    private BigDecimal price;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;


    @ApiModelProperty("单箱净重")
    private BigDecimal singleBoxNet;

    @ApiModelProperty("单箱毛重")
    private BigDecimal singleBoxWeight;

    @ApiModelProperty("单箱体积")
    private BigDecimal singleBoxVolume;

    @ApiModelProperty("含税单价")
    private BigDecimal taxPrice;

    @ApiModelProperty("税率")
    private BigDecimal rate;

    @ApiModelProperty("含税金额")
    private BigDecimal taxAmo;

    @ApiModelProperty("不含税金额")
    private BigDecimal amo;

    @ApiModelProperty("运费")
    private BigDecimal transportationPost;

    @ApiModelProperty("保费")
    private BigDecimal premiumPost;



    /* 附加属性 */

    @ApiModelProperty("商品编号|回显")
    private String partNo;

    @ApiModelProperty("商品中文名称|回显")
    private String cnName;

    @ApiModelProperty("商品英文名|回显")
    private String enName;

    @ApiModelProperty("商品规格|回显")
    private String specification;

    @ApiModelProperty("商品英文规格|回显")
    private String enSpecification;

    @ApiModelProperty("净重|回显")
    private BigDecimal cmNetWeight;

    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;

    @ApiModelProperty("增值税率|回显")
    private BigDecimal ccVatRate;

    @ApiModelProperty("商品体积|回显")
    private BigDecimal cmVolume;


    public void setField(CmCommodity cmCommodity) {
        if (ObjectUtil.isEmpty(cmCommodity)) {
            return;
        }

        this.cnName = cmCommodity.getCnName();
        this.enName = cmCommodity.getEnName();
        this.specification = cmCommodity.getSpecification();
        this.enSpecification = cmCommodity.getEnSpecification();
        this.quantityUnit = cmCommodity.getQuantityUnit();
        this.commodityId = cmCommodity.getId();
        this.singleBoxNet = cmCommodity.getSingleBoxNetWeight();
        this.singleBoxWeight = cmCommodity.getGrossWeight();
        this.cmVolume = cmCommodity.getSingleBoxVolume();
    }

    private static final long serialVersionUID = 1L;
}
