package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
* 客户联系人
* @TableName sup_supplier_contacts
*/
@ApiModel("客户联系人")
@TableName("sup_supplier_contacts")
@Data
public class SupSupplierContacts implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 客户id
    */
    @ApiModelProperty("客户id")
    private String supplierId;
    /**
    * 联系人姓名
    */
    @ApiModelProperty("联系人姓名")
    private String contactsName;
    /**
    * 称谓
    */
    @ApiModelProperty("称谓")
    private String nickname;

    @TableField(exist = false)
    @ApiModelProperty("联系人邮箱|入参")
    private List<SupContactsEmail> emails;
    /**
    * 性别
    */
    @ApiModelProperty("性别")
    private Boolean sex;

    /**
    * 职位
    */
    @ApiModelProperty("职位")
    private String position;
    /**
    * 电话
    */
    @ApiModelProperty("电话")
    private String phoneNum;

    /**
    * 手机
    */
    @ApiModelProperty("手机")
    private String mobilePhoneNum;
    /**
    * 传真
    */
    @ApiModelProperty("传真")
    private String fax;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    @TableField(exist = false)
    private String supPhoneNum;

    public void setSupPhoneNum(String supPhoneNum) {
        this.supPhoneNum = supPhoneNum;
        this.phoneNum = supPhoneNum;
    }

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
