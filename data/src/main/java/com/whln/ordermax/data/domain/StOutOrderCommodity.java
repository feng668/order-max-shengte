package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 出库单商品
 *
 * @TableName st_out_order_commodity
 */
@ApiModel("出库单商品")
@TableName("st_out_order_commodity")
@Data
public class StOutOrderCommodity implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    @ApiModelProperty("运费")
    private BigDecimal transportationPost;
    @ApiModelProperty("保费")
    private BigDecimal premiumPost;
    /**
     * 出库单id
     */
    @ApiModelProperty("出库单id")
    private String billId;
    private String supplierId;
    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    private String commodityId;
    /**
     * 单箱数量
     */
    @ApiModelProperty("单箱数量")
    private BigDecimal singleBoxNum;
    /**
     * 箱数
     */
    @ApiModelProperty("箱数")
    private BigDecimal containerNum;
    /**
     * 销售数量
     */
    @ApiModelProperty("出库数量")
    private BigDecimal totalNum;
    @ApiModelProperty("出库数量quotationNum")
    private BigDecimal quotationNum;

    @ApiModelProperty("应收数量")
    private BigDecimal quantityNum;
    @ApiModelProperty("实收数量")
    private BigDecimal realReceiveNum;

    @ApiModelProperty("开票数量")
    private BigDecimal invoicingNum;
    /**
     * 单箱净重
     */
    @ApiModelProperty("单箱净重")
    private BigDecimal singleBoxNet;

    @ApiModelProperty("仓库id")
    private String warehouseId;

    /**
     * 单箱毛重
     */
    @ApiModelProperty("单箱毛重")
    private BigDecimal singleBoxWeight;
    /**
     * 单箱体积
     */
    @ApiModelProperty("单箱体积")
    private BigDecimal singleBoxVolume;
    /**
     * 不含税单价
     */
    @ApiModelProperty("不含税单价")
    private BigDecimal price;

    /**
     * 销项税率
     */
    @ApiModelProperty("销项税率")
    private BigDecimal outputRate;
    /**
     * 销项税额
     */
    @ApiModelProperty("销项税额")
    private BigDecimal outputTax;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @ApiModelProperty("含税单价")
    private BigDecimal taxPrice;

    @ApiModelProperty("税率")
    private BigDecimal rate;

    @ApiModelProperty("含税金额")
    private BigDecimal taxAmo;

    @ApiModelProperty("不含税金额")
    private BigDecimal amo;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ApiModelProperty("批号")
    private String batchNo;
    private String commodityBatchNo;


    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品英文规格|回显")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private Double cmNetWeight;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;
    @TableField(exist = false)
    @ApiModelProperty("商品体积|回显")
    private Double cmVolume;
    @TableField(exist = false)
    @ApiModelProperty("仓库名字|回显")
    private String warehouseName;

    @ApiModelProperty("简称")
    @TableField(exist = false)
    private String miniName;
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    @TableField(exist = false)
    private Integer contactTax;
    @TableField(exist = false)
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("元素")
    private String elements;
    @TableField(exist = false)
    @ApiModelProperty("纯度")
    private String purity;

    @TableField(exist = false)
    @ApiModelProperty("模糊查询参数")
    private String searchInfo;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
