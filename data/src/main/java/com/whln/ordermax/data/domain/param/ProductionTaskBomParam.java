package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author liurun
 * @date 2023-02-20 18:41
 */
@Data
@ApiModel("生产任务单")
public class ProductionTaskBomParam {
    @ApiModelProperty("商品id")
    private String commodityId;
    @ApiModelProperty("商品数量")
    private Integer partNum;
    @ApiModelProperty("任务单id")
    @NotBlank(message = "生产任务单id不能为空")
    private String taskId;
}
