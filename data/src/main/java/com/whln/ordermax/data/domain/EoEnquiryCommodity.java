package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
* 询价商品清单
* @TableName eo_enquiry_commodity
*/
@ApiModel("询价商品清单")
@TableName("eo_enquiry_commodity")
@Data
public class EoEnquiryCommodity implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 询盘单id
    */
    @ApiModelProperty("询盘单id")
    private String billId;
    /**
    * 商品id
    */
    @ApiModelProperty("商品id")
    private String commodityId;

    @ApiModelProperty("单箱净重")
    private BigDecimal singleBoxNet;

    @ApiModelProperty("单箱毛重")
    private BigDecimal singleBoxWeight;

    @ApiModelProperty("单箱体积")
    private BigDecimal singleBoxVolume;
    /**
    * 单箱数量
    */
    @ApiModelProperty("单箱数量")
    private BigDecimal singleBoxNum;
    /**
    * 箱数
    */
    @ApiModelProperty("箱数")
    private BigDecimal containerNum;
    /**
    * 询盘数量
    */
    @ApiModelProperty("询盘数量")
    private BigDecimal quotationNum;
    /**
    * 单价
    */
    @ApiModelProperty("单价")
    private Double price;
    @ApiModelProperty("商品总金额")
    private Double amo;
    /**
    * 供应商
    */
    @ApiModelProperty("供应商")
    private String supplierId;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd H:m:s")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd H:m:s")
    private LocalDateTime updateDate;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;


    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    private String supplierName;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private Double cmNetWeight;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
