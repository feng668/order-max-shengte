package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* 入账核销单据
* @TableName fn_offset_bill
*/
@ApiModel("入账核销单据")
@TableName("fn_offset_bill")
@Data
public class FnOffsetBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 入账id
    */
    @ApiModelProperty("入账id")
    private String inAccountId;
    /**
    * 核销单据id
    */
    @ApiModelProperty("核销单据id")
    private String billId;


    /* 附加属性 */

    @TableField(exist = false)
    @ApiModelProperty("出运编号|回显")
    private String transportationNo;
    @TableField(exist = false)
    @ApiModelProperty("创建日期|回显")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    @TableField(exist = false)
    @ApiModelProperty("客户id")
    private String customerId;
    @TableField(exist = false)
    @ApiModelProperty("合同id")
    private String soOrderId;
    @TableField(exist = false)
    @ApiModelProperty("合同金额原币|回显")
    private Double originalCurTotalAmo;
    @TableField(exist = false)
    @ApiModelProperty("结算币种|回显")
    private String currency;
    @TableField(exist = false)
    @ApiModelProperty("汇率|回显")
    private Double rate;
    @TableField(exist = false)
    @ApiModelProperty("合同金额本币|回显")
    private Double localCurTotalAmo;
    @TableField(exist = false)
    @ApiModelProperty("出运总毛重|回显")
    private Double totalGrossWeight;
    @TableField(exist = false)
    @ApiModelProperty("出运总金额|回显")
    private Double totalAmo;
    @TableField(exist = false)
    @ApiModelProperty("出运总净重|回显")
    private Double totalNetWeight;
    @TableField(exist = false)
    @ApiModelProperty("出运总数量|回显")
    private Integer totalNum;
    @TableField(exist = false)
    @ApiModelProperty("装柜型号|回显")
    private String loadingType;
    @TableField(exist = false)
    @ApiModelProperty("装柜百分比|回显")
    private Double loadingPercentage;
    @TableField(exist = false)
    @ApiModelProperty("出运总体积|回显")
    private Double totalVolume;
    @TableField(exist = false)
    @ApiModelProperty("柜剩余体积|回显")
    private Double chestRemainingVolume;
    @TableField(exist = false)
    @ApiModelProperty("起运港|回显")
    private String initiallyPort;
    @TableField(exist = false)
    @ApiModelProperty("目的港|回显")
    private String finallyPort;
    @TableField(exist = false)
    @ApiModelProperty("运输方式|回显")
    private String transportType;
    @TableField(exist = false)
    @ApiModelProperty("价格条款|回显")
    private String priceTerms;
    @TableField(exist = false)
    @ApiModelProperty("船名航次|回显")
    private String voyageNo;
    @TableField(exist = false)
    @ApiModelProperty("货代|回显")
    private String freightForwarding;
    @TableField(exist = false)
    @ApiModelProperty("运输费|回显")
    private BigDecimal transportationPost;
    @TableField(exist = false)
    @ApiModelProperty("港杂费|回显")
    private Double portHandlingPost;
    @TableField(exist = false)
    @ApiModelProperty("单证费|回显")
    private Double documentPost;
    @TableField(exist = false)
    @ApiModelProperty("其他杂费|回显")
    private Double otherPost;
    @TableField(exist = false)
    @ApiModelProperty("创建时间|回显")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @TableField(exist = false)
    @ApiModelProperty("更新时间|回显")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    @TableField(exist = false)
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    @ApiModelProperty("客户中文名|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("销售订单单号|回显")
    private String soodOrderNo;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
