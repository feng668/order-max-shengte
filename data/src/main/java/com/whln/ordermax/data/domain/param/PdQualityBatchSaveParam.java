package com.whln.ordermax.data.domain.param;

import com.whln.ordermax.data.domain.FlFlowBillCommodity;
import com.whln.ordermax.data.domain.base.BillBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.whln.ordermax.data.domain.PdQuality;

import java.util.List;

/**
 * @author liurun
 * @date 2023-02-18 14:42
 */
@ApiModel("质检单批量保存参数")
@Data
public class PdQualityBatchSaveParam extends BillBase {
    @ApiModelProperty("生成质检单所在步骤的billid")
    private String parentBillId;
    @ApiModelProperty("生成质检单所在步骤的billCode")
    private String parentBillCode;

    @ApiModelProperty("质检单列表")
    List<PdQuality> entityList;
    private String id;

    private String flowBillCode;
    private List<FlFlowBillCommodity> commodityFlowList;
}
