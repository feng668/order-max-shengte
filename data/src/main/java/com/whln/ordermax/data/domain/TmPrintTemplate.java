package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
* 打印模板
* @TableName tm_print_template
*/
@ApiModel("打印模板")
@TableName("tm_print_template")
@Data
public class TmPrintTemplate implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 模板名称
    */
    @ApiModelProperty("模板名称")
    private String templateName;
    /**
    * 所属模块
    */
    @ApiModelProperty("所属模块")
    private String billCode;
    /**
    * html路径
    */
    @ApiModelProperty("html路径")
    private String htmlPath;
    /**
    * flt路径
    */
    @ApiModelProperty("flt路径")
    private String fltPath;
    /**
    * 备注
    */
    @ApiModelProperty("备注")
    private String note;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;


    /**打印模板*/
    @TableField(exist = false)
    private String htmlStr;
    @ApiModelProperty("搜索参数")
    @TableField(exist = false)
    private String searchInfo;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
