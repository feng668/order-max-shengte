package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.OrderPlan;
import com.whln.ordermax.data.mapper.OrderPlanMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class OrderPlanRepository extends ServiceImpl<OrderPlanMapper, OrderPlan>{

    public IPage<OrderPlan> pageByEntity(OrderPlan entity, Page<OrderPlan> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<OrderPlan> listByEntity(OrderPlan entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<OrderPlan> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<OrderPlan> listByBill(OrderPlan entity){
        return baseMapper.listByBill(entity);
    }

    public OrderPlan listByBill1(OrderPlan entity) {
        return baseMapper.listByBill1(entity);
    }
}




