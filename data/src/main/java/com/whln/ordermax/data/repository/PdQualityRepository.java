package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PdQuality;
import com.whln.ordermax.data.domain.PdQualityStandard;
import com.whln.ordermax.data.mapper.PdQualityMapper;
import com.whln.ordermax.data.mapper.PdQualityStandardMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class PdQualityRepository extends ServiceImpl<PdQualityMapper, PdQuality> {

    @Resource
    private PdQualityStandardMapper pdQualityStandardMapper;
    public IPage<PdQuality> pageByEntity(PdQuality entity, Page<PdQuality> page) {
        return baseMapper.listByEntity(entity, page);
    }

    public PdQuality getAllById(String id) {
        PdQuality pdQuality = baseMapper.get(id);
        List<PdQualityStandard> pdQualityStandards = pdQualityStandardMapper.selectList(new LambdaQueryWrapper<PdQualityStandard>().eq(PdQualityStandard::getQualityId, pdQuality.getId()));
        pdQuality.setStandardList(pdQualityStandards);
        return pdQuality;
    }

    public List<PdQuality> listByEntity(PdQuality entity) {
        return baseMapper.listByEntity(entity);
    }

}




