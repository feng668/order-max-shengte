package com.whln.ordermax.data.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liurun
 * @date 2023-03-08 18:08
 */
@Data
public class TransactionNumAnAmountVO implements Serializable {
    private static final long serialVersionUID = 3836481763631845697L;
    private String id;
    private String billNo;
    private BigDecimal price;
    private String billName;
    private String billCode;
    private BigDecimal quotationNum;
    private Date time;
}
