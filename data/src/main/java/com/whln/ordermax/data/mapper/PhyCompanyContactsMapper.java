package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PhyCompanyContacts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PhyCompanyContacts
 */
public interface PhyCompanyContactsMapper extends BaseMapper<PhyCompanyContacts> {

    List<PhyCompanyContacts> listByEntity(@Param("entity")PhyCompanyContacts entity);

    IPage<PhyCompanyContacts> listByEntity(@Param("entity")PhyCompanyContacts entity, Page<PhyCompanyContacts> page);

    Integer insertBatch(@Param("entitylist")List<PhyCompanyContacts> entitylist);

}




