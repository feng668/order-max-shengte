package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.QoQuoteCommodity;
import com.whln.ordermax.data.domain.QoQuoteOrder;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.QoQuoteCommodityMapper;
import com.whln.ordermax.data.mapper.QoQuoteOrderMapper;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Repository
public class QoQuoteOrderRepository extends ServiceImpl<QoQuoteOrderMapper, QoQuoteOrder>{
    @Resource
    private QoQuoteCommodityMapper qoQuoteCommodityMapper;
    public IPage<QoQuoteOrder> pageByEntity(QoQuoteOrder entity, Page<QoQuoteOrder> page) {
        IPage<QoQuoteOrder> qoQuoteOrderIPage = baseMapper.listByEntity(entity, page);
        List<QoQuoteOrder> records = qoQuoteOrderIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(QoQuoteOrder::getId).collect(Collectors.toSet());
            BillQuery param = new BillQuery();
            param.setBillIds(billIds);
            List<QoQuoteCommodity> qoQuoteCommodities = qoQuoteCommodityMapper.commodityList(param);


            Map<String, List<QoQuoteCommodity>> idMap = qoQuoteCommodities.stream()
                    .collect(Collectors.groupingBy(QoQuoteCommodity::getBillId));
            for(QoQuoteOrder q:records){
                List<QoQuoteCommodity> qoQuoteCommodities1 = idMap.get(q.getId());
                q.setCommodityList(qoQuoteCommodities1);
            }
            qoQuoteOrderIPage.setRecords(records);
        }

        return qoQuoteOrderIPage;
    }

    public List<QoQuoteOrder> listByEntity(QoQuoteOrder entity) {
        return baseMapper.listByEntity(entity);
    }

    public QoQuoteOrder getAllById(String id) {
        QoQuoteOrder allById = baseMapper.getAllById(id);
        BillQuery param = new BillQuery();
        param.setBillIds(CollUtils.toSet(allById.getId()));

        List<QoQuoteCommodity> select = qoQuoteCommodityMapper.commodityList(param);
        allById.setCommodityList(select);
        return allById;
    }

    public Boolean insertBatch(List<QoQuoteOrder> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public Map<String, Object> getByIdWithInfo(String queteId) {
        return baseMapper.getByIdWithInfo(queteId);
    }

}




