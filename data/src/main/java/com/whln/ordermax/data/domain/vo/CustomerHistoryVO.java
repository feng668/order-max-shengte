package com.whln.ordermax.data.domain.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author liurun
 * @date 2023-03-08 21:22
 */
@Data
public class CustomerHistoryVO implements Serializable {
    private static final long serialVersionUID = -7206733206710617500L;
    private String billNo;
    private String billId;
    private String billCode;
    private String partNo;
    private String cnName;
    private String enName;
    private String specification;
    private String enSpecification;
    private String supplierId;
}
