package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.QoQuoteCommodityPrice;
import com.whln.ordermax.data.mapper.QoQuoteCommodityPriceMapper;

import java.util.List;

/**
 *
 */
@Repository
public class QoQuoteCommodityPriceRepository extends ServiceImpl<QoQuoteCommodityPriceMapper, QoQuoteCommodityPrice> {

    public IPage<QoQuoteCommodityPrice> pageByEntity(QoQuoteCommodityPrice entity, Page<QoQuoteCommodityPrice> page) {
        return baseMapper.listByEntity(entity, page);
    }

    public List<QoQuoteCommodityPrice> listByEntity(QoQuoteCommodityPrice entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<QoQuoteCommodityPrice> entityList) {
        Boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }

    public boolean saveOrUpdateBatchByMutiId(List<QoQuoteCommodityPrice> priceList) {
        for (QoQuoteCommodityPrice price : priceList) {
            QoQuoteCommodityPrice exsits = this.getOne(
                    new QueryWrapper<QoQuoteCommodityPrice>().lambda()
                            .eq(QoQuoteCommodityPrice::getQuoteId, price.getQuoteId())
                            .eq(QoQuoteCommodityPrice::getCommodityId, price.getCommodityId())
                            .eq(QoQuoteCommodityPrice::getPriceLevelId, price.getPriceLevelId())
            );
            if (exsits != null) {
                this.update(
                        new UpdateWrapper<QoQuoteCommodityPrice>().lambda()
                                .eq(QoQuoteCommodityPrice::getQuoteId, price.getQuoteId())
                                .eq(QoQuoteCommodityPrice::getCommodityId, price.getCommodityId())
                                .eq(QoQuoteCommodityPrice::getPriceLevelId, price.getPriceLevelId())
                                .set(QoQuoteCommodityPrice::getPrice, price.getPrice())
                );
            }else {
                this.save(price);
            }
        }
        return true;
    }

    public List<QoQuoteCommodityPrice> listNewPriceByComIdAndCustId(String commodityId, String customerId) {
        return baseMapper.listNewPriceByComIdAndCustId(commodityId,customerId);
    }
}




