package com.whln.ordermax.data.domain.vo;

import com.whln.ordermax.data.domain.base.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author liurun
 * @date 2023-03-06 14:58
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CustomerLabelVO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -648779431822606346L;
    /**
     * id
     */
    @ApiModelProperty("id")
    private String id;
    private String createBy;
    private String updateBy;
    /**
     * 标签名
     */
    @ApiModelProperty("标签名")
    @NotBlank(message = "组名不能为空")
    private String labelName;

    /**
     * 组id
     */
    @ApiModelProperty("组名")
    private String groupName;
    private String type;

    private String groupId;

}
