package com.whln.ordermax.data.domain.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-17 16:13
 */
@Data
public class GetCustomerInfoQuery {
    @ApiModelProperty("拥有人id")
    private String ownerId;
}
