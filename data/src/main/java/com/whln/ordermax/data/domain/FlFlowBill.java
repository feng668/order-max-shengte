package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;

/**
* 
* @TableName fl_flow_bill
*/
@ApiModel("")
@TableName("fl_flow_bill")
@Data
public class FlFlowBill implements Serializable {

    /**
    * 单据id
    */
    private String billId;
    /**
    * 单据编码
    */
    @ApiModelProperty("单据编码")
    private String billCode;
    /**
    * 流转后的单据编码
    */
    @ApiModelProperty("流转后的单据编码")
    private String flowBillCode;
    /**
    * 流转状态
    */
    @ApiModelProperty("流转状态")
    private Integer flowStatus;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
