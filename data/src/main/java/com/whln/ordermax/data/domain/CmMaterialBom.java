package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@TableName("cm_material_bom")
@ApiModel("原料与配件关系")
@Data
public class CmMaterialBom implements Serializable {


    /**
     * 配件id
     */
    @ApiModelProperty("配件id")
    private String materialId;
    /**
     * 配件id
     */
    @ApiModelProperty("原料id")
    private String partId;
    /**
     * 配件数量
     */
    @ApiModelProperty("原料数量")
    private BigDecimal partNum;
}
