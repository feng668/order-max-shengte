package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.OaRule;
import com.whln.ordermax.data.mapper.OaRuleMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class OaRuleRepository extends ServiceImpl<OaRuleMapper, OaRule>{

    public IPage<OaRule> pageByEntity(OaRule entity, Page<OaRule> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<OaRule> listByEntity(OaRule entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<OaRule> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




