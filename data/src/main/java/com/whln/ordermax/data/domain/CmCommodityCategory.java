package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
* 商品
* @TableName cm_commodity_category
*/
@ApiModel("商品分类")
@TableName("cm_commodity_category")
@Data
public class CmCommodityCategory implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 商品分类名称
    */
    @ApiModelProperty("商品分类名称")
    private String cnName;
    /**
    * 
    */
    @ApiModelProperty("")
    private String parentId;
    /**
    * 
    */
    @ApiModelProperty("")
    private String parentIds;
    /**
    * 上级分类名称
    */
    @ApiModelProperty("上级分类名称")
    private String parentCnNames;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @TableField(exist = false)
    @ApiModelProperty("子节点")
    private List<CmCommodityCategory> chirds;

    @ApiModelProperty("分类类别(商品=COMMODITY，配件=PART)")
    private String articleType;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
