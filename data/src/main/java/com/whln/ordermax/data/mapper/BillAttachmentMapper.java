package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.BillAttachment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.BillAttachment
 */
public interface BillAttachmentMapper extends BaseMapper<BillAttachment> {

    List<BillAttachment> listByEntity(@Param("entity")BillAttachment entity);

    IPage<BillAttachment> listByEntity(@Param("entity")BillAttachment entity, Page<BillAttachment> page);

    BillAttachment getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<BillAttachment> entitylist);

}




