package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-27 17:26
 */
@ApiModel("导入的返回值")
@Data
public class QoQuoteCommodityImportVO {
    @ApiModelProperty("存在的")
    List<QoQuoteCommodityVO> exits ;
    @ApiModelProperty("不存在的")
    List<QoQuoteCommodityVO> noExits;
    private Integer exitsNum;
    private Integer noExitsNum;
}
