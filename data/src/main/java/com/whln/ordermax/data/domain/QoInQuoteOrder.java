package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.FilterOaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
* 内销报价单
* @TableName qo_in_quote_order
*/
@ApiModel("内销报价单")
@TableName("qo_in_quote_order")
@Data
public class QoInQuoteOrder extends FilterOaBill implements Serializable {


    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    /**
     * 报价单编号
     */
    @ApiModelProperty("报价单编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;
    /**
     * 报价日期
     */
    @ApiModelProperty("报价日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate quotationDate;

    /**
     * 客户id
     */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
     * 有效天数
     */
    @ApiModelProperty("有效天数")
    private Integer effectiveDays;
    /**
     * 结算币种
     */
    @ApiModelProperty("结算币种")
    private String currency;
    /**
     * 汇率
     */
    @ApiModelProperty("汇率")
    private Double rate;
    /**
     * 商品总金额
     */
    @ApiModelProperty("商品总金额")
    private Double totalAmo;
    /**
     * 出货公司
     */
    @ApiModelProperty("出货公司")
    private String companyId;
    /**
     * 出货公司银行
     */
    @ApiModelProperty("出货公司银行")
    private String companyBank;
    /**
     * 客户联系人id
     */
    @ApiModelProperty("客户联系人id")
    private String customerContactsId;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @DateTimeFormat( pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    /**
     * 商品总体积
     */
    @ApiModelProperty("商品总体积")
    private Double totalVolume;
    /**
     * 商品总数量
     */
    @ApiModelProperty("商品总数量")
    private double totalNum;
    /**
     * 运输方式
     */
    @ApiModelProperty("运输方式")
    private String transportType;
    /**
     * 价格条款
     */
    @ApiModelProperty("价格条款")
    private String priceTerms;
    /**
     * 启运港口
     */
    @ApiModelProperty("启运港口")
    private String initiallyPort;
    /**
     * 目的港口
     */
    @ApiModelProperty("目的港口")
    private String finallyPort;
    /**
     * 运输单号
     */
    @ApiModelProperty("运输单号")
    private String transportOrderNo;

    @ApiModelProperty("截止日期")
    private LocalDate expirationDate;
    @ApiModelProperty("付款方式")
    private String paymentType;
    @ApiModelProperty("产品包装")
    private String packagingRemark;

    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("保存的报价单商品|入参")
    private List<QoInQuoteCommodity> commodityList;
    @TableField(exist = false)
    @ApiModelProperty("删除的报价单商品id数组|入参")
    private List<String> commodityDelIdList;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;

    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("客户地址|回显")
    private String custAddress;
    @TableField(exist = false)
    @ApiModelProperty("客户电话|回显")
    private String custPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("客户开户行|回显")
    private String custBank;
    @TableField(exist = false)
    @ApiModelProperty("客户银行账号|回显")
    private String custBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("客户邮编|回显")
    private String custZipCode;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司邮编|回显")
    private String cpnZipCode;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    private String searchInfo;
    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
