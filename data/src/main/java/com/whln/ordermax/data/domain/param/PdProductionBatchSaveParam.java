package com.whln.ordermax.data.domain.param;

import com.whln.ordermax.data.domain.FlFlowBillCommodity;
import com.whln.ordermax.data.domain.PdProductionTask;
import com.whln.ordermax.data.domain.base.BillBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-02-25 14:46
 */
@ApiModel
@Data
public class PdProductionBatchSaveParam extends BillBase {
    @ApiModelProperty("生成质检单所在步骤的billid")
    private String parentBillId;
    @ApiModelProperty("生成质检单所在步骤的billCode")
    private String parentBillCode;

    private String parentBillNo;
    @ApiModelProperty("生产任务单列表")
    List<PdProductionTask> entityList;
    @ApiModelProperty("sourceBillCode")
    private String sourceBillCode;
    @ApiModelProperty("sourceBillIds")
    private String sourceBillIds;
    private String id;
    @ApiModelProperty("billId")
    private String billId;
    private String billCode;
    @ApiModelProperty("flowBillCode")
    private String flowBillCode;
    private List<FlFlowBillCommodity> commodityFlowList;
}
