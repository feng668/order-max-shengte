package com.whln.ordermax.data.domain.base;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class OaBill extends FilterBill{

    /**
     * 审批状态（0=驳回，1=审核中，2=审核通过）
     */
    private Integer oaStatus;

    /**
     * 审批级别
     */
    private Integer oaLevel;

    @TableField(exist = false)
    private String getOaPendding;
}
