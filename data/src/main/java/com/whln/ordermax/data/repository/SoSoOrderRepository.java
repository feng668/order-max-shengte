package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.SoSoOrder;
import com.whln.ordermax.data.domain.SoSoOrderCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.SoSoOrderCommodityMapper;
import com.whln.ordermax.data.mapper.SoSoOrderMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class SoSoOrderRepository extends ServiceImpl<SoSoOrderMapper, SoSoOrder> {
    @Resource
    private SoSoOrderCommodityMapper soSoOrderCommodityMapper;
    public IPage<SoSoOrder> pageByEntity(SoSoOrder entity, Page<SoSoOrder> page) {
        return baseMapper.listByEntity(entity, page);
    }

    public List<SoSoOrder> listByEntity(SoSoOrder entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SoSoOrder> entityList) {
        Boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }


    public SoSoOrder getAllById(String id) {
        SoSoOrder soSoOrder = baseMapper.getAllById(id);
        if (ObjectUtil.isEmpty(soSoOrder)) {
            return null;
        }
        BillQuery param = BillQuery.builder()
                .billIds(CollUtils.toSet(soSoOrder.getId()))
                .build();
        List<SoSoOrderCommodity> soSoOrderCommodityList = soSoOrderCommodityMapper.commodityList(param);
        soSoOrder.setCommodityList(soSoOrderCommodityList);
        return soSoOrder;
    }
}




