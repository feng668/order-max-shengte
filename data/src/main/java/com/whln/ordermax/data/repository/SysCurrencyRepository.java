package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.SysCurrency;
import com.whln.ordermax.data.domain.param.ExcelExportParam;
import com.whln.ordermax.data.mapper.SysCurrencyMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class SysCurrencyRepository extends ServiceImpl<SysCurrencyMapper, SysCurrency>{

    public IPage<SysCurrency> pageByEntity(SysCurrency entity, Page<SysCurrency> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SysCurrency> listByEntity(SysCurrency entity) {
        return baseMapper.listByEntity(entity);
    }

    public SysCurrency getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<SysCurrency> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<SysCurrency> listNewst(SysCurrency entity) {
        return baseMapper.listNewst(entity);
    }

    public List<SysCurrency> listHistory(SysCurrency entity) {
        return baseMapper.listHistory(entity);
    }

    public List<SysCurrency> excel(ExcelExportParam param) {
        return baseMapper.excel(param);
    }
}




