package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.SysRolePower;
import com.whln.ordermax.data.mapper.SysRolePowerMapper;

import java.util.List;

/**
 *
 */
@Repository
public class SysRolePowerRepository extends ServiceImpl<SysRolePowerMapper, SysRolePower>{


    public Boolean insertBatch(List<SysRolePower> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




