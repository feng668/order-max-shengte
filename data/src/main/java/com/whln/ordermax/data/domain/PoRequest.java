package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.whln.ordermax.data.domain.base.FlowFilterOaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 采购申请单
* @TableName po_request
*/
@ApiModel("采购申请单")
@TableName("po_request")
@Data
public class PoRequest extends FlowFilterOaBill  implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    private String parentBillId;

    private String parentBillCode;
    /**
     * 报价单id
     */
    @ApiModelProperty("销售合同id")
    private String sourceBillId;
    /**
     * 合同编号
     */
    @ApiModelProperty("合同编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;
    /**
     * 供应商id
     */
    @ApiModelProperty("供应商id")
    private String departmentId;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;
    /**
     * 公司id
     */
    @ApiModelProperty("公司id")
    private String companyId;
    @ApiModelProperty("来源  外销0  内销1")
    private String sourceType;
    /**
     * 公司人员id
     */
    @ApiModelProperty("公司人员id")
    private String companyPersonnelId;
    /**
     * 创建人id
     */
    @ApiModelProperty("创建人id")
    private String createrId;
    /**
     * 创建日期
     */
    @ApiModelProperty("创建日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
     * 客户订单
     */
    @ApiModelProperty("客户订单")
    private String customerOrderNo;
    /**
     * 合同总金额（本币）
     */
    @ApiModelProperty("合同总金额（本币）")
    private Double localCurTotalAmo;
    /**
     * 付款方式
     */
    @ApiModelProperty("付款方式")
    private String payType;
    /**
     * 合同总数量
     */
    @ApiModelProperty("合同总数量")
    private Integer totalNum;
    /**
     * 合同总体积
     */
    @ApiModelProperty("合同总体积")
    private Double totalVolume;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 生产要求
     */
    @ApiModelProperty("生产要求")
    private String productionRequested;
    /**
     * 包装要求
     */
    @ApiModelProperty("包装要求")
    private String packagingRequested;
    /**
     * 交付要求
     */
    @ApiModelProperty("交付要求")
    private String deliveryRequested;
    /**
     * 验收标准
     */
    @ApiModelProperty("验收标准")
    private String acceptanceStandard;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    @ApiModelProperty("来源|IN内销,OUT外销")
    private String inOutType;
    @ApiModelProperty("交货日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("保存的申请单商品|入参")
    private List<PoRequestCommodity> commodityList;
    @TableField(exist = false)
    @ApiModelProperty("删除的申请单单商品id数组|入参")
    private List<String> commodityDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("流转的申请单商品id数组|入参")
    private List<String> commodityFlowIdList;

    @TableField(exist = false)
    @ApiModelProperty("供应商中文名称|回显")
    private String supName;
    @TableField(exist = false)
    @ApiModelProperty("供应商地址|回显")
    private String supAddress;
    @TableField(exist = false)
    @ApiModelProperty("供应商电话|回显")
    private String supPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("供应商开户行|回显")
    private String supBank;
    @TableField(exist = false)
    @ApiModelProperty("供应商银行账号|回显")
    private String supBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("供应商邮编|回显")
    private String suptZipCode;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司邮编|回显")
    private String cpnZipCode;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("创建人姓名|回显")
    private String createrName;
    @TableField(exist = false)
    @ApiModelProperty("部门名称|回显")
    private String dptName;
    @TableField(exist = false)
    @ApiModelProperty("部门全称|回显")
    private String fullDptName;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    @TableField(exist = false)
    @JsonIgnore
    private String lastBillCode;
    @TableField(exist = false)
    @JsonIgnore
    private String lastBillId;

}
