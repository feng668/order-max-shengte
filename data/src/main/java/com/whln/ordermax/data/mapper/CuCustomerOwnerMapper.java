package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.CuCustomerOwner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.ordermax.data.domain.CuCustomerOwner
 */
public interface CuCustomerOwnerMapper extends BaseMapper<CuCustomerOwner> {

    List<CuCustomerOwner> listByEntity(@Param("entity")CuCustomerOwner entity);

    IPage<CuCustomerOwner> listByEntity(@Param("entity")CuCustomerOwner entity, Page<CuCustomerOwner> page);

    CuCustomerOwner getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<CuCustomerOwner> entitylist);

}




