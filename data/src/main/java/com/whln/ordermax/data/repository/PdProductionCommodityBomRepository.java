package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PdProductionCommodityBom;
import com.whln.ordermax.data.domain.param.ProductionTaskBomParam;
import com.whln.ordermax.data.mapper.PdProductionCommodityBomMapper;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 */
@Repository
public class PdProductionCommodityBomRepository extends ServiceImpl<PdProductionCommodityBomMapper, PdProductionCommodityBom> {

    public IPage<PdProductionCommodityBom> pageByEntity(PdProductionCommodityBom entity, Page<PdProductionCommodityBom> page) {
        return baseMapper.listByEntity(entity, page);
    }

    public List<PdProductionCommodityBom> listByEntity(PdProductionCommodityBom entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PdProductionCommodityBom> entityList) {
        Boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }

    public boolean saveOrUpdateBatchByMutiId(Collection<PdProductionCommodityBom> bomList) {
        List<String> productionIds = bomList.stream().map(PdProductionCommodityBom::getProductionId)
                .collect(Collectors.toList());
        List<PdProductionCommodityBom> pdProductionCommodityBomList = baseMapper.selectList(
                new LambdaQueryWrapper<PdProductionCommodityBom>()
                        .in(PdProductionCommodityBom::getProductionId, productionIds));
        Map<String, PdProductionCommodityBom> toMapByProdIdAndPartId = pdProductionCommodityBomList.stream()
                .collect(Collectors.toMap(
                        item -> item.getProductionId() + item.getPartId(), Function.identity(), (x, y) -> x
                ));
        for (PdProductionCommodityBom bom : bomList) {
            PdProductionCommodityBom exsits = toMapByProdIdAndPartId.get(bom.getProductionId() + bom.getPartId());
            if (ObjectUtil.isEmpty(exsits)) {
                this.update(
                        new UpdateWrapper<PdProductionCommodityBom>().lambda()
                                .eq(PdProductionCommodityBom::getPartId, bom.getPartId())
                                .eq(PdProductionCommodityBom::getProductionId, bom.getProductionId())
                                .set(PdProductionCommodityBom::getPartNum, bom.getPartNum())
                );
            } else {
                baseMapper.insert(bom);
            }
        }
        return true;
    }

    public List<PdProductionCommodityBom> getProductionTaskBom(ProductionTaskBomParam param) {
        return baseMapper.getProductionTaskBom(param);
    }
}




