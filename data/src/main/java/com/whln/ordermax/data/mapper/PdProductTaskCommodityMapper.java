package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.param.ProductionTaskBomParam;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.PdProductTaskCommodity;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PdProductTaskCommodity
 */
public interface PdProductTaskCommodityMapper extends BaseMapper<PdProductTaskCommodity> {

    List<PdProductTaskCommodity> listByEntity(@Param("entity")PdProductTaskCommodity entity);

    IPage<PdProductTaskCommodity> listByEntity(@Param("entity")PdProductTaskCommodity entity, Page<PdProductTaskCommodity> page);

    Integer insertBatch(@Param("entitylist")List<PdProductTaskCommodity> entitylist);

    List<PdProductTaskCommodity> getProductionTaskBom(ProductionTaskBomParam param);
}




