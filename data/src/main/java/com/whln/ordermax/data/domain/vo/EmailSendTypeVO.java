package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-17 16:39
 */
@ApiModel("收件箱发送人的类型")
@Data
public class EmailSendTypeVO {
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("邮箱地址")
    private String emailAddress;

    @ApiModelProperty("类型")
    private String type;
    @ApiModelProperty("公司名")
    private String name;
}
