package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.CmMaterial;
import org.apache.ibatis.annotations.Param;

/**
 * @author liurun
 */
public interface CmMaterialMapper extends BaseMapper<CmMaterial> {
    /**
     * 根据数据库实体查询列表
     * @param entity
     * @param page
     * @return
     */
    IPage<CmMaterial> listByEntity(@Param("entity")CmMaterial entity, Page<CmMaterial> page);
}
