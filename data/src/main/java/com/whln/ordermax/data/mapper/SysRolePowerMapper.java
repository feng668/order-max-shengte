package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.SysRolePower;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysRolePower
 */
public interface SysRolePowerMapper extends BaseMapper<SysRolePower> {
    Integer insertBatch(@Param("entitylist") List<SysRolePower> entitylist);

}




