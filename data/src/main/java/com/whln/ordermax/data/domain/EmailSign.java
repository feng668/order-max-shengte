package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liurun
 * @date 2023-02-21 18:45
 */
@Data
@TableName("email_sign")
public class EmailSign implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 关联id
     */
    private String relationId;
    /**
     * 签名
     */
    private String sign;
    /**
     * 标题
     */
    private String title;
    /**
     *html内容
     */
    private String html;
}
