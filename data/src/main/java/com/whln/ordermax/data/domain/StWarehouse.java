package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* 
* @TableName st_warehouse
*/
@ApiModel("仓库")
@TableName("st_warehouse")
@Data
public class StWarehouse implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 仓库编号
    */
    @ApiModelProperty("仓库编号")
    @NotBlank
    private String warehouseNo;
    /**
    * 仓库名称
    */
    @ApiModelProperty("仓库名称")
    private String warehouseName;
    /**
    * 地址
    */
    @ApiModelProperty("地址")
    private String address;
    /**
    * 电话
    */
    @ApiModelProperty("电话")
    private String phone;
    /**
    * 传真
    */
    @ApiModelProperty("传真")
    private String fax;
    /**
    * 属性
    */
    @ApiModelProperty("属性")
    private Integer warehouseAttribute;
    /**
    * 保税仓库
    */
    @ApiModelProperty("保税仓库")
    private Integer isBondedWarehouse;
    /**
    * 供应商id
    */
    @ApiModelProperty("供应商id")
    private String vendorId;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名")
    private String ownerName;
    /**
    * 录入日期
    */
    @ApiModelProperty("录入日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    @ApiModelProperty("模糊搜索")
    @TableField(exist = false)
    private String searchInfo;

    @ApiModelProperty("模糊搜索")
    @TableField(exist = false)
    private String commodityId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
