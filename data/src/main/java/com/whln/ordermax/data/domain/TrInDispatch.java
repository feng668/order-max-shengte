package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.whln.ordermax.data.domain.base.FilterOaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 内销发货单
* @TableName tr_in_dispatch
*/
@ApiModel("内销发货单")
@TableName("tr_in_dispatch")
@Data
public class TrInDispatch extends FilterOaBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;

    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    private String parentBillId;
    
    private String parentBillCode;
    private String parentBillNo;
    /**
     * 出运编号
     */
    @ApiModelProperty("出运编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;

//    private String billId;
    /**
    * 创建日期
    */
    @ApiModelProperty("创建日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;
    /**
    * 客户id
    */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
    * 公司id
    */
    @ApiModelProperty("公司id")
    private String companyId;
    /**
    * 货代公司id
    */
    @ApiModelProperty("货代公司id")
    private String forwarderCompanyId;
    /**
    * 合同id
    */
    @ApiModelProperty("合同id")
    private String soOrderId;
    /**
    * 合同金额原币
    */
    @ApiModelProperty("合同金额原币")
    private BigDecimal originalCurTotalAmo;
    /**
    * 结算币种
    */
    @ApiModelProperty("结算币种")
    private String currency;
    /**
    * 汇率
    */
    @ApiModelProperty("汇率")
    private BigDecimal rate;
    /**
    * 合同金额本币
    */
    @ApiModelProperty("合同金额本币")
    private BigDecimal localCurTotalAmo;
    /**
    * 出运总毛重
    */
    @ApiModelProperty("出运总毛重")
    private BigDecimal totalGrossWeight;
    /**
    * 出运总金额
    */
    @ApiModelProperty("出运总金额")
    private BigDecimal totalAmo;
    /**
    * 出运总净重
    */
    @ApiModelProperty("出运总净重")
    private BigDecimal totalNetWeight;
    /**
    * 出运总数量
    */
    @ApiModelProperty("出运总数量")
    private Integer totalNum;
    /**
    * 装柜型号
    */
    @ApiModelProperty("装柜型号")
    private String loadingType;
    /**
    * 装柜百分比
    */
    @ApiModelProperty("装柜百分比")
    private BigDecimal loadingPercentage;
    /**
    * 出运总体积
    */
    @ApiModelProperty("出运总体积")
    private BigDecimal totalVolume;
    /**
    * 柜剩余体积
    */
    @ApiModelProperty("柜剩余体积")
    private BigDecimal chestRemainingVolume;
    /**
    * 起运港
    */
    @ApiModelProperty("起运港")
    private String initiallyPort;
    /**
    * 目的港
    */
    @ApiModelProperty("目的港")
    private String finallyPort;
    /**
    * 运输方式
    */
    @ApiModelProperty("运输方式")
    private String transportType;
    /**
    * 价格条款
    */
    @ApiModelProperty("价格条款")
    private String priceTerms;
    /**
    * 船名航次
    */
    @ApiModelProperty("船名航次")
    private String voyageno;
    /**
    * 货代
    */
    @ApiModelProperty("货代")
    private String freightForwarding;
    /**
    * 运输费
    */
    @ApiModelProperty("运输费")
    private BigDecimal transportationPost;
    /**
    * 港杂费
    */
    @ApiModelProperty("港杂费")
    private BigDecimal portHandlingPost;
    /**
    * 单证费
    */
    @ApiModelProperty("单证费")
    private BigDecimal documentPost;
    /**
    * 其他杂费
    */
    @ApiModelProperty("其他杂费")
    private BigDecimal otherPost;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    /**
    * 是否核销
    */
    @ApiModelProperty("是否核销")
    private Boolean isOffset;
    /**
    * 保费
    */
    @ApiModelProperty("保费")
    private BigDecimal premiumPost;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;
    @TableField(exist = false)
    private String billCode;
    @TableField(exist = false)
    private String billId;
    @TableField(exist = false)
    private String sourceBillId;
    @TableField(exist = false)
    private String sourceBillCode;
    @TableField(exist = false)
    private String lastBillCode;
    @TableField(exist = false)
    private String lastBillId;
    @TableField(exist = false)
    private List<String> idNotIn;
    @TableField(exist = false)
    @ApiModelProperty("保存的发货单商品|入参")
    private List<TrInDispatchCommodity> commodityList;
    @TableField(exist = false)
    @ApiModelProperty("删除的发货单商品id数组|入参")
    private List<String> commodityDelIdList;

    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("客户地址|回显")
    private String custAddress;
    @TableField(exist = false)
    @ApiModelProperty("客户电话|回显")
    private String custPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("客户开户行|回显")
    private String custBank;
    @TableField(exist = false)
    @ApiModelProperty("客户银行账号|回显")
    private String custBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("客户邮编|回显")
    private String custZipCode;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司邮编|回显")
    private String cpnZipCode;
    @TableField(exist = false)
    @ApiModelProperty("销售订单单号|回显")
    private String soodOrderNo;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("货代公司名称|回显")
    private String phycFullName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;


    @JsonIgnore
    @ApiModelProperty("单据总数|回显")
    @TableField(exist = false)
    private int orderNum;
    @TableField(exist = false)
    @ApiModelProperty("单据总金额|回显")
    @JsonIgnore
    private BigDecimal orderAmount;
    @JsonIgnore
    @TableField(exist = false)
    @ApiModelProperty("出运数量|回显")
    private int transNum;
    @JsonIgnore
    @TableField(exist = false)
    @ApiModelProperty("出运总金额|回显")
    private BigDecimal transAmount;
}
