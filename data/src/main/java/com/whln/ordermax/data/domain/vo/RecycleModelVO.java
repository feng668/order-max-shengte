package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-25 18:19
 */
@AllArgsConstructor
@ApiModel("模块")
@Data
public class RecycleModelVO {
    @ApiModelProperty("code")
    private String code;

    @ApiModelProperty("名字")
    private String name;
}
