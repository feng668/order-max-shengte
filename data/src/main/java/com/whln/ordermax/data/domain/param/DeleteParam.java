package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

/**
 * @author liurun
 * @date 2023-03-21 14:03
 */
@ApiModel("删除使用的参数")
@Data
public class DeleteParam {
    @ApiModelProperty("要删除id集合")
    private Set<String> ids;
    @ApiModelProperty("要删除的id")
    private String id;
}
