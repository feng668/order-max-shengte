package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.FilterBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 供应商
* @TableName sup_supplier
*/
@ApiModel("供应商")
@TableName("sup_supplier")
@Data
public class SupSupplier extends FilterBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 供应商编号
    */
    @ApiModelProperty("供应商编号")
    @NotBlank(message = "编号不能为空")
    private String supplierNo;


    @ApiModelProperty("内销、外销")
    private String typeName;
    @ApiModelProperty("内销1、外销2")
    private Integer type;

    /**
    * 创建日期
    */
    @ApiModelProperty("创建日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
    * 业务类别
    */
    @ApiModelProperty("业务类别")
    private String businessCategory;
    /**
    * 状态
    */
    @ApiModelProperty("状态")
    private Byte state;
    /**
    * 简称
    */
    @ApiModelProperty("简称")
    private String referred;
    /**
    * 供应商全称
    */
    @ApiModelProperty("供应商全称")
    private String fullName;
    /**
    * 供应商英文名
    */
    @ApiModelProperty("供应商英文名")
    private String enName;
    /**
    * 电话
    */
    @ApiModelProperty("电话")
    private String phoneNum;
    /**
    * 网页
    */
    @ApiModelProperty("网页")
    private String website;
    /**
    * 传真
    */
    @ApiModelProperty("传真")
    private String fax;
    /**
    * 邮编
    */
    @ApiModelProperty("邮编")
    private String zipCode;
    /**
    * 地址
    */
    @ApiModelProperty("地址")
    private String address;
    /**
    * 税号
    */
    @ApiModelProperty("税号")
    private String taxNum;
    /**
    * 国家
    */
    @ApiModelProperty("国家")
    private String country;
    /**
    * 城市
    */
    @ApiModelProperty("城市")
    private String city;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;

    /**
    * 归类
    */
    @ApiModelProperty("归类")
    private Byte classified;
    /**
    * 等级
    */
    @ApiModelProperty("等级")
    private Byte level;
    /**
    * 来源
    */
    @ApiModelProperty("来源")
    private Byte source;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    /**
     * 开户银行
     */
    @ApiModelProperty("开户银行")
    private String bank;
    /**
     * 银行账号
     */
    @ApiModelProperty("银行账号")
    private String bankAccount;
    /**
     * 供应商类型
     */
    @ApiModelProperty("供应商类型")
    private String supplierType;



    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("联系人|入参")
    private List<SupSupplierContacts> contactsList;
    @TableField(exist = false)
    @ApiModelProperty("删除联系人id|入参")
    private List<String> contactsDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("联系人邮箱|入参")
    private List<SupContactsEmail> emails;
    @TableField(exist = false)
    @ApiModelProperty("删除邮箱id|入参")
    private List<String> emailDelIds;


    @TableField(exist = false)
    private String billNo;

    public void setSupplierNo(String supplierNo) {
        this.billNo = supplierNo;
        this.supplierNo = supplierNo;
    }

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
