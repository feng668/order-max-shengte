package com.whln.ordermax.data.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-02-24 14:26
 */
@ApiModel("生产单检验标准")
@Data
public class PdQualityStandardVO implements Serializable {
    /**
     * id
     */
    @ApiModelProperty("id")
    private String id;
    /**
     * 质检单id
     */
    @ApiModelProperty("质检单id")
    private String qualityId;
    /**
     * 要素（检验项目）
     */
    @ApiModelProperty("要素（检验项目）")
    private String factor;
    /**
     * 标准条件
     */
    @ApiModelProperty("标准条件")
    private String standardCondition;
    @ApiModelProperty("结果值")
    private String resultValue;
    @ApiModelProperty("结果状态")
    private String resultStatus;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;

    private static final long serialVersionUID = 1L;
}
