package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.query.BillQuery;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.TrCustomsDeclarationCommodity;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.TrCustomsDeclarationCommodity
 */
public interface TrCustomsDeclarationCommodityMapper extends BaseMapper<TrCustomsDeclarationCommodity> {

    List<TrCustomsDeclarationCommodity> listByEntity(@Param("entity")TrCustomsDeclarationCommodity entity);

    IPage<TrCustomsDeclarationCommodity> listByEntity(@Param("entity")TrCustomsDeclarationCommodity entity, Page<TrCustomsDeclarationCommodity> page);

    Integer insertBatch(@Param("entitylist")List<TrCustomsDeclarationCommodity> entitylist);

    List<TrCustomsDeclarationCommodity> commodityList(BillQuery param);
}




