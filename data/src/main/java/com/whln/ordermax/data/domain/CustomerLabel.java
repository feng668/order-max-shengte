package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.whln.ordermax.data.domain.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author liurun
 * @date 2023-03-06 14:58
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("customer_label")
public class CustomerLabel extends BaseEntity implements Serializable {
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 标签名
     */
    private String labelName;
    /**
     * 组id
     */
    private String groupId;

    private String type;


}
