package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.CmCommodityBom;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.CmCommodityBomMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author liurun
 */
@Repository
public class CmCommodityBomRepository extends ServiceImpl<CmCommodityBomMapper, CmCommodityBom> {

    public IPage<CmCommodityBom> pageByEntity(CmCommodityBom entity, Page<CmCommodityBom> page) {
        return baseMapper.listByEntity(entity, page);
    }

    public List<CmCommodityBom> listByEntity(CmCommodityBom entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<CmCommodityBom> entityList) {
        boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }

    public boolean saveOrUpdateBatchByMutiId(List<CmCommodityBom> bomList, String commodityId) throws BusinessException {
        List<CmCommodityBom> commodityBomList = super.list(new LambdaQueryWrapper<CmCommodityBom>().eq(CmCommodityBom::getCommodityId, commodityId));
        Map<String, CmCommodityBom> commodityBomMap = commodityBomList.stream()
                .collect(Collectors.toMap(item -> item.getCommodityId() + item.getPartId(), Function.identity()));
        for (CmCommodityBom bom : bomList) {
            if (ObjectUtil.isEmpty(bom)) {
                continue;
            }
            if (ObjectUtil.isEmpty(bom.getPartNum())) {
                throw new BusinessException("配件数量不能为空");
            }
            CmCommodityBom cmCommodityBom = commodityBomMap.get(bom.getCommodityId() + bom.getPartId());
            if (cmCommodityBom != null) {
                super.update(
                        new LambdaUpdateWrapper<CmCommodityBom>()
                                .eq(CmCommodityBom::getPartId, bom.getPartId())
                                .eq(CmCommodityBom::getCommodityId, bom.getCommodityId())
                                .set(CmCommodityBom::getPartNum, bom.getPartNum())
                );
            } else {
                super.save(bom);
            }
        }
        return true;
    }

    public List<CmCommodityBom> listByCommodityIdNum(String commodityId, Integer num) {
        return baseMapper.listByCommodityIdNum(commodityId, num);
    }

    public List<CmCommodityBom> queryBom(BillQuery query) {
        return baseMapper.list(query);
    }
}




