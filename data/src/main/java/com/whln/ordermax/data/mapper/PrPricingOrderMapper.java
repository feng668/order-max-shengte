package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PrPricingOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PrPricingOrder
 */
public interface PrPricingOrderMapper extends BaseMapper<PrPricingOrder> {

    List<PrPricingOrder> listByEntity(@Param("entity")PrPricingOrder entity);

    IPage<PrPricingOrder> listByEntity(@Param("entity")PrPricingOrder entity, Page<PrPricingOrder> page);

    PrPricingOrder getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<PrPricingOrder> entitylist);

}




