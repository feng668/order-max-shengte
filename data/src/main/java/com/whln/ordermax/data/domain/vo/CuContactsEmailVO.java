package com.whln.ordermax.data.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-03-20 12:13
 */
@ApiModel("联系人邮箱返回值")
@Data
public class CuContactsEmailVO {
    /**
     * id
     */
    private String id;
    /**
     * 邮箱
     */
    @ApiModelProperty("邮箱")
    private String emailAddress;
    /**
     * 联系人id
     */
    @ApiModelProperty("联系人id")
    private String contactId;
    /**
     * 是否默认
     */
    @ApiModelProperty("是否默认")
    private Integer isDefault;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @TableField(exist = false)
    private String customerId;
}
