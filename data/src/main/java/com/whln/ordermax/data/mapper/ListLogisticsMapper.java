package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.vo.ListLogisticsStatVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author liurun
 * @date 2023-03-14 15:11
 */
public interface ListLogisticsMapper {
    List<ListLogisticsStatVO> LogisticsStatList(@Param("entity") Map<String, String> entity);
}
