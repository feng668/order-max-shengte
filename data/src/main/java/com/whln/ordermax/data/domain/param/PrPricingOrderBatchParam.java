package com.whln.ordermax.data.domain.param;

import com.whln.ordermax.data.domain.PrPricingOrder;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-04-19 16:53
 */

@Data
public class PrPricingOrderBatchParam {
    List<PrPricingOrder> pricingOrderList;
}
