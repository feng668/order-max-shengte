package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.CuCustomerContacts;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CuCustomerContacts
 */
public interface CuCustomerContactsMapper extends BaseMapper<CuCustomerContacts> {

    List<CuCustomerContacts> listByEntity(@Param("entity")CuCustomerContacts entity);

    IPage<CuCustomerContacts> listByEntity(@Param("entity")CuCustomerContacts entity, Page<CuCustomerContacts> page);

    Integer insertBatch(@Param("entitylist")List<CuCustomerContacts> entitylist);

}




