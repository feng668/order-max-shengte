package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PoContractCommodity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.query.BillQuery;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PoContractCommodity
 */
public interface PoContractCommodityMapper extends BaseMapper<PoContractCommodity> {

    List<PoContractCommodity> listByEntity(@Param("entity")PoContractCommodity entity);

    IPage<PoContractCommodity> listByEntity(@Param("entity")PoContractCommodity entity, Page<PoContractCommodity> page);

    Integer insertBatch(@Param("entitylist")List<PoContractCommodity> entitylist);

    List<PoContractCommodity> commodityList(BillQuery param);
}




