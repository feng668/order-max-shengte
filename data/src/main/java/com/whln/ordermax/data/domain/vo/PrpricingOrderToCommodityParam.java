package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-18 20:42
 */
@ApiModel("核价单生成商品档案参数")
@Data
public class PrpricingOrderToCommodityParam {
    @ApiModelProperty("核价单id")
    private String prpricingOrderId;
}
