package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.CmCommodityStandard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CmCommodityStandard
 */
public interface CmCommodityStandardMapper extends BaseMapper<CmCommodityStandard> {

    List<CmCommodityStandard> listByEntity(@Param("entity")CmCommodityStandard entity);

    IPage<CmCommodityStandard> listByEntity(@Param("entity")CmCommodityStandard entity, Page<CmCommodityStandard> page);

    Integer insertBatch(@Param("entitylist")List<CmCommodityStandard> entitylist);

}




