package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.SoSiInvoiceCommodity;
import com.whln.ordermax.data.mapper.SoSiInvoiceCommodityMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class SoSiInvoiceCommodityRepository extends ServiceImpl<SoSiInvoiceCommodityMapper, SoSiInvoiceCommodity>{

    public IPage<SoSiInvoiceCommodity> pageByEntity(SoSiInvoiceCommodity entity, Page<SoSiInvoiceCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SoSiInvoiceCommodity> listByEntity(SoSiInvoiceCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SoSiInvoiceCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




