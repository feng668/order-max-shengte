package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-02-21 18:53
 */
@Data
@ApiModel("签名查询参数")
public class EmailSignParam {
    @ApiModelProperty("关联id")
    private String relationId;
    @ApiModelProperty("id")
    private String id;

}
