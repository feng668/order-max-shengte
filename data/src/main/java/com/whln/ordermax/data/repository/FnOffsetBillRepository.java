package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.FnOffsetBill;
import com.whln.ordermax.data.mapper.FnOffsetBillMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class FnOffsetBillRepository extends ServiceImpl<FnOffsetBillMapper, FnOffsetBill>{

    public IPage<FnOffsetBill> pageByEntity(FnOffsetBill entity, Page<FnOffsetBill> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<FnOffsetBill> listByEntity(FnOffsetBill entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<FnOffsetBill> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<FnOffsetBill> listBySuperiorId(String superiorId){
        return baseMapper.listBySuperiorId(superiorId);
    }

}




