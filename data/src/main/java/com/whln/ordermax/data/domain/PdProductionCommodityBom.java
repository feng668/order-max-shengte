package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* 商品bom
* @TableName pd_production_commodity_bom
*/
@ApiModel("商品bom")
@TableName("pd_production_commodity_bom")
@Data
public class PdProductionCommodityBom implements Serializable {

    /**
    * 商品id
    */
    @ApiModelProperty("bom的id")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    @ApiModelProperty("任务单id")
    private String productionId;
    /**
    * 配件id
    */
    @ApiModelProperty("配件id")
    private String partId;
    /**
     * 单箱数量
     */
    @ApiModelProperty("单箱数量")
    private BigDecimal singleBoxNum;
    @ApiModelProperty("单箱净重")
    private BigDecimal singleBoxNet;
    /**
     * 单箱毛重
     */
    @ApiModelProperty("单箱毛重")
    private BigDecimal singleBoxWeight;
    /**
     * 单箱体积
     */
    @ApiModelProperty("单箱体积")
    private BigDecimal singleBoxVolume;


    @ApiModelProperty("配件总数量")
    private BigDecimal totalPartNum;
    /**
    * 配件数量
    */
    @ApiModelProperty("配件数量")
    private BigDecimal partNum;
    @TableField(exist = false)
    @ApiModelProperty("商品id")
    private String commodityId;
    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private BigDecimal netWeight;
    @TableField(exist = false)
    @ApiModelProperty("毛重|回显")
    private BigDecimal grossWeight;
    @TableField(exist = false)
    @ApiModelProperty("长度|回显")
    private Double length;
    @TableField(exist = false)
    @ApiModelProperty("宽度|回显")
    private Double width;
    @TableField(exist = false)
    @ApiModelProperty("高度|回显")
    private Double highly;
    @TableField(exist = false)
    @ApiModelProperty("体积|回显")
    private Double volume;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("包装要求|回显")
    private String packagingRemark;
    @TableField(exist = false)
    @ApiModelProperty("备注|回显")
    private String note;
    @TableField(exist = false)
    @ApiModelProperty("默认采购价|回显")
    private Double purchasePrice;
    @TableField(exist = false)
    @ApiModelProperty("批号")
    private String commodityBatchNo;
    @ApiModelProperty("简称")
    @TableField(exist = false)
    private String miniName;

    @TableField(exist = false)
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    private Integer contactTax;

    @TableField(exist = false)
    @NotNull(message = "元素不能为空")
    @ApiModelProperty("元素")
    private String elements;
    @TableField(exist = false)
    @ApiModelProperty("规格配比")
    private String ratio;

    @TableField(exist = false)
    @NotNull(message = "纯度不能为空")
    @ApiModelProperty("纯度")
    private String purity;



    @ApiModelProperty("创建时间|回显")
    @TableField(exist = false)
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    @TableField(exist = false)
    @ApiModelProperty("更新时间|回显")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    @TableField(exist = false)
    @ApiModelProperty("默认供应商id|回显")
    private String defaultSupplierId;
    @TableField(exist = false)
    @ApiModelProperty("报关编码|回显")
    private String ccDeclarationNo;
    @TableField(exist = false)
    @ApiModelProperty("报关中文名|回显")
    private String ccCustomsCnName;
    @TableField(exist = false)
    @ApiModelProperty("报关英文名|回显")
    private String ccCustomsEnName;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;
    @TableField(exist = false)
    @ApiModelProperty("退税率|回显")
    private Double ccRebatesRate;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("默认供应商编号|回显")
    private String defaultSupplierNo;
    @TableField(exist = false)
    @ApiModelProperty("默认供应商全名|回显")
    private String defaultSupplierName;

    @ApiModelProperty("二维码的base64")
    @TableField(exist = false)
    private String QRCode;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
