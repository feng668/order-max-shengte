package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.CuBigCustomerNorm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CuBigCustomerNorm
 */
public interface CuBigCustomerNormMapper extends BaseMapper<CuBigCustomerNorm> {

    List<CuBigCustomerNorm> listByEntity(@Param("entity")CuBigCustomerNorm entity);

    IPage<CuBigCustomerNorm> listByEntity(@Param("entity")CuBigCustomerNorm entity, Page<CuBigCustomerNorm> page);

    CuBigCustomerNorm getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<CuBigCustomerNorm> entitylist);

}




