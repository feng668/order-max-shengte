package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.FnInAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.FnInAccount
 */
public interface FnInAccountMapper extends BaseMapper<FnInAccount> {

    List<FnInAccount> listByEntity(@Param("entity")FnInAccount entity);

    IPage<FnInAccount> listByEntity(@Param("entity")FnInAccount entity, Page<FnInAccount> page);

    Integer insertBatch(@Param("entitylist")List<FnInAccount> entitylist);

}




