package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.OaRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.OaRecord
 */
public interface OaRecordMapper extends BaseMapper<OaRecord> {

    List<OaRecord> listByEntity(@Param("entity")OaRecord entity);

    IPage<OaRecord> listByEntity(@Param("entity")OaRecord entity, Page<OaRecord> page);

    Integer insertBatch(@Param("entitylist")List<OaRecord> entitylist);

}




