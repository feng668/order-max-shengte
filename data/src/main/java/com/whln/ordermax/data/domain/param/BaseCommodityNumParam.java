package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author liurun
 * @date 2023-03-14 11:30
 */
@ApiModel("选择仓库参数")
@Data
public class BaseCommodityNumParam {
    @NotBlank(message = "商品id不能为空")
    @ApiModelProperty(value = "商品id")
    String commodityId;
    @ApiModelProperty("模糊查询参数")
    private String searchInfo;
    @ApiModelProperty(value = "当前页码",example = "1")
    Long current=1L;
    @ApiModelProperty(value = "每页大小",example = "1")
    Long size=10L;
}
