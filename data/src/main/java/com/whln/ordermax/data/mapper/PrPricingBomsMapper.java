package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PrPricingBoms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.PrPricingCommodity;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PrPricingBoms
 */
public interface PrPricingBomsMapper extends BaseMapper<PrPricingBoms> {

    List<PrPricingBoms> listByEntity(@Param("entity")PrPricingBoms entity);

    IPage<PrPricingBoms> listByEntity(@Param("entity")PrPricingBoms entity, Page<PrPricingBoms> page);

    PrPricingBoms getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<PrPricingBoms> entitylist);

    List<PrPricingBoms> listBomsByCommodity(@Param("entitylist")List<PrPricingCommodity> entityList);
}




