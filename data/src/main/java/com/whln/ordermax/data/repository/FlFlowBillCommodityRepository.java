package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.FlFlowBillCommodity;
import com.whln.ordermax.data.mapper.FlFlowBillCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class FlFlowBillCommodityRepository extends ServiceImpl<FlFlowBillCommodityMapper, FlFlowBillCommodity>{

    public IPage<FlFlowBillCommodity> pageByEntity(FlFlowBillCommodity entity, Page<FlFlowBillCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<FlFlowBillCommodity> listByEntity(FlFlowBillCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public FlFlowBillCommodity getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<FlFlowBillCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    @Override
    public boolean saveOrUpdate(FlFlowBillCommodity entity) {
        FlFlowBillCommodity exsits = this.getOne(
                new QueryWrapper<FlFlowBillCommodity>()
                        .eq("bill_commodity_data_id", entity.getBillCommodityDataId())
                        .eq("bill_code", entity.getBillCode())
                        .eq("flow_bill_code", entity.getFlowBillCode())
        );
        if (exsits==null){
            return save(entity);
        }else {
            return update(
                    entity,
                    new UpdateWrapper<FlFlowBillCommodity>()
                            .eq("bill_commodity_data_id", entity.getBillCommodityDataId())
                            .eq("bill_code", entity.getBillCode())
                            .eq("flow_bill_code", entity.getFlowBillCode())
            );
        }
    }
}




