package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PhyForwarderCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PhyForwarderCompany
 */
public interface PhyForwarderCompanyMapper extends BaseMapper<PhyForwarderCompany> {

    List<PhyForwarderCompany> listByEntity(@Param("entity")PhyForwarderCompany entity);

    IPage<PhyForwarderCompany> listByEntity(@Param("entity")PhyForwarderCompany entity, Page<PhyForwarderCompany> page);

    Integer insertBatch(@Param("entitylist")List<PhyForwarderCompany> entitylist);

}




