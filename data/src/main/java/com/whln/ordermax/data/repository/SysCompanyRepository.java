package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.SysCompany;
import com.whln.ordermax.data.mapper.SysCompanyMapper;

import java.util.List;

/**
 *
 */
@Repository
public class SysCompanyRepository extends ServiceImpl<SysCompanyMapper, SysCompany>{

    public IPage<SysCompany> pageByEntity(SysCompany entity, Page<SysCompany> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SysCompany> listByEntity(SysCompany entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SysCompany> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




