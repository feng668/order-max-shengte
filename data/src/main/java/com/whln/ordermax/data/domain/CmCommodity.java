package com.whln.ordermax.data.domain;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.common.exception.BusinessException;
import com.whln.ordermax.data.domain.base.FilterBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 商品
 *
 * @author zz
 * @TableName cm_commodity
 */
@EqualsAndHashCode(callSuper = true)
@Builder
@ApiModel("商品")
@TableName("cm_commodity")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CmCommodity extends FilterBill implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 分类id
     */
    @ApiModelProperty("分类id")
    private String categoryId;
    /**
     * 商品编号
     */
    @ApiModelProperty("商品编号")
    @NotBlank(message = "编号不能为空")
    private String partNo;
    /**
     * 商品中文名称
     */
    @ApiModelProperty("商品中文名称")
    private String cnName;
    @ApiModelProperty("规格配比")
    private String ratio;


    @ApiModelProperty("简称")
    private String miniName;

    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    private Integer contactTax;

    @NotNull(message = "元素不能为空")
    @ApiModelProperty("元素")
    private String elements;


    @NotNull(message = "纯度不能为空")
    @ApiModelProperty("纯度")
    private String purity;

    /**
     * 商品英文名
     */
    @ApiModelProperty("商品英文名")
    private String enName;
    /**
     * 报关代码id
     */
    @ApiModelProperty("报关代码id")
    private String customsCodeId;
    /**
     * 商品规格
     */
    @ApiModelProperty("商品规格")
    private String specification;
    /**
     * 商品英文规格
     */
    @ApiModelProperty("英文规格")
    private String enSpecification;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    /**
     * 净重
     */
    @ApiModelProperty("净重")
    private BigDecimal netWeight;
    /**
     * 毛重
     */
    @ApiModelProperty("毛重")
    private BigDecimal grossWeight;
    /**
     * 长度
     */
    @ApiModelProperty("长度")
    private BigDecimal length;
    /**
     * 宽度
     */
    @ApiModelProperty("宽度")
    private BigDecimal width;
    /**
     * 高度
     */
    @ApiModelProperty("高度")
    private BigDecimal highly;

    @ApiModelProperty("是否删除  1  已删除 0 正常状态")
    private Integer isDelete;
    /**
     * 体积
     */
    @ApiModelProperty("单箱体积")
    private BigDecimal singleBoxVolume;
    @ApiModelProperty("单箱净重")
    private BigDecimal singleBoxNetWeight;

    /**
     * 计量单位
     */
    @ApiModelProperty("计量单位")
    private String quantityUnit;
    /**
     * 包装要求
     */
    @ApiModelProperty("包装要求")
    private String packagingRemark;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 默认采购价
     */
    @ApiModelProperty("默认采购价")
    private BigDecimal purchasePrice;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 默认供应商id
     */
    @ApiModelProperty("默认供应商id")
    private String defaultSupplierId;
    /**
     * 销售价
     */
    @ApiModelProperty("销售价")
    private BigDecimal salePrice;
    /**
     * 物件类别(商品=COMMODITY，配件=PART)
     */
    @ApiModelProperty("物件类别(商品=COMMODITY，配件=PART)")
    private String articleType;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("保存的图片路径数组|入参")
    private List<CmCommodityImg> imgList;
    @TableField(exist = false)
    @ApiModelProperty("删除的图片路径id数组|入参")
    private List<String> imgDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("删除的图片路径数组|入参")
    private List<String> imgDelUriList;
    @TableField(exist = false)
    @ApiModelProperty("保存的bom数组|入参")
    private List<CmCommodityBom> bomList;
    @TableField(exist = false)
    @ApiModelProperty("删除的商品 配件id数组|入参")
    private List<String> partDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("保存的检验标准数组|入参")
    private List<CmCommodityStandard> standardList;
    @TableField(exist = false)
    @ApiModelProperty("删除的检验标准id数组|入参")
    private List<String> standardDelIdList;
    @TableField(exist = false)
    private List<String> categoryIdIn;
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("分类名称")
    private String categoryName;

    @TableField(exist = false)
    @ApiModelProperty("报关编码|回显")
    private String ccDeclarationNo;
    @TableField(exist = false)
    @ApiModelProperty("报关中文名|回显")
    private String ccCustomsCnName;
    @TableField(exist = false)
    @ApiModelProperty("报关英文名|回显")
    private String ccCustomsEnName;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private BigDecimal ccVatRate;
    @TableField(exist = false)
    @ApiModelProperty("退税率|回显")
    private BigDecimal ccRebatesRate;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("默认供应商编号|回显")
    private String defaultSupplierNo;
    @TableField(exist = false)
    @ApiModelProperty("默认供应商全名|回显")
    private String defaultSupplierName;
    @TableField(exist = false)
    @ApiModelProperty("客户最后一次报价|数据暂存")
    private BigDecimal lastQoPrice;
    @TableField(exist = false)
    @ApiModelProperty("客户id|报价单专用")
    private String customerId;
    @TableField(exist = false)
    private BigDecimal amount;
    @TableField(exist = false)
    private BigDecimal num;
    @TableField(exist = false)
    private String commodityId;
    @ApiModelProperty("客户id|报价单专用")
    @TableField(exist = false)
    private String billNo;

    public void setPartNo(String partNo) {
        this.partNo = partNo;
        this.billNo = partNo;
    }
    public static CmCommodity prPricingOrderToCommodity(PrPricingOrder prPricingOrder) {
        if (ObjectUtil.isEmpty(prPricingOrder.getCommodityNo())){
            throw new BusinessException("商品信息不完整");
        }
        return CmCommodity.builder()
                .partNo(prPricingOrder.getCommodityNo())
                .enName(prPricingOrder.getEnName())
                .cnName(prPricingOrder.getCnName())
                .specification(prPricingOrder.getSpecification())
                .enSpecification(prPricingOrder.getEnSpecification())
                .quantityUnit(prPricingOrder.getQuantityUnit())
                .createDate(LocalDate.now())
                .ownerId(prPricingOrder.getOwnerId())
                .ownerName(prPricingOrder.getOwnerName())
                .build();

    }
}
