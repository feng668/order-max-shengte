package com.whln.ordermax.data.domain.param;

import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-25 19:36
 */

@Data
public class RecycleDeleteParam {
    private String code;
    private String id;
}
