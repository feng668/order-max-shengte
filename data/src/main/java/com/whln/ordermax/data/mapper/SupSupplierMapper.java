package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.SupSupplier;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SupSupplier
 */
public interface SupSupplierMapper extends BaseMapper<SupSupplier> {

    List<SupSupplier> listByEntity(@Param("entity")SupSupplier entity);

    IPage<SupSupplier> listByEntity(@Param("entity")SupSupplier entity, Page<SupSupplier> page);

    Integer insertBatch(@Param("entitylist")List<SupSupplier> entitylist);

}




