package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CustomerLabel;
import com.whln.ordermax.data.mapper.CustomerLabelMapper;
import org.springframework.stereotype.Repository;

/**
 * @author liurun
 * @date 2023-03-06 15:43
 */
@Repository
public class CustomerLabelRepository extends ServiceImpl<CustomerLabelMapper, CustomerLabel> {
}
