package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.TrCustomsDeclaration;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.TrCustomsDeclaration
 */
public interface TrCustomsDeclarationMapper extends BaseMapper<TrCustomsDeclaration> {

    List<TrCustomsDeclaration> listByEntity(@Param("entity")TrCustomsDeclaration entity);

    IPage<TrCustomsDeclaration> listByEntity(@Param("entity")TrCustomsDeclaration entity, Page<TrCustomsDeclaration> page);

    Integer insertBatch(@Param("entitylist")List<TrCustomsDeclaration> entitylist);

    TrCustomsDeclaration getAllById(String id);
}




