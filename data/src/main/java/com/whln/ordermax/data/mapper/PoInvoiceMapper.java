package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PoInvoice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PoInvoice
 */
public interface PoInvoiceMapper extends BaseMapper<PoInvoice> {

    List<PoInvoice> listByEntity(@Param("entity")PoInvoice entity);

    IPage<PoInvoice> listByEntity(@Param("entity")PoInvoice entity, Page<PoInvoice> page);

    Integer insertBatch(@Param("entitylist")List<PoInvoice> entitylist);

    PoInvoice getAllById(@Param("id") String id);
}




