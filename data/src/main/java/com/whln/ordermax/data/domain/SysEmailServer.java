package com.whln.ordermax.data.domain;

import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
* 邮箱服务器
* @TableName sys_email_server
*/
@ApiModel("邮箱服务器")
@TableName("sys_email_server")
@Data
public class SysEmailServer {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * 主键
    */
    private String id;
    /**
    * 邮箱域名
    */
    @ApiModelProperty("邮箱域名")
    private String emailDomain;
    /**
    * 接收服务器
    */
    @ApiModelProperty("接收服务器")
    private String receiveAddress;
    /**
    * 发送服务器
    */
    @ApiModelProperty("发送服务器")
    private String sendAddress;
    /**
    * 
    */
    @ApiModelProperty("")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 
    */
    @ApiModelProperty("")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
    * 
    */
    @ApiModelProperty("")
    private String ownerId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
