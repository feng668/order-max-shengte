package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CustomerLabelGroup;
import com.whln.ordermax.data.mapper.UserLabelGroupMapper;
import org.springframework.stereotype.Repository;

/**
 * @author liurun
 * @date 2023-03-06 15:44
 */
@Repository
public class CustomerLabelGroupRepository extends ServiceImpl<UserLabelGroupMapper, CustomerLabelGroup> {
}
