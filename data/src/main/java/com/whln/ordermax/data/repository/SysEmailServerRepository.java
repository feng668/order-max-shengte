package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.SysEmailServer;
import com.whln.ordermax.data.mapper.SysEmailServerMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class SysEmailServerRepository extends ServiceImpl<SysEmailServerMapper, SysEmailServer>{

    public IPage<SysEmailServer> pageByEntity(SysEmailServer entity, Page<SysEmailServer> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SysEmailServer> listByEntity(SysEmailServer entity) {
        return baseMapper.listByEntity(entity);
    }

    public SysEmailServer getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<SysEmailServer> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




