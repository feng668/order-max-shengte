package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.EmSubmitted;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EmSubmitted
 */
public interface EmSubmittedMapper extends BaseMapper<EmSubmitted> {

    List<EmSubmitted> listByEntity(@Param("entity")EmSubmitted entity);

    IPage<EmSubmitted> listByEntity(@Param("entity")EmSubmitted entity, Page<EmSubmitted> page);

    Integer insertBatch(@Param("entitylist")List<EmSubmitted> entitylist);

}




