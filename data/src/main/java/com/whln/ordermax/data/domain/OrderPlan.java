package com.whln.ordermax.data.domain;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 订单进度
 *
 * @TableName order_plan
 */
@ApiModel("订单进度")
@TableName("order_plan")
@Data
public class OrderPlan implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 单据id
     */
    @ApiModelProperty("单据id")
    private String billId;
    /**
     * 单据编号
     */
    @ApiModelProperty("单据编号")
    private String billNo;
    /**
     * 模块代码
     */
    @ApiModelProperty("模块代码")
    private String billCode;
    /**
     * 源单单号
     */
    @ApiModelProperty("源单单号")
    private String rootBillId;
    /**
     * 订单状态
     */
    @ApiModelProperty("订单状态")
    private Integer billStatus;
    private Integer temId;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    /*附加属性*/
    @TableField(exist = false)
    private List<OrderPlan> chirds;
    @ApiModelProperty("上一个单据id|入参")
    private String lastBillId;
    @ApiModelProperty("上一个模块代码|入参")
    private String lastBillCode;
    @TableField(exist = false)
    private String billName;
    @TableField(exist = false)
    private List<OrderPlan> orders;



    public void addChild(List<OrderPlan> orderPlan) {
        chirds = orderPlan;
    }
    public void addChild(OrderPlan orderPlan) {
        if (ObjectUtil.isEmpty(chirds)) {
            chirds = new ArrayList<>();
        }
        chirds.add(orderPlan);
    }
}
