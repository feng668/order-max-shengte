package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.param.TransactionHistoryParam;
import com.whln.ordermax.data.domain.vo.CommodityTransactionHistoryVO;
import com.whln.ordermax.data.domain.vo.ListLogisticsStatVO;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.mapper.DynamicMapper;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Repository
public class DynamicRepository extends ServiceImpl<DynamicMapper, Object> {

    public List<Map> listAll(String tableName) {
        return baseMapper.listAll(tableName);
    }

    public String getMaxNo(String billNoField, String tableName, String matchInfo) {
        return baseMapper.getMaxNo(billNoField, tableName, matchInfo);
    }

    public Boolean updateBillOaStatus(String tableName,String id,String oaLevelOpera,Integer oaStatus){
        return baseMapper.updateBillOaStatus(tableName,id,oaLevelOpera, oaStatus);
    }

    public List<Map<String,Object>> listEventByDateRange(String startDate,String endDate){
        return baseMapper.listEventByDateRange(startDate,endDate);
    }

    public Page<CommodityTransactionHistoryVO > listDealByCommodityId(TransactionHistoryParam param){
        return baseMapper.listDealByCommodityId(param, new Page<>(param.getCurrent(), param.getSize()));
    }

    public List<Map<String,Object>> listFollowByCustomerId(String id){
        return baseMapper.listFollowByCustomerId(id);
    }

    public Boolean updateBillFlowStatus(String billId, String tableName,Integer flowNo) {
        return baseMapper.updateBillFlowStatus(billId,tableName,flowNo);
    }

    public Boolean batchUpdateBillComFlowStatus(List<String> idList, String tableName,Integer flowNo) {
        return baseMapper.batchUpdateBillComFlowStatus(idList,tableName,flowNo);
    }

    public Boolean updateBillFlowStatusByComFlow(String sourceBillId, String tableName, String comTableName, Integer flowNo) {
        return baseMapper.updateBillFlowStatusByComFlow(sourceBillId,tableName,comTableName,flowNo);
    }

    public String getBorderIdByBillId(String id, String tableName, String borderType) {
        return baseMapper.getBorderIdByBillId(id,tableName,borderType);
    }

    public List<ListLogisticsStatVO> listLogisticsStat(Map<String,String> entity) {
        return baseMapper.listLogisticsStat(entity);
    }
}




