package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.CuContactsEmail;
import com.whln.ordermax.data.mapper.CuContactsEmailMapper;

import java.util.List;

/**
 *
 */
@Repository
public class CuContactsEmailRepository extends ServiceImpl<CuContactsEmailMapper, CuContactsEmail>{

    public IPage<CuContactsEmail> pageByEntity(CuContactsEmail entity, Page<CuContactsEmail> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<CuContactsEmail> listByEntity(CuContactsEmail entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<CuContactsEmail> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<CuContactsEmail> listWithCusId() {
        return baseMapper.listWithCusId();
    }

}




