package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.whln.ordermax.data.domain.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author liurun
 * @since 2023-04-23
 */
@Getter
@Setter
@TableName("invoice")
@ApiModel("")
public class Invoice extends BaseEntity {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "发票类型")
    @TableField("type")
    private Integer type;

    @ApiModelProperty(value = "发票编号")
    @TableField("bill_no")
    private String billNo;

    @ApiModelProperty(value = "拥有人")
    @TableField("owner_id")
    private String ownerId;
    @ApiModelProperty(value = "是否开票")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "开票总金额")
    @TableField("total_amount")
    private BigDecimal totalAmount;

    @ApiModelProperty(value = "币种")
    @TableField("currency_name")
    private String currencyName;
    private Integer isDelete;

    @ApiModelProperty(value = "付款方式")
    @TableField("pay_method")
    private String payMethod;

    @ApiModelProperty(value = "公司id")
    @TableField("company_id")
    private String companyId;

    @ApiModelProperty(value = "客户id")
    @TableField("customer_id")
    private String customerId;

    @TableField("invoice_date")
    private LocalDateTime invoiceDate;

    @ApiModelProperty(value = "备注")
    @TableField("note")
    private String note;
    @TableField(exist = false)
    private String billCode;
    @TableField(exist = false)
    private List<String> billList;
    @TableField(exist = false)
    private List<Object> commodityList;
}
