package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 质检单
 *
 * @TableName pd_quality_standard
 */
@ApiModel("质检单标准条件")
@TableName("pd_quality_standard")
@Data
public class PdQualityStandard implements Serializable {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 质检单id
     */
    @ApiModelProperty("质检单id")
    private String qualityId;
    /**
     * 要素（检验项目）
     */
    @ApiModelProperty("要素（检验项目）")
    private String factor;
    /**
     * 标准条件
     */
    @ApiModelProperty("标准条件")
    private String standardCondition;
    @ApiModelProperty("结果值")
    private String resultValue;
    @ApiModelProperty("结果状态")
    private String resultStatus;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
