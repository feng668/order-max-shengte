package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.OaRuleLevelAssessor;
import com.whln.ordermax.data.mapper.OaRuleLevelAssessorMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 *
 * @author liurun
 */
@Repository
public class OaRuleLevelAssessorRepository extends ServiceImpl<OaRuleLevelAssessorMapper, OaRuleLevelAssessor> {

    public IPage<OaRuleLevelAssessor> pageByEntity(OaRuleLevelAssessor entity, Page<OaRuleLevelAssessor> page) {
        return baseMapper.listByEntity(entity, page);
    }

    public List<OaRuleLevelAssessor> listByEntity(OaRuleLevelAssessor entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<OaRuleLevelAssessor> entityList) {
        Boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }

}




