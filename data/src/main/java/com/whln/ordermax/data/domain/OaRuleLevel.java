package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
* oa规则层级
* @TableName oa_rule_level
*/
@ApiModel("oa规则层级")
@TableName("oa_rule_level")
@Data
public class OaRuleLevel implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 规则id
    */
    @ApiModelProperty("规则id")
    private String ruleId;
    /**
    * 审批层级
    */
    @ApiModelProperty("审批层级")
    private Integer oaLevelNum;
    /**
    * 审批层级名称
    */
    @ApiModelProperty("审批层级名称")
    private String oaLevelName;
    @ApiModelProperty("通过方式|通过方式（会签ALL,或签ONE）")
    private String passType;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;


    @ApiModelProperty("是否启用")
    @TableField(exist = false)
    private Integer isActive;

    @TableField(exist = false)
    @ApiModelProperty("审批者们")
    private List<OaRuleLevelAssessor> assessorList;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
