package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.SoSoOrderCommodity;
import com.whln.ordermax.data.mapper.SoSoOrderCommodityMapper;

import java.util.List;

/**
 *
 */
@Repository
public class SoSoOrderCommodityRepository extends ServiceImpl<SoSoOrderCommodityMapper, SoSoOrderCommodity>{

    public IPage<SoSoOrderCommodity> pageByEntity(SoSoOrderCommodity entity, Page<SoSoOrderCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SoSoOrderCommodity> listByEntity(SoSoOrderCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SoSoOrderCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




