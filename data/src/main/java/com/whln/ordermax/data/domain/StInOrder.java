package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.OaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 入库单
* @TableName st_in_order
*/
@ApiModel("入库单")
@TableName("st_in_order")
@Data
public class StInOrder extends OaBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    private Integer status;

    private String parentBillId;
    
    private String parentBillCode;
    
    private String parentBillNo;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;
    @ApiModelProperty(value = "红蓝字 1(采购入库)，-1（采购退货）")
    private Integer frob;

    @ApiModelProperty(value = "入库状态，0进行中 1 已完成")
    private Integer putStatus;
    /**
    * 出库单编号
    */
    @ApiModelProperty("出库单编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;
    /**
    * 供应商id
    */
    @ApiModelProperty("供应商id")
    private String supplierId;
    /**
    * 入库日期
    */
    @ApiModelProperty("入库日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate proceedDate;
    /**
    * 入库人员id
    */
    @ApiModelProperty("入库人员id")
    private String storagerId;
    /**
    * 入库部门id
    */
    @ApiModelProperty("入库部门id")
    private String departmentId;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    /**
     * 仓库id
     */
    @ApiModelProperty("仓库id")
    private String warehouseId;
    /**
     * 入库类型（采购入库|其他入库）
     */
    @ApiModelProperty("入库类型 PUR采购入库 DEFAULT 生产入库")
    private String putType;
    //总数量
    private BigDecimal quotationNum;


    private String sourceBillId;
    @ApiModelProperty("单据来源")
    private String source;
    @ApiModelProperty("源单编号")
    private String sourceBillNo;
    //总金额业务员
    @ApiModelProperty("单据总金额")
    private BigDecimal totalAmount;
    @ApiModelProperty("原单类型")
    private String originBillType;
    @ApiModelProperty("原单单号")
    private String originBillNo;

    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("保存的入库单商品|入参")
    private List<StInOrderCommodity> commodityList;
    @TableField(exist = false)
    @ApiModelProperty("删除的入库单商品id数组|入参")
    private List<String> commodityDelIdList;

    @TableField(exist = false)
    @ApiModelProperty("供应商名称|回显")
    private String supName;
    @TableField(exist = false)
    @ApiModelProperty("入库人员姓名|回显")
    private String storagerName;
    @TableField(exist = false)
    @ApiModelProperty("部门名称|回显")
    private String dptName;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("仓库编号|回显")
    private String warehouseNo;
    @TableField(exist = false)
    @ApiModelProperty("仓库名称|回显")
    private String warehouseName;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
