package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.StWarehouse;
import com.whln.ordermax.data.mapper.StWarehouseMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class StWarehouseRepository extends ServiceImpl<StWarehouseMapper, StWarehouse>{

    public IPage<StWarehouse> pageByEntity(StWarehouse entity, Page<StWarehouse> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<StWarehouse> listByEntity(StWarehouse entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<StWarehouse> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




