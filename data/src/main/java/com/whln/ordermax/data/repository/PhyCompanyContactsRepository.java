package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PhyCompanyContacts;
import com.whln.ordermax.data.mapper.PhyCompanyContactsMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class PhyCompanyContactsRepository extends ServiceImpl<PhyCompanyContactsMapper, PhyCompanyContacts>{

    public IPage<PhyCompanyContacts> pageByEntity(PhyCompanyContacts entity, Page<PhyCompanyContacts> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PhyCompanyContacts> listByEntity(PhyCompanyContacts entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PhyCompanyContacts> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




