package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whln.ordermax.data.domain.CustomerLabelGroup;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liurun
 * @date 2023-03-06 15:41
 */
@Mapper
public interface UserLabelGroupMapper extends BaseMapper<CustomerLabelGroup> {
}
