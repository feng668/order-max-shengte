package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
* 编号规则单据
* @TableName nr_bill
*/
@ApiModel("编号规则单据")
@TableName("nr_bill")
@Data
public class NrBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 单据类别
    */
    @ApiModelProperty("单据类别")
    private String billCategory;
    /**
    * 表名
    */
    @ApiModelProperty("表名")
    private String tableName;
    /**
    * 编号字段
    */
    @ApiModelProperty("编号字段")
    private String billNoField;
    @ApiModelProperty("单据模块")
    private String billCode;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;



    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("保存单据编号规则|入参")
    private List<NrRoleNode> roleNodeList;
    @TableField(exist = false)
    @ApiModelProperty("删除的单据编号规则id数组|入参")
    private List<String> nrRoleDelIdList;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
