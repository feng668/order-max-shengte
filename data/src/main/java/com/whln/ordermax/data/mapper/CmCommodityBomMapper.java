package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.CmCommodityBom;
import com.whln.ordermax.data.domain.query.BillQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CmCommodityBom
 */
public interface CmCommodityBomMapper extends BaseMapper<CmCommodityBom> {

    List<CmCommodityBom> listByEntity(@Param("entity")CmCommodityBom entity);

    IPage<CmCommodityBom> listByEntity(@Param("entity")CmCommodityBom entity, Page<CmCommodityBom> page);

    Integer insertBatch(@Param("entitylist")List<CmCommodityBom> entitylist);

    List<CmCommodityBom> listByCommodityIdNum(@Param("commodityId")String commodityId,@Param("num") Integer num);

    List<CmCommodityBom> list(BillQuery param);

    List<CmCommodityBom> queryBom(BillQuery query);
}




