package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-02 9:35
 */
@ApiModel("oa审核列表返回值")
@Builder
@Data
public class OaRuleListResult implements Serializable {
    private static final long serialVersionUID = 1701234760424420481L;
    List<OaRuleVO> oaRuleVOList;
    @ApiModelProperty("总数")
    private Integer total;
}
