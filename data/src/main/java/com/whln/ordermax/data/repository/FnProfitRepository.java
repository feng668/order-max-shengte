package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.FnProfit;
import com.whln.ordermax.data.mapper.FnProfitMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class FnProfitRepository extends ServiceImpl<FnProfitMapper, FnProfit>{

    public IPage<FnProfit> pageByEntity(FnProfit entity, Page<FnProfit> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<FnProfit> listByEntity(FnProfit entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<FnProfit> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




