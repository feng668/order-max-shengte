package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.TrTransportationCost;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.TrTransportationCost
 */
public interface TrTransportationCostMapper extends BaseMapper<TrTransportationCost> {

    List<TrTransportationCost> listByEntity(@Param("entity")TrTransportationCost entity);

    IPage<TrTransportationCost> listByEntity(@Param("entity")TrTransportationCost entity, Page<TrTransportationCost> page);

    Integer insertBatch(@Param("entitylist")List<TrTransportationCost> entitylist);

}




