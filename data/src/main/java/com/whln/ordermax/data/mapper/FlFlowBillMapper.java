package com.whln.ordermax.data.mapper;

import cn.hutool.json.JSONObject;
import com.whln.ordermax.data.domain.FlFlowBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Map;

/**
 * @Entity whln.ordermax.data.domain.FlFlowBill
 */
public interface FlFlowBillMapper extends BaseMapper<FlFlowBill> {

    List<FlFlowBill> listByEntity(@Param("entity")FlFlowBill entity);

    IPage<FlFlowBill> listByEntity(@Param("entity")FlFlowBill entity, Page<FlFlowBill> page);

    FlFlowBill getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<FlFlowBill> entitylist);

    List<Map<String,Object>> listBillCommodityWithRemainNum(@Param("entity")FlFlowBill entity,@Param("tableBody") String tableBody);

    List<Map<String, Object>> listNotFlowBill(@Param("entity") JSONObject entity, @Param("tableBody") String tableBody);
}




