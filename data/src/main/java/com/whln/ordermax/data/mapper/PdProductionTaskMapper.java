package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PdProductionTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PdProductionTask
 */
public interface PdProductionTaskMapper extends BaseMapper<PdProductionTask> {

    List<PdProductionTask> listByEntity(@Param("entity")PdProductionTask entity);

    IPage<PdProductionTask> listByEntity(@Param("entity")PdProductionTask entity, Page<PdProductionTask> page);

    Integer insertBatch(@Param("entitylist")List<PdProductionTask> entitylist);

    PdProductionTask getAllById(String id);
}




