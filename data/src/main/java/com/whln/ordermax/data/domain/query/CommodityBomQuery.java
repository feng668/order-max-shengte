package com.whln.ordermax.data.domain.query;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;

/**
 * @author liurun
 * @date 2023-03-03 9:45
 */
@Data
@Builder
public class CommodityBomQuery {
    /**
     * 商品id
     */
    private Collection<String> commodityIds;
}
