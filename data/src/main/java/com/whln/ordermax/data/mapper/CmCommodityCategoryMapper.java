package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.CmCommodityCategory;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CmCommodityCategory
 */
public interface CmCommodityCategoryMapper extends BaseMapper<CmCommodityCategory> {

    List<CmCommodityCategory> listByEntity(@Param("entity")CmCommodityCategory entity);

    IPage<CmCommodityCategory> listByEntity(@Param("entity")CmCommodityCategory entity, Page<CmCommodityCategory> page);

    Integer insertBatch(@Param("entitylist")List<CmCommodityCategory> entitylist);

}




