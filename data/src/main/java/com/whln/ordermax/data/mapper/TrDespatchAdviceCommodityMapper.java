package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.TrDespatchAdviceCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.TrDespatchAdviceCommodity
 */
public interface TrDespatchAdviceCommodityMapper extends BaseMapper<TrDespatchAdviceCommodity> {

    List<TrDespatchAdviceCommodity> listByEntity(@Param("entity")TrDespatchAdviceCommodity entity);

    IPage<TrDespatchAdviceCommodity> listByEntity(@Param("entity")TrDespatchAdviceCommodity entity, Page<TrDespatchAdviceCommodity> page);

    Integer insertBatch(@Param("entitylist")List<TrDespatchAdviceCommodity> entitylist);

    List<TrDespatchAdviceCommodity> commodityList(BillQuery param);
}




