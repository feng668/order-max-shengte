package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PdQuality;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PdQuality
 */
public interface PdQualityMapper extends BaseMapper<PdQuality> {

    List<PdQuality> listByEntity(@Param("entity")PdQuality entity);

    IPage<PdQuality> listByEntity(@Param("entity")PdQuality entity, Page<PdQuality> page);

    Integer insertBatch(@Param("entitylist")List<PdQuality> entitylist);

    PdQuality get(@Param("id") String id);
}




