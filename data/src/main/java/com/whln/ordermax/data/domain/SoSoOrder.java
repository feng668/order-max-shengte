package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.whln.ordermax.data.domain.base.ComFilterOaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 销售合同
 *
 * @TableName so_so_order
 */
@ApiModel("外销销合同")
@TableName("so_so_order")
@Data
public class SoSoOrder extends ComFilterOaBill implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    @ApiModelProperty("是否开发票， 0未开 1已开")
    private Integer invoiceStatus;
    private String parentBillId;
    
    private String parentBillCode;

    @ApiModelProperty("结算时间和结算方式")
    private String settlementMethodDate;
    @ApiModelProperty("纠纷解决")
    private String dispute;
    @ApiModelProperty("生效条件")
    private String effectiveConditions;
    @ApiModelProperty("交付条件")
    private String deliverConditions;
    @ApiModelProperty("时间运输方式及费运负担验收标准")
    private String dateTranMethod;
    @ApiModelProperty("地点和期限")
    private String placeTerm;
    private String parentBillNo;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;

    /**
     * 合同编号
     */
    @ApiModelProperty("合同编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;
    /**
     * 创建日期
     */
    @ApiModelProperty("交货日期")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;
    /**
     * 报价单id
     */
    @ApiModelProperty("报价单id")
    private String sourceBillId;

    /**
     * 结算币种
     */
    @ApiModelProperty("结算币种")
    private String currency;
    /**
     * 汇率
     */
    @ApiModelProperty("汇率")
    private BigDecimal rate;
    /**
     * 合同总金额（原币）
     */
    @ApiModelProperty("合同总金额（原币）")
    private BigDecimal originalCurTotalAmo;
    /**
     * 合同总金额（本币）
     */
    @ApiModelProperty("合同总金额（本币）")
    private BigDecimal localCurTotalAmo;
    /**
     * 价格条款
     */
    @ApiModelProperty("价格条款")
    private String priceTerms;
    /**
     * 付款方式
     */
    @ApiModelProperty("付款方式")
    private String paymentType;
    /**
     * 起运港
     */
    @ApiModelProperty("起运港")
    private String initiallyPort;
    /**
     * 目的港
     */
    @ApiModelProperty("目的港")
    private String finallyPort;
    /**
     * 运输方式
     */
    @ApiModelProperty("运输方式")
    private String transportType;
    /**
     * 总数量
     */
    @ApiModelProperty("总数量")
    private Integer totalNum;
    /**
     * 总体积
     */
    @ApiModelProperty("总体积")
    private BigDecimal totalVolume;
    /**
     * 柜型
     */
    @ApiModelProperty("柜型")
    private String cabinetModel;
    /**
     * 装柜百分比
     */
    @ApiModelProperty("装柜百分比")
    private BigDecimal loadingPercentage;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 客户id
     */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
     * 公司id
     */
    @ApiModelProperty("公司id")
    private String companyId;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    @ApiModelProperty("生产要求")
    private String productionRequested;
    @ApiModelProperty("包装要求")
    private String packagingRequested;
    @ApiModelProperty("标签要求")
    private String deliveryRequested;
    @ApiModelProperty("随货单据")
    private String acceptanceStandard;
    @ApiModelProperty("佣金金额")
    private BigDecimal commissionAmo;
    @ApiModelProperty("客户收货地址")
    private String custShippingAddress;
    @ApiModelProperty("客户单据地址")
    private String custBillAddress;

    private String flowStatus;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("保存的报价单商品|入参")
    private List<SoSoOrderCommodity> commodityList;
    @TableField(exist = false)
    @ApiModelProperty("删除的报价单商品id数组|入参")
    private List<String> commodityDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("保存的商品占用|入参")
    private List<StInventoryGrab> InventoryGrabList;


    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("客户地址|回显")
    private String custAddress;
    @TableField(exist = false)
    @ApiModelProperty("客户电话|回显")
    private String custPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("客户开户行|回显")
    private String custBank;
    @TableField(exist = false)
    @ApiModelProperty("客户银行账号|回显")
    private String custBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("客户邮编|回显")
    private String custZipCode;
    @TableField(exist = false)
    @ApiModelProperty("客户传真|回显")
    private String custFax;
    @TableField(exist = false)
    @ApiModelProperty("客户税号|回显")
    private String custDutyNo;

    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司邮编|回显")
    private String cpnZipCode;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("报价单号|回显")
    private String quotationNo;

    @TableField(exist = false)
    private String comTable = "so_so_order_commodity";



    @TableField(exist = false)
    private final String modelCode = "SO_SO_ORDER";
    @TableField(exist = false)
    private String getOaPendding;
    @JsonIgnore
    @ApiModelProperty("单据总数|回显")
    @TableField(exist = false)
    private int orderNum=1;
    @TableField(exist = false)
    @ApiModelProperty("单据总金额|回显")
    @JsonIgnore
    private BigDecimal orderAmount;
    @JsonIgnore
    @TableField(exist = false)
    @ApiModelProperty("出运数量|回显")
    private int transNum=1;
    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    @JsonIgnore
    @TableField(exist = false)
    @ApiModelProperty("出运总金额|回显")
    private BigDecimal transAmount;
}
