package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.CuCustomerContacts;
import com.whln.ordermax.data.mapper.CuCustomerContactsMapper;

import java.util.List;

/**
 *
 */
@Repository
public class CuCustomerContactsRepository extends ServiceImpl<CuCustomerContactsMapper, CuCustomerContacts>{

    public IPage<CuCustomerContacts> pageByEntity(CuCustomerContacts entity, Page<CuCustomerContacts> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<CuCustomerContacts> listByEntity(CuCustomerContacts entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<CuCustomerContacts> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




