package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.EoEnquiryPlans;
import com.whln.ordermax.data.mapper.EoEnquiryPlansMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class EoEnquiryPlansRepository extends ServiceImpl<EoEnquiryPlansMapper, EoEnquiryPlans>{

    public IPage<EoEnquiryPlans> pageByEntity(EoEnquiryPlans entity, Page<EoEnquiryPlans> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<EoEnquiryPlans> listByEntity(EoEnquiryPlans entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<EoEnquiryPlans> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




