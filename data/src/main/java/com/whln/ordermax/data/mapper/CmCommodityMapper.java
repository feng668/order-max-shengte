package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.CmCommodity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CmCommodity
 */
public interface CmCommodityMapper extends BaseMapper<CmCommodity> {

    List<CmCommodity> listByEntity(@Param("entity")CmCommodity entity);

    Page<CmCommodity> listByEntity(@Param("entity")CmCommodity entity, Page<CmCommodity> page);

    Integer insertBatch(@Param("entitylist")List<CmCommodity> entitylist);

    List<CmCommodity> listWithLastQoPriceByCustId(@Param("customerId") String customerId);

    Page<CmCommodity> pageWithLastQoPriceByCustId(@Param("entity")CmCommodity entity, Page<CmCommodity> page);

    Page<CmCommodity> cmList(CmCommodity entity, Page<CmCommodity> page);
}




