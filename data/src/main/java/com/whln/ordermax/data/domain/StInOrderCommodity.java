package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
* 入库单商品
* @TableName st_in_order_commodity
*/
@ApiModel("入库单商品")
@TableName("st_in_order_commodity")
@Data
public class StInOrderCommodity implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    @ApiModelProperty("运费")
    private BigDecimal transportationPost;
    @ApiModelProperty("保费")
    private BigDecimal premiumPost;
    /**
    * 入库单id
    */
    @ApiModelProperty("入库单id")
    private String billId;

    @ApiModelProperty("含税单价")
    private BigDecimal taxPrice;

    @ApiModelProperty("税率")
    private BigDecimal rate;
    @ApiModelProperty("供应商id")
    private String supplierId;
    @ApiModelProperty("含税金额")
    private BigDecimal taxAmo;

    @ApiModelProperty("不含税金额")
    private BigDecimal amo;

    @ApiModelProperty("批号")
    private String commodityBatchNo;
    @ApiModelProperty("简称")
    @TableField(exist = false)
    private String miniName;

    @TableField(exist = false)
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    private Integer contactTax;

    @TableField(exist = false)
    @NotNull(message = "元素不能为空")
    @ApiModelProperty("元素")
    private String elements;
    @TableField(exist = false)
    @ApiModelProperty("规格配比")
    private String ratio;

    @TableField(exist = false)
    @NotNull(message = "纯度不能为空")
    @ApiModelProperty("纯度")
    private String purity;
    /**
    * 商品id
    */
    @ApiModelProperty("商品id")
    private String commodityId;

    @ApiModelProperty("仓库id")
    @NotNull(message = "仓库id不能为空")
    private String warehouseId;

    @TableField(exist = false)
    @ApiModelProperty("仓库名|回显")
    private String warehouseName;
    @ApiModelProperty("单箱数量")
    private BigDecimal packNum;
    @ApiModelProperty("箱数")
    private BigDecimal boxNum;

    /**
    * 箱数
    */
    @ApiModelProperty("箱数")
    private BigDecimal containerNum;


    @ApiModelProperty("应收数量")
    private BigDecimal quantityNum;
    @ApiModelProperty("实收数量")
    private BigDecimal realReceiveNum;

    @ApiModelProperty("开票数量")
    private BigDecimal invoicingNum;


    /**
    * 采购数量
    */
    @ApiModelProperty("入库数量")
    private BigDecimal quotationNum;
    /**
    * 单箱净重
    */
    @ApiModelProperty("单箱净重")
    private BigDecimal singleBoxNet;
    /**
     * 单箱数量
     */
    @ApiModelProperty("单箱数量")
    private BigDecimal singleBoxNum;
    /**
    * 单箱毛重
    */
    @ApiModelProperty("单箱毛重")
    private BigDecimal singleBoxWeight;
    /**
    * 单箱体积
    */
    @ApiModelProperty("单箱体积")
    private BigDecimal singleBoxVolume;

    /**
    * 不含税单价
    */
    @ApiModelProperty("不含税单价")
    private BigDecimal price;

    /**
    * 进项税率
    */
    @ApiModelProperty("进项税率")
    private BigDecimal inputRate;
    /**
    * 进项税额
    */
    @ApiModelProperty("进项税额")
    private BigDecimal inputTax;
    /**
    * 备注
    */
    @ApiModelProperty("备注")
    private String note;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ApiModelProperty("批号")
    private String batchNo;

    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品英文规格|回显")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private Double cmNetWeight;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;
    @TableField(exist = false)
    @ApiModelProperty("商品体积|回显")
    private Double cmVolume;
    @TableField(exist = false)
    @ApiModelProperty("模糊查询参数")
    private String searchInfo;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
