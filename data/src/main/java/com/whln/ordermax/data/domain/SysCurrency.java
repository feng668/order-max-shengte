package com.whln.ordermax.data.domain;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* 币种
* @TableName sys_currency
*/
@ApiModel("币种")
@TableName("sys_currency")
@Data
public class SysCurrency implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 币种编码
    */
    @ApiModelProperty("币种名称")
    private String currencyName;
    /**
    * 币种名称
    */
    @ApiModelProperty("币种编码")
    private String currencyCode;
    /**
    * 汇率
    */
    @ApiModelProperty("汇率")
    private Double rate;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    private Date updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ApiModelProperty("期")
    private Integer stageNum;

    /* 附加属性 */
    @ApiModelProperty("上期汇率")
    @ExcelIgnore
    @TableField(exist = false)
    private Double lastRate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
