package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 货代公司
* @TableName phy_forwarder_company
*/
@ApiModel("货代公司")
@TableName("phy_forwarder_company")
@Data
public class PhyForwarderCompany implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 货代公司编号
    */
    @ApiModelProperty("货代公司编号")
    private String forwarderCompanyNo;
    /**
    * 创建日期
    */
    @ApiModelProperty("创建日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
    * 简称
    */
    @ApiModelProperty("简称")
    private String referred;
    /**
    * 全称
    */
    @ApiModelProperty("全称")
    private String fullName;
    /**
    * 电话
    */
    @ApiModelProperty("电话")
    private String phoneNum;
    /**
    * 传真
    */
    @ApiModelProperty("传真")
    private String fax;
    /**
    * 地址
    */
    @ApiModelProperty("地址")
    private String address;
    /**
    * 税号
    */
    @ApiModelProperty("税号")
    private String taxNum;
    /**
    * 国家
    */
    @ApiModelProperty("国家")
    private String country;
    /**
    * 城市
    */
    @ApiModelProperty("城市")
    private String city;
    /**
    * 归类
    */
    @ApiModelProperty("归类")
    private Integer classified;
    /**
    * 来源
    */
    @ApiModelProperty("来源")
    private Integer source;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    /**
    * 开户银行
    */
    @ApiModelProperty("开户银行")
    private String bank;
    /**
    * 银行账号
    */
    @ApiModelProperty("银行账号")
    private String bankAccount;

    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("联系人|入参")
    private List<PhyCompanyContacts> contactsList;
    @TableField(exist = false)
    @ApiModelProperty("删除联系人id|入参")
    private List<String> contactsDelIdList;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
