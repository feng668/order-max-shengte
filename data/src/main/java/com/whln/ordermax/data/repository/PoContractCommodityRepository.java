package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PoContractCommodity;
import com.whln.ordermax.data.mapper.PoContractCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class PoContractCommodityRepository extends ServiceImpl<PoContractCommodityMapper, PoContractCommodity>{

    public IPage<PoContractCommodity> pageByEntity(PoContractCommodity entity, Page<PoContractCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PoContractCommodity> listByEntity(PoContractCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PoContractCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




