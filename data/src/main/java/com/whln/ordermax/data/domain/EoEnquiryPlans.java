package com.whln.ordermax.data.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
* 询价进度
* @TableName eo_enquiry_plans
*/
@ApiModel("询价进度")
@TableName("eo_enquiry_plans")
@Data
public class EoEnquiryPlans implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 询价单id
    */
    @ApiModelProperty("询价单id")
    private String enquiryId;
    /**
    * 序号
    */
    @ApiModelProperty("序号")
    private Integer sort;
    /**
    * 阶段编号
    */
    @ApiModelProperty("阶段编号")
    private Integer plan;
    /**
    * 阶段名称
    */
    @ApiModelProperty("阶段名称")
    private String planName;
    /**
    * 单据id
    */
    @ApiModelProperty("单据id")
    private String billId;
    /**
    * 模块代码
    */
    @ApiModelProperty("模块代码")
    private String billCode;
    /**
    * 阶段处理人
    */
    @ApiModelProperty("阶段处理人")
    private String ownerId;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


    /*附加属性*/
    @TableField(exist = false)
    private List<String> enquiryIdIn;


    @TableField(exist = false)
    @ApiModelProperty("处理人姓名")
    private String ownerName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
