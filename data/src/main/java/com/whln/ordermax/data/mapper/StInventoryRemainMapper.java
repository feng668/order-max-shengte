package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.StInventoryRemain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.ordermax.data.domain.StInventoryRemain
 */
public interface StInventoryRemainMapper extends BaseMapper<StInventoryRemain> {

    List<StInventoryRemain> listByEntity(@Param("entity")StInventoryRemain entity);

    IPage<StInventoryRemain> listByEntity(@Param("entity")StInventoryRemain entity, Page<StInventoryRemain> page);

    StInventoryRemain getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<StInventoryRemain> entitylist);

    List<StInventoryRemain> listRemainNumByOrder(@Param("commodityIdList")List<String> commodityIdList, @Param("warehouseId")String warehouseId);
}




