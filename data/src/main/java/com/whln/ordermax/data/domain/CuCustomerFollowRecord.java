package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-03-21 13:38
 */
@TableName("cu_customer_follow_record")
@Data
public class CuCustomerFollowRecord implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 拥有人id
     */
    private String ownerId;
    /**
     * 标题
     */
    private String title;
    /**
     * 备注
     */
    private String note;
    /**
     * 单据id
     */
    private String billId;
    /*
     * 单据模块
     */
    private String billCode;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 类型 做了什么
     */
    private String type;

    /**
     * 客户的
     */
    private String customerId;
}
