package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-06 16:10
 */
@Data
@TableName("customer_label_relation")
public class CustomerLabelRelation implements Serializable {
    @TableId(type=IdType.ASSIGN_ID)
    private String id;
    /**
     * 用户id
     */
    private String customerId;
    /**
     * 标签id
     */
    private String labelId;

    public static List<CustomerLabelRelation> build(List<String> labels,String customerId) {
        List<CustomerLabelRelation> userLabelRelations = new ArrayList<>();
       for (String labelId:labels ){
           CustomerLabelRelation userLabelRelation = new CustomerLabelRelation();
           userLabelRelation.setLabelId(labelId);
           userLabelRelation.setCustomerId(customerId);
           userLabelRelations.add(userLabelRelation);
       }
        return userLabelRelations;
    }
}
