package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PhyForwarderCompany;
import com.whln.ordermax.data.mapper.PhyForwarderCompanyMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class PhyForwarderCompanyRepository extends ServiceImpl<PhyForwarderCompanyMapper, PhyForwarderCompany>{

    public IPage<PhyForwarderCompany> pageByEntity(PhyForwarderCompany entity, Page<PhyForwarderCompany> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PhyForwarderCompany> listByEntity(PhyForwarderCompany entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PhyForwarderCompany> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




