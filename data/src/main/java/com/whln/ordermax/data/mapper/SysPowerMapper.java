package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.SysPower;

import java.util.List;
import java.util.Map;

/**
 * @Entity whln.dyx.data.domain.SysPower
 */
public interface SysPowerMapper extends BaseMapper<SysPower> {

    List<SysPower> listPowersByUserId(@Param("userId") String userId);

    List<SysPower> listPowersByRoleId(@Param("roleId")String roleId);

    List<Map<String,String>> listNonExistentFields(String userId, String billCode);

    List<SysPower> listFieldByParentId(String parentId);

    String getModelQueryFieldByUserId(@Param("userId")String userId, @Param("modelCode")String modelCode, @Param("fieldSource")String fieldSource);

    List<String> getModelFieldListByUserId(@Param("userId")String userId, @Param("modelCode")String modelCode, @Param("fieldSource")String fieldSource);

    List<Map> listFieldByParentIdRoleId(@Param("parentId")String parentId,@Param("roleId") String roleId);

    List<SysPower> listModelQueryFieldByUserId(@Param("userId")String userId, @Param("modelCode")String modelCode);

    String[] listPermissionByUserId(String userId, String category);
}




