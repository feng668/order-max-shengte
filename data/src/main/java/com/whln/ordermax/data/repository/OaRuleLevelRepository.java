package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.OaRuleLevel;
import com.whln.ordermax.data.mapper.OaRuleLevelMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class OaRuleLevelRepository extends ServiceImpl<OaRuleLevelMapper, OaRuleLevel>{

    public IPage<OaRuleLevel> pageByEntity(OaRuleLevel entity, Page<OaRuleLevel> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<OaRuleLevel> listByEntity(OaRuleLevel entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<OaRuleLevel> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




