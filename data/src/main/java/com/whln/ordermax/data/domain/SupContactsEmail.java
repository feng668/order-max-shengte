package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
* 供应商联系人邮箱
* @TableName sup_contacts_email
*/
@ApiModel("供应商联系人邮箱")
@TableName("sup_contacts_email")
@Data
public class SupContactsEmail implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 邮箱
    */
    @ApiModelProperty("邮箱")
    private String emailAddress;
    /**
    * 联系人id
    */
    @ApiModelProperty("联系人id")
    private String contactId;
    /**
    * 是否默认
    */
    @ApiModelProperty("是否默认")
    private Boolean isDefault;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @TableField(exist = false)
    private String supplierId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
