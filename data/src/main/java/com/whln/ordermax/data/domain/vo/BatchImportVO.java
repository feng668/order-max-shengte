package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-29 15:41
 */
@ApiModel("批量导入")
@Data
public class BatchImportVO {
    @ApiModelProperty("商品列表")
    List<CmCommodityVO> list;

    @ApiModelProperty("文档中是否有重复的编号  1  有  0没有")
    private Integer docRepeat;
}
