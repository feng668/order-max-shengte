package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.FnPayment;
import com.whln.ordermax.data.mapper.FnPaymentMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class FnPaymentRepository extends ServiceImpl<FnPaymentMapper, FnPayment>{

    public IPage<FnPayment> pageByEntity(FnPayment entity, Page<FnPayment> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<FnPayment> listByEntity(FnPayment entity) {
        return baseMapper.listByEntity(entity);
    }

    public FnPayment getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<FnPayment> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




