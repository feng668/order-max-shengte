package com.whln.ordermax.data.domain.base;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.whln.ordermax.common.entity.FilterCondition;

import java.util.List;

@Data
public class FilterBill extends BillBase{
    @ApiModelProperty("该单据的oa审核规则是否生效")
    @TableField(exist = false)
    private int oaIsActive;
    @TableField(exist = false)
    protected List<FilterCondition> filterConditionList;
}
