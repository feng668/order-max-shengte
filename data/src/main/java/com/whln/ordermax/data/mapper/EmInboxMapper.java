package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.EmInbox;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EmInbox
 */
public interface EmInboxMapper extends BaseMapper<EmInbox> {

    List<EmInbox> listByEntity(@Param("entity")EmInbox entity);

    IPage<EmInbox> listByEntity(@Param("entity")EmInbox entity, Page<EmInbox> page);

    Integer insertBatch(@Param("entitylist")List<EmInbox> entitylist);

    Boolean updateInboxBind(@Param("oppositeId")String oppositeId, @Param("emailMapList")List<HashMap<String, String>> emailMapList,@Param("oppositeType")String oppositeType);

    List<EmInbox> classifyByTypeCustomer(@Param("entity")EmInbox entity);
}




