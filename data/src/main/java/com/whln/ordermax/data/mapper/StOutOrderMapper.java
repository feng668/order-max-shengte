package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.StOutOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.StOutOrder
 */
public interface StOutOrderMapper extends BaseMapper<StOutOrder> {

    List<StOutOrder> listByEntity(@Param("entity")StOutOrder entity);

    IPage<StOutOrder> listByEntity(@Param("entity")StOutOrder entity, Page<StOutOrder> page);

    Integer insertBatch(@Param("entitylist")List<StOutOrder> entitylist);

}




