package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PdProductionLot;
import com.whln.ordermax.data.mapper.PdProductionLotMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class PdProductionLotRepository extends ServiceImpl<PdProductionLotMapper, PdProductionLot>{

    public IPage<PdProductionLot> pageByEntity(PdProductionLot entity, Page<PdProductionLot> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PdProductionLot> listByEntity(PdProductionLot entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PdProductionLot> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




