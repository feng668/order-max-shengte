package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.SupSupplierContacts;
import com.whln.ordermax.data.mapper.SupSupplierContactsMapper;

import java.util.List;

/**
 *
 */
@Repository
public class SupSupplierContactsRepository extends ServiceImpl<SupSupplierContactsMapper, SupSupplierContacts>{

    public IPage<SupSupplierContacts> pageByEntity(SupSupplierContacts entity, Page<SupSupplierContacts> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SupSupplierContacts> listByEntity(SupSupplierContacts entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SupSupplierContacts> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




