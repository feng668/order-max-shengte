package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.TrDespatchAdvice;
import com.whln.ordermax.data.domain.TrDespatchAdviceCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.TrDespatchAdviceCommodityMapper;
import com.whln.ordermax.data.mapper.TrDespatchAdviceMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class TrDespatchAdviceRepository extends ServiceImpl<TrDespatchAdviceMapper, TrDespatchAdvice>{

    @Resource
    private TrDespatchAdviceCommodityMapper trDespatchAdviceCommodityMapper;

    public IPage<TrDespatchAdvice> pageByEntity(TrDespatchAdvice entity, Page<TrDespatchAdvice> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<TrDespatchAdvice> listByEntity(TrDespatchAdvice entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<TrDespatchAdvice> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }


    public TrDespatchAdvice getAllById(String id) {
        TrDespatchAdvice allById = baseMapper.getAllById(id);
        BillQuery param = BillQuery.builder().billIds(CollUtils.toSet(allById.getId())).build();
        List<TrDespatchAdviceCommodity> trDespatchAdviceCommodities = trDespatchAdviceCommodityMapper.commodityList(param);
        allById.setCommodityList(trDespatchAdviceCommodities);
        return allById;
    }

}




