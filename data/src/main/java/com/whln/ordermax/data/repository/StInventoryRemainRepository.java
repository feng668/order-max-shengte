package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.StInventoryRemain;
import com.whln.ordermax.data.mapper.StInventoryRemainMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 */
@Repository
public class StInventoryRemainRepository extends ServiceImpl<StInventoryRemainMapper, StInventoryRemain>{

    public IPage<StInventoryRemain> pageByEntity(StInventoryRemain entity, Page<StInventoryRemain> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<StInventoryRemain> listByEntity(StInventoryRemain entity) {
        return baseMapper.listByEntity(entity);
    }

    public StInventoryRemain getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<StInventoryRemain> entityList){
        boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<StInventoryRemain> listRemainNumByOrder(List<String> commodityIdList,String warehouseId){
        return baseMapper.listRemainNumByOrder(commodityIdList,warehouseId);
    }

    public Boolean updateNum(String commodityId, String warehouseId, BigDecimal num, String inOutType){
        StInventoryRemain exsitsInventory = this.getOne(
                new QueryWrapper<StInventoryRemain>()
                        .eq("commodity_id", commodityId)
                        .eq("warehouse_id", warehouseId)
        );
        if ("IN".equals(inOutType)){
            if (exsitsInventory==null){
                exsitsInventory.setCommodityId(commodityId);
                exsitsInventory.setWarehouseId(warehouseId);
            }else {
                exsitsInventory.setNum(exsitsInventory.getNum().add(num));
            }
        }else {
            if (exsitsInventory==null){
                return false;
            }else {
                num = exsitsInventory.getNum().subtract(num);
                if (num.compareTo(BigDecimal.ZERO)<0){
                    return false;
                }
                exsitsInventory.setNum(exsitsInventory.getNum().subtract(num));
            }
        }
        this.saveOrUpdate(exsitsInventory);
        return true;
    }

    public Boolean saveOrUpdateBatchByUnique(List<StInventoryRemain> entityList){
        for (StInventoryRemain entity : entityList) {
            this.saveOrUpdateByUnique(entity);
        }
        return true;
    }

    public Boolean saveOrUpdateByUnique(StInventoryRemain entity){
        StInventoryRemain exsits = this.getOne(
                new QueryWrapper<StInventoryRemain>().lambda()
                        .eq(StInventoryRemain::getWarehouseId, entity.getWarehouseId())
                        .eq(StInventoryRemain::getCommodityId, entity.getCommodityId())
        );
        if (exsits != null) {
            this.update(
                    entity,
                    new QueryWrapper<StInventoryRemain>().lambda()
                            .eq(StInventoryRemain::getWarehouseId, entity.getWarehouseId())
                            .eq(StInventoryRemain::getCommodityId, entity.getCommodityId())
            );
        }else {
            this.save(entity);
        }
        return true;
    }

}




