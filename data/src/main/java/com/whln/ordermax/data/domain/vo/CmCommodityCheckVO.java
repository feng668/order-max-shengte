package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-27 14:37
 */
@Data
@ApiModel("商品导入参数")
public class CmCommodityCheckVO {
    @ApiModelProperty("已存在的")
    private List<CmCommodityVO> exits;
    @ApiModelProperty("不存在的")
    private List<CmCommodityVO> noExits;
    private List<CmCommodityVO> docRepeat;
    private Integer docRepeatNum;
    @ApiModelProperty("已存在的数量")
    private Integer exitsNum;
    @ApiModelProperty("不存在的数量")
    private Integer noExitsNum;

}
