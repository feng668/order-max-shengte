package com.whln.ordermax.data.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-02-25 11:43
 */
@ApiModel("库存明细")
@Data
public class StInventoryVO implements Serializable {
    private static final long serialVersionUID = -792998414872656322L;
    private String id;
    @ApiModelProperty("单据编号")
    private String billNo;
    @ApiModelProperty("入库类型")
    private String putType;
    @ApiModelProperty("商品中文名")
    private String cnName;
    @ApiModelProperty("商品英文名")
    private String enName;
    @ApiModelProperty("商品编号")
    private String partNo;
    @ApiModelProperty("计量单位")
    private String quantityUnit;
    @ApiModelProperty("中文规格")
    private String specification;
    @ApiModelProperty("英文规格")
    private String enSpecification;
    private String inOutType;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("操作时间")
    private LocalDateTime proceedDate;
    @TableField(exist = false)
    private String commodityId;
    @TableField(exist = false)
    private String oppId;
    @TableField(exist = false)
    private String oppName;
    @TableField(exist = false)
    private String oppNo;
    @TableField(exist = false)
    private String singleBoxVolume;
    @ApiModelProperty("数量")
    private String quotationNum;
    @ApiModelProperty("仓库编号")
    private String warehouseNo;
    @ApiModelProperty("批号")
    private String commodityBatchNo;
    @ApiModelProperty("仓库名字")
    private String warehouseName;

    public StInventoryVO() {
    }


}
