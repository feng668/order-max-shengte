package com.whln.ordermax.data.domain.base;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;

/**
 * @author liurun
 * @date 2023-03-03 17:52
 */
@Data
public class BillBase  implements Serializable {
    private static final long serialVersionUID = -6046810672636597523L;
    @TableField(exist = false)
    protected String lastBillId;
    @TableField(exist = false)
    protected String lastBillCode;
    @TableField(exist = false)
    protected String currentBillCode;
    @TableField(exist = false)
    protected String currentBillId;
}
