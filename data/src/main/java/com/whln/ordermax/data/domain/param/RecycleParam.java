package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-25 16:39
 */
@ApiModel("回收站")
@Data
public class RecycleParam {
    @ApiModelProperty("分页参数")
    private Integer current;
    @ApiModelProperty("分页参数")
    private Integer size;
    @ApiModelProperty("模块名")
    private String code;
    @ApiModelProperty("搜索参数")
    private String searchInfo;
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("批量删除参数")
    List<RecycleDeleteParam> recycleDeleteParams;
}
