package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whln.ordermax.data.domain.CuCustomerFollowRecord;

/**
 * @author liurun
 */
public interface CuCustomerFollowRecordMapper extends BaseMapper<CuCustomerFollowRecord> {
}
