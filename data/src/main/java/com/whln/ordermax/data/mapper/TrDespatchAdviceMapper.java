package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.TrDespatchAdvice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.TrDespatchAdvice
 */
public interface TrDespatchAdviceMapper extends BaseMapper<TrDespatchAdvice> {

    List<TrDespatchAdvice> listByEntity(@Param("entity")TrDespatchAdvice entity);

    IPage<TrDespatchAdvice> listByEntity(@Param("entity")TrDespatchAdvice entity, Page<TrDespatchAdvice> page);

    Integer insertBatch(@Param("entitylist")List<TrDespatchAdvice> entitylist);

    TrDespatchAdvice getAllById(String id);
}




