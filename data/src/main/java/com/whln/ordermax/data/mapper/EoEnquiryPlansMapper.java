package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.EoEnquiryPlans;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EoEnquiryPlans
 */
public interface EoEnquiryPlansMapper extends BaseMapper<EoEnquiryPlans> {

    List<EoEnquiryPlans> listByEntity(@Param("entity")EoEnquiryPlans entity);

    IPage<EoEnquiryPlans> listByEntity(@Param("entity")EoEnquiryPlans entity, Page<EoEnquiryPlans> page);

    Integer insertBatch(@Param("entitylist")List<EoEnquiryPlans> entitylist);

}




