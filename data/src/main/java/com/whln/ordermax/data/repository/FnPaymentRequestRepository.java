package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.FnPaymentRequest;
import com.whln.ordermax.data.mapper.FnPaymentRequestMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class FnPaymentRequestRepository extends ServiceImpl<FnPaymentRequestMapper, FnPaymentRequest>{

    public IPage<FnPaymentRequest> pageByEntity(FnPaymentRequest entity, Page<FnPaymentRequest> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<FnPaymentRequest> listByEntity(FnPaymentRequest entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<FnPaymentRequest> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




