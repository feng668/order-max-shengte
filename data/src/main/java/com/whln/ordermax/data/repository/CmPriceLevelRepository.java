package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.CmPriceLevel;
import com.whln.ordermax.data.mapper.CmPriceLevelMapper;

import java.util.List;

/**
 *
 */
@Repository
public class CmPriceLevelRepository extends ServiceImpl<CmPriceLevelMapper, CmPriceLevel>{

    public IPage<CmPriceLevel> pageByEntity(CmPriceLevel entity, Page<CmPriceLevel> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<CmPriceLevel> listByEntity(CmPriceLevel entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<CmPriceLevel> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




