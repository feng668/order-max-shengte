package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.CmCommodityImg;
import com.whln.ordermax.data.mapper.CmCommodityImgMapper;

import java.util.List;

/**
 *
 */
@Repository
public class CmCommodityImgRepository extends ServiceImpl<CmCommodityImgMapper, CmCommodityImg>{

    public IPage<CmCommodityImg> pageByEntity(CmCommodityImg entity, Page<CmCommodityImg> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<CmCommodityImg> listByEntity(CmCommodityImg entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<CmCommodityImg> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




