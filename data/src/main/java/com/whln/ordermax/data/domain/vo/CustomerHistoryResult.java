package com.whln.ordermax.data.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-08 21:22
 */
@Data
public class CustomerHistoryResult implements Serializable {
    private static final long serialVersionUID = 4127138629984543475L;
    private String billNo;
    private String billId;
    List<CustomerHistoryVO> list;
}
