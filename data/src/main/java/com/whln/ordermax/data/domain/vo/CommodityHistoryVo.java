package com.whln.ordermax.data.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author liurun
 * @date 2023-03-08 21:57
 */
@Data
public class CommodityHistoryVo implements Serializable {
    private static final long serialVersionUID = 438781337833005108L;
    private String id;
    private String billNo;
    private String billName;
    private String commodityId;
    private String billCode;
    private BigDecimal price;
    private BigDecimal quotationNum;
    private Date time;
    private BigDecimal amount;
    private BigDecimal num;
}
