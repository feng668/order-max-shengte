package com.whln.ordermax.data.domain.base;

import lombok.Data;

@Data
public class FlowComFilterOaBill extends ComFilterOaBill {
    private String flowStatus;
}
