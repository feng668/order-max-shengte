package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* 商品bom
* @TableName cm_commodity_bom
*/
@ApiModel("商品bom")
@TableName("cm_commodity_bom")
@Data
public class CmCommodityBom implements Serializable {



    /**
    * 商品id
    */
    @ApiModelProperty("商品id")
    private String commodityId;
    /**
    * 配件id
    */
    @ApiModelProperty("配件id")
    private String partId;
    /**
    * 配件数量
    */
    @ApiModelProperty("配件数量")
    private BigDecimal partNum;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("配件总数量")
    private BigDecimal totalPartNum;
    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private BigDecimal netWeight;
    @TableField(exist = false)
    @ApiModelProperty("毛重|回显")
    private BigDecimal grossWeight;
    @TableField(exist = false)
    @ApiModelProperty("长度|回显")
    private Double length;
    @TableField(exist = false)
    @ApiModelProperty("宽度|回显")
    private Double width;
    @TableField(exist = false)
    @ApiModelProperty("高度|回显")
    private Double highly;
    @TableField(exist = false)
    @ApiModelProperty("体积|回显")
    private Double volume;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("包装要求|回显")
    private String packagingRemark;
    @TableField(exist = false)
    @ApiModelProperty("备注|回显")
    private String note;
    @TableField(exist = false)
    @ApiModelProperty("默认采购价|回显")
    private Double purchasePrice;
    @TableField(exist = false)
    @ApiModelProperty("创建时间|回显")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    @TableField(exist = false)
    @ApiModelProperty("更新时间|回显")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    @TableField(exist = false)
    @ApiModelProperty("默认供应商id|回显")
    private String defaultSupplierId;
    @TableField(exist = false)
    @ApiModelProperty("报关编码|回显")
    private String ccDeclarationNo;
    @TableField(exist = false)
    @ApiModelProperty("报关中文名|回显")
    private String ccCustomsCnName;
    @TableField(exist = false)
    @ApiModelProperty("报关英文名|回显")
    private String ccCustomsEnName;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;
    @TableField(exist = false)
    @ApiModelProperty("退税率|回显")
    private Double ccRebatesRate;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("默认供应商编号|回显")
    private String defaultSupplierNo;
    @TableField(exist = false)
    @ApiModelProperty("默认供应商全名|回显")
    private String defaultSupplierName;
    @TableField(exist = false)
    private String articleType;


    @ApiModelProperty("简称")
    @TableField(exist = false)
    private String miniName;
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    @TableField(exist = false)
    private Integer contactTax;
    @TableField(exist = false)
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("元素")
    private String elements;
    @TableField(exist = false)
    @ApiModelProperty("纯度")
    private String purity;
    @TableField(exist = false)

    private static final long serialVersionUID = 1L;
}
