package com.whln.ordermax.data.repository;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CmCommodity;
import com.whln.ordermax.data.domain.StInventoryRemain;
import com.whln.ordermax.data.domain.param.StInventoryListParam;
import com.whln.ordermax.data.domain.vo.BaseInfoVO;
import com.whln.ordermax.data.domain.vo.BaseTotalVO;
import com.whln.ordermax.data.domain.vo.CmCommodityVO;
import com.whln.ordermax.data.domain.vo.StInventoryVO;
import com.whln.ordermax.data.mapper.StInventoryMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 */
@Repository
public class StInventoryRepository extends ServiceImpl<StInventoryMapper, Object> {

    @Resource
    private CmCommodityRepository commodityRepository;
    /*  public IPage<StInventoryVO> pageByEntity(StInventoryListParam entity, Page<StInventoryListParam> page) {
          return baseMapper.listStInventory(entity,page);
      }
  */
    public IPage<Map> pageByEntity(Map entity, Page<Map> page) {
        return baseMapper.listByEntity(entity, page);
    }

    public IPage<BaseTotalVO> pageTotalByStageType(Map entity, Page<Map> page) {
        return baseMapper.pageTotalByStageType(entity, page);
    }

    public IPage<CmCommodityVO> listStInventory(StInventoryListParam entity, Page<StInventoryListParam> page) {
        IPage<StInventoryVO> stInventoryVOIPage = baseMapper.page(entity, page);
        List<StInventoryVO> records = stInventoryVOIPage.getRecords();
        records.sort((o1, o2) -> {
            if (ObjectUtil.isEmpty(o1.getProceedDate()) && ObjectUtil.isEmpty(o2.getProceedDate())) {
                return 0;
            }
            if (ObjectUtil.isEmpty(o1.getProceedDate())) {
                return 1;
            }
            if (ObjectUtil.isEmpty(o2.getProceedDate())) {
                return -1;
            }
            return (int) -(o1.getProceedDate().toInstant(ZoneOffset.UTC).toEpochMilli() - o2.getProceedDate().toInstant(ZoneOffset.UTC).toEpochMilli());
        });

        Map<String, List<StInventoryVO>> groupByCommodityId = records.stream().collect(Collectors.groupingBy(StInventoryVO::getCommodityId));
        Set<String> commodityIdSet = groupByCommodityId.keySet();
        List<CmCommodity> cmCommodityList = commodityRepository.listByIds(commodityIdSet);
        List<CmCommodityVO> collect = cmCommodityList.stream().map(e -> {
            CmCommodityVO commodityVO = BeanUtil.copyProperties(e, CmCommodityVO.class);
            List<StInventoryVO> list = groupByCommodityId.get(e.getId());
            commodityVO.setStInventoryVOList(list);
            return commodityVO;
        }).collect(Collectors.toList());
        if (ObjectUtil.isNotEmpty(page)) {
            List<CmCommodityVO> records1 = collect.stream().skip(((page.getCurrent() - 1) * page.getSize())).limit(page.getSize()).collect(Collectors.toList());
            Page<CmCommodityVO> resultPage = new Page<>();
            resultPage.setRecords(records1);
            resultPage.setTotal(collect.size());
            return resultPage;
        } else {
            Page<CmCommodityVO> resultPage = new Page<>();
            resultPage.setRecords(collect);
            resultPage.setTotal(collect.size());
            return resultPage;
        }
    }

    public Page<BaseInfoVO> baseInfo(StInventoryRemain entity, Page<StInventoryListParam> page) {
        //查出入库单
        List<BaseInfoVO> stInOrder = baseMapper.getSoSiOrder(entity);
        //查出出库单
        List<BaseInfoVO> stOutOrders = baseMapper.getSoSoOrder(entity);
        //查出占用单
        List<BaseInfoVO> inventoryOrders = baseMapper.getStInventory(entity);

        Map<String, BaseInfoVO> stInOrderGroupBy = stInOrder.stream()
                .collect(Collectors.toMap(e -> e.getCommodityId() + e.getWarehouseId() + e.getCommodityBatchNo(), Function.identity(), (x, y) -> {
                    x.setNum(x.getNum().add(y.getNum()));
                    return x;
                }));

        Map<String, BaseInfoVO> stOutOrdersGroupBy = stOutOrders.stream()
                .collect(Collectors.toMap(e -> e.getCommodityId() + e.getWarehouseId() + e.getCommodityBatchNo(), Function.identity(), (x, y) -> {
                    x.setNum(x.getNum().add(y.getNum()));
                    return x;
                }));

        Map<String, BaseInfoVO> inventoryOrdersGroupBy = inventoryOrders.stream()
                .collect(Collectors.toMap(e -> e.getCommodityId() + e.getWarehouseId() + e.getCommodityBatchNo(), Function.identity(), (x, y) -> {
                    x.setNum(x.getNum().add(y.getNum()));
                    return x;
                }));
        //入库单减去出库单和占用单的数量
        stOutOrdersGroupBy.forEach((key, value) -> {
            BaseInfoVO baseInfoVO = stInOrderGroupBy.get(key);
            BaseInfoVO inventory = inventoryOrdersGroupBy.get(key);
            if (ObjectUtil.isNotEmpty(baseInfoVO)) {
                //占用单
                baseInfoVO.setNum(baseInfoVO.getNum().subtract(value.getNum()));
                if (ObjectUtil.isNotEmpty(inventory)) {
                    baseInfoVO.setGrabNum(inventory.getNum());
                } else {
                    baseInfoVO.setGrabNum(BigDecimal.ZERO);
                }
                stInOrderGroupBy.put(baseInfoVO.getCommodityId() + baseInfoVO.getWarehouseId() + baseInfoVO.getCommodityBatchNo(), baseInfoVO);
            } else {
                value.setNum(value.getNum().negate());
                if (ObjectUtil.isNotEmpty(inventory)) {
                    value.setGrabNum(inventory.getNum());
                } else {
                    value.setGrabNum(BigDecimal.ZERO);
                }
                stInOrderGroupBy.put(key, value);
            }
        });

        Collection<BaseInfoVO> values = stInOrderGroupBy.values();
        List<BaseInfoVO> resultList = values.stream().peek(item -> {
            if (ObjectUtil.isEmpty(item.getGrabNum())) {
                item.setGrabNum(BigDecimal.ZERO);
            }
            item.setFreeNum(item.getNum().subtract(item.getGrabNum()));
        }).collect(Collectors.toList());

        if (ObjectUtil.isNotEmpty(page)) {
            List<BaseInfoVO> records = resultList.stream().skip(((page.getCurrent() - 1) * page.getSize())).limit(page.getSize()).collect(Collectors.toList());
            Page<BaseInfoVO> resultPage = new Page<>();
            resultPage.setRecords(records);
            resultPage.setTotal(resultList.size());
            return resultPage;
        } else {
            Page<BaseInfoVO> resultPage = new Page<>();
            resultPage.setRecords(resultList);
            resultPage.setTotal(resultList.size());
            return resultPage;
        }


    }

    public Page<CmCommodityVO> baseInfoByCommodity(StInventoryRemain entity, Page<StInventoryListParam> page){
        Page<BaseInfoVO> baseInfoVOPage = baseInfo(entity, null);
        List<BaseInfoVO> records = baseInfoVOPage.getRecords();
        Page<CmCommodityVO> resultPage = new Page<>();
        if (ObjectUtil.isNotEmpty(records)) {
            Map<String, List<BaseInfoVO>> groupByCommodityId = records.stream().collect(Collectors.groupingBy(BaseInfoVO::getCommodityId));
            Set<String> commodityIdSet = groupByCommodityId.keySet();
            List<CmCommodity> cmCommodityList = commodityRepository.listByIds(commodityIdSet);
            List<CmCommodityVO> collect = cmCommodityList.stream().map(e -> {
                CmCommodityVO commodityVO = BeanUtil.copyProperties(e, CmCommodityVO.class);
                List<BaseInfoVO> list = groupByCommodityId.get(e.getId());
                commodityVO.setBaseInfoVOList(list);
                return commodityVO;
            }).collect(Collectors.toList());
            if (ObjectUtil.isNotEmpty(page)) {
                List<CmCommodityVO> records1 = collect.stream().skip(((page.getCurrent() - 1) * page.getSize())).limit(page.getSize()).collect(Collectors.toList());
                resultPage.setRecords(records1);
                resultPage.setTotal(collect.size());
                return resultPage;
            } else {
                resultPage.setRecords(collect);
                resultPage.setTotal(collect.size());
                return resultPage;
            }

        }
        return resultPage;
    }

}




