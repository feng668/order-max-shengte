package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.param.ListHistoryPriceByComIdParam;
import com.whln.ordermax.data.domain.vo.LastPriceByComVO;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.common.entity.quote.QoCommodityHistoryQueteVo;
import com.whln.ordermax.data.domain.QoQuoteCommodity;
import com.whln.ordermax.data.mapper.QoQuoteCommodityMapper;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Repository
public class QoQuoteCommodityRepository extends ServiceImpl<QoQuoteCommodityMapper, QoQuoteCommodity>{

    public IPage<QoQuoteCommodity> pageByEntity(QoQuoteCommodity entity, Page<QoQuoteCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<QoQuoteCommodity> listByEntity(QoQuoteCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<QoQuoteCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<Map> getMapByIdWithInfo(String queteId) {
        return baseMapper.getMapByIdWithInfo(queteId);
    }

    public IPage<QoCommodityHistoryQueteVo> listHistoryPriceByComIdCustId(ListHistoryPriceByComIdParam param) {
        IPage<QoCommodityHistoryQueteVo> qoCommodityHistoryQueteVoIPage = baseMapper.listHistoryPriceByComIdCustId(param, new Page<>(param.getCurrent(), param.getSize()));
        return qoCommodityHistoryQueteVoIPage;
    }

    public LastPriceByComVO  getLastPriceByComIdAndCustId(String customerId, String commodityId) {
        LastPriceByComVO lastPriceByComIdAndCustId = baseMapper.getLastPriceByComIdAndCustId(customerId, commodityId);
        return lastPriceByComIdAndCustId;
    }
}




