package com.whln.ordermax.data.domain.base;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-03-06 15:02
 */
@Data
public class BaseEntity {
    /**
     * 创建时间
     */
    protected LocalDateTime createTime;
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

}
