package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.common.annotaions.FieldFilter;
import com.whln.ordermax.data.domain.SoSoOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SoSoOrder
 */
public interface SoSoOrderMapper extends BaseMapper<SoSoOrder> {
    @FieldFilter
    List<SoSoOrder> listByEntity(@Param("entity")SoSoOrder entity);
    @FieldFilter
    IPage<SoSoOrder> listByEntity(@Param("entity")SoSoOrder entity, Page<SoSoOrder> page);

    SoSoOrder getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<SoSoOrder> entitylist);

}




