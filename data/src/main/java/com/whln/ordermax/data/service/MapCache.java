package com.whln.ordermax.data.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.whln.ordermax.common.enums.CommodityModel;
import com.whln.ordermax.common.enums.ModelMessage;
import com.whln.ordermax.common.enums.ModelMessage1;
import com.whln.ordermax.common.redis.RedisClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author liurun
 * @date 2023-03-14 16:24
 */
@AllArgsConstructor
@Component
public class MapCache {

    private static final String baseReference = "com.whln.ordermax.data";

    private static final String mapperContext = ".mapper";

    private static final String repositoryContext = ".repository";

    private static final String entityContext = ".domain";

    private static final String repositorySuffix = "Repository";
    private static final String mapperSuffix = "Mapper";


    private final RedisClient<String,Object> redisClient;


    public Map<String, Map<String, String>> commodityModelMap() {
        Object o = redisClient.get("commodityModelMap:");
        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(),HashMap.class);
        }
        Map<String, Map<String, String>> mapperModelMap = new HashMap();
        for (CommodityModel modelMessage : CommodityModel.values()) {
                mapperModelMap.put(
                        StrUtil.concat(true, baseReference, mapperContext, ".", modelMessage.getCommodityEntityName(), mapperSuffix),
                        new HashMap<String, String>() {{
                            put("modelCode", modelMessage.getCode());
                            put("fieldSource", "CM");
                        }}
                );
        }
        redisClient.set("commodityModelMap:",JSONUtil.toJsonStr(mapperModelMap));
        return mapperModelMap;
    }

    public Map<String, String> entityComMapperXMap() {
        Object o = redisClient.get("entityComMapperXMap");
        if (ObjectUtil.isNotEmpty(o)) {
            return (Map<String, String>) JSONUtil.toBean(o.toString(), HashMap.class);
        }

        Map<String, String> collect = Arrays.stream(ModelMessage.values()).collect(Collectors.toMap(ModelMessage::getEntityName, m -> {
            if (m == null) {
                return null;
            } else {
                return StrUtil.concat(true, baseReference, mapperContext, ".", m.getCommodityEntityName(), mapperSuffix);
            }
        }, (o1, o2) -> o1));
        redisClient.set("entityComMapperXMap", JSONUtil.toJsonStr(collect));
        return collect;
    }

    public Map<String, String> entityCodeMap() {
        Object o = redisClient.get("entityCodeMap");
        if (ObjectUtil.isNotEmpty(o)) {
            return (Map<String, String>) JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, String> collect = Arrays.stream(ModelMessage.values()).filter(m -> m.getEntityName() != null).collect(
                Collectors.toMap(
                        ModelMessage::getEntityName,
                        ModelMessage::getCode
                )
        );
        redisClient.set("entityCodeMap", JSONUtil.toJsonStr(collect));
        return collect;
    }


    public Map<String, String> entityComRepositoryMap() {
        return Arrays.stream(ModelMessage.values()).collect(Collectors.toMap(ModelMessage::getEntityName, m -> {
            if (m == null) {
                return null;
            } else {
                return StrUtil.concat(true, baseReference, repositoryContext, ".", m.getCommodityEntityName(), repositorySuffix);
            }
        }, (o1, o2) -> o1));
    }


    public Map<String, String> entityComEntityMap() {
        return Arrays.stream(ModelMessage.values()).collect(Collectors.toMap(ModelMessage::getEntityName, m -> {
            if (m == null) {
                return null;
            } else {
                return StrUtil.concat(true, baseReference, entityContext, ".", m.getCommodityEntityName());
            }
        }, (o1, o2) -> o1));
    }


    public Map<String, Map<String, String>> codeTableMap() {
        return Arrays.stream(ModelMessage.values()).collect(Collectors.toMap(ModelMessage::getCode, m -> {
            if (m == null) {
                return null;
            } else {
                return new HashMap() {{
                    put("M", StrUtil.toUnderlineCase(m.getEntityName()));
                    put("CM", StrUtil.toUnderlineCase(m.getCommodityEntityName()));

                }};
            }
        }, (x, y) -> x));
    }


    public Map<String, String> codeFullRepositoryMap() {
        Object o = redisClient.get("codeFullRepositoryMap");
        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, String> collect = Arrays.stream(ModelMessage.values()).collect(
                Collectors.toMap(
                        ModelMessage::getCode,
                        m -> StrUtil.concat(
                                true, baseReference, repositoryContext, ".", m.getEntityName(), repositorySuffix
                        )
                        , (x, y) -> x)
        );
        redisClient.set("codeFullRepositoryMap", JSONUtil.toJsonStr(collect));
        return collect;
    }


    public Map<String, String> codeFullEntityMap() {
        Object o = redisClient.get("codeFullEntityMap");
        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, String> collect = Arrays.stream(ModelMessage.values()).collect(
                Collectors.toMap(
                        ModelMessage::getCode,
                        m -> StrUtil.concat(
                                true, baseReference, entityContext, ".", m.getEntityName()
                        )
                )
        );
        redisClient.set("codeFullEntityMap", JSONUtil.toJsonStr(collect));
        return collect;
    }


    public Map<String, String> codeTableNameMap() {
        Object o = redisClient.get("codeTableNameMap");
        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, String> collect = Arrays.stream(ModelMessage.values()).collect(
                Collectors.toMap(
                        ModelMessage::getCode,
                        ModelMessage::getTableName
                )
        );
        redisClient.set("codeTableNameMap", JSONUtil.toJsonStr(collect));
        return collect;
    }


    /**
     * 后期删除
     */
    public Map<String, String> codeTemplatePathMap() {
        Object o = redisClient.get("codeTemplatePathMap");
        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, String> collect = Arrays.stream(ModelMessage.values()).filter(m -> m.getTemplatePath() != null).collect(
                Collectors.toMap(
                        ModelMessage::getCode,
                        ModelMessage::getTemplatePath
                )
        );
        redisClient.set("codeTemplatePathMap", JSONUtil.toJsonStr(collect));
        return collect;
    }


    public Map<String, String> codeFullCommodityMapperMap() {
        Object o = redisClient.get("codeFullCommodityMapperMap");
        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, String> collect = Arrays.stream(ModelMessage.values()).filter(m -> m.getCommodityEntityName() != null).collect(
                Collectors.toMap(
                        ModelMessage::getCode,
                        m -> StrUtil.concat(
                                true, baseReference, mapperContext, ".", m.getCommodityEntityName(), mapperSuffix
                        )
                )
        );
        redisClient.set("codeFullCommodityMapperMap", JSONUtil.toJsonStr(collect));
        return collect;
    }


    public Map<String, String> codeFullMapperMap() {
        Object o = redisClient.get("codeFullMapperMap");
        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, String> collect = Arrays.stream(ModelMessage.values()).filter(m -> m.getCommodityEntityName() != null).collect(
                Collectors.toMap(
                        ModelMessage::getCode,
                        m -> StrUtil.concat(
                                true, baseReference, mapperContext, ".", m.getEntityName(), mapperSuffix
                        )
                )
        );
        redisClient.set("codeFullMapperMap", JSONUtil.toJsonStr(collect));
        return collect;
    }


    public Map<String, String> codeFullCommodityRepositoryMap() {
        Object o = redisClient.get("codeFullCommodityRepositoryMap");
        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, String> collect = Arrays.stream(ModelMessage.values()).filter(m -> m.getCommodityEntityName() != null).collect(
                Collectors.toMap(
                        ModelMessage::getCode,
                        m -> StrUtil.concat(
                                true, baseReference, repositoryContext, ".", m.getCommodityEntityName(), repositorySuffix
                        )
                )
        );
        redisClient.set("codeFullCommodityRepositoryMap", JSONUtil.toJsonStr(collect));
        return collect;
    }


    /**
     * 主档和商品mapper全路径 map
     */
    public Map<String, Map<String, String>> mapperModelMap() {
        Object o = redisClient.get("mapperModelMap");

        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, Map<String, String>> mapperModelMap = new HashMap();
        for (ModelMessage modelMessage : ModelMessage.values()) {
            if (modelMessage.getCommodityEntityName() != null) {
                mapperModelMap.put(
                        StrUtil.concat(true, baseReference, mapperContext, ".", modelMessage.getCommodityEntityName(), mapperSuffix),
                        new HashMap<String, String>(2) {{
                            put("modelCode", modelMessage.getCode());
                            put("fieldSource", "CM");
                        }}
                );
            }
            if (modelMessage.getEntityName() != null) {
                mapperModelMap.put(
                        StrUtil.concat(true, baseReference, mapperContext, ".", modelMessage.getEntityName(), mapperSuffix),
                        new HashMap<String, String>(2) {{
                            put("modelCode", modelMessage.getCode());
                            put("fieldSource", "M");
                        }}
                );
            }
        }
        redisClient.set("mapperModelMap", JSONUtil.toJsonStr(mapperModelMap));
        return mapperModelMap;
    }
    public Map<String, Map<String, String>> mapperModelMap1() {
        Object o = redisClient.get("mapperModelMap1");
        if (ObjectUtil.isNotEmpty(o)) {
            return JSONUtil.toBean(o.toString(), HashMap.class);
        }
        Map<String, Map<String, String>> mapperModelMap = new HashMap();
        for (ModelMessage1 modelMessage : ModelMessage1.values()) {
            if (modelMessage.getCommodityEntityName() != null) {
                mapperModelMap.put(
                        StrUtil.concat(true, baseReference, mapperContext, ".", modelMessage.getCommodityEntityName(), mapperSuffix),
                        new HashMap<String, String>(2) {{
                            put("modelCode", modelMessage.getCode());
                            put("fieldSource", "CM");
                        }}
                );
            }
            if (modelMessage.getEntityName() != null) {
                String key = StrUtil.concat(true, baseReference, mapperContext, ".", modelMessage.getEntityName(), mapperSuffix);
                if (ObjectUtil.isNotEmpty(modelMessage.getMethodName())) {
                    key = StrUtil.concat(true, key, ".", modelMessage.getMethodName());
                }
                mapperModelMap.put(
                        key,
                        new HashMap<String, String>(2) {{
                            put("modelCode", modelMessage.getCode());
                            put("fieldSource", "M");
                        }}
                );
            }
        }
        redisClient.set("mapperModelMap1", JSONUtil.toJsonStr(mapperModelMap));
        return mapperModelMap;
    }

}
