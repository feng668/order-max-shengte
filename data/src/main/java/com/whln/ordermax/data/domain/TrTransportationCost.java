package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.FilterBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 出运费用
* @TableName tr_transportation_cost
*/
@ApiModel("出运费用")
@TableName("tr_transportation_cost")
@Data
public class TrTransportationCost extends FilterBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 合同id
    */
    @ApiModelProperty("合同id")
    private String transportationId;
    /**
    * 费用代码
    */
    @ApiModelProperty("费用代码")
    private String costCode;
    @ApiModelProperty("出运类型 1 内销 2外销")
    private Integer transportationType;
    /**
    * 费用名称
    */
    @ApiModelProperty("费用名称")
    private String costName;
    /**
    * 人民币金额
    */
    @ApiModelProperty("人民币金额")
    private Double localCurrencyAmo;
    /**
    * 汇率
    */
    @ApiModelProperty("汇率")
    private BigDecimal rmbRate;
    @TableField(exist = false)
    private String custCnName;
    /**
    * 美金金额
    */
    @ApiModelProperty("美金金额")
    private BigDecimal originalCurrencyAmo;
    /**
    * 货代公司id
    */
    @ApiModelProperty("货代公司id")
    private String forwarderCompanyId;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @ApiModelProperty("费用实际金额")
    private BigDecimal realAmount;
    @ApiModelProperty("费用实际金额差额")
    private BigDecimal difference;

    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;



    /*附加属性*/
    @TableField(exist = false)
    private List<String> idIn;

    @TableField(exist = false)
    @ApiModelProperty("出运编号")
    private String transportationNo;

    @ApiModelProperty("运单号")
    @TableField(exist = false)
    private String voyageNo;
    @ApiModelProperty("目的地")
    @TableField(exist = false)
    private String finallyPort;
    @ApiModelProperty("总毛重")
    @TableField(exist = false)
    private BigDecimal totalGrossWeight;
    @ApiModelProperty("拥有人")
    @TableField(exist = false)
    private String ownerName;
    @ApiModelProperty("合同编号")
    @TableField(exist = false)
    private String contractNos;
    @ApiModelProperty("运输类型")
    @TableField(exist = false)
    private String transportType;
    @TableField(exist = false)
    @ApiModelProperty("出运日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate voyageDate;
    @TableField(exist = false)
    @ApiModelProperty("货代公司名称|回显")
    private String forwarderCompanyName;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
