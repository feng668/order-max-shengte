package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.FilterOaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 核价单
* @TableName pr_pricing_order
*/
@ApiModel("核价单")
@TableName("pr_pricing_order")
@Data
public class PrPricingOrder extends FilterOaBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;

    /**
    * 核价单编号
    */
    @ApiModelProperty("核价单编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;
    /**
    * 交货日期
    */
    @ApiModelProperty("交货日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;
    /**
    * 客户id
    */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
    * 有效天数
    */
    @ApiModelProperty("有效天数")
    private Integer effectiveDays;
    /**
    * 结算币种
    */
    @ApiModelProperty("结算币种")
    private String currency;
    /**
    * 汇率
    */
    @ApiModelProperty("汇率")
    private Double rate;
    /**
    * 商品总金额
    */
    @ApiModelProperty("商品总金额")
    private Double saleAmo;
    /**
    * bom总成本
    */
    @ApiModelProperty("bom总成本")
    private Double costAmo;
    /**
    * 利润
    */
    @ApiModelProperty("利润")
    private BigDecimal profitAmo;


    @ApiModelProperty("利润率")
    private BigDecimal profitMargin;
    @ApiModelProperty("管理费用")
    private BigDecimal manageCost;
    @ApiModelProperty("利润值")
    private BigDecimal profitValue;
    @ApiModelProperty("其他费用")
    private BigDecimal otherCost;
    @ApiModelProperty("产品成本")
    private BigDecimal productionCost;
    @ApiModelProperty("市场价格")
    private BigDecimal marketPrice;
    @ApiModelProperty("市场价格外币")
    private BigDecimal marketPriceOther;
    @ApiModelProperty("出货公司")
    private String companyId;
    @ApiModelProperty("商品id")
    private String commodityId;
    @ApiModelProperty("商品商品")
    private String commodityNo;

    /**
    * 出货公司银行
    */
    @ApiModelProperty("出货公司银行")
    private String companyBank;
    /**
    * 客户联系人id
    */
    @ApiModelProperty("客户联系人id")
    private String customerContactsId;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
    * 拥有人id(业务员)
    */
    @ApiModelProperty("拥有人id(业务员)")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    /**
    * 商品总体积
    */
    @ApiModelProperty("商品总体积")
    private Double totalVolume;
    /**
    * 商品总数量
    */
    @ApiModelProperty("商品总数量")
    private BigDecimal totalNum;
    @ApiModelProperty("商品长度")
    private BigDecimal length;
    @ApiModelProperty("商品宽度")
    private BigDecimal width;
    @ApiModelProperty("商品高度")
    private BigDecimal highly;
    @ApiModelProperty("商品毛重")
    private BigDecimal grossWeight;
    @ApiModelProperty("商品净重")
    private BigDecimal netWeight;
    @ApiModelProperty("商品简称")
    private String miniName;
    @ApiModelProperty("商品中文名")
    private String cnName;
    @ApiModelProperty("商品英文名")
    private String enName;
    @ApiModelProperty("商品中文规格")
    private String specification;
    @ApiModelProperty("商品英文规格")
    private String enSpecification;
    @ApiModelProperty("商品计量单位商品计量单位")
    private String quantityUnit;
    /**
    * 截止日期
    */
    @ApiModelProperty("截止日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate expirationDate;
    /**
    * 付款方式
    */
    @ApiModelProperty("付款方式")
    private String paymentType;
    /**
    * 包装要求
    */
    @ApiModelProperty("包装要求")
    private String packagingRemark;
    /**
    * 来源
    */
    @ApiModelProperty("来源")
    private String billSource;
    /**
    * 客户要求
    */
    @ApiModelProperty("客户要求")
    private String custRequire;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("保存的核价单商品|入参")
    private List<PrPricingCommodity> commodityList;
    @TableField(exist = false)
    @ApiModelProperty("删除的核价单商品id数组|入参")
    private List<String> commodityDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("保存的核价单商品|入参")
    private List<PrPricingBoms> bomsList;
    @TableField(exist = false)
    @ApiModelProperty("删除的核价单商品id数组|入参")
    private List<String> bomsDelIdList;

    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("客户地址|回显")
    private String custAddress;
    @TableField(exist = false)
    @ApiModelProperty("客户电话|回显")
    private String custPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("客户开户行|回显")
    private String custBank;
    @TableField(exist = false)
    @ApiModelProperty("客户银行账号|回显")
    private String custBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("客户邮编|回显")
    private String custZipCode;
    @TableField(exist = false)
    @ApiModelProperty("客户传真|回显")
    private String custFax;
    @TableField(exist = false)
    @ApiModelProperty("客户税号|回显")
    private String custDutyNo;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司邮编|回显")
    private String cpnZipCode;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    private String searchInfo;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
