package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CuCustomer;
import com.whln.ordermax.data.domain.query.GetCustomerInfoQuery;
import com.whln.ordermax.data.domain.vo.EmailHistoryVO;
import com.whln.ordermax.data.domain.vo.EmailSendTypeVO;
import com.whln.ordermax.data.mapper.CuCustomerMapper;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 */
@Repository
public class CuCustomerRepository extends ServiceImpl<CuCustomerMapper, CuCustomer>{

    public IPage<CuCustomer> pageByEntity(CuCustomer entity, Page<CuCustomer> page) {
        IPage<CuCustomer> cuCustomerPage = baseMapper.listByEntity(entity, page);
        List<CuCustomer> records = cuCustomerPage.getRecords();
        Map<String, CuCustomer> collect = records.stream().collect(Collectors.toMap(CuCustomer::getId, Function.identity(), (x, y) -> x, LinkedHashMap::new));
        cuCustomerPage.setRecords(new ArrayList<>(collect.values()));
        return cuCustomerPage;
    }

    public List<CuCustomer> listByEntity(CuCustomer entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<CuCustomer> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }


    public List<Map> listCommodityByCustomerId(String id, String searchInfo, String billCode) {

      /* List<CustomerHistoryVO > q= qoQuoteCommodityRepository.getBaseMapper().getByCustomerId(id, searchInfo);
        Map<String, List<CustomerHistoryVO>> collect = q.stream().collect(Collectors.groupingBy(item -> {
            return item.getBillCode() + "," + item.getBillNo() + "," + item.getBillId();
        }));*/
        return baseMapper.listCommodityByCustomerId(id, searchInfo, billCode);
    }

    public List<String> listIdByNotConclude(Integer category, Integer status, Long notUpDay) {
        return baseMapper.listIdByNotConclude(category,status,notUpDay);
    }

    public Integer transferPubByIdList(List<String> customerIdList) {
        return baseMapper.update(
                null,
                new LambdaUpdateWrapper<CuCustomer>()
                        .set(CuCustomer::getIsPub,1)
                        .set(CuCustomer::getLastOwnerTime, LocalDateTime.now())
                        .in(CuCustomer::getId,customerIdList));
    }

    public List<String> queryPubCustomer() {
        return baseMapper.queryPubCustomer();
    }

    public List<EmailSendTypeVO> getCustomerInfo(GetCustomerInfoQuery getCustomerInfo) {
        return baseMapper.getCustomerInfo(getCustomerInfo);
    }

    public List<EmailHistoryVO> emailHistory(String costomerId) {
       return baseMapper.emailHistory(costomerId);
    }
}




