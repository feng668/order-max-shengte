package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.StWarehouse;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.StWarehouse
 */
public interface StWarehouseMapper extends BaseMapper<StWarehouse> {

    List<StWarehouse> listByEntity(@Param("entity")StWarehouse entity);

    IPage<StWarehouse> listByEntity(@Param("entity")StWarehouse entity, Page<StWarehouse> page);

    Integer insertBatch(@Param("entitylist")List<StWarehouse> entitylist);

}




