package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.PoInvoice;
import com.whln.ordermax.data.domain.PoInvoiceCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.PoInvoiceCommodityMapper;
import com.whln.ordermax.data.mapper.PoInvoiceMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class PoInvoiceRepository extends ServiceImpl<PoInvoiceMapper, PoInvoice>{

    public IPage<PoInvoice> pageByEntity(PoInvoice entity, Page<PoInvoice> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PoInvoice> listByEntity(PoInvoice entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PoInvoice> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    @Resource
    PoInvoiceCommodityMapper poInvoiceCommodityMapper;
    public PoInvoice getAllById(String id) {
        PoInvoice allById = baseMapper.getAllById(id);
        BillQuery param = new BillQuery();
        List<PoInvoiceCommodity> list = poInvoiceCommodityMapper.commodityList(param);
        allById.setCommodityList(list);
        param.setBillIds(CollUtils.toSet(allById.getId()));

        return allById;
    }

}




