package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PoRequest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PoRequest
 */
public interface PoRequestMapper extends BaseMapper<PoRequest> {

    List<PoRequest> listByEntity(@Param("entity")PoRequest entity);

    IPage<PoRequest> listByEntity(@Param("entity")PoRequest entity, Page<PoRequest> page);

    Integer insertBatch(@Param("entitylist")List<PoRequest> entitylist);

    PoRequest getAllById(@Param("id") String id);
}




