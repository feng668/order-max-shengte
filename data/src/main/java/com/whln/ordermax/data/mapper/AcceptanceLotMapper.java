package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.PdAcceptanceLot;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PdProductionLot
 */
public interface AcceptanceLotMapper extends BaseMapper<PdAcceptanceLot> {

    List<PdAcceptanceLot> listByEntity(@Param("entity") PdAcceptanceLot entity);

    IPage<PdAcceptanceLot> listByEntity(@Param("entity") PdAcceptanceLot entity, Page<PdAcceptanceLot> page);

    Integer insertBatch(@Param("entitylist")List<PdAcceptanceLot> entitylist);

}




