package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.PdProductionCommodityBom;
import com.whln.ordermax.data.domain.param.ProductionTaskBomParam;
import com.whln.ordermax.data.domain.query.BillQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PdProductionCommodityBom
 */
public interface PdProductionCommodityBomMapper extends BaseMapper<PdProductionCommodityBom> {

    List<PdProductionCommodityBom> listByEntity(@Param("entity")PdProductionCommodityBom entity);

    IPage<PdProductionCommodityBom> listByEntity(@Param("entity")PdProductionCommodityBom entity, Page<PdProductionCommodityBom> page);

    Integer insertBatch(@Param("entitylist")List<PdProductionCommodityBom> entitylist);

    List<PdProductionCommodityBom> listConcatCommodityInfo(BillQuery param);

    List<PdProductionCommodityBom> commodityList(BillQuery param);

    List<PdProductionCommodityBom> getProductionTaskBom(ProductionTaskBomParam param);
}




