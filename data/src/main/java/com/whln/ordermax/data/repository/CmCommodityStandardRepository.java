package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CmCommodityStandard;
import com.whln.ordermax.data.mapper.CmCommodityStandardMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class CmCommodityStandardRepository extends ServiceImpl<CmCommodityStandardMapper, CmCommodityStandard>{

    public IPage<CmCommodityStandard> pageByEntity(CmCommodityStandard entity, Page<CmCommodityStandard> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<CmCommodityStandard> listByEntity(CmCommodityStandard entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<CmCommodityStandard> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




