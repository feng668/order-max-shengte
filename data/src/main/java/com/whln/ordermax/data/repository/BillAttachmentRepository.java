package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.BillAttachment;
import com.whln.ordermax.data.mapper.BillAttachmentMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class BillAttachmentRepository extends ServiceImpl<BillAttachmentMapper, BillAttachment>{

    public IPage<BillAttachment> pageByEntity(BillAttachment entity, Page<BillAttachment> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<BillAttachment> listByEntity(BillAttachment entity) {
        return baseMapper.listByEntity(entity);
    }

    public BillAttachment getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<BillAttachment> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




