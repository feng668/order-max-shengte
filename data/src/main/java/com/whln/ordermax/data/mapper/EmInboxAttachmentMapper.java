package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.EmInboxAttachment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EmInboxAttachment
 */
public interface EmInboxAttachmentMapper extends BaseMapper<EmInboxAttachment> {

    List<EmInboxAttachment> listByEntity(@Param("entity")EmInboxAttachment entity);

    IPage<EmInboxAttachment> listByEntity(@Param("entity")EmInboxAttachment entity, Page<EmInboxAttachment> page);

    Integer insertBatch(@Param("entitylist")List<EmInboxAttachment> entitylist);

}




