package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-20 21:16
 */
@ApiModel("商品交易历史参数")
@Data
public class TransactionHistoryParam {
    private String id;
    private Integer current = 1;
    private Integer size = 10;
}
