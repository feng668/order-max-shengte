package com.whln.ordermax.data.domain.param;

import com.whln.ordermax.data.domain.vo.QoQuoteCommodityVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-27 14:46
 */
@ApiModel("报价单商品导入")
@Data
public class QoQuoteCommodityVOImportParam {
    @ApiModelProperty("报价单id")
    private String billId;
    @ApiModelProperty("报价单商品")
    List<QoQuoteCommodityVO> cmCommodityList;
}
