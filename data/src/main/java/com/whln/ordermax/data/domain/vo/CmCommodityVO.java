package com.whln.ordermax.data.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-22 15:07
 */
@Data
public class CmCommodityVO {
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 分类id
     */
    @ApiModelProperty("分类id")
    private String categoryId;
    /**
     * 商品编号
     */
    @ApiModelProperty("商品编号")
    private String partNo;
    @ApiModelProperty("单箱数量")
    private BigDecimal singleBoxNum;

    /**
     * 商品中文名称
     */
    @ApiModelProperty("商品中文名称")
    private String cnName;
    @ApiModelProperty("规格配比")
    private String ratio;


    @ApiModelProperty("简称")
    private String miniName;

    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    private Integer contactTax;

    @NotNull(message = "元素不能为空")
    @ApiModelProperty("元素")
    private String elements;
    @NotNull(message = "纯度不能为空")
    @ApiModelProperty("纯度")
    private String purity;

    /**
     * 商品英文名
     */
    @ApiModelProperty("商品英文名")
    private String enName;
    /**
     * 报关代码id
     */
    @ApiModelProperty("报关代码id")
    private String customsCodeId;
    /**
     * 商品规格
     */
    @ApiModelProperty("商品规格")
    private String specification;
    /**
     * 商品英文规格
     */
    @ApiModelProperty("英文规格")
    private String enSpecification;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    /**
     * 净重
     */
    @ApiModelProperty("净重")
    private BigDecimal netWeight;
    /**
     * 毛重
     */
    @ApiModelProperty("毛重")
    private BigDecimal grossWeight;
    /**
     * 长度
     */
    @ApiModelProperty("长度")
    private BigDecimal length;
    /**
     * 宽度
     */
    @ApiModelProperty("宽度")
    private BigDecimal width;
    @ApiModelProperty("仓库名")
    private String warehouseName;
    @ApiModelProperty("仓库编号")
    private String warehouseNo;
    @ApiModelProperty("仓库id")
    private String warehouseId;
    /**
     * 高度
     */
    @ApiModelProperty("高度")
    private BigDecimal highly;

    /**
     * 采购数量
     */
    @ApiModelProperty("入库数量")
    private BigDecimal quotationNum;
    @ApiModelProperty("单箱净重")
    private BigDecimal singleBoxNet;
    /**
     * 单箱毛重
     */
    @ApiModelProperty("单箱毛重")
    private BigDecimal singleBoxWeight;
    @ApiModelProperty("批号")
    private String commodityBatchNo;
    /**
     * 单箱体积
     */
    @ApiModelProperty("单箱体积")
    private BigDecimal singleBoxVolume;
    /**
     * 计量单位
     */
    @ApiModelProperty("计量单位")
    private String quantityUnit;
    /**
     * 包装要求
     */
    @ApiModelProperty("包装要求")
    private String packagingRemark;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 默认采购价
     */
    @ApiModelProperty("默认采购价")
    private BigDecimal purchasePrice;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 默认供应商id
     */
    @ApiModelProperty("默认供应商id")
    private String defaultSupplierId;
    /**
     * 销售价
     */
    @ApiModelProperty("销售价")
    private BigDecimal salePrice;
    /**
     * 物件类别(商品=COMMODITY，配件=PART)
     */
    @ApiModelProperty("物件类别(商品=COMMODITY，配件=PART)")
    private String articleType;
    @ApiModelProperty("参考价格1")
    private String costPrice1;
    @ApiModelProperty("参考价格2")
    private String costPrice2;
    @ApiModelProperty("参考价格3")
   private String costPrice3;
    @ApiModelProperty("参考价格4")
    private String costPrice4;
    @ApiModelProperty("参考价格5")
    private String costPrice5;

    @ApiModelProperty("是否重复， 0 否 1是")
    private Integer repeat;


    @ApiModelProperty("即时库存")
    List<BaseInfoVO> baseInfoVOList;
    @ApiModelProperty("库存明细")
    List<StInventoryVO> stInventoryVOList;

}
