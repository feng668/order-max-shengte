package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
* 大客户指标
* @TableName cu_big_customer_norm
*/
@ApiModel("大客户指标")
@TableName("cu_big_customer_norm")
@Data
public class CuBigCustomerNorm implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 销售额
    */
    @ApiModelProperty("销售额")
    private BigDecimal saleAmo;
    /**
    * 利润
    */
    @ApiModelProperty("利润")
    private BigDecimal saleProfit;
    /**
    * 预计销售额
    */
    @ApiModelProperty("预计销售额")
    private BigDecimal futureSaleAmo;
    /**
    * 预计利润
    */
    @ApiModelProperty("预计利润")
    private BigDecimal futureSaleProfit;
    /**
    * 行业影响力
    */
    @ApiModelProperty("行业影响力")
    private Integer industryForce;
    /**
    * 预计赢单率
    */
    @ApiModelProperty("预计赢单率")
    private BigDecimal futureWinRatio;
    /**
    * 是否激活
    */
    @ApiModelProperty("是否激活")
    private Integer isEnable;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    private Date updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
