package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.FnPayment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.FnPayment
 */
public interface FnPaymentMapper extends BaseMapper<FnPayment> {

    List<FnPayment> listByEntity(@Param("entity")FnPayment entity);

    IPage<FnPayment> listByEntity(@Param("entity")FnPayment entity, Page<FnPayment> page);

    FnPayment getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<FnPayment> entitylist);

}




