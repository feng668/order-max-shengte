package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.QoInQuoteCommodity;
import com.whln.ordermax.data.domain.QoInQuoteOrder;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.QoInQuoteCommodityMapper;
import com.whln.ordermax.data.mapper.QoInQuoteOrderMapper;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Repository
@AllArgsConstructor
public class QoInQuoteOrderRepository extends ServiceImpl<QoInQuoteOrderMapper, QoInQuoteOrder> {
    private final QoInQuoteCommodityMapper
            qoInQuoteCommodityMapper;


    public IPage<QoInQuoteOrder> pageByEntity(QoInQuoteOrder entity, Page<QoInQuoteOrder> page) {
        IPage<QoInQuoteOrder> qoInQuoteOrderIPage = baseMapper.listByEntity(entity, page);
        List<QoInQuoteOrder> records = qoInQuoteOrderIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> recordIds = records.stream().map(QoInQuoteOrder::getId).collect(Collectors.toSet());
            List<QoInQuoteCommodity> list = qoInQuoteCommodityMapper.commodityList(BillQuery.builder()
                    .billIds(recordIds)
                    .build());
            Map<String, List<QoInQuoteCommodity>> groupByBillId =
                    list.stream().collect(Collectors.groupingBy(QoInQuoteCommodity::getBillId));
            List<QoInQuoteOrder> resultRecords = records.stream().peek(item -> {
                item.setCommodityList(groupByBillId.get(item.getId()));
            }).collect(Collectors.toList());

            qoInQuoteOrderIPage.setRecords(resultRecords);
        }

        return qoInQuoteOrderIPage;
    }


    public List<QoInQuoteOrder> listByEntity(QoInQuoteOrder entity) {
        List<QoInQuoteOrder> qoInQuoteOrderIPage = baseMapper.listByEntity(entity);
        Set<String> recordIds = qoInQuoteOrderIPage.stream().map(QoInQuoteOrder::getId).collect(Collectors.toSet());
        List<QoInQuoteCommodity> list = qoInQuoteCommodityMapper.commodityList(BillQuery.builder()
                .billIds(recordIds)
                .build());
        Map<String, List<QoInQuoteCommodity>> groupByBillId =
                list.stream().collect(Collectors.groupingBy(QoInQuoteCommodity::getBillId));

        return qoInQuoteOrderIPage.stream().peek(item -> {
            item.setCommodityList(groupByBillId.get(item.getId()));
        }).collect(Collectors.toList());
    }


    public Boolean insertBatch(List<QoInQuoteOrder> entityList) {
        Boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }

    public QoInQuoteOrder getByIdWithInfo(String queteId) {
        return baseMapper.getByIdWithInfo(queteId);
    }


    public QoInQuoteOrder getAllById(String id) {
        QoInQuoteOrder qoInQuoteOrder = baseMapper.getByIdWithInfo(id);
        List<QoInQuoteCommodity> list = qoInQuoteCommodityMapper.commodityList(BillQuery.builder().billIds(CollUtils.toSet(qoInQuoteOrder.getId())).build());
        qoInQuoteOrder.setCommodityList(list);
        return qoInQuoteOrder;
    }

}




