package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 角色
 *
 * @TableName sys_role
 */
@ApiModel("角色")
@TableName(value = "sys_role")
@Data
public class SysRole implements Serializable {

    /**
     * 编号
     */
    @TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty("编号")
    private String id;
    private String parentId;
    /**
     * 角色名称
     */
    @ApiModelProperty("角色名称")
    private String name;
    /**
     * 英文名称
     */
    @ApiModelProperty("英文名称")
    @NotBlank(message = "角色的code不能为空")
    private String code;
    /**
     * 创建时间
     */
    private LocalDateTime createDate;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private LocalDateTime updateDate;
    /**
     * 备注信息
     */
    @ApiModelProperty("备注信息")
    private String remarks;
    @TableField(exist = false)
    private List<String> userIds;
    @ApiModelProperty("权限id集合")
    @TableField(exist = false)
    private List<String> powerIds;
    @TableField(exist = false)
    private String searchInfo;
    @TableField(exist = false)
    private List<SysUser> users;
    @TableField(exist = false)
    private List<SysRole> children;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
