package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
* 用户部门
* @TableName sys_department
*/
@ApiModel("用户部门")
@TableName("sys_department")
@Data
public class SysDepartment implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 
    */
    @ApiModelProperty("")
    private String parentId;
    /**
    * 
    */
    @ApiModelProperty("")
    private String parentIds;
    /**
    * 公司id
    */
    @ApiModelProperty("公司id")
    private String sysCompanyId;
    /**
    * 部门代码
    */
    @ApiModelProperty("部门代码")
    private String code;
    /**
    * 部门名称
    */
    @ApiModelProperty("部门名称")
    private String dptName;
    /**
    * 上级部门名称
    */
    @ApiModelProperty("上级部门名称")
    private String parentDptNames;
    /**
    * 备注
    */
    @ApiModelProperty("备注")
    private String note;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;


    @TableField(exist = false)
    private List<SysDepartment> chirds;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
