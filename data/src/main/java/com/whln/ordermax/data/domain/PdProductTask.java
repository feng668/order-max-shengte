package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 生产任务单
 *
 * @TableName pd_product_task
 */
@ApiModel("生产任务单")
@TableName("pd_product_task")
@Data
public class PdProductTask implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    /**
     * 任务单编号
     */
    @ApiModelProperty("任务单编号")
    private String billNo;
    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private String createrId;
    /**
     * 创建日期
     */
    @ApiModelProperty("创建日期")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
     * 排产人员id
     */
    @ApiModelProperty("排产人员id")
    private String producterId;
    /**
     * 生产部门id
     */
    @ApiModelProperty("生产部门id")
    private String productDepartmentId;
    /**
     * 生产总数量
     */
    @ApiModelProperty("生产总数量")
    private Integer totalNum;
    /**
     * 生产总体积
     */
    @ApiModelProperty("生产总体积")
    private Double totalVolume;
    /**
     * 合同总净重
     */
    @ApiModelProperty("合同总净重")
    private Double totalNetWeight;
    /**
     * 生产要求
     */
    @ApiModelProperty("生产要求")
    private String productionRequested;
    /**
     * 包装要求
     */
    @ApiModelProperty("包装要求")
    private String packagingRequested;
    /**
     * 交付要求
     */
    @ApiModelProperty("交付要求")
    private String deliveryRequested;
    /**
     * 验收标准
     */
    @ApiModelProperty("验收标准")
    private String acceptanceStandard;
    /**
     * 客户id
     */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
     * 公司id
     */
    @ApiModelProperty("公司id")
    private String companyId;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("保存的任务单商品|入参")
    private List<PdProductTaskCommodity> commodityList;
    @TableField(exist = false)
    @ApiModelProperty("删除的任务单商品id数组|入参")
    private List<String> commodityDelIdList;


    @TableField(exist = false)
    @ApiModelProperty("排产人员姓名|回显")
    private String producterName;
    @TableField(exist = false)
    @ApiModelProperty("生产部门名称|回显")
    private String productDepartmentName;
    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("客户地址|回显")
    private String custAddress;
    @TableField(exist = false)
    @ApiModelProperty("客户电话|回显")

    private String custPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("客户开户行|回显")
    private String custBank;
    @TableField(exist = false)
    @ApiModelProperty("客户银行账号|回显")
    private String custBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("创建人姓名|回显")
    private String createrName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
