package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.TrInDispatchCommodity;
import com.whln.ordermax.data.mapper.TrInDispatchCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class TrInDispatchCommodityRepository extends ServiceImpl<TrInDispatchCommodityMapper, TrInDispatchCommodity>{

    public IPage<TrInDispatchCommodity> pageByEntity(TrInDispatchCommodity entity, Page<TrInDispatchCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<TrInDispatchCommodity> listByEntity(TrInDispatchCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<TrInDispatchCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




