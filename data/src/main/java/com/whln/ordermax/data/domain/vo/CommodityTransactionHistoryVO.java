package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-03-20 14:22
 */
@ApiModel("商品交易历史")
@Data
public class CommodityTransactionHistoryVO {
    private String id;
    private String billNo;
    private BigDecimal price;
    private BigDecimal quotationNum;
    private LocalDateTime time;
    private String cnName;
    private String enName;
    private String specification;
    private String enSpecification;
    private Integer contactTax;
    private String elements;
    private String purity;
    private String ratio;
    private BigDecimal taxAmo;
    private BigDecimal amo;
    private String realName;
    private String note;
    private String billName;
    private String billCode;
    private String partNo;
    private String quantityUnit;

}
