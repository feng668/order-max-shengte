package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.QoQuoteCommodityPrice;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.QoQuoteCommodityPrice
 */
public interface QoQuoteCommodityPriceMapper extends BaseMapper<QoQuoteCommodityPrice> {

    List<QoQuoteCommodityPrice> listByEntity(@Param("entity")QoQuoteCommodityPrice entity);

    IPage<QoQuoteCommodityPrice> listByEntity(@Param("entity")QoQuoteCommodityPrice entity, Page<QoQuoteCommodityPrice> page);

    Integer insertBatch(@Param("entitylist")List<QoQuoteCommodityPrice> entitylist);

    List<QoQuoteCommodityPrice> listNewPriceByComIdAndCustId(@Param("commodityId")String commodityId, @Param("customerId")String customerId);
}




