package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.SoSiInvoice;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SoSiOrder
 */
public interface SoSiInvoiceMapper extends BaseMapper<SoSiInvoice> {

    List<SoSiInvoice> listByEntity(@Param("entity")SoSiInvoice entity);

    IPage<SoSiInvoice> listByEntity(@Param("entity")SoSiInvoice entity, Page<SoSiInvoice> page);

    Integer insertBatch(@Param("entitylist")List<SoSiInvoice> entitylist);

    SoSiInvoice getSoSiInvoice(String id);
}




