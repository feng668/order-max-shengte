package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whln.ordermax.data.domain.EmailSign;

/**
 * @author liurun
 * @date 2023-02-21 18:46
 */
public interface EmailSignMapper extends BaseMapper<EmailSign> {
}
