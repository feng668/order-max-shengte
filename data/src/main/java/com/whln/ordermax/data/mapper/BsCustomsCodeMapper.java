package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.BsCustomsCode;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.BsCustomsCode
 */
public interface BsCustomsCodeMapper extends BaseMapper<BsCustomsCode> {

    List<BsCustomsCode> listByEntity(@Param("entity")BsCustomsCode entity);

    IPage<BsCustomsCode> listByEntity(@Param("entity")BsCustomsCode entity, Page<BsCustomsCode> page);

    Integer insertBatch(@Param("entitylist")List<BsCustomsCode> entitylist);

}




