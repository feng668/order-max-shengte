package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.SysUserRole;
import com.whln.ordermax.data.mapper.SysUserRoleMapper;

import java.util.List;

/**
 *
 */
@Repository
public class SysUserRoleRepository extends ServiceImpl<SysUserRoleMapper, SysUserRole>{

    public Boolean insertBatch(List<SysUserRole> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




