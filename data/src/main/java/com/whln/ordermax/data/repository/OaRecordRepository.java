package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.OaRecord;
import com.whln.ordermax.data.mapper.OaRecordMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class OaRecordRepository extends ServiceImpl<OaRecordMapper, OaRecord>{

    public IPage<OaRecord> pageByEntity(OaRecord entity, Page<OaRecord> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<OaRecord> listByEntity(OaRecord entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<OaRecord> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




