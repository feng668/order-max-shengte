package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.StInOrder;
import com.whln.ordermax.data.domain.StInOrderCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.StInOrderCommodityMapper;
import com.whln.ordermax.data.mapper.StInOrderMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class StInOrderRepository extends ServiceImpl<StInOrderMapper, StInOrder>{

    public IPage<StInOrder> pageByEntity(StInOrder entity, Page<StInOrder> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<StInOrder> listByEntity(StInOrder entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<StInOrder> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    @Resource
    StInOrderCommodityMapper stInOrderCommodityMapper;
    public StInOrder getAllById(String id) {
        StInOrder stInOrder = baseMapper.selectById(id);
        List<StInOrderCommodity> stInOrderCommodities = stInOrderCommodityMapper
                .commodityList(BillQuery.builder()
                        .billIds(CollUtils.toSet(stInOrder.getId()))
                        .build());
        stInOrder.setCommodityList(stInOrderCommodities);
        return stInOrder;

    }
}




