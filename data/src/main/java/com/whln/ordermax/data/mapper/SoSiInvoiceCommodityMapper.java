package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.SoSiInvoiceCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SoSiOrderCommodity
 */
public interface SoSiInvoiceCommodityMapper extends BaseMapper<SoSiInvoiceCommodity> {

    List<SoSiInvoiceCommodity> listByEntity(@Param("entity")SoSiInvoiceCommodity entity);

    IPage<SoSiInvoiceCommodity> listByEntity(@Param("entity")SoSiInvoiceCommodity entity, Page<SoSiInvoiceCommodity> page);

    Integer insertBatch(@Param("entitylist")List<SoSiInvoiceCommodity> entitylist);

    List<SoSiInvoiceCommodity> commodityList(BillQuery billIds);
}




