package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
* 用户公司
* @TableName sys_company
*/
@ApiModel("用户公司")
@TableName("sys_company")
@Data
public class SysCompany implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    /**
     * 公司代码
     */
    @ApiModelProperty("公司代码")
    private String code;
    /**
     * 公司名称
     */
    @ApiModelProperty("公司名称")
    private String cnName;
    /**
     * 公司英文名
     */
    @ApiModelProperty("公司英文名")
    private String enName;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 开户银行
     */
    @ApiModelProperty("开户银行")
    private String bank;
    /**
     * 银行账号
     */
    @ApiModelProperty("银行账号")
    @Pattern(regexp = "[1-9]")
    private String bankAccount;
    @ApiModelProperty("银行行号")
    private String bankNo;
    @ApiModelProperty("银行地址")
    private String bankAddress;
    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String address;
    /**
     * 电话
     */
    @ApiModelProperty("电话")
    @Pattern(regexp = "[1-9]")
    private String phoneNum;
    /**
     * 邮编
     */
    @ApiModelProperty("邮编")
    private String zipCode;
    @ApiModelProperty("SWIFT")
    private String swift;
    @TableField(exist = false)
    private String searchInfo;


    /* 附加字段 */
    @TableField(exist = false)
    @ApiModelProperty("删除的部门id数组|入参")
    private List<String> departmentDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("部门数组|入参")
    private List<SysDepartment> departmentList;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
