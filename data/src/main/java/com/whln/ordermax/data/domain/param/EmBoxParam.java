package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-17 15:10
 */
@ApiModel("邮件参数")
@Data
public class EmBoxParam {
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("类型1， 收件箱 2 发件箱")
    private Integer type;
}
