package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.SysRole;
import com.whln.ordermax.data.mapper.SysRoleMapper;

import java.util.List;

/**
 *
 */
@Repository
public class SysRoleRepository extends ServiceImpl<SysRoleMapper, SysRole> {

    public List<SysRole> listByEntity(SysRole entity) {
        return baseMapper.listByEntity(entity);
    }
    public IPage<SysRole> listByEntity(SysRole entity, Page<SysRole> page) {
        return baseMapper.listByEntity(entity, page);
    }
    public List<SysRole> listRolesByUserId(String userId) {
        return baseMapper.listRolesByUserId(userId);
    }

}




