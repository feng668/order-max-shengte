package com.whln.ordermax.data.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author liurun
 * @date 2023-02-27 15:30
 */
@ApiModel("即时库存")
@Data
public class BaseInfoVO implements Serializable {
    private static final long serialVersionUID = 2724302294190412411L;
    @ApiModelProperty("仓库名")
    private String warehouseName;
    @ApiModelProperty("仓库编号")
    private String warehouseNo;
    @ApiModelProperty("仓库id")
    private String warehouseId;
    @ApiModelProperty("商品id")
    private String commodityId;
    @ApiModelProperty("商品中文名")
    private String cnName;
    private String enName;
    private String partNo;
    @ApiModelProperty("计量单位")
    private String quantityUnit;
    @ApiModelProperty("中文规格")
    private String specification;
    @ApiModelProperty("英文规格")
    private String enSpecification;
    @ApiModelProperty("创建时间")
    private String createTime;

    @ApiModelProperty("占用数量")
    private BigDecimal grabNum;
    @ApiModelProperty("空闲数量")
    private BigDecimal freeNum;
    private String supplierId;
    @ApiModelProperty("仓库总数量")
    private BigDecimal num;
    @ApiModelProperty("类型")
    private Integer type;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate time;
    @ApiModelProperty("批号")
    private String commodityBatchNo;


}
