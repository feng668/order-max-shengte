package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.TrInDispatchCommodity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.query.BillQuery;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.TrInDispatchCommodity
 */
public interface TrInDispatchCommodityMapper extends BaseMapper<TrInDispatchCommodity> {

    List<TrInDispatchCommodity> listByEntity(@Param("entity")TrInDispatchCommodity entity);

    IPage<TrInDispatchCommodity> listByEntity(@Param("entity")TrInDispatchCommodity entity, Page<TrInDispatchCommodity> page);

    Integer insertBatch(@Param("entitylist")List<TrInDispatchCommodity> entitylist);

    List<TrInDispatchCommodity> commodityList(BillQuery build);
}




