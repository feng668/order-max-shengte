package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.PdQualityLot;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PdProductionLot
 */
public interface PdQualityLotMapper extends BaseMapper<PdQualityLot> {

    List<PdQualityLot> listByEntity(@Param("entity")PdQualityLot entity);

    IPage<PdQualityLot> listByEntity(@Param("entity")PdQualityLot entity, Page<PdQualityLot> page);

    Integer insertBatch(@Param("entitylist")List<PdQualityLot> entitylist);

}




