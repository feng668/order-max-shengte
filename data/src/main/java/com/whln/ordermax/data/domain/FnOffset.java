package com.whln.ordermax.data.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
* 收款核销
* @TableName fn_offset
*/
@ApiModel("收款核销")
@TableName("fn_offset")
@Data
public class FnOffset implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 核销单号
    */
    @ApiModelProperty("核销单号")
    private String billNo;
    /**
    * 入账id
    */
    @ApiModelProperty("入账id")
    private String inAccountId;
    /**
    * 销售订单id
    */
    @ApiModelProperty("销售订单id")
    private String soOrderId;
    /**
    * 核销人员
    */
    @ApiModelProperty("核销人员")
    private String offseterId;
    /**
    * 付款银行
    */
    @ApiModelProperty("付款银行")
    private String customerBank;
    /**
    * 付款账号
    */
    @ApiModelProperty("付款账号")
    private String customerBankAccount;
    /**
    * 付款单位id(客户id)
    */
    @ApiModelProperty("付款单位id(客户id)")
    private String customerId;
    /**
    * 来源
    */
    @ApiModelProperty("来源")
    private String source;
    /**
    * 核销日期
    */
    @ApiModelProperty("核销日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate offsetDate;
    /**
    * 核销金额合计
    */
    @ApiModelProperty("核销金额合计")
    private Double totalAmo;
    /**
    * 入账币种
    */
    @ApiModelProperty("入账币种")
    private String currency;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;


    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("入账登记单号|回显")
    private String accountNo;
    @TableField(exist = false)
    @ApiModelProperty("销售订单编号|回显")
    private String soOrderNo;
    @TableField(exist = false)
    @ApiModelProperty("核销人员姓名|回显")
    private String offseterName;

    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
