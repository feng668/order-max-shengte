package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.SysDepartment;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysDepartment
 */
public interface SysDepartmentMapper extends BaseMapper<SysDepartment> {

    List<SysDepartment> listByEntity(@Param("entity")SysDepartment entity);

    IPage<SysDepartment> listByEntity(@Param("entity")SysDepartment entity, Page<SysDepartment> page);

    Integer insertBatch(@Param("entitylist")List<SysDepartment> entitylist);

}




