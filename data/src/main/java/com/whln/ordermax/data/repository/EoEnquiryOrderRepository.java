package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.EoEnquiryOrder;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.EoEnquiryCommodityMapper;
import com.whln.ordermax.data.mapper.EoEnquiryOrderMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class EoEnquiryOrderRepository extends ServiceImpl<EoEnquiryOrderMapper, EoEnquiryOrder>{

    public IPage<EoEnquiryOrder> pageByEntity(EoEnquiryOrder entity, Page<EoEnquiryOrder> page) {
        IPage<EoEnquiryOrder> eoEnquiryOrderIPage = baseMapper.listByEntity(entity, page);
        return eoEnquiryOrderIPage;
    }

    public List<EoEnquiryOrder> listByEntity(EoEnquiryOrder entity) {
        return baseMapper.listByEntity(entity);
    }

    @Resource
    private EoEnquiryCommodityMapper eoEnquiryCommodityMapper;
    public EoEnquiryOrder getAllById(String id) {
        EoEnquiryOrder allById = baseMapper.getAllById(id);
        eoEnquiryCommodityMapper.list(BillQuery.builder().billIds(CollUtils.toSet(allById.getId())).build());
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<EoEnquiryOrder> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




