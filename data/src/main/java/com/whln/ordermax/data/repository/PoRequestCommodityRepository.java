package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PoRequestCommodity;
import com.whln.ordermax.data.mapper.PoRequestCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class PoRequestCommodityRepository extends ServiceImpl<PoRequestCommodityMapper, PoRequestCommodity>{

    public IPage<PoRequestCommodity> pageByEntity(PoRequestCommodity entity, Page<PoRequestCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PoRequestCommodity> listByEntity(PoRequestCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PoRequestCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




