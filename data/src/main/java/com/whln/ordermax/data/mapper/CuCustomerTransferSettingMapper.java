package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.CuCustomerTransferSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CuCustomerTransferSetting
 */
public interface CuCustomerTransferSettingMapper extends BaseMapper<CuCustomerTransferSetting> {

    List<CuCustomerTransferSetting> listByEntity(@Param("entity")CuCustomerTransferSetting entity);

    IPage<CuCustomerTransferSetting> listByEntity(@Param("entity")CuCustomerTransferSetting entity, Page<CuCustomerTransferSetting> page);

    CuCustomerTransferSetting getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<CuCustomerTransferSetting> entitylist);

}




