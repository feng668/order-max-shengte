package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* 已付款单据
* @TableName fn_payment_bill
*/
@ApiModel("已付款单据")
@TableName("fn_payment_bill")
@Data
public class FnPaymentBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 付款单id
    */
    @ApiModelProperty("付款单id")
    private String paymentId;
    /**
    * 已付款单据id
    */
    @ApiModelProperty("已付款单据id")
    private String billId;


    /* 附加信息 */

    @TableField(exist = false)
    @ApiModelProperty("合同编号")
    private String contractNo;
    @TableField(exist = false)
    @ApiModelProperty("供应商id")
    private String supplierId;
    @TableField(exist = false)
    @ApiModelProperty("公司id")
    private String companyId;
    @TableField(exist = false)
    @ApiModelProperty("公司人员id")
    private String companyPersonnelId;
    @TableField(exist = false)
    @ApiModelProperty("创建人id")
    private String createrId;
    @TableField(exist = false)
    @ApiModelProperty("创建日期|回显")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    @TableField(exist = false)
    @ApiModelProperty("客户订单|回显")
    private String customerOrderNo;
    @TableField(exist = false)
    @ApiModelProperty("合同总金额（本币）|回显")
    private Double localCurTotalAmo;
    @TableField(exist = false)
    @ApiModelProperty("付款方式|回显")
    private String payType;
    @TableField(exist = false)
    @ApiModelProperty("合同总数量|回显")
    private Integer totalNum;
    @TableField(exist = false)
    @ApiModelProperty("合同总体积|回显")
    private Double totalVolume;
    @TableField(exist = false)
    @ApiModelProperty("备注|回显")
    private String note;
    @TableField(exist = false)
    @ApiModelProperty("生产要求|回显")
    private String productionRequested;
    @TableField(exist = false)
    @ApiModelProperty("包装要求|回显")
    private String packagingRequested;
    @TableField(exist = false)
    @ApiModelProperty("交付要求|回显")
    private String deliveryRequested;
    @TableField(exist = false)
    @ApiModelProperty("验收标准|回显")
    private String acceptanceStandard;
    @TableField(exist = false)
    @ApiModelProperty("创建时间|回显")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @TableField(exist = false)
    @ApiModelProperty("更新时间|回显")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    @TableField(exist = false)
    @ApiModelProperty("拥有人id|回显")
    private String ownerId;
    @TableField(exist = false)
    @ApiModelProperty("送货地址|回显")
    private String address;
    @TableField(exist = false)
    @ApiModelProperty("供应商中文名称|回显")
    private String supName;
    @TableField(exist = false)
    @ApiModelProperty("供应商地址|回显")
    private String supAddress;
    @TableField(exist = false)
    @ApiModelProperty("供应商电话|回显")
    private String supPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("供应商开户行|回显")
    private String supBank;
    @TableField(exist = false)
    @ApiModelProperty("供应商银行账号|回显")
    private String supBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("创建人姓名|回显")
    private String createrName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
