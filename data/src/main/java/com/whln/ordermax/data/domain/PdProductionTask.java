package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.OaBill;
import com.whln.ordermax.data.domain.param.ProductionTaskBomParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 生产任务单
* @TableName pd_production_task
*/
@ApiModel("生产任务单")
@TableName("pd_production_task")
@Data
public class PdProductionTask extends OaBill  implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */

    private String id;
    private Integer status;
    @ApiModelProperty("是否删除 0未删除 1已删除")
    private Integer isDelete;

    private String parentBillId;
    
    private String parentBillCode;
    
    private String parentBillNo;

    @ApiModelProperty("批号")
    private String commodityBatchNo;
    /**
    * 任务单编号
    */
    @ApiModelProperty("任务单编号")
    private String billNo;
    /**
    * 销售合同id
    */
    @ApiModelProperty("预计完成时间")
    private LocalDateTime estimateDate;

    @ApiModelProperty("销售合同id")
    private String sourceBillId;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;
    /**
    * 客户id
    */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
    * 公司id
    */
    @ApiModelProperty("公司id")
    private String companyId;
    /**
    * 创建人
    */
    @ApiModelProperty("创建人")
    private String createrId;
    private Integer flowState;
    /**
    * 生产日期
    */
    @ApiModelProperty("生产日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate prodDate;
    /**
    * 排产人员id
    */
    @ApiModelProperty("排产人员id")
    private String producterId;
    /**
    * 生产部门id
    */
    @ApiModelProperty("生产部门id")
    private String productDepartmentId;
    /**
    * 生产要求
    */
    @ApiModelProperty("生产要求")
    private String productionRequested;
    /**
    * 包装要求
    */
    @ApiModelProperty("包装要求")
    private String packagingRequested;
    /**
    * 交付要求
    */
    @ApiModelProperty("交付要求")
    private String deliveryRequested;
    /**
    * 验收标准
    */
    @ApiModelProperty("验收标准")
    private String acceptanceStandard;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    /**
    * 商品id
    */
    @ApiModelProperty("商品id")
    private String commodityId;
//    private Integer flowStatus;
    /**
    * 单箱数量
    */
    @ApiModelProperty("单箱数量")
    private Integer singleBoxNum;
    /**
    * 箱数
    */
    @ApiModelProperty("箱数")
    private BigDecimal containerNum;
    /**
    * 生产数量
    */
    @ApiModelProperty("生产数量")
    private BigDecimal productNum;
    /**
    * 单箱净重
    */
    @ApiModelProperty("单箱净重")
    private BigDecimal singleBoxNet;


    /**
    * 单箱毛重
    */
    @ApiModelProperty("单箱毛重")
    private BigDecimal singleBoxWeight;
    /**
    * 单箱体积
    */
    @ApiModelProperty("单箱体积")
    private BigDecimal singleBoxVolume;
    /**
    * 炉数
    */
    @ApiModelProperty("炉数")
    private BigDecimal furnaceNum;
    /**
    * 瓶数
    */
    @ApiModelProperty("瓶数")
    private BigDecimal bottNum;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    @ApiModelProperty("预计完工日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate predictFinishDate;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;
    @TableField(exist = false)
    private List<String> ownerIds;
    @TableField(exist = false)
    @ApiModelProperty("保存的批号|入参")
    private List<PdProductionLot> lotList;
    @TableField(exist = false)
    @ApiModelProperty("保存的BOM|入参")
    private List<PdProductionCommodityBom> bomList;
    @TableField(exist = false)
    private List<PdProductionCommodityBom> commodityList;
    @TableField(exist = false)
    private List<ProductionTaskBomParam> pdProductionCommodityBomList;

    @TableField(exist = false)
    @ApiModelProperty("流转任务单商品id|入参")
    private String commodityFlowId;

    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private Double cmNetWeight;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;
    @TableField(exist = false)
    @ApiModelProperty("商品体积|回显")
    private Double cmVolume;
    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("排产人员姓名|回显")
    private String producterName;


    @ApiModelProperty("简称")
    @TableField(exist = false)
    private String miniName;

    @TableField(exist = false)
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    private Integer contactTax;

    @TableField(exist = false)
    @NotNull(message = "元素不能为空")
    @ApiModelProperty("元素")
    private String elements;
    @TableField(exist = false)
    @ApiModelProperty("规格配比")
    private String ratio;

    @TableField(exist = false)
    @NotNull(message = "纯度不能为空")
    @ApiModelProperty("纯度")
    private String purity;
    @ApiModelProperty("配件总数量")
    @TableField(exist = false)
    private BigDecimal quotationNum;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
