package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.FilterOaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 出库单
 *
 * @TableName st_out_order
 */
@ApiModel("出库单")
@TableName("st_out_order")
@Data
public class StOutOrder extends FilterOaBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    private Integer status;

    private String parentBillId;
    
    private String parentBillCode;
    
    private String parentBillNo;

    /**
     * 出库单编号
     */
    @ApiModelProperty("出库单编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;
    @ApiModelProperty(value = "红蓝字  1（销售出库），-1（销售退货）")
    private Integer frob;
    @ApiModelProperty(value = "业务员id")
    private String salesmanId;
    @ApiModelProperty(value = "来源")
    private String source;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;
    @ApiModelProperty("原单类型")
    private String originBillType;
    @ApiModelProperty("原单编号")
    private String originBillNo;
    /**
     * 客户id
     */
    @ApiModelProperty("客户id")
    private String customerId;
    @ApiModelProperty("备注")
    private String note;
    /**
     * 收货联系人
     */
    @ApiModelProperty("收货联系人")
    private String receiver;
    /**
     * 联系电话
     */
    @ApiModelProperty("联系电话")
    private String receiverPhoneNum;
    /**
     * 出库日期
     */
    @ApiModelProperty("出库日期")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate proceedDate;
    /**
     * 出库人员id
     */
    @ApiModelProperty("出库人员id")
    private String storagerId;
    /**
     * 出库部门id
     */
    @ApiModelProperty("出库部门id")
    private String departmentId;
    /**
     * 是否完成扫码
     */
    @ApiModelProperty("是否完成扫码")
    private Integer isScan;
    /**
     * 送货地址
     */
    @ApiModelProperty("送货地址")
    private String shippingAddress;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    /**
     * 仓库id
     */
    @ApiModelProperty("仓库id")
    private String warehouseId;

    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    private String commodityId;
    /**
     * 生产数量
     */
    @ApiModelProperty("生产数量")
    private Integer materialNum;
    /**
     * 出库类型（销售出库|其他出库）
     */
    @ApiModelProperty("入库类型（销售出库|其他出库）")
    private String putType;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("保存的出库单商品|入参")
    private List<StOutOrderCommodity> commodityList;

    @TableField(exist = false)
    private List<StOutOrderCommodity> bomList;
    @TableField(exist = false)
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("删除的入库单商品id数组|入参")
    private List<String> commodityDelIdList;

    @TableField(exist = false)
    @ApiModelProperty("保存的库存占用|入参")
    private List<StInventoryGrab> inventoryGrabList;
    @TableField(exist = false)
    @ApiModelProperty("删除的库存占用id|入参")
    private List<String> inventoryGrabDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|入参")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|入参")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品中文规格|入参")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品英文规格|入参")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("商品计量单位|入参")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("客户名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("出库人员姓名|回显")
    private String storagerName;
    @TableField(exist = false)
    @ApiModelProperty("部门名称|回显")
    private String dptName;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("仓库编号|回显")
    private String warehouseNo;
    @TableField(exist = false)
    @ApiModelProperty("仓库名称|回显")
    private String warehouseName;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
