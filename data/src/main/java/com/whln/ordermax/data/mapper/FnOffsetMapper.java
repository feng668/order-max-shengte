package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.FnOffset;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.FnOffset
 */
public interface FnOffsetMapper extends BaseMapper<FnOffset> {

    List<FnOffset> listByEntity(@Param("entity")FnOffset entity);

    IPage<FnOffset> listByEntity(@Param("entity")FnOffset entity, Page<FnOffset> page);

    Integer insertBatch(@Param("entitylist")List<FnOffset> entitylist);

    FnOffset getAllById(@Param("id")String id);
}




