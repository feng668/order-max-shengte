package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CuCustomerTransferSetting;
import com.whln.ordermax.data.mapper.CuCustomerTransferSettingMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class CuCustomerTransferSettingRepository extends ServiceImpl<CuCustomerTransferSettingMapper, CuCustomerTransferSetting>{

    public IPage<CuCustomerTransferSetting> pageByEntity(CuCustomerTransferSetting entity, Page<CuCustomerTransferSetting> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<CuCustomerTransferSetting> listByEntity(CuCustomerTransferSetting entity) {
        return baseMapper.listByEntity(entity);
    }

    public CuCustomerTransferSetting getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<CuCustomerTransferSetting> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




