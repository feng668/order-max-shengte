package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PoInvoiceCommodity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.query.BillQuery;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PoInvoiceCommodity
 */
public interface PoInvoiceCommodityMapper extends BaseMapper<PoInvoiceCommodity> {

    List<PoInvoiceCommodity> listByEntity(@Param("entity")PoInvoiceCommodity entity);

    IPage<PoInvoiceCommodity> listByEntity(@Param("entity")PoInvoiceCommodity entity, Page<PoInvoiceCommodity> page);

    Integer insertBatch(@Param("entitylist")List<PoInvoiceCommodity> entitylist);

    List<PoInvoiceCommodity> commodityList(BillQuery param);
}




