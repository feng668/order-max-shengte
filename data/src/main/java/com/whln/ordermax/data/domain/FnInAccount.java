package com.whln.ordermax.data.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
* 入帐登记
* @TableName fn_in_account
*/
@ApiModel("入帐登记")
@TableName("fn_in_account")
@Data
public class FnInAccount implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 编号
    */
    @ApiModelProperty("编号")
    private String billNo;
    /**
    * 回执单号
    */
    @ApiModelProperty("回执单号")
    private String receiptsNo;
    /**
    * 入账类型
    */
    @ApiModelProperty("入账类型")
    private String type;
    /**
    * 支付方式
    */
    @ApiModelProperty("支付方式")
    private String paymentType;
    /**
    * 付款单位id（客户id）
    */
    @ApiModelProperty("付款单位id（客户id）")
    private String customerId;
    /**
    * 付款银行
    */
    @ApiModelProperty("付款银行")
    private String customerBank;
    /**
    * 付款账号
    */
    @ApiModelProperty("付款账号")
    private String customerBankAccount;
    /**
    * 人民币汇率
    */
    @ApiModelProperty("人民币汇率")
    private Double rmbRate;
    /**
    * 收款单位id（公司id）
    */
    @ApiModelProperty("收款单位id（公司id）")
    private String companyId;
    /**
    * 美元汇率
    */
    @ApiModelProperty("美元汇率")
    private Double dollarRate;
    /**
    * 入账币种
    */
    @ApiModelProperty("入账币种")
    private String currency;
    /**
    * 入账金额(原币)
    */
    @ApiModelProperty("入账金额(原币)")
    private Double originalCurrencyAmo;
    /**
    * 收款银行
    */
    @ApiModelProperty("收款银行")
    private String companyBank;
    /**
    * 收款账号
    */
    @ApiModelProperty("收款账号")
    private String companyBankAccount;
    /**
    * 入账金额(本币)
    */
    @ApiModelProperty("入账金额(本币)")
    private Double localCurrencyAmo;
    /**
    * 剩余可核销金额
    */
    @ApiModelProperty("剩余可核销金额")
    private Double surplusAmo;
    /**
    * 备注
    */
    @ApiModelProperty("备注")
    private String note;
    /**
    * 核销状态
    */
    @ApiModelProperty("核销状态")
    private Integer offsetStatus;
    /**
    * 入账日期
    */
    @ApiModelProperty("入账日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;


    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("保存的已核销单据|入参")
    private List<FnOffsetBill> billList;
    @TableField(exist = false)
    @ApiModelProperty("删除的已核销单据id|入参")
    private List<String> billDelIdList;



    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
