package com.whln.ordermax.data.domain;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.whln.ordermax.common.exception.BusinessException;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @TableName fl_flow_bill_commodity
 */
@ApiModel("")
@TableName("fl_flow_bill_commodity")
@Data
public class FlFlowBillCommodity implements Serializable {

    /**
     * 单据id
     */
    private String billId;
    /**
     * 单据商品数据id
     */
    private String billCommodityDataId;
    /**
     * 单据编码
     */
    @ApiModelProperty("单据编码")
    private String billCode;
    /**
     * 流转后的单据编码
     */
    @ApiModelProperty("流转后的单据编码")
    private String flowBillCode;
    /**
     * 流转数量
     */
    @ApiModelProperty("流转数量")
    private BigDecimal flowRemainNum;
    /**
     * 流转状态
     */
    @ApiModelProperty("流转状态")
    private Integer flowStatus;

    public void check() {
        if (ObjectUtil.isEmpty(billId)) {
            throw new BusinessException("缺少单据id");
        }
       /* if (ObjectUtil.isEmpty(billCommodityDataId)) {
            throw new BusinessException("单据商品id为空");
        }*/
        if (ObjectUtil.isEmpty(flowRemainNum)) {
            throw new BusinessException("流转为空");
        }

    }

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
