package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.FlFlowBillCommodity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.ordermax.data.domain.FlFlowBillCommodity
 */
public interface FlFlowBillCommodityMapper extends BaseMapper<FlFlowBillCommodity> {

    List<FlFlowBillCommodity> listByEntity(@Param("entity")FlFlowBillCommodity entity);

    IPage<FlFlowBillCommodity> listByEntity(@Param("entity")FlFlowBillCommodity entity, Page<FlFlowBillCommodity> page);

    FlFlowBillCommodity getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<FlFlowBillCommodity> entitylist);

}




