package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.SoSiOrderCommodity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.query.BillQuery;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SoSiOrderCommodity
 */
public interface SoSiOrderCommodityMapper extends BaseMapper<SoSiOrderCommodity> {

    List<SoSiOrderCommodity> listByEntity(@Param("entity")SoSiOrderCommodity entity);

    IPage<SoSiOrderCommodity> listByEntity(@Param("entity")SoSiOrderCommodity entity, Page<SoSiOrderCommodity> page);

    Integer insertBatch(@Param("entitylist")List<SoSiOrderCommodity> entitylist);

    List<SoSiOrderCommodity> commodityList(BillQuery billIds);
}




