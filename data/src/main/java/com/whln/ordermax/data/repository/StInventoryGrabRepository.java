package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.StInventoryGrab;
import com.whln.ordermax.data.mapper.StInventoryGrabMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class StInventoryGrabRepository extends ServiceImpl<StInventoryGrabMapper, StInventoryGrab>{

    public IPage<StInventoryGrab> pageByEntity(StInventoryGrab entity, Page<StInventoryGrab> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<StInventoryGrab> listByEntity(StInventoryGrab entity) {
        return baseMapper.listByEntity(entity);
    }

    public StInventoryGrab getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<StInventoryGrab> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public Boolean saveOrUpdateBatchByUnique(List<StInventoryGrab> entityList) {
        for (StInventoryGrab entity : entityList) {
            this.saveOrUpdateByUnique(entity);
        }
        return true;
    }

    public Boolean saveOrUpdateByUnique(StInventoryGrab entity){
        StInventoryGrab exsits = this.getOne(
                new QueryWrapper<StInventoryGrab>().lambda()
                        .eq(StInventoryGrab::getWarehouseId, entity.getWarehouseId())
                        .eq(StInventoryGrab::getCommodityId, entity.getCommodityId())
                        .eq(StInventoryGrab::getOrderId, entity.getOrderId())
        );
        if (exsits != null) {
            this.update(
                    entity,
                    new QueryWrapper<StInventoryGrab>().lambda()
                            .eq(StInventoryGrab::getWarehouseId, entity.getWarehouseId())
                            .eq(StInventoryGrab::getCommodityId, entity.getCommodityId())
                            .eq(StInventoryGrab::getOrderId, entity.getOrderId())
            );
        }else {
            this.save(entity);
        }
        return true;
    }

    public List<StInventoryGrab> listComGrabByTransportationId(String billId, String commodityId) {
        return baseMapper.listComGrabByTransportationId(billId,commodityId);
    }
}




