package com.whln.ordermax.data.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.CmPriceLevel;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CmPriceLevel
 */
public interface CmPriceLevelMapper extends BaseMapper<CmPriceLevel> {

    List<CmPriceLevel> listByEntity(@Param("entity")CmPriceLevel entity);

    IPage<CmPriceLevel> listByEntity(@Param("entity")CmPriceLevel entity, Page<CmPriceLevel> page);

    Integer insertBatch(@Param("entitylist")List<CmPriceLevel> entitylist);

}




