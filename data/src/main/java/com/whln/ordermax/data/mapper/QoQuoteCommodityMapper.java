package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.vo.CustomerHistoryVO;
import com.whln.ordermax.data.domain.param.ListHistoryPriceByComIdParam;
import com.whln.ordermax.data.domain.vo.LastPriceByComVO;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.common.entity.quote.QoCommodityHistoryQueteVo;
import com.whln.ordermax.data.domain.QoQuoteCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;

import java.util.List;
import java.util.Map;

/**
 * @Entity whln.dyx.data.domain.QoQuoteCommodity
 */
public interface QoQuoteCommodityMapper extends BaseMapper<QoQuoteCommodity> {

    List<QoQuoteCommodity> listByEntity(@Param("entity")QoQuoteCommodity entity);

    IPage<QoQuoteCommodity> listByEntity(@Param("entity")QoQuoteCommodity entity, Page<QoQuoteCommodity> page);

    Integer insertBatch(@Param("entitylist")List<QoQuoteCommodity> entitylist);

    List<Map> getMapByIdWithInfo(@Param("queteId")String queteId);

    IPage<QoCommodityHistoryQueteVo> listHistoryPriceByComIdCustId(@Param("param") ListHistoryPriceByComIdParam param,Page<ListHistoryPriceByComIdParam> page);

    LastPriceByComVO  getLastPriceByComIdAndCustId(@Param("customerId") String customerId, @Param("commodityId") String commodityId);

    List<QoQuoteCommodity> commodityList(BillQuery param);

    List<CustomerHistoryVO> getByCustomerId(@Param("id") String id,@Param("searchInfo") String searchInfo);
}




