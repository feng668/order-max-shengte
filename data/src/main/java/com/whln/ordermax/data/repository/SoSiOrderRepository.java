package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.SoSiOrder;
import com.whln.ordermax.data.domain.SoSiOrderCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.SoSiOrderCommodityMapper;
import com.whln.ordermax.data.mapper.SoSiOrderMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Repository
public class SoSiOrderRepository extends ServiceImpl<SoSiOrderMapper, SoSiOrder> {
    @Resource
    SoSiOrderCommodityMapper soSiOrderCommodityMapper;
    @Resource
    SoSiOrderCommodityRepository soSiOrderCommodityRepository;

    public IPage<SoSiOrder> pageByEntity(SoSiOrder entity, Page<SoSiOrder> page) {
        IPage<SoSiOrder> soSiOrderIPage = baseMapper.listByEntity(entity, page);
        List<SoSiOrder> records = soSiOrderIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(SoSiOrder::getId).collect(Collectors.toSet());
            List<SoSiOrderCommodity> soSiOrderCommodities = soSiOrderCommodityMapper
                    .commodityList(BillQuery.builder().billIds(billIds).build());
            Map<String, List<SoSiOrderCommodity>> groupByBillId =
                    soSiOrderCommodities.stream().collect(Collectors.groupingBy(SoSiOrderCommodity::getBillId));
            List<SoSiOrder> record = records.stream().peek(item -> {
                item.setCommodityList(groupByBillId.get(item.getId()));
            }).collect(Collectors.toList());
            soSiOrderIPage.setRecords(record);
        }
        return soSiOrderIPage;
    }

    public List<SoSiOrder> listByEntity(SoSiOrder entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SoSiOrder> entityList) {
        Boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }

    public SoSiOrder getSoSiOrder(String id) {
        SoSiOrder soSiOrder = baseMapper.getSoSiOrder(id);
        SoSiOrderCommodity soSiOrderCommodity = new SoSiOrderCommodity();
        soSiOrderCommodity.setBillId(soSiOrder.getId());
        List<SoSiOrderCommodity> soSiOrderCommodities = soSiOrderCommodityMapper.listByEntity(soSiOrderCommodity);
        soSiOrder.setCommodityList(soSiOrderCommodities);
        return soSiOrder;

    }

    public SoSiOrder getAllById(String id) {
        SoSiOrder soSiOrder = baseMapper.getSoSiOrder(id);
        SoSiOrderCommodity soSiOrderCommodity = new SoSiOrderCommodity();
        soSiOrderCommodity.setBillId(soSiOrder.getId());
        List<SoSiOrderCommodity> soSiOrderCommodities = soSiOrderCommodityMapper.listByEntity(soSiOrderCommodity);
        soSiOrder.setCommodityList(soSiOrderCommodities);
        return soSiOrder;
    }
}




