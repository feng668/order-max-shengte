package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.SoSiOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SoSiOrder
 */
public interface SoSiOrderMapper extends BaseMapper<SoSiOrder> {

    List<SoSiOrder> listByEntity(@Param("entity")SoSiOrder entity);

    IPage<SoSiOrder> listByEntity(@Param("entity")SoSiOrder entity, Page<SoSiOrder> page);

    Integer insertBatch(@Param("entitylist")List<SoSiOrder> entitylist);

    SoSiOrder getSoSiOrder(String id);
}




