package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.TrTransportationCost;
import com.whln.ordermax.data.mapper.TrTransportationCostMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class TrTransportationCostRepository extends ServiceImpl<TrTransportationCostMapper, TrTransportationCost> {
    @Resource
    private TrTransportationRepository trTransportationRepository;

    public IPage<TrTransportationCost> pageByEntity(TrTransportationCost entity, Page<TrTransportationCost> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<TrTransportationCost> listByEntity(TrTransportationCost entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<TrTransportationCost> entityList) {
        Boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }

}




