package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.PoContract;
import com.whln.ordermax.data.domain.PoContractCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.PoContractCommodityMapper;
import com.whln.ordermax.data.mapper.PoContractMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Repository
public class PoContractRepository extends ServiceImpl<PoContractMapper, PoContract>{
    @Resource
    private PoContractCommodityMapper poContractCommodityMapper;

    public IPage<PoContract> pageByEntity(PoContract entity, Page<PoContract> page) {
        IPage<PoContract> poContractIPage = baseMapper.listByEntity(entity, page);
        List<PoContract> records = poContractIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(PoContract::getId).collect(Collectors.toSet());
            BillQuery param = new BillQuery();
            param.setBillIds(billIds);

            List<PoContractCommodity> list = poContractCommodityMapper.commodityList(param);
            Map<String, List<PoContractCommodity>> collect = list.stream().collect(Collectors.groupingBy(PoContractCommodity::getBillId));
            for(PoContract poContract:records){
                poContract.setCommodityList(collect.get(poContract.getId()));
            }
            poContractIPage.setRecords(records);
        }
        return poContractIPage;
    }

    public List<PoContract> listByEntity(PoContract entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PoContract> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public PoContract getAllById(String id) {
        PoContract allById = baseMapper.get(id);
        if (ObjectUtil.isEmpty(allById)) {
            return null;
        }
        BillQuery param = new BillQuery();
        param.setBillIds(CollUtils.toSet(allById.getId()));
        List<PoContractCommodity> list = poContractCommodityMapper.commodityList(param);
        allById.setCommodityList(list);
        return allById;
    }

    }





