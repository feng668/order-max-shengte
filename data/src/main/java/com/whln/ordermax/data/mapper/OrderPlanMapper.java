package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.OrderPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.OrderPlan
 */
public interface OrderPlanMapper extends BaseMapper<OrderPlan> {

    List<OrderPlan> listByEntity(@Param("entity")OrderPlan entity);

    IPage<OrderPlan> listByEntity(@Param("entity")OrderPlan entity, Page<OrderPlan> page);

    Integer insertBatch(@Param("entitylist")List<OrderPlan> entitylist);

    List<OrderPlan> listByBill(@Param("entity")OrderPlan entity);

    OrderPlan listByBill1(OrderPlan entity);
}




