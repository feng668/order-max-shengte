package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-03-21 13:46
 */
@Data
@ApiModel("客户跟进记录")
public class CuCustomerFollowRecordVO {
    @ApiModelProperty("id")
    private String id;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    /**
     * 标题
     */
    @ApiModelProperty("标题")
    @NotBlank(message = "标题不能为空")
    private String title;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 单据id
     */
    @ApiModelProperty("单据id")
    private String billId;
    /*
     * 单据模块
     */
    @ApiModelProperty("单据模块")
    private String billCode;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private LocalDateTime createDate;
    /**
     * 类型 做了什么
     */
    @ApiModelProperty("类型 做了什么")
    private String type;
    @ApiModelProperty("客户的id")
    @NotBlank(message = "客户不能为空")
    private String customerId;
}
