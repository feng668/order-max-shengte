package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.TrTransportationCommodity;
import com.whln.ordermax.data.mapper.TrTransportationCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class TrTransportationCommodityRepository extends ServiceImpl<TrTransportationCommodityMapper, TrTransportationCommodity>{

    public IPage<TrTransportationCommodity> pageByEntity(TrTransportationCommodity entity, Page<TrTransportationCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<TrTransportationCommodity> listByEntity(TrTransportationCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<TrTransportationCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




