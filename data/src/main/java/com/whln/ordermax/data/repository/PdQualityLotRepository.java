package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PdQualityLot;
import com.whln.ordermax.data.mapper.PdQualityLotMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class PdQualityLotRepository extends ServiceImpl<PdQualityLotMapper, PdQualityLot>{

    public IPage<PdQualityLot> pageByEntity(PdQualityLot entity, Page<PdQualityLot> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PdQualityLot> listByEntity(PdQualityLot entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PdQualityLot> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




