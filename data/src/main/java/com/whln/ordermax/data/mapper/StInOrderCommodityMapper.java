package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.StInOrderCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.StInOrderCommodity
 */
public interface StInOrderCommodityMapper extends BaseMapper<StInOrderCommodity> {

    List<StInOrderCommodity> listByEntity(@Param("entity")StInOrderCommodity entity);

    IPage<StInOrderCommodity> listByEntity(@Param("entity")StInOrderCommodity entity, Page<StInOrderCommodity> page);

    Integer insertBatch(@Param("entitylist")List<StInOrderCommodity> entitylist);

    List<StInOrderCommodity> commodityList(BillQuery param);
}




