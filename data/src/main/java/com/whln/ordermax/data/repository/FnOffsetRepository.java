package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.FnOffset;
import com.whln.ordermax.data.mapper.FnOffsetMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class FnOffsetRepository extends ServiceImpl<FnOffsetMapper, FnOffset>{

    public IPage<FnOffset> pageByEntity(FnOffset entity, Page<FnOffset> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<FnOffset> listByEntity(FnOffset entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<FnOffset> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public FnOffset getAllById(String id) {
        return baseMapper.getAllById(id);
    }

}




