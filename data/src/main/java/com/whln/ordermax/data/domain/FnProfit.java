package com.whln.ordermax.data.domain;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.config.typeHandler.JsonTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 利润中心
 *
 * @TableName fn_profit
 */
@ApiModel("利润中心")
@TableName(value = "fn_profit", autoResultMap = true)
@Data
public class FnProfit implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    @ApiModelProperty("编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;
    /**
     * 合同id
     */
    @ApiModelProperty("出运id")
    private String transportationId;
    /**
     * 采购成本（本币）
     */
    @ApiModelProperty("采购成本（本币）")
    private BigDecimal purchaseAmo;
    /**
     * 销售金额（本币）
     */
    @ApiModelProperty("销售金额（本币）")
    private BigDecimal saleAmo;
    /**
     * 费用金额（本币）
     */
    @ApiModelProperty("费用金额（本币）")
    private BigDecimal costAmo;
    /**
     * 实际毛利（本币）
     */
    @ApiModelProperty("实际毛利（本币）")
    private BigDecimal profitAmo;
    /**
     * 销售类别（内，外）
     */
    @ApiModelProperty("销售类别（内，外）")
    private String saleCategory;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 退税额（本币）
     */
    @ApiModelProperty("退税额（本币）")
    private BigDecimal refundDutyAmo;
    /**
     * 暗拥
     */
    @ApiModelProperty("暗拥")
    private BigDecimal darkBrokerage;
    /**
     * 汇兑损益（本币）
     */
    @ApiModelProperty("汇兑损益（本币）")
    private BigDecimal exchangeReturn;
    /**
     * 附加收取款项（本币）
     */
    @ApiModelProperty("附加收取款项（本币）")
    private BigDecimal additionalCollection;
    /**
     * 附加扣除款项（本币）
     */
    @ApiModelProperty("附加扣除款项（本币）")
    private BigDecimal additionalDeductions;
    /**
     * 扣款金额（原币）
     */
    @ApiModelProperty("扣款金额（原币）")
    private BigDecimal originalDeductionAmo;
    /**
     * 银行损益（原币）
     */
    @ApiModelProperty("银行损益（原币）")
    private BigDecimal originalBankReturn;
    /**
     * 预计毛利（本币）
     */
    @ApiModelProperty("预计毛利（本币）")
    private BigDecimal estimatedProfitAmo;
    /**
     * 预计毛利率
     */
    @ApiModelProperty("预计毛利率")
    private BigDecimal estimatedProfitMargin;
    /**
     * 实际毛利率
     */
    @ApiModelProperty("实际毛利率")
    private BigDecimal profitMargin;
    /**
     * 实付金额
     */
    @ApiModelProperty("实付金额")
    private BigDecimal realPayAmo;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    @ApiModelProperty("客户id")
    private String customerId;

    @TableField(typeHandler = JsonTypeHandler.class)
    private JSONObject bindingBill;

    @ApiModelProperty("汇率")
    private BigDecimal rate;
    @ApiModelProperty("销售金额（原币）")
    private BigDecimal originalSaleAmo;
    @ApiModelProperty("采购商品金额")
    private BigDecimal commodityPurchaseAmo;
    @ApiModelProperty("采购商品生产金额")
    private BigDecimal commodityProductionAmo;
    @ApiModelProperty("销售提成")
    private BigDecimal saleRoyalty;
    @ApiModelProperty("销售提成比例")
    private BigDecimal saleRoyaltyRatio;
    @ApiModelProperty("其他费用金额")
    private BigDecimal orhterCostAmo;
    @ApiModelProperty("管理费用金额")
    private BigDecimal manageCostAmo;
    @ApiModelProperty("费用金额总计")
    private BigDecimal costTotalAmo;
    @ApiModelProperty("其他费用总计金额")
    private BigDecimal orhterCostTotalAmo;


    /*附加属性*/
    @TableField(exist = false)
    @ApiModelProperty("出运编号")
    private String transportationNo;
    @TableField(exist = false)
    @ApiModelProperty("合同编号")
    private String orderNo;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
