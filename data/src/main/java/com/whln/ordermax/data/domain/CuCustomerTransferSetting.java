package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* 客户公海设置
* @TableName cu_customer_transfer_setting
*/
@ApiModel("客户公海设置")
@TableName("cu_customer_transfer_setting")
@Data
public class CuCustomerTransferSetting implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 是否启用自动转移
    */
    @ApiModelProperty("是否启用自动转移")
    private Integer isUsedTransfer;
    /**
    * 提醒时长
    */
    @ApiModelProperty("提醒时长")
    private Long remindInterval;
    /**
    * 是否启用抢回限制
    */
    @ApiModelProperty("是否启用抢回限制")
    private Integer isUsedFindLimit;
    /**
    * 抢回限制时长
    */
    @ApiModelProperty("抢回限制时长")
    private Long findLimitInterval;
    @ApiModelProperty("转移时间间隔")
    private Long transferInterval;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    private Date updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ApiModelProperty("是否启用")
    private Integer isActive;


    /**附加属性*/
    @TableField(exist = false)
    @ApiModelProperty("公海规则删除id")
    private List<String> ruleDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("公海规则")
    private List<CuCustomerTransferRule> ruleList;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
