package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* 单据附件
* @TableName bill_attachment
*/
@ApiModel("单据附件")
@TableName("bill_attachment")
@Data
public class BillAttachment implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 路径
    */
    @ApiModelProperty("路径")
    private String path;
    /**
    * 文件名
    */
    @ApiModelProperty("文件名")
    private String fileName;
    /**
    * 文件大小
    */
    @ApiModelProperty("文件大小")
    private Long fileSize;
    /**
    * 模块代码
    */
    @ApiModelProperty("模块代码")
    private String billCode;
    /**
    * 单据id
    */
    @ApiModelProperty("单据id")
    private String billId;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    private Date updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
