package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.SoSiInvoice;
import com.whln.ordermax.data.domain.SoSiInvoiceCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.SoSiInvoiceCommodityMapper;
import com.whln.ordermax.data.mapper.SoSiInvoiceMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 */
@Repository
public class SoSiInvoiceRepository extends ServiceImpl<SoSiInvoiceMapper, SoSiInvoice> {
    @Resource
    SoSiInvoiceCommodityMapper soSiInvoiceCommodityMapper;


    public IPage<SoSiInvoice> pageByEntity(SoSiInvoice entity, Page<SoSiInvoice> page) {
        IPage<SoSiInvoice> soSiOrderIPage = baseMapper.listByEntity(entity, page);
        List<SoSiInvoice> records = soSiOrderIPage.getRecords();
        if (ObjectUtil.isNotEmpty(records)) {
            Set<String> billIds = records.stream().map(SoSiInvoice::getId).collect(Collectors.toSet());
            List<SoSiInvoiceCommodity> soSiOrderCommodities = soSiInvoiceCommodityMapper
                    .commodityList(BillQuery.builder().billIds(billIds).build());
            Map<String, List<SoSiInvoiceCommodity>> groupByBillId =
                    soSiOrderCommodities.stream().collect(Collectors.groupingBy(SoSiInvoiceCommodity::getBillId));
            List<SoSiInvoice> record = records.stream().peek(item -> {
                item.setCommodityList(groupByBillId.get(item.getId()));
            }).collect(Collectors.toList());
            soSiOrderIPage.setRecords(record);
        }
        return soSiOrderIPage;
    }

    public List<SoSiInvoice> listByEntity(SoSiInvoice entity) {
        return baseMapper.listByEntity(entity);
    }



    public SoSiInvoice getSoSiInvoice(String id) {
        SoSiInvoice soSiOrder = baseMapper.getSoSiInvoice(id);
        SoSiInvoiceCommodity soSiOrderCommodity = new SoSiInvoiceCommodity();
        soSiOrderCommodity.setBillId(soSiOrder.getId());
        List<SoSiInvoiceCommodity> soSiOrderCommodities = soSiInvoiceCommodityMapper.listByEntity(soSiOrderCommodity);
        soSiOrder.setCommodityList(soSiOrderCommodities);
        return soSiOrder;

    }

    public SoSiInvoice getAllById(String id) {
        SoSiInvoice soSiOrder = baseMapper.getSoSiInvoice(id);
        SoSiInvoiceCommodity soSiOrderCommodity = new SoSiInvoiceCommodity();
        soSiOrderCommodity.setBillId(soSiOrder.getId());
        List<SoSiInvoiceCommodity> soSiOrderCommodities = soSiInvoiceCommodityMapper.listByEntity(soSiOrderCommodity);
        soSiOrder.setCommodityList(soSiOrderCommodities);
        return soSiOrder;
    }
}




