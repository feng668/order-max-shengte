package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.StInOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.StInOrder
 */
public interface StInOrderMapper extends BaseMapper<StInOrder> {

    List<StInOrder> listByEntity(@Param("entity")StInOrder entity);

    IPage<StInOrder> listByEntity(@Param("entity")StInOrder entity, Page<StInOrder> page);

    Integer insertBatch(@Param("entitylist")List<StInOrder> entitylist);

}




