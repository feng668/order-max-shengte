package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.vo.BaseTotalVO;
import com.whln.ordermax.data.domain.StInventoryRemain;
import com.whln.ordermax.data.domain.vo.BaseInfoVO;
import com.whln.ordermax.data.domain.vo.StInventoryVO;
import com.whln.ordermax.data.domain.param.StInventoryListParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Entity whln.ordermax.data.domain.StInventory
 */
public interface StInventoryMapper extends BaseMapper<Object> {

    IPage<Map> listByEntity(Map entity, Page<Map> page);

    IPage<BaseTotalVO > pageTotalByStageType(Map entity, Page<Map> page);

    IPage<StInventoryVO> page(@Param("entity") StInventoryListParam entity, Page<StInventoryListParam> page);

    List<BaseInfoVO> baseInfo(@Param("entity") StInventoryRemain entity);

    List<BaseInfoVO> getSoSiOrder(StInventoryRemain entity);

    List<BaseInfoVO> getSoSoOrder(StInventoryRemain entity);

    List<BaseInfoVO> getStInventory(StInventoryRemain entity);
}




