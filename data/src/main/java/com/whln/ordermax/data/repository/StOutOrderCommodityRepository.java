package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.StOutOrderCommodity;
import com.whln.ordermax.data.mapper.StOutOrderCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class StOutOrderCommodityRepository extends ServiceImpl<StOutOrderCommodityMapper, StOutOrderCommodity>{

    public IPage<StOutOrderCommodity> pageByEntity(StOutOrderCommodity entity, Page<StOutOrderCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<StOutOrderCommodity> listByEntity(StOutOrderCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<StOutOrderCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




