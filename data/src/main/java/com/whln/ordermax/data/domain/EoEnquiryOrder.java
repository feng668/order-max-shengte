package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.OaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 询价单
* @TableName eo_enquiry_order
*/
@ApiModel("询价单")
@TableName("eo_enquiry_order")
@Data
public class EoEnquiryOrder extends OaBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;

    /**
    * 询价单编号
    */
    @ApiModelProperty("询价单编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;
    /**
    * 询价日期
    */
    @ApiModelProperty("询价日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate enquiryDate;
    /**
    * 交货日期
    */
    @ApiModelProperty("交货日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;
    /**
    * 客户id
    */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
    * 有效天数
    */
    @ApiModelProperty("有效天数")
    private Integer effectiveDays;
    /**
    * 结算币种
    */
    @ApiModelProperty("供应商id")
    private String supplierId;
    @ApiModelProperty("结算币种")
    private String currency;
    /**
    * 汇率
    */
    @ApiModelProperty("汇率")
    private BigDecimal rate;
    /**
    * 商品总金额
    */
    @ApiModelProperty("商品总金额")
    private BigDecimal totalAmo;
    /**
    * 出货公司
    */
    @ApiModelProperty("出货公司")
    private String companyId;
    /**
    * 出货公司银行
    */
    @ApiModelProperty("出货公司银行")
    private String companyBank;
    /**
    * 客户联系人id
    */
    @ApiModelProperty("客户联系人id")
    private String customerContactsId;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
    * 拥有人id(业务员)
    */
    @ApiModelProperty("拥有人id(业务员)")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    /**
    * 商品总体积
    */
    @ApiModelProperty("商品总体积")
    private Double totalVolume;
    /**
    * 商品总数量
    */
    @ApiModelProperty("商品总数量")
    private Integer totalNum;
    /**
    * 截止日期
    */
    @ApiModelProperty("截止日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate expirationDate;
    /**
    * 付款方式
    */
    @ApiModelProperty("付款方式")
    private String paymentType;
    @TableField(exist = false)
    private String searchInfo;
    /**
    * 包装要求
    */
    @ApiModelProperty("包装要求")
    private String packagingRemark;
    /**
    * 阶段
    */
    @ApiModelProperty("阶段")
    private Integer plan;
    /**
    * 来源
    */
    @ApiModelProperty("来源")
    private String billSource;
    /**
    * 客户要求
    */
    @ApiModelProperty("客户要求")
    private String custRequire;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("保存的报价单商品|入参")
    private List<EoEnquiryCommodity> commodityList;
    @TableField(exist = false)
    @ApiModelProperty("删除的报价单商品id数组|入参")
    private List<String> commodityDelIdList;


    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("客户地址|回显")
    private String custAddress;
    @TableField(exist = false)
    @ApiModelProperty("客户电话|回显")
    private String custPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("客户开户行|回显")
    private String custBank;
    @TableField(exist = false)
    @ApiModelProperty("客户银行账号|回显")
    private String custBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("客户邮编|回显")
    private String custZipCode;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司邮编|回显")
    private String cpnZipCode;
    @TableField(exist = false)
    @ApiModelProperty("供应商中文名称|回显")
    private String supName;
    @TableField(exist = false)
    @ApiModelProperty("供应商地址|回显")
    private String supAddress;
    @TableField(exist = false)
    @ApiModelProperty("供应商电话|回显")
    private String supPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("供应商开户行|回显")
    private String supBank;
    @TableField(exist = false)
    @ApiModelProperty("供应商银行账号|回显")
    private String supBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("供应商邮编|回显")
    private String suptZipCode;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
