package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.EmEmailAttachment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EmEmailAttachment
 */
public interface EmEmailAttachmentMapper extends BaseMapper<EmEmailAttachment> {

    List<EmEmailAttachment> listByEntity(@Param("entity")EmEmailAttachment entity);

    IPage<EmEmailAttachment> listByEntity(@Param("entity")EmEmailAttachment entity, Page<EmEmailAttachment> page);

    Integer insertBatch(@Param("entitylist")List<EmEmailAttachment> entitylist);

}




