package com.whln.ordermax.data.domain.query;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-02-22 10:11
 */
@ApiModel("删除参数")
@Data
public class DeleteQuery {
    /**
     * id
     */
    private String id;
}
