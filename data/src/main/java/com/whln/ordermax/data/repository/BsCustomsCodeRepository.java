package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.BsCustomsCode;
import com.whln.ordermax.data.mapper.BsCustomsCodeMapper;

import java.util.List;

/**
 *
 */
@Repository
public class BsCustomsCodeRepository extends ServiceImpl<BsCustomsCodeMapper, BsCustomsCode>{

    public IPage<BsCustomsCode> pageByEntity(BsCustomsCode entity, Page<BsCustomsCode> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<BsCustomsCode> listByEntity(BsCustomsCode entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<BsCustomsCode> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




