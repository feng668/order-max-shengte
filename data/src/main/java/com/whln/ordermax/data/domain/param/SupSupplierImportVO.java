package com.whln.ordermax.data.domain.param;

import com.whln.ordermax.data.domain.SupSupplier;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-28 13:52
 */
@Data
public class SupSupplierImportVO {
    @ApiModelProperty("已存在的")
    private List<SupSupplier> exits;
    @ApiModelProperty("不存在的")
    private List<SupSupplier> noExits;
    @ApiModelProperty("已存在的数量")
    private Integer exitsNum;
    @ApiModelProperty("不存在的数量")
    private Integer noExitsNum;
}
