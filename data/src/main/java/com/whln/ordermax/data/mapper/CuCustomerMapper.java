package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.CuCustomer;
import com.whln.ordermax.data.domain.query.GetCustomerInfoQuery;
import com.whln.ordermax.data.domain.vo.EmailHistoryVO;
import com.whln.ordermax.data.domain.vo.EmailSendTypeVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @Entity whln.dyx.data.domain.CuCustomer
 */
public interface CuCustomerMapper extends BaseMapper<CuCustomer> {

    List<CuCustomer> listByEntity(@Param("entity")CuCustomer entity);

    IPage<CuCustomer> listByEntity(@Param("entity")CuCustomer entity, Page<CuCustomer> page);

    Integer insertBatch(@Param("entitylist")List<CuCustomer> entitylist);

    List<Map> listCommodityByCustomerId(@Param("id")String id,@Param("searchInfo") String searchInfo,@Param("billCode") String billCode);

    List<String> listIdByNotConclude(@Param("category")Integer category,@Param("status") Integer status, @Param("notUpDay")Long notUpDay);

    List<String> queryPubCustomer();

    List<EmailSendTypeVO> getCustomerInfo(GetCustomerInfoQuery getCustomerInfo);

    List<EmailHistoryVO> emailHistory(String costomerId);
}




