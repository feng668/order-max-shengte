package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.common.entity.quote.QoCommodityHistoryQueteVo;
import com.whln.ordermax.data.domain.QoInQuoteCommodity;
import com.whln.ordermax.data.mapper.QoInQuoteCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Repository
public class QoInQuoteCommodityRepository extends ServiceImpl<QoInQuoteCommodityMapper, QoInQuoteCommodity>{

    public IPage<QoInQuoteCommodity> pageByEntity(QoInQuoteCommodity entity, Page<QoInQuoteCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<QoInQuoteCommodity> listByEntity(QoInQuoteCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<QoInQuoteCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<Map> getMapByIdWithInfo(String queteId) {
        return baseMapper.getMapByIdWithInfo(queteId);
    }

    public List<QoCommodityHistoryQueteVo> listHistoryPriceByComIdCustId(String customerId, String commodityId) {
        return baseMapper.listHistoryPriceByComIdCustId(customerId,commodityId);
    }

    public Double getLastPriceByComIdAndCustId(String customerId, String commodityId) {
        return baseMapper.getLastPriceByComIdAndCustId(customerId,commodityId);
    }
}




