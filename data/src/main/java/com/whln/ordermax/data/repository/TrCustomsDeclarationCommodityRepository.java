package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.TrCustomsDeclarationCommodity;
import com.whln.ordermax.data.mapper.TrCustomsDeclarationCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class TrCustomsDeclarationCommodityRepository extends ServiceImpl<TrCustomsDeclarationCommodityMapper, TrCustomsDeclarationCommodity>{

    public IPage<TrCustomsDeclarationCommodity> pageByEntity(TrCustomsDeclarationCommodity entity, Page<TrCustomsDeclarationCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<TrCustomsDeclarationCommodity> listByEntity(TrCustomsDeclarationCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<TrCustomsDeclarationCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




