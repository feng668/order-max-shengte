package com.whln.ordermax.data.domain.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @author liurun
 * @date 2023-03-15 21:32
 */
@Data
public class BaseTotalVO {
    @ApiModelProperty("仓库id")
    private String warehouseId;
    @ApiModelProperty("季度")
    private String stage;
    @ApiModelProperty("商品id")
    private String commodityId;
    @ApiModelProperty("期初数量")
    private String beforeTotalNum;
    @ApiModelProperty("期初金额")
    private String beforeTotalAmo;
    @ApiModelProperty("当期入库")
    private String currentInNum;
    @ApiModelProperty("当期出库")
    private String currentOutNum;
    @ApiModelProperty("当期入库金额")
    private BigDecimal currentInAmo;
    @ApiModelProperty("当期出库金额")
    private BigDecimal currentOutAmo;
    @ApiModelProperty("商品编号")
    private String partNo;
    @ApiModelProperty("商品中文名")
    private String cnName;
    @ApiModelProperty("商品英文名")
    private String enName;
    @TableField(exist = false)
    private String cmCustomsCodeId;
    @ApiModelProperty("商品中文规格")
    private String specification;
    @ApiModelProperty("商品英文规格")
    private String enSpecification;
    @TableField(exist = false)
    private String cmNetWeight;
    @ApiModelProperty("计量单位")
    private String quantityUnit;
    @TableField(exist = false)
    private BigDecimal singleBoxVolume;
    @TableField(exist = false)
    private String ccVatRate;
    @ApiModelProperty("仓库名")
    private String warehouseName;
    @ApiModelProperty("仓库编号")
    private String warehouseNo;
    @ApiModelProperty("不含税单价")
    private BigDecimal price;
    @ApiModelProperty("含税单价")
    private BigDecimal taxPrice;
    @ApiModelProperty("期末数量")
    private BigDecimal afterTotalNum;
    @ApiModelProperty("期末金额")
    private BigDecimal afterTotalAmo;
}
