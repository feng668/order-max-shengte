package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.OaRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.OaRule
 */
public interface OaRuleMapper extends BaseMapper<OaRule> {

    List<OaRule> listByEntity(@Param("entity")OaRule entity);

    IPage<OaRule> listByEntity(@Param("entity")OaRule entity, Page<OaRule> page);

    Integer insertBatch(@Param("entitylist")List<OaRule> entitylist);

}




