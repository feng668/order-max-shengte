package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 付款单
* @TableName fn_payment
*/
@ApiModel("付款单")
@TableName("fn_payment_request")
@Data
public class FnPaymentRequest implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 付款单号
    */
    @ApiModelProperty("付款单号")
    private String billNo;
    /**
    * 记账日期
    */
    @ApiModelProperty("记账日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
    * 付款总金额(原币)
    */
    @ApiModelProperty("付款总金额(原币)")
    private Double originalCurrencyAmo;
    /**
    * 支付方式
    */
    @ApiModelProperty("支付方式")
    private String paymentType;
    /**
    * 付款总金额(本币)
    */
    @ApiModelProperty("付款总金额(本币)")
    private Double localCurrencyAmo;
    /**
    * 付款币种
    */
    @ApiModelProperty("付款币种")
    private String currency;
    /**
    * 实付金额(原币)
    */
    @ApiModelProperty("实付金额(原币)")
    private Double realityOriginalCurrencyAmo;
    /**
    * 本币汇率
    */
    @ApiModelProperty("本币汇率")
    private Double rmbRate;
    /**
    * 实付金额(本币)
    */
    @ApiModelProperty("实付金额(本币)")
    private Double realityLocalCurrencyAmo;
    /**
    * 银行水单
    */
    @ApiModelProperty("银行水单")
    private String bankSerialNo;
    /**
    * 收款单位id(供应商id)
    */
    @ApiModelProperty("收款单位id(供应商id)")
    private String supplierId;
    /**
    * 收款银行
    */
    @ApiModelProperty("收款银行")
    private String supplierBank;
    /**
    * 收款账号
    */
    @ApiModelProperty("收款账号")
    private String supplierBankAccount;
    /**
    * 付款单位id(公司id)
    */
    @ApiModelProperty("付款单位id(公司id)")
    private String customerId;
    /**
    * 付款账号
    */
    @ApiModelProperty("付款账号")
    private String customerBank;
    /**
    * 付款银行
    */
    @ApiModelProperty("付款银行")
    private String customerBankAccount;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;



    /* 附加属性 */
    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("保存的已付款单据|入参")
    private List<FnPaymentBill> billList;
    @TableField(exist = false)
    @ApiModelProperty("删除的已付款单据id|入参")
    private List<String> billDelIdList;



    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("供应商中文名称|回显")
    private String supName;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
