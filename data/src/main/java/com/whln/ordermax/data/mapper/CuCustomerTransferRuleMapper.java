package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.CuCustomerTransferRule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CuCustomerTransferRule
 */
public interface CuCustomerTransferRuleMapper extends BaseMapper<CuCustomerTransferRule> {

    List<CuCustomerTransferRule> listByEntity(@Param("entity")CuCustomerTransferRule entity);

    IPage<CuCustomerTransferRule> listByEntity(@Param("entity")CuCustomerTransferRule entity, Page<CuCustomerTransferRule> page);

    CuCustomerTransferRule getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<CuCustomerTransferRule> entitylist);

}




