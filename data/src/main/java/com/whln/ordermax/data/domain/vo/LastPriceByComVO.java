package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author liurun
 * @date 2023-03-15 17:00
 */
@ApiModel("最后报价返回值")
@Data
public class LastPriceByComVO implements Serializable {
    private static final long serialVersionUID = 5020746154397306165L;
    @ApiModelProperty("价格")
    private BigDecimal price;
}
