package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PdProductTask;
import com.whln.ordermax.data.mapper.PdProductTaskMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class PdProductTaskRepository extends ServiceImpl<PdProductTaskMapper, PdProductTask>{

    public IPage<PdProductTask> pageByEntity(PdProductTask entity, Page<PdProductTask> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PdProductTask> listByEntity(PdProductTask entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PdProductTask> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




