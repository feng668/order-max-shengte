package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.QoQuoteOrder;

import java.util.List;
import java.util.Map;

/**
 * @Entity whln.dyx.data.domain.QoQuoteOrder
 */
public interface QoQuoteOrderMapper extends BaseMapper<QoQuoteOrder> {

    List<QoQuoteOrder> listByEntity(@Param("entity")QoQuoteOrder entity);

    IPage<QoQuoteOrder> listByEntity(@Param("entity")QoQuoteOrder entity, Page<QoQuoteOrder> page);

    QoQuoteOrder getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<QoQuoteOrder> entitylist);

    Map<String,Object> getByIdWithInfo(@Param("id")String queteId);
}




