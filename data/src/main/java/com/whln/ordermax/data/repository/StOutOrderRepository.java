package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.StOutOrder;
import com.whln.ordermax.data.domain.StOutOrderCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.StOutOrderCommodityMapper;
import com.whln.ordermax.data.mapper.StOutOrderMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class StOutOrderRepository extends ServiceImpl<StOutOrderMapper, StOutOrder>{

    public IPage<StOutOrder> pageByEntity(StOutOrder entity, Page<StOutOrder> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<StOutOrder> listByEntity(StOutOrder entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<StOutOrder> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    @Resource
    private StOutOrderCommodityMapper stOutOrderCommodityMapper;
    public StOutOrder getAllById(String id) {
        StOutOrder stOutOrder = baseMapper.selectById(id);

        List<StOutOrderCommodity> stOutOrderCommodities = stOutOrderCommodityMapper
                .commodityList(BillQuery
                        .builder()
                        .billIds(CollUtils.toSet(stOutOrder.getId()))
                        .build());
        stOutOrder.setCommodityList(stOutOrderCommodities);
        return stOutOrder;
    }
}




