package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.EmSubmitted;
import com.whln.ordermax.data.mapper.EmSubmittedMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class EmSubmittedRepository extends ServiceImpl<EmSubmittedMapper, EmSubmitted>{

    public IPage<EmSubmitted> pageByEntity(EmSubmitted entity, Page<EmSubmitted> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<EmSubmitted> listByEntity(EmSubmitted entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<EmSubmitted> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




