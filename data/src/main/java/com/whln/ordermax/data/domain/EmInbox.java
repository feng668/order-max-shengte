package com.whln.ordermax.data.domain;

import cn.hutool.json.JSONArray;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.config.typeHandler.JsonArrTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
* 收件箱
* @TableName em_inbox
*/
@ApiModel("收件箱")
@TableName("em_inbox")
@Data
public class EmInbox implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 对方id
    */
    @ApiModelProperty("发送方id")
    private String oppositeId;
    /**
    * 我方邮箱
    */
    @ApiModelProperty("接收邮箱")
    private String ownerEmail;
    /**
    * 对方邮箱
    */
    @ApiModelProperty("发送方邮箱")
    private String oppositeEmail;

    @ApiModelProperty("发送方联系人id")
    private String opppsiteContactsId;
    @ApiModelProperty("发送方称呼")
    private String oppositeSendName;
    /**
    * 主题
    */
    @ApiModelProperty("主题")
    private String theme;
    /**
    * 正文
    */
    @ApiModelProperty("正文")
    private String bodyText;
    @ApiModelProperty("正文路径")
    private String bodyPath;
    /**
    * 业务类型
    */
    @ApiModelProperty("业务类型")
    private String oppositeType;

    @ApiModelProperty("状态 1 草稿")
    private Integer status;
    /**
    * 是否标记
    */
    @ApiModelProperty("是否标记")
    private Integer isFlag;
    /**
    * 是否删除
    */
    @ApiModelProperty("是否删除")
    private Integer isDelete;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    /**
     * 消息唯一id
     */
    @ApiModelProperty("消息唯一id")
    private String messageId;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("接收时间")
    private LocalDateTime receiveTime;
    @ApiModelProperty("是否已读")
    private Integer isRead;

    /**
     * 是否重要  0  普通 1 重要
     */
    @ApiModelProperty("是否重要  0  普通 1 重要")
    private Integer isImportant;
    @ApiModelProperty("接收人")
    @TableField(typeHandler = JsonArrTypeHandler.class)
    private JSONArray recivedArr;

    /*@TableField(exist = false)
    private String aaaaaaaa;

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
        this.aaaaaaaa = bodyText;
    }*/

    /*附加属性*/
    @ApiModelProperty("发送方名称")
    @TableField(exist = false)
    private String oppositeName;
    @ApiModelProperty("模糊查询参数")
    @TableField(exist = false)
    private String likeQuery;
    @ApiModelProperty("邮件重要与否")
    @TableField(exist = false)
    private String emailLevel;
    @ApiModelProperty("邮箱地址")
    @TableField(exist = false)
    private String emailAddress;
    @TableField(exist = false)
    private List<String> emails;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
