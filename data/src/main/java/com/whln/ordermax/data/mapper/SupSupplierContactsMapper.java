package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.SupSupplierContacts;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SupSupplierContacts
 */
public interface SupSupplierContactsMapper extends BaseMapper<SupSupplierContacts> {

    List<SupSupplierContacts> listByEntity(@Param("entity")SupSupplierContacts entity);

    IPage<SupSupplierContacts> listByEntity(@Param("entity")SupSupplierContacts entity, Page<SupSupplierContacts> page);

    Integer insertBatch(@Param("entitylist")List<SupSupplierContacts> entitylist);

}




