package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-03-15 16:31
 */
@ApiModel("往期报价列表")
@Data
public class ListHistoryPriceByComIdParam {
    private String customerId;
    private String commodityId;
    private String searchInfo;
    private Integer current = 1;
    private Integer size = 10;

}
