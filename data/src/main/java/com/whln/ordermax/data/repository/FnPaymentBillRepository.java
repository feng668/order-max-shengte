package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.FnPaymentBill;
import com.whln.ordermax.data.mapper.FnPaymentBillMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class FnPaymentBillRepository extends ServiceImpl<FnPaymentBillMapper, FnPaymentBill>{

    public IPage<FnPaymentBill> pageByEntity(FnPaymentBill entity, Page<FnPaymentBill> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<FnPaymentBill> listByEntity(FnPaymentBill entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<FnPaymentBill> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<FnPaymentBill> listBySuperiorId(String superiorId){
        return baseMapper.listBySuperiorId(superiorId);
    }

}




