package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
* 报关代码
* @TableName bs_customs_code
*/
@ApiModel("报关代码")
@TableName("bs_customs_code")
@Data
public class BsCustomsCode implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 报关商品编号
    */
    @ApiModelProperty("报关商品编号")
    private String declarationNo;
    /**
    * 报关中文名
    */
    @ApiModelProperty("报关中文名")
    private String customsCnName;
    /**
    * 报关英文名
    */
    @ApiModelProperty("报关英文名")
    private String customsEnName;
    /**
    * 增值税率
    */
    @ApiModelProperty("增值税率")
    private BigDecimal vatRate;
    /**
    * 退税率
    */
    @ApiModelProperty("退税率")
    private BigDecimal rebatesRate;
    /**
    * 是否启用
    */
    @ApiModelProperty("是否启用")
    private Integer isEnable;
    /**
    * 批次
    */
    @ApiModelProperty("批次")
    private Integer batch;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
