package com.whln.ordermax.data.repository;

import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.FlFlowBill;
import com.whln.ordermax.data.mapper.FlFlowBillMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;
import java.util.Map;

/**
 *
 */
@Repository
public class FlFlowBillRepository extends ServiceImpl<FlFlowBillMapper, FlFlowBill>{

    public IPage<FlFlowBill> pageByEntity(FlFlowBill entity, Page<FlFlowBill> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<FlFlowBill> listByEntity(FlFlowBill entity) {
        return baseMapper.listByEntity(entity);
    }

    public FlFlowBill getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<FlFlowBill> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<Map<String,Object>> listBillCommodityWithRemainNum(FlFlowBill entity, String tableBody) {
        return baseMapper.listBillCommodityWithRemainNum(entity,tableBody);
    }

    @Override
    public boolean saveOrUpdate(FlFlowBill entity) {
        FlFlowBill exsits = getOne(
                new QueryWrapper<FlFlowBill>()
                        .eq("bill_id", entity.getBillId())
                        .eq("bill_code", entity.getBillCode())
                        .eq("flow_bill_code", entity.getFlowBillCode())
        );
        if (exsits==null){
            return save(entity);
        }else {
            return update(
                    entity,
                    new UpdateWrapper<FlFlowBill>()
                            .eq("bill_id", entity.getBillId())
                            .eq("bill_code", entity.getBillCode())
                            .eq("flow_bill_code", entity.getFlowBillCode())
            );
        }
    }

    public List<Map<String, Object>> listNotFlowBill(JSONObject entity, String tableBody) {
        return baseMapper.listNotFlowBill(entity,tableBody);
    }
}