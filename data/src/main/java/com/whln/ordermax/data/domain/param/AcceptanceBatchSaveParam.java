package com.whln.ordermax.data.domain.param;

import com.whln.ordermax.data.domain.FlFlowBillCommodity;
import com.whln.ordermax.data.domain.PdAcceptance;
import com.whln.ordermax.data.domain.base.BillBase;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-02-18 14:42
 */
@ApiModel("验收单批量保存参数")
@Data
public class AcceptanceBatchSaveParam extends BillBase {
    @ApiModelProperty("生成验收单所在步骤的billid")
    private String parentBillId;
    @ApiModelProperty("生成验收单所在步骤的billCode")
    private String parentBillCode;
    List<FlFlowBillCommodity> commodityFlowList;
    @ApiModelProperty("验收单列表")
    List<PdAcceptance> entityList;
    private String id;
    private String saveType;
}
