package com.whln.ordermax.data.domain.param;

import com.whln.ordermax.data.domain.CmCommodity;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-27 21:30
 */
@Data
public class CmCommodityImportParam {
    private List<CmCommodity> commodityList;
    private Integer type;
}
