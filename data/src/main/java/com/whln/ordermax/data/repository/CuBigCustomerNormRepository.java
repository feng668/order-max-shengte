package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.CuBigCustomerNorm;
import com.whln.ordermax.data.mapper.CuBigCustomerNormMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class CuBigCustomerNormRepository extends ServiceImpl<CuBigCustomerNormMapper, CuBigCustomerNorm>{

    public IPage<CuBigCustomerNorm> pageByEntity(CuBigCustomerNorm entity, Page<CuBigCustomerNorm> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<CuBigCustomerNorm> listByEntity(CuBigCustomerNorm entity) {
        return baseMapper.listByEntity(entity);
    }

    public CuBigCustomerNorm getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<CuBigCustomerNorm> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




