package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.EmSubmittedAttachment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EmSubmittedAttachment
 */
public interface EmSubmittedAttachmentMapper extends BaseMapper<EmSubmittedAttachment> {

    List<EmSubmittedAttachment> listByEntity(@Param("entity")EmSubmittedAttachment entity);

    IPage<EmSubmittedAttachment> listByEntity(@Param("entity")EmSubmittedAttachment entity, Page<EmSubmittedAttachment> page);

    Integer insertBatch(@Param("entitylist")List<EmSubmittedAttachment> entitylist);

}




