package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.SoSoOrderCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SoSoOrderCommodity
 */
public interface SoSoOrderCommodityMapper extends BaseMapper<SoSoOrderCommodity> {

    List<SoSoOrderCommodity> listByEntity(@Param("entity")SoSoOrderCommodity entity);

    IPage<SoSoOrderCommodity> listByEntity(@Param("entity")SoSoOrderCommodity entity, Page<SoSoOrderCommodity> page);

    Integer insertBatch(@Param("entitylist")List<SoSoOrderCommodity> entitylist);

    List<SoSoOrderCommodity> commodityList(BillQuery param);
}




