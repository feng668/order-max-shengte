package com.whln.ordermax.data.domain.vo;

import cn.hutool.json.JSONArray;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.OaRuleLevel;
import com.whln.ordermax.data.domain.OaRuleLevelAssessor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-02 9:34
 */
@ApiModel("oa")
@Data
public class OaRuleVO implements Serializable {
    /**
     * id
     */
    private String id;
    /**
     * 规则名称
     */
    @ApiModelProperty("规则名称")
    private String ruleName;
    /**
     * 单据类别名称
     */
    @ApiModelProperty("单据类别名称")
    private String billName;
    /**
     * 单据类别代码
     */
    @ApiModelProperty("单据类别代码")
    private String billCode;
    /**
     * 单据过滤条件
     */
    @ApiModelProperty("单据过滤条件")
    private String filterCondition;
    /**
     * 审批总层级
     */
    @ApiModelProperty("审批总层级")
    private Integer totalLevel;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ApiModelProperty("是否启用")
    private Integer isActive;
    @ApiModelProperty("进行oa条件")
    private JSONArray proceedOaCondition;

    /**附加属性*/
    @TableField(exist = false)
    @ApiModelProperty("保存的审核级别|入参")
    private List<OaRuleLevel> levelList;
    @TableField(exist = false)
    @ApiModelProperty("删除的审核级别id|入参")
    private List<String> delLevelList;
    @TableField(exist = false)
    @ApiModelProperty("保存的审核级别|入参")
    private List<OaRuleLevelAssessor> assessorList;
    @TableField(exist = false)
    @ApiModelProperty("删除的审核级别id|入参")
    private List<String> delAssessorList;
    @TableField(exist = false)
    private Integer num;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
