package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.TrInDispatch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.TrInDispatch
 */
public interface TrInDispatchMapper extends BaseMapper<TrInDispatch> {

    List<TrInDispatch> listByEntity(@Param("entity")TrInDispatch entity);

    IPage<TrInDispatch> listByEntity(@Param("entity")TrInDispatch entity, Page<TrInDispatch> page);

    Integer insertBatch(@Param("entitylist")List<TrInDispatch> entitylist);

    TrInDispatch getAllById(String id);
}




