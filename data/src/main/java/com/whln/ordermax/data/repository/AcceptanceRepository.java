package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PdAcceptance;
import com.whln.ordermax.data.domain.PdAcceptanceStandard;
import com.whln.ordermax.data.mapper.AcceptanceMapper;
import com.whln.ordermax.data.mapper.AcceptanceStandardMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@AllArgsConstructor
@Repository
public class AcceptanceRepository extends ServiceImpl<AcceptanceMapper, PdAcceptance> {
    @Resource
    private final AcceptanceStandardMapper acceptanceStandardMapper;

    public Page<PdAcceptance> pageByEntity(PdAcceptance entity, Page<PdAcceptance> page) {
        return baseMapper.listByEntity(entity, page);
    }

    public PdAcceptance getAllById(String id) {
        PdAcceptance acceptance = baseMapper.get(id);
        if (ObjectUtil.isEmpty(acceptance)) {
            return null;
        }
        List<PdAcceptanceStandard> pdQualityStandards = acceptanceStandardMapper
                .selectList(new LambdaQueryWrapper<PdAcceptanceStandard>()
                        .eq(PdAcceptanceStandard::getAcceptanceId, acceptance.getId()));
        acceptance.setStandardList(pdQualityStandards);
        return acceptance;
    }

    public List<PdAcceptance> listByEntity(PdAcceptance entity) {
        return baseMapper.listByEntity(entity);
    }

}




