package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author liurun
 * @date 2023-03-06 16:32
 */
@ApiModel("用户标签组")
@Data
public class CustomerLabelGroupVO implements Serializable {

    private static final long serialVersionUID = -365394105543851740L;
    @ApiModelProperty("组名")
    @NotBlank(message = "组名不能为空")
    private String groupName;
    @ApiModelProperty("标签组id")
    private String id;
}
