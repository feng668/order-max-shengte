package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.CmCommodity;
import com.whln.ordermax.data.mapper.CmCommodityMapper;

import java.util.List;

/**
 *
 */
@Repository
public class CmCommodityRepository extends ServiceImpl<CmCommodityMapper, CmCommodity>{

    public Page<CmCommodity> pageByEntity(CmCommodity entity, Page<CmCommodity> page) {
        if (ObjectUtil.equals(entity.getArticleType(),"PART")){
            return baseMapper.cmList(entity,page);
        }
        Page<CmCommodity> cmCommodityIPage = baseMapper.listByEntity(entity, page);
        return cmCommodityIPage;
    }

    public List<CmCommodity> listByEntity(CmCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<CmCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public IPage<CmCommodity> pageWithLastQoPriceByCustId(CmCommodity entity, Page<CmCommodity> page) {
        return baseMapper.pageWithLastQoPriceByCustId(entity,page);
    }

}




