package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
* oa规则审核员
* @TableName oa_rule_level_assessor
*/
@ApiModel("oa规则审核员")
@TableName("oa_rule_level_assessor")
@Data
public class OaRuleLevelAssessor implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    /**
     * 规则id
     */
    @ApiModelProperty("规则id")
    private String ruleId;
    /**
     * 审批人id
     */
    @ApiModelProperty("审批人id")
    private String assessorId;
    /**
     * 审批级别id
     */
    @ApiModelProperty("审批级别id")
    private String roleLevelId;
    /**
     * 审批级别
     */
    @ApiModelProperty("审批层级|回显")
    private Integer oaLevelNum;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


    /**附加属性*/
    @TableField(exist = false)
    private Integer isActive;
    @TableField(exist = false)
    @ApiModelProperty("规则名称|回显")
    private String billCode;
    @TableField(exist = false)
    @ApiModelProperty("审批人姓名|回显")
    private String assessorName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
