package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
* 客户联系人
* @TableName cu_customer_contacts
*/
@ApiModel("客户联系人")
@TableName("cu_customer_contacts")
@Data
public class CuCustomerContacts implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 客户id
    */
    @ApiModelProperty("客户id")
    private String customerId;
    @ApiModelProperty("联系人图片")
    private String pic;
    /**
    * 联系人姓名
    */
    @ApiModelProperty("联系人姓名")
    private String contactsName;
    /**
    * 职位
    */
    @ApiModelProperty("职位")
    private String position;
    /**
    * 电话
    */
    @ApiModelProperty("电话")
    private String phoneNum;
    /**
    * WhatsApp账号
    */
    @ApiModelProperty("WhatsApp账号")
    private String whatsappAccount;
    /**
    * 其他社媒
    */
    @ApiModelProperty("其他社媒")
    private String otherSocialAccount;
    /**
    * 地址
    */
    @ApiModelProperty("地址")
    private String address;
    /**
    * 备注
    */
    @ApiModelProperty("备注")
    private String note;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ApiModelProperty("排序字段")
    private Integer sort;
    @TableField(exist = false)
    @ApiModelProperty("联系人邮箱|入参")
    private List<CuContactsEmail> emails;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
