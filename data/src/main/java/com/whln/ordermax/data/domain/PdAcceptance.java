package com.whln.ordermax.data.domain;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.FilterBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 质检单
 *
 * @TableName
 */
@ApiModel("验收单")
@TableName("pd_acceptance")
@Data
public class PdAcceptance extends FilterBill implements Serializable {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    
    private String parentBillId;
    private String supplierId;
    private String parentBillCode;


    private String parentBillNo;

    /**
     * 质检单编号
     */
    @ApiModelProperty("质检单编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;

    @TableField(exist = false)
    private String getOaPendding;
    @ApiModelProperty("外包装数量")
    private BigDecimal outPackNum;
    @ApiModelProperty("内包装数量")
    private BigDecimal inPackNum;
    @ApiModelProperty("包装状态")
    private String packStatus;

    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;
    /**
     * 生产日期
     */
    @ApiModelProperty("生产日期")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate prodDate;

    /**
     * 验收人员id
     */
    @ApiModelProperty("质检人员id")
    private String acceptanceId;

    @ApiModelProperty("批号")
    private String commodityBatchNo;

    @ApiModelProperty("简称")
    @TableField(exist = false)
    private String miniName;

    @TableField(exist = false)
    @ApiModelProperty("是否含税 0 不含1 含税")
    private Integer contactTax;

    @TableField(exist = false)
    @ApiModelProperty("元素")
    private String elements;

    @TableField(exist = false)
    @ApiModelProperty("规格配比")
    private String ratio;

    @TableField(exist = false)
    @ApiModelProperty("纯度")
    private String purity;
    /**
     * 质检部门id
     */
    @ApiModelProperty("质检部门id")
    private String acceptanceDepartmentId;
    /**
     * 生产要求
     */
    @ApiModelProperty("生产要求")
    private String productionRequested;
    /**
     * 包装要求
     */
    @ApiModelProperty("包装要求")
    private String packagingRequested;
    /**
     * 交付要求
     */
    @ApiModelProperty("交付要求")
    private String deliveryRequested;
    /**
     * 验收标准
     */
    @ApiModelProperty("验收标准")
    private String acceptanceStandard;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    @TableField(exist = false)
    private List<String> ownerIds;

    @TableField(exist = false)
    private String ownerName;
    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    @NotNull(message = "商品id不能为空")
    private String commodityId;
    /**
     * 批次号
     */
    @ApiModelProperty("批次号")
    private Integer furnaceNo;

    @ApiModelProperty("商品数量")
    private BigDecimal quotationNum;

    @ApiModelProperty("实收数量")
    private BigDecimal realReceiveNum;
    /**
     * 检验结果
     */
    @ApiModelProperty("检验结果")
    private String inspectionResult;
    /**
     * 检验方式
     */
    @ApiModelProperty("检验方式")
    private String inspectionType;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate createTime;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    @TableField(exist = false)
    @ApiModelProperty("商品体积|回显")
    private BigDecimal singleBoxVolume;

    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    @ApiModelProperty("保存的检验项目|入参")
    private List<PdAcceptanceStandard> standardList;

    @TableField(exist = false)
    @ApiModelProperty("删除的检验项目id数组|入参")
    private List<String> standardDelIdList;

    @TableField(exist = false)
    @ApiModelProperty("部门全称|回显")
    private String fullDptName;

    @TableField(exist = false)
    @ApiModelProperty("保存的批号|入参")
    private List<PdAcceptanceLot> inLotList;

    @TableField(exist = false)
    @ApiModelProperty("保存的批号|入参")
    private List<PdAcceptanceLot> outLotList;

    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private Double cmNetWeight;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;

    @TableField(exist = false)
    @ApiModelProperty("质检人员姓名|回显")
    private String qualitierName;
    @TableField(exist = false)
    @ApiModelProperty("供应商编号")
    private String supplierNo;
    @ApiModelProperty("供应商全称")
    @TableField(exist = false)
    private String oopName;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    public void initNullList(){
        if (ObjectUtil.isEmpty(this.standardList)) {
            this.standardList = new ArrayList<>(0);
        }
        if (ObjectUtil.isEmpty(this.inLotList)) {
            this.inLotList = new ArrayList<>(0);
        }
        if (ObjectUtil.isEmpty(this.outLotList)) {
            this.outLotList = new ArrayList<>(0);
        }
    }
}
