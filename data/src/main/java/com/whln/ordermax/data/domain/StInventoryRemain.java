package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
* 
* @TableName st_inventory_remain
*/
@ApiModel("库存余量")
@TableName("st_inventory_remain")
@Data
public class StInventoryRemain implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * 
    */
    private String id;
    /**
    * 商品id
    */
    @ApiModelProperty("商品id")
    private String commodityId;
    @ApiModelProperty("批号")
    private String commodityBatchNo;
    /**
    * 仓库id
    */
    @ApiModelProperty("仓库id")
    private String warehouseId;
    /**
    * 剩余数量
    */
    @ApiModelProperty("剩余数量")
    private BigDecimal num=BigDecimal.ZERO;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    private Date updateTime;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    /*附加属性*/
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    private List<String> commodityIdList;
    @TableField(exist = false)
    @ApiModelProperty("批号")
    private List<String > commodityBatchNoList;
    @TableField(exist = false)
    @ApiModelProperty("已占用数量")
    private BigDecimal grabNum;
    @TableField(exist = false)
    @ApiModelProperty("可占用数量")
    private BigDecimal grabableNum;
    @TableField(exist = false)
    @ApiModelProperty("仓库编号|回显")
    private String warehouseNo;
    @TableField(exist = false)
    @ApiModelProperty("仓库名称|回显")
    private String warehouseName;
    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;
    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品英文规格|回显")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private Double cmNetWeight;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
