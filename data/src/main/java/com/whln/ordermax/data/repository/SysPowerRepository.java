package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.SysPower;
import com.whln.ordermax.data.mapper.SysPowerMapper;

import java.util.List;
import java.util.Map;

/**
 *
 */
@Repository
public class SysPowerRepository extends ServiceImpl<SysPowerMapper, SysPower>{

    public String[] listPermissionByUserId(String userId,String category) {
        return baseMapper.listPermissionByUserId(userId,category);
    }

    public List<SysPower> listPowersByUserId(String userId) {
        return baseMapper.listPowersByUserId(userId);
    }

    public List<SysPower> listPowersByRoleId(String roleId) {
        return baseMapper.listPowersByRoleId(roleId);
    }

    public List<Map<String,String>> listNonExistentFields(String userId, String billCode) {
        return baseMapper.listNonExistentFields(userId,billCode);
    }

    public List<SysPower> listFieldByParentId(String parentId) {
        return baseMapper.listFieldByParentId(parentId);
    }

    public String getModelQueryFieldByUserId(String userId, String modelCode, String fieldSource) {
        return baseMapper.getModelQueryFieldByUserId(userId, modelCode, fieldSource);
    }
    public List<String> getModelFieldListByUserId(String userId, String modelCode, String fieldSource) {
        return baseMapper.getModelFieldListByUserId(userId, modelCode, fieldSource);
    }
    public List<Map> listFieldByParentIdRoleId(String parentId, String roleId) {
        return baseMapper.listFieldByParentIdRoleId(parentId, roleId);
    }

    public List<SysPower> listModelQueryFieldByUserId(String userId, String modelCode) {
        return baseMapper.listModelQueryFieldByUserId(userId, modelCode);
    }
}




