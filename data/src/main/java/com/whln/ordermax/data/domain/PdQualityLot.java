package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
* 质检单
* @TableName pd_production_lot
*/
@ApiModel("质检单lot")
@TableName("pd_quality_lot")
@Data
public class PdQualityLot  implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 生产任务单id
    */
    @ApiModelProperty("质检单id")
    private String billId;
    /**
    * 班次
    */
    @ApiModelProperty("班次")
    private String teamNo;
    /**
    * 产线
    */
    @ApiModelProperty("产线")
    private String line;
    /**
    * 生产日期
    */
    @ApiModelProperty("生产日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate prodDate;
    /**
    * 炉次
    */
    @ApiModelProperty("炉次")
    private Integer furnaceNo;
    /**
    * 瓶次
    */
    @ApiModelProperty("瓶次")
    private Integer bottNo;
    @ApiModelProperty("每箱数量")
    private Integer qty;
    @ApiModelProperty("标签号")
    private String labelNo;

    /**
    * lot瓶号
    */
    @ApiModelProperty("lot瓶号")
    private String lotNo;

    @TableField(exist = false)
    @ApiModelProperty("二维码")
    private String qrcode;


    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
