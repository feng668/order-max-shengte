package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.StInOrderCommodity;
import com.whln.ordermax.data.mapper.StInOrderCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class StInOrderCommodityRepository extends ServiceImpl<StInOrderCommodityMapper, StInOrderCommodity>{

    public IPage<StInOrderCommodity> pageByEntity(StInOrderCommodity entity, Page<StInOrderCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<StInOrderCommodity> listByEntity(StInOrderCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<StInOrderCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




