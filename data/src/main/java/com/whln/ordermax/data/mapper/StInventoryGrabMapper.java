package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.StInventoryGrab;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.ordermax.data.domain.StInventoryGrab
 */
public interface StInventoryGrabMapper extends BaseMapper<StInventoryGrab> {

    List<StInventoryGrab> listByEntity(@Param("entity")StInventoryGrab entity);

    IPage<StInventoryGrab> listByEntity(@Param("entity")StInventoryGrab entity, Page<StInventoryGrab> page);

    StInventoryGrab getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<StInventoryGrab> entitylist);

    List<StInventoryGrab> listComGrabByTransportationId(@Param("billId")String billId, @Param("commodityId")String commodityId);
}




