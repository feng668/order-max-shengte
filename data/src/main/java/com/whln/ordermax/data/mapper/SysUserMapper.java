package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.SysUser;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.SysUser
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> listByEntity(@Param("entity")SysUser entity);

    IPage<SysUser> listByEntity(@Param("entity")SysUser entity, Page<SysUser> page);

    SysUser getUserByUserId(@Param("userId")String userId);
}




