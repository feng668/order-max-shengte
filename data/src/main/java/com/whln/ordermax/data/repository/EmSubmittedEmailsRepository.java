package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.EmSubmittedEmails;
import com.whln.ordermax.data.mapper.EmSubmittedEmailsMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class EmSubmittedEmailsRepository extends ServiceImpl<EmSubmittedEmailsMapper, EmSubmittedEmails>{

    public IPage<EmSubmittedEmails> pageByEntity(EmSubmittedEmails entity, Page<EmSubmittedEmails> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<EmSubmittedEmails> listByEntity(EmSubmittedEmails entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<EmSubmittedEmails> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




