package com.whln.ordermax.data.domain.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author liurun
 * @date 2023-03-14 14:50
 */
@Data
public class ListLogisticsStatVO implements Serializable {
    private static final long serialVersionUID = -3593969714829058352L;
    private String billNo;
    private String traceNo;
    private String custCnName;
    private String ownerName;
    private String soodOrderNo;
    private String fullDptName;
    private String forwarderCompanyName;
    private LocalDateTime createDate;
}
