package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whln.ordermax.data.domain.CustomerLabelRelation;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liurun
 * @date 2023-03-06 16:12
 */
@Mapper
public interface UserLabelRelationMapper extends BaseMapper<CustomerLabelRelation> {
}
