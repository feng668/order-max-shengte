package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.FilterOaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 采购合同
 *
 * @TableName po_contract
 */
@ApiModel("采购合同")
@TableName("po_contract")
@Data
public class PoContract extends FilterOaBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;

    private String parentBillId;

    private String parentBillCode;
    private String parentBillNo;

    private String flowStatus;
    @TableField(exist = false)
    private String commodityType;

    /**
     * 申请单id
     */
    @ApiModelProperty("申请单id")
    private String requestId;

    @ApiModelProperty("预计完成时间")
    private LocalDateTime estimateDate;
    /**
     * 合同编号
     */
    @ApiModelProperty("合同编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;
    @ApiModelProperty("交货日期")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate deliveryDate;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;
    /**
     * 供应商id
     */
    @ApiModelProperty("供应商id")
    private String supplierId;

    @ApiModelProperty("是否开发票， 0未开 1已开")
    private Integer invoiceStatus;
    /**
     * 公司id
     */
    @ApiModelProperty("公司id")
    private String companyId;
    /**
     * 公司人员id
     */
    @ApiModelProperty("公司人员id")
    private String companyPersonnelId;
    /**
     * 创建人id
     */
    @ApiModelProperty("创建人id")
    private String createrId;
    /**
     * 创建日期
     */
    @ApiModelProperty("创建日期")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
     * 客户订单
     */
    @ApiModelProperty("客户订单")
    private String customerOrderNo;
    /**
     * 合同总金额（本币）
     */
    @ApiModelProperty("合同总金额（本币）")
    private Double localCurTotalAmo;

    @ApiModelProperty("单据来源 内销，外销")
    private String billSource;
    /**
     * 付款方式
     */
    @ApiModelProperty("付款方式")
    private String payType;
    /**
     * 合同总数量
     */
    @ApiModelProperty("合同总数量")
    private Integer totalNum;
    @ApiModelProperty("供应商编号")
    @TableField(exist = false)
    private String supplierNo;
    /**
     * 合同总体积
     */
    @ApiModelProperty("合同总体积")
    private Double totalVolume;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 生产要求
     */
    @ApiModelProperty("生产要求")
    private String productionRequested;
    /**
     * 包装要求
     */
    @ApiModelProperty("包装要求")
    private String packagingRequested;
    /**
     * 交付要求
     */
    @ApiModelProperty("交付要求")
    private String deliveryRequested;
    /**
     * 验收标准
     */
    @ApiModelProperty("验收标准")
    private String acceptanceStandard;
    @TableField(exist = false)
    private String getOaPendding;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;

    @ApiModelProperty("结算时间和结算方式")
    private String settlementMethodDate;
    @ApiModelProperty("纠纷解决")
    private String dispute;
    @ApiModelProperty("生效条件")
    private String effectiveConditions;
    @ApiModelProperty("交付条件")
    private String deliverConditions;
    @ApiModelProperty("时间运输方式及费运负担验收标准")
    private String dateTranMethod;
    @ApiModelProperty("地点和期限")
    private String placeTerm;

    /**
     * 送货地址
     */
    @ApiModelProperty("送货地址")
    private String address;
    /**
     * 是否付款
     */
    @ApiModelProperty("是否付款")
    private Integer isPayment;
    @ApiModelProperty("起运港")
    private String initiallyPort;
    @ApiModelProperty("目的港")
    private String finallyPort;
    @ApiModelProperty("佣金金额")
    private Double brokerageAmo;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;

    @TableField(exist = false)
    private List<String> idNotIn;
    @TableField(exist = false)
    @ApiModelProperty("保存的合同商品|入参")
    private List<PoContractCommodity> commodityList;
    @TableField(exist = false)
    @ApiModelProperty("删除的报价单商品id数组|入参")
    private List<String> commodityDelIdList;
    @TableField(exist = false)
    private List<String> idIn;

    @TableField(exist = false)
    @ApiModelProperty("供应商中文名称|回显")
    private String supName;
    @TableField(exist = false)
    @ApiModelProperty("供应商地址|回显")
    private String supAddress;
    @TableField(exist = false)
    @ApiModelProperty("供应商电话|回显")
    private String supPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("供应商开户行|回显")
    private String supBank;
    @TableField(exist = false)
    @ApiModelProperty("供应商银行账号|回显")
    private String supBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("供应商邮编|回显")
    private String suptZipCode;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司邮编|回显")
    private String cpnZipCode;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("创建人姓名|回显")
    private String createrName;
    @TableField(exist = false)
    private List<String> ownerIds;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
