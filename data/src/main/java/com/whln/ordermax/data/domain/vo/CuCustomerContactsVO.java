package com.whln.ordermax.data.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-20 12:09
 */

@ApiModel("客户联系人")
@Data
public class CuCustomerContactsVO {

    /**
     * id
     */
    private String id;
    /**
     * 客户id
     */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
     * 联系人姓名
     */
    @ApiModelProperty("联系人姓名")
    private String contactsName;
    /**
     * 职位
     */
    @ApiModelProperty("职位")
    private String position;
    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phoneNum;
    /**
     * WhatsApp账号
     */
    @ApiModelProperty("WhatsApp账号")
    private String whatsappAccount;
    /**
     * 其他社媒
     */
    @ApiModelProperty("其他社媒")
    private String otherSocialAccount;
    /**
     * 地址
     */
    @ApiModelProperty("地址")
    private String address;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @ApiModelProperty("排序字段")
    private Integer sort;
    private List<CuContactsEmailVO> emails;
}
