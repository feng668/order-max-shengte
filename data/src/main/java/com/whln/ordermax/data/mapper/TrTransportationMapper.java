package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.TrTransportation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.TrTransportation
 */
public interface TrTransportationMapper extends BaseMapper<TrTransportation> {

    List<TrTransportation> listByEntity(@Param("entity")TrTransportation entity);

    IPage<TrTransportation> listByEntity(@Param("entity")TrTransportation entity, Page<TrTransportation> page);

    Integer insertBatch(@Param("entitylist")List<TrTransportation> entitylist);

    TrTransportation getAllById(String id);
}




