package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.OaRuleLevelAssessor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.OaRuleLevelAssessor
 */
public interface OaRuleLevelAssessorMapper extends BaseMapper<OaRuleLevelAssessor> {

    List<OaRuleLevelAssessor> listByEntity(@Param("entity")OaRuleLevelAssessor entity);

    IPage<OaRuleLevelAssessor> listByEntity(@Param("entity")OaRuleLevelAssessor entity, Page<OaRuleLevelAssessor> page);

    Integer insertBatch(@Param("entitylist")List<OaRuleLevelAssessor> entitylist);

}




