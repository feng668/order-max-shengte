package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PdProductTaskCommodity;
import com.whln.ordermax.data.domain.param.ProductionTaskBomParam;
import com.whln.ordermax.data.mapper.PdProductTaskCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class PdProductTaskCommodityRepository extends ServiceImpl<PdProductTaskCommodityMapper, PdProductTaskCommodity>{

    public IPage<PdProductTaskCommodity> pageByEntity(PdProductTaskCommodity entity, Page<PdProductTaskCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PdProductTaskCommodity> listByEntity(PdProductTaskCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PdProductTaskCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    public List<PdProductTaskCommodity> getProductionTaskBom(ProductionTaskBomParam param) {
       return baseMapper.getProductionTaskBom(param);
    }
}




