package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author liurun
 * @date 2023-03-14 11:17
 */
@Data
@Builder
@ApiModel("选择仓库返回参数")
public class CommodityBaseNumVO implements Serializable {
    private static final long serialVersionUID = -9122378917291375579L;
    @ApiModelProperty(value = "总数")
    private Integer total;
    @ApiModelProperty("商品")
    private List<CmCommodityVO> list;

}
