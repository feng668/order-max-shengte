package com.whln.ordermax.data.repository;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.common.enums.RedisKey;
import com.whln.ordermax.common.redis.RedisClient;
import com.whln.ordermax.data.domain.SysDict;
import com.whln.ordermax.data.domain.SysDictDetail;
import com.whln.ordermax.data.mapper.SysDictMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liurun
 * @since 2023-04-21
 */
@Service
@AllArgsConstructor
public class SysDictRepository extends ServiceImpl<SysDictMapper, SysDict> {
    private final SysDictDetailRepository sysDictDetailRepository;
    private final RedisClient<String, SysDict> redisClient;
    private final RedisClient<String, SysDictDetail> redisClientDetail;

    public List<SysDict> listSysDict() {
        List<SysDict> sysDictList = redisClient.lRange(RedisKey.SYS_DICT_ALL.getKey(), 0, -1);
        if (ObjectUtil.isEmpty(sysDictList)) {
            sysDictList = this.list();
            for (SysDict sysDict : sysDictList) {
                redisClient.rPush(RedisKey.SYS_DICT_ALL.getKey(), sysDict);
            }
        }

        List<SysDictDetail> sysDictDetailList = sysDictDetailRepository.list();
        if (ObjectUtil.isEmpty(sysDictDetailList)) {
            sysDictDetailList = sysDictDetailRepository.list();
            for (SysDictDetail sysDictDetail : sysDictDetailList) {
                redisClientDetail.rPush(RedisKey.SYS_DICT_DETAIL_ALL.getKey(), sysDictDetail);
            }
        }

        Map<String, List<SysDictDetail>> collect = sysDictDetailList.stream().collect(Collectors.groupingBy(SysDictDetail::getDictId));
        for (SysDict sysDict : sysDictList) {
            List<SysDictDetail> sysDictDetails = collect.get(sysDict.getId());
            if (ObjectUtil.isNotEmpty(sysDictDetails)) {
                sysDict.setDictDetailList(sysDictDetails);
            }
        }
        return sysDictList;
    }
}
