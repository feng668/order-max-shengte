package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.OaRuleLevel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.OaRuleLevel
 */
public interface OaRuleLevelMapper extends BaseMapper<OaRuleLevel> {

    List<OaRuleLevel> listByEntity(@Param("entity")OaRuleLevel entity);

    IPage<OaRuleLevel> listByEntity(@Param("entity")OaRuleLevel entity, Page<OaRuleLevel> page);

    Integer insertBatch(@Param("entitylist")List<OaRuleLevel> entitylist);

}




