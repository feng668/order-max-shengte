package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
* 邮箱附件
* @TableName em_email_attachment
*/
@ApiModel("邮箱附件")
@TableName("em_email_attachment")
@Data
public class EmEmailAttachment implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    /**
    * 绑定id
    */
    @ApiModelProperty("绑定id")
    private String bindId;
    /**
    * 附件名称
    */
    @ApiModelProperty("附件名称")
    private String name;
    /**
    * 路径
    */
    @ApiModelProperty("路径")
    private String path;
    /**
    * 文件类型
    */
    @ApiModelProperty("文件类型")
    private String fileType;
    /**
    * 绑定类型
    */
    @ApiModelProperty("绑定类型")
    private String bindType;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
