package com.whln.ordermax.data.mapper;

import org.apache.ibatis.annotations.ResultMap;
import com.whln.ordermax.data.domain.FnProfit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.FnProfit
 */
public interface FnProfitMapper extends BaseMapper<FnProfit> {

    List<FnProfit> listByEntity(@Param("entity")FnProfit entity);

    @ResultMap("mybatis-plus_FnProfit")
    IPage<FnProfit> listByEntity(@Param("entity")FnProfit entity, Page<FnProfit> page);

    Integer insertBatch(@Param("entitylist")List<FnProfit> entitylist);

}




