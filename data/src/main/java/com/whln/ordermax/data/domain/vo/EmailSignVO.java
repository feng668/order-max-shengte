package com.whln.ordermax.data.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author liurun
 * @date 2023-02-21 18:51
 */
@Data
@ApiModel("邮箱签名")
public class EmailSignVO {
    @ApiModelProperty("id")
    private String id;
    @NotBlank(message = "标签必须关联员工")
    @ApiModelProperty("签名关联的id")
    private String relationId;
    @ApiModelProperty("标题")
    private String title;
    @ApiModelProperty("签名内容")
    private String sign;
    @ApiModelProperty("html内容")
    private String html;
    @ApiModelProperty("字段名和对应的值")
    List<SignResult> result;
}
