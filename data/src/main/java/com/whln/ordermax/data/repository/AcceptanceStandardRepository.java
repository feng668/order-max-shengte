package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.PdAcceptanceStandard;
import com.whln.ordermax.data.mapper.AcceptanceStandardMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public class AcceptanceStandardRepository extends ServiceImpl<AcceptanceStandardMapper, PdAcceptanceStandard>{

    public IPage<PdAcceptanceStandard> pageByEntity(PdAcceptanceStandard entity, Page<PdAcceptanceStandard> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PdAcceptanceStandard> listByEntity(PdAcceptanceStandard entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<PdAcceptanceStandard> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




