package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.EoEnquiryOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.EoEnquiryOrder
 */
public interface EoEnquiryOrderMapper extends BaseMapper<EoEnquiryOrder> {

    List<EoEnquiryOrder> listByEntity(@Param("entity")EoEnquiryOrder entity);

    IPage<EoEnquiryOrder> listByEntity(@Param("entity")EoEnquiryOrder entity, Page<EoEnquiryOrder> page);

    EoEnquiryOrder getAllById(@Param("id")String id);

    Integer insertBatch(@Param("entitylist")List<EoEnquiryOrder> entitylist);

}




