package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.TrTransportation;
import com.whln.ordermax.data.domain.TrTransportationCommodity;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.TrTransportationCommodityMapper;
import com.whln.ordermax.data.mapper.TrTransportationMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class TrTransportationRepository extends ServiceImpl<TrTransportationMapper, TrTransportation>{

    public IPage<TrTransportation> pageByEntity(TrTransportation entity, Page<TrTransportation> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<TrTransportation> listByEntity(TrTransportation entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<TrTransportation> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

    @Resource
    private TrTransportationCommodityMapper trTransportationCommodityMapper;

    public TrTransportation getAllById(String id) {
        TrTransportation trTransportation = baseMapper.getAllById(id);
        List<TrTransportationCommodity> trTransportationCommodities = trTransportationCommodityMapper.commodityList(BillQuery.builder()
                .billIds(CollUtils.toSet(trTransportation.getId())).build());
        trTransportation.setCommodityList(trTransportationCommodities);
        return trTransportation;
    }

}




