package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.whln.ordermax.data.domain.base.FilterOaBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
* 出库单
* @TableName so_si_order
*/
@ApiModel("内销订单")
@TableName("so_si_order")
@Data
public class SoSiOrder extends FilterOaBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    @ApiModelProperty("是否开发票， 0未开 1已开")
    private Integer invoiceStatus;
    private Integer status;
    private String parentBillId;
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;
    private String parentBillCode;
    
    private String parentBillNo;
    @TableField(exist = false)
    private String cpnSwift;
    @ApiModelProperty("银行地址")
    @TableField(exist = false)
    private String cpnBankAdd;
    /**
    * 合同编号
    */
    @ApiModelProperty("合同编号")
    @NotBlank(message = "编号不能为空")
    private String billNo;

    @TableField(exist = false)
    private String flowBillCode;
    /**
    * 创建日期
    */
    @ApiModelProperty("创建日期")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate createDate;
    /**
    * 报价单id
    */
    @ApiModelProperty("报价单id")
    private String sourceBillId;

    @TableField(exist = false)
    private String sourceBillCode;

    /**
    * 结算币种
    */
    @ApiModelProperty("结算币种")
    private String currency;
    @ApiModelProperty("销项税总计")
    private BigDecimal totalSales;
    /**
    * 汇率
    */
    @ApiModelProperty("汇率")
    private BigDecimal rate;
    /**
    * 合同总金额（原币）
    */
    @ApiModelProperty("合同总金额（原币）")
    private BigDecimal originalCurTotalAmo;
    /**
    * 合同总金额（本币）
    */
    @ApiModelProperty("合同总金额（本币）")
    private BigDecimal localCurTotalAmo;
    /**
    * 价格条款
    */
    @ApiModelProperty("价格条款")
    private String priceTerms;
    /**
    * 付款方式
    */
    @ApiModelProperty("付款方式")
    private String paymentType;
    /**
    * 起运港
    */
    @ApiModelProperty("起运港")
    private String initiallyPort;
    /**
    * 目的港
    */
    @ApiModelProperty("目的港")
    private String finallyPort;
    /**
    * 运输方式
    */
    @ApiModelProperty("运输方式")
    private String transportType;
    /**
    * 总数量
    */
    @ApiModelProperty("总数量")
    private Integer totalNum;
    /**
    * 总体积
    */
    @ApiModelProperty("总体积")
    private BigDecimal totalVolume;
    /**
    * 柜型
    */
    @ApiModelProperty("柜型")
    private String cabinetModel;
    /**
    * 装柜百分比
    */
    @ApiModelProperty("装柜百分比")
    private BigDecimal loadingPercentage;
    /**
    * 备注
    */
    @ApiModelProperty("备注")
    private String note;
    /**
    * 客户id
    */
    @ApiModelProperty("客户id")
    private String customerId;
    /**
    * 公司id
    */
    @ApiModelProperty("公司id")
    private String companyId;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;
    @TableField(exist = false)
    private String billCode;
    @TableField(exist = false)
    private String billId;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    @TableField(exist = false)
    private List<String> ownerIds;
    /**
    * 生产要求
    */
    @ApiModelProperty("生产要求")
    private String productionRequested;
    /**
    * 包装要求
    */
    @ApiModelProperty("包装要求")
    private String packagingRequested;
    /**
    * 交付要求
    */
    @ApiModelProperty("交付要求")
    private String deliveryRequested;
    /**
    * 验收标准
    */
    @ApiModelProperty("验收标准")
    private String acceptanceStandard;
    /**
    * 佣金金额
    */
    @ApiModelProperty("佣金金额")
    private BigDecimal commissionAmo;
    /**
    * 客户收货地址
    */
    @ApiModelProperty("客户收货地址")
    private String custShippingAddress;


    /* 附加信息 */
    @TableField(exist = false)
    @ApiModelProperty("保存的报价单商品|入参")
    private List<SoSiOrderCommodity> commodityList;
    @TableField(exist = false)
    private List<FlFlowBillCommodity> commodityFlowList;
    @TableField(exist = false)
    @ApiModelProperty("删除的报价单商品id数组|入参")
    private List<String> commodityDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("客户中文名称|回显")
    private String custCnName;
    @TableField(exist = false)
    @ApiModelProperty("客户地址|回显")
    private String custAddress;
    @TableField(exist = false)
    @ApiModelProperty("客户电话|回显")
    private String custPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("客户开户行|回显")
    private String custBank;
    @TableField(exist = false)
    @ApiModelProperty("客户银行账号|回显")
    private String custBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("客户邮编|回显")
    private String custZipCode;
    @TableField(exist = false)
    @ApiModelProperty("公司中文名称|回显")
    private String cpnCnName;
    @TableField(exist = false)
    @ApiModelProperty("银行行号")
    private String cpnBankNo;
    @TableField(exist = false)
    @ApiModelProperty("公司地址|回显")
    private String cpnAddress;
    @TableField(exist = false)
    @ApiModelProperty("公司电话|回显")
    private String cpnPhoneNum;
    @TableField(exist = false)
    @ApiModelProperty("公司开户行|回显")
    private String cpnBank;
    @TableField(exist = false)
    @ApiModelProperty("公司银行账号|回显")
    private String cpnBankAccount;
    @TableField(exist = false)
    @ApiModelProperty("公司邮编|回显")
    private String cpnZipCode;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("报价单号|回显")
    private String sourceBillNo;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    @JsonIgnore
    @ApiModelProperty("单据总数|回显")
    @TableField(exist = false)
    private int orderNum=1;
    @TableField(exist = false)
    @ApiModelProperty("单据总金额|回显")
    @JsonIgnore
    private BigDecimal orderAmount;
    @JsonIgnore
    @TableField(exist = false)
    @ApiModelProperty("出运数量|回显")
    private int transNum;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    @JsonIgnore
    @TableField(exist = false)
    @ApiModelProperty("出运总金额|回显")
    private BigDecimal transAmount;
    public void setSourceBillId(String sourceBillId) {
        this.sourceBillId = sourceBillId;
        this.parentBillId = sourceBillId;
    }

}
