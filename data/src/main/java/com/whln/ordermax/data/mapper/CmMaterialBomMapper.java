package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.whln.ordermax.data.domain.CmMaterialBom;

public interface CmMaterialBomMapper extends BaseMapper<CmMaterialBom> {
}
