package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.PdProductTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PdProductTask
 */
public interface PdProductTaskMapper extends BaseMapper<PdProductTask> {

    List<PdProductTask> listByEntity(@Param("entity")PdProductTask entity);

    IPage<PdProductTask> listByEntity(@Param("entity")PdProductTask entity, Page<PdProductTask> page);

    Integer insertBatch(@Param("entitylist")List<PdProductTask> entitylist);

}




