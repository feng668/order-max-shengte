package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.NrRoleNode;
import com.whln.ordermax.data.mapper.NrRoleNodeMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class NrRoleNodeRepository extends ServiceImpl<NrRoleNodeMapper, NrRoleNode>{

    public IPage<NrRoleNode> pageByEntity(NrRoleNode entity, Page<NrRoleNode> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<NrRoleNode> listByEntity(NrRoleNode entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<NrRoleNode> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




