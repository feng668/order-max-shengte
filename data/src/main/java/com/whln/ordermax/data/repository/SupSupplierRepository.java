package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Repository;
import com.whln.ordermax.data.domain.SupSupplier;
import com.whln.ordermax.data.mapper.SupSupplierMapper;

import java.util.List;

/**
 *
 */
@Repository
public class SupSupplierRepository extends ServiceImpl<SupSupplierMapper, SupSupplier>{

    public IPage<SupSupplier> pageByEntity(SupSupplier entity, Page<SupSupplier> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<SupSupplier> listByEntity(SupSupplier entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<SupSupplier> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




