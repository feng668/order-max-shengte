package com.whln.ordermax.data.domain.param;

import com.whln.ordermax.data.domain.SysUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author liurun
 * @date 2023-03-28 13:52
 */
@Data
public class SysUserImportVO {
    @ApiModelProperty("已存在的")
    private List<SysUser> exits;
    @ApiModelProperty("不存在的")
    private List<SysUser> noExits;
    @ApiModelProperty("已存在的数量")
    private Integer exitsNum;
    @ApiModelProperty("不存在的数量")
    private Integer noExitsNum;
}
