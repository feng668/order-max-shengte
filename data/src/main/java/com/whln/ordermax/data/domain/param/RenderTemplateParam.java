package com.whln.ordermax.data.domain.param;

import com.whln.ordermax.data.domain.TmPrintTemplate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-02-22 14:18
 */
@Data
@ApiModel("渲染打印模板")
public class RenderTemplateParam {

    @ApiModelProperty("打印模板id")
    private String templateId;

    @ApiModelProperty("单据的id")
    private String id;
    private TmPrintTemplate templateData;
}
