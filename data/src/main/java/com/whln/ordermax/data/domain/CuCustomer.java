package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.whln.ordermax.data.domain.base.FilterBill;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 客户
 *
 * @TableName cu_customer
 */
@ApiModel("客户")
@TableName("cu_customer")
@Data
public class CuCustomer extends FilterBill implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * id
     */
    private String id;
    /**
     * 编号
     */
    @ApiModelProperty("编号")
    @NotBlank(message = "客户编号不能为空")
    private String customerNo;
    @ApiModelProperty("内销、外销")
    private String typeName;
    @ApiModelProperty("内销1、外销2")
    private Integer type;
    /**
     * 中文名称
     */
    @ApiModelProperty("中文名称")
    private String cnName;
    /**
     * 英文名称
     */
    @ApiModelProperty("英文名称")
    private String enNames;
    /**
     * 等级
     */
    @ApiModelProperty("等级")
    private Integer level;
    @ApiModelProperty("等级中文显示")
    private String levelName;
    /**
     * 地址
     */
    @ApiModelProperty("公司地址")
    private String address;
    @ApiModelProperty("单据地址")
    private String billAddress;
    @ApiModelProperty("收获地址")
    private String harvestAddress;
    /**
     * 类别
     */
    @ApiModelProperty("类别")
    private Integer category;
    @ApiModelProperty("类别")
    private String categoryName;
    /**
     * 所属国家
     */
    @ApiModelProperty("所属国家")
    private String country;
    /**
     * 城市
     */
    @ApiModelProperty("城市")
    private String city;
    /**
     * 所属业务员id
     */
    @ApiModelProperty("所属业务员id")
    private String salesmanId;
    /**
     * 电话
     */
    @ApiModelProperty("电话")
    private String phoneNum;
    /**
     * 来源
     */
    @ApiModelProperty("来源")
    private String source;
    @ApiModelProperty("来源中文显示")
    private String sourceName;
    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String note;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     * 拥有人id
     */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    /**
     * 客户简称
     */
    @ApiModelProperty("客户简称")
    private String miniName;
    /**
     * 客户状态
     */
    @ApiModelProperty("客户状态")
    private Integer status;
    @ApiModelProperty("是否删除 0 未删除 1已删除")
    private Integer isDelete;
    @ApiModelProperty("客户状态中文显示")
    private String statusName;
    /**
     * 网页
     */
    @ApiModelProperty("网页")
    private String website;
    /**
     * 传真
     */
    @ApiModelProperty("传真")
    private String fax;
    /**
     * 邮编
     */
    @ApiModelProperty("邮编")
    private String zipCode;
    @ApiModelProperty("税号")
    private String dutyNo;
    @ApiModelProperty("发票公司名称")
    private String invoiceCnName;
    /**
     * 开户银行
     */
    @ApiModelProperty("开户银行")
    private String bank;
    /**
     * 银行账号
     */
    @ApiModelProperty("银行账号")
    private String bankAccount;
    @ApiModelProperty("银行税号")
    private String bankDutyNo;
    @ApiModelProperty("头像")
    private String portrait;
    private LocalDateTime lastOwnerTime;
    private Integer isPub;
    @ApiModelProperty("宗教节日")
    private String religiousFestivals;
    @ApiModelProperty("信用额度")
    private BigDecimal creditLine;
    @ApiModelProperty("销售额")
    private BigDecimal saleAmo;
    @ApiModelProperty("利润")
    private BigDecimal saleProfit;
    @ApiModelProperty("预计销售额")
    private BigDecimal futureSaleAmo;
    @ApiModelProperty("预计利润")
    private BigDecimal futureSaleProfit;
    @ApiModelProperty("行业影响力")
    private Integer industryForce;
    @ApiModelProperty("预计赢单率")
    private BigDecimal futureWinRatio;




    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("搜索参数|入参")
    private String searchInfo;
    @TableField(exist = false)
    private String billNo;

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
        this.billNo = customerNo;
    }

    @TableField(exist = false)
    @ApiModelProperty("归属类型|回显")
    private String ownerType;
    @TableField(exist = false)
    @ApiModelProperty("业务员姓名|回显")
    private String salesmanName;
    @TableField(exist = false)
    @ApiModelProperty("拥有人姓名|回显")
    private String ownerName;
    @TableField(exist = false)
    @ApiModelProperty("下单次数|回显")
    private Integer saleCount;
    @TableField(exist = false)
    @ApiModelProperty("合同数量|回显")
    private int orderNum;
    /**
     * 合同总金额（原币）
     */
    @ApiModelProperty("合同总金额（原币）")
    @TableField(exist = false)
    private BigDecimal originalCurTotalAmo;
    /**
     * 合同总金额（本币）
     */
    @ApiModelProperty("合同总金额（本币）")
    @TableField(exist = false)
    private BigDecimal localCurTotalAmo;

    /*@TableField(exist = false)
    @ApiModelProperty("出运数量|回显")
    private int transNum;*/

    @TableField(exist = false)
    @ApiModelProperty("出运总金额(原币)|回显")
    private BigDecimal transAmountOrigin;

    @TableField(exist = false)
    @ApiModelProperty("出运总金额(本币)|回显")
    private BigDecimal transAmountLocal;

    @TableField(exist = false)
    private Boolean ownerIdIsNull;

    @TableField(exist = false)
    @ApiModelProperty("联系人|入参")
    private List<CuCustomerContacts> contactsList;
    @TableField(exist = false)
    @ApiModelProperty("删除联系人id|入参")
    private List<String> contactsDelIdList;
    @TableField(exist = false)
    @ApiModelProperty("联系人邮箱|入参")
    private List<CuContactsEmail> emails;
    @TableField(exist = false)
    @ApiModelProperty("删除邮箱id|入参")
    private List<String> emailDelIds;
    @TableField(exist = false)
    private String labelName;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
