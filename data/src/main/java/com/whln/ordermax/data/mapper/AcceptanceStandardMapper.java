package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.PdAcceptanceStandard;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.PdQualityStandard
 */
public interface AcceptanceStandardMapper extends BaseMapper<PdAcceptanceStandard> {

    List<PdAcceptanceStandard> listByEntity(@Param("entity") PdAcceptanceStandard entity);

    IPage<PdAcceptanceStandard> listByEntity(@Param("entity") PdAcceptanceStandard entity, Page<PdAcceptanceStandard> page);

    Integer insertBatch(@Param("entitylist")List<PdAcceptanceStandard> entitylist);

}




