package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.FnPaymentRequest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.FnPayment
 */
public interface FnPaymentRequestMapper extends BaseMapper<FnPaymentRequest> {

    List<FnPaymentRequest> listByEntity(@Param("entity") FnPaymentRequest entity);

    IPage<FnPaymentRequest> listByEntity(@Param("entity") FnPaymentRequest entity, Page<FnPaymentRequest> page);

    Integer insertBatch(@Param("entitylist")List<FnPaymentRequest> entitylist);

}




