package com.whln.ordermax.data.mapper;

import com.whln.ordermax.common.entity.quote.QoCommodityHistoryQueteVo;
import com.whln.ordermax.data.domain.QoInQuoteCommodity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.whln.ordermax.data.domain.vo.CustomerHistoryVO;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.query.BillQuery;

import java.util.List;
import java.util.Map;

/**
 * @Entity whln.dyx.data.domain.QoInQuoteCommodity
 */
public interface QoInQuoteCommodityMapper extends BaseMapper<QoInQuoteCommodity> {

    List<QoInQuoteCommodity> listByEntity(@Param("entity")QoInQuoteCommodity entity);

    IPage<QoInQuoteCommodity> listByEntity(@Param("entity")QoInQuoteCommodity entity, Page<QoInQuoteCommodity> page);

    Integer insertBatch(@Param("entitylist")List<QoInQuoteCommodity> entitylist);

    List<Map> getMapByIdWithInfo(@Param("queteId")String queteId);

    List<QoCommodityHistoryQueteVo> listHistoryPriceByComIdCustId(@Param("customerId") String customerId, @Param("commodityId") String commodityId);

    Double getLastPriceByComIdAndCustId(@Param("customerId") String customerId, @Param("commodityId") String commodityId);

    List<QoInQuoteCommodity> commodityList(BillQuery build);

    List<CustomerHistoryVO> getByCustomerId(@Param("id") String id, @Param("searchInfo") String searchInfo);
}




