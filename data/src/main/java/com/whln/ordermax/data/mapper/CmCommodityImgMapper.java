package com.whln.ordermax.data.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.whln.ordermax.data.domain.CmCommodityImg;

import java.util.List;

/**
 * @Entity whln.dyx.data.domain.CmCommodityImg
 */
public interface CmCommodityImgMapper extends BaseMapper<CmCommodityImg> {

    List<CmCommodityImg> listByEntity(@Param("entity")CmCommodityImg entity);

    IPage<CmCommodityImg> listByEntity(@Param("entity")CmCommodityImg entity, Page<CmCommodityImg> page);

    Integer insertBatch(@Param("entitylist")List<CmCommodityImg> entitylist);

}




