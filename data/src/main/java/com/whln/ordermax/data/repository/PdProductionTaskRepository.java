package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.common.utils.CollUtils;
import com.whln.ordermax.data.domain.PdProductionCommodityBom;
import com.whln.ordermax.data.domain.PdProductionTask;
import com.whln.ordermax.data.domain.query.BillQuery;
import com.whln.ordermax.data.mapper.PdProductionCommodityBomMapper;
import com.whln.ordermax.data.mapper.PdProductionTaskMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
public class PdProductionTaskRepository extends ServiceImpl<PdProductionTaskMapper, PdProductionTask>{
    @Resource
    PdProductionCommodityBomMapper pdProductionCommodityBomMapper;
    public IPage<PdProductionTask> pageByEntity(PdProductionTask entity, Page<PdProductionTask> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<PdProductionTask> listByEntity(PdProductionTask entity) {
        return baseMapper.listByEntity(entity);
    }

    public PdProductionTask getAllById(String id) {
        PdProductionTask pdProductionTask = baseMapper.getAllById(id);
        List<PdProductionCommodityBom> list = pdProductionCommodityBomMapper.commodityList(BillQuery.builder()
                .billIds(CollUtils.toSet(pdProductionTask.getId()))
                .build());
        pdProductionTask.setCommodityList(list);
        return pdProductionTask;

    }

}




