package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-02-23 20:57
 */
@ApiModel("文件下载参数")
@Data
public class FileDownloadParam {
    @ApiModelProperty("文件路径")
    private String filePath;
}
