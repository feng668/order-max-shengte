package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @TableName sys_user_email
 */
@ApiModel("用户邮件")
@TableName("sys_user_email")
@Data
public class SysUserEmail implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
     * 主键
     */
    private String id;
    /**
     * 用户id
     */
    @ApiModelProperty("用户id")
    private String userId;
    /**
     * 邮件
     */
    @ApiModelProperty("邮件")
    private String email;
    /**
     * 邮件授权码
     */
    @ApiModelProperty("邮件授权码")
    private String emailPassword;
    @ApiModelProperty("收件箱保留时间FOREVER永久，其他传毫秒")
    private String retentionTime;
    @ApiModelProperty("邮件同步方式")
    private String syncType;
    @ApiModelProperty("是否开始同步")
    private Integer sycnStart;
    @ApiModelProperty("同步时间间隔")
    private Long syncInterval;
    @ApiModelProperty("同步周期（天）")
    private Integer syncCycleDay;

    /**
     *
     */
    @ApiModelProperty("")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
     *
     */
    @ApiModelProperty("")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
     *
     */
    @ApiModelProperty("")
    private Integer isDefault;


    /*附加属性*/
    @ApiModelProperty("用户姓名")
    @TableField(exist = false)
    private String userName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
