package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.TmPrintTemplate;
import com.whln.ordermax.data.mapper.TmPrintTemplateMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class TmPrintTemplateRepository extends ServiceImpl<TmPrintTemplateMapper, TmPrintTemplate>{

    public IPage<TmPrintTemplate> pageByEntity(TmPrintTemplate entity, Page<TmPrintTemplate> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<TmPrintTemplate> listByEntity(TmPrintTemplate entity) {
        return baseMapper.listByEntity(entity);
    }

    public TmPrintTemplate getAllById(String id) {
        return baseMapper.getAllById(id);
    }

    public Boolean insertBatch(List<TmPrintTemplate> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




