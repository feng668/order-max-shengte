package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-04-14 18:00
 */
@TableName("sys_set")
@Data
public class SysSet {
    @TableId()
    private String id;
}
