package com.whln.ordermax.data.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author liurun
 * @date 2023-02-25 11:25
 */
@Data
@ApiModel("库存明细")
public class StInventoryListParam {
    @ApiModelProperty("查询参数")
    private String searchInfo;
    @ApiModelProperty("仓库id")
    private String warehouseId;
    @ApiModelProperty("入库类型")
    private String putType;
}
