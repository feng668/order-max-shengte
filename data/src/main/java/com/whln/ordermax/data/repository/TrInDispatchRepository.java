package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.TrInDispatch;
import com.whln.ordermax.data.domain.TrInDispatchCommodity;
import com.whln.ordermax.data.mapper.TrInDispatchCommodityMapper;
import com.whln.ordermax.data.mapper.TrInDispatchMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 */
@Repository
@AllArgsConstructor
public class TrInDispatchRepository extends ServiceImpl<TrInDispatchMapper, TrInDispatch> {

    private final TrInDispatchCommodityMapper trInDispatchCommodityMapper;

    public IPage<TrInDispatch> pageByEntity(TrInDispatch entity, Page<TrInDispatch> page) {
        IPage<TrInDispatch> trInDispatchIPage = baseMapper.listByEntity(entity, page);
        return trInDispatchIPage;
    }

    public List<TrInDispatch> listByEntity(TrInDispatch entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<TrInDispatch> entityList) {
        Boolean result = false;
        if (entityList.size() > 0) {
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows > 0) {
                result = true;
            }
        }
        return result;
    }

    @Resource
    TrInDispatchCommodityRepository trInDispatchCommodityRepository;

    public TrInDispatch getAllById(String id) {
        TrInDispatch trInDispatch = baseMapper.getAllById(id);
        TrInDispatchCommodity trInDispatchCommodity = new TrInDispatchCommodity();
        trInDispatchCommodity.setBillId(trInDispatch.getId());

        List<TrInDispatchCommodity> trInDispatchCommodities = trInDispatchCommodityRepository.listByEntity(trInDispatchCommodity);
        trInDispatch.setCommodityList(trInDispatchCommodities);
        return trInDispatch;
    }

}




