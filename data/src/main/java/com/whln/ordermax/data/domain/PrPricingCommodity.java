package com.whln.ordermax.data.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
* 核价单商品
* @TableName pr_pricing_commodity
*/
@ApiModel("核价单商品")
@TableName("pr_pricing_commodity")
@Data
public class PrPricingCommodity implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    /**
    * id
    */
    private String id;
    @ApiModelProperty("简称")
    @TableField(exist = false)
    private String miniName;

    @TableField(exist = false)
    @NotNull(message = "商品是否含税")
    @ApiModelProperty("是否含税 0 不含1 含税")
    private Integer contactTax;

    @TableField(exist = false)
    @NotNull(message = "元素不能为空")
    @ApiModelProperty("元素")
    private String elements;
    @TableField(exist = false)
    @ApiModelProperty("规格配比")
    private String ratio;

    @TableField(exist = false)
    @NotNull(message = "纯度不能为空")
    @ApiModelProperty("纯度")
    private String purity;
    /**
    * 询盘单id
    */
    @ApiModelProperty("核价单id")
    private String billId;
    /**
    * 商品id
    */
    @ApiModelProperty("商品id")
    private String commodityId;
    /**
    * 单箱数量
    */
    @ApiModelProperty("单箱数量")
    private BigDecimal singleBoxNum;
    @ApiModelProperty("单箱净重")
    private BigDecimal singleBoxNet;

    @ApiModelProperty("单箱毛重")
    private BigDecimal singleBoxWeight;

    @ApiModelProperty("单箱体积")
    private BigDecimal singleBoxVolume;
    /**
    * 箱数
    */
    @ApiModelProperty("箱数")
    private BigDecimal containerNum;
    /**
    * 询盘数量
    */
    @ApiModelProperty("询盘数量")
    private BigDecimal quotationNum;
    /**
    * 单价
    */
    @ApiModelProperty("单价")
    private Double price;
    /**
    * 供应商
    */
    @ApiModelProperty("供应商")
    private String supplierId;
    /**
    * 创建时间
    */
    @ApiModelProperty("创建时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createDate;
    /**
    * 更新时间
    */
    @ApiModelProperty("更新时间")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateDate;
    /**
    * 拥有人id
    */
    @ApiModelProperty("拥有人id")
    private String ownerId;
    /**
    * 金额
    */
    @ApiModelProperty("金额")
    private Double amo;


    /* 附加属性 */
    @TableField(exist = false)
    @ApiModelProperty("商品编号|回显")
    private String partNo;
    @TableField(exist = false)
    @ApiModelProperty("商品中文名称|回显")
    private String cnName;
    @TableField(exist = false)
    @ApiModelProperty("商品英文名|回显")
    private String enName;


    @TableField(exist = false)
    @ApiModelProperty("商品规格|回显")
    private String specification;
    @TableField(exist = false)
    @ApiModelProperty("商品英文规格|回显")
    private String enSpecification;
    @TableField(exist = false)
    @ApiModelProperty("净重|回显")
    private Double cmNetWeight;
    @TableField(exist = false)
    @ApiModelProperty("计量单位|回显")
    private String quantityUnit;
    @TableField(exist = false)
    @ApiModelProperty("增值税率|回显")
    private Double ccVatRate;
    @TableField(exist = false)
    @ApiModelProperty("商品体积|回显")
    private Double cmVolume;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
