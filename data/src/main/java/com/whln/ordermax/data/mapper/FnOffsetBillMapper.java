package com.whln.ordermax.data.mapper;

import com.whln.ordermax.data.domain.FnOffsetBill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * @Entity whln.dyx.data.domain.FnOffsetBill
 */
public interface FnOffsetBillMapper extends BaseMapper<FnOffsetBill> {

    List<FnOffsetBill> listByEntity(@Param("entity")FnOffsetBill entity);

    IPage<FnOffsetBill> listByEntity(@Param("entity")FnOffsetBill entity, Page<FnOffsetBill> page);

    Integer insertBatch(@Param("entitylist")List<FnOffsetBill> entitylist);

    List<FnOffsetBill> listBySuperiorId(@Param("superiorId") String superiorId);
}




