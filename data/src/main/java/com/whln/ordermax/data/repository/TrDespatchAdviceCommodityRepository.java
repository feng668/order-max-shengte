package com.whln.ordermax.data.repository;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.whln.ordermax.data.domain.TrDespatchAdviceCommodity;
import com.whln.ordermax.data.mapper.TrDespatchAdviceCommodityMapper;
import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;

/**
 *
 */
@Repository
public class TrDespatchAdviceCommodityRepository extends ServiceImpl<TrDespatchAdviceCommodityMapper, TrDespatchAdviceCommodity>{

    public IPage<TrDespatchAdviceCommodity> pageByEntity(TrDespatchAdviceCommodity entity, Page<TrDespatchAdviceCommodity> page) {
        return baseMapper.listByEntity(entity,page);
    }

    public List<TrDespatchAdviceCommodity> listByEntity(TrDespatchAdviceCommodity entity) {
        return baseMapper.listByEntity(entity);
    }

    public Boolean insertBatch(List<TrDespatchAdviceCommodity> entityList){
        Boolean result = false;
        if (entityList.size()>0){
            Integer affectedRows = baseMapper.insertBatch(entityList);
            if (affectedRows>0){
                result = true;
            }
        }
        return result;
    }

}




